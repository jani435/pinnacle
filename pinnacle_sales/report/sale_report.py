# -*- coding: utf-8 -*-
from odoo import tools
from odoo import api, fields, models


class SaleReport(models.Model):
    _name = "sale.report.product.level"
    _description = "Product Level Data"
    _auto = False
    _rec_name = 'date'
    _order = 'date desc'


    name = fields.Char('Order Reference', readonly=True)
    date = fields.Datetime('Date Order', readonly=True)
    product_id = fields.Many2one('product.product', 'Product', readonly=True)
    product_uom = fields.Many2one('product.uom', 'Unit of Measure', readonly=True)
    product_uom_qty = fields.Float('# of Qty', readonly=True)
    qty_delivered = fields.Float('Qty Delivered', readonly=True)
    qty_to_invoice = fields.Float('Qty To Invoice', readonly=True)
    qty_invoiced = fields.Float('Qty Invoiced', readonly=True)
    partner_id = fields.Many2one('res.partner', 'Partner', readonly=True)
    company_id = fields.Many2one('res.company', 'Company', readonly=True)
    user_id = fields.Many2one('res.users', 'Salesperson', readonly=True)
    price_total = fields.Float('Total', readonly=True)
    price_subtotal = fields.Float('Untaxed Total', readonly=True)
    product_tmpl_id = fields.Many2one('product.template', 'Product Template', readonly=True)
    categ_id = fields.Many2one('product.category', 'Product Category', readonly=True)
    nbr = fields.Integer('# of Lines', readonly=True)
    pricelist_id = fields.Many2one('product.pricelist', 'Pricelist', readonly=True)
    analytic_account_id = fields.Many2one('account.analytic.account', 'Analytic Account', readonly=True)
    team_id = fields.Many2one('crm.team', 'Sales Team', readonly=True, oldname='section_id')
    country_id = fields.Many2one('res.country', 'Partner Country', readonly=True)
    commercial_partner_id = fields.Many2one('res.partner', 'Commercial Entity', readonly=True)
    weight = fields.Float('Gross Weight', readonly=True)
    volume = fields.Float('Volume', readonly=True)
    state = fields.Selection([
        ('quote_draft', 'Draft Quote'),
        ('quote_sent', 'Quote Sent'),
        ('closed_quote', 'Quote Closed'),
        ('quote_vir_processing', 'Virtual Processing'),
        ('quote_review', 'Review Quote'),
        ('quote_man_approval', 'Manager Approval'),
        ('quote_man_approved', 'Manager Approved'),
        ('quote_man_rejected', 'Manager Rejected'),
        ('quote_approved', 'Quote Approved'),
        ('quote_rejected', 'Quote Rejected'),
        ('draft', 'Draft'),
        ('art_processing', 'Art Processing'),
        ('pending_review', 'Pending Review'),
        ('to_approve', 'Manager Approval'),
        ('approve', 'Manager Approved'),
        ('reject', 'Manager Rejected'),
        ('sent', 'Confirmation Sent'),
        ('confirmation_received', 'Confirmation Received'),
        ('accounting_approval', 'Accounting Approval'),
        ('accounting_approved', 'Accounting Approved'),
        ('accounting_rejected', 'Accounting Rejected'),
        ('accounting_p_approval', 'Accounting Payment Approval'),
        ('accounting_p_approved', 'Accounting Payment Approved'),
        ('accounting_p_rejected', 'Accounting Payment Rejected'),
        ('sale', 'PO Generated'),
        ('shipped', 'Shipped'),
        ('delivered', 'Delivered'),
        ('partially_invoiced', 'Partially Invoiced'),
        ('invoiced', 'Invoiced'),
        ('close', 'Closed'),
        ('reopen', 'Re-Opened'),
        ('done', 'Re-Closed'),
        ('cancel', 'Cancelled'),
        ('hold', 'On Hold'),
        ('inactive','Inactive'),
    ], string='Status', 
        readonly=True)
    change_supplier = fields.Many2one('product.supplierinfo', 'Change Supplier', readonly=True)
    product_subtotal = fields.Float('Total Revenue', readonly=True)
    avg_qty = fields.Float('Avg Qty per order', readonly=True, group_operator="avg")
    supplier = fields.Many2one('res.partner', readonly=True)

    def _select(self):
        """Returns : Select String with appropriate values"""
        select_str = """
            WITH currency_rate as (%s)
             SELECT min(l.id) as id,
                    l.product_id as product_id,
                    l.change_supplier as change_supplier,
                    t.uom_id as product_uom,
                    sum(l.product_uom_qty * l.price_unit) as product_subtotal,
                    sum(l.product_uom_qty / u.factor * u2.factor) as avg_qty,
                    sum(l.product_uom_qty / u.factor * u2.factor) as product_uom_qty,
                    sum(l.qty_delivered / u.factor * u2.factor) as qty_delivered,
                    sum(l.qty_invoiced / u.factor * u2.factor) as qty_invoiced,
                    sum(l.qty_to_invoice / u.factor * u2.factor) as qty_to_invoice,
                    sum(l.price_total / COALESCE(cr.rate, 1.0)) as price_total,
                    sum(l.price_subtotal / COALESCE(cr.rate, 1.0)) as price_subtotal,
                    count(*) as nbr,
                    s.name as name,
                    s.date_order as date,
                    s.state as state,
                    s.partner_id as partner_id,
                    s.user_id as user_id,
                    s.company_id as company_id,
                    extract(epoch from avg(date_trunc('day',s.date_order)-date_trunc('day',s.create_date)))/(24*60*60)::decimal(16,2) as delay,
                    t.categ_id as categ_id,
                    s.pricelist_id as pricelist_id,
                    s.project_id as analytic_account_id,
                    s.team_id as team_id,
                    p.product_tmpl_id,
                    psi.name as supplier,
                    partner.country_id as country_id,
                    partner.commercial_partner_id as commercial_partner_id,
                    sum(p.weight * l.product_uom_qty / u.factor * u2.factor) as weight,
                    sum(p.volume * l.product_uom_qty / u.factor * u2.factor) as volume
        """ % self.env['res.currency']._select_companies_rates()
        return select_str

    def _from(self):
        """Returns: From String with Appropriate Values"""
        from_str = """
                sale_order_line l
                      join sale_order s on (l.order_id=s.id)
                      join res_partner partner on s.partner_id = partner.id
                        left join product_supplierinfo psi on (l.change_supplier=psi.id)
                            left join res_partner rs on (psi.name=rs.id)
                        left join product_product p on (l.product_id=p.id)
                            left join product_template t on (p.product_tmpl_id=t.id)
                    left join product_uom u on (u.id=l.product_uom)
                    left join product_uom u2 on (u2.id=t.uom_id)
                    left join product_pricelist pp on (s.pricelist_id = pp.id)
                    left join currency_rate cr on (cr.currency_id = pp.currency_id and
                        cr.company_id = s.company_id and
                        cr.date_start <= coalesce(s.date_order, now()) and
                        (cr.date_end is null or cr.date_end > coalesce(s.date_order, now())))
        """
        return from_str

    def _group_by(self):
        """Returns: Group By String with Appropriate Values"""
        group_by_str = """
            GROUP BY l.product_id,
                    l.change_supplier,
                    l.order_id,
                    t.uom_id,
                    t.categ_id,
                    s.name,
                    s.date_order,
                    s.partner_id,
                    s.user_id,
                    s.state,
                    s.company_id,
                    s.pricelist_id,
                    s.project_id,
                    s.team_id,
                    p.product_tmpl_id,
                    psi.name,
                    partner.country_id,
                    partner.commercial_partner_id
        """
        return group_by_str

    @api.model_cr
    def init(self):
        """Create or Replace the database view for sales_report.
        This method will call when module Install or Upgrade"""
        # self._table = sale_report
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""CREATE or REPLACE VIEW %s as (
            %s
            FROM ( %s )
            %s
            )""" % (self._table, self._select(), self._from(), self._group_by()))
