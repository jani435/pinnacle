# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import tools
from odoo import api, fields, models


class ArtJobsCompletedByDesignerReport(models.Model):
    _name = "art.jobs.completed.by.designer.report"
    _description = "Art Jobs Completed By Designer Report"
    _auto = False

    designer_id = fields.Many2one('res.users', 'Designer', readonly=True)
    total_jobs = fields.Integer('# of jobs completed by Designer', readonly=True)
    create_date = fields.Datetime('Create Date', readonly=True)

    def _select(self):
        """Returns: select string with appropriate values"""
        select_str = """
             SELECT min(l.id) as id,
                    l.designer_id as designer_id,
                    min(l.create_date) as create_date,
                    count(DISTINCT l.request_type) as total_jobs
        """
        return select_str

    def _from(self):
        """Returns: from string with appropriate values"""
        from_str = """
                track_issue l
                left join art_production s on l.art_id = s.id

        """
        return from_str

    def _group_by(self):
        """Returns: group_by string with appropriate values"""
        group_by_str = """
            GROUP BY l.designer_id                    
                    
        """
        return group_by_str

    @api.model_cr
    def init(self):
        """Create or Replace database view for report.
        This method will call when Module Installed or Upgrade"""
        # self._table = art_report
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""CREATE or REPLACE VIEW %s as (
            %s
            FROM ( %s )
            %s
            )""" % (self._table, self._select(), self._from(), self._group_by()))


class ArtRushJobsCompletedByDesignerReport(models.Model):
    _name = "art.rush.jobs.completed.by.designer.report"
    _description = "Art Rush Jobs Completed By Designer Report"
    _auto = False

    designer_id = fields.Many2one('res.users', 'Designer', readonly=True)
    rush_jobs = fields.Integer('# of Rush jobs completed by Designer', readonly=True)
    create_date = fields.Datetime('Create Date', readonly=True)

    def _select(self):
        """Returns: select string with appropriate values"""
        select_str = """
             SELECT min(l.id) as id,
                    l.designer_id as designer_id,
                    min(l.create_date) as create_date,
                    count(DISTINCT l.rush) as rush_jobs
        """
        return select_str

    def _from(self):
        """Returns: from string with appropriate values"""
        from_str = """
                track_issue l
                left join art_production s on l.art_id = s.id

        """
        return from_str

    def _group_by(self):
        """Returns: group_by string with appropriate values"""
        group_by_str = """
            GROUP BY l.designer_id,
                    l.request_type                   
                    
        """
        return group_by_str

    @api.model_cr
    def init(self):
        """Create or Replace database view for report.
        This method will call when Module Installed or Upgrade"""
        # self._table = art_report
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""CREATE or REPLACE VIEW %s as (
            %s
            FROM ( %s )
            %s
            )""" % (self._table, self._select(), self._from(), self._group_by()))


class ArtRushPercentageJobsCompletedByDesignerReport(models.Model):
    _name = "art.rush.percentage.jobs.completed.by.designer.report"
    _description = "Art Rush Percentage Jobs Completed By Designer Report"
    _auto = False

    designer_id = fields.Many2one('res.users', 'Designer', readonly=True)
    per_rush_jobs = fields.Float('(%) of total jobs completed by Designer that are Rush', readonly=True, group_operator="avg")
    create_date = fields.Datetime('Create Date', readonly=True)

    def _select(self):
        """Returns: select string with appropriate values"""
        select_str = """
             SELECT min(l.id) as id,
                    l.designer_id as designer_id,
                    min(l.create_date) as create_date,
                    (count(DISTINCT l.rush) * 1.0 / count(DISTINCT l.request_type) * 100) as per_rush_jobs
        """
        return select_str

    def _from(self):
        """Returns: from string with appropriate values"""
        from_str = """
                track_issue l
                left join art_production s on l.art_id = s.id

        """
        return from_str

    def _group_by(self):
        """Returns: group_by string with appropriate values"""
        group_by_str = """
            GROUP BY l.designer_id,
                    l.request_type                   
                    
        """
        return group_by_str

    @api.model_cr
    def init(self):
        """Create or Replace database view for report.
        This method will call when Module Installed or Upgrade"""
        # self._table = art_report
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""CREATE or REPLACE VIEW %s as (
            %s
            FROM ( %s )
            %s
            )""" % (self._table, self._select(), self._from(), self._group_by()))


class ArtRushJobsSubmiitedBySalesUserReport(models.Model):
    _name = "art.rush.jobs.submitted.by.sales.user.report"
    _description = "Art Rush Jobs Submiited By Sales User Report"
    _auto = False

    user_id = fields.Many2one('res.users', 'Sales User', readonly=True)
    rush_sales_jobs = fields.Integer('# of Rush Jobs Submitted by Sales User', readonly=True)
    create_date = fields.Datetime('Create Date', readonly=True)

    def _select(self):
        """Returns: select string with appropriate values"""
        select_str = """
             SELECT min(l.id) as id,
                    l.user_id as user_id,
                    min(l.create_date) as create_date,
                    count(DISTINCT l.rush) as rush_sales_jobs
        """
        return select_str

    def _from(self):
        """Returns: from string with appropriate values"""
        from_str = """
                track_issue l
                left join art_production s on l.art_id = s.id

        """
        return from_str

    def _group_by(self):
        """Returns: group_by string with appropriate values"""
        group_by_str = """
            GROUP BY l.user_id,
                    l.request_type                   
                    
        """
        return group_by_str

    @api.model_cr
    def init(self):
        """Create or Replace database view for report.
        This method will call when Module Installed or Upgrade"""
        # self._table = art_report
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""CREATE or REPLACE VIEW %s as (
            %s
            FROM ( %s )
            %s
            )""" % (self._table, self._select(), self._from(), self._group_by()))


class ArtRushPercentageJobsSubmittedBySalesUserReport(models.Model):
    _name = "art.rush.percentage.jobs.submitted.by.sales.user.report"
    _description = "Art Rush Percentage Jobs Submitted By Sales User Report"
    _auto = False

    user_id = fields.Many2one('res.users', 'Sales User', readonly=True)
    per_sales_rush_jobs = fields.Float('(%) of total jobs submitted by Sales User that are Rush', readonly=True, group_operator="avg")
    create_date = fields.Datetime('Create Date', readonly=True)

    def _select(self):
        """Returns: select string with appropriate values"""
        select_str = """
             SELECT min(l.id) as id,
                    l.user_id as user_id,
                    min(l.create_date) as create_date,
                    (count(DISTINCT l.rush) * 1.0 / count(DISTINCT l.request_type) * 100) as per_sales_rush_jobs
        """
        return select_str

    def _from(self):
        """Returns: from string with appropriate values"""
        from_str = """
                track_issue l
                left join art_production s on l.art_id = s.id

        """
        return from_str

    def _group_by(self):
        """Returns: group_by string with appropriate values"""
        group_by_str = """
            GROUP BY l.user_id,
                    l.request_type                   
                    
        """
        return group_by_str

    @api.model_cr
    def init(self):
        """Create or Replace database view for report.
        This method will call when Module Installed or Upgrade"""
        # self._table = art_report
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""CREATE or REPLACE VIEW %s as (
            %s
            FROM ( %s )
            %s
            )""" % (self._table, self._select(), self._from(), self._group_by()))


class ArtMistakesLoggedForDesignerReport(models.Model):
    _name = "art.mistakes.logged.for.designer.report"
    _description = "Art Mistakes Logged For Designer Report"
    _auto = False

    designer_id = fields.Many2one('res.users', 'Designer', readonly=True)
    total_logged_designer_jobs = fields.Integer('# of mistakes logged for Designer', readonly=True)
    create_date = fields.Datetime('Create Date', readonly=True)

    def _select(self):
        """Returns: select string with appropriate values"""
        select_str = """
             SELECT min(l.id) as id,
                    l.designer_id as designer_id,
                    min(l.create_date) as create_date,
                    count(*) as total_logged_designer_jobs
        """
        return select_str

    def _from(self):
        """Returns: from string with appropriate values"""
        from_str = """
                track_issue l
                left join art_production s on l.art_id = s.id

        """
        return from_str

    def _group_by(self):
        """Returns: group_by string with appropriate values"""
        group_by_str = """
            GROUP BY l.designer_id,
                    l.request_type                
                    
        """
        return group_by_str

    @api.model_cr
    def init(self):
        """Create or Replace database view for report.
        This method will call when Module Installed or Upgrade"""
        # self._table = art_report
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""CREATE or REPLACE VIEW %s as (
            %s
            FROM ( %s )
            %s
            )""" % (self._table, self._select(), self._from(), self._group_by()))


class ArtMistakesLoggedForSalesUserReport(models.Model):
    _name = "art.mistakes.logged.for.sales.user.report"
    _description = "Art Mistakes Logged For Sales User Report"
    _auto = False

    user_id = fields.Many2one('res.users', 'Sales User', readonly=True)
    total_logged_sales_jobs = fields.Integer('# of mistakes logged for Sales User', readonly=True)
    create_date = fields.Datetime('Create Date', readonly=True)

    def _select(self):
        """Returns: select string with appropriate values"""
        select_str = """
             SELECT min(l.id) as id,
                    l.user_id as user_id,
                    min(l.create_date) as create_date,
                    count(*) as total_logged_sales_jobs
        """
        return select_str

    def _from(self):
        """Returns: from string with appropriate values"""
        from_str = """
                track_issue l
                left join art_production s on l.art_id = s.id

        """
        return from_str

    def _group_by(self):
        """Returns: group_by string with appropriate values"""
        group_by_str = """
            GROUP BY l.user_id,
                    l.request_type                
                    
        """
        return group_by_str

    @api.model_cr
    def init(self):
        """Create or Replace database view for report.
        This method will call when Module Installed or Upgrade"""
        # self._table = art_report
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""CREATE or REPLACE VIEW %s as (
            %s
            FROM ( %s )
            %s
            )""" % (self._table, self._select(), self._from(), self._group_by()))


class ArtMistakesLoggedForArtTeamReport(models.Model):
    _name = "art.mistakes.logged.for.art.team.report"
    _description = "Art Mistakes Logged For Art Team Report"
    _auto = False

    designer_id = fields.Many2one('res.users', 'Designer', readonly=True)
    art_team_logged = fields.Integer('# of mistakes logged for Art Team', readonly=True)
    create_date = fields.Datetime('Create Date', readonly=True)

    def _select(self):
        """Returns: select string with appropriate values"""
        select_str = """
             SELECT min(l.id) as id,
                    l.designer_id as designer_id,
                    min(l.create_date) as create_date,
                    count(*) as art_team_logged
        """
        return select_str

    def _from(self):
        """Returns: from string with appropriate values"""
        from_str = """
                track_issue l

        """
        return from_str

    def _group_by(self):
        """Returns: group_by string with appropriate values"""
        group_by_str = """
            GROUP BY l.designer_id,
                    l.request_type                
                    
        """
        return group_by_str

    @api.model_cr
    def init(self):
        """Create or Replace database view for report.
        This method will call when Module Installed or Upgrade"""
        # self._table = art_report
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""CREATE or REPLACE VIEW %s as (
            %s
            FROM 
            %s
            WHERE 
            l.notes_team = 'art'
            %s
            )""" % (self._table, self._select(), self._from(), self._group_by()))


class ArtMistakesLoggedForSalesTeamReport(models.Model):
    _name = "art.mistakes.logged.for.sales.team.report"
    _description = "Art Mistakes Logged For Sales Team Report"
    _auto = False

    user_id = fields.Many2one('res.users', 'Sales User', readonly=True)
    sales_team_logged = fields.Integer('# of mistakes logged for Sales Team', readonly=True)
    create_date = fields.Datetime('Create Date', readonly=True)

    def _select(self):
        """Returns: select string with appropriate values"""
        select_str = """
             SELECT min(l.id) as id,
                    l.user_id as user_id,
                    min(l.create_date) as create_date,
                    count(*) as sales_team_logged
        """
        return select_str

    def _from(self):
        """Returns: from string with appropriate values"""
        from_str = """
                track_issue l
        """
        return from_str

    def _group_by(self):
        """Returns: group_by string with appropriate values"""
        group_by_str = """
            GROUP BY l.user_id         
                    
        """
        return group_by_str

    @api.model_cr
    def init(self):
        """Create or Replace database view for report.
        This method will call when Module Installed or Upgrade"""
        # self._table = art_report
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""CREATE or REPLACE VIEW %s as (
            %s
            FROM 
            %s
            WHERE
            l.notes_team = 'sales'
            %s
            )""" % (self._table, self._select(), self._from(), self._group_by()))


class ArtRevisionJobsCompletedByDesignerReport(models.Model):
    _name = "art.revision.jobs.completed.by.designer.report"
    _description = "Art Revision Jobs Completed By Designer Report"
    _auto = False

    designer_id = fields.Many2one('res.users', 'Designer', readonly=True)
    revision_jobs = fields.Integer('# of Revision jobs completed by Designer', readonly=True)
    create_date = fields.Datetime('Create Date', readonly=True)

    def _select(self):
        """Returns: select string with appropriate values"""
        select_str = """
             SELECT min(l.id) as id,
                    l.designer_id as designer_id,
                    min(l.create_date) as create_date,
                    count(*) as revision_jobs
        """
        return select_str

    def _from(self):
        """Returns: from string with appropriate values"""
        from_str = """
                track_issue l
        """
        return from_str

    def _group_by(self):
        """Returns: group_by string with appropriate values"""
        group_by_str = """
            GROUP BY l.designer_id         
                    
        """
        return group_by_str

    @api.model_cr
    def init(self):
        """Create or Replace database view for report.
        This method will call when Module Installed or Upgrade"""
        # self._table = art_report
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""CREATE or REPLACE VIEW %s as (
            %s
            FROM 
            %s
            WHERE
            l.request_type = 'revision'
            %s
            )""" % (self._table, self._select(), self._from(), self._group_by()))

class ArtVirtualJobsCompletedByDesignerReport(models.Model):
    _name = "art.virtual.jobs.completed.by.designer.report"
    _description = "Art Virtual Jobs Completed By Designer Report"
    _auto = False

    designer_id = fields.Many2one('res.users', 'Designer', readonly=True)
    total_virtual_jobs = fields.Integer('# of Virtual jobs completed by Designer that become Sales Orders', readonly=True)
    create_date = fields.Datetime('Create Date', readonly=True)

    def _select(self):
        """Returns: select string with appropriate values"""
        select_str = """
             SELECT min(l.id) as id,
                    l.designer_id as designer_id,
                    min(l.create_date) as create_date,
                    count(*) as total_virtual_jobs
        """
        return select_str

    def _from(self):
        """Returns: from string with appropriate values"""
        from_str = """
                track_issue l
        """
        return from_str

    def _group_by(self):
        """Returns: group_by string with appropriate values"""
        group_by_str = """
            GROUP BY l.designer_id         
                    
        """
        return group_by_str

    @api.model_cr
    def init(self):
        """Create or Replace database view for report.
        This method will call when Module Installed or Upgrade"""
        # self._table = art_report
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""CREATE or REPLACE VIEW %s as (
            %s
            FROM 
            %s
            WHERE
            l.request_type = 'virtual_spec'
            %s
            )""" % (self._table, self._select(), self._from(), self._group_by()))


class ArtJobsSubmittedRushReport(models.Model):
    _name = "art.jobs.submitted.rush.report"
    _description = "Art Jobs Submitted Rush Report"
    _auto = False

    designer_id = fields.Many2one('res.users', 'Designer', readonly=True)
    total_jobs_rush = fields.Integer('# of total jobs submitted as a Rush', readonly=True)
    create_date = fields.Datetime('Create Date', readonly=True)

    def _select(self):
        """Returns: select string with appropriate values"""
        select_str = """
             SELECT min(l.id) as id,
                    l.designer_id as designer_id,
                    min(l.create_date) as create_date,
                    count(*) as total_jobs_rush
        """
        return select_str

    def _from(self):
        """Returns: from string with appropriate values"""
        from_str = """
                track_issue l
        """
        return from_str

    def _group_by(self):
        """Returns: group_by string with appropriate values"""
        group_by_str = """
            GROUP BY l.designer_id         
                    
        """
        return group_by_str

    @api.model_cr
    def init(self):
        """Create or Replace database view for report.
        This method will call when Module Installed or Upgrade"""
        # self._table = art_report
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""CREATE or REPLACE VIEW %s as (
            %s
            FROM 
            %s
            WHERE
            l.rush = 'Rush'
            %s
            )""" % (self._table, self._select(), self._from(), self._group_by()))

