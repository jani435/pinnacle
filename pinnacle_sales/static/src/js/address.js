odoo.define('pinnacle_sales.address', function (require) {
"use strict";

var core = require('web.core');
var form_common = require('web.form_common');
var data = require('web.data');
var Model = require('web.DataModel');
var object
var _t = core._t;
var autocomplete
var place

var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        sublocality_level_1: 'long_name',
        administrative_area_level_1: 'long_name',
        country: 'long_name',
        postal_code: 'short_name'
      };

var AddressAutofill = form_common.FormWidget.extend(form_common.ReinitializeWidgetMixin, {
	
	events: {
        "click #autocomplete": "geolocate",
    },
    template : "AddressAutofill",
	init: function() {
        this._super.apply(this, arguments);
    },
    start: function(){
    	object = this;
    	var key
    	var param = new Model('ir.config_parameter').call('get_param',['google_api_key',new data.CompoundContext()]).
			    		then(function (result) {
			    			if (typeof(result) != "string")
			    			{
			    				console.log("The system parameter 'google_api_key' is not defined.");
			    			}
							if (result){
								$.getScript('https://maps.googleapis.com/maps/api/js?key='+result+'&libraries=places', function() {
						            autocomplete = new google.maps.places.Autocomplete(
						            (document.getElementById('autocomplete')),
						            {types: ['geocode']});
						        	autocomplete.addListener('place_changed', object.fillInAddress);
						         });
							}
						});
    	
    },
    fillInAddress: function() {
       var self = this;
       if(autocomplete.getPlace()){ 
	        place = autocomplete.getPlace();
	        componentForm['street_number'] = 'short_name'
	        componentForm['route'] = 'long_name'
	        componentForm['locality'] = 'long_name'
	        componentForm['sublocality_level_1'] = 'long_name'
	        componentForm['administrative_area_level_1'] = 'long_name'
	        componentForm['country'] = 'long_name'
	        componentForm['postal_code'] = 'short_name'
	        
	        for (var i = 0; i < place.address_components.length; i++) {
	          var addressType = place.address_components[i].types[0];
	          if (componentForm[addressType]) {
	            componentForm[addressType] = place.address_components[i][componentForm[addressType]];
	          }
	        }
	        object.setAddress(componentForm);
	    }
    },
    setAddress: function(componentForm){
    	if (componentForm){
	    	var self = this;
	    	$(".o_address_format :input").val('')
	    	var route = null
	    	var street = null
	    	if (componentForm['street_number'] != 'short_name'){
	    		if (componentForm['route'] != 'long_name')
	    			route = (componentForm['route'])
	    		street = componentForm['street_number']+' '+route
	    		self.field_manager.fields.street.set_value(street)
	    	}
	    	if (componentForm['sublocality_level_1'] != 'long_name')
	    		self.field_manager.fields.street2.set_value(componentForm['sublocality_level_1'])
	    	if (componentForm['locality'] != 'long_name')
	    		self.field_manager.fields.city.set_value(componentForm['locality'])
	    	if (componentForm['administrative_area_level_1'] != 'long_name')
	    		var domain = [['name', '=', componentForm['administrative_area_level_1']]];
	    		var state_id = new Model('res.country.state').query(['id']).filter(domain).first().
	    		then(function (result) {
					if (result){
						self.field_manager.fields.state_id.set_value(result.id)
					}
				});
	    	if (componentForm['country'] != 'long_name')
	    		var domain = [['name', '=', componentForm['country']]];
	    		var country_id = new Model('res.country').query(['id']).filter(domain).first().
	    		then(function (result) {
					if (result){
						self.field_manager.fields.country_id.set_value(result.id)
					}
				});
	    	if (componentForm['postal_code'] != 'short_name')
	    		self.field_manager.fields.zip.set_value(componentForm['postal_code'])
	    }

    },
    geolocate: function(event) {
    	object = this;
    	 
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
       
    	
    }
});

core.form_custom_registry.add('address_autofill', AddressAutofill);

});

odoo.define('web.form_widgets_pinnacle', function (require) {
"use strict";
var core = require('web.core');
var FieldChar = core.form_widget_registry.get('char');

	var FieldEmail = FieldChar.extend({
	    template: 'FieldEmail',
	    prefix: 'mailto',
	    init: function() {
	        this._super.apply(this, arguments);
	        this.clickable = true;
	    },
	    initialize_content: function() {
	        this._super();
	        var $button = this.$el.find('button');
	        $button.click(this.on_button_clicked);
	        this.setupFocus($button);
	    },
	    render_value: function() {
	        this._super();
	        if (this.get("effective_readonly") && this.clickable) {
	            this.$el.attr('href', this.prefix + ':' + this.get('value'));
	        }
	    },
	    on_button_clicked: function() {
	        if (!this.get('value') || !this.is_syntax_valid()) {
	            this.do_warn(_t("E-mail Error"), _t("Can't send email to invalid e-mail address"));
	        } else {
	            location.href = 'mailto:' + this.get('value');
	        }
	    }
	});

core.form_widget_registry.add('email', FieldEmail)
});

