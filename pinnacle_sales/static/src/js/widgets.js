odoo.define('pinnacle_sales.pinnacle_sales_widgets', function (require) {
    "use strict";
    var ajax = require('web.ajax');
    var core = require('web.core');
    var Dialog = require('web.Dialog');
    var Model = require('web.DataModel');
    var FormView = require('web.FormView');
    var DateWidget = require('web.datepicker').DateWidget;
    var _t = core._t;
    var QWeb = core.qweb;
    var FieldOne2Many = core.form_widget_registry.get('one2many_list');
    var Sidebar = require('web.Sidebar');
    var data = require('web.data');
    var time = require('web.time');
    var FieldDate = core.form_widget_registry.get('date');
    var List = core.view_registry.get('list');
    var FieldUrl = core.form_widget_registry.get('url');
    var list_widget_registry = core.list_widget_registry;
    var ColumnChar =  list_widget_registry.get('field.char')
    var FieldMany2ManyTags = core.form_widget_registry.get('many2many_tags')
    var FieldFloat = core.form_widget_registry.get('float');
    var FieldBinaryFile = core.form_widget_registry.get('binary');
    var kanban = require('web_kanban.KanbanView');
    var cfs_url = ''

    Dialog.confirmation_popup = function (owner, message, options) {
        var buttons = [
            {
                text: _t("YES, CLEAR !"),
                classes: 'btn-primary',
                close: true,
                click: options && options.confirm_callback
            },
            {
                text: _t("NO, KEEP IT !"),
                classes: 'btn-primary',
                close: true,
                click: options && options.cancel_callback
            }
        ];
        return new Dialog(owner, _.extend({
            size: 'medium',
            buttons: buttons,
            $content: $('<div>', {
                text: message,
            }),
            title: _t("Confirmation"),
        }, options)).open();
    };

    new Model('ir.config_parameter').call('get_param',['cfs.web.url',new data.CompoundContext()]).
                        then(function (result) {
                            if (result){
                                cfs_url = result
                            }
                        }
    );

    List.include({
       render_buttons: function(){
            var self = this;
            this._super.apply(this, arguments)
            this.wizard_id = false
            ajax.jsonRpc('/web/fetch/quick_action_id').then(function(result) {
                    if (result){
                        self.wizard_id = result
                    }
             });
            this.$buttons.find('.o_call_create_wizard').click(function(){
                self.rpc("/web/action/load", {
                    action_id: self.wizard_id,
                    context: {},
                }).done(function(result) {
                    result.context = new data.CompoundContext(
                        result.context || {})
                            .set_eval_context();
                    result.flags = result.flags || {};
                    result.flags.new_window = true;
                    self.do_action(result, {
                        on_close: function() {
                            
                        },
                    });
                });
            })
       }
    });

    kanban.include({
       render_buttons: function(){
            var self = this;
            this._super.apply(this, arguments)
            this.wizard_id = false
            ajax.jsonRpc('/web/fetch/quick_action_id').then(function(result) {
                    if (result){
                        self.wizard_id = result
                    }
             });
            if (self.$buttons){
                self.$buttons.find('.o_call_create_wizard').click(function(){
                    self.rpc("/web/action/load", {
                        action_id: self.wizard_id,
                        context: {},
                    }).done(function(result) {
                        result.context = new data.CompoundContext(
                            result.context || {})
                                .set_eval_context();
                        result.flags = result.flags || {};
                        result.flags.new_window = true;
                        self.do_action(result, {
                            on_close: function() {
                            },
                        });
                    });
                })
            }
       }
    });

    FieldFloat.include({
        start: function(){
            this._super(this, arguments);
            var self = this;
            if (this.options.digit_limit){
                this.$el.bind("cut copy paste",function(e) {
                      e.preventDefault();
                   });
                this.$el.keypress(function(evt){
                    var el = this;
                    var charCode = (evt.which) ? evt.which : event.keyCode;
                    var number = el.value.split('.');
                    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
                        return false;
                    }
                    if(number.length>1 && charCode == 46){
                        return false;
                    }
                    var caratPos = self.getSelectionStart(el);
                    var dotPos = el.value.indexOf(".");
                    if( caratPos > dotPos && dotPos>-1 && (number[1].length > 1)){
                        return false;
                    }
                    return true;
                });
            };
        },
        getSelectionStart: function(o) {
            if (o.createTextRange) {
                var r = document.selection.createRange().duplicate()
                r.moveEnd('character', o.value.length)
                if (r.text == '') return o.value.length
                return o.value.lastIndexOf(r.text)
            } else return o.selectionStart
        },
    });

    List.List.include({
        //name_get issue in one2many not passing context
        render_cell: function (record, column) {
            var value;
            if(column.type === 'reference') {
                value = record.get(column.id);
                var ref_match;
                // Ensure that value is in a reference "shape", otherwise we're
                // going to loop on performing name_get after we've resolved (and
                // set) a human-readable version. m2o does not have this issue
                // because the non-human-readable is just a number, where the
                // human-readable version is a pair
                if (value && (ref_match = /^([\w\.]+),(\d+)$/.exec(value))) {
                    // reference values are in the shape "$model,$id" (as a
                    // string), we need to split and name_get this pair in order
                    // to get a correctly displayable value in the field
                    var model = ref_match[1],
                        id = parseInt(ref_match[2], 10);

                    new data.DataSet(this.view, model).name_get([id]).done(function(names) {
                        if (!names.length) { return; }
                        record.set(column.id + '__display', names[0][1]);
                    });
                }
            } else if (column.type === 'many2one') {
                value = record.get(column.id);
                // m2o values are usually name_get formatted, [Number, String]
                // pairs, but in some cases only the id is provided. In these
                // cases, we need to perform a name_get call to fetch the actual
                // displayable value
                if (typeof value === 'number' || value instanceof Number) {
                    // fetch the name, set it on the record (in the right field)
                    // and let the various registered events handle refreshing the
                    // row
                    new Model(column.relation)
                        .call('name_get', [value, this.dataset.get_context()]).done(function (names) {
                        if (!names.length) { return; }
                        record.set(column.id, names[0]);
                    });
                }
            } else if (column.type === 'many2many') {
                value = record.get(column.id);
                // non-resolved (string) m2m values are arrays
                if (value instanceof Array && !_.isEmpty(value)
                        && !record.get(column.id + '__display')) {
                    var ids;
                    // they come in two shapes:
                    if (value[0] instanceof Array) {
                        _.each(value, function(command) {
                            switch (command[0]) {
                                case 4: ids.push(command[1]); break;
                                case 5: ids = []; break;
                                case 6: ids = command[2]; break;
                                default: throw new Error(_.str.sprintf( _t("Unknown m2m command %s"), command[0]));
                            }
                        });
                    } else {
                        // 2. an array of ids
                        ids = value;
                    }
                    new Model(column.relation)
                        .call('name_get', [ids, this.dataset.get_context()]).done(function (names) {
                            // FIXME: nth horrible hack in this poor listview
                            record.set(column.id + '__display',
                                       _(names).pluck(1).join(', '));
                            record.set(column.id, ids);
                        });
                    // temporary empty display name
                    record.set(column.id + '__display', false);
                }
            }
            return column.format(record.toForm().data, {
                model: this.dataset.model,
                id: record.get('id')
            });
        }
    });
    FormView.include({ 
        _actualize_mode: function() {
            if (this.get("actual_mode") == "edit")
            {   
                if (this.model == 'sale.order.edit.decoration.line' || this.model == 'sale.order.edit.multi.decoration.line'
                            || this.model == 'sale.order.add.decoration.line' || this.model == 'product.imprint' 
                            || this.model == 'product.decoration.customer.art' || this.model == 'res.finisher' 
                            || this.model == 'sale.advance.payment.inv' || 
                            this.model == 'product.template.multi.images')
                {
                    this.do_onchange(null).done(function(){
                        if (this._super != null)
                        {this._super.apply(this, arguments);}
                    });
                }
                else
                {
                    this._super.apply(this, arguments);
                }
            }
            else
            {
                this._super.apply(this, arguments);
            }
        },
        on_button_hold: function() {
            var self = this;
            if (self.model == 'art.production'){
                self.session.user_has_group('sales_team.group_sale_manager')
                            .then(function(flag) {
                                if (flag){
                                    new Model(self.model).call("action_hold", [[self.datarecord.id], {}]).then(function (result)
                                    {
                                        if (result == true)
                                        {
                                            self.reload();
                                        }
                                    });
                                }
                                else{
                                    Dialog.alert(self, _t('You are not allowed to do this operation.'), {
                                        title: _t('Warning'),
                                    });
                                }
                            });    
            }
            else{
                new Model(self.model).call("action_hold", [[self.datarecord.id], {}]).then(function (result)
                {
                    if (result == true)
                    {
                        self.reload();
                    }
                });
            }
        },

        on_button_cancel_record: function() {
            var self = this;
            new Model(self.model).call("action_cancel", [[self.datarecord.id], {}]).then(function (result)
            {
                if (result == true)
                {
                    self.reload();
                }
            });
        },

        on_button_mark_inactive_record: function() {
            var self = this;
            new Model(self.model).call("action_mark_inactive", [[self.datarecord.id], {}]).then(function (result)
            {
                if (result == true)
                {
                    self.reload();
                }
            });
        },

        on_button_force_out: function() {
            var self = this;
            new Model(self.model).call("action_force_out", [[self.datarecord.id], {}]).then(function (result)
            {
                if (result == true)
                {
                    self.reload();
                }
            });
        },

        render_sidebar: function($node) {
            if (!this.sidebar && this.options.sidebar) {
                this.sidebar = new Sidebar(this, {editable: this.is_action_enabled('edit')});
                if (this.fields_view.toolbar) {
                    this.sidebar.add_toolbar(this.fields_view.toolbar);
                }
                if (this.model == 'sale.order' || this.model == 'purchase.order' || this.model == 'art.production'){
                    this.sidebar.add_items('other', _.compact([
                        this.is_action_enabled('delete') && { label: _t('Delete'), callback: this.on_button_delete },
                        this.is_action_enabled('create') && { label: _t('Duplicate'), callback: this.on_button_duplicate },
                        this.is_action_enabled('edit') && { label: _t('Put on Hold'), callback: this.on_button_hold }
                    ]));
                    if (this.model == 'sale.order' || this.model == 'purchase.order')
                    {
                        this.sidebar.add_items('other', _.compact([
                            this.is_action_enabled('cancel_record') && { label: _t('Cancel'), callback: this.on_button_cancel_record }
                        ]));
                    }
                    if (this.model == 'sale.order')
                    {
                        this.sidebar.add_items('other', _.compact([
                            this.is_action_enabled('mark_inactive') && { label: _t('Mark Inactive'), callback: this.on_button_mark_inactive_record }
                        ]));
                    }
                    if (this.model == 'art.production')
                    {
                        this.sidebar.add_items('other', _.compact([
                                this.is_action_enabled('force_out') && { label: _t('Force Out'), callback: this.on_button_force_out }
                            ]));
                    }
                }
                else{
                    this.sidebar.add_items('other', _.compact([
                        this.is_action_enabled('delete') && { label: _t('Delete'), callback: this.on_button_delete },
                        this.is_action_enabled('create') && { label: _t('Duplicate'), callback: this.on_button_duplicate },
                    ]));
                }

                this.sidebar.appendTo($node);

                // Show or hide the sidebar according to the view mode
                this.toggle_sidebar();
            }
        },

        _process_save: function(save_obj) {
            var self = this;
            var prepend_on_create = save_obj.prepend_on_create;
            var def_process_save = $.Deferred();
            try {
                var form_invalid = false,
                    values = {},
                    first_invalid_field = null,
                    readonly_values = {},
                    deferred = [];

                $.when.apply($, deferred).always(function () {

                    _.each(self.fields, function (f) {
                        if (!f.is_valid()) {
                            form_invalid = true;
                            if (!first_invalid_field) {
                                first_invalid_field = f;
                            }
                        } else if (f.name !== 'id' && (!self.datarecord.id || f._dirty_flag)) {
                            // Special case 'id' field, do not save this field
                            // on 'create' : save all non readonly fields
                            // on 'edit' : save non readonly modified fields
                            if (!f.get("readonly")) {
                                values[f.name] = f.get_value(true);
                            } else {
                                readonly_values[f.name] = f.get_value(true);
                            }
                        }

                    });

                    // Heuristic to assign a proper sequence number for new records that
                    // are added in a dataset containing other lines with existing sequence numbers
                    if (!self.datarecord.id && self.fields.sequence &&
                        !_.has(values, 'sequence') && !_.isEmpty(self.dataset.cache)) {
                        // Find current max or min sequence (editable top/bottom)
                        var current = _[prepend_on_create ? "min" : "max"](
                            _.map(self.dataset.cache, function(o){return o.values.sequence})
                        );
                        values['sequence'] = prepend_on_create ? current - 1 : current + 1;
                    }
                    if (form_invalid) {
                        self.set({'display_invalid_fields': true});
                        first_invalid_field.focus();
                        self.on_invalid();
                        def_process_save.reject();
                    } else {
                        self.set({'display_invalid_fields': false});
                        var save_deferral;
                        if (!self.datarecord.id) {
                            // Creation save
                            var show_confirmation_popup_for_change_to_qty = false;
                            var show_confirmation_popup_for_change_to_variant = false;
                            var show_confirmation_popup_for_change_to_decoration = false;

                            if ((self.model == 'sale.order.edit.decoration' || self.model == 'sale.order.edit.multi.decoration') && 'sale_order_edit_decoration_ids' in values)
                            {
                                var sale_order_edit_decoration_ids = values['sale_order_edit_decoration_ids'];
                                if (sale_order_edit_decoration_ids.length > 0)
                                {
                                    sale_order_edit_decoration_ids.forEach(function(sale_order_edit_decoration_id) {
                                        if (sale_order_edit_decoration_id[0] == 0 && 'has_any_decoration_charge_or_any_shipping_line' in sale_order_edit_decoration_id[2])
                                        {
                                            var has_any_decoration_charge_or_any_shipping_line = sale_order_edit_decoration_id[2]['has_any_decoration_charge_or_any_shipping_line'];
                                            if (has_any_decoration_charge_or_any_shipping_line == true)
                                            {
                                                if ('pms_code' in sale_order_edit_decoration_id[2] && 'copy_pms_code' in sale_order_edit_decoration_id[2])
                                                {
                                                    var decoration_pms_code = [];
                                                    var wizard_pms_code = [];
                                                    if (sale_order_edit_decoration_id[2]['pms_code'] != false)
                                                    {
                                                        decoration_pms_code = sale_order_edit_decoration_id[2]['pms_code'][0][2];
                                                    }
                                                    if (sale_order_edit_decoration_id[2]['copy_pms_code'] != false)
                                                    {
                                                        wizard_pms_code = sale_order_edit_decoration_id[2]['copy_pms_code'][0][2];
                                                    }
                                                    if (decoration_pms_code.length != wizard_pms_code.length)
                                                    {
                                                        show_confirmation_popup_for_change_to_decoration = true;
                                                    }
                                                    else
                                                    {
                                                        decoration_pms_code.forEach(function(pms_code) {
                                                            var pms_code_exist = false
                                                            for (var i = 0, len = wizard_pms_code.length; i < len; i++){
                                                                if (wizard_pms_code[i] == pms_code){
                                                                    pms_code_exist = true
                                                                    break
                                                                }
                                                            }
                                                            if (pms_code_exist == false)
                                                            {
                                                                show_confirmation_popup_for_change_to_decoration = true
                                                            }
                                                        });
                                                    }
                                                }
                                                if ('stock_color' in sale_order_edit_decoration_id[2] && 'copy_stock_color' in sale_order_edit_decoration_id[2])
                                                {
                                                    var decoration_stock_color = [];
                                                    var wizard_stock_color = [];
                                                    if (sale_order_edit_decoration_id[2]['stock_color'] != false)
                                                    {
                                                        decoration_stock_color = sale_order_edit_decoration_id[2]['stock_color'][0][2];
                                                    }
                                                    if (sale_order_edit_decoration_id[2]['copy_stock_color'] != false)
                                                    {
                                                        wizard_stock_color = sale_order_edit_decoration_id[2]['copy_stock_color'][0][2];
                                                    }
                                                    if (decoration_stock_color.length != wizard_stock_color.length)
                                                    {
                                                        show_confirmation_popup_for_change_to_decoration = true;
                                                    }
                                                    else
                                                    {
                                                        decoration_stock_color.forEach(function(stock_color) {
                                                            var stock_color_exist = false
                                                            for (var i = 0, len = wizard_stock_color.length; i < len; i++){
                                                                if (wizard_stock_color[i] == stock_color){
                                                                    stock_color_exist = true
                                                                    break
                                                                }
                                                            }
                                                            if (stock_color_exist == false)
                                                            {
                                                                show_confirmation_popup_for_change_to_decoration = true
                                                            } 
                                                        });
                                                    }
                                                }
                                            }
                                        }
                                    });
                                } 
                            }

                            if (self.model == 'sale.order.wizard.change' && 'sale_order_lines' in values)
                            {
                                var sale_order_lines = values['sale_order_lines'];
                                if (sale_order_lines.length > 0)
                                {
                                    sale_order_lines.forEach(function(sale_order_line) {
                                        if (sale_order_line[0] == 0 && 'change_qty' in sale_order_line[2] && 'product_uom_qty' in sale_order_line[2] && 'has_any_charge' in sale_order_line[2])
                                        {
                                            var sale_order_line_product_uom_qty = sale_order_line[2]['product_uom_qty'];
                                            var wizard_change_qty = sale_order_line[2]['change_qty'];
                                            var has_any_charge = sale_order_line[2]['has_any_charge'];
                                            if (sale_order_line_product_uom_qty != wizard_change_qty && has_any_charge == true)
                                            {
                                                show_confirmation_popup_for_change_to_qty = true;
                                            }
                                        }
                                        if (sale_order_line[0] == 0 && 'change_variant' in sale_order_line[2] && 'product_id' in sale_order_line[2] && 'has_any_decoration' in sale_order_line[2])
                                        {
                                            var sale_order_line_product_id = sale_order_line[2]['product_id'];
                                            var wizard_change_variant = sale_order_line[2]['change_variant'];
                                            var has_any_decoration = sale_order_line[2]['has_any_decoration'];
                                            if (sale_order_line_product_id != wizard_change_variant && has_any_decoration == true)
                                            {
                                                show_confirmation_popup_for_change_to_variant = true;
                                            }
                                        }
                                    });
                                }
                            }
                            if (show_confirmation_popup_for_change_to_qty || show_confirmation_popup_for_change_to_variant || show_confirmation_popup_for_change_to_decoration)
                            {                                
                                var message = "This change will impact the Decoration Group Charges and Shipping Charges you've applied to this order. Would you like for this change to clear them out ?"
                                if (show_confirmation_popup_for_change_to_variant == true)
                                {
                                    message = "This change will impact the Decorations, Decoration Groups, Decoration Group Charges, Shipping Lines and Shipping Charges you've applied to this order. Would you like for this change to clear them out ?"
                                }
                                if (show_confirmation_popup_for_change_to_decoration == true)
                                {
                                    message = "This change will impact the Decoration Groups, Decoration Group Charges, Shipping Lines and Shipping Charges you've applied to this order. Would you like for this change to clear them out ?"
                                }
                                var options = {
                                    title: _t("Confirmation Popup"),
                                    confirm_callback: function() {
                                        if (show_confirmation_popup_for_change_to_qty == true)
                                        {
                                            values['user_response_about_change_to_qty'] = true
                                        }
                                        if (show_confirmation_popup_for_change_to_variant == true)
                                        {
                                            values['user_response_about_change_to_variant'] = true
                                        }
                                        if (show_confirmation_popup_for_change_to_decoration)
                                        {
                                            values['user_response_about_change_to_decoration'] = true  
                                            values['any_change_to_decoration_in_pms_code_or_stock_color'] = true 
                                        }
                                        save_deferral = self.dataset.create(values, {readonly_fields: readonly_values}).then(function(r) {
                                            self.display_translation_alert(values);
                                            return self.record_created(r, prepend_on_create);
                                        }, null);
                                        save_deferral.then(function(result) {
                                            def_process_save.resolve(result);
                                        }).fail(function() {
                                            def_process_save.reject();
                                        });
                                        return def_process_save;
                                    },
                                    cancel_callback: function() {
                                        if (show_confirmation_popup_for_change_to_decoration)
                                        {
                                            values['any_change_to_decoration_in_pms_code_or_stock_color'] = true 
                                        }
                                        save_deferral = self.dataset.create(values, {readonly_fields: readonly_values}).then(function(r) {
                                            self.display_translation_alert(values);
                                            return self.record_created(r, prepend_on_create);
                                        }, null);
                                        save_deferral.then(function(result) {
                                            def_process_save.resolve(result);
                                        }).fail(function() {
                                            def_process_save.reject();
                                        });
                                        return def_process_save;
                                    },
                                };
                                var dialog = Dialog.confirmation_popup(self, message, options);
                                dialog.$modal.on('hidden.bs.modal', function() {
                                    return def_process_save.reject();
                                });
                            }
                            else
                            {
                                save_deferral = self.dataset.create(values, {readonly_fields: readonly_values}).then(function(r) {
                                    self.display_translation_alert(values);
                                    return self.record_created(r, prepend_on_create);
                                }, null);
                                save_deferral.then(function(result) {
                                    def_process_save.resolve(result);
                                }).fail(function() {
                                    def_process_save.reject();
                                });
                            }

                        } else if (_.isEmpty(values)) {
                            // Not dirty, noop save
                            save_deferral = $.Deferred().resolve({}).promise();
                            save_deferral.then(function(result) {
                                def_process_save.resolve(result);
                            }).fail(function() {
                                def_process_save.reject();
                            });
                        } else {
                            // Write save
                            var show_confirmation_popup_for_change_to_qty = false;

                            if (self.model == 'sale.order' && 'order_line' in values)
                            {
                                var order_lines = values['order_line'];
                                if (order_lines.length > 0)
                                {
                                    order_lines.forEach(function(order_line) {
                                        if (order_line[0] == 1 && 'product_uom_qty' in order_line[2] && 'has_any_charge' in order_line[2])
                                        {
                                            show_confirmation_popup_for_change_to_qty = true;
                                        }
                                    });
                                }
                            }

                            if (show_confirmation_popup_for_change_to_qty)
                            {
                                var message = "This change will impact the Decoration Group Charges, Shipping Charges you've applied to this order. Would you like for this change to clear them out ?"
                                var options = {
                                    title: _t("Confirmation Popup"),
                                    confirm_callback: function() {
                                        save_deferral = self.dataset.write(self.datarecord.id, values, {readonly_fields: readonly_values, context: {'user_response_about_change_to_qty': true}}).then(function(r) {
                                            self.display_translation_alert(values);
                                            return self.record_saved(r);
                                        }, null);
                                        save_deferral.then(function(result) {
                                            def_process_save.resolve(result);
                                        }).fail(function() {
                                            def_process_save.reject();
                                        });
                                        return def_process_save;
                                    },
                                    cancel_callback: function() {
                                        save_deferral = self.dataset.write(self.datarecord.id, values, {readonly_fields: readonly_values}).then(function(r) {
                                            self.display_translation_alert(values);
                                            return self.record_saved(r);
                                        }, null);
                                        save_deferral.then(function(result) {
                                            def_process_save.resolve(result);
                                        }).fail(function() {
                                            def_process_save.reject();
                                        });
                                        return def_process_save;
                                    },
                                };
                                var dialog = Dialog.confirmation_popup(self, message, options);
                                dialog.$modal.on('hidden.bs.modal', function() {
                                    return def_process_save.reject();
                                });
                            }
                            else
                            {
                                save_deferral = self.dataset.write(self.datarecord.id, values, {readonly_fields: readonly_values}).then(function(r) {
                                    self.display_translation_alert(values);
                                    return self.record_saved(r);
                                }, null);
                                save_deferral.then(function(result) {
                                    def_process_save.resolve(result);
                                }).fail(function() {
                                    def_process_save.reject();
                                });
                            }
                        }
                    }
                });
            } catch (e) {
                console.error(e);
                return def_process_save.reject();
            }
            return def_process_save;
        },
    });
    

    Dialog.include({
        open: function() {
            self = this._super.apply(this, arguments);
            if (this.res_model == 'sale.order.edit.decoration.line' || this.res_model == 'sale.order.edit.multi.decoration.line' || this.res_model == 'sale.order.add.decoration.line')
            {
                this.$modal.find("button:contains('Save & New')").remove()
            }
            return self;
        },
    });
    
    var One2ManySelectable = FieldOne2Many.extend(
    {
        multi_selection: true,
        events: {
        "change .o_list_record_selector": "checkbox_clicked", 
        // "focus .o_list_view": "checkbox_focus", 
        // "blur .o_list_record_selector": "checkbox_blur", 
        },

        // checkbox_blur: function() {
        //     var ids =[];
        //     this.$(".o_list_record_selector input:checked").closest('tr').each(function () {
        //             ids.push(parseInt($(this).context.dataset.id));
        //     });
           
        //     var selected_ids = []
        //     for (var id in ids) {
        //         if (!isNaN(ids[id]))
        //         {
        //             selected_ids.push(ids[id])
        //         }
        //     }

        //     var model = new Model(this.field.relation);
        //     if (selected_ids.length > 0)
        //     {
        //         model.call("write", [selected_ids, {'select':false}] ,{"context": this.build_context()});
        //     }

        //     this.$(".o_list_record_selector input:checked").attr("checked", false);
        // },

        // checkbox_focus: function() {
            
        //     var ids =[];
        //     this.$(".o_list_record_selector input:checked").closest('tr').each(function () {
        //             ids.push(parseInt($(this).context.dataset.id));
        //     });
           
        //     var selected_ids = []
        //     for (var id in ids) {
        //         if (!isNaN(ids[id]))
        //         {
        //             selected_ids.push(ids[id])
        //         }
        //     }

        //     var model = new Model(this.field.relation);
        //     if (selected_ids.length > 0)
        //     {
        //         model.call("write", [selected_ids, {'select':false}] ,{"context": this.build_context()});
        //     }

        //     this.$(".o_list_record_selector input:checked").attr("checked", false);
        // },

        checkbox_clicked: function() {
           var ids =[];
           this.$(".o_list_record_selector input:checked").closest('tr').each(function () {
                    ids.push(parseInt($(this).context.dataset.id));
           });
           var selected_ids = []
           for (var id in ids) {
                if (!isNaN(ids[id]))
                {
                    selected_ids.push(ids[id])
                }
            }
           var model = new Model(this.field.relation);
           if (this.name != 'decoration_line' && this.name != 'art_line' && this.name != 'art_lines')
           {
                if (selected_ids.length > 0)
                {
                    model.call("write", [selected_ids, {'select':true}] ,{"context": this.build_context()});
                }
           }
           else
           {
                if (selected_ids.length > 0)
                {
                    if (this.name == 'decoration_line')
                    {
                        model.call("write", [selected_ids, {'select_decoration_line':true}] ,{"context": this.build_context()});   
                    }
                    else
                    {
                        model.call("write", [selected_ids, {'select_art_line':true}] ,{"context": this.build_context()});   
                    }
                }
           }

            var ids =[];
            this.$(".o_list_record_selector input:checkbox:not(:checked)").closest('tr').each(function () {
                    ids.push(parseInt($(this).context.dataset.id));
            });
           
            var unselected_ids = []
            for (var id in ids) {
                if (!isNaN(ids[id]))
                {
                    unselected_ids.push(ids[id])
                }
            }
            if (selected_ids.length > 0) {
                if (this.name == 'order_line'){
                    $('.split-so-line').css('display', 'block');
                }
            }
            else {
                $('.split-so-line').css('display', 'none');
            }

            if (this.name != 'decoration_line' && this.name != 'art_line' && this.name != 'art_lines')
            {
                if (unselected_ids.length > 0)
                {
                    model.call("write", [unselected_ids, {'select':false}] ,{"context": this.build_context()});
                }
            }
            else
            {
                if (unselected_ids.length > 0)
                {
                    if (this.name == 'decoration_line')
                    {
                        model.call("write", [unselected_ids, {'select_decoration_line':false}] ,{"context": this.build_context()});   
                    }
                    else
                    {
                        model.call("write", [unselected_ids, {'select_art_line':false}] ,{"context": this.build_context()});   
                    }
                }
            }
        },
    });    

    core.form_widget_registry
    .add('one2many_selectable', One2ManySelectable);

    var FieldUrlLink = FieldUrl.extend({
        render_value: function() {
            this._super();
            if(this.get("effective_readonly")) {
                var tmp = this.get('value');
                var url = cfs_url+tmp+ '?dl=1'
//                var s = /(\w+):(.+)|^\.{0,2}\//.exec(tmp);
//                if (!s) {
//                    tmp = "http://" + this.get('value');
//                }
                var text = this.get('value') ? this.node.attrs.text || tmp : '';
                var filename_fieldname = this.node.attrs.filename;
                var filename_field = this.view.fields && this.view.fields[filename_fieldname];
                var filename_val = filename_field ? filename_field.get('value') : null;
                this.$el.attr('href', url).text('Download ' + filename_val);

                if(this.node.attrs.name == 'vendor_link'){
                    console.log(this.node);
                    this.$el.attr('href', tmp).text('Get Inventory');
                }
                if(this.node.attrs.name == 'vendor_product_link'){
                    console.log(this.node);
                    this.$el.attr('href', tmp).text('Link');
                }
            }
        }

    });

    core.form_widget_registry.add('link', FieldUrlLink);


    var ColumnUrlLink = list_widget_registry.get('field.url').extend({
        /**
         * Regex checking if a URL has a scheme
         */

        PROTOCOL_REGEX: /^(?!\w+:?\/\/)/,

        /**
         * Format a column as a URL if the column has content.
         * Add "//" (inherit current protocol) specified in
         * RFC 1808, 2396, and 3986 if no other protocol is included.
         *
         * @param row_data record whose values should be displayed in the cell
         * @param options
         */
        _format: function(row_data, options) {
            var value = row_data[this.id].value;
            if (! row_data['web_link'])
                var filename_val = row_data[this.filename].value
            if (value && filename_val) {
                if (value.slice(-5) == '?dl=1')
                    value = value.slice(0, -5)
                var url = cfs_url+value
                return _.template("<center>\
                                        <a href='<%-href%>' target='_blank' title='<%-text%>'>\
                                            <img src='<%-href%>?h=90' height=90></img>\
                                        </a><br/>\
                                        <a href='<%-href%>?dl=1' target='_blank'>Download</a>\
                                   </center>"
                                 )
                ({
                    href: url.trim().replace(this.PROTOCOL_REGEX, '//'),
                    text: filename_val,
                });
            }
            if (value && row_data['web_link']) {
                return _.template("<a href='<%-href%>' target='_blank'><%-text%></a>")({
                    href: value.trim().replace(this.PROTOCOL_REGEX, '//'),
                    text: 'Link',
                });
            }
            return this._super(row_data, options);
        }
    });

    list_widget_registry.add('field.link', ColumnUrlLink);

    var ColumnCharExtended = ColumnChar.extend({
        _format: function (row_data, options) {
            var value = row_data[this.id].value;
            if (value && value.length >= 25)
                return(value.substring(0, 25) + "...");
            return this._super(row_data, options);
        }
    });
    list_widget_registry.add('field.char_extended', ColumnCharExtended);

    var ChatThread = require('mail.ChatThread');

    ChatThread.include({
        update_timestamps: function(){
        }
    })

    var FieldMany2ManyTags = FieldMany2ManyTags.include({
        open_color_picker: function(ev){
        if (this.fields.color &&  (this.field.__attrs.name != 'partner_ids' && this.field.__attrs.name != 'cc_partner_ids' && this.field.__attrs.name != 'bcc_partner_ids' && this.field.__attrs.name != 'aam_id')) {
            this.$color_picker = $(QWeb.render('FieldMany2ManyTag.colorpicker', {
                'widget': this,
                'tag_id': $(ev.currentTarget).data('id'),
            }));

            $(ev.currentTarget).append(this.$color_picker);
            this.$color_picker.dropdown('toggle');
            this.$color_picker.attr("tabindex", 1).focus();
        }
    },
    });

    FieldBinaryFile.include({
        render_value: function() {
            var filename = this.view.datarecord[this.node.attrs.filename];
            if (this.get("effective_readonly")) {
                this.do_toggle(!!this.get('value'));
                if (this.get('value')) {
                    this.$el.empty().append($("<span/>").addClass('fa fa-download'));
                    if (filename) {
                        this.$el.append(" " + filename);
                    }
                }
            } else {
                if(this.get('value')) {
                    this.$el.children().removeClass('o_hidden');
                    this.$('.o_select_file_button').first().addClass('o_hidden');
                    var loaded_filename = this.node.attrs.filename;
                    var loaded_filename_value = '';
                    if (loaded_filename)
                    {
                        loaded_filename_value = this.field_manager.fields[loaded_filename].get_value();
                    }
                    this.$input.val(loaded_filename_value || filename || this.get('value'));
                } else {
                    this.$el.children().addClass('o_hidden');
                    this.$('.o_select_file_button').first().removeClass('o_hidden');
                }
            }
        }
    });

    var DateWithPassedDaysWidget = DateWidget.extend({
        init: function(parent, options) {
            this._super.apply(this, arguments);

            var l10n = _t.database.parameters;

            this.name = parent.name;
            this.options = _.defaults(options || {}, {
                pickTime: this.type_of_date === 'datetime',
                useSeconds: this.type_of_date === 'datetime',
                startDate: moment({ y: 1900 }),
                endDate: moment().add(200, "y"),
                calendarWeeks: true,
                beforeShowDay: function(date) 
                            {
                                var todaydate = new Date();
                                if (date < todaydate) {
                                    return [true, ' passedoutday', ''];
                                }
                                return [true, '', ''];
                            },
                icons: {
                    time: 'fa fa-clock-o',
                    date: 'fa fa-calendar',
                    up: 'fa fa-chevron-up',
                    down: 'fa fa-chevron-down'
                },
                language : moment.locale(),
                format : time.strftime_to_moment_format((this.type_of_date === 'datetime')? (l10n.date_format + ' ' + l10n.time_format) : l10n.date_format),
            });
    },
    });

    var FieldDateWithPassedDays = FieldDate.extend({
        build_widget: function() {
            return new DateWithPassedDaysWidget(this);
        },
    });

    core.form_widget_registry
        .add('date_with_passed_days', FieldDateWithPassedDays);
});