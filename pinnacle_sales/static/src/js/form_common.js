odoo.define('pinnacle_sales.form_common_change', function (require) {
"use strict";
    
var core = require('web.core');
var data = require('web.data');
var common = require('web.form_common');
var _t = core._t;

var FieldMany2One = core.form_widget_registry.get('many2one');
var FieldBinaryFile = core.form_widget_registry.get('binary');
var ListView = require('web.ListView');
var SearchView = require('web.SearchView');

FieldMany2One.include({
    //define in AbstractField class of odoo still fetching issue of this method.
    //need to check why
    is_set: function() {
        return !this.is_false();
    },

    display_string: function (str) {
        var noValue = (str === null);
        if (!this.get("effective_readonly")) {
            this.$input.val(noValue ? "" : (str.split("\n")[0].trim() || $(data.noDisplayContent).text()));
            this.current_display = this.$input.val();
            this.$follow_button.toggle(this.is_set());
            this.$el.toggleClass('o_with_button', !!this.$follow_button && this.$follow_button.length > 0 && this.is_set());
        } else {
            this.$el.html(noValue ? "" : (_.escape(str.trim()).split("\n").join("<br/>") || data.noDisplayContent));
            if (this.get_value()){
                $(this.$el).attr('href', _.str.sprintf("#id=%s&view_type=form&model=%s", this.get_value(), this.field.relation))
            }
            // Define callback to perform when clicking on the field
            if (!this.options.no_open) {
                // Remove potential previously added event handler
                this.$el.off('click');
                // Ensure that the callback is performed only once even with multiple clicks
                var execute_formview_action_once = _.once(this.execute_formview_action.bind(this));
                this.$el.click(function (ev) {
                    ev.preventDefault();
                    execute_formview_action_once();
                });
            }
        }
    },

    get_search_result: function(search_val) {
        var self = this;
        var dataset = new data.DataSet(this, this.field.relation, self.build_context());
        this.last_query = search_val;
        var exclusion_domain = [], ids_blacklist = this.get_search_blacklist();
        if (!_(ids_blacklist).isEmpty()) {
            exclusion_domain.push(['id', 'not in', ids_blacklist]);
        }

        return this.orderer.add(dataset.name_search(
                search_val, new data.CompoundDomain(self.build_domain(), exclusion_domain),
                'ilike', this.limit + 1, self.build_context())).then(function(_data) {
            self.last_search = _data;
            // possible selections for the m2o
            var values = _.map(_data, function(x) {
                x[1] = x[1].split("\n")[0];
                return {
                    label: _.str.escapeHTML(x[1].trim()) || data.noDisplayContent,
                    value: x[1],
                    name: x[1],
                    id: x[0],
                };
            });
            
            // search more... if more results that max
            if (values.length > self.limit) {
                values = values.slice(0, self.limit);
                values.push({
                    label: _t("Search More..."),
                    action: function() {
                        dataset.name_search(search_val, self.build_domain(), 'ilike', 1000000).done(function(_data) {
                            self._search_create_popup("search", _data);
                        });
                    },
                    classname: 'o_m2o_dropdown_option'
                });
            }
            // quick create
            var raw_result = _(_data.result).map(function(x) {return x[1];});
            if (search_val.length > 0 && !_.include(raw_result, search_val) &&
                ! (self.options && (self.options.no_create || self.options.no_quick_create))) {
                self.can_create && values.push({
                    label: _.str.sprintf(_t('Create "<strong>%s</strong>"'),
                        $('<span />').text(search_val).html()),
                    action: function() {
                        self._quick_create(search_val);
                    },
                    classname: 'o_m2o_dropdown_option'
                });
            }
            
            var show_create_and_edit_when_global_search_enabled = self.is_the_global_search_enabled()

            // create... 
            if ((!(self.options && (self.options.no_create || self.options.no_create_edit)) && self.can_create) || show_create_and_edit_when_global_search_enabled){
                values.push({
                    label: _t("Create and Edit..."),
                    action: function() {
                        self._search_create_popup("form", undefined, self._create_context(search_val));
                    },
                    classname: 'o_m2o_dropdown_option'
                });
            }
            else if (values.length === 0) {
                values.push({
                    label: _t("No results to show..."),
                    action: function() {},
                    classname: 'o_m2o_dropdown_option'
                });
            }

            return values;
        });
    },

    is_the_global_search_enabled: function(){
        var context = this.build_context().eval();
        var show_create_and_edit_when_global_search_enabled = false;
        if (context.hasOwnProperty('is_the_global_search_enabled'))
        {
            if (context['is_the_global_search_enabled'] )
            {
                if (context.hasOwnProperty('imprint_location'))
                {
                    if (context['imprint_location'] )
                    {
                        show_create_and_edit_when_global_search_enabled = true;
                    }
                }
                if (context.hasOwnProperty('imprint_method'))
                {
                    if (context['imprint_method'] )
                    {
                        show_create_and_edit_when_global_search_enabled = true;
                    }
                }
                if (context.hasOwnProperty('vendor_decoration_location'))
                {
                    if (context['vendor_decoration_location'] )
                    {
                        show_create_and_edit_when_global_search_enabled = true;
                    }
                }
                if (context.hasOwnProperty('vendor_decoration_method'))
                {
                    if (context['vendor_decoration_method'] )
                    {
                        show_create_and_edit_when_global_search_enabled = true;
                    }
                }
            }
        }
        return show_create_and_edit_when_global_search_enabled
    },

});
core.view_registry.add('form_common_change', FieldMany2One);

var SelectCreateListView = ListView.extend({
    do_add_record: function () {
        this.popup.create_edit_record();
    },
    select_record: function(index) {
        this.popup.on_selected([this.dataset.ids[index]]);
        this.popup.close();
    },
    do_select: function(ids, records) {
        this._super.apply(this, arguments);
        this.popup.on_click_element(ids);
    }
});


common.SelectCreateDialog.include({

    setup: function(search_defaults, fields_views) {
        var self = this;
        if (this.searchview) {
            this.searchview.destroy();
        }
        var fragment = document.createDocumentFragment();
        var $header = $('<div/>').addClass('o_modal_header').appendTo(fragment);
        var $pager = $('<div/>').addClass('o_pager').appendTo($header);
        var options = {
            $buttons: $('<div/>').addClass('o_search_options').appendTo($header),
            search_defaults: search_defaults,
        };

        this.searchview = new SearchView(this, this.dataset, fields_views.search, options);
        this.searchview.on('search_data', this, function(domains, contexts, groupbys) {
            if (this.initial_ids) {
                this.do_search(domains.concat([[["id", "in", this.initial_ids]], this.domain]),
                    contexts.concat(this.context), groupbys);
                this.initial_ids = undefined;
            } else {
                this.do_search(domains.concat([this.domain]), contexts.concat(this.context), groupbys);
            }
        });
        return this.searchview.prependTo($header).then(function() {
            self.searchview.toggle_visibility(true);

            self.view_list = new SelectCreateListView(self,
                self.dataset, fields_views.list,
                _.extend({'deletable': false,
                    'selectable': !self.options.disable_multiple_selection,
                    'import_enabled': false,
                    '$buttons': self.$buttons,
                    'disable_editable_mode': true,
                    'pager': true,
                }, self.options.list_view_options || {}));
            self.view_list.on('edit:before', self, function (e) {
                e.cancel = true;
            });
            self.view_list.popup = self;
            self.view_list.on('list_view_loaded', self, function() {
                this.on_view_list_loaded();
            });

            var buttons = [
                {text: _t("Cancel"), classes: "btn-default o_form_button_cancel", close: true}
            ];

            var show_create_and_edit_when_global_search_enabled = self.is_the_global_search_enabled()

            if((!self.options.no_create) || show_create_and_edit_when_global_search_enabled) {
                buttons.splice(0, 0, {text: _t("Create"), classes: "btn-primary", click: function() {
                    self.create_edit_record();
                }});
            }
            if(!self.options.disable_multiple_selection) {
                buttons.splice(0, 0, {text: _t("Select"), classes: "btn-primary o_selectcreatepopup_search_select", disabled: true, close: true, click: function() {
                    self.on_selected(self.selected_ids);
                }});
            }
            self.set_buttons(buttons);

            return self.view_list.appendTo(fragment).then(function() {
                self.view_list.do_show();
                self.view_list.render_pager($pager);
                if (self.options.initial_facet) {
                    self.searchview.query.reset([self.options.initial_facet], {
                        preventSearch: true,
                    });
                }
                self.searchview.do_search();

                return fragment;
            });
        });
    },

    is_the_global_search_enabled: function(){
        var context = this.context.eval();
        var show_create_and_edit_when_global_search_enabled = false;
        if (context.hasOwnProperty('is_the_global_search_enabled'))
        {
            if (context['is_the_global_search_enabled'] )
            {
                if (context.hasOwnProperty('imprint_location'))
                {
                    if (context['imprint_location'] )
                    {
                        show_create_and_edit_when_global_search_enabled = true;
                    }
                }
                if (context.hasOwnProperty('imprint_method'))
                {
                    if (context['imprint_method'] )
                    {
                        show_create_and_edit_when_global_search_enabled = true;
                    }
                }
            }
        }
        return show_create_and_edit_when_global_search_enabled
    },

});

FieldBinaryFile.include({
    init: function(field_manager, node) {
        this._super(field_manager, node);
        this.events = _.extend(this.events, {
            'dragover .o_select_file_button': function(e) {
                    e.preventDefault();e.stopPropagation();
                    $('.o_select_file_button').height(150).width(150)
            },
            'drop .o_select_file_button': function(e) {
                e.preventDefault();e.stopPropagation();
                e.target = e.originalEvent.dataTransfer
                this.on_file_change(e)
                $('.o_select_file_button').height(18).width(112)
            },
            'dragleave .o_select_file_button': function(e){
                $('.o_select_file_button').height(18).width(112)
            },
        })
    },
});

return FieldMany2One;
});

