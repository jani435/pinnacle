odoo.define('global_search_drc.systray', function (require) {
"use strict";

var core = require('web.core');
var SystrayMenu = require('web.SystrayMenu');
var Widget = require('web.Widget');
var Model = require('web.DataModel');
var ListView = require('web.ListView');
var ControlPanel = require('web.ControlPanel');
var data = require('web.data');
var common = require('web.form_common');
require('web.ListEditor'); // one must be sure that the include of ListView are done (for eg: add start_edition methods)

var Dialog = require('web.Dialog');
var session = require('web.session');
var utils = require('web.utils');
var ViewManager = require('web.ViewManager');

var FieldMany2Many = core.form_widget_registry.get('many2many');

var QWeb = core.qweb;
var _t = core._t;
var COMMANDS = common.commands;

var GlobalSearch = Widget.extend({
    template:'global.search.drc.CommonSearch',

    events: {
        'keydown input.o_menu_search_input': 'on_keydown',
    },

    start: function () {
        this.$input = this.$('input');
        return this._super.apply(this, arguments);
    },

    on_keydown: function(event) {
        switch (event.which) {
            case $.ui.keyCode.ENTER:
                if(this.$input.val() == '')
                {
                    this.$input.blur();
                    this.do_notify(_t('Global Search Warning !'),_t('Please enter at least one character to search.'));
                    return;
                }
                else
                {
                    this.make_global_search();
                }
            default:
                if (!this.$input.is(':focus')) {
                    this.$input.focus();
                }
        }
    },

    make_global_search: function(){
        var self = this;
        var $input = self.$input;
        var search_query = $input.val();
        var search_query_wizard_obj = new Model('search.query.wizard');
        search_query_wizard_obj.call("create", [{'search_query': search_query}]).then(function (record)
            {
                if (record)
                {
                    search_query_wizard_obj.call("perform_search", [[record]]).then(function (result)
                        {
                            if(result)
                            {
                                self.do_action({
                                    type: result['type'],
                                    res_model: result['res_model'],
                                    res_id: result['res_id'],
                                    views: [[false, 'form']],
                                    target: 'main',
                                    context: result['context'],
                                    name: result['name'],
                                    flags: {'form': {'action_buttons': false}, 'sidebar': false, 'pager': false},
                                });
                                self.$input.val('');
                                self.$input.blur();
                            }
                        });
                }
            });
    }

});

GlobalSearch.prototype.sequence = 100;
SystrayMenu.Items.push(GlobalSearch);

var X2ManyList = ListView.List.extend({
    pad_table_to: function (count) {
        if (!this.view.is_action_enabled('create') || this.view.x2m.get('effective_readonly')) {
            this._super(count);
            return;
        }

        this._super(count > 0 ? count - 1 : 0);

        var self = this;
        var columns = _(this.columns).filter(function (column) {
            return column.invisible !== '1';
        }).length;
        if (this.options.selectable) { columns++; }
        if (this.options.deletable) { columns++; }

        var $cell = $('<td>', {
            colspan: columns,
            'class': 'o_form_field_x2many_list_row_add'
        }).append(
            $('<a>', {href: '#'}).text(_t("Add an item"))
                .click(function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    var def;
                    if (self.view.editable()) {
                        // FIXME: there should also be an API for that one
                        if (self.view.editor.form.__blur_timeout) {
                            clearTimeout(self.view.editor.form.__blur_timeout);
                            self.view.editor.form.__blur_timeout = false;
                        }
                        def = self.view.save_edition();
                    }
                    $.when(def).done(self.view.do_add_record.bind(self));
                }));

        var $padding = this.$current.find('tr:not([data-id]):first');
        var $newrow = $('<tr>').append($cell);
        if ($padding.length) {
            $padding.before($newrow);
        } else {
            this.$current.append($newrow);
        }
    },
});

var X2ManyListView = ListView.extend({
    is_valid: function () {
        if (!this.fields_view || !this.editable()){
            return true;
        }
        if (_.isEmpty(this.records.records)){
            return true;
        }
        var fields = this.editor.form.fields;
        var current_values = {};
        _.each(fields, function(field){
            field._inhibit_on_change_flag = true;
            field.__no_rerender = field.no_rerender;
            field.no_rerender = true;
            current_values[field.name] = field.get('value');
        });
        var ids = _.map(this.records.records, function (item) { return item.attributes.id; });
        var cached_records = _.filter(this.dataset.cache, function(item){return _.contains(ids, item.id) && !_.isEmpty(item.values) && !item.to_delete;});
        var valid = _.every(cached_records, function(record){
            _.each(fields, function(field){
                var value = record.values[field.name];
                field._inhibit_on_change_flag = true;
                field.no_rerender = true;
                field.set_value(_.isArray(value) && _.isArray(value[0]) ? [COMMANDS.delete_all()].concat(value) : value);
            });
            return _.every(fields, function(field){
                field.process_modifiers();
                field._check_css_flags();
                return field.is_valid();
            });
        });
        _.each(fields, function(field){
            field.set('value', current_values[field.name], {silent: true});
            field._inhibit_on_change_flag = false;
            field.no_rerender = field.__no_rerender;
        });
        return valid;
    },
    render_pager: function($node, options) {
        options = _.extend(options || {}, {
            single_page_hidden: true,
        });
        this._super($node, options);
    },
    display_nocontent_helper: function () {
        return false;
    },
});

var Many2ManyListViewForListWithForm = X2ManyListView.extend({

    init: function () {
        this._super.apply(this, arguments);
        this.options = _.extend(this.options, {
            ListType: X2ManyList,
        });
        this.on('edit:after', this, this.proxy('_after_edit'));
        this.on('save:before cancel:before', this, this.proxy('_before_unedit'));
    },
    do_add_record: function () {
        var self = this;

        new common.SelectCreateDialog(this, {
            res_model: this.model,
            domain: new data.CompoundDomain(this.x2m.build_domain(), ["!", ["id", "in", this.x2m.dataset.ids]]),
            context: this.x2m.build_context(),
            title: _t("Add: ") + this.x2m.string,
            alternative_form_view: this.x2m.field.views ? this.x2m.field.views.form : undefined,
            no_create: this.x2m.options.no_create || !this.is_action_enabled('create'),
            on_selected: function(element_ids) {
                return self.x2m.data_link_multi(element_ids).then(function() {
                    self.x2m.reload_current_view();
                });
            }
        }).open();
    },
    do_activate_record: function(index, id) {
        var self = this;
        self.do_action({
                type: 'ir.actions.act_window',
                res_model: self.model,
                res_id: id,
                views: [[false, 'form']],
                target: 'current',
                context: self.x2m.build_context(),
            });
    },
    do_button_action: function(name, id, callback) {
        var self = this;
        var _sup = _.bind(this._super, this);
        if (! this.x2m.options.reload_on_button) {
            return _sup(name, id, callback);
        } else {
            return this.x2m.view.save().then(function() {
                return _sup(name, id, function() {
                    self.x2m.view.reload();
                });
            });
        }
    },
    _after_edit: function () {
        this.editor.form.on('blurred', this, this._on_blur_many2many);
    },
    _before_unedit: function () {
        this.editor.form.off('blurred', this, this._on_blur_many2many);
    },
    _on_blur_many2many: function() {
        return this.save_edition().done(function () {
            if (self._dataset_changed) {
                self.dataset.trigger('dataset_changed');
            }
        });
    },

});


var FieldMany2ManyForListWithForm = FieldMany2Many.extend({
    init: function() {
        this._super.apply(this, arguments);
        this.x2many_views = {
            list: Many2ManyListViewForListWithForm,
            kanban: core.view_registry.get('many2many_kanban'),
        };
    },
    start: function() {
        this.$el.addClass('o_form_field_many2many');
        return this._super.apply(this, arguments);
    }
});

core.form_widget_registry
    .add('many2many_list_with_form', FieldMany2ManyForListWithForm);

});
