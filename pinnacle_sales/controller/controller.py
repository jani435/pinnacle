from odoo.http import request
import odoo
from odoo import http

class WebClient(http.Controller):

    @http.route('/web/fetch/quick_action_id', type='json', auth="none")
    def fetch_quick_action_id(self, action_name=None):
        return request.env['ir.model.data'].get_object_reference('pinnacle_sales', 'action_view_product_create_1')[1]