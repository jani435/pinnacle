# -*- coding: utf-8 -*-
from openerp import models, fields, api, _
from openerp.exceptions import UserError


class SaleOrderApproveAndReject(models.TransientModel):
    _name = 'sale.order.approve.and.reject'

    name = fields.Text('Comments')

    @api.multi
    def approve(self):
        if self.env.context.get('active_id', False):
            sale_order = self.env['sale.order'].browse(
                [self.env.context.get('active_id', False)])
            if sale_order:
                if self.env.user in sale_order.approval_level.users:
                    self.env['sale.order.approval.level.history'].create(
                        {'order_id': sale_order.id,
                         'approval_level': sale_order.approval_level.id,
                         'approved_by': self.env.user.id,
                         'status': 'Approved',
                         'comments': self.name})
                    levels = self.env['sale.order.approval.level'].search(
                        ['&', ('lower_limit', '<', sale_order.amount_total),
                            ('approval_order', '>', sale_order.approval_level.
                                approval_order)]).sorted(
                        key=lambda r: r.approval_order)
                    if levels:
                        for level in levels:
                            if levels[0]:
                                sale_order.approval_level = level.id
                                sale_order.state = 'to_approve'
                                mail_template = self.env.ref(
                                    'pinnacle_sales.sale_order_template_' +
                                    'approval_notification')
                                mail_id = mail_template.send_mail(
                                    sale_order.id)
                                sale_order.env['mail.mail'].browse([mail_id]).send()
                                mail_template = sale_order.env.ref(
                                    'pinnacle_sales.' +
                                    'sale_order_template_manager_approve_notification')
                                mail_id = mail_template.send_mail(sale_order.id)
                                sale_order.env['mail.mail'].browse([mail_id]).send()
                                break
                        return
                    else:
                        sale_order.approval_level = False
                        sale_order.state = 'approve'
                        mail_template = sale_order.env.ref(
                            'pinnacle_sales.' +
                            'sale_order_template_manager_approve_notification')
                        mail_id = mail_template.send_mail(sale_order.id)
                        sale_order.env['mail.mail'].browse([mail_id]).send()
                        return True
                else:
                    raise UserError(
                        _('You do not have the authority to approve ' +
                          'this Sale Order.'))

    @api.multi
    def reject(self):
        if self.env.context.get('active_id', False):
            sale_order = self.env['sale.order'].browse(
                [self.env.context.get('active_id', False)])
            if sale_order:
                if self.env.user in sale_order.approval_level.users:
                    self.env['sale.order.approval.level.history'].create(
                        {'order_id': sale_order.id,
                         'approval_level': sale_order.approval_level.id,
                         'approved_by': self.env.user.id,
                         'status': 'Rejected',
                         'comments': self.name})
                    sale_order.approval_level = False
                    sale_order.state = 'reject'
                    mail_template = sale_order.env.ref(
                        'pinnacle_sales.' +
                        'sale_order_template_manager_reject_notification')
                    mail_id = mail_template.send_mail(sale_order.id)
                    sale_order.env['mail.mail'].browse([mail_id]).send()
                    return True
                else:
                    raise UserError(
                        _('You do not have the authority to reject ' +
                          'this Sale Order.'))
