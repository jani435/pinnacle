# -*- coding: utf-8 -*-
from openerp import models, fields, api, _
from openerp.addons.pinnacle_sales.wizard.common import common
from openerp.exceptions import UserError
import base64
import urllib2

class SaleOrderAddDecoration(models.TransientModel):
    _name = 'sale.order.add.decoration'

    name = fields.Char(string='Add Decoration To')
    is_decoration_disable = fields.Boolean(string="Blank Product")
    sale_order_add_decoration_line_ids = fields.One2many(
        'sale.order.add.decoration.line',
        'sale_order_add_decoration_id',
        'Sale Order Add Decoration Lines')

    @api.onchange('is_decoration_disable')
    def _onchange_is_decoration_enable(self):
        if self.is_decoration_disable:
            self.sale_order_add_decoration_line_ids = []

    @api.model
    def default_get(self, fields):
        res = super(SaleOrderAddDecoration, self).default_get(fields)
        name = False
        if self.env.context.get('manual_artworksheet') and self.env.context.get('active_ids', False) and not name:
            for art_line in self.env['art.production.line'].browse(self.env.context.get('active_ids', False)):
                if name:
                    if art_line.product_sku and art_line.description and art_line.product_variant_name:
                        name = name + ' <br/> ' + art_line.product_sku + " - " + art_line.description + " - " + art_line.product_variant_name
                    elif art_line.product_sku and art_line.description and (not art_line.product_variant_name):
                        name = name + ' <br/> ' + art_line.product_sku + " - " + art_line.description
                    elif art_line.product_sku and (not art_line.description) and art_line.product_variant_name:
                        name = name + ' <br/> ' + art_line.product_sku + " - " + art_line.product_variant_name
                    elif (not art_line.product_sku) and art_line.description and art_line.product_variant_name:
                        name = name + ' <br/> ' + art_line.description + " - " + art_line.product_variant_name
                    elif (not art_line.product_sku) and (not art_line.description) and art_line.product_variant_name:
                        name = name + ' <br/> ' + art_line.product_variant_name
                    elif (not art_line.product_sku) and art_line.description and (not art_line.product_variant_name):
                        name = name + ' <br/> ' + art_line.description
                    elif art_line.product_sku and (not art_line.description) and (not art_line.product_variant_name):
                        name = name + ' <br/> ' + art_line.product_sku
                    else:
                        pass
                else:
                    if art_line.product_sku and art_line.description and art_line.product_variant_name:
                        name = art_line.product_sku + " - " + art_line.description + " - " + art_line.product_variant_name
                    elif art_line.product_sku and art_line.description and (not art_line.product_variant_name):
                        name = art_line.product_sku + " - " + art_line.description
                    elif art_line.product_sku and (not art_line.description) and art_line.product_variant_name:
                        name = art_line.product_sku + " - " + art_line.product_variant_name
                    elif (not art_line.product_sku) and art_line.description and art_line.product_variant_name:
                        name = art_line.description + " - " + art_line.product_variant_name
                    elif (not art_line.product_sku) and (not art_line.description) and art_line.product_variant_name:
                        name = art_line.product_variant_name
                    elif (not art_line.product_sku) and art_line.description and (not art_line.product_variant_name):
                        name = art_line.description
                    elif art_line.product_sku and (not art_line.description) and (not art_line.product_variant_name):
                        name = art_line.product_sku
                    else:
                        pass
            res.update({'name': name})
        elif self.env.context.get('active_ids', False) and self.env.context.get('order_id', False) and not name:
            for sale_order_line in self.env['sale.order.line'].browse(self.env.context.get('active_ids', False)):
                if name:
                    if sale_order_line.product_sku and sale_order_line.name and sale_order_line.product_variant_name:
                        name = name + ' <br/> ' + sale_order_line.product_sku + " - " + sale_order_line.name + " - " + sale_order_line.product_variant_name
                    elif sale_order_line.product_sku and sale_order_line.name and (not sale_order_line.product_variant_name):
                        name = name + ' <br/> ' + sale_order_line.product_sku + " - " + sale_order_line.name
                    elif sale_order_line.product_sku and (not sale_order_line.name) and sale_order_line.product_variant_name:
                        name = name + ' <br/> ' + sale_order_line.product_sku + " - " + sale_order_line.product_variant_name
                    elif (not sale_order_line.product_sku) and sale_order_line.name and sale_order_line.product_variant_name:
                        name = name + ' <br/> ' + sale_order_line.name + " - " + sale_order_line.product_variant_name
                    elif (not sale_order_line.product_sku) and (not sale_order_line.name) and sale_order_line.product_variant_name:
                        name = name + ' <br/> ' + sale_order_line.product_variant_name
                    elif (not sale_order_line.product_sku) and sale_order_line.name and (not sale_order_line.product_variant_name):
                        name = name + ' <br/> ' + sale_order_line.name
                    elif sale_order_line.product_sku and (not sale_order_line.name) and (not sale_order_line.product_variant_name):
                        name = name + ' <br/> ' + sale_order_line.product_sku
                    else:
                        pass
                else:
                    if sale_order_line.product_sku and sale_order_line.name and sale_order_line.product_variant_name:
                        name = sale_order_line.product_sku + " - " + sale_order_line.name + " - " + sale_order_line.product_variant_name
                    elif sale_order_line.product_sku and sale_order_line.name and (not sale_order_line.product_variant_name):
                        name = sale_order_line.product_sku + " - " + sale_order_line.name
                    elif sale_order_line.product_sku and (not sale_order_line.name) and sale_order_line.product_variant_name:
                        name = sale_order_line.product_sku + " - " + sale_order_line.product_variant_name
                    elif (not sale_order_line.product_sku) and sale_order_line.name and sale_order_line.product_variant_name:
                        name = sale_order_line.name + " - " + sale_order_line.product_variant_name
                    elif (not sale_order_line.product_sku) and (not sale_order_line.name) and sale_order_line.product_variant_name:
                        name = sale_order_line.product_variant_name
                    elif (not sale_order_line.product_sku) and sale_order_line.name and (not sale_order_line.product_variant_name):
                        name = sale_order_line.name
                    elif sale_order_line.product_sku and (not sale_order_line.name) and (not sale_order_line.product_variant_name):
                        name = sale_order_line.product_sku
                    else:
                        pass
            res.update({'name': name})
        return res

    @api.multi
    def create_sale_order_line_decoration(self):
        if self.env['sale.order'].browse(self.env.context.get('order_id', False)):
            for sale_order_line in self.env['sale.order'].browse(self.env.context.get('order_id', False)).order_line:
                sale_order_line.select_decoration_line = False
        imprint_obj = self.env['product.imprint']
        for sale_order_add_decoration_line_id in self.sale_order_add_decoration_line_ids:
            order_line_index = 0
            while order_line_index < len(self.env['sale.order.line'].browse(self.env.context.get('active_ids', False))):
                if sale_order_add_decoration_line_id.imprint_location and sale_order_add_decoration_line_id.imprint_method and sale_order_add_decoration_line_id.finisher and (self.env['sale.order.line'].browse(self.env.context.get('active_ids', False))[order_line_index]):
                    finisher_decorations = (self.env['sale.order.line'].browse(self.env.context.get('active_ids', False))[order_line_index]).product_id.finisher_ids.search(['&',('product_id','=',(self.env['sale.order.line'].browse(self.env.context.get('active_ids', False))[order_line_index]).product_id.product_tmpl_id.id),('finisher_id','=',sale_order_add_decoration_line_id.finisher.id)])
                    if finisher_decorations:
                        finisher_decoration = finisher_decorations[0]
                        finisher_finishing_types = finisher_decoration.product_imprint_ids.search(['&',('product_finisher_id','=',finisher_decoration.id),'&',('decoration_location','=',sale_order_add_decoration_line_id.imprint_location.id),('decoration_method','=',sale_order_add_decoration_line_id.imprint_method.id)])
                        if finisher_finishing_types:
                            finisher_finishing_type = finisher_finishing_types[0]
                            if (len(sale_order_add_decoration_line_id.stock_color.ids) + len(sale_order_add_decoration_line_id.pms_code.ids)) > finisher_finishing_type.color_limit and finisher_finishing_type.color_limit != 0:
                                product_tmpl_name = str((self.env['sale.order.line'].browse(self.env.context.get('active_ids', False))[order_line_index]).product_id.product_tmpl_id.name)
                                raise UserError(_('You can only enter total '+str(finisher_finishing_type.color_limit)+' colors for \n\n - Product Template : '+ product_tmpl_name +'\n - Finisher : '+sale_order_add_decoration_line_id.finisher.name+'\n - Decoration Location : '+sale_order_add_decoration_line_id.imprint_location.name+ '\n - Decoration Method : '+sale_order_add_decoration_line_id.imprint_method.name ))
                order_line_index += 1
        for sale_order_line in self.env['sale.order.line'].browse(self.env.context.get('active_ids', False)):
            finisher_id = False
            sale_order_line.write({'is_decoration_disable': self.is_decoration_disable})
            for sale_order_add_decoration_line_id in self.sale_order_add_decoration_line_ids:
                point_size = '0'
                imprint_area = '0Wx0Hx0D'
                if sale_order_add_decoration_line_id.imprint_location and sale_order_add_decoration_line_id.imprint_method:
                    domain = [('decoration_method', '=', sale_order_add_decoration_line_id.imprint_method.id),
                              ('decoration_location', '=', sale_order_add_decoration_line_id.imprint_location.id),
                              ('product_finisher_id.finisher_id', '=', sale_order_add_decoration_line_id.finisher and
                               sale_order_add_decoration_line_id.finisher.id or False),
                              ('product_finisher_id.product_id', '=', sale_order_line.product_id.product_tmpl_id.id)]
                    imprint_rec = imprint_obj.search(domain, limit=1)
                    point_size = imprint_rec and imprint_rec.point_size or '0'
                    if imprint_rec:
                        imprint_area = imprint_rec.imprint_area
                self.env['sale.order.line.decoration'].create({
                    'imprint_location':
                    sale_order_add_decoration_line_id.imprint_location and
                    sale_order_add_decoration_line_id.imprint_location.id or
                    False,
                    'imprint_method':
                    sale_order_add_decoration_line_id.imprint_method and
                    sale_order_add_decoration_line_id.imprint_method.id or
                    False,
                    'customer_art':
                    sale_order_add_decoration_line_id.customer_art and
                    sale_order_add_decoration_line_id.customer_art.id or
                    False,
                    'finisher':
                    sale_order_add_decoration_line_id.finisher and
                    sale_order_add_decoration_line_id.finisher.id or
                    False,
                    'pms_code':
                    sale_order_add_decoration_line_id.pms_code.ids and
                    [[6, 0, sale_order_add_decoration_line_id.pms_code.ids]] or
                    False,
                    'stock_color':
                    sale_order_add_decoration_line_id.stock_color.ids and
                    [[6, 0, sale_order_add_decoration_line_id.stock_color.ids]]
                    or
                    False,
                    'vendor_decoration_location':
                    sale_order_add_decoration_line_id.vendor_decoration_location and
                    sale_order_add_decoration_line_id.vendor_decoration_location.id or
                    False,
                    'vendor_decoration_method':
                    sale_order_add_decoration_line_id.vendor_decoration_method and
                    sale_order_add_decoration_line_id.vendor_decoration_method.id or
                    False,
                    'vendor_prod_specific': sale_order_add_decoration_line_id.vendor_prod_specific,
                    'sale_order_line_id': sale_order_line.id,
                    'min_pt_size': point_size,
                    'area' : imprint_area,
                    'pms_color_code': sale_order_add_decoration_line_id.pms_color_code,
                    'stock_color_char': sale_order_add_decoration_line_id.stock_color_char
                })
                finisher_id = sale_order_add_decoration_line_id.finisher and sale_order_add_decoration_line_id.finisher.id or False,
            sale_order_line.finisher_id = finisher_id

    @api.multi
    def create_art_line_decoration(self):
        art_rec = self.env['art.production'].browse(self.env.context.get('art_id', False))
        if art_rec:
            for art_line in self.env['art.production'].browse(self.env.context.get('art_id', False)).art_lines:
                art_line.select_art_line = False
        imprint_obj = self.env['product.imprint']
        for sale_order_add_decoration_line_id in self.sale_order_add_decoration_line_ids:
            order_line_index = 0
            while order_line_index < len(self.env['art.production.line'].browse(self.env.context.get('art_lines', False))):
                if sale_order_add_decoration_line_id.imprint_location and sale_order_add_decoration_line_id.imprint_method and sale_order_add_decoration_line_id.finisher and (self.env['art.production.line'].browse(self.env.context.get('art_lines', False))[order_line_index]):
                    finisher_decorations = (self.env['art.production.line'].browse(self.env.context.get('art_lines', False))[order_line_index]).product_id.finisher_ids.search(['&',('product_id','=',(self.env['art.production.line'].browse(self.env.context.get('art_lines', False))[order_line_index]).product_id.product_tmpl_id.id),('finisher_id','=',sale_order_add_decoration_line_id.finisher.id)])
                    if finisher_decorations:
                        finisher_decoration = finisher_decorations[0]
                        finisher_finishing_types = finisher_decoration.product_imprint_ids.search(['&',('product_finisher_id','=',finisher_decoration.id),'&',('decoration_location','=',sale_order_add_decoration_line_id.imprint_location.id),('decoration_method','=',sale_order_add_decoration_line_id.imprint_method.id)])
                        if finisher_finishing_types:
                            finisher_finishing_type = finisher_finishing_types[0]
                            if (len(sale_order_add_decoration_line_id.stock_color.ids) + len(sale_order_add_decoration_line_id.pms_code.ids)) > finisher_finishing_type.color_limit and finisher_finishing_type.color_limit != 0:
                                product_tmpl_name = str((self.env['sale.order.line'].browse(self.env.context.get('active_ids', False))[order_line_index]).product_id.product_tmpl_id.name)
                                raise UserError(_('You can only enter total '+str(finisher_finishing_type.color_limit)+' colors for \n\n - Product Template : '+ product_tmpl_name +'\n - Finisher : '+sale_order_add_decoration_line_id.finisher.name+'\n - Decoration Location : '+sale_order_add_decoration_line_id.imprint_location.name+ '\n - Decoration Method : '+sale_order_add_decoration_line_id.imprint_method.name ))
                order_line_index += 1
        for art_line in self.env['art.production.line'].browse(self.env.context.get('art_lines', False)):
            # finisher_id = False
            # art_line.write({'is_decoration_disable': self.is_decoration_disable})
            decoration_dict = {}
            for sale_order_add_decoration_line_id in self.sale_order_add_decoration_line_ids:
                finisher = sale_order_add_decoration_line_id.finisher and sale_order_add_decoration_line_id.finisher.id or False
                point_size = '0'
                imprint_area = '0Wx0Hx0D'
                if sale_order_add_decoration_line_id.imprint_location and sale_order_add_decoration_line_id.imprint_method:
                    domain = [('decoration_method', '=', sale_order_add_decoration_line_id.imprint_method.id),
                              ('decoration_location', '=', sale_order_add_decoration_line_id.imprint_location.id),
                              ('product_finisher_id.finisher_id', '=', sale_order_add_decoration_line_id.finisher and
                               sale_order_add_decoration_line_id.finisher.id or False),
                              ('product_finisher_id.product_id', '=', art_line.product_id.product_tmpl_id.id)]
                    imprint_rec = imprint_obj.search(domain, limit=1)
                    point_size = imprint_rec and imprint_rec.point_size or '0'
                    if imprint_rec:
                        imprint_area = imprint_rec.imprint_area
                decoration = self.env['sale.order.line.decoration'].create({
                    'imprint_location':
                    sale_order_add_decoration_line_id.imprint_location and
                    sale_order_add_decoration_line_id.imprint_location.id or
                    False,
                    'imprint_method':
                    sale_order_add_decoration_line_id.imprint_method and
                    sale_order_add_decoration_line_id.imprint_method.id or
                    False,
                    'customer_art':
                    sale_order_add_decoration_line_id.customer_art and
                    sale_order_add_decoration_line_id.customer_art.id or
                    False,
                    'finisher': finisher,
                    'pms_code':
                    sale_order_add_decoration_line_id.pms_code.ids and
                    [[6, 0, sale_order_add_decoration_line_id.pms_code.ids]] or
                    False,
                    'stock_color':
                    sale_order_add_decoration_line_id.stock_color.ids and
                    [[6, 0, sale_order_add_decoration_line_id.stock_color.ids]]
                    or
                    False,
                    'vendor_decoration_location':
                    sale_order_add_decoration_line_id.vendor_decoration_location and
                    sale_order_add_decoration_line_id.vendor_decoration_location.id or
                    False,
                    'vendor_decoration_method':
                    sale_order_add_decoration_line_id.vendor_decoration_method and
                    sale_order_add_decoration_line_id.vendor_decoration_method.id or
                    False,
                    'vendor_prod_specific': sale_order_add_decoration_line_id.vendor_prod_specific,
                    'art_line_id': art_line.id,
                    'art_id': art_line.art_id.id,
                    'min_pt_size': point_size,
                    'area': imprint_area,
                    'pms_color_code': sale_order_add_decoration_line_id.pms_color_code,
                    'stock_color_char': sale_order_add_decoration_line_id.stock_color_char
                })
                art_rec.create_decoration_group(decoration.id)

class SaleOrderAddDecorationLine(models.TransientModel):
    _name = 'sale.order.add.decoration.line'

    @api.multi
    @api.depends('stock_color')
    def _get_pms_color_code(self):
        for this in self:
            pms_color_code = common().compute_pms_color_code(this)
            this.pms_color_code = pms_color_code

    @api.multi
    @api.depends('stock_color')
    def _get_stock_color(self):
        for rec in self:
            stock_color = []
            for stk in rec.stock_color:
                stock_color.append(stk.name)
            rec.stock_color_char = ", ".join(stock_color)

    @api.model
    def _default_finisher(self):
        return common().get_default_value_for_finisher_field(self)

    sale_order_add_decoration_id = fields.Many2one(
        'sale.order.add.decoration',
        string='Sale Order Add Decoration',
        ondelete="cascade")
    sale_order_line_decoration_id = fields.Many2one(
        'sale.order.line.decoration',
        string='Decoration Line Number')
    imprint_location = fields.Many2one(
        'decoration.location', string="Imprint Location")
    imprint_method = fields.Many2one(
        'decoration.method', string="Imprint Method")
    customer_art = fields.Many2one(
        'product.decoration.customer.art', string="Art (file or text)")
    finisher = fields.Many2one(
        'res.partner', string="Finisher", default=_default_finisher)
    pms_code = fields.Many2many(
        'product.decoration.pms.code',
        relation="pms_code_add_decoration_line_rel",
        column1="sale_order_add_decoration_line_id",
        column2="product_decoration_pms_code_id",
        string="PMS Code")
    stock_color = fields.Many2many(
        'product.decoration.stock.color',
        relation="stock_color_add_decoration_line_rel",
        column1="sale_order_add_decoration_line_id",
        column2="product_decoration_stock_color_id",
        string="Stock Color")
    pms_color_code = fields.Char(compute='_get_pms_color_code', store=True)
    stock_color_char = fields.Char(compute='_get_stock_color')
    color_pms_code = fields.Char()
    customer_arts = fields.Many2many(
        'product.decoration.customer.art', compute="_compute_customer_arts")
    global_search = fields.Boolean(
        'All options',
        help='Global Search On Location, Method, Finisher',
        default=False)
    vendor_decoration_location = fields.Many2one('vendor.decoration.location', 'Vendor Imprint Location')
    vendor_decoration_method = fields.Many2one('vendor.decoration.method', 'Vendor Imprint Method')
    vendor_prod_specific = fields.Char('Vendor Method Product Specific')
    image_preview = fields.Binary(
        compute="_compute_image_preview", string='Image Preview')
    preview_text = fields.Html(
        compute="_compute_preview_text", string="Text Preview", store=True)

    @api.one
    @api.depends('customer_art', 'customer_art.name_url')
    def _compute_image_preview(self):
        self.image_preview = False
        if self.customer_art and self.customer_art.name_url:
            cfs_url = str(self.env['ir.config_parameter'].get_param('cfs.web.url'))
            final_url = cfs_url + self.customer_art.name_url
            url = common().url_fix(final_url)
            try:
                self.image_preview = base64.encodestring(urllib2.urlopen(url).read())
            except Exception as e:
                self.image_preview = ''


    @api.one
    @api.depends('customer_art', 'customer_art.text', 'customer_art.font')
    def _compute_preview_text(self):
        if self.customer_art and self.customer_art.font and self.customer_art.text:
            self.preview_text = "<font style=\"font-family: '" + self.customer_art.font.name + "'\"><h2>" + self.customer_art.text + "</h2></font>"

    @api.onchange('customer_art') 
    def _onchange_customer_art(self):
        # if not self.env.context.get('manual_artworksheet'):
        customer_arts = self.customer_arts.ids
        if 'sale_order_add_decoration_line_ids' in self.env.context:
            for sale_order_add_decoration_line_id in self.env.context['sale_order_add_decoration_line_ids']:
                if sale_order_add_decoration_line_id[0] == 0:
                    if sale_order_add_decoration_line_id[2]['customer_art']:
                        customer_arts.append(sale_order_add_decoration_line_id[2]['customer_art'])
                if sale_order_add_decoration_line_id[0] == 4:
                    if self.env['sale.order.add.decoration.line'].browse(sale_order_add_decoration_line_id[1]):
                        if self.env['sale.order.add.decoration.line'].browse(sale_order_add_decoration_line_id[1])[0].customer_art:
                            customer_arts.append(self.env['sale.order.add.decoration.line'].browse(sale_order_add_decoration_line_id[1])[0].customer_art.id)
                        else:
                            customer_arts.append(False)
                if sale_order_add_decoration_line_id[0] == 1:
                    if 'customer_art' in sale_order_add_decoration_line_id[2]:
                        customer_arts.append(sale_order_add_decoration_line_id[2]['customer_art'])
                    else:
                        if self.env['sale.order.add.decoration.line'].browse(sale_order_add_decoration_line_id[1]):
                            if self.env['sale.order.add.decoration.line'].browse(sale_order_add_decoration_line_id[1])[0].customer_art:
                                customer_arts.append(self.env['sale.order.add.decoration.line'].browse(sale_order_add_decoration_line_id[1])[0].customer_art.id)
                            else:
                                customer_arts.append(False)
        item_with_atleat_one_preset_decoration = False
        customer_arts_for_item_with_atleat_one_preset_decoration = []
        item_with_preset_decoration = False
        customer_arts_for_item_with_preset_decoration = []
        if self.env.context.get('sale_order_lines', False):
            for sale_order_line in self.env['sale.order.line'].browse(self.env.context.get('sale_order_lines', False)):
                if sale_order_line.product_id and sale_order_line.product_id.product_tmpl_id and sale_order_line.product_id.product_tmpl_id.present_decoration:
                    item_with_preset_decoration = True
                    customer_arts_for_item_with_preset_decoration += (sale_order_line.product_id.product_tmpl_id.customer_arts.ids)
                    for finisher_id in sale_order_line.product_id.product_tmpl_id.finisher_ids:
                        for product_imprint_id in finisher_id.product_imprint_ids:
                            if product_imprint_id.present_decoration:
                                item_with_atleat_one_preset_decoration = True
                                customer_arts_for_item_with_atleat_one_preset_decoration.append(product_imprint_id.customer_art.id)
        customer_arts = list(set(customer_arts))
        if customer_arts_for_item_with_preset_decoration:
            used_customer_arts_for_item_with_preset_decoration = list(set(customer_arts).intersection(customer_arts_for_item_with_preset_decoration))
            if used_customer_arts_for_item_with_preset_decoration:
                customer_arts = used_customer_arts_for_item_with_preset_decoration
            else:
                customer_arts = customer_arts_for_item_with_preset_decoration
        if item_with_atleat_one_preset_decoration:
            customer_arts = customer_arts_for_item_with_atleat_one_preset_decoration
        return {'domain': {
            'customer_art': [('id', 'in', customer_arts)],
        }}
    
    @api.onchange('imprint_method', 'imprint_location', 'finisher',
                  'global_search', 'customer_art')
    def _onchange_imprint_method(self): 
        if self and not self.finisher and not self.env.context.get('manual_artworksheet', False):
            self.imprint_method = False
            self.imprint_location = False
        imprint_methods, vendor_imprint_methods, imprint_locations, vendor_imprint_locations, finishers, all_finishers_assigned_to_product = common().onchange_imprint_method(self) 
        stock_colors = common().onchange_stock_color(self)
        pms_codes = common().onchange_pms_code(self)
        if 'imprint_method' in self.env.context or 'imprint_location' in self.env.context or 'finisher' in self.env.context and self:
            vendor_decoration_location, vendor_decoration_method, vendor_prod_specific = common().return_vendor_decoration_value(self)
            self.vendor_decoration_method = vendor_decoration_method
            self.vendor_decoration_location = vendor_decoration_location
            self.vendor_prod_specific = vendor_prod_specific
        return {'domain': {
            'imprint_method': ['|', ('id', 'in', imprint_methods), ('id', 'in', vendor_imprint_methods)],
            'imprint_location': ['|', ('id', 'in', imprint_locations), ('id', 'in', vendor_imprint_locations)],
            'finisher': ['|', ('id', 'in', finishers), ('id', 'in', all_finishers_assigned_to_product)],
            'stock_color': [('id', 'in', stock_colors)],
            'pms_code': [('id', 'in', pms_codes)]
        }}

    @api.onchange('imprint_method', 'finisher', 'vendor_decoration_method', 'global_search')
    def _onchange_imprint_method_or_finisher(self):
        if not self.global_search:
            vendor_decoration_methods, vendor_decoration_locations = common().return_vendor_decoration_domain(self)
            return {
                'domain': {
                    'vendor_decoration_method': [('id', 'in', vendor_decoration_methods)],
                    'vendor_decoration_location': [('id', 'in', vendor_decoration_locations)]
                }
            }
        else:
            vendor_decoration_methods = self.env['vendor.decoration.method'].search([]).ids
            vendor_decoration_locations = self.env['vendor.decoration.location'].search([]).ids
            return {
                'domain': {
                    'vendor_decoration_method': [('id', 'in', vendor_decoration_methods)],
                    'vendor_decoration_location': [('id', 'in', vendor_decoration_locations)]
                }
            }

    @api.onchange('sale_order_line_decoration_id')
    def _onchange_sale_order_line_decoration_id(self):
        sale_order_line_decoration_ids = []
        if self.env.context.get('order_id', False) and self.env.context.get('sale_order_lines', False):
            sale_order = self.env['sale.order'].browse(self.env.context.get('order_id', False))
            if sale_order and sale_order.order_line:
                for sale_order_second in sale_order.order_line:
                    sale_order_line_decoration_ids += sale_order_second.decorations.ids
        if self.env.context.get('art_id', False) and self.env.context.get('art_lines', False):
            art_rec = self.env['art.production'].browse(self.env.context.get('art_id', False))
            sale_order_line_decoration_ids += art_rec.decorations.ids
        return {'domain': {
            'sale_order_line_decoration_id':
            [('id', 'in', sale_order_line_decoration_ids)],
        }}

    @api.onchange('sale_order_line_decoration_id')
    def _onchange_sale_order_line_id(self):
        if self.sale_order_line_decoration_id and 'sale_order_line_decoration' in self.env.context:
            if self.env.context.get('sale_order_lines', False) or self.env.context.get('art_lines', False):
                common()._onchange_sale_order_line_id(self)

    @api.multi
    def _compute_customer_arts(self):
        if self.env.context.get('order_id', False):
            sale_order = self.env['sale.order'].browse(
                self.env.context.get('order_id', False))
            if sale_order:
                self.customer_arts = [(6, 0, sale_order.customer_arts.ids)]
        else:
            self.customer_arts = [(6, 0, [])]