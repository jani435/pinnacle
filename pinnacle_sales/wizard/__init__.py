# -*- coding: utf-8 -*-
import feedback_wizard
import sale_order_add_decoration
import product_variant_selection
import product_variant_split
import sale_order_edit_decoration
import sale_order_approve_and_reject
import sale_order_edit_multi_decoration
import sale_order_edit_line
import mail_compose_message
import sale_order_split
import art_upload_file
import art_comment_wiz
import sale_make_invoice_advance
import accounting_rejected_comment
import accounting_approved_comment
import invite
import invoice_options_wizard
import quick_product_create
import art_production_line_manage_vendor_web_link
import notes_when_request_for_approval
import force_state_change_wizard