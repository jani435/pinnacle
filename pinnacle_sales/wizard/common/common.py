# -*- coding: utf-8 -*-
import urllib
import urlparse

class common:
    def onchange_imprint_method(self, selfclass):
        imprint_methods = []
        imprint_locations = []
        vendor_imprint_methods = []
        vendor_imprint_locations = []
        finishers = []
        other_finishers = []
        if selfclass.global_search:
            imprint_methods = selfclass.env['decoration.method'].with_context(imprint_method=True, is_the_global_search_enabled=True).search([]).ids
            imprint_locations = selfclass.env['decoration.location'].with_context(imprint_location=True, is_the_global_search_enabled=True).search([]).ids
            finishers = selfclass.env['res.partner'].search([('is_finisher', '=', True)]).ids
        else:
            if selfclass.env.context.get('art_lines', False):
                lines = selfclass.env['art.production.line'].browse(selfclass.env.context.get('art_lines', False))
                art_finisher = selfclass.env.context.get('vendor_id', False)
                for line in lines:
                    if line.n_product_id:
                        finisher_ids = line.n_product_id.finisher_ids
                    if line.product_id:
                        finisher_ids = line.product_id.finisher_ids
                    for finisher in finisher_ids:
                        if finisher.finisher_id.id != line.art_id.vendor_id.id:
                            continue
                        for product_imprint_id in finisher.product_imprint_ids:
                            if not selfclass.imprint_location and not selfclass.finisher:
                                imprint_methods.append(
                                    product_imprint_id.decoration_method.id)
                            elif selfclass.imprint_location and not selfclass.finisher:
                                if selfclass.imprint_location.id == product_imprint_id.decoration_location.id:
                                    imprint_methods.append(product_imprint_id.decoration_method.id)
                            elif not selfclass.imprint_location and selfclass.finisher:
                                if selfclass.finisher.id == finisher.finisher_id.id:
                                    imprint_methods.append(product_imprint_id.decoration_method.id)
                            else:
                                if selfclass.imprint_location.id == product_imprint_id.decoration_location.id and selfclass.finisher.id == finisher.finisher_id.id:
                                    imprint_methods.append(product_imprint_id.decoration_method.id)
                            if not selfclass.imprint_method and not selfclass.finisher:
                                imprint_locations.append(
                                    product_imprint_id.decoration_location.id)
                            elif selfclass.imprint_method and not selfclass.finisher:
                                if selfclass.imprint_method.id == product_imprint_id.decoration_method.id:
                                    imprint_locations.append(product_imprint_id.decoration_location.id)
                            elif not selfclass.imprint_method and selfclass.finisher:
                                if selfclass.finisher.id == finisher.finisher_id.id:
                                    imprint_locations.append(product_imprint_id.decoration_location.id)
                            else:
                                if selfclass.imprint_method.id == product_imprint_id.decoration_method.id and selfclass.finisher.id == finisher.finisher_id.id:
                                    imprint_locations.append(product_imprint_id.decoration_location.id)
                            if not selfclass.imprint_method and not selfclass.imprint_location:
                                finishers.append(finisher.finisher_id.id)
                            elif selfclass.imprint_method and not selfclass.imprint_location:
                                if selfclass.imprint_method.id == product_imprint_id.decoration_method.id:
                                    finishers.append(finisher.finisher_id.id)
                            elif not selfclass.imprint_method and selfclass.imprint_location:
                                if selfclass.imprint_location.id == product_imprint_id.decoration_location.id:
                                    finishers.append(finisher.finisher_id.id)
                            else:
                                if selfclass.imprint_method.id == product_imprint_id.decoration_method.id and selfclass.imprint_location.id == product_imprint_id.decoration_location.id:
                                    finishers.append(finisher.finisher_id.id)
            if selfclass.env.context.get('sale_order_lines', False):
                lines = selfclass.env['sale.order.line'].browse(selfclass.env.context.get('sale_order_lines', False))
                for line in lines:
                    for finisher in line.product_id.finisher_ids:
                        # 12305 : When edit decoration, not all options show
                        # Start Of More than 1 decoration for variant should use same finisher
                        # same_finisher = True
                        # same_finisher_ids = []
                        # if 'sale_order_add_decoration_line_ids' in selfclass.env.context:
                        #     if len(selfclass.env.context['sale_order_add_decoration_line_ids']) >= 1 and not selfclass.finisher:
                        #         for sale_order_add_decoration_line_id in selfclass.env.context['sale_order_add_decoration_line_ids']:
                        #             if sale_order_add_decoration_line_id[0] == 0:
                        #                 if sale_order_add_decoration_line_id[2]['finisher']:
                        #                     same_finisher_ids.append(sale_order_add_decoration_line_id[2]['finisher'])
                        #             if sale_order_add_decoration_line_id[0] == 4:
                        #                 if selfclass.env['sale.order.add.decoration.line'].browse(sale_order_add_decoration_line_id[1]):
                        #                     if selfclass.env['sale.order.add.decoration.line'].browse(sale_order_add_decoration_line_id[1])[0].finisher:
                        #                         same_finisher_ids.append(selfclass.env['sale.order.add.decoration.line'].browse(sale_order_add_decoration_line_id[1])[0].finisher.id)
                        #                     else:
                        #                         same_finisher_ids.append(False)
                        #             if sale_order_add_decoration_line_id[0] == 1:
                        #                 if 'finisher' in sale_order_add_decoration_line_id[2]:
                        #                     same_finisher_ids.append(sale_order_add_decoration_line_id[2]['finisher'])
                        #                 else:
                        #                     if selfclass.env['sale.order.add.decoration.line'].browse(sale_order_add_decoration_line_id[1]):
                        #                         if selfclass.env['sale.order.add.decoration.line'].browse(sale_order_add_decoration_line_id[1])[0].finisher:
                        #                             same_finisher_ids.append(selfclass.env['sale.order.add.decoration.line'].browse(sale_order_add_decoration_line_id[1])[0].finisher.id)
                        #                         else:
                        #                             same_finisher_ids.append(False)
                        #     if same_finisher_ids:
                        #         for same_finisher_id in same_finisher_ids:
                        #             if same_finisher_id != same_finisher_ids[0]:
                        #                 same_finisher = False
                        #                 break
                        #     else:
                        #         same_finisher = False
                        # if 'sale_order_edit_decoration_ids' in selfclass.env.context:
                        #     same_finisher = True
                        #     if len(selfclass.env.context['sale_order_edit_decoration_ids']) >= 1 and not selfclass.finisher:
                        #         for sale_order_edit_decoration_id in selfclass.env.context['sale_order_edit_decoration_ids']:
                        #             if sale_order_edit_decoration_id[0] == 0:
                        #                 if sale_order_edit_decoration_id[2]['finisher']:
                        #                     same_finisher_ids.append(sale_order_edit_decoration_id[2]['finisher'])
                        #             if sale_order_edit_decoration_id[0] == 4:
                        #                 if selfclass.env['sale.order.edit.multi.decoration.line'].browse(sale_order_edit_decoration_id[1]):
                        #                     if selfclass.env['sale.order.edit.multi.decoration.line'].browse(sale_order_edit_decoration_id[1])[0].finisher:
                        #                         same_finisher_ids.append(selfclass.env['sale.order.edit.multi.decoration.line'].browse(sale_order_edit_decoration_id[1])[0].finisher.id)
                        #                     else:
                        #                         same_finisher_ids.append(False)
                        #             if sale_order_edit_decoration_id[0] == 1:
                        #                 if 'finisher' in sale_order_edit_decoration_id[2]:
                        #                     same_finisher_ids.append(sale_order_edit_decoration_id[2]['finisher'])
                        #                 else:
                        #                     if selfclass.env['sale.order.edit.multi.decoration.line'].browse(sale_order_edit_decoration_id[1]):
                        #                         if selfclass.env['sale.order.edit.multi.decoration.line'].browse(sale_order_edit_decoration_id[1])[0].finisher:
                        #                             same_finisher_ids.append(selfclass.env['sale.order.edit.multi.decoration.line'].browse(sale_order_edit_decoration_id[1])[0].finisher.id)
                        #                         else:
                        #                             same_finisher_ids.append(False)
                        #     if same_finisher_ids:
                        #         for same_finisher_id in same_finisher_ids:
                        #             if same_finisher_id != same_finisher_ids[0]:
                        #                 same_finisher = False
                        #                 break
                        #     else:
                        #         same_finisher = False
                        # if same_finisher:
                        #     if same_finisher_ids:
                        #         if finisher.finisher_id.id != same_finisher_ids[0]:
                        #             continue
                        # End Of More than 1 decoration for variant should use same finisher
                        for product_imprint_id in finisher.product_imprint_ids:
                            if not selfclass.imprint_location and not selfclass.finisher:
                                imprint_methods.append(
                                    product_imprint_id.decoration_method.id)
                            elif selfclass.imprint_location and not selfclass.finisher:
                                if selfclass.imprint_location.id == product_imprint_id.decoration_location.id:
                                    imprint_methods.append(product_imprint_id.decoration_method.id)
                            elif not selfclass.imprint_location and selfclass.finisher:
                                if selfclass.finisher.id == finisher.finisher_id.id:
                                    imprint_methods.append(product_imprint_id.decoration_method.id)
                            else:
                                if selfclass.imprint_location.id == product_imprint_id.decoration_location.id and selfclass.finisher.id == finisher.finisher_id.id:
                                    imprint_methods.append(product_imprint_id.decoration_method.id)
                            if not selfclass.imprint_method and not selfclass.finisher:
                                imprint_locations.append(
                                    product_imprint_id.decoration_location.id)
                            elif selfclass.imprint_method and not selfclass.finisher:
                                if selfclass.imprint_method.id == product_imprint_id.decoration_method.id:
                                    imprint_locations.append(product_imprint_id.decoration_location.id)
                            elif not selfclass.imprint_method and selfclass.finisher:
                                if selfclass.finisher.id == finisher.finisher_id.id:
                                    imprint_locations.append(product_imprint_id.decoration_location.id)
                            else:
                                if selfclass.imprint_method.id == product_imprint_id.decoration_method.id and selfclass.finisher.id == finisher.finisher_id.id:
                                    imprint_locations.append(product_imprint_id.decoration_location.id)
                            if not selfclass.imprint_method and not selfclass.imprint_location:
                                finishers.append(finisher.finisher_id.id)
                            elif selfclass.imprint_method and not selfclass.imprint_location:
                                if selfclass.imprint_method.id == product_imprint_id.decoration_method.id:
                                    finishers.append(finisher.finisher_id.id)
                            elif not selfclass.imprint_method and selfclass.imprint_location:
                                if selfclass.imprint_location.id == product_imprint_id.decoration_location.id:
                                    finishers.append(finisher.finisher_id.id)
                            else:
                                if selfclass.imprint_method.id == product_imprint_id.decoration_method.id and selfclass.imprint_location.id == product_imprint_id.decoration_location.id:
                                    finishers.append(finisher.finisher_id.id)
                imprint_methods = list(set(imprint_methods))
                imprint_locations = list(set(imprint_locations))
                finishers = list(set(finishers))
                # Fetching Data From Finsihers
                vendor_imprint_methods, vendor_imprint_locations, other_finishers = self.fetching_vendor_locations_and_methods(selfclass)
                imprint_methods, imprint_locations, finishers = self._onchange_imprint_method_extra(selfclass, imprint_methods, imprint_locations, finishers, vendor_imprint_methods, vendor_imprint_locations, other_finishers)
                vendor_imprint_methods, vendor_imprint_locations, other_finishers = self._onchange_vendor_imprint_method_extra(selfclass, vendor_imprint_methods, vendor_imprint_locations, other_finishers)
                imprint_methods = list(set(imprint_methods))
                imprint_locations = list(set(imprint_locations))
                vendor_imprint_methods = list(set(vendor_imprint_methods) - set(imprint_methods))
                vendor_imprint_locations = list(set(vendor_imprint_locations) - set(imprint_locations))
                imprint_methods += vendor_imprint_methods
                imprint_locations += vendor_imprint_locations
                # To Delete Unwanted Finishers and Vendor Methods & Locations
                to_be_deletes = []
                for vendor_imprint_method in vendor_imprint_methods:
                    if vendor_imprint_method not in imprint_methods:
                        to_be_deletes.append(vendor_imprint_method)
                for to_be_delete in to_be_deletes:
                    vendor_imprint_methods.remove(to_be_delete)
                to_be_deletes = []
                for vendor_imprint_location in vendor_imprint_locations:
                    if vendor_imprint_location not in imprint_locations:
                        to_be_deletes.append(vendor_imprint_location)
                for to_be_delete in to_be_deletes:
                    vendor_imprint_locations.remove(to_be_delete)
        return imprint_methods, vendor_imprint_methods, imprint_locations, vendor_imprint_locations, list(set(finishers + other_finishers)), finishers
        
    def _onchange_imprint_method_extra(self, selfclass, imprint_methods, imprint_locations, finishers , vendor_imprint_methods, vendor_imprint_locations, other_finishers):
        """ Private method added for later given functionality"""
        to_be_deletes, change_supplier = [],[]
        for finisher_id in finishers:
            delete = False
            if selfclass.env.context.get('art_lines', False):
                lines = selfclass.env['art.production.line'].browse(selfclass.env.context.get('art_lines', False))
                for line in lines:
                    existed = False
                    change_supplier_included = False
                    if line.n_product_id:
                        finisher_ids = line.n_product_id.finisher_ids
                    if line.product_id:
                        finisher_ids = line.product_id.finisher_ids
                    for finisher in finisher_ids:
                        if finisher_id == finisher.finisher_id.id:
                            existed = True
                            if line.art_id.vendor_id.is_finisher and line.art_id.vendor_id.id == finisher.finisher_id.id and not change_supplier_included:
                                change_supplier.append(line.art_id.vendor_id.id)
                                change_supplier_included = True
            if selfclass.env.context.get('sale_order_lines', False):
                lines = selfclass.env['sale.order.line'].browse(selfclass.env.context.get('sale_order_lines', False))
                for line in lines:
                    existed = False
                    for finisher in line.product_id.finisher_ids:
                        if finisher_id == finisher.finisher_id.id:
                            existed = True
                    if not existed:
                        delete = True
            if delete:
                to_be_deletes.append(finisher_id)
        line_len = 0
        if selfclass.env.context.get('art_lines', False):
            line_len = len(selfclass.env.context.get('art_lines'))
            if len(change_supplier) == line_len:
                same_item = False
                all_are_same = True
                for item in change_supplier:
                    if not same_item:
                        same_item = item
                    if same_item != item:
                        all_are_same = False
                if all_are_same:
                    to_be_deletes = []
                    finishers = [item]
                else:
                    to_be_deletes = []
                    finishers = []
        for to_be_delete in to_be_deletes:
            finishers.remove(to_be_delete)
        to_be_deletes = []
        finisher_method_exist = []
        unwanted_decoration_methods = selfclass.env['decoration.method'].search([('name', 'ilike', 'set at product')])
        if unwanted_decoration_methods:
            to_be_deletes += unwanted_decoration_methods.ids
        for imprint_method in imprint_methods:
            delete = False
            if selfclass.env.context.get('art_lines', False):
                lines = selfclass.env['art.production.line'].browse(selfclass.env.context.get('art_lines', False))
                for line in lines:
                    existed = False
                    if line.n_product_id:
                        finisher_ids = line.n_product_id.finisher_ids
                    if line.product_id:
                        finisher_ids = line.product_id.finisher_ids
                    for finisher in finisher_ids:
                        for product_imprint_id in finisher.product_imprint_ids:
                            if imprint_method == product_imprint_id.decoration_method.id and product_imprint_id.decoration_location.id in imprint_locations and finisher.finisher_id.id in finishers:
                                existed = True
                                finisher_method_exist.append(finisher.finisher_id.id)
                    if not existed:
                        delete = True
            elif selfclass.env.context.get('sale_order_lines', False):
                lines = selfclass.env['sale.order.line'].browse(selfclass.env.context.get('sale_order_lines', False))
                for line in lines:
                    existed = False
                    for finisher in line.product_id.finisher_ids:
                        for product_imprint_id in finisher.product_imprint_ids:
                            if imprint_method == product_imprint_id.decoration_method.id and (product_imprint_id.decoration_location.id in imprint_locations or product_imprint_id.decoration_location.id in vendor_imprint_locations) and (finisher.finisher_id.id in finishers or finisher.finisher_id.id in other_finishers):
                                existed = True
                                finisher_method_exist.append(finisher.finisher_id.id)
                    if not existed:
                        delete = True
            if delete:
                to_be_deletes.append(imprint_method)
        for to_be_delete in to_be_deletes:
            if to_be_delete in imprint_methods:
                imprint_methods.remove(to_be_delete)
        to_be_deleted = []
        finisher_location_exist = []
        unwanted_decoration_locations = selfclass.env['decoration.location'].search([('name', 'ilike', 'set at product')])
        if unwanted_decoration_locations:
            to_be_deleted += unwanted_decoration_locations.ids
        for imprint_location in imprint_locations:
            delete = False
            if selfclass.env.context.get('art_lines', False):
                lines = selfclass.env['art.production.line'].browse(selfclass.env.context.get('art_lines', False))
                for line in lines:
                    existed = False
                    if line.n_product_id:
                        finisher_ids = line.n_product_id.finisher_ids
                    if line.product_id:
                        finisher_ids = line.product_id.finisher_ids
                    for finisher in finisher_ids:
                        for product_imprint_id in finisher.product_imprint_ids:
                            if imprint_location == product_imprint_id.decoration_location.id and product_imprint_id.decoration_method.id in imprint_methods and finisher.finisher_id.id in finishers:
                                existed = True
                                finisher_location_exist.append(finisher.finisher_id.id)
                    if not existed:
                        delete = True
            elif selfclass.env.context.get('sale_order_lines', False):
                lines = selfclass.env['sale.order.line'].browse(selfclass.env.context.get('sale_order_lines', False))
                for line in lines:
                    existed = False
                    for finisher in line.product_id.finisher_ids:
                        for product_imprint_id in finisher.product_imprint_ids:
                            if imprint_location == product_imprint_id.decoration_location.id and (product_imprint_id.decoration_method.id in imprint_methods or product_imprint_id.decoration_method.id in vendor_imprint_methods) and (finisher.finisher_id.id in finishers or finisher.finisher_id.id in other_finishers):
                                existed = True
                                finisher_location_exist.append(finisher.finisher_id.id)
                    if not existed:
                        delete = True
            if delete:
                to_be_deleted.append(imprint_location)
        for to_delete in to_be_deleted:
            if to_delete in imprint_locations:
                imprint_locations.remove(to_delete)
        if selfclass.env.context.get('art_lines', False):
            finisher_method_exist = list(set(finisher_method_exist))
            finisher_location_exist = list(set(finisher_location_exist))
            to_be_deleted = []
            for finisher in finishers:
                if finisher not in finisher_method_exist or finisher not in finisher_location_exist:
                    to_be_deleted.append(finisher)
            for to_delete in to_be_deleted:
                finishers.remove(to_delete)
        return (imprint_methods, imprint_locations, finishers)
   
    def _onchange_vendor_imprint_method_extra(self, selfclass, imprint_methods, imprint_locations, finishers):
        """ Private method added for later given functionality"""
        unwanted_decoration_methods = selfclass.env['decoration.method'].search([('name', 'ilike', 'set at product')])
        if unwanted_decoration_methods:
            for to_be_delete in unwanted_decoration_methods:
                if to_be_delete.id in imprint_methods:
                    imprint_methods.remove(to_be_delete.id)
        unwanted_decoration_locations = selfclass.env['decoration.location'].search([('name', 'ilike', 'set at product')])
        if unwanted_decoration_locations:
            for to_delete in unwanted_decoration_locations:
                if to_delete.id in imprint_locations:
                    imprint_locations.remove(to_delete.id)
        return (imprint_methods, imprint_locations, finishers)

    def fetching_vendor_locations_and_methods(self, selfclass):
        vendor_imprint_methods = []
        vendor_imprint_locations = []
        finishers = []
        all_finishers = selfclass.env['res.partner'].search([('is_finisher', '=', True)])
        for finisher in all_finishers:
            for finisher_id in finisher.finisher_ids:
                if not selfclass.imprint_location and not selfclass.finisher:
                    vendor_imprint_methods.append(
                        finisher_id.decoration_method.id)
                elif selfclass.imprint_location and not selfclass.finisher:
                    if selfclass.imprint_location.id == finisher_id.decoration_location.id:
                        vendor_imprint_methods.append(finisher_id.decoration_method.id)
                elif not selfclass.imprint_location and selfclass.finisher:
                    if selfclass.finisher.id == finisher.id:
                        vendor_imprint_methods.append(finisher_id.decoration_method.id)
                else:
                    if selfclass.imprint_location.id == finisher_id.decoration_location.id and selfclass.finisher.id == finisher.id:
                        vendor_imprint_methods.append(finisher_id.decoration_method.id)
                if not selfclass.imprint_method and not selfclass.finisher:
                    vendor_imprint_locations.append(
                        finisher_id.decoration_location.id)
                elif selfclass.imprint_method and not selfclass.finisher:
                    if selfclass.imprint_method.id == finisher_id.decoration_method.id:
                        vendor_imprint_locations.append(finisher_id.decoration_location.id)
                elif not selfclass.imprint_method and selfclass.finisher:
                    if selfclass.finisher.id == finisher.id:
                        vendor_imprint_locations.append(finisher_id.decoration_location.id)
                else:
                    if selfclass.imprint_method.id == finisher_id.decoration_method.id and selfclass.finisher.id == finisher.id:
                        vendor_imprint_locations.append(finisher_id.decoration_location.id)
                if not selfclass.imprint_method and not selfclass.imprint_location:
                    finishers.append(finisher.id)
                elif selfclass.imprint_method and not selfclass.imprint_location:
                    if selfclass.imprint_method.id == finisher_id.decoration_method.id:
                        finishers.append(finisher.id)
                elif not selfclass.imprint_method and selfclass.imprint_location:
                    if selfclass.imprint_location.id == finisher_id.decoration_location.id:
                        finishers.append(finisher.id)
                else:
                    if selfclass.imprint_method.id == finisher_id.decoration_method.id and selfclass.imprint_location.id == finisher_id.decoration_location.id:
                        finishers.append(finisher.id)
        return list(set(vendor_imprint_methods)), list(set(vendor_imprint_locations)), list(set(finishers))

    def get_default_value_for_finisher_field(self, selfclass):
        all_finishers_assigned_to_product = []
        if selfclass.env.context.get('sale_order_lines', False):
            lines = selfclass.env['sale.order.line'].browse(selfclass.env.context.get('sale_order_lines', False))
            for line in lines:
                for finisher in line.product_id.finisher_ids:
                    if finisher.finisher_id and finisher.finisher_id.id not in all_finishers_assigned_to_product:
                        all_finishers_assigned_to_product.append(finisher.finisher_id.id)
        to_be_deletes, change_supplier = [],[]
        for finisher_id in all_finishers_assigned_to_product:
            delete = False
            if selfclass.env.context.get('sale_order_lines', False):
                lines = selfclass.env['sale.order.line'].browse(selfclass.env.context.get('sale_order_lines', False))
                for line in lines:
                    existed = False
                    change_supplier_included = False
                    for finisher in line.product_id.finisher_ids:
                        if finisher_id == finisher.finisher_id.id:
                            existed = True
                            if line.change_supplier.name.is_finisher and line.change_supplier.name.id == finisher.finisher_id.id and not change_supplier_included:
                                change_supplier.append(line.change_supplier.name.id)
                                change_supplier_included = True
                if not existed:
                    delete = True
            if delete:
                to_be_deletes.append(finisher_id)
        line_len = 0
        if selfclass.env.context.get('sale_order_lines', False):
            line_len = len(selfclass.env.context.get('sale_order_lines'))
            if len(change_supplier) == line_len:
                same_item = False
                all_are_same = True
                for item in change_supplier:
                    if not same_item:
                        same_item = item
                    if same_item != item:
                        all_are_same = False
                if all_are_same:
                    to_be_deletes = []
                    all_finishers_assigned_to_product = [item]
                else:
                    to_be_deletes = []
                    all_finishers_assigned_to_product = []
        for to_be_delete in to_be_deletes:
            all_finishers_assigned_to_product.remove(to_be_delete)
        if len(all_finishers_assigned_to_product) == 1:
            return all_finishers_assigned_to_product[0]
        if len(all_finishers_assigned_to_product) >= 1:
            preferred_finisher = False
            if selfclass.env.context.get('sale_order_lines', False):
                lines = selfclass.env['sale.order.line'].browse(selfclass.env.context.get('sale_order_lines', False))
                for finisher in all_finishers_assigned_to_product:
                    exist_in_all_order_lines = True
                    for line in lines:
                        if line.product_id.finisher_ids.filtered("preferred_vendor"):
                            preferred_finishers = line.product_id.finisher_ids.filtered("preferred_vendor").mapped("finisher_id.id")
                            if finisher not in preferred_finishers:
                                exist_in_all_order_lines = False
                        else:
                            exist_in_all_order_lines = False
                    if exist_in_all_order_lines:
                        preferred_finisher = finisher
                        break
            return preferred_finisher
        return False
                   
    def onchange_customer_art(self, sales_order_class):
        """Returns customer data"""
        customer_arts = sales_order_class.customer_arts.ids
        if sales_order_class.env.context.get('order_id', False):
            sale_order = sales_order_class.env['sale.order'].browse(
                sales_order_class.env.context.get('order_id', False))
            if sale_order:
                customer_arts = sale_order.customer_arts.ids
        if sales_order_class.env.context['model'] == 'sale.order.edit.decoration':
            if 'sale_order_edit_decoration_ids' in sales_order_class.env.context:
                for sale_order_edit_decoration_id in sales_order_class.env.context['sale_order_edit_decoration_ids']:
                    if sale_order_edit_decoration_id[0] == 0:
                        if sale_order_edit_decoration_id[2]['customer_art']:
                            customer_arts.append(sale_order_edit_decoration_id[2]['customer_art'])
                    if sale_order_edit_decoration_id[0] == 4:
                        if sales_order_class.env['sale.order.edit.decoration.line'].browse(sale_order_edit_decoration_id[1]):
                            if sales_order_class.env['sale.order.edit.decoration.line'].browse(sale_order_edit_decoration_id[1])[0].customer_art:
                                customer_arts.append(sales_order_class.env['sale.order.edit.decoration.line'].browse(sale_order_edit_decoration_id[1])[0].customer_art.id)
                            else:
                                customer_arts.append(False)
                    if sale_order_edit_decoration_id[0] == 1:
                        if 'customer_art' in sale_order_edit_decoration_id[2]:
                            customer_arts.append(sale_order_edit_decoration_id[2]['customer_art'])
                        else:
                            if sales_order_class.env['sale.order.edit.decoration.line'].browse(sale_order_edit_decoration_id[1]):
                                if sales_order_class.env['sale.order.edit.decoration.line'].browse(sale_order_edit_decoration_id[1])[0].customer_art:
                                    customer_arts.append(sales_order_class.env['sale.order.edit.decoration.line'].browse(sale_order_edit_decoration_id[1])[0].customer_art.id)
                                else:
                                    customer_arts.append(False)
        if sales_order_class.env.context['model'] == 'sale.order.edit.multi.decoration':
            if 'sale_order_edit_decoration_ids' in sales_order_class.env.context:
                for sale_order_edit_decoration_id in sales_order_class.env.context['sale_order_edit_decoration_ids']:
                    if sale_order_edit_decoration_id[0] == 0:
                        if sale_order_edit_decoration_id[2]['customer_art']:
                            customer_arts.append(sale_order_edit_decoration_id[2]['customer_art'])
                    if sale_order_edit_decoration_id[0] == 4:
                        if sales_order_class.env['sale.order.edit.multi.decoration.line'].browse(sale_order_edit_decoration_id[1]):
                            if sales_order_class.env['sale.order.edit.multi.decoration.line'].browse(sale_order_edit_decoration_id[1])[0].customer_art:
                                customer_arts.append(sales_order_class.env['sale.order.edit.multi.decoration.line'].browse(sale_order_edit_decoration_id[1])[0].customer_art.id)
                            else:
                                customer_arts.append(False)
                    if sale_order_edit_decoration_id[0] == 1:
                        if 'customer_art' in sale_order_edit_decoration_id[2]:
                            customer_arts.append(sale_order_edit_decoration_id[2]['customer_art'])
                        else:
                            if sales_order_class.env['sale.order.edit.multi.decoration.line'].browse(sale_order_edit_decoration_id[1]):
                                if sales_order_class.env['sale.order.edit.multi.decoration.line'].browse(sale_order_edit_decoration_id[1])[0].customer_art:
                                    customer_arts.append(sales_order_class.env['sale.order.edit.multi.decoration.line'].browse(sale_order_edit_decoration_id[1])[0].customer_art.id)
                                else:
                                    customer_arts.append(False)
        item_with_atleat_one_preset_decoration = False
        customer_arts_for_item_with_atleat_one_preset_decoration = []
        item_with_preset_decoration = False
        customer_arts_for_item_with_preset_decoration = []
        if sales_order_class.env.context.get('sale_order_lines', False):
            for sale_order_line in sales_order_class.env['sale.order.line'].browse(sales_order_class.env.context.get('sale_order_lines', False)):
                if sale_order_line.product_id and sale_order_line.product_id.product_tmpl_id  and sale_order_line.product_id.product_tmpl_id.present_decoration:
                    item_with_preset_decoration = True
                    customer_arts_for_item_with_preset_decoration += (sale_order_line.product_id.product_tmpl_id.customer_arts.ids)
                    for finisher_id in sale_order_line.product_id.product_tmpl_id.finisher_ids:
                        for product_imprint_id in finisher_id.product_imprint_ids:
                            if product_imprint_id.present_decoration:
                                item_with_atleat_one_preset_decoration = True
                                customer_arts_for_item_with_atleat_one_preset_decoration.append(product_imprint_id.customer_art.id)     
        customer_arts = list(set(customer_arts))
        if customer_arts_for_item_with_preset_decoration:
            used_customer_arts_for_item_with_preset_decoration = list(set(customer_arts).intersection(customer_arts_for_item_with_preset_decoration))
            if used_customer_arts_for_item_with_preset_decoration:
                customer_arts = used_customer_arts_for_item_with_preset_decoration
            else:
                customer_arts = customer_arts_for_item_with_preset_decoration
        if item_with_atleat_one_preset_decoration:
            customer_arts = customer_arts_for_item_with_atleat_one_preset_decoration
        return customer_arts 

    def onchange_stock_color(self, selfclass):
        stock_colors = []
        if selfclass.global_search:
            for color_group in selfclass.env['color.group'].search([('generic_list', '=', True)]):
                for color in color_group.color_lines:
                    if color.color:
                        stock_colors.append(color.color.id)
        else:
            # Check For The Stock Colors Of Customer Art
            stock_colors_is_set_from_customer_art = False
            if selfclass.customer_art and selfclass.env.context.get('art_lines', False):
                for line in selfclass.env['art.production.line'].browse(selfclass.env.context.get('art_lines', False)):
                    if line.n_product_id:
                        prod_id = line.n_product_id
                    if line.product_id:
                        prod_id = line.product_id
                    if prod_id and prod_id.product_tmpl_id and prod_id.product_tmpl_id.present_decoration and prod_id.product_tmpl_id.customer_arts:
                        if selfclass.customer_art.id in prod_id.product_tmpl_id.customer_arts.ids:
                            stock_colors_is_set_from_customer_art = True
                            stock_colors += selfclass.customer_art.stock_color.ids
            if selfclass.customer_art and selfclass.env.context.get('sale_order_lines', False):
                for sale_order_line in selfclass.env['sale.order.line'].browse(selfclass.env.context.get('sale_order_lines', False)):
                    if sale_order_line.product_id and sale_order_line.product_id.product_tmpl_id and sale_order_line.product_id.product_tmpl_id.present_decoration and sale_order_line.product_id.product_tmpl_id.customer_arts:
                        if selfclass.customer_art.id in sale_order_line.product_id.product_tmpl_id.customer_arts.ids:
                            stock_colors_is_set_from_customer_art = True
                            stock_colors += selfclass.customer_art.stock_color.ids
            if not stock_colors_is_set_from_customer_art:   
                if selfclass.finisher and selfclass.imprint_method and selfclass.imprint_location:
                    color_groups = []
                    if selfclass.env.context.get('sale_order_lines', False):
                        for sale_order_line in selfclass.env['sale.order.line'].browse(selfclass.env.context.get('sale_order_lines', False)):
                            stock_color_available_for_current_order_line = False
                            for finisher in sale_order_line.product_id.finisher_ids:
                                if selfclass.finisher.id == finisher.finisher_id.id:
                                    for product_imprint_id in finisher.product_imprint_ids:
                                        if selfclass.imprint_location.id == product_imprint_id.decoration_location.id and selfclass.imprint_method.id == product_imprint_id.decoration_method.id:
                                            if product_imprint_id.stock_color:
                                                stock_color_available_for_current_order_line = True
                                                color_groups.append(product_imprint_id.stock_color.id)
                            if not stock_color_available_for_current_order_line:
                                if selfclass.finisher:
                                    for finisher_id in selfclass.finisher.finisher_ids:
                                        if finisher_id.decoration_method.id == selfclass.imprint_method.id and finisher_id.decoration_location.id == selfclass.imprint_location.id:
                                            color_groups += finisher_id.stock_color.ids
                    elif selfclass.env.context.get('art_lines', False):
                        for line in selfclass.env['art.production.line'].browse(selfclass.env.context.get('art_lines', False)):
                            if line.n_product_id:
                                finisher_ids = line.n_product_id.finisher_ids
                            if line.product_id:
                                finisher_ids = line.product_id.finisher_ids
                            for finisher in finisher_ids:
                                if selfclass.finisher.id == finisher.finisher_id.id:
                                    for product_imprint_id in finisher.product_imprint_ids:
                                        if selfclass.imprint_location.id == product_imprint_id.decoration_location.id and selfclass.imprint_method.id == product_imprint_id.decoration_method.id:
                                            if product_imprint_id.stock_color:
                                                color_groups.append(product_imprint_id.stock_color.id)
                    color_groups = list(set(color_groups))
                    if color_groups:
                        for color_group in selfclass.env['color.group'].browse(color_groups):
                            for color in color_group.color_lines:
                                if color.color:
                                    stock_colors.append(color.color.id)
        stock_colors = list(set(stock_colors))
        return stock_colors

    def onchange_pms_code(self, selfclass):
        pms_codes = []
        # Check For The Pms Codes Of Customer Art
        pms_codes_is_set_from_customer_art = False
        if selfclass.customer_art and selfclass.env.context.get('art_lines', False) and not selfclass.global_search:
            for line in selfclass.env['art.production.line'].browse(selfclass.env.context.get('art_lines', False)):
                if line.n_product_id:
                    prod_id = line.n_product_id
                if line.product_id:
                    prod_id = line.product_id
                if prod_id and prod_id.product_tmpl_id and prod_id.product_tmpl_id.present_decoration and prod_id.product_tmpl_id.customer_arts:
                    if selfclass.customer_art.id in prod_id.product_tmpl_id.customer_arts.ids:
                        pms_codes_is_set_from_customer_art = True
                        pms_codes += selfclass.customer_art.pms_code.ids
        elif selfclass.customer_art and selfclass.env.context.get('sale_order_lines', False) and not selfclass.global_search:
            for sale_order_line in selfclass.env['sale.order.line'].browse(selfclass.env.context.get('sale_order_lines', False)):
                if sale_order_line.product_id and sale_order_line.product_id.product_tmpl_id and sale_order_line.product_id.product_tmpl_id.present_decoration and sale_order_line.product_id.product_tmpl_id.customer_arts:
                    if selfclass.customer_art.id in sale_order_line.product_id.product_tmpl_id.customer_arts.ids:
                        pms_codes_is_set_from_customer_art = True
                        pms_codes += selfclass.customer_art.pms_code.ids
        if not pms_codes_is_set_from_customer_art:
            pms_codes = selfclass.env['product.decoration.pms.code'].search([]).ids
        pms_codes = list(set(pms_codes))
        return pms_codes

    def _onchange_sale_order_line_id(self, selfclass):
        onchange_data = selfclass._onchange_imprint_method()
        if selfclass.sale_order_line_decoration_id.finisher.id in onchange_data['domain']['finisher'][1][2]:
            selfclass.finisher = selfclass.sale_order_line_decoration_id.finisher.id
        onchange_data = selfclass._onchange_imprint_method()
        if selfclass.sale_order_line_decoration_id.imprint_method.id in onchange_data['domain']['imprint_method'][1][2]:
            selfclass.imprint_method = selfclass.sale_order_line_decoration_id.imprint_method.id
        onchange_data = selfclass._onchange_imprint_method()
        if selfclass.sale_order_line_decoration_id.imprint_location.id in onchange_data['domain']['imprint_location'][1][2]:
            selfclass.imprint_location = selfclass.sale_order_line_decoration_id.imprint_location.id
        onchange_data = selfclass._onchange_customer_art()
        if selfclass.sale_order_line_decoration_id.customer_art.id in onchange_data['domain']['customer_art'][0][2]:
            selfclass.customer_art = selfclass.sale_order_line_decoration_id.customer_art.id
        onchange_data = selfclass._onchange_imprint_method()
        if selfclass.sale_order_line_decoration_id.pms_code.ids:
            existed_pms_code_ids = []
            for pms_code in selfclass.sale_order_line_decoration_id.pms_code.ids:
                if pms_code in onchange_data['domain']['pms_code'][0][2]:
                    existed_pms_code_ids.append(pms_code)
            selfclass.pms_code = [[6, 0, existed_pms_code_ids]]
        if selfclass.sale_order_line_decoration_id.stock_color.ids:
            existed_stock_color_ids = []
            for stock_color in selfclass.sale_order_line_decoration_id.stock_color.ids:
                if stock_color in onchange_data['domain']['stock_color'][0][2]:
                    existed_stock_color_ids.append(stock_color)
            selfclass.stock_color = [[6, 0, existed_stock_color_ids]]
        vendor_decoration_location, vendor_decoration_method, vendor_prod_specific = self.return_vendor_decoration_value(selfclass)
        selfclass.vendor_decoration_method = vendor_decoration_method
        selfclass.vendor_decoration_location = vendor_decoration_location
        selfclass.vendor_prod_specific = vendor_prod_specific

    def compute_pms_color_code(self, this):
        colors = [color.id for color in this.stock_color]
        color_lines = []
        if this.global_search:
            generic_list = this.env['color.group'].search([('generic_list', '=', True)]).ids
            generic_color_lines = this.env['finisher.color.line'].search([('generic_list', '=', True),
                                                                  ('color', 'in', colors),
                                                                  ('name', 'in', generic_list)])
            if generic_color_lines:
                color_lines = generic_color_lines
        else:
            imprint_lines = this.env['product.imprint'].search([('decoration_location', '=', this.imprint_location.id),
                                                         ('decoration_method', '=', this.imprint_method.id),
                                                         ('res_finisher', '=', this.finisher.id)])
            groups = [line.stock_color.id for line in imprint_lines]
            color_lines = this.env['finisher.color.line'].search([('partner_id', '=', this.finisher.id),
                                                                  ('color', 'in', colors),
                                                                  ('name', 'in', groups)])
            if not color_lines:
                generic_list = this.env['color.group'].search([('generic_list', '=', True)]).ids
                color_lines = this.env['finisher.color.line'].search([('generic_list', '=', True),
                                                                      ('color', 'in', colors),
                                                                      ('name', 'in', generic_list)])
        pms_color_code = ''
        if len(color_lines) == 1:
            pms_color_code += color_lines.code
        else:
            for line in color_lines:
                pms_color_code += line.code+', '
            pms_color_code = pms_color_code[:-2]
        return pms_color_code

    def return_vendor_decoration_domain(self, selfclass):
        vendor_decoration_methods = []
        vendor_decoration_locations = []
        if selfclass.imprint_method and selfclass.finisher:
            res_finishers = selfclass.env['res.finisher'].search([('decoration_method', '=', selfclass.imprint_method.id), ('partner_id', '=', selfclass.finisher.id)])
            vendor_decoration_methods = [res_finisher.vendor_decoration_method.id for res_finisher in res_finishers]
        if selfclass.imprint_method and selfclass.finisher and selfclass.vendor_decoration_method:
            res_finishers = selfclass.env['res.finisher'].search([('decoration_method', '=', selfclass.imprint_method.id), ('partner_id', '=', selfclass.finisher.id),('vendor_decoration_method', '=', selfclass.vendor_decoration_method.id)])
            vendor_decoration_locations = [res_finisher.vendor_decoration_location.id for res_finisher in res_finishers]                
        return vendor_decoration_methods, vendor_decoration_locations

    def return_vendor_decoration_value(self, selfclass):
        vendor_decoration_location, vendor_decoration_method, vendor_prod_specific = False, False, False
        all_vendor_decoration_location, all_vendor_decoration_method, all_vendor_prod_specific = [], [], []
        if selfclass.imprint_method and selfclass.imprint_location and selfclass.finisher:
            if selfclass.env.context.get('art_lines', False):
                for line in selfclass.env['art.production.line'].browse(selfclass.env.context.get('art_lines', False)):
                    if line.n_product_id:
                        finisher_ids = line.n_product_id.finisher_ids
                    if line.product_id:
                        finisher_ids = line.product_id.finisher_ids
                    for finisher in finisher_ids:
                        if finisher.finisher_id and finisher.finisher_id.id == selfclass.finisher.id:
                            for product_imprint_id in finisher.product_imprint_ids:
                                if product_imprint_id:
                                    if product_imprint_id.decoration_method.id == selfclass.imprint_method.id and product_imprint_id.decoration_location.id == selfclass.imprint_location.id:
                                        all_vendor_decoration_location.append(product_imprint_id.vendor_decoration_location.id)
                                        all_vendor_decoration_method.append(product_imprint_id.vendor_decoration_method.id)
                                        all_vendor_prod_specific.append(product_imprint_id.imprint_method_product_specific)
            elif selfclass.env.context.get('sale_order_lines', False):
                for sale_order_line in selfclass.env['sale.order.line'].browse(selfclass.env.context.get('sale_order_lines', False)):
                    for finisher in sale_order_line.product_id.finisher_ids:
                        if finisher.finisher_id and finisher.finisher_id.id == selfclass.finisher.id:
                            for product_imprint_id in finisher.product_imprint_ids:
                                if product_imprint_id:
                                    if product_imprint_id.decoration_method.id == selfclass.imprint_method.id and product_imprint_id.decoration_location.id == selfclass.imprint_location.id:
                                        all_vendor_decoration_location.append(product_imprint_id.vendor_decoration_location.id)
                                        all_vendor_decoration_method.append(product_imprint_id.vendor_decoration_method.id)
                                        all_vendor_prod_specific.append(product_imprint_id.imprint_method_product_specific)
                if not all_vendor_decoration_location or (len(selfclass.env.context.get('sale_order_lines', False)) != len(all_vendor_decoration_location)) or not self.is_list_has_all_values_similar(all_vendor_decoration_location): 
                    all_vendor_decoration_location = []
                    for sale_order_line in selfclass.env['sale.order.line'].browse(selfclass.env.context.get('sale_order_lines', False)):
                        if selfclass.finisher:
                            for finisher_id in selfclass.finisher.finisher_ids:
                                if finisher_id.decoration_method.id == selfclass.imprint_method.id and finisher_id.decoration_location.id == selfclass.imprint_location.id:
                                    all_vendor_decoration_location.append(finisher_id.vendor_decoration_location.id)
                if not all_vendor_decoration_method or (len(selfclass.env.context.get('sale_order_lines', False)) != len(all_vendor_decoration_method)) or not self.is_list_has_all_values_similar(all_vendor_decoration_method):
                    all_vendor_decoration_method = []
                    for sale_order_line in selfclass.env['sale.order.line'].browse(selfclass.env.context.get('sale_order_lines', False)):
                        if selfclass.finisher:
                            for finisher_id in selfclass.finisher.finisher_ids:
                                if finisher_id.decoration_method.id == selfclass.imprint_method.id and finisher_id.decoration_location.id == selfclass.imprint_location.id:
                                    all_vendor_decoration_method.append(finisher_id.vendor_decoration_method.id)
            if all_vendor_decoration_location:
                if self.is_list_has_all_values_similar(all_vendor_decoration_location):
                    vendor_decoration_location = all_vendor_decoration_location[0]
            if all_vendor_decoration_method:
                if self.is_list_has_all_values_similar(all_vendor_decoration_method):
                    vendor_decoration_method = all_vendor_decoration_method[0]
            if all_vendor_prod_specific:
                if self.is_list_has_all_values_similar(all_vendor_prod_specific):
                    vendor_prod_specific = all_vendor_prod_specific[0]
        return vendor_decoration_location, vendor_decoration_method, vendor_prod_specific

    def is_list_has_all_values_similar(self, list):
        for item in list[1:]:
            if item != list[0]:
                return False
        return True

    def url_fix(self, s, charset='utf-8'):
        if isinstance(s, unicode):
            s = s.encode(charset, 'ignore')
        scheme, netloc, path, qs, anchor = urlparse.urlsplit(s)
        path = urllib.quote(path, '/%')
        qs = urllib.quote_plus(qs, ':&=')
        return urlparse.urlunsplit((scheme, netloc, path, qs, anchor))
