from openerp import models, fields, api


class FeedbackWizard(models.TransientModel):
    _name = 'feedback.wizard'

    order_conf_html = fields.Html('Order Confirmation HTML')


class WarningWizard(models.TransientModel):
    _name = 'warning.wizard'

    @api.multi
    def get_def_war(self):
        if 'warning' in self._context:
            return self._context['warning']

    name = fields.Text(default=get_def_war)