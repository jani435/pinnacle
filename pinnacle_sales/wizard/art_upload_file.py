# -*- coding: utf-8 -*-
from openerp import models, fields, api
import requests
import json
from odoo import tools


class ArtUploadFile(models.TransientModel):
    _name = 'art.upload.file'

    spec_file = fields.Binary('Art Page 1 File(s)')
    spec_file_url = fields.Char('Art Page 1 URL')
    spec_file_name = fields.Char('Art Page 1 File(s) Name')
    vendor_file = fields.Binary('Vendor Files')
    vendor_file_url = fields.Char('Vendor URL')
    vendor_file_name = fields.Char('Vendor File Name')
    spec_file2 = fields.Binary('Art Page 2 File(s)')
    spec_file2_name = fields.Char('Art Page 2 File(s) Name') 
    spec_file2_url = fields.Char('Art Page 2 URL')

    @api.multi
    def bulk_upload(self):
        self.ensure_one()
        #============Task: 13442================
        for aws_id in self.env.context.get('art_id', False):
            if aws_id:
                art_id = self.env['art.production'].browse(aws_id)
                if art_id:
                    for associate_file in self.env['art.production.line'].browse(
                            self.env.context.get('art_line_ids', False)):
                        if self.spec_file:
                            associate_file.spec_file_name = self.spec_file_name
                            associate_file.spec_file = self.spec_file
                        else:
                            associate_file.spec_file_url = self.spec_file_url
                            if not self.spec_file_url:
                                associate_file.spec_file_name = self.spec_file_name
                        if self.vendor_file:
                            associate_file.vendor_file_name = self.vendor_file_name
                            associate_file.vendor_file = self.vendor_file
                        else:
                            associate_file.vendor_file_url = self.vendor_file_url
                            if not self.vendor_file_url:
                                associate_file.vendor_file_name = self.vendor_file_name
                        if self.spec_file2:
                            associate_file.spec_file2_name = self.spec_file2_name
                            associate_file.spec_file2 = self.spec_file2
                        else:
                            associate_file.spec_file2_url = self.spec_file2_url
                            if not self.spec_file2_url:
                                associate_file.spec_file2_name = self.spec_file2_name
                        associate_file.select = False
                    # art_id.hide_file(art_id.id)
        return {'type': 'ir.actions.act_window_close'}

    @api.multi
    def remove_spec_file(self):
        if self.spec_file_url:
            for aws_id in self.env.context.get('art_id', False):
                if aws_id:
                    for associate_file in self.env['art.production.line'].browse(
                            self.env.context.get('art_line_ids', False)):
                        if self.spec_file_url:
                            associate_file.spec_file_att.unlink()
                            associate_file.spec_file_url = False
        return True

    @api.multi
    def remove_spec_file2(self):
        if self.spec_file2_url:
            for aws_id in self.env.context.get('art_id', False):
                if aws_id:
                    for associate_file in self.env['art.production.line'].browse(
                            self.env.context.get('art_line_ids', False)):
                        if self.spec_file2_url:
                            associate_file.spec_file2_att.unlink()
                            associate_file.spec_file2_url = False
        return True

    @api.multi
    def remove_vendor_file(self):
        if self.vendor_file_url:
            for aws_id in self.env.context.get('art_id', False):
                if aws_id:
                    for associate_file in self.env['art.production.line'].browse(
                            self.env.context.get('art_line_ids', False)):
                        if self.vendor_file_url:
                            associate_file.vendor_file_att.unlink()
                            associate_file.vendor_file_url = False
        return True

    @api.multi
    def remove_all_files(self):
        for aws_id in self.env.context.get('art_id', False):
            if aws_id:
                for associate_file in self.env['art.production.line'].browse(
                    self.env.context.get('art_line_ids', False)):
                    if self.vendor_file_url:
                        associate_file.vendor_file_att.unlink()
                        associate_file.vendor_file_url = False
                    if self.spec_file2_url:
                        associate_file.spec_file2_att.unlink()
                        associate_file.spec_file2_url = False
                    if self.spec_file_url:
                        associate_file.spec_file_att.unlink()
                        associate_file.spec_file_url = False
        return True

