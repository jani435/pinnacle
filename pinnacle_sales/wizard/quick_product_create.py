# -*- coding: utf-8 -*-
from openerp import models, fields, api, _
from openerp.exceptions import UserError
import re

class QuickProductCreate(models.TransientModel):
    _name = 'quick.product.create'


    product_name = fields.Char(string="Product Name")
    product_id = fields.Char(string="Product ID")
    vendor = fields.Many2one('res.partner', domain="[('supplier', '=', True)]")
    vendor_product_link = fields.Char('Vendor Product Link')
    product_size = fields.Char('Product Size')
    min_qty = fields.Integer('Minimum Quantity')
    can_be_sample = fields.Boolean('Can be Sample')
    attribute_line_ids = fields.One2many('attribute.lines', 'attrib_line_id', 'Product Attributes')
    is_exist = fields.Boolean(default=False)
    product_exist = fields.Many2one('product.template')
    categ_id = fields.Many2one('product.category', string='Product Category')

    @api.onchange('product_id', 'vendor')
    def onchange_product(self):
        self.is_exist = False
        vals = self.env['product.supplierinfo'].search([('product_code', '=', self.product_id)])
        self.product_exist = False
        if vals and self.product_id:
            for val in vals:
                if  val.name.id == self.vendor.id and (not val.product_tmpl_id.channel_id or 'Pinnacle Promotions' in [channel.name for channel in val.product_tmpl_id.channel_id]):
                    self.is_exist = True
                    self.product_exist = val.product_tmpl_id.id
                    return

    @api.multi
    def redirect_to_product(self):
        if self.product_exist:
            return {
                        'name': 'Product Template',
                        'context': self.env.context,
                        'view_type': 'form',
                        'view_mode': 'form',
                        'res_model': 'product.template',
                        'type': 'ir.actions.act_window',
                        'target': 'current',
                        'res_id': self.product_exist.id,
                        }

    @api.multi
    def save_product(self, vals):
        if self.is_exist:
            return self.redirect_to_product()
        attributes = []
        all_rec = []
        for attrib in self.attribute_line_ids:
            attributes.append((0, 0, {
                                    'attribute_id': attrib.attribute_id.id,
                                    'value_ids': [(6, 0, attrib.value_ids.ids)],
                                     }))
        for tier in self.vendor.teir_assignment_ids:
            if tier.tier_sequence == 1:
                all_rec.append([0,0,({
                            'min_qty':self.min_qty,
                            'tier_sequence': tier.tier_sequence,
                            'vendor_tier_id': tier.id
                        })])
            else:
                all_rec.append([0,0,({
                            'tier_sequence': tier.tier_sequence,
                            'vendor_tier_id': tier.id
                            })])

        categ_search = self.env['res.partner.category'].search([('name', '=', 'Added via Wizard')], limit=1)
        categ_value = [(0, 0, {'name': 'Added via Wizard'})]
        if categ_search:
            categ_value = [(6, 0, categ_search.ids)]
        product = self.env['product.template'].create({
                'name': self.product_name,
                'sample_ok': self.can_be_sample,
                'attribute_line_ids': attributes,
                'inventory_control_code': 'ds',
                'category_id': categ_value,
                'product_sku': self.product_id,
                'categ_id': self.categ_id.id,
            })

        seller_dict = {
            'min_qty': self.min_qty,
            'name': self.vendor.id,
            'product_dimensions': self.product_size,
            'product_vendor_tier_ids': all_rec,
            'product_code': self.product_id,
            'product_tmpl_id': product.id,
            'product_name': self.product_name,
            'preferred_vendor': True,
        }

        seller = self.env['product.supplierinfo'].create(seller_dict)
        if self.vendor_product_link:
            token = re.compile(re.escape('{productcode}'), re.IGNORECASE)
            seller.vendor_product_link = token.sub(self.product_id, self.vendor_product_link)
        else:
            seller.onchange_product_code()
        product.create_set_variants_avialable()

class AttributeLines(models.TransientModel):
    _name = 'attribute.lines'

    attrib_line_id = fields.Many2one('quick.product.create')
    attribute_id = fields.Many2one('product.attribute', string="Attribute")
    value_ids = fields.Many2many('product.attribute.value', string='Attribute Values')
