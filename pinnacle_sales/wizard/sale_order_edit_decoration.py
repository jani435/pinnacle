# -*- coding: utf-8 -*-
from openerp import models, fields, api, _
from openerp.addons.pinnacle_sales.wizard.common import common
from openerp.exceptions import UserError
import base64
import urllib2

class SaleOrderEditDecoration(models.TransientModel):
    _name = 'sale.order.edit.decoration'

    name = fields.Char(string='Edit Decoration To')
    sale_order_edit_decoration_ids = fields.One2many(
        'sale.order.edit.decoration.line',
        'sale_order_edit_decoration_id',
        'Sale Order Add Decoration Lines',
        required=True) 
    is_decoration_disable = fields.Boolean(string="Blank Product", default=False)
    user_response_about_change_to_decoration = fields.Boolean('User Response About Change To Decoration', default=False)
    any_change_to_decoration_in_pms_code_or_stock_color = fields.Boolean('Any Change In Decoraion\'s PMS Code OR Stock Color ?', default=False)

    @api.onchange('is_decoration_disable')
    def _onchange_is_decoration_enable(self):
        if self.is_decoration_disable:
            self.sale_order_edit_decoration_ids = []

    @api.model
    def default_get(self, fields):
        res = super(SaleOrderEditDecoration,self).default_get(fields)
        name = False
        records, decorations, lines = [], [], []
        if self.env.context.get('manual_artworksheet') and self.env.context.get('active_ids', False):
            for art_line in self.env['art.production.line'].browse(self.env.context.get('active_ids', False)):
                decorations = art_line.decorations
                for decoration in decorations:
                    record = {'imprint_location':
                                  decoration.imprint_location and
                                  decoration.imprint_location.id or False,
                              'imprint_method':
                                  decoration.imprint_method and
                                  decoration.imprint_method.id or False,
                              'customer_art':
                                  decoration.customer_art and
                                  decoration.customer_art.id or False,
                              'finisher':
                                  decoration.finisher and
                                  decoration.finisher.id or False,
                              'pms_code':
                                  decoration.pms_code.ids and
                                  [[6, 0, decoration.pms_code.ids]] or False,
                              'stock_color':
                                  decoration.stock_color.ids and
                                  [[6, 0, decoration.stock_color.ids]] or False,
                              'vendor_decoration_location':
                                  decoration.vendor_decoration_location and
                                  decoration.vendor_decoration_location.id or
                                  False,
                              'vendor_decoration_method':
                                  decoration.vendor_decoration_method and
                                  decoration.vendor_decoration_method.id or
                                  False,
                              'vendor_prod_specific': decoration.vendor_prod_specific,
                              'already_existed_decoration_line': True,
                              'sale_order_line_decoration_id':
                                  decoration.id,
                              'pms_color_code': decoration.pms_color_code,
                              'stock_color_char': decoration.stock_color_char
                              }
                    records.append([0, 0, record])
        elif self.env.context.get('active_ids', False) and self.env.context.get('order_id', False):
            for sale_order_line in self.env['sale.order.line'].browse(self.env.context.get('active_ids', False)):
                decorations = sale_order_line.decorations
                for decoration in decorations:
                    record = {'imprint_location':
                                  decoration.imprint_location and
                                  decoration.imprint_location.id or False,
                              'imprint_method':
                                  decoration.imprint_method and
                                  decoration.imprint_method.id or False,
                              'customer_art':
                                  decoration.customer_art and
                                  decoration.customer_art.id or False,
                              'finisher':
                                  decoration.finisher and
                                  decoration.finisher.id or False,
                              'pms_code':
                                  decoration.pms_code.ids and
                                  [[6, 0, decoration.pms_code.ids]] or False,
                              'stock_color':
                                  decoration.stock_color.ids and
                                  [[6, 0, decoration.stock_color.ids]] or False,
                              'vendor_decoration_location':
                                  decoration.vendor_decoration_location and
                                  decoration.vendor_decoration_location.id or
                                  False,
                              'vendor_decoration_method':
                                  decoration.vendor_decoration_method and
                                  decoration.vendor_decoration_method.id or
                                  False,
                              'vendor_prod_specific': decoration.vendor_prod_specific,
                              'already_existed_decoration_line': True,
                              'sale_order_line_decoration_id':
                                  decoration.id,
                              'pms_color_code': decoration.pms_color_code,
                              'stock_color_char': decoration.stock_color_char,
                              'has_any_decoration_charge_or_any_shipping_line': decoration.sale_order_line_id and
                                  decoration.sale_order_line_id.has_any_decoration_charge_or_any_shipping_line or False,
                              'copy_pms_code':
                                  decoration.pms_code.ids and
                                  [[6, 0, decoration.pms_code.ids]] or False,
                              'copy_stock_color':
                                  decoration.stock_color.ids and
                                  [[6, 0, decoration.stock_color.ids]] or False,
                              }
                    records.append([0, 0, record])
        if self.env.context.get('manual_artworksheet') and self.env.context.get('active_ids', False) and not name:
            for art_line in self.env['art.production.line'].browse(self.env.context.get('active_ids', False)):
                if name:
                    if art_line.product_sku and art_line.description and art_line.product_variant_name:
                        name = name + ' <br/> ' + art_line.product_sku + " - " + art_line.description + " - " + art_line.product_variant_name
                    elif art_line.product_sku and art_line.description and (not art_line.product_variant_name):
                        name = name + ' <br/> ' + art_line.product_sku + " - " + art_line.description
                    elif art_line.product_sku and (not art_line.description) and art_line.product_variant_name:
                        name = name + ' <br/> ' + art_line.product_sku + " - " + art_line.product_variant_name
                    elif (not art_line.product_sku) and art_line.description and art_line.product_variant_name:
                        name = name + ' <br/> ' + art_line.description + " - " + art_line.product_variant_name
                    elif (not art_line.product_sku) and (not art_line.description) and art_line.product_variant_name:
                        name = name + ' <br/> ' + art_line.product_variant_name
                    elif (not art_line.product_sku) and art_line.description and (not art_line.product_variant_name):
                        name = name + ' <br/> ' + art_line.description
                    elif art_line.product_sku and (not art_line.description) and (not art_line.product_variant_name):
                        name = name + ' <br/> ' + art_line.product_sku
                    else:
                        pass
                else:
                    if art_line.product_sku and art_line.description and art_line.product_variant_name:
                        name = art_line.product_sku + " - " + art_line.description + " - " + art_line.product_variant_name
                    elif art_line.product_sku and art_line.description and (not art_line.product_variant_name):
                        name = art_line.product_sku + " - " + art_line.description
                    elif art_line.product_sku and (not art_line.description) and art_line.product_variant_name:
                        name = art_line.product_sku + " - " + art_line.product_variant_name
                    elif (not art_line.product_sku) and art_line.description and art_line.product_variant_name:
                        name = art_line.description + " - " + art_line.product_variant_name
                    elif (not art_line.product_sku) and (not art_line.description) and art_line.product_variant_name:
                        name = art_line.product_variant_name
                    elif (not art_line.product_sku) and art_line.description and (not art_line.product_variant_name):
                        name = art_line.description
                    elif art_line.product_sku and (not art_line.description) and (not art_line.product_variant_name):
                        name = art_line.product_sku
                    else:
                        pass
        elif self.env.context.get('active_ids', False) and self.env.context.get('order_id', False) and not name:
            for sale_order_line in self.env['sale.order.line'].browse(self.env.context.get('active_ids', False)):
                if name:
                    if sale_order_line.product_sku and sale_order_line.name and sale_order_line.product_variant_name:
                        name = name + ' <br/> ' + sale_order_line.product_sku + " - " + sale_order_line.name + " - " + sale_order_line.product_variant_name
                    elif sale_order_line.product_sku and sale_order_line.name  and (not sale_order_line.product_variant_name):
                        name = name + ' <br/> ' + sale_order_line.product_sku + " - " + sale_order_line.name
                    elif sale_order_line.product_sku and (not sale_order_line.name) and sale_order_line.product_variant_name:
                        name = name + ' <br/> ' + sale_order_line.product_sku + " - " + sale_order_line.product_variant_name
                    elif (not sale_order_line.product_sku) and sale_order_line.name and sale_order_line.product_variant_name:
                        name = name + ' <br/> ' + sale_order_line.name + " - " + sale_order_line.product_variant_name
                    elif (not sale_order_line.product_sku) and (not sale_order_line.name) and sale_order_line.product_variant_name:
                        name = name + ' <br/> ' + sale_order_line.product_variant_name
                    elif (not sale_order_line.product_sku) and sale_order_line.name and (not sale_order_line.product_variant_name):
                        name = name + ' <br/> ' + sale_order_line.name
                    elif sale_order_line.product_sku and (not sale_order_line.name) and (not sale_order_line.product_variant_name):
                        name = name + ' <br/> ' + sale_order_line.product_sku
                    else:
                        pass
                else:
                    if sale_order_line.product_sku and sale_order_line.name and sale_order_line.product_variant_name:
                        name = sale_order_line.product_sku + " - " + sale_order_line.name + " - " + sale_order_line.product_variant_name
                    elif sale_order_line.product_sku and sale_order_line.name and (not sale_order_line.product_variant_name):
                        name = sale_order_line.product_sku + " - " + sale_order_line.name
                    elif sale_order_line.product_sku and (not sale_order_line.name) and sale_order_line.product_variant_name:
                        name = sale_order_line.product_sku + " - " + sale_order_line.product_variant_name
                    elif (not sale_order_line.product_sku) and sale_order_line.name and sale_order_line.product_variant_name:
                        name = sale_order_line.name + " - " + sale_order_line.product_variant_name
                    elif (not sale_order_line.product_sku) and (not sale_order_line.name) and sale_order_line.product_variant_name:
                        name = sale_order_line.product_variant_name
                    elif (not sale_order_line.product_sku) and sale_order_line.name and (not sale_order_line.product_variant_name):
                        name = sale_order_line.name
                    elif sale_order_line.product_sku and (not sale_order_line.name) and (not sale_order_line.product_variant_name):
                        name = sale_order_line.product_sku
                    else:
                        pass
        res.update({'name':name, 'sale_order_edit_decoration_ids': records, 'is_decoration_disable': self.env.context.get('is_decoration_disable', False), 'user_response_about_change_to_decoration': False, 'any_change_to_decoration_in_pms_code_or_stock_color': False})
        return res

    @api.multi
    def edit_sale_order_line_decoration(self):
        if self.env['sale.order'].browse(self.env.context.get('order_id', False)):
            for sale_order_line in self.env['sale.order'].browse(self.env.context.get('order_id', False)).order_line:
                sale_order_line.select_decoration_line = False
        imprint_obj = self.env['product.imprint']
        for sale_order_edit_decoration_line_id in self.sale_order_edit_decoration_ids:
            if sale_order_edit_decoration_line_id.imprint_location and sale_order_edit_decoration_line_id.imprint_method and sale_order_edit_decoration_line_id.finisher and (self.env['sale.order.line'].browse(self.env.context.get('active_ids', False))[0]):
                finisher_decorations = (self.env['sale.order.line'].browse(self.env.context.get('active_ids', False))[0]).product_id.finisher_ids.search(['&',('product_id','=',(self.env['sale.order.line'].browse(self.env.context.get('active_ids', False))[0]).product_id.product_tmpl_id.id),('finisher_id','=',sale_order_edit_decoration_line_id.finisher.id)])
                if finisher_decorations:
                    finisher_decoration = finisher_decorations[0]
                    finisher_finishing_types = finisher_decoration.product_imprint_ids.search(['&',('product_finisher_id','=',finisher_decoration.id),'&',('decoration_location','=',sale_order_edit_decoration_line_id.imprint_location.id),('decoration_method','=',sale_order_edit_decoration_line_id.imprint_method.id)])
                    if finisher_finishing_types: 
                        finisher_finishing_type = finisher_finishing_types[0]
                        if (len(sale_order_edit_decoration_line_id.stock_color.ids) + len(sale_order_edit_decoration_line_id.pms_code.ids))> finisher_finishing_type.color_limit and finisher_finishing_type.color_limit != 0:
                            raise UserError(_('You can only enter total '+str(finisher_finishing_type.color_limit)+' colors for \n\n - Finisher : '+sale_order_edit_decoration_line_id.finisher.name+'\n - Decoration Location : '+sale_order_edit_decoration_line_id.imprint_location.name+ '\n - Decoration Method : '+sale_order_edit_decoration_line_id.imprint_method.name ))
        if self.env.context.get('active_id', False):
            sale_order_line = self.env['sale.order.line'].browse(self.env.context.get('active_id', False))
            to_be_deleted = []
            finisher_id = False
            sale_order_line.is_decoration_disable = self.is_decoration_disable
            if self.is_decoration_disable:
                self.env['shipping.line'].search([('decoration_line_id', 'in', sale_order_line.decorations.ids)]).unlink()
                sale_order_line.decorations.unlink()

            for decoration in sale_order_line.decorations:
                delete = True
                for sale_order_edit_decoration_line_id in self.sale_order_edit_decoration_ids:
                    if decoration.id == sale_order_edit_decoration_line_id.sale_order_line_decoration_id.id and sale_order_edit_decoration_line_id.already_existed_decoration_line:
                        delete = False
                if delete:
                    to_be_deleted.append(decoration.id)
            art_obj = self.env['art.production']
            art_line_obj = self.env['art.production.line']
            if not self.is_decoration_disable:
                if self.any_change_to_decoration_in_pms_code_or_stock_color:
                    if self.user_response_about_change_to_decoration:
                        self.env['shipping.line'].search([('shipping_sale_orders', 'in', sale_order_line.id)]).unlink()
                else:
                    self.env['shipping.line'].search([('shipping_sale_orders', 'in', sale_order_line.id)]).unlink()
                self.env['sale.order.line.decoration'].browse(to_be_deleted).unlink()
                if not sale_order_line.art_line_id.decorations:
                    sale_order_line.art_line_id.unlink()
                    if not sale_order_line.art_id.art_lines:
                        sale_order_line.art_id.unlink()
                for sale_order_edit_decoration_line_id in self.sale_order_edit_decoration_ids:
                    domain = [('decoration_method', '=', sale_order_edit_decoration_line_id.imprint_method.id),
                              ('decoration_location', '=', sale_order_edit_decoration_line_id.imprint_location.id),
                              ('product_finisher_id.finisher_id', '=', sale_order_edit_decoration_line_id.finisher and
                               sale_order_edit_decoration_line_id.finisher.id or False),
                              ('product_finisher_id.product_id', '=', sale_order_line.product_id.product_tmpl_id.id)]
                    imprint_rec = imprint_obj.search(domain, limit=1)
                    point_size = imprint_rec and imprint_rec.point_size or '0'
                    imprint_area = '0Wx0Hx0D'
                    if imprint_rec:
                        imprint_area = imprint_rec.imprint_area
                    if not sale_order_edit_decoration_line_id.already_existed_decoration_line:
                        self.env['sale.order.line.decoration'].create(
                            {'imprint_location':
                             sale_order_edit_decoration_line_id.imprint_location and
                             sale_order_edit_decoration_line_id.imprint_location.id or False,
                             'imprint_method':
                             sale_order_edit_decoration_line_id.
                             imprint_method and
                             sale_order_edit_decoration_line_id.
                             imprint_method.id or
                             False,
                             'customer_art':
                             sale_order_edit_decoration_line_id.
                             customer_art and
                             sale_order_edit_decoration_line_id.
                             customer_art.id or
                             False,
                             'finisher':
                             sale_order_edit_decoration_line_id.
                             finisher and
                             sale_order_edit_decoration_line_id.
                             finisher.id or
                             False,
                             'pms_code':
                             sale_order_edit_decoration_line_id.
                             pms_code.ids and
                             [[6, 0, sale_order_edit_decoration_line_id.
                               pms_code.ids]] or False,
                             'stock_color':
                             sale_order_edit_decoration_line_id.
                             stock_color.ids and
                             [[6, 0, sale_order_edit_decoration_line_id.
                               stock_color.ids]] or False,
                             'vendor_decoration_location':
                             sale_order_edit_decoration_line_id.vendor_decoration_location and
                             sale_order_edit_decoration_line_id.vendor_decoration_location.id or
                             False,
                             'vendor_decoration_method':
                             sale_order_edit_decoration_line_id.vendor_decoration_method and
                             sale_order_edit_decoration_line_id.vendor_decoration_method.id or
                             False,
                             'vendor_prod_specific': sale_order_edit_decoration_line_id.vendor_prod_specific,
                             'sale_order_line_id': sale_order_line.id,
                             'art_id': sale_order_line.art_id and sale_order_line.art_id.id or False,
                             'art_line_id': sale_order_line.art_line_id and sale_order_line.art_line_id.id or False,
                             'product_id': sale_order_line.product_id and sale_order_line.product_id.id or False,
                             'min_pt_size': point_size,
                             'area': imprint_area,
                             'pms_color_code': sale_order_edit_decoration_line_id.pms_color_code,
                             'stock_color_char': sale_order_edit_decoration_line_id.stock_color_char})
                        finisher_id = sale_order_edit_decoration_line_id.finisher and sale_order_edit_decoration_line_id.finisher.id or False
                    else:
                        if sale_order_edit_decoration_line_id.sale_order_line_decoration_id:
                            art_wroksheets_to_be_archived = []
                            decorations_ids_to_be_updated_for_artworksheet = []
                            old_finisher_id = sale_order_edit_decoration_line_id.sale_order_line_decoration_id.finisher and sale_order_edit_decoration_line_id.sale_order_line_decoration_id.finisher.id or False
                            new_finisher_id = sale_order_edit_decoration_line_id.finisher and sale_order_edit_decoration_line_id.finisher.id or False
                            old_decoration_line = self.env['sale.order.line.decoration'].browse([sale_order_edit_decoration_line_id.sale_order_line_decoration_id.id])
                            if old_decoration_line:
                                if old_decoration_line.imprint_location != sale_order_edit_decoration_line_id.imprint_location or old_decoration_line.imprint_method != sale_order_edit_decoration_line_id.imprint_method or old_decoration_line.finisher != sale_order_edit_decoration_line_id.finisher or old_decoration_line.stock_color.ids != sale_order_edit_decoration_line_id.stock_color.ids:
                                    old_decoration_line.sale_order_line_id.decoration_group_sequences = False
                            if sale_order_line.artworksheet_test_for_checking_submitted():
                                if old_finisher_id != new_finisher_id:
                                    if old_decoration_line and old_decoration_line.art_id:
                                        art_wroksheets_to_be_archived.append(old_decoration_line.art_id.id)
                                        for art_line in old_decoration_line.art_id.art_lines:
                                            for decoration in art_line.decorations:
                                                if decoration.id != old_decoration_line.id:
                                                    decorations_ids_to_be_updated_for_artworksheet.append(decoration.id)
                            if art_wroksheets_to_be_archived:
                                sale_order_line.copying_data_of_art_wroksheets_to_be_archived(art_wroksheets_to_be_archived)
                            if art_wroksheets_to_be_archived:
                                sale_order_line.actual_archiving_of_artworksheets_with_handling_of_other_data(art_wroksheets_to_be_archived)
                                if old_decoration_line:
                                    old_decoration_line.write({'art_id': False ,'art_line_id': False})
                            self.env['sale.order.line.decoration'].with_context(user_response_about_change_to_decoration=self.user_response_about_change_to_decoration).browse(
                                [sale_order_edit_decoration_line_id.
                                 sale_order_line_decoration_id.id]).write({
                                     'imprint_location':
                                     sale_order_edit_decoration_line_id.
                                     imprint_location and
                                     sale_order_edit_decoration_line_id.
                                     imprint_location.id or
                                     False,
                                     'imprint_method':
                                     sale_order_edit_decoration_line_id.
                                     imprint_method and
                                     sale_order_edit_decoration_line_id.
                                     imprint_method.id or
                                     False,
                                     'customer_art':
                                     sale_order_edit_decoration_line_id.
                                     customer_art and
                                     sale_order_edit_decoration_line_id.
                                     customer_art.id or
                                     False,
                                     'finisher':
                                     sale_order_edit_decoration_line_id.
                                     finisher and
                                     sale_order_edit_decoration_line_id.
                                     finisher.id or
                                     False,
                                     'pms_code':
                                     sale_order_edit_decoration_line_id.
                                     pms_code.ids and
                                     [[6, 0, sale_order_edit_decoration_line_id.
                                       pms_code.ids]] or [[6, 0, []]],
                                     'stock_color':
                                     sale_order_edit_decoration_line_id.
                                     stock_color.ids and
                                     [[6, 0, sale_order_edit_decoration_line_id.
                                       stock_color.ids]] or [[6, 0, []]],
                                     'vendor_decoration_location':
                                     sale_order_edit_decoration_line_id.vendor_decoration_location and
                                     sale_order_edit_decoration_line_id.vendor_decoration_location.id or
                                     False,
                                     'vendor_decoration_method':
                                     sale_order_edit_decoration_line_id.vendor_decoration_method and
                                     sale_order_edit_decoration_line_id.vendor_decoration_method.id or
                                     False,
                                     'vendor_prod_specific': sale_order_edit_decoration_line_id.vendor_prod_specific,
                                     'pms_color_code': sale_order_edit_decoration_line_id.pms_color_code,
                                     'stock_color_char': sale_order_edit_decoration_line_id.stock_color_char})
                            imprint_area_and_min_pt_size_to_be_written = False
                            if self.any_change_to_decoration_in_pms_code_or_stock_color:
                                if self.user_response_about_change_to_decoration:
                                    imprint_area_and_min_pt_size_to_be_written = True
                            else:
                                imprint_area_and_min_pt_size_to_be_written = True
                            if imprint_area_and_min_pt_size_to_be_written:
                                self.env['sale.order.line.decoration'].browse([sale_order_edit_decoration_line_id.sale_order_line_decoration_id.id]).write({'area': imprint_area, 'min_pt_size': point_size})
                            if decorations_ids_to_be_updated_for_artworksheet:
                                sale_order_line.updating_other_decorations_of_archiving_artworksheets(decorations_ids_to_be_updated_for_artworksheet)

    @api.multi
    def edit_art_production_line_decoration(self):
        art_id = self.env['art.production'].browse(self.env.context.get('art_id', False))
        if art_id:
            for art_line in self.env['art.production'].browse(self.env.context.get('art_id', False)).art_lines:
                art_line.select_art_line = False
        imprint_obj = self.env['product.imprint']
        for sale_order_edit_decoration_line_id in self.sale_order_edit_decoration_ids:
            if sale_order_edit_decoration_line_id.imprint_location and sale_order_edit_decoration_line_id.imprint_method and sale_order_edit_decoration_line_id.finisher and (self.env['art.production.line'].browse(self.env.context.get('active_ids', False))[0]):
                finisher_decorations = (self.env['art.production.line'].browse(self.env.context.get('active_ids', False))[0]).n_product_id.finisher_ids.search(['&',('product_id','=',(self.env['art.production.line'].browse(self.env.context.get('active_ids', False))[0]).n_product_id.product_tmpl_id.id),('finisher_id','=',sale_order_edit_decoration_line_id.finisher.id)])
                if finisher_decorations:
                    finisher_decoration = finisher_decorations[0]
                    finisher_finishing_types = finisher_decoration.product_imprint_ids.search(['&',('product_finisher_id','=',finisher_decoration.id),'&',('decoration_location','=',sale_order_edit_decoration_line_id.imprint_location.id),('decoration_method','=',sale_order_edit_decoration_line_id.imprint_method.id)])
                    if finisher_finishing_types:
                        finisher_finishing_type = finisher_finishing_types[0]
                        if (len(sale_order_edit_decoration_line_id.stock_color.ids) + len(sale_order_edit_decoration_line_id.pms_code.ids))> finisher_finishing_type.color_limit and finisher_finishing_type.color_limit != 0:
                            raise UserError(_('You can only enter total '+str(finisher_finishing_type.color_limit)+' colors for \n\n - Finisher : '+sale_order_edit_decoration_line_id.finisher.name+'\n - Decoration Location : '+sale_order_edit_decoration_line_id.imprint_location.name+ '\n - Decoration Method : '+sale_order_edit_decoration_line_id.imprint_method.name ))
        if self.env.context.get('active_id', False):
            art_line = self.env['art.production.line'].browse(self.env.context.get('active_id', False))
            to_be_deleted = []
            finisher_id = False
            # if self.is_decoration_disable:
            #     sale_order_line.is_decoration_disable = self.is_decoration_disable
            #     sale_order_line.decorations.unlink()
            for decoration in art_line.decorations:
                delete = True
                for sale_order_edit_decoration_line_id in self.sale_order_edit_decoration_ids:
                    if decoration.id == sale_order_edit_decoration_line_id.sale_order_line_decoration_id.id and sale_order_edit_decoration_line_id.already_existed_decoration_line:
                        delete = False
                if delete:
                    to_be_deleted.append(decoration.id)
            art_obj = self.env['art.production']
            art_line_obj = self.env['art.production.line']
            if not self.is_decoration_disable:
                self.env['sale.order.line.decoration'].browse(to_be_deleted).unlink()
                for sale_order_edit_decoration_line_id in self.sale_order_edit_decoration_ids:
                    domain = [('decoration_method', '=', sale_order_edit_decoration_line_id.imprint_method.id),
                              ('decoration_location', '=', sale_order_edit_decoration_line_id.imprint_location.id),
                              ('product_finisher_id.finisher_id', '=', sale_order_edit_decoration_line_id.finisher and
                               sale_order_edit_decoration_line_id.finisher.id or False),
                              ('product_finisher_id.product_id', '=', art_line.n_product_id.product_tmpl_id.id)]
                    imprint_rec = imprint_obj.search(domain, limit=1)
                    point_size = imprint_rec and imprint_rec.point_size or '0'
                    imprint_area = '0Wx0Hx0D'
                    if imprint_rec:
                        imprint_area = imprint_rec.imprint_area
                    if not sale_order_edit_decoration_line_id.already_existed_decoration_line:
                        finisher = sale_order_edit_decoration_line_id.finisher and sale_order_edit_decoration_line_id.finisher.id or False
                        decoration = self.env['sale.order.line.decoration'].create(
                            {'imprint_location':
                             sale_order_edit_decoration_line_id.imprint_location and
                             sale_order_edit_decoration_line_id.imprint_location.id or False,
                             'imprint_method':
                             sale_order_edit_decoration_line_id.
                             imprint_method and
                             sale_order_edit_decoration_line_id.
                             imprint_method.id or
                             False,
                             'customer_art':
                             sale_order_edit_decoration_line_id.
                             customer_art and
                             sale_order_edit_decoration_line_id.
                             customer_art.id or
                             False,
                             'finisher': finisher,
                             'pms_code':
                             sale_order_edit_decoration_line_id.
                             pms_code.ids and
                             [[6, 0, sale_order_edit_decoration_line_id.
                               pms_code.ids]] or False,
                             'stock_color':
                             sale_order_edit_decoration_line_id.
                             stock_color.ids and
                             [[6, 0, sale_order_edit_decoration_line_id.
                               stock_color.ids]] or False,
                             'vendor_decoration_location':
                             sale_order_edit_decoration_line_id.vendor_decoration_location and
                             sale_order_edit_decoration_line_id.vendor_decoration_location.id or
                             False,
                             'vendor_decoration_method':
                             sale_order_edit_decoration_line_id.vendor_decoration_method and
                             sale_order_edit_decoration_line_id.vendor_decoration_method.id or
                             False,
                             'vendor_prod_specific': sale_order_edit_decoration_line_id.vendor_prod_specific,
                             'art_id': art_id and art_id.id or False,
                             'art_line_id': art_line and art_line.id or False,
                             'product_id': art_line.n_product_id and art_line.n_product_id.id or False,
                             'min_pt_size': point_size,
                             'area': imprint_area,
                             'pms_color_code': sale_order_edit_decoration_line_id.pms_color_code,
                             'stock_color_char': sale_order_edit_decoration_line_id.stock_color_char})
                        art_id.create_decoration_group(decoration.id)
                    else:
                        if sale_order_edit_decoration_line_id.sale_order_line_decoration_id:
                            old_finisher_id = sale_order_edit_decoration_line_id.sale_order_line_decoration_id.finisher and sale_order_edit_decoration_line_id.sale_order_line_decoration_id.finisher.id or False
                            new_finisher_id = sale_order_edit_decoration_line_id.finisher and sale_order_edit_decoration_line_id.finisher.id or False
                            old_decoration_line = self.env['sale.order.line.decoration'].browse([sale_order_edit_decoration_line_id.sale_order_line_decoration_id.id])
                            if old_decoration_line:
                                if old_decoration_line.imprint_location != sale_order_edit_decoration_line_id.imprint_location or old_decoration_line.imprint_method != sale_order_edit_decoration_line_id.imprint_method or old_decoration_line.finisher != sale_order_edit_decoration_line_id.finisher or old_decoration_line.stock_color.ids != sale_order_edit_decoration_line_id.stock_color.ids:
                                    old_decoration_line.sale_order_line_id.decoration_group_sequences = False
                            self.env['sale.order.line.decoration'].browse(
                                [sale_order_edit_decoration_line_id.
                                 sale_order_line_decoration_id.id]).write({
                                     'imprint_location':
                                     sale_order_edit_decoration_line_id.
                                     imprint_location and
                                     sale_order_edit_decoration_line_id.
                                     imprint_location.id or
                                     False,
                                     'imprint_method':
                                     sale_order_edit_decoration_line_id.
                                     imprint_method and
                                     sale_order_edit_decoration_line_id.
                                     imprint_method.id or
                                     False,
                                     'customer_art':
                                     sale_order_edit_decoration_line_id.
                                     customer_art and
                                     sale_order_edit_decoration_line_id.
                                     customer_art.id or
                                     False,
                                     'finisher':
                                     sale_order_edit_decoration_line_id.
                                     finisher and
                                     sale_order_edit_decoration_line_id.
                                     finisher.id or
                                     False,
                                     'pms_code':
                                     sale_order_edit_decoration_line_id.
                                     pms_code.ids and
                                     [[6, 0, sale_order_edit_decoration_line_id.
                                       pms_code.ids]] or [[6, 0, []]],
                                     'stock_color':
                                     sale_order_edit_decoration_line_id.
                                     stock_color.ids and
                                     [[6, 0, sale_order_edit_decoration_line_id.
                                       stock_color.ids]] or [[6, 0, []]],
                                     'vendor_decoration_location':
                                     sale_order_edit_decoration_line_id.vendor_decoration_location and
                                     sale_order_edit_decoration_line_id.vendor_decoration_location.id or
                                     False,
                                     'vendor_decoration_method':
                                     sale_order_edit_decoration_line_id.vendor_decoration_method and
                                     sale_order_edit_decoration_line_id.vendor_decoration_method.id or
                                     False,
                                     'vendor_prod_specific': sale_order_edit_decoration_line_id.vendor_prod_specific,
                                     'min_pt_size': point_size,
                                     'area': imprint_area,
                                     'pms_color_code': sale_order_edit_decoration_line_id.pms_color_code,
                                     'stock_color_char': sale_order_edit_decoration_line_id.stock_color_char})
                            if not sale_order_edit_decoration_line_id.sale_order_line_decoration_id.decoration_group_id:
                                art_id.create_decoration_group(sale_order_edit_decoration_line_id.sale_order_line_decoration_id.id)
                art_files_extensions = []
                for decoration in art_line.decorations:
                    try:
                        if decoration.customer_art and decoration.customer_art.art_filename:
                            filename = decoration.customer_art.art_filename.split(".")[-1].lower()
                            art_files_extensions.append(str(filename))
                    except:
                        pass
                art_type = ''
                for file_type in art_files_extensions:
                    if file_type not in ["eps", "ai"]:
                        art_type = 'raster_recreate'
                    break
                art_id.write({'art_type': art_type})

class SaleOrderEditDecorationLine(models.TransientModel):
    _name = 'sale.order.edit.decoration.line'

    @api.multi
    @api.depends('stock_color')
    def _get_pms_color_code(self):
        for this in self:
            pms_color_code = common().compute_pms_color_code(this)
            this.pms_color_code = pms_color_code

    @api.multi
    @api.depends('stock_color')
    def _get_stock_color(self):
        for rec in self:
            stock_color = []
            for stk in rec.stock_color:
                stock_color.append(stk.name)
            rec.stock_color_char = ", ".join(stock_color)

    @api.model
    def _default_finisher(self):
        return common().get_default_value_for_finisher_field(self)
        
    sale_order_edit_decoration_id = fields.Many2one(
        'sale.order.edit.decoration',
        string='Sale Order Edit Decoration',
        ondelete="cascade")
    sale_order_line_decoration_id = fields.Many2one(
        'sale.order.line.decoration',
        string='Decoration Line Number')
    imprint_location = fields.Many2one(
        'decoration.location', string="Imprint Location", domain=lambda self: self._domain_filter_decoration('imprint_location'))
    imprint_method = fields.Many2one(
        'decoration.method', string="Imprint Method", domain=lambda self: self._domain_filter_decoration('imprint_method'))
    customer_art = fields.Many2one(
        'product.decoration.customer.art', string="Art (file or text)", domain=lambda self: self._domain_filter_decoration('customer_art'))
    finisher = fields.Many2one(
        'res.partner', string="Finisher", domain=lambda self: self._domain_filter_decoration('finisher'), default=_default_finisher)
    pms_code = fields.Many2many(
        'product.decoration.pms.code',
        relation="pms_code_edit_decoration_line_rel",
        column1="sale_order_edit_decoration_line_id",
        column2="product_decoration_pms_code_id",
        string="PMS Code")
    pms_color_code = fields.Char(compute="_get_pms_color_code")
    stock_color_char = fields.Char(compute='_get_stock_color')
    stock_color = fields.Many2many(
        'product.decoration.stock.color',
        relation="stock_color_edit_decoration_line_rel",
        column1="sale_order_edit_decoration_line_id",
        column2="product_decoration_stock_color_id",
        string="Stock Color")
    customer_arts = fields.Many2many(
        'product.decoration.customer.art', compute="_compute_customer_arts")
    already_existed_decoration_line = fields.Boolean(default=False)

    global_search = fields.Boolean(
        'All options',
        help='Global Search On Location, Method, Finisher',
        default=False)
    vendor_decoration_location = fields.Many2one('vendor.decoration.location', 'Vendor Imprint Location')
    vendor_decoration_method = fields.Many2one('vendor.decoration.method', 'Vendor Imprint Method')
    vendor_prod_specific = fields.Char('Vendor Method Product Specific')
    image_preview = fields.Binary(
        compute="_compute_image_preview", string='Image Preview')
    preview_text = fields.Html(
        compute="_compute_preview_text", string="Text Preview", store=True)
    has_any_decoration_charge_or_any_shipping_line = fields.Boolean(string="Has Any Decoration Charge Or Shipping Line ?", default=False)
    copy_pms_code = fields.Many2many(
        'product.decoration.pms.code',
        relation="copy_pms_code_edit_decoration_line_rel",
        column1="sale_order_edit_decoration_line_id",
        column2="product_decoration_pms_code_id",
        string="PMS Code Copy")
    copy_stock_color = fields.Many2many(
        'product.decoration.stock.color',
        relation="copy_stock_color_edit_decoration_line_rel",
        column1="sale_order_edit_decoration_line_id",
        column2="product_decoration_stock_color_id",
        string="Stock Color Copy")

    @api.one
    @api.depends('customer_art', 'customer_art.name_url')
    def _compute_image_preview(self):
        self.image_preview = False
        if self.customer_art and self.customer_art.name_url:
            cfs_url = str(self.env['ir.config_parameter'].get_param('cfs.web.url'))
            final_url = cfs_url + self.customer_art.name_url
            url = common().url_fix(final_url)
            try:
                self.image_preview = base64.encodestring(urllib2.urlopen(url).read())
            except Exception as e:
                self.image_preview = ''


    @api.one
    @api.depends('customer_art', 'customer_art.text', 'customer_art.font')
    def _compute_preview_text(self):
        if self.customer_art and self.customer_art.font and self.customer_art.text:
            self.preview_text = "<font style=\"font-family: '" + self.customer_art.font.name + "'\"><h2>" + self.customer_art.text + "</h2></font>"
            
    @api.onchange('customer_art')
    def _onchange_customer_art(self):
        new_context = dict(self.env.context).copy()
        new_context.update({'model': 'sale.order.edit.decoration'})
        self.env.context = new_context
        customer_arts = common().onchange_customer_art(self) 
        return {'domain': {
            'customer_art': [('id', 'in', customer_arts)],
        }}
        
    @api.model
    def _domain_filter_decoration(self, field=False):
        if field:
            res = self._onchange_imprint_method()
            if field == 'customer_art':
                res = self._onchange_customer_art()
            return res and res['domain'][field] or False

    @api.multi
    def _onchange_imprint_method_computation(self):
        imprint_methods = []
        imprint_locations = []
        vendor_imprint_methods = []
        vendor_imprint_locations = []
        finishers = []
        other_finishers = []
        if self.env.context.get('sale_order_lines', False):
            for sale_order_line in self.env['sale.order.line'].browse(self.env.context.get('sale_order_lines', False)):
                for finisher in sale_order_line.product_id.finisher_ids:
                    # 12305 : When edit decoration, not all options show
                    # Start Of More than 1 decoration for variant should use same finisher
                    # same_finisher = True
                    # same_finisher_ids = []
                    # if 'sale_order_edit_decoration_ids' in self.env.context:
                    #     if len(self.env.context['sale_order_edit_decoration_ids']) >= 1 and not self.finisher:
                    #         for sale_order_edit_decoration_id in self.env.context['sale_order_edit_decoration_ids']:
                    #             if sale_order_edit_decoration_id[0] == 0:
                    #                 if sale_order_edit_decoration_id[2]['finisher']:
                    #                     same_finisher_ids.append(sale_order_edit_decoration_id[2]['finisher'])
                    #             if sale_order_edit_decoration_id[0] == 4:
                    #                 if self.env['sale.order.edit.decoration.line'].browse(sale_order_edit_decoration_id[1]):
                    #                     if self.env['sale.order.edit.decoration.line'].browse(sale_order_edit_decoration_id[1])[0].finisher:
                    #                         same_finisher_ids.append(self.env['sale.order.edit.decoration.line'].browse(sale_order_edit_decoration_id[1])[0].finisher.id)
                    #                     else:
                    #                         same_finisher_ids.append(False)
                    #             if sale_order_edit_decoration_id[0] == 1:
                    #                 if 'finisher' in sale_order_edit_decoration_id[2]:
                    #                     same_finisher_ids.append(sale_order_edit_decoration_id[2]['finisher'])
                    #                 else:
                    #                     if self.env['sale.order.edit.decoration.line'].browse(sale_order_edit_decoration_id[1]):
                    #                         if self.env['sale.order.edit.decoration.line'].browse(sale_order_edit_decoration_id[1])[0].finisher:
                    #                             same_finisher_ids.append(self.env['sale.order.edit.decoration.line'].browse(sale_order_edit_decoration_id[1])[0].finisher.id)
                    #                         else:
                    #                             same_finisher_ids.append(False)
                    #     if same_finisher_ids:
                    #         for same_finisher_id in same_finisher_ids:
                    #             if same_finisher_id != same_finisher_ids[0]:
                    #                 same_finisher = False
                    #                 break
                    #     else:
                    #         same_finisher = False
                    # if same_finisher:
                    #     if same_finisher_ids:
                    #         if finisher.finisher_id.id != same_finisher_ids[0]:
                    #             continue
                    # End Of More than 1 decoration for variant should use same finisher
                    for product_imprint_id in finisher.product_imprint_ids:
                        if not self.imprint_location and not self.finisher:
                            imprint_methods.append(
                                product_imprint_id.decoration_method.id)
                        elif self.imprint_location and not self.finisher:
                            if self.imprint_location.id == product_imprint_id.decoration_location.id:
                                imprint_methods.append(product_imprint_id.decoration_method.id)
                        elif not self.imprint_location and self.finisher:
                            if self.finisher.id == finisher.finisher_id.id:
                                imprint_methods.append(product_imprint_id.decoration_method.id)
                        else:
                            if self.imprint_location.id == product_imprint_id.decoration_location.id and self.finisher.id == finisher.finisher_id.id:
                                imprint_methods.append(product_imprint_id.decoration_method.id)
                        if not self.imprint_method and not self.finisher:
                            imprint_locations.append(product_imprint_id.decoration_location.id)
                        elif self.imprint_method and not self.finisher:
                            if self.imprint_method.id == product_imprint_id.decoration_method.id:
                                imprint_locations.append(product_imprint_id.decoration_location.id)
                        elif not self.imprint_method and self.finisher:
                            if self.finisher.id == finisher.finisher_id.id:
                                imprint_locations.append(product_imprint_id.decoration_location.id)
                        else:
                            if self.imprint_method.id == product_imprint_id.decoration_method.id and self.finisher.id == finisher.finisher_id.id:
                                imprint_locations.append(product_imprint_id.decoration_location.id)
                        if not self.imprint_method and not self.imprint_location:
                            finishers.append(finisher.finisher_id.id)
                        elif self.imprint_method and not self.imprint_location:
                            if self.imprint_method.id == product_imprint_id.decoration_method.id:
                                finishers.append(finisher.finisher_id.id)
                        elif not self.imprint_method and self.imprint_location:
                            if self.imprint_location.id ==product_imprint_id.decoration_location.id:
                                finishers.append(finisher.finisher_id.id)
                        else:
                            if self.imprint_method.id == product_imprint_id.decoration_method.id and self.imprint_location.id == product_imprint_id.decoration_location.id:
                                finishers.append(finisher.finisher_id.id)
                break
            imprint_methods = list(set(imprint_methods))
            imprint_locations = list(set(imprint_locations))
            finishers = list(set(finishers))
            # Fetching Data From Finsiher (res.partner)
            vendor_imprint_methods, vendor_imprint_locations, other_finishers = common().fetching_vendor_locations_and_methods(self)
            imprint_methods, imprint_locations, finishers = common()._onchange_imprint_method_extra(self, imprint_methods, imprint_locations, finishers, vendor_imprint_methods, vendor_imprint_locations, other_finishers)
            vendor_imprint_methods, vendor_imprint_locations, other_finishers = common()._onchange_vendor_imprint_method_extra(self, vendor_imprint_methods, vendor_imprint_locations, other_finishers)
            imprint_methods = list(set(imprint_methods))
            imprint_locations = list(set(imprint_locations))
            vendor_imprint_methods = list(set(vendor_imprint_methods) - set(imprint_methods))
            vendor_imprint_locations = list(set(vendor_imprint_locations) - set(imprint_locations))
            imprint_methods += vendor_imprint_methods
            imprint_locations += vendor_imprint_locations
            # To Delete Unwanted Vendor Methods & Locations
            to_be_deletes = []
            for vendor_imprint_method in vendor_imprint_methods:
                if vendor_imprint_method not in imprint_methods:
                    to_be_deletes.append(vendor_imprint_method)
            for to_be_delete in to_be_deletes:
                vendor_imprint_methods.remove(to_be_delete)
            to_be_deletes = []
            for vendor_imprint_location in vendor_imprint_locations:
                if vendor_imprint_location not in imprint_locations:
                    to_be_deletes.append(vendor_imprint_location)
            for to_be_delete in to_be_deletes:
                vendor_imprint_locations.remove(to_be_delete)
        elif self.env.context.get('art_lines', False):
            for art_line in self.env['art.production.line'].browse(self.env.context.get('art_lines', False)):
                for finisher in art_line.n_product_id.finisher_ids:
                    for product_imprint_id in finisher.product_imprint_ids:
                        if not self.imprint_location and not self.finisher:
                            imprint_methods.append(
                                product_imprint_id.decoration_method.id)
                        elif self.imprint_location and not self.finisher:
                            if self.imprint_location.id == product_imprint_id.decoration_location.id:
                                imprint_methods.append(product_imprint_id.decoration_method.id)
                        elif not self.imprint_location and self.finisher:
                            if self.finisher.id == finisher.finisher_id.id:
                                imprint_methods.append(product_imprint_id.decoration_method.id)
                        else:
                            if self.imprint_location.id == product_imprint_id.decoration_location.id and self.finisher.id == finisher.finisher_id.id:
                                imprint_methods.append(product_imprint_id.decoration_method.id)
                        if not self.imprint_method and not self.finisher:
                            imprint_locations.append(product_imprint_id.decoration_location.id)
                        elif self.imprint_method and not self.finisher:
                            if self.imprint_method.id == product_imprint_id.decoration_method.id:
                                imprint_locations.append(product_imprint_id.decoration_location.id)
                        elif not self.imprint_method and self.finisher:
                            if self.finisher.id == finisher.finisher_id.id:
                                imprint_locations.append(product_imprint_id.decoration_location.id)
                        else:
                            if self.imprint_method.id == product_imprint_id.decoration_method.id and self.finisher.id == finisher.finisher_id.id:
                                imprint_locations.append(product_imprint_id.decoration_location.id)
                        if not self.imprint_method and not self.imprint_location:
                            finishers.append(finisher.finisher_id.id)
                        elif self.imprint_method and not self.imprint_location:
                            if self.imprint_method.id == product_imprint_id.decoration_method.id:
                                finishers.append(finisher.finisher_id.id)
                        elif not self.imprint_method and self.imprint_location:
                            if self.imprint_location.id == product_imprint_id.decoration_location.id:
                                finishers.append(finisher.finisher_id.id)
                        else:
                            if self.imprint_method.id == product_imprint_id.decoration_method.id and self.imprint_location.id == product_imprint_id.decoration_location.id:
                                finishers.append(finisher.finisher_id.id)
                break
            imprint_methods = list(set(imprint_methods))
            imprint_locations = list(set(imprint_locations))
            finishers = list(set(finishers))
            imprint_methods, imprint_locations, finishers = common()._onchange_imprint_method_extra(self, imprint_methods, imprint_locations, finishers)
        return imprint_methods, vendor_imprint_methods, imprint_locations, vendor_imprint_locations, list(set(finishers + other_finishers)), finishers

    @api.onchange('imprint_method', 'imprint_location', 'finisher',
                  'global_search', 'customer_art')
    def _onchange_imprint_method(self):
        imprint_methods = []
        imprint_locations = []
        vendor_imprint_methods = []
        vendor_imprint_locations = []
        finishers = []
        all_finishers_assigned_to_product = []
        if self and not self.finisher and not self.env.context.get('manual_artworksheet', False):
            self.imprint_method = False
            self.imprint_location = False
        stock_colors = common().onchange_stock_color(self)
        pms_codes = common().onchange_pms_code(self)
        if self.global_search:
            imprint_methods = self.env['decoration.method'].with_context(imprint_method=True, is_the_global_search_enabled=True).search([]).ids
            imprint_locations = self.env['decoration.location'].with_context(imprint_location=True, is_the_global_search_enabled=True).search([]).ids
            finishers = self.env['res.partner'].search(
                [('is_finisher', '=', True)]).ids
        else:
            imprint_methods, vendor_imprint_methods, imprint_locations, vendor_imprint_locations, finishers, all_finishers_assigned_to_product = self._onchange_imprint_method_computation()
        if 'imprint_method' in self.env.context or 'imprint_location' in self.env.context or 'finisher' in self.env.context and self:
            vendor_decoration_location, vendor_decoration_method, vendor_prod_specific = common().return_vendor_decoration_value(self)
            self.vendor_decoration_method = vendor_decoration_method
            self.vendor_decoration_location = vendor_decoration_location
            self.vendor_prod_specific = vendor_prod_specific
        return {'domain': {
            'imprint_method': ['|', ('id', 'in', imprint_methods), ('id', 'in', vendor_imprint_methods)],
            'imprint_location': ['|', ('id', 'in', imprint_locations), ('id', 'in', vendor_imprint_locations)],
            'finisher': ['|', ('id', 'in', finishers), ('id', 'in', all_finishers_assigned_to_product)],
            'stock_color': [('id', 'in', stock_colors)],
            'pms_code': [('id', 'in', pms_codes)]
        }}

    @api.onchange('sale_order_line_decoration_id')
    def _onchange_sale_order_line_decoration_id(self):
        sale_order_line_decoration_ids = []
        if self.env.context.get('art_id', False) and self.env.context.get('art_lines', False):
            art_rec = self.env['art.production'].browse(self.env.context.get('art_id', False))
            deco_obj = self.env['sale.order.line.decoration']
            if art_rec and art_rec.art_lines:
                for art_line in art_rec.art_lines:
                    dec_recs = deco_obj.search([('art_line_id', '=', art_line.id)])
                    sale_order_line_decoration_ids += dec_recs.ids
        if self.env.context.get('order_id', False) and self.env.context.get('sale_order_lines', False):
            sale_order = self.env['sale.order'].browse(self.env.context.get('order_id', False))
            if sale_order and sale_order.order_line:
                for sale_order_second in sale_order.order_line:
                    sale_order_line_decoration_ids += sale_order_second.decorations.ids
        return {'domain': {
            'sale_order_line_decoration_id':
            [('id', 'in', sale_order_line_decoration_ids)],
        }}

    @api.onchange('imprint_method', 'finisher', 'vendor_decoration_method', 'global_search')
    def _onchange_imprint_method_or_finisher(self):
        if not self.global_search:
            vendor_decoration_methods, vendor_decoration_locations = common().return_vendor_decoration_domain(self)
            return {    
                        'domain': {
                            'vendor_decoration_method': [('id', 'in', vendor_decoration_methods)],
                            'vendor_decoration_location': [('id', 'in', vendor_decoration_locations)]
                        }
                    }
        else:
            vendor_decoration_methods = self.env['vendor.decoration.method'].search([]).ids
            vendor_decoration_locations = self.env['vendor.decoration.location'].search([]).ids
            return {    
                        'domain': {
                            'vendor_decoration_method': [('id', 'in', vendor_decoration_methods)],
                            'vendor_decoration_location': [('id', 'in', vendor_decoration_locations)]
                        }
                    }
                
    @api.onchange('sale_order_line_decoration_id')
    def _onchange_sale_order_line_id(self):
        if self.sale_order_line_decoration_id and not self.already_existed_decoration_line and 'sale_order_line_decoration' in self.env.context:
            if self.env.context.get('sale_order_lines', False) or self.env.context.get('art_lines', False):
                common()._onchange_sale_order_line_id(self)

    @api.multi
    def _compute_customer_arts(self):
        if self.env.context.get('order_id', False):
            sale_order = self.env['sale.order'].browse(
                self.env.context.get('order_id', False))
            if sale_order:
                self.customer_arts = [(6, 0, sale_order.customer_arts.ids)]
        else:
            self.customer_arts = [(6, 0, [])]