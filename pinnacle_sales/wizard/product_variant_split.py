# -*- coding: utf-8 -*-
from openerp import models, fields, api, _
import openerp.addons.decimal_precision as dp
from openerp.exceptions import UserError


class ProductVariantSplit(models.TransientModel):
    """
    Work as a wizard for splitting at a time one sale order line into multiple sale order lines by its quantity.
    """

    _name = 'product.variant.split'

    name = fields.Char(
        compute="_compute_name",
        string='Product',
        readonly=True)
    qty = fields.Float(
        compute="_compute_qty",
        string="Current Quantity",
        digits=dp.get_precision('Product Unit of Measure'),
        default=1.0,
        readonly=True)
    product_variant_split_line_ids = fields.One2many(
        'product.variant.split.line',
        'product_variant_split_id',
        'To Be Splitted In Product Variants With Their Quantity',
        required=True)

    @api.one
    @api.multi
    def _compute_name(self):
        """
        Load the selected sale order line's product details into the current wizard's name field.
        """
        if self.env.context.get('active_id', False):
            sale_order_line = self.env['sale.order.line'].browse([self.env.context.get('active_id', False)])
            combined_name = False
            if sale_order_line:
                combined_name = [sale_order_line.product_sku or False,
                                 sale_order_line.product_variant_name or False,
                                 sale_order_line.name or False]
            if combined_name:
                self.name = ' - '.join(filter(None, combined_name))
            else:
                self.name = 'Nothing'

    @api.one
    @api.multi
    def _compute_qty(self):
        """
        Load the selected sale order line's quantity into the current wizard's qty field.
        """
        if self.env.context.get('active_id', False):
            sale_order_line = self.env['sale.order.line'].browse(
                [self.env.context.get('active_id', False)])
            self.qty = sale_order_line.product_uom_qty

    @api.onchange('product_variant_split_line_ids')
    def _onchange_name(self):
        """
        Reload the selected sale order line's product details and quantity into the current wizard's name and qty fields respectively.
        """
        if self.env.context.get('active_id', False):
            sale_order_line = self.env['sale.order.line'].browse(
                [self.env.context.get('active_id', False)])
            combined_name = False
            if sale_order_line:
                combined_name = [sale_order_line.product_sku or False,
                                 sale_order_line.product_variant_name or False,
                                 sale_order_line.name or False]
                self.qty = sale_order_line.product_uom_qty
            if combined_name:
                self.name = ' - '.join(filter(None, combined_name))
            else:
                self.name = 'Nothing'

    @api.multi
    def split_sale_order_line(self):
        """
        Split the selected sale order line with its total quantity into multiple sale order lines with their quantity
        from inputed number of times quantity. That means one inputed quantity line is equal to one sale order line
        with that inputed quantity.
        """
        if self.env.context.get('active_id', False):
            sale_order_line = self.env['sale.order.line'].browse([self.env.context.get('active_id', False)])
            total_qty = 0.00
            for product_variant_split_line in self.product_variant_split_line_ids:
                total_qty += product_variant_split_line.product_uom_qty
            if total_qty != sale_order_line.product_uom_qty:
                raise UserError(_('Total Quantity (' + str(sale_order_line.product_uom_qty) + ') Of Sale Order Line Does Not Match' + 'With Total Quantity (' + str(total_qty)+') Of Splitted Lines.'))
            for product_variant_split_line in self.product_variant_split_line_ids:
                sale_order_line.order_id.write(
                                                {'order_line': [[1, sale_order_line.copy(
                                                    {'product_id': sale_order_line.product_id.id,
                                                     'product_uom_qty': product_variant_split_line.product_uom_qty,
                                                     'product_uom': sale_order_line.product_uom.id,
                                                     'select': False,
                                                     'order_id': sale_order_line.order_id.id,
                                                    }).id, {}]]
                                                }
                                            )
            sale_order = sale_order_line.order_id
            sale_order_line.unlink()
            return {'type': 'ir.actions.act_window', 'view_mode': 'form',
                    'target': 'current', 'res_model':
                    'sale.order', 'res_id': sale_order.id,
                    }


class ProductVariantSplitLine(models.TransientModel):
    """
    Help to input the quantity for each new sale order line to be created.
    """

    _name = 'product.variant.split.line'

    product_variant_split_id = fields.Many2one(
        'product.variant.split',
        'Product')
    product_uom_qty = fields.Float(
        string='New Quantity',
        digits=dp.get_precision('Product Unit of Measure'),
        required=True,
        default=1.0)
