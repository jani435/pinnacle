# -*- coding: utf-8 -*-
from openerp import models, fields, api, _
from openerp.exceptions import UserError

class NotesWhenRequestforApproval(models.TransientModel):
    _name = 'notes.when.request.for.approval'

    notes = fields.Text('Notes')

    @api.multi
    def send(self):
        self.ensure_one()
        if self.env.context.get('active_id', False):
            sale_order = self.env['sale.order'].browse([self.env.context.get('active_id', False)])
            if sale_order:
                if self.env.context.get('is_a_manager_approval', False):
                    levels = self.env['sale.order.approval.level'].search([]).sorted(key=lambda r: r.approval_order)
                    for level in levels:
                        if level:
                            if sale_order.amount_total >= level.lower_limit:
                                sale_order.approval_level = level.id
                                sale_order.state = 'to_approve'
                                sale_order.approval_url = str(self.env['ir.config_parameter'].search([('key', '=', 'web.base.url')]).value) + '/web?#id=' + str(sale_order.id) + '&view_type=form&model=sale.order&action=' + str(self.env.ref('pinnacle_sales.' +'action_waiting_for_approval_orders').id) + '&menu_id='
                                mail_template = self.env.ref(
                                    'pinnacle_sales.' +
                                    'sale_order_template_approval_notification')
                                if self.notes:
                                    mail_id = mail_template.with_context(notes=str(self.notes)).send_mail(sale_order.id)
                                else:
                                    mail_id = mail_template.send_mail(sale_order.id)
                                sale_order.env['mail.mail'].browse([mail_id]).send()
                                body = "<p><b>The User (%s) is Requesting For Manager Approval for Sales Order #%s </b></p>" % (self.env.user.name, sale_order.name)
                                if self.notes:
                                    body += "<p><b>Notes When Requesting For Manager Approval : %s</b></p>" % self.notes
                                sale_order.message_post(body=body, attachments=[], message_type='comment')
                                break
                    sale_order.skipped_pending_review = False
                    return True
                if self.env.context.get('is_a_accounting_approval', False):
                    sale_order.state = 'accounting_approval'
                    sale_order.skipped_confirmation_step = False
                    sale_order.approval_url = str(self.env['ir.config_parameter'].search([('key', '=', 'web.base.url')]).value) + '/web?#id=' + str(sale_order.id) + '&view_type=form&model=sale.order&action=' + str(self.env.ref('pinnacle_sales.' +'action_waiting_for_approval_orders').id) + '&menu_id='
                    mail_template = self.env.ref(
                        'pinnacle_sales.' +
                        'sale_order_template_accounting_approval_notification')
                    if self.notes:
                        mail_id = mail_template.with_context(notes=str(self.notes)).send_mail(sale_order.id)
                    else:
                        mail_id = mail_template.send_mail(sale_order.id)
                    body = "<p><b>The User (%s) is Requesting For Accounting Approval for Sales Order #%s </b></p>" % (self.env.user.name, sale_order.name)
                    if self.notes:
                        body += "<p><b>Notes When Requesting For Accounting Approval : %s</b></p>" % self.notes
                    sale_order.message_post(body=body, attachments=[], message_type='comment')
                    sale_order.env['mail.mail'].browse([mail_id]).send()
                    return True
        return False
