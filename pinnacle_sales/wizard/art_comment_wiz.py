# -*- coding: utf-8 -*-
from openerp import models, fields, api, _
from odoo import tools
import urllib2
import json
from openerp.exceptions import UserError
import base64

class ArtComment(models.TransientModel):
    _name = 'art.comment'

    description = fields.Text(string='Comment')
    subject = fields.Char(string='Subject')
    attachment_ids = fields.Many2many(
        'ir.attachment', 'art_comment_ir_attachments_rel',
        'wizard_id', 'attachment_id', 'Upload Attachments')
    partner_ids = fields.Many2many('res.partner', string="To")

    @api.multi
    def comment(self):
        context = dict(self.env.context).copy()
        art_id = context.get('art_id')
        art_obj = self.env['art.production']
        template_obj = self.env['mail.template']
        art_rec = art_obj.browse(art_id)
        self.ensure_one()
        try:
            request_url = str(self.env['ir.config_parameter'].get_param('artq.web.url'))
            request_url += "Completed&worksheet_id=%s" % art_id
            req = urllib2.Request(request_url)
            parents = urllib2.urlopen(req).read()
            response = json.loads(parents)
            if response != 'success':
                raise UserError(_("Cannot update ArtQ from Odoo."))
        except urllib2.HTTPError:
            raise UserError(_("Cannot access ArtQ URL."))
        mail_template = self.env.ref(
            'pinnacle_sales.' +
            'art_production_template_finished_notification')
        body = tools.html_sanitize(mail_template.body_html)
        if self.description:
            body += '<b>Comments:</b>' + self.description
        ir_config_parameter_obj = self.env['ir.config_parameter']
        cfs_url = ir_config_parameter_obj.get_param('cfs.web.url', raise_exception=True)
        if cfs_url:
            for associated_file in art_rec.associated_file_ids:
                template = """<left>\
                        <a href=%s target='_blank' title=%s>\
                            <img src=%s ></img>\
                        </a><br/>\
                        <a href=%s target='_blank'>Download</a>\
                   </left>
                """
                if associated_file.spec_file_url:
                    final_url = cfs_url + associated_file.spec_file_url
                    spec_file = template % (
                        (final_url + '?h=40'),
                        associated_file.spec_file_name,
                        (final_url + '?h=40'),
                        (final_url + '?dl=1'))
                    body += '<br />' + spec_file
                if associated_file.spec_file2_url:
                    final_url = cfs_url + associated_file.spec_file2_url
                    spec_file2 = template % (
                        (final_url + '?h=40'),
                        associated_file.spec_file2_name,
                        (final_url + '?h=40'),
                        (final_url + '?dl=1'))
                    body += '<br />' + spec_file2
                if associated_file.vendor_file_url:
                    final_url = cfs_url + associated_file.vendor_file_url
                    vendor_file = template % (
                        (final_url + '?h=40'),
                        associated_file.vendor_file_name,
                        (final_url + '?h=40'),
                        (final_url + '?dl=1'))
                    body += '<br />' + vendor_file
        # Report as a mail.message
        report_name = 'Art Worksheet'
        ctx = self._context.copy()
        ctx.update({'active_model': 'art.production',
                    'active_ids': [art_rec.id], 'active_id': art_rec.id})
        res, format = self.env['report'].sudo().with_context(ctx).get_pdf(art_rec._ids,
                                                         'pinnacle_sales.report_art_worksheet'), 'pdf'
        ext = "." + format
        if not report_name.endswith(ext):
            report_name += ext
        data_attach = {
            'name': report_name,
            'datas': base64.b64encode(res),
            'datas_fname': report_name,
            'res_model': 'mail.compose.message',
            'res_id': 0,
            'type': 'binary',  # override default_type from context, possibly meant for another model!
        }
        art_report = self.env['ir.attachment'].create(data_attach).id
        signature = self.env.user.signature
        if signature:
            body = tools.append_content_to_html(body, signature, plaintext=False)
        body_updated = template_obj.render_template(
            body, mail_template.model, [art_id])
        emails = []
        # emails.append(art_rec.sale_id and art_rec.sale_id.user_id.partner_id.id or False)
        partner_ids = [partner_id.id for partner_id in self.partner_ids]
        emails.extend(partner_ids)
        # if art_rec.sale_id and art_rec.sale_id.aam_id:
        #     for aam_id in art_rec.sale_id.aam_id:
        #         emails.append(aam_id.partner_id.id)
        # emails = [email for email in emails if email]
        if art_rec.status == 'store_image':
            partner_obj = self.env['res.partner']
            # self.store_image = True
            follower1 = partner_obj.search([('email', '=', 'robyn.goldstein@pinnaclepromotions.com')])
            follower2 = partner_obj.search([('email', '=', 'shannon.lee@pinnaclepromotions.com')])
            follower3 = partner_obj.search([('email', '=', 'kelly.johnson@pinnaclepromotions.com')])
            if not follower1:
                follower1 = partner_obj.create({'name': 'Robyn Goldstein', 'email': 'robyn.goldstein@pinnaclepromotions.com'})
            if not follower2:
                follower2 = partner_obj.create({'name': 'Shannon Lee', 'email': 'shannon.lee@pinnaclepromotions.com'})
            if not follower3:
                follower3 = partner_obj.create({'name': 'Kelly Johnson', 'email': 'kelly.johnson@pinnaclepromotions.com'})
            emails.extend([follower1.id, follower2.id, follower3.id])
        attach_ids = [attach.id for attach in self.attachment_ids]
        attach_ids.append(art_report)
        art_rec.message_post(attachment_ids=attach_ids, subtype='mail.mt_comment', message_type='comment', body=body_updated.get(art_id), subject= self.subject)
        # 18528 : Art - Completed Company Store Image Emails 
        # self.env['mail.compose.message'].with_context({
        #     'default_model': 'art.production',
        #     'default_res_id': art_id,
        #     'default_use_template': bool(mail_template),
        #     'default_template_id': mail_template,
        #     'default_composition_mode': 'comment',
        #     'default_partner_ids': emails,
        #     'remove_followers_of_the_document': True,
        # }).create({'body': body_updated.get(art_id), 'subject': self.subject, 'attachment_ids': [(6, 0, attach_ids)]
        #            }).send_mail()
        # 13173: DEV: Art Worksheet - Issue Tab
        track_id = art_rec.track_issue_ids and art_rec.track_issue_ids[-1] or False
        if track_id:
            track_id.designer_id = art_rec.designer_id
        if not art_rec.initial_designer_id:
            initial_designer_id = self.env.uid or art_rec.initial_designer_id.id
        else:
            initial_designer_id = art_rec.initial_designer_id.id
        art_rec.write({'state': 'completed', 'designer_id': False, 'status': False, 'day_requested': False,
                       'time_requested': False, 'art_type': False,
                       'complete_date': fields.datetime.now(),
                       'rush_request1': False, 'store_image': False, 'completed': False, 'alter_logo': False,
                       'show_complete': False, 'initial_designer_id': initial_designer_id})
        return {'type': 'ir.actions.act_window_close'}
