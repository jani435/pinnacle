# -*- coding: utf-8 -*-
from openerp import models, fields, api, _
from openerp.exceptions import UserError

class AccountingRejectedWizard(models.TransientModel):
    """
    Work as a wizard for accountly rejecting the sale order with inputting comment message.
    """
    _name = 'accouting.rejected.wizard'

    comment = fields.Text('Comment')
    order_id = fields.Many2one('sale.order')

    @api.multi
    def set_to_reject(self):
        """
        Accountly reject the sale order by a user who have an access of Accountant.
        It also send the email by rendering the template 'sale_order_template_accounting_rejected_notification' and
        post the simple message with inputted comment.
        """
        self.ensure_one()
        order = self.order_id
        accountant_group_id = self.env.ref('account.group_account_user')
        if not accountant_group_id:
            raise UserError(_('Accountant Group is not available'))
        if self.env.user.id not in accountant_group_id.users.ids:
            raise UserError(_('You don\'t have the Accountant access needed to reject the sale order.'))
        else:
            if order:
                order.state = 'accounting_rejected'
                order.accounting_approval_or_rejection_comment = self.comment
                mail_template = self.env.ref(
                    'pinnacle_sales.' +
                    'sale_order_template_accounting_rejected_notification')
                mail_id = mail_template.send_mail(order.id)
                self.env['mail.mail'].browse([mail_id]).send()
                body = "<p><b>Sales Order %s is rejected by an accountant (%s) </b></p>" % (order.name, self.env.user.name)
                body += "<p><b>Rejected comment : %s</b></p>" % self.comment
                order.message_post(body=body, attachments=[], message_type='comment')

