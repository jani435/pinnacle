# -*- coding: utf-8 -*-

from odoo import api, models, fields


class MailComposeMessage(models.TransientModel):
    _inherit = 'mail.compose.message'

    @api.multi
    def send_mail(self, auto_commit=False):
        """
        Override this method to track the mail compose message of sale quotation's
        and update the sale order state and confirmation sent date accordingly.
        """
        if self._context.get('default_model') == 'sale.order' and self._context.get('default_res_id') and self._context.get('mark_so_as_sent'):
            order = self.env['sale.order'].browse([self._context['default_res_id']])
            if order.state in ['pending_review', 'approve']:
                order.write({'state': 'sent', 'confirmation_sent': fields.datetime.now()})
            self = self.with_context(mail_post_autofollow=True)
        return super(MailComposeMessage, self).send_mail(auto_commit=auto_commit)
