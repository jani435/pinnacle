# -*- coding: utf-8 -*-
from openerp import models, fields, api, _
import openerp.addons.decimal_precision as dp
from openerp.exceptions import UserError


class SaleOrderSplit(models.TransientModel):
    _name = 'sale.order.split'

    sale_order_id = fields.Many2one(
        'sale.order', string="Sale Order Reference")
    sale_order_split_line_ids = fields.One2many(
        'sale.order.split.line', 'sale_order_split_id', string="Product Templates")

    @api.model
    def default_get(self, fields):
        res = super(SaleOrderSplit, self).default_get(fields)
        if self._context.get('active_id') and self.env['sale.order'].browse(self._context.get('active_id')):
            if self.env['sale.order'].browse(self._context.get('active_id')).state not in ['draft', 'art_processing', 'pending_review', 'sent']:
                raise UserError(
                    _('You cannot split a Sales Order once POs are generated.'))
            product_template_lines = []
            product_template_ids = []
            for order_line in self.env['sale.order'].browse(self._context.get('active_id')).order_line:
                if order_line.product_id.type != 'service' and order_line.product_id.product_tmpl_id.id not in product_template_ids:
                    value = {}
                    value[
                        'product_template_id'] = order_line.product_id.product_tmpl_id.id
                    product_template_lines.append([0, 0, value])
                    product_template_ids.append(
                        order_line.product_id.product_tmpl_id.id)
            res.update({'sale_order_id':  self._context.get(
                'active_id'), 'sale_order_split_line_ids': product_template_lines})
        return res

    def manage_art_worksheet(self, order_record, splitted_sale_order, order_line_mapping, decoration_group_id_mapping, decoration_mapping):
        art_mapping = {}
        art_line_mapping = {}
        for order_line in order_record.order_line:
            if order_line.id in order_line_mapping:
                for decoration in order_line.decorations:
                    if decoration.art_id and decoration.art_id.id not in art_mapping:
                        if decoration.art_id.reorder_sale_id:
                            art_copied = decoration.art_id.with_context(art_copy= True).copy({'status': decoration.art_id.status, 'state': decoration.art_id.state,
                                                                 'sale_id': splitted_sale_order.id, 'reorder_sale_id': decoration.art_id.reorder_sale_id.id})
                        else:
                            art_copied = decoration.art_id.with_context(art_copy= True).copy(
                                {'status': decoration.art_id.status, 'state': decoration.art_id.state, 'sale_id': splitted_sale_order.id})
                        art_mapping[decoration.art_id.id] = art_copied.id
                        for art_line in decoration.art_id.art_lines:
                            if art_line.id not in art_line_mapping:
                                copy_art_line = True
                                for decoration in art_line.decorations:
                                    if decoration.id not in decoration_mapping:
                                        copy_art_line = False
                                        break
                                if art_line.art_id and art_line.art_id.id in art_mapping and copy_art_line:
                                    art_line_copied = art_line.copy({'art_id': art_mapping[
                                                                    art_line.art_id.id], 'sale_line_id': order_line_mapping[art_line.sale_line_id.id]})
                                else:
                                    art_line_copied = False
                                if art_line_copied:
                                    art_line_mapping[
                                        art_line.id] = art_line_copied.id
                        for product_id in decoration.art_id.product_ids:
                            if product_id.id not in art_line_mapping:
                                copy_art_line = True
                                for decoration in product_id.decorations:
                                    if decoration.id not in decoration_mapping:
                                        copy_art_line = False
                                        break
                                if product_id.art_id and product_id.art_id.id in art_mapping and copy_art_line:
                                    art_line_copied = product_id.copy({'art_id': art_mapping[
                                                                      product_id.art_id.id, 'sale_line_id': order_line_mapping[product_id.sale_line_id.id]]})
                                else:
                                    art_line_copied = False
                                if art_line_copied:
                                    art_line_mapping[
                                        product_id.id] = art_line_copied.id
        for order_line in order_record.order_line:
            if order_line.id in order_line_mapping:
                for decoration in order_line.decorations:
                    if decoration.art_line_id and decoration.art_line_id.id not in art_line_mapping:
                        copy_art_line = True
                        for decoration in decoration.art_line_id.decorations:
                            if decoration.id not in decoration_mapping:
                                copy_art_line = False
                                break
                        if decoration.art_id and decoration.art_id.id in art_mapping and order_line.id in order_line_mapping and copy_art_line:
                            art_line_copied = decoration.art_line_id.copy({'art_id': art_mapping[
                                                                          decoration.art_id.id], 'sale_line_id': order_line_mapping[decoration.art_line_id.sale_line_id.id]})
                        else:
                            art_line_copied = False
                        if art_line_copied:
                            art_line_mapping[
                                decoration.art_line_id.id] = art_line_copied.id
        art_note_mapping = {}
        for order_line in order_record.order_line:
            if order_line.id in order_line_mapping:
                for decoration in order_line.decorations:
                    if decoration.note_id:
                        art_note_copied = decoration.note_id.copy()
                        art_note_mapping[
                            decoration.note_id.id] = art_note_copied.id
        for order_line in order_record.order_line:
            for decoration in order_line.decorations:
                if decoration.id in decoration_mapping:
                    if decoration.art_id and decoration.art_id.id in art_mapping:
                        self.env['sale.order.line.decoration'].browse(
                            [decoration_mapping[decoration.id]]).art_id = art_mapping[decoration.art_id.id]
                    if decoration.art_line_id and decoration.art_line_id.id in art_line_mapping:
                        self.env['sale.order.line.decoration'].browse([decoration_mapping[
                                                                      decoration.id]]).art_line_id = art_line_mapping[decoration.art_line_id.id]
                    if decoration.note_id and decoration.note_id.id in art_note_mapping:
                        self.env['sale.order.line.decoration'].browse(
                            [decoration_mapping[decoration.id]]).note_id = art_note_mapping[decoration.note_id.id]
        return True

    def manage_shipping_lines(self, order_record, splitted_sale_order, order_line_mapping):
        shipping_line_mapping = {}
        shipping_line_to_be_deleted = []
        order_shipping_line_to_be_deleted = []
        delivery_carrier_products = []
        for delivery_carrier in self.env['delivery.carrier'].search([]):
            if delivery_carrier.product_id:
                delivery_carrier_products.append(
                    delivery_carrier.product_id.id)
        for shipping_line in order_record.shipping_line:
            if shipping_line.tmpl_id.id in [sale_order_split_line_id.product_template_id.id for sale_order_split_line_id in self.sale_order_split_line_ids]:
                old_shipping_sale_orders = [
                    shipping_sale_order.id for shipping_sale_order in shipping_line.shipping_sale_orders]
                new_shipping_sale_orders = []
                for old_shipping_sale_order in old_shipping_sale_orders:
                    if old_shipping_sale_order in order_line_mapping:
                        new_shipping_sale_orders.append(
                            order_line_mapping[old_shipping_sale_order])
                shipping_line_copied = shipping_line.copy(
                    {'order': splitted_sale_order.id, 'shipping_sale_orders': [[6, 0, new_shipping_sale_orders]]})
                shipping_line_mapping[
                    shipping_line.id] = shipping_line_copied.id
                shipping_line_to_be_deleted.append(shipping_line.id)
        for order_line in splitted_sale_order.order_line:
            if order_line.product_id.id in delivery_carrier_products:
                if order_line.shipping_id.id in shipping_line_mapping:
                    order_line.shipping_id = shipping_line_mapping[
                        order_line.shipping_id.id]
                else:
                    order_shipping_line_to_be_deleted.append(order_line.id)
        if order_shipping_line_to_be_deleted:
            self.env['sale.order.line'].browse(
                list(set(order_shipping_line_to_be_deleted))).unlink()
        return shipping_line_to_be_deleted

    @api.multi
    def split_sale_order(self):
        if self._context.get('active_id'):
            self.ensure_one()
            so_rec = self.env['sale.order'].browse(
                self._context.get('active_id'))
            default = {'aam_id': [[6, 0, so_rec.aam_id.ids]],
                       'soc_id': so_rec.soc_id and so_rec.soc_id.id or False,
                       'user_id': so_rec.user_id and so_rec.user_id.id or False,
                       'customer_po_id': so_rec.customer_po_id,
                       'customer_po_file': so_rec.customer_po_file,
                       'customer_po_file_name': so_rec.customer_po_file_name, }
            splitted_sale_order = so_rec.with_context(
                split_sale_order=True).copy(default=default)
            if splitted_sale_order:
                splitted_sale_order.splttedorder_ref = self._context.get(
                    'active_id')
                order_line_mapping = {}
                order_line_to_be_deleted = []
                order_line_mapping = {}
                decoration_group_id_mapping = {}
                delivery_carrier_and_decoration_charge_products = []
                for delivery_carrier in self.env['delivery.carrier'].search([]):
                    if delivery_carrier.product_id:
                        delivery_carrier_and_decoration_charge_products.append(
                            delivery_carrier.product_id.id)
                product_setup_sale_price = self.env.ref(
                    'pinnacle_sales.product_product_setup_sale_price')
                product_run_charge = self.env.ref(
                    'pinnacle_sales.product_product_run_charge')
                product_pms_patching = self.env.ref(
                    'pinnacle_sales.product_product_pms_patching')
                if not product_setup_sale_price:
                    raise UserError(_('Setup Sale Price Product Not Found'))
                else:
                    delivery_carrier_and_decoration_charge_products.append(
                        product_setup_sale_price.id)
                if not product_run_charge:
                    raise UserError(_('Run Charge Product Not Found'))
                else:
                    delivery_carrier_and_decoration_charge_products.append(
                        product_run_charge.id)
                if not product_pms_patching:
                    raise UserError(_('PMS Matching Product Not Found'))
                else:
                    delivery_carrier_and_decoration_charge_products.append(
                        product_pms_patching.id)
                for order_line in self.env['sale.order'].browse(self._context.get('active_id')).order_line:
                    if order_line.product_id.product_tmpl_id.id in [sale_order_split_line_id.product_template_id.id for sale_order_split_line_id in self.sale_order_split_line_ids]:
                        order_line_copied = order_line.with_context(
                            sale_order_split=True).copy({'order_id': splitted_sale_order.id})
                        order_line_mapping[
                            order_line.id] = order_line_copied.id
                        for decoration_group_id in order_line.decoration_group_sequences:
                            decoration_group_id_copied = decoration_group_id.copy(
                                {'order_id': splitted_sale_order.id})
                            decoration_group_id_mapping[
                                decoration_group_id.id] = decoration_group_id_copied.id
                        order_line_to_be_deleted.append(order_line.id)
                    if order_line.product_id.id in delivery_carrier_and_decoration_charge_products:
                        order_line_copied = order_line.copy(
                            {'order_id': splitted_sale_order.id})
                        order_line_mapping[
                            order_line.id] = order_line_copied.id
                decoration_mapping = {}
                for order_line in self.env['sale.order'].browse(self._context.get('active_id')).order_line:
                    if order_line.id in order_line_mapping:
                        for decoration in order_line.decorations:
                            if decoration.decoration_group_id and decoration.decoration_group_id.id in decoration_group_id_mapping:
                                decoration_copied = decoration.copy({'sale_order_line_id': order_line_mapping[
                                                                    order_line.id], 'decoration_group_id': decoration_group_id_mapping[decoration.decoration_group_id.id]})
                            else:
                                decoration_copied = decoration.copy(
                                    {'sale_order_line_id': order_line_mapping[order_line.id]})
                            decoration_mapping[
                                decoration.id] = decoration_copied.id
                charge_lines_mapping = {}
                for decoration_group_id in self.env['sale.order'].browse(self._context.get('active_id')).decoration_group_ids:
                    copy_decoration_charges = True
                    for decoration in decoration_group_id.decorations:
                        if decoration.id not in decoration_mapping:
                            copy_decoration_charges = False
                            break
                    if copy_decoration_charges:
                        for charge_line in decoration_group_id.charge_lines:
                            if charge_line.decoration_group_id and charge_line.decoration_group_id.id in decoration_group_id_mapping:
                                if charge_line.at_least_one_decoration_recalculate and charge_line.at_least_one_decoration_recalculate.id in decoration_mapping:
                                    charge_line_copied = charge_line.copy({'decoration_group_id': decoration_group_id_mapping[
                                                                          charge_line.decoration_group_id.id], 'at_least_one_decoration_recalculate': decoration_mapping[charge_line.at_least_one_decoration_recalculate.id]})
                                else:
                                    charge_line_copied = charge_line.copy(
                                        {'decoration_group_id': decoration_group_id_mapping[charge_line.decoration_group_id.id]})
                                charge_lines_mapping[
                                    charge_line.id] = charge_line_copied.id
                for order_line in self.env['sale.order'].browse(self._context.get('active_id')).order_line:
                    if order_line.decoration_charge_group_id and order_line.decoration_charge_group_id.id in charge_lines_mapping:
                        self.env['sale.order.line'].browse([order_line_mapping[order_line.id]]).decoration_charge_group_id = charge_lines_mapping[
                            order_line.decoration_charge_group_id.id]
                self.manage_art_worksheet(self.env['sale.order'].browse(self._context.get(
                    'active_id')), splitted_sale_order, order_line_mapping, decoration_group_id_mapping, decoration_mapping)
                shipping_line_to_be_deleted = self.manage_shipping_lines(self.env['sale.order'].browse(
                    self._context.get('active_id')), splitted_sale_order, order_line_mapping)
                for order_line in splitted_sale_order.order_line:
                    if order_line.product_id.id == product_setup_sale_price.id or order_line.product_id.id == product_run_charge.id or order_line.product_id.id == product_pms_patching.id:
                        if not order_line.decoration_charge_group_id:
                            order_line_to_be_deleted.append(order_line.id)
                if order_line_to_be_deleted:
                    self.env['sale.order.line'].browse(
                        list(set(order_line_to_be_deleted))).unlink()
                if shipping_line_to_be_deleted:
                    self.env['shipping.line'].browse(
                        list(set(shipping_line_to_be_deleted))).unlink()
                result = {}
                imd = self.env['ir.model.data']
                action = imd.xmlid_to_object("sale.action_quotations")
                list_view_id = imd.xmlid_to_res_id('sale.view_quotation_tree')
                form_view_id = imd.xmlid_to_res_id('sale.view_order_form')

                result = {
                    'name': action.name,
                    'help': action.help,
                    'type': action.type,
                    'views': [[list_view_id, 'tree'], [form_view_id, 'form'], [False, 'graph'], [False, 'kanban'], [False, 'calendar'], [False, 'pivot']],
                    'target': action.target,
                    'context': action.context,
                    'res_model': action.res_model,
                }
                result['views'] = [(form_view_id, 'form')]
                result['res_id'] = splitted_sale_order.id
                return result
        return False


class SaleOrderSplitLine(models.TransientModel):
    _name = 'sale.order.split.line'

    sale_order_split_id = fields.Many2one(
        'sale.order.split', 'Sale Order Split')
    product_template_id = fields.Many2one(
        'product.template', 'Product Template')
