# -*- coding: utf-8 -*-
from odoo import api, fields, models, _

class SaleInvoiceOptionsWizard(models.TransientModel):
    """
    Implement a wizard that will help to select one of the three options 
        - Consolidated Invoice Order
        - Invoice Order
        - Create & Send Invoice
    and perfom the operations related to selected option.
    """
    _name = "sale.inv.options.wizard"
    _description = "Sales Invoice Options"

    sale_options = fields.Selection([('Consolidated Invoice Order', 'Consolidated Invoice Order'),
                                     ('Invoice Order', 'Invoice Order'),
                                     ('Create Send Invoice', 'Create &amp; Send Invoice')], string='Sale Options')
    
    @api.multi
    def perform_operation(self):
        """
        Perform the operations related to selected option.
        """
        self.ensure_one()
        if self.sale_options == 'Consolidated Invoice Order':
            action = self.env.ref('pinnacle_sales.action_view_sale_advance_payment_consolidated_invoice')
            action_dict = action.read()
            res = action_dict[0]
            res['context'] = {'active_ids': self.env.context.get('active_ids'),
                        'active_model': self.env.context.get('active_model')}
            return res
        elif self.sale_options == 'Invoice Order':
            action = self.env.ref('sale.action_view_sale_advance_payment_inv')
            action_dict = action.read()
            res = action_dict[0]
            res['context'] = {'active_ids': self.env.context.get('active_ids'),
                        'active_model': self.env.context.get('active_model')}
            return res
        elif self.sale_options == 'Create Send Invoice':
            sale_orders = self.env['sale.order'].browse(self.env.context.get('active_ids'))
            for order in sale_orders:
                order.with_context({'create_send_invoice': True}).action_invoice_create()
            return sale_orders.action_view_invoice()
