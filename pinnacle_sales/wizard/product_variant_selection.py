# -*- coding: utf-8 -*-
from openerp import models, fields, api, _
import openerp.addons.decimal_precision as dp
from openerp.exceptions import UserError


class ProductVariantSelection(models.TransientModel):
    """
    Work as a wizard for inputting the product template and its variants for creating new sale order lines.
    """

    _name = 'product.variant.selection'

    product_template_id = fields.Many2one(
        'product.template',
        'Product',
        required=True,
        domain="[('sale_ok','=',True)]",
        help="Product Template")
    product_variant_selection_line_ids = fields.One2many(
        'product.variant.selection.line',
        'product_selection_id',
        'Product Variants',
        required=True)
    check_box = fields.Boolean("Update Price")

    @api.multi
    def open_vendor_link(self):
        if self.product_variant_selection_line_ids:
            ir_config_parameter_obj = self.env['ir.config_parameter']
            base_url = ir_config_parameter_obj.get_param('inventory_url', raise_exception=True)
            vendor = self.product_variant_selection_line_ids[0].change_supplier.name.name
            sku = self.product_template_id.product_sku
            base_url = base_url.rstrip('/').lstrip(' ')
            url = base_url+'/suppliers_odoo/inventory_status/'+str(vendor)+'/'+str(sku)
            return {
                'type': 'ir.actions.act_url',
                'url': url,
                'target': 'new',
            }
        else:
            raise UserError(_("Please select at least one Product Variant."))

    @api.onchange('product_template_id')
    def _onchange_product_temp1late_id(self):
        art_id = int(self.env.context.get('art_id', False))
        if art_id:
            return {'domain': {}}
        self.product_variant_selection_line_ids = []
        sale_order_obj = self.env['sale.order'].browse(int(self.env.context.get('sale_order', False)))
        if not sale_order_obj.is_regular:
            return {'domain': {'product_template_id': [('sample_ok','=', True)]}}

    @api.onchange('product_variant_selection_line_ids')
    def _onchange_product_variant_selection_line_ids(self):
        self.check_box = False

    @api.onchange('check_box')
    def _onchange_check_box(self):
        if self.check_box:
            self.calculate_so_line()

    def calculate_so_line(self):
        all_value = []
        sale_order_id = int(self.env.context.get('sale_order', False))
        sale_order_lines = self.env['sale.order.line'].search([('order_id','=',sale_order_id),('product_id','in', self.product_template_id.product_variant_ids.ids)])
        group__by = {}
        def update_dict(attb_id, line, group__by):
            if not group__by.has_key(attb_id):
                group__by[attb_id] = line.product_uom_qty
            else:
                group__by[attb_id] += line.product_uom_qty

        for line in sale_order_lines:
            for attb_id in line.product_id.attribute_value_ids.ids:
                update_dict(attb_id, line, group__by)

        for line in self.product_variant_selection_line_ids:
             for attb_id in line.product_variant_id.attribute_value_ids.ids:
               update_dict(attb_id, line, group__by)

        group_price_by = {}
        attrib_value = self.env['attribute.value']
        for key in group__by:
            line = attrib_value.search([('tmpl_id','=', self.product_template_id.id),('value_id','=',key)], limit=1)
            if line:
                tier = self.product_template_id._get_tier(group__by[key] or 0)[1]
                if tier ==1:
                    group_price_by[key] = line.tier1
                elif tier ==2:
                    group_price_by[key] = line.tier2
                elif tier ==3:
                    group_price_by[key] = line.tier3
                elif tier ==4:
                    group_price_by[key] = line.tier4
                elif tier ==5:
                    group_price_by[key] = line.tier5
                elif tier ==6:
                    group_price_by[key] = line.tier6
            else:
                group_price_by[key] = 0
        for line in self.product_variant_selection_line_ids:
            price = 0
            for attrib in line.product_variant_id.attribute_value_ids.ids:
                if group_price_by.has_key(attrib):
                    price = price + group_price_by[attrib]
            line.update({'additional_price_per_product': price})

   
    @api.multi
    def create_sale_order_line(self):
        """
        Create new sale order lines from the inputted product variants with quantity,supplier and additional price.
        """
        sale_order_id = int(self.env.context.get('sale_order', False))
        if len(self.product_variant_selection_line_ids) == 0:
            raise UserError('Please select the variant of the product you would like to add to your order.')
        if sale_order_id:
            sale_order_obj = self.env['sale.order'].browse(sale_order_id)
            decoratin_obj = self.env['sale.order.line.decoration']
            fini_color_obj = self.env['finisher.color.line']
            for product_variant_selection in self:
                finisher = product_variant_selection.product_template_id.finisher_ids and \
                           product_variant_selection.product_template_id.finisher_ids[0] or False
                for product_variant_selection_line_id in product_variant_selection.product_variant_selection_line_ids:
                    # Create new sale order lines
                    sale_line_id = self.env['sale.order.line'].create(
                            {'product_id': product_variant_selection_line_id.product_variant_id.id,
                             'product_uom_qty': product_variant_selection_line_id.product_uom_qty,
                             'product_uom': product_variant_selection_line_id.product_variant_id.uom_id.id,
                             'order_id': sale_order_id,
                             'change_supplier': product_variant_selection_line_id.change_supplier.id,
                             'is_decoration_disable': not sale_order_obj.is_regular
                             })
                    if not finisher:
                        continue
                    if product_variant_selection.product_template_id.present_decoration and not product_variant_selection.product_template_id.customer_arts:
                        for decoration in finisher.product_imprint_ids:
                            if not decoration.present_decoration:
                                continue
                            imprint_area = decoration.imprint_area
                            colors = []
                            if decoration.customer_art and decoration.customer_art.stock_color:
                                colors = [color.id for color in decoration.customer_art.stock_color]

                            color_lines = fini_color_obj.search([('partner_id', '=', finisher.finisher_id.id),
                                                                                  ('color', '=', colors),
                                                                                  ('name', '=', decoration.stock_color.id)])
                            pms_color_code = ''
                            if len(color_lines) == 1:
                                pms_color_code += color_lines.code
                            else:
                                for line in color_lines:
                                    pms_color_code += line.code + ', '
                                pms_color_code = pms_color_code[:-2]
                            decoratin_obj.create({
                                'imprint_location':
                                decoration.decoration_location and
                                decoration.decoration_location.id or
                                False,
                                'imprint_method':
                                decoration.decoration_method and
                                decoration.decoration_method.id or
                                False,
                                'customer_art':
                                decoration.customer_art and
                                decoration.customer_art.id or
                                False,
                                'finisher':
                                finisher.finisher_id and
                                finisher.finisher_id.id or
                                False,
                                'pms_code':
                                decoration.customer_art and decoration.customer_art.pms_code and decoration.customer_art.pms_code.ids and
                                [[6, 0, decoration.customer_art.pms_code.ids]] or
                                False,
                                'stock_color':
                                decoration.customer_art and decoration.customer_art.stock_color and decoration.customer_art.stock_color.ids and
                                [[6, 0, decoration.customer_art.stock_color.ids]]
                                or
                                False,
                                'sale_order_line_id': sale_line_id.id,
                                'min_pt_size': decoration.point_size,
                                'area': imprint_area,
                                'pms_color_code': pms_color_code,
                                'vendor_decoration_location':
                                decoration.vendor_decoration_location and
                                decoration.vendor_decoration_location.id or
                                False,
                                'vendor_decoration_method':
                                decoration.vendor_decoration_method and
                                decoration.vendor_decoration_method.id or
                                False,
                                'vendor_prod_specific': decoration.imprint_method_product_specific,
                                'is_reorder': decoration.is_reorder,
                                'reorder_ref': decoration.reorder_ref,
                                'reorder_ref_po': decoration.reorder_ref_po
                            })
                            finisher_id = finisher.finisher_id and finisher.finisher_id.id or False,
                            sale_line_id.finisher_id = finisher_id
            sale_order_obj.update_tier_price(self.product_template_id.id)

    @api.multi
    def create_art_product_line(self):
        art_id = int(self.env.context.get('art_id', False))
        if len(self.product_variant_selection_line_ids) == 0:
            raise UserError('Please select the variant of the product you would like to add to your order.')
        if art_id:
            art_obj = self.env['art.production'].browse(art_id)
            art_line_obj = self.env['art.production.line']
            decoratin_obj = self.env['sale.order.line.decoration']
            fini_color_obj = self.env['finisher.color.line']
            for product_variant_selection in self:
                finisher = product_variant_selection.product_template_id.finisher_ids and \
                           product_variant_selection.product_template_id.finisher_ids[0] or False
                product_name = product_variant_selection.product_template_id.name
                product_sku = product_variant_selection.product_template_id.product_sku
                for product_variant_selection_line_id in product_variant_selection.product_variant_selection_line_ids:
                    supplier_id = product_variant_selection_line_id.change_supplier and product_variant_selection_line_id.change_supplier.name.id or False
                    for seller in product_variant_selection.product_template_id.seller_ids:
                        if seller.name.id == supplier_id:
                            product_name = seller.product_name
                            product_sku = seller.product_code
                            break
                    if product_variant_selection_line_id.change_supplier.product_length or product_variant_selection_line_id.change_supplier.product_width \
                            or product_variant_selection_line_id.change_supplier.product_height or product_variant_selection_line_id.change_supplier.product_diameter:
                        length = str(product_variant_selection_line_id.change_supplier.product_length) + 'Lx'
                        width = str(product_variant_selection_line_id.change_supplier.product_width) + 'Wx'
                        height = str(product_variant_selection_line_id.change_supplier.product_height) + 'Hx'
                        diameter = str(product_variant_selection_line_id.change_supplier.product_diameter) + 'D'
                        item_dimensions = length + width + height + diameter
                    else:
                        item_dimensions = product_variant_selection_line_id.change_supplier.product_dimensions
                    product_dict = {
                        'product_id': product_variant_selection_line_id.product_variant_id.id,
                        'n_product_id': product_variant_selection_line_id.product_variant_id.id,
                        'quantity': product_variant_selection_line_id.product_uom_qty or 0.0,
                        'art_id': art_id,
                        'item_dimensions': item_dimensions,
                        'description': product_name,
                        'product_sku': product_sku,
                        'supplier_id': supplier_id}
                    art_line = art_line_obj.create(product_dict)
                    if not finisher:
                        continue
                    if product_variant_selection.product_template_id.present_decoration and not product_variant_selection.product_template_id.customer_arts:
                        for decoration in finisher.product_imprint_ids:
                            if not decoration.present_decoration:
                                continue
                            imprint_area = decoration.imprint_area
                            colors = []
                            if decoration.customer_art and decoration.customer_art.stock_color:
                                colors = [color.id for color in decoration.customer_art.stock_color]

                            color_lines = fini_color_obj.search([('partner_id', '=', finisher.finisher_id.id),
                                                                                  ('color', '=', colors),
                                                                                  ('name', '=', decoration.stock_color.id)])
                            pms_color_code = ''
                            if len(color_lines) == 1:
                                pms_color_code += color_lines.code
                            else:
                                for line in color_lines:
                                    pms_color_code += line.code + ', '
                                pms_color_code = pms_color_code[:-2]
                            #stk_color = [color.code for color in decoration.stock_color]
                            #stock_color_char = ", ".join(stk_color)
                            stock_color_char = "" #TODO Kalpana Hemani
                            decoratin_obj.create({
                                'imprint_location':
                                decoration.decoration_location and
                                decoration.decoration_location.id or
                                False,
                                'imprint_method':
                                decoration.decoration_method and
                                decoration.decoration_method.id or
                                False,
                                'customer_art':
                                decoration.customer_art and
                                decoration.customer_art.id or
                                False,
                                'finisher':
                                finisher.finisher_id and
                                finisher.finisher_id.id or
                                False,
                                'pms_code':
                                decoration.customer_art and decoration.customer_art.pms_code and decoration.customer_art.pms_code.ids and
                                [[6, 0, decoration.customer_art.pms_code.ids]] or
                                False,
                                'stock_color':
                                decoration.customer_art and decoration.customer_art.stock_color and decoration.customer_art.stock_color.ids and
                                [[6, 0, decoration.customer_art.stock_color.ids]]
                                or
                                False,
                                'min_pt_size': decoration.point_size,
                                'area': imprint_area,
                                'pms_color_code': pms_color_code,
                                'stock_color_char': stock_color_char,
                                'vendor_decoration_location':
                                decoration.vendor_decoration_location and
                                decoration.vendor_decoration_location.id or
                                False,
                                'vendor_decoration_method':
                                decoration.vendor_decoration_method and
                                decoration.vendor_decoration_method.id or
                                False,
                                'vendor_prod_specific': decoration.imprint_method_product_specific,
                                'is_reorder': decoration.is_reorder,
                                'reorder_ref': decoration.reorder_ref,
                                'reorder_ref_po': decoration.reorder_ref_po,
                                'art_line_id': art_line.id,
                                'art_id': art_id
                            })
                            finisher_id = finisher.finisher_id and finisher.finisher_id.id or False,


class ProductVariantSelectionLine(models.TransientModel):
    """
    Help to input the product variant and its quantity, supplier and additional price.
    """
    _name = 'product.variant.selection.line'

    @api.depends('product_selection_id')
    def _compute_supplier_required(self):
        for this in self:
            if this.product_selection_id:
                if not this.product_selection_id.product_template_id.seller_ids:
                    this.supplier_required = False
                else:
                    this.supplier_required = True

    product_selection_id = fields.Many2one(
        'product.variant.selection', 'Product')
    product_variant_id = fields.Many2one(
        'product.product', 'Variants', required=True)
    product_uom_qty = fields.Float(string='Quantity', digits=dp.get_precision(
        'Product Unit of Measure'), required=True, default=1.0)
    additional_price_per_product = fields.Float(
        store=True, readonly=True,
        string="Additional Price Per Product")
    change_supplier = fields.Many2one('product.supplierinfo', string="Supplier")
    supplier_required = fields.Boolean(compute="_compute_supplier_required", store=True)

    @api.onchange('product_variant_id')
    def _onchange_domain_vendor_available(self):
        template = self.env['product.template'].browse(self.env.context.get('product_tmp_id'))
        ids, preferred =  template.get_supplier(self.product_variant_id)
        partner_ids, preferred_ids, supp_dict = template.get_supplier_id(self.product_variant_id)
        if not self.change_supplier.id:
            if self.env.context.get('vendor_id') in partner_ids:
                self.change_supplier = supp_dict.get(self.env.context.get('vendor_id'))
            elif preferred in ids:
                self.change_supplier = preferred
        elif self.change_supplier.id not in ids:
            self.change_supplier = False

