# -*- coding: utf-8 -*-
from openerp import models, fields, api, _
import openerp.addons.decimal_precision as dp
from openerp.exceptions import UserError
class SaleOrderWizardChange(models.TransientModel):
    _name = 'sale.order.wizard.change'

    name = fields.Char(string="Sale order", readonly=True)
    sale_order_lines = fields.One2many(
        'sale.order.line.wizard.change',
        'sale_order_id',
        'Edit Sale Order',
        required=True)
    user_response_about_change_to_qty = fields.Boolean('User Response About Change To Quantity', default=False)
    user_response_about_change_to_variant = fields.Boolean('User Response About Change To Variant', default=False)

    @api.model
    def default_get(self, fields):
        res = super(SaleOrderWizardChange, self).default_get(fields)
        sale_order = self.env['sale.order'].browse(self.env.context.get('sale_order'))
        product_rush_policy = self.env.ref('pinnacle_sales.product_product_rush_policy_price_update_change')
        delivery_carrier_products = []
        for delivery_carrier in self.env['delivery.carrier'].search([]):
            if delivery_carrier.product_id:
                delivery_carrier_products.append(delivery_carrier.product_id.id)
        charge_lines_products = [
                self.env.ref('pinnacle_sales.product_product_setup_sale_price').id,
                self.env.ref('pinnacle_sales.product_product_run_charge').id,
                self.env.ref('pinnacle_sales.product_product_pms_patching').id,
                self.env.ref('pinnacle_sales.product_product_rush_policy_price_update_change').id
                ]
        all_rec = []
        for rec in self.env['sale.order.line'].browse(self.env.context.get('active_ids')):
            value = {}
            value['change_qty'] = rec.product_uom_qty
            value['product_uom_qty'] = rec.product_uom_qty
            value['has_any_charge'] = rec.has_any_charge
            value['has_any_decoration'] = rec.has_any_decoration
            value['sale_order_line_id'] = rec.id
            value['change_supplier'] = rec.change_supplier.id
            value['change_variant'] = rec.product_id.id
            value['product_id'] = rec.product_id.id
            value['product_template'] = rec.product_id.product_tmpl_id.id
            value['is_blank'] = rec.blank_line
            value['partner_id'] = rec.other_supplier_id.id
            if rec.product_id.id == product_rush_policy.id:
                value['is_blank'] = True
            all_rec.append([0,0,value])
        res.update({'name':  sale_order.name, 'sale_order_lines':all_rec, 'user_response_about_change_to_qty': False, 'user_response_about_change_to_variant':  False})
        return res

    @api.multi
    def save_change(self):
        for this in self.sale_order_lines:
            value = {}
            if this.is_blank:
                value['name'] = this.change_variant.name_get()[0][1]
                blank_order_product = self.env.ref('pinnacle_sales.product_product_blank_order')
                if blank_order_product and this.change_variant.product_tmpl_id.id == blank_order_product.product_tmpl_id.id:
                    value['name'] = str(this.change_variant.with_context(product_name_get=True).name_get()[0][1])
            so = this.sale_order_line_id
            changed_supplier = False
            shipping_lines = self.env['shipping.line'].search([('shipping_sale_orders', '=', so.id)])
            if this.partner_id.id:
                value['other_supplier_id'] = this.partner_id.id
            if this.change_qty != this.sale_order_line_id.product_uom_qty:
                value['product_uom_qty'] = this.change_qty
                decoration_group_ids = []
                if this.sale_order_line_id.art_line_id:
                    this.sale_order_line_id.art_line_id.quantity = this.change_qty
                if self.user_response_about_change_to_qty:
                    for decoration in so.decorations:
                        if decoration.decoration_group_id:
                            decoration_group_ids.append(decoration.decoration_group_id.id)
                    for decoration_group  in self.env['sale.order.line.decoration.group'].browse(list(set(decoration_group_ids))):
                        if decoration_group.charge_lines.exists():
                            decoration_group.charge_lines.exists().unlink()
                    if shipping_lines:
                        for line in shipping_lines:
                            for orderline in line.shipping_sale_orders:
                                orderline.tax_id = [(6, 0, [])]
                            line.delivery_line.unlink()
                            self.env['sale.order.line'].search([('shipping_id', '=', line.id)]).unlink()
            if this.change_supplier.id != this.sale_order_line_id.change_supplier.id:
                value['change_supplier'] = this.change_supplier.id
                # Check Supplier is Only Supplier
                if not (this.change_supplier.name.supplier and not this.change_supplier.name.is_finisher): 
                    # Archiving Fields For Maintaining Sale Order Details When Art Work Sheet Archived
                    if this.sale_order_line_id.artworksheet_test_for_checking_submitted():
                        art_wroksheets_to_be_archived = [] 
                        decorations_ids_to_be_updated_for_artworksheet = [] 
                        for decoration in this.sale_order_line_id.decorations:
                            if decoration.art_id and decoration.art_id.id not in art_wroksheets_to_be_archived:    
                                for art_line in decoration.art_id.art_lines:
                                    if art_line.sale_line_id.id and art_line.sale_line_id.id != this.sale_order_line_id.id:
                                        decorations_ids_to_be_updated_for_artworksheet += art_line.decorations.ids
                                art_wroksheets_to_be_archived.append(decoration.art_id.id)
                        if  art_wroksheets_to_be_archived:
                            this.sale_order_line_id.copying_data_of_art_wroksheets_to_be_archived(art_wroksheets_to_be_archived)
                            this.sale_order_line_id.actual_archiving_of_artworksheets_with_handling_of_other_data(art_wroksheets_to_be_archived)
                        if decorations_ids_to_be_updated_for_artworksheet:
                            this.sale_order_line_id.updating_other_decorations_of_archiving_artworksheets(decorations_ids_to_be_updated_for_artworksheet)
                        if so.decorations:
                            so.decorations.unlink()
                # Updating Tier Prices Of All Order Lines When Changing Supplier Of Any Line
                changed_supplier = True
                value['1'] = False
                if shipping_lines:
                    for line in shipping_lines:
                        for orderline in line.shipping_sale_orders:
                            orderline.tax_id = [(6, 0, [])]
                        line.unlink()
                    this_line = self.env['shipping.line'].search([('order', '=', so.order_id.id), ('tmpl_id', '=', so.product_id.product_tmpl_id.id),
                                                                    ('supplier', '=', value['change_supplier'])])
                    if this_line:
                        for line in this_line:
                            line.delivery_line.unlink()
                            self.env['sale.order.line'].search([('shipping_id', '=', line.id)]).unlink()
            if this.change_variant.id != this.sale_order_line_id.product_id.id:
                p_id = self.env.ref('pinnacle_sales.product_product_promocode_apply')
                if p_id.product_tmpl_id.id == this.product_template.id:
                    value['price_unit'] =  this.change_variant.lst_price
                value['product_id'] = this.change_variant.id
                value['is_decoration_disable'] = False
                if self.user_response_about_change_to_variant:
                    if this.sale_order_line_id.artworksheet_test_for_checking_submitted():
                        if so.decorations:
                            so.decorations.unlink()
                    if shipping_lines.exists():
                        for line in shipping_lines.exists():
                            for orderline in line.shipping_sale_orders:
                                orderline.tax_id = [(6, 0, [])]
                            flag = line.unlink()
                else:
                    if shipping_lines.exists():
                        for line in shipping_lines.exists():
                            product_lines = self.env['product.shipping.line'].search([('shipping_id', '=', line.id)])
                            for proline in product_lines:
                                if proline.product.id != value['product_id']:
                                    proline.write({'product': value['product_id']})
            so.write(value)
            if changed_supplier:
                if so.order_id:
                    so.order_id.update_tier_price()
            #so.order_id.update_shipping_line()


class SaleOrderLineWizardChange(models.TransientModel):
    _name = 'sale.order.line.wizard.change'

    sale_order_id = fields.Many2one('sale.order.wizard.change', string='Sale Order')
    change_qty = fields.Float(string='Quantity', digits=dp.get_precision('Product Unit of Measure'), 
                        required=True, default=1.0)
    change_supplier = fields.Many2one('product.supplierinfo')
    change_variant = fields.Many2one("product.product", string="Variants", required=True)
    product_template = fields.Many2one("product.template", string="Product Template", readonly=True)
    sale_order_line_id = fields.Many2one("sale.order.line", string="Sale Order Line")
    is_blank = fields.Boolean(default=False)
    is_rush = fields.Boolean(default=False)
    hide_description = fields.Boolean(default=False)
    description = fields.Char()
    partner_id = fields.Many2one('res.partner', 'Vendor')
    product_uom_qty = fields.Float(string='Sale Order Line Quantity', digits=dp.get_precision('Product Unit of Measure'), 
                        required=True, default=1.0)
    product_id = fields.Many2one("product.product", string="Sale Order Line Variant", required=True)
    has_any_charge = fields.Boolean(string="Has Any Charge ?", default=False)
    has_any_decoration = fields.Boolean(string="Has Any Decoration ?", store=True)

    @api.onchange('change_variant')
    def _onchange_domain_vendor_available(self):
        ids, preferred =  self.product_template.get_supplier(self.change_variant)
        if not self.change_supplier.id:
            if preferred in ids:
                self.change_supplier = preferred
        elif self.change_supplier.id not in ids:
            self.change_supplier = False
