# -*- coding: utf-8 -*-
from openerp import models, fields, api, _

class ChangeState(models.TransientModel):
    _name = 'change.state'

    so_status = fields.Selection([
        ('quote_draft', 'Draft Quote'),
        ('quote_vir_processing', 'Virtual Processing'),
        ('quote_sent', 'Quote Sent'),
        ('closed_quote', 'Quote Closed'),
        ('quote_review', 'Review Quote'),
        ('quote_man_approval', 'Manager Approval'),
        ('quote_man_approved', 'Manager Approved'),
        ('quote_man_rejected', 'Manager Rejected'),
        ('quote_approved', 'Quote Approved'),
        ('quote_rejected', 'Quote Rejected'),
        ('draft', 'Draft'),
        ('art_processing', 'Art Processing'),
        ('pending_review', 'Pending Review'),
        ('to_approve', 'Manager Approval'),
        ('approve', 'Manager Approved'),
        ('reject', 'Manager Rejected'),
        ('sent', 'Confirmation Sent'),
        ('confirmation_received', 'Confirmation Received'),
        ('accounting_approval', 'Accounting Approval'),
        ('accounting_approved', 'Accounting Approved'),
        ('accounting_rejected', 'Accounting Rejected'),
        ('accounting_p_approval', 'Accounting Payment Approval'),
        ('accounting_p_approved', 'Accounting Payment Approved'),
        ('accounting_p_rejected', 'Accounting Payment Rejected'),
        ('sale', 'PO Generated'),
        ('ready_for_fullfillment', 'Ready for Fulfillment'),
        ('shipped', 'Shipped'),
        ('partially_invoiced', 'Partially Invoiced'),
        ('invoiced', 'Invoiced'),
        ('delivered', 'Delivered'),
        ('close', 'Closed'),
        ('reopen', 'Re-Opened'),
        ('done', 'Re-Closed'),
        ('cancel', 'Cancelled'),
        ('hold', 'On Hold'),
        ('inactive', 'Inactive'),
    ])

    po_status = fields.Selection([
        ('draft', 'Draft'),
        ('sent', 'Submitted'),
        ('to approve', 'To Approve'),
        ('purchase', 'Confirmed'),
        ('shipped', 'Shipped'),
        ('deliver', 'Delivered'),
        ('close', 'Closed'),
        ('reopen', 'Re-Opened'),
        ('done', 'Re-Closed'),
        ('cancel', 'Cancelled'),
        ('hold', 'On Hold'),
    ])

    @api.multi
    def update_state(self):
        model = self._context.get('active_model', False)
        res_id = self._context.get('active_id', False)
        if model and res_id:
            if model == 'sale.order':
                self.env[model].browse(res_id).write({'state': self.so_status})
            if model == 'purchase.order':
                self.env[model].browse(res_id).write({'state': self.po_status})