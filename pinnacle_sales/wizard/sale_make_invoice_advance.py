# -*- coding: utf-8 -*-

import time

from odoo import api, fields, models, _
import odoo.addons.decimal_precision as dp
from odoo.exceptions import UserError

class SaleAdvancePaymentInv(models.TransientModel):
    """
    Override this TransientModel to implement the concept of 'Consolidated Invoice'.
    """
    _inherit = "sale.advance.payment.inv"

    @api.model
    def _default_note(self):
        return self.env.user.company_id.sale_note

    @api.model
    def _get_default_team(self):
        return self.env['crm.team']._get_default_team_id()

    partner_id = fields.Many2one('res.partner', string='Customer')
    partner_invoice_id = fields.Many2one('res.partner', string='Customer Invoice Address')
    partner_shipping_id = fields.Many2one('res.partner', string='Customer Shipping Address')
    payment_term_id = fields.Many2one('account.payment.term', string="Payment Terms")
    fiscal_position_id = fields.Many2one('account.fiscal.position', string='Fiscal Position')
    note = fields.Text('Terms and conditions', default=_default_note)
    team_id = fields.Many2one('crm.team', 'Sales Team', change_default=True, default=_get_default_team)
    user_id = fields.Many2one('res.users', string='Salesperson', index=True, track_visibility='onchange', default=lambda self: self.env.user)
    send_email_of_sale_invoice = fields.Boolean(string="Send Auto Email Of Invoice To Customer ?", default=False)
    customer_po_id = fields.Char('Customer PO ID', help='Customer PO ID required for creating Purchase Ordes From the Sale Order, Otherwise approval needed from accounting department')
    customer_po_file = fields.Binary('Customer PO File', help='Customer PO File required for creating Purchase Ordes From the Sale Order, Otherwise approval needed from accounting department')
    customer_po_file_name = fields.Char('Customer PO File Name')

    @api.model
    def default_get(self, fields):
        res = super(SaleAdvancePaymentInv,self).default_get(fields)
        sale_orders = self.env['sale.order'].browse(self._context.get('active_ids', []))
        for sale_order in sale_orders:
            permitted_users_id = []
            group_account_user = self.env.ref('account.group_account_user')
            group_account_manager = self.env.ref('account.group_account_manager')
            if group_account_user and group_account_manager:
                permitted_users_id += group_account_user.users.ids
                permitted_users_id += group_account_manager.users.ids
            if sale_order.send_email_of_sale_invoice == False and self.env.user.id not in permitted_users_id:
                raise UserError(_('You don\'t have the access of accountant and its parent access group and the Sale order ('+str(sale_order.name)+') has the \'Send Auto Email Of Invoice To Customer\' field\'s value inactive.\nSo you cann\'t generate the invoice.'))
        return res

    @api.onchange('partner_id')
    def _onchange_partner_id(self):
        if self.partner_id:
            addr = self.partner_id.address_get(['delivery', 'invoice'])
            self.partner_invoice_id = addr['invoice']
            self.partner_shipping_id = addr['delivery']
            self.payment_term_id = self.partner_id.property_payment_term_id and self.partner_id.property_payment_term_id.id or False,

    @api.multi
    @api.onchange('partner_shipping_id', 'partner_id')
    def onchange_partner_shipping_id(self):
        """
        Trigger the change of fiscal position when the shipping address is modified.
        """
        self.fiscal_position_id = self.env['account.fiscal.position'].get_fiscal_position(self.partner_id.id, self.partner_shipping_id.id)
        return {}

    @api.multi
    def create_invoices(self):  
        sale_orders = self.env['sale.order'].browse(self._context.get('active_ids', []))
        for sale_order in sale_orders:
            permitted_users_id = []
            group_account_user = self.env.ref('account.group_account_user')
            group_account_manager = self.env.ref('account.group_account_manager')
            if group_account_user and group_account_manager:
                permitted_users_id += group_account_user.users.ids
                permitted_users_id += group_account_manager.users.ids
            if sale_order.send_email_of_sale_invoice == False and self.env.user.id not in permitted_users_id:
                raise UserError(_('You don\'t have the access of accountant and its parent access group and the Sale order ('+str(sale_order.name)+') has the \'Send Auto Email Of Invoice To Customer\' field\'s value inactive.\nSo you cann\'t generate the invoice.'))
        partner_id, partner_invoice_id, partner_shipping_id, payment_term_id, fiscal_position_id, note, team_id, user_id, send_email_of_sale_invoice, customer_po_id, customer_po_file, customer_po_file_name = {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}
        if self._context.get('consolidated_invoice', False):
            for sale_order in sale_orders:
                if sale_order.currency_id.id != sale_orders[0].currency_id.id:
                    raise UserError(_('You can not consolidate invoices of different currencies.'))
                if sale_order.company_id.id != sale_orders[0].company_id.id:
                    raise UserError(_('You can not consolidate invoices of different companies.'))
            for sale_order in sale_orders:
                partner_id[sale_order.id] = sale_order.partner_id.id
                sale_order.with_context(consolidated_invoice=True).partner_id = self.partner_id.id
                partner_invoice_id[sale_order.id] = sale_order.partner_invoice_id.id
                sale_order.with_context(consolidated_invoice=True).partner_invoice_id = self.partner_invoice_id.id
                partner_shipping_id[sale_order.id] = sale_order.partner_shipping_id.id
                sale_order.with_context(consolidated_invoice=True).partner_shipping_id = self.partner_shipping_id.id
                payment_term_id[sale_order.id] = sale_order.payment_term_id.id
                sale_order.payment_term_id = self.payment_term_id.id
                fiscal_position_id[sale_order.id] = sale_order.fiscal_position_id.id
                sale_order.fiscal_position_id = self.fiscal_position_id.id
                note[sale_order.id] = sale_order.note
                sale_order.note = self.note
                team_id[sale_order.id] = sale_order.team_id.id
                sale_order.team_id = self.team_id.id
                user_id[sale_order.id] = sale_order.user_id.id
                sale_order.user_id = self.user_id.id
                send_email_of_sale_invoice[sale_order.id] = sale_order.send_email_of_sale_invoice
                sale_order.send_email_of_sale_invoice = self.send_email_of_sale_invoice
                customer_po_id[sale_order.id] = sale_order.customer_po_id
                sale_order.customer_po_id = self.customer_po_id
                customer_po_file[sale_order.id] = sale_order.customer_po_file
                sale_order.customer_po_file = self.customer_po_file
                customer_po_file_name[sale_order.id] = sale_order.customer_po_file_name
                sale_order.customer_po_file_name = self.customer_po_file_name
        res = super(SaleAdvancePaymentInv, self).create_invoices()
        if self._context.get('consolidated_invoice', False):
            for sale_order in sale_orders:
                sale_order.with_context(consolidated_invoice=True).partner_id = partner_id[sale_order.id]
                sale_order.with_context(consolidated_invoice=True).partner_invoice_id = partner_invoice_id[sale_order.id]
                sale_order.with_context(consolidated_invoice=True).partner_shipping_id = partner_shipping_id[sale_order.id]
                sale_order.payment_term_id = payment_term_id[sale_order.id]
                sale_order.fiscal_position_id = fiscal_position_id[sale_order.id]
                sale_order.note = note[sale_order.id]
                sale_order.team_id = team_id[sale_order.id]
                sale_order.user_id = user_id[sale_order.id]
                sale_order.send_email_of_sale_invoice = send_email_of_sale_invoice[sale_order.id]
                sale_order.customer_po_id = customer_po_id[sale_order.id]
                sale_order.customer_po_file = customer_po_file[sale_order.id]
                sale_order.customer_po_file_name = customer_po_file_name[sale_order.id]
        return res
