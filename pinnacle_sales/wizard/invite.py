from odoo import _, api, fields, models

class Invite(models.TransientModel):
    """ Wizard to invite partners (or channels) and make them followers. """
    _inherit = 'mail.wizard.invite'

    @api.onchange('partner_ids')
    def onchange_partner_ids(self):
        user_ids = self.env['res.users'].search([])
        user_ids = self.env['res.users'].browse(user_ids.ids).mapped('partner_id').ids
        return {'domain': {'partner_ids': [('id', 'in', user_ids)]}}
