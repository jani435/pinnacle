# -*- coding: utf-8 -*-
from openerp import models, fields, api


class ArtProductionLineManageVendorWebLink(models.TransientModel):
    """
    Work as a wizard for inputting the vendor value from user.
    """
    _name = 'art.production.line.manage.vendor.web.link'

    vendor_web_link = fields.Char(string='Vendor Web Link')

    @api.multi
    def manage_vendor_web_link_art_production_line(self):
        """
        Update the vendor value for multiple artworksheet lines.
        """
        self.ensure_one()
        if self.env.context.get('active_ids', False):
            art_production_lines = self.env['art.production.line'].browse(self.env.context.get('active_ids', False))
            art_production_lines.write({'web_link': self.vendor_web_link})
        return True

class UpdateItemDimensionsWizard(models.TransientModel):
    """
    Work as a wizard for inputting the vendor value from user.
    """
    _name = 'update.item.dimensions.wizard'

    item_dimensions = fields.Char(string='Item Dimensions')

    @api.multi
    def update_item_dimensions(self):
        """
        Update the item dimensions for multiple artworksheet lines.
        """
        self.ensure_one()
        if self.env.context.get('active_ids', False):
            art_production_lines = self.env['art.production.line'].browse(self.env.context.get('active_ids', False))
            art_production_lines.write({'item_dimensions': self.item_dimensions})
        return True

