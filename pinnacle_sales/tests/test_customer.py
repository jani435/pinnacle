# -*- coding: utf-8 -*-
from openerp.tests import common


class TestCustomer(common.TransactionCase):

    def setUp(self):
        super(TestCustomer, self).setUp()
        self.res_partner_model = self.env['res.partner']

    def test_customer(self):
        record = self.res_partner_model.create(
            {'customer': True,
             'name': 'Test Customer'})
        self.customer = record
        self.assertTrue(record, 'Created Customer')
        result = record.write({'name': 'Test Customer Write'})
        self.assertTrue(result, 'Written Customer')
        record = record.unlink()
        self.assertTrue(record, 'Deleted Customer')


class TestSaleOrderLineDecoration(common.TransactionCase):

    def setUp(self):
        super(TestSaleOrderLineDecoration, self).setUp()
        self.sale_order_model = self.env['sale.order']
        self.sale_order_line_model = self.env['sale.order.line']
        self.sale_order_line_decoration_model = self.env[
            'sale.order.line.decoration']
        self.product_imprint_model = self.env['product.imprint']
        self.product_finisher_model = self.env['product.finisher']
        self.decoration_method_model = self.env['decoration.method']
        self.decoration_location_model = self.env['decoration.location']
        self.product = self.env.ref(
            'pinnacle_sales.product_product_decoration')
        self.partner = self.env.ref('base.res_partner_1')
        self.res_partner_finisher = self.env.ref('pinnacle_sales.res_partner_finisher_1')

    def test_decoration(self):
        product_finisher = self.product_finisher_model.create({
            'finisher_id': self.res_partner_finisher.id,
            'product_id': self.product.product_tmpl_id.id,
        })
        partner_decoration_methods = [record.decoration_method and
                                      record.decoration_method.id
                                      for record in
                                      self.res_partner_finisher.finisher_ids]
        self.assertTrue(partner_decoration_methods, 'Finisher Methods')
        product_imprint = self.product_imprint_model.create({
            'decoration_method': self.decoration_method_model.
            browse(partner_decoration_methods) and
            self.decoration_method_model.
            browse(partner_decoration_methods)[0].id or False,
            'decoration_location': self.decoration_location_model.
            search([]) and self.decoration_location_model.
            search([])[0].id or False,
            'product_finisher_id': product_finisher
            and product_finisher.id or False
        })
        self.assertTrue(product_imprint, 'Product Decorated')
        sale_order = self.sale_order_model.create({
            'partner_id': self.partner.id,
            'partner_invoice_id': self.partner.id,
            'partner_shipping_id': self.partner.id,
            'order_line': [(0, 0, {'name': self.product.name,
                                   'product_id': self.product.id,
                                   'product_uom_qty': 2,
                                   'product_uom': self.product.uom_id.id,
                                   'price_unit': self.product.list_price})],
            'pricelist_id': self.env.ref('product.list0').id,
        })
        self.assertTrue(sale_order, 'Sale Order Created')
        decoration_line = self.sale_order_line_decoration_model.create({
            'imprint_location': product_imprint[0].decoration_location.id,
            'imprint_method': product_imprint[0].decoration_method.id,
            'finisher': self.res_partner_finisher.id,
            'customer_art': self.env.
            ref('pinnacle_sales.product_decoration_customer_art_1').id,
            'pms_code':
            [(6, 0, [self.env.
                     ref('pinnacle_sales.product_decoration_pms_code_1').id])],
            'stock_color':
            [(6, 0, [self.env.
                     ref('pinnacle_sales.product_decoration_stock_color_1')
                     .id])],
            'sale_order_line_id': sale_order and sale_order.order_line[0].id
        })
        self.assertTrue(decoration_line, 'Decoration Line Created')
        if sale_order:
            if sale_order.decoration_line[0].decorations:
                result = sale_order.decoration_line[0].decorations.write({'stock_color':[(6, 0, [self.env.ef('pinnacle_sales.product_decoration_stock_color_2').id])]})
                self.assertTrue(result, 'Decoration Line Written')
        if sale_order:
            if sale_order.decoration_line[0].decorations:
                record = sale_order.decoration_line[0].decorations.unlink()
                self.assertTrue(record, 'Decoration Line Deleted')
