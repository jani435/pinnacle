import logging
import math
import suds
from openerp import models, fields, api, _
from openerp.addons.delivery_fedex.models.fedex_request import FedexRequest
from openerp.exceptions import ValidationError
from openerp.exceptions import UserError , Warning
from urllib2 import URLError
import openerp.addons.decimal_precision as dp
import string
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT

import time
import urllib2
import json
import base64
import urllib
import urlparse
from lxml import etree
from odoo import tools
import datetime
import pytz
from odoo.osv import expression

ART_STATUS = {'draft': 'Draft',
              'submit': 'Submitted',
              'received': 'Received',
              'in_process': 'In Progress',
              'on_hold': 'On Hold',
              'completed': 'Completed',
              'archive': 'Archived',
              'hold': 'On Hold'}

_logger = logging.getLogger(__name__)

def url_fix(s, charset='utf-8'):
    if isinstance(s, unicode):
        s = s.encode(charset, 'ignore')
    scheme, netloc, path, qs, anchor = urlparse.urlsplit(s)
    path = urllib.quote(path, '/%')
    qs = urllib.quote_plus(qs, ':&=')
    return urlparse.urlunsplit((scheme, netloc, path, qs, anchor))

class FeedbackLines(models.Model):
    _name = 'feedback.lines'

    section = fields.Selection([('order_details', 'Order Details'),
                                ('shipping_information', 'Shipping Information'),
                                ('art_review', 'Art Review'),
                                ('billing_information', 'Billing Information')
                                ])
    status = fields.Selection([('yes', 'Yes'), ('no', 'No')], 'Status')
    customer_changes = fields.Char('Customer Changes', default='Test')
    comments = fields.Char('Comments')
    order_id = fields.Many2one('sale.order')

class DeliveryLine(models.Model):
    _name = 'delivery.line'

    checked = fields.Boolean()
    delivery_name = fields.Char('Delivery Method')
    delivery_id = fields.Many2one('delivery.carrier', 'Delivery Method')
    price = fields.Float()
    cost = fields.Float()
    transit_time = fields.Char('Transit Time')
    shipping = fields.Many2one('shipping.line')
    service_type = fields.Char('Service Type')
    zipcode = fields.Char('Zip')
    flag = fields.Boolean(default=True)
    order_line_id = fields.Many2one('sale.order.line')


class ProductShippingLine(models.Model):
    _name = 'product.shipping.line'
    _rec_name = 'product'

    product = fields.Many2one('product.product')
    shipping_id = fields.Many2one('shipping.line')


class ShippingLine(models.Model):
    _name = 'shipping.line'

    @api.depends('product_line', 'product_qty')
    def _compute_name(self):
        for line in self:
            if line.tmpl_id:
                line.name = line.tmpl_id.name + ', ' + \
                            str(int(line.product_qty)) + ' ' + 'Variants'

    # @api.onchange('delivery_method')
    # def onchange_delivery(self):
    #     if self.delivery_method.delivery_type != 'fedex':
    #         self.customer_account = True
    #     if self.delivery_method.delivery_type == 'fedex':
    #         if self.delivery_method.country_ids.code != 'US':
    #             self.customer_account = True
    #         else:
    #             self.customer_account = False

    @api.onchange('customer_account')
    def onchange_customer_account(self):
        if self.customer_account:
            self.vendor_account = False
        if self.contact and self.delivery_method:
            if self.delivery_method.delivery_type == 'fedex':
                self.account_number = self.contact.fedex_customer_account
            if self.delivery_method.delivery_type == 'ups':
                self.account_number = self.contact.ups_customer_account

    @api.onchange('vendor_account')
    def onchange_vendor_account(self):
        if self.vendor_account:
            self.customer_account = False

    @api.multi
    def _get_default_contact(self):
        order_id = self._context.get('order_id')
        if order_id:
            partner_shipping_id = self.env['sale.order'].browse(order_id).partner_shipping_id
            if partner_shipping_id:
                return partner_shipping_id.id

    @api.multi
    def _get_default_contact_country_id(self):
        order_id = self._context.get('order_id')
        if order_id:
            partner_shipping_id = self.env['sale.order'].browse(order_id).partner_shipping_id        
            if partner_shipping_id and not partner_shipping_id.country_id:
                us_country = self.ref('base.us')
                if us_country:
                    return us_country.id
            if partner_shipping_id and partner_shipping_id.country_id:
                return partner_shipping_id.country_id.id

    @api.multi
    def _get_default_method(self):
        country_id = self.env['res.country'].search([('code', '=', 'US')])
        fedex = self.env['delivery.carrier'].search(
            [('delivery_type', '=', 'fedex'), ('country_ids', 'in', country_id.id)], limit=1)
        return fedex

    # @api.multi
    # def _get_delivery_methods(self):
    #     country_id = self.env['res.country'].search([('code', '=', 'US')])
    #     if country_id:
    #         methods = self.env['delivery.carrier'].search(
    #             ['|', ('delivery_type', 'in', ['fedex', 'ups']), ('country_ids', '=', country_id.id)])
    #         res = [method.id for method in methods]
    #         res.append(self.env.ref('pinnacle_sales.delivery_carrier_alternative').id)
    #         res.append(self.env.ref('pinnacle_sales.delivery_carrier_freight').id)
    #         return [('id', 'in', res)]

    @api.onchange('carton_dimension')
    def onchange_carton_dimension(self):
        if self.supplier:
            supplier = self.supplier
            self.carton_weight = supplier.carton_weight
            self.carton_length = supplier.carton_length
            self.carton_width = supplier.carton_width
            self.carton_height = supplier.carton_height
            self.items_per_carton = supplier.items_per_cartoon
        else:
            self.carton_weight = 0
            self.carton_length = 0
            self.carton_width = 0
            self.carton_height = 0
            self.items_per_carton = 0


    # @api.depends('customer_account', 'delivery_method')
    # def _compute_required(self):
    #     if self.customer_account:
    #         if self.delivery_method.delivery_type == 'ups':
    #             self.account_required = True
    #         if self.delivery_method.delivery_type == 'fedex':
    #             if self.delivery_method.country_ids.code == 'US':
    #                 self.account_required = False
    #             # else:
    #             #     self.account_required = False
    #     # else:
    #     #     self.account_required = False

    @api.depends('order', 'product_line', 'finisher', 'supplier')
    def get_production_time(self):
        for this in self:
            if this.order.is_regular == False or not this.finisher:
                if this.product_line:
                    product = this.product_line[0].product.product_tmpl_id
                    this.production_time = str(this.supplier.min_production_time) + '-' + str(
                        this.supplier.max_production_time) + ' ' + 'days'
            else:
                if this.product_line and this.finisher:
                    product = this.product_line[0].product.product_tmpl_id
                    if this.shipping_sale_orders:
                        finisher = this.env['product.finisher'].search([('product_id', '=', product.id),
                                                                        ('finisher_id', '=', this.finisher.id)])
                        imprints = []
                        for decoration in this.shipping_sale_orders[0].decorations:
                            # todo nandan check
                            imprint = this.env['product.imprint'].search([('product_finisher_id', 'in', finisher.ids),
                                                                          ('decoration_method', '=',
                                                                           decoration.imprint_method.id),
                                                                          ('decoration_location', '=',
                                                                           decoration.imprint_location.id)])
                            imprints.append(imprint)
                        this.production_time = ''
                        for line in imprints:
                            production_time = ''
                            #need to check with Jennifer for this functionality
                            for l in line:
                                min_time = l.min_production_time if l.min_production_time else ''
                                max_time = l.max_production_time if l.max_production_time else ''
                                decoration_name = l.decoration_method.name if l.decoration_method.name else ''
                                if min_time != '' and min_time != '' and decoration_name != '':
                                    production_time = decoration_name + ' ' + str(min_time) + '-' + str(
                                        max_time) + ' ' + 'days' + '\n'
                                    this.production_time += production_time

    @api.depends('delivery_method', 'delivery_line')
    def _compute_method_name(self):
        for this in self:
            if this.ship_to_location == 'no':
                if this.delivery_method:
                    delivery_method = this.delivery_method.name
                    if this.delivery_line:
                        service = this.delivery_line.search(
                            [('checked', '=', True), ('shipping', '=', this.id)])
                        if service:
                            this.method_name = delivery_method + \
                                               ', ' + service[0].service_type
                        else:
                            this.method_name = delivery_method
                    else:
                        this.method_name = delivery_method
            if this.ship_to_location == 'yes':
                if this.delivery_method:
                    delivery_method = this.delivery_method.name
                    this.method_name = delivery_method

    @api.depends('product_qty')
    def _compute_qty_int(self):
        for this in self:
            this.product_qty_int = this.product_qty

    @api.depends('supplier', 'finisher')
    def _compute_rush_notes(self):
        for this in self:
            if this.supplier.name != this.finisher:
                finisher_rush_note = this.finisher.name + ' - ' + \
                                     this.finisher.rush_notes if this.finisher.rush_notes else ''
                supplier_rush_note = this.supplier.name.name + ' - ' + \
                                     this.supplier.rush_notes if this.supplier.rush_notes else ''
                this.rush_notes = finisher_rush_note + "\n" + supplier_rush_note
            else:
                supplier_rush_note = this.supplier.name.name + ' - ' + \
                                     this.supplier.rush_notes if this.supplier.rush_notes else ''
                this.rush_notes = supplier_rush_note

    @api.one
    @api.depends('contact')
    def _compute_text_data(self):
        if self.contact:
            contact = self.contact
            name = contact.name if contact.name else ''
            s1 = contact.street if contact.street else ''
            s2 = contact.street2 if contact.street2 else '' 
            city = contact.city if contact.city else ''
            state = contact.state_id.name if contact.state_id.name else ''
            zipcode = contact.zip if contact.zip else ''
            self.contact_data = name+"\n"+s1+"\n"+s2+"\n"+city+" "+\
                                state+" "+zipcode

    #@api.one
    @api.onchange('partner_supplier')
    def onchange_partner_supplier(self):
        if self.partner_supplier:
            res = self.env['product.supplierinfo'].search([('name', '=', self.partner_supplier.id), ('product_tmpl_id', '=', self.tmpl_id.id)], limit=1)
            if res:
                self.supplier = res.id
            # else:
            #     self.supplier = False
            if not self.lock_dim:
                self.onchange_carton_dimension()

    name = fields.Char(compute='_compute_name', string='Product', copy=False)
    order = fields.Many2one('sale.order', copy=False)
    product_qty = fields.Float('Quantity', copy=True)
    product_line = fields.One2many('product.shipping.line', 'shipping_id', readonly=True, string="Product(s)",
                                   copy=True, ondelete="cascade")
    tmpl_id = fields.Many2one('product.template', copy=True)
    carton_dimension = fields.Boolean('Carton Dimensions', copy=True, default=True)
    carton_weight = fields.Float('Carton Weight', copy=True)
    carton_length = fields.Float('L', copy=True)
    carton_width = fields.Float('W', copy=True)
    carton_height = fields.Float('H', copy=True)
    items_per_carton = fields.Float('Items Per Carton', default=1, copy=True)
    firm_for_event = fields.Selection([('yes', 'Yes'), ('no', 'No')], 'Firm for Event', default='yes', copy=True)
    firm_date = fields.Date('Firm Date', copy=True)
    event_date = fields.Date('Event Date', copy=True)
    packaging = fields.Selection([('Test', 'Test')], copy=True)
    ship_to_location = fields.Selection([('yes', 'Yes'), ('no', 'No')],
                                        'Multiple Ship to Location', default='no', copy=True)
    ship_date = fields.Date('Ship Date', copy=True)
    delivery_date = fields.Date('Delivery Date', copy=True)
    delivery_line = fields.One2many('delivery.line', 'shipping', string="Delivery Line", copy=True, ondelete="cascade")
    zips = fields.Text('TO ZIPS', copy=True,
                       help="Provide for each location in all caps: 'ZIP Quantity SERVICE TYPE Country Code'")
    contact = fields.Many2one('res.partner', 'Ship to Location', default=_get_default_contact, copy=True)
    contact_data = fields.Text(compute='_compute_text_data')
    finisher = fields.Many2one('res.partner', string='Finisher (to)', copy=True, domain=[('is_finisher', '=', True)])
    tracking = fields.Char('Tracking No.', copy=True)
    fedex_droppoff_type = fields.Selection([('BUSINESS_SERVICE_CENTER', 'BUSINESS_SERVICE_CENTER'),
                                            ('DROP_BOX', 'DROP_BOX'),
                                            ('REGULAR_PICKUP', 'REGULAR_PICKUP'),
                                            ('REQUEST_COURIER', 'REQUEST_COURIER'),
                                            ('STATION', 'STATION')],
                                           string="Fedex Drop-off Type", default='REGULAR_PICKUP', copy=True)
    fedex_packaging_type = fields.Selection([('FEDEX_BOX', 'FEDEX_BOX'),
                                             ('FEDEX_10KG_BOX', 'FEDEX_10KG_BOX'),
                                             ('FEDEX_25KG_BOX', 'FEDEX_25KG_BOX'),
                                             ('FEDEX_ENVELOPE', 'FEDEX_ENVELOPE'),
                                             ('FEDEX_PAK', 'FEDEX_PAK'),
                                             ('FEDEX_TUBE', 'FEDEX_TUBE'),
                                             ('YOUR_PACKAGING', 'YOUR_PACKAGING')],
                                            string='Fedex Packaging Type', default='YOUR_PACKAGING', copy=True)
    delivery_method = fields.Many2one('delivery.carrier', 'Delivery Method', copy=True, default=_get_default_method)
    csv_file = fields.Binary('Split Shipment File', copy=True)
    csv_filename = fields.Char('CSV File name', copy=True)
    partner_supplier = fields.Many2one('res.partner', 'Supplier (from)', copy=True, domain=['|',('supplier', '=', True), ('is_finisher', '=', True)])
    supplier = fields.Many2one('product.supplierinfo', copy=True)
    account_number = fields.Char(copy=True)
    customer_account = fields.Boolean(copy=True)
    vendor_account = fields.Boolean(copy=True)
    product_qty_int = fields.Integer(compute="_compute_qty_int")
    is_finisher_line = fields.Boolean(copy=True)
    is_supplier_line = fields.Boolean(copy=True)
    is_decoration_disable = fields.Boolean(default=False, copy=True)
    shipping_sale_orders = fields.Many2many(
        "sale.order.line", 
        relation="sale_order_line_shipping_line_rel", 
        column1="shipping_line_id", 
        column2="sale_order_line_id", 
        string="Shipping line sale order", copy=True)
    company_id = fields.Many2one('res.company', 'Company',
                                 default=lambda self: self.env['res.company']._company_default_get())
    production_time = fields.Text('Production Time', compute='get_production_time')
    rush_notes = fields.Text(compute="_compute_rush_notes", store=True)
    rush_finisher = fields.Many2one('res.partner', string="Rush Finisher")
    rush_supplier = fields.Many2one('res.partner', string="Rush Supplier")
    decoration_line_id = fields.Many2one('sale.order.line.decoration')
    method_name = fields.Char(compute="_compute_method_name")
    address_name = fields.Char('Address Name', size=30)
    street1 = fields.Char('Street 1', size=30)
    street2 = fields.Char('Street 2', size=30)
    city = fields.Char('City', size=30)
    state_id = fields.Many2one('res.country.state', 'State')
    zipcode = fields.Char('Zip', size=10)
    other_contact_info = fields.Char('Other Contact Info', size=30)
    contact_country_id = fields.Many2one('res.country', 'Country', default=_get_default_contact_country_id)
    hide_shipping = fields.Boolean('Hide Shipping for Customer')
    inv_code = fields.Char('Inv Code')
    lock_dim = fields.Boolean(default=True)
    store_address = fields.Boolean(string="Save to Address Book")

    @api.multi
    def write(self, vals):
        if vals.get('store_address', False) and vals.get('store_address', False) == True:
            if self.contact:
                data = {'type': 'delivery', 'name': vals['address_name'], 'street': vals.get('street1', self.street1), 'street2': vals.get('street2', self.street2), 
                        'city': vals.get('city', self.city), 'state_id': vals.get('state_id', self.state_id.id), 'zip': vals.get('zipcode', self.zipcode), 'country_id': vals.get('contact_country_id', self.contact_country_id.id),
                        'parent_id': self.contact.id, 'customer': self.contact.customer, 'supplier': self.contact.supplier, 'is_finisher': self.contact.is_finisher}
                self.env['res.partner'].create(data)
        return super(ShippingLine, self).write(vals)

    @api.onchange('firm_date')
    def _onchange_firm_date(self):
        if self.firm_date and self.firm_date < fields.Date.today():
            raise Warning(_('Please note that the date you\'ve selected has already past.'))
            # raise UserError(_("Please note that the Firm Date you've selected has already past."))

    @api.onchange('event_date')
    def _onchange_event_date(self):
        if self.event_date and self.event_date < fields.Date.today():
            raise Warning(_('Please note that the date you\'ve selected has already past.'))
            # raise UserError(_("Please note that the Event Date you've selected has already past."))

    @api.onchange('ship_date')
    def _onchange_ship_date(self):
        if self.ship_date and self.ship_date < fields.Date.today():
            raise Warning(_('Please note that the date you\'ve selected has already past.'))
            # raise UserError(_("Please note that the Ship Date you've selected has already past."))

    @api.onchange('delivery_date')
    def _onchange_delivery_date(self):
        if self.delivery_date and self.delivery_date < fields.Date.today():
            raise Warning(_('Please note that the date you\'ve selected has already past.'))
            # raise UserError(_("Please note that the Delivery Date you've selected has already past."))

    @api.onchange('contact')
    def onchange_contact(self):
        if self.contact:
            if self.contact.parent_id:
                self.address_name = self.contact.parent_id.name
                self.other_contact_info = 'ATTN: '+self.contact.name
            else:
                self.other_contact_info = ''
                self.address_name = self.contact.name    
            
            self.street1 = self.contact.street
            self.street2 = self.contact.street2
            self.city = self.contact.city
            self.state_id = self.contact.state_id.id
            self.zipcode = self.contact.zip
            if self.contact.country_id:
                self.contact_country_id = self.contact.country_id.id
            else:
                country_rec = self.ref('base.us')
                if country_rec:
                    self.contact_country_id = country_rec.id

    # @api.multi
    # def unlink(self):
    #     print "::::::::::::::;in unlink of shipping:::::::::::::::"
    #     records = self.env['sale.order.line'].search([('is_delivery', '=', True), ('shipping_id', '=',  self.id)])
    #     if records:
    #         records.unlink()
    #     res = super(ShippingLine, self).unlink()
    #     return res

    @api.onchange('rush_supplier')
    def onchange_rush_supplier(self):
        if not self.partner_supplier.id:
            self.rush_supplier = False
        elif self.partner_supplier.id == self.finisher.id:
            self.rush_finisher = self.rush_supplier.id

    @api.onchange('rush_finisher')
    def onchange_rush_finisher(self):
        if not self.finisher.id:
            self.finisher = False
        elif self.supplier.name.id == self.finisher.id:
            self.rush_finisher = self.rush_supplier.id

    @api.multi
    def generate_rush_lines(self):
        product_rush_policy = self.env.ref('pinnacle_sales.product_product_rush_policy_price_update_change')
        if not product_rush_policy:
            UserError(_('Rush Over Policy Product Not Found'))

        sale_order_line = self.env['sale.order.line']
        for line in sale_order_line.search([('rush_shipping_id', '=', self.id)]):
            if line.rush_supplier.id != self.rush_supplier.id or line.rush_finisher.id != self.rush_finisher.id:
                line.unlink()

        product_name = " ".join(
            [line.product.product_sku + ',' for line in self.product_line])
        product_name += " - Rush Order"
        sale_order_line.create({'product_id': product_rush_policy.id,
                                'name': product_name,
                                'product_uom_qty': 1,
                                'product_uom': product_rush_policy.uom_id.id,
                                'price_unit': 0,
                                'purchase_price': 0,
                                'order_id': self.order.id,
                                'rush_supplier': self.rush_supplier.id,
                                'rush_finisher': self.rush_finisher.id,
                                'rush_shipping_id': self.id,
                                })

    @api.multi
    def check_account_number(self):
        if self.delivery_method:
            if self.customer_account:
                if self.account_number:
                    warning = {
                        'title': _('Error!'),
                        'message': _('Please enter a valid shipping account number.'),
                    }
                    if self.delivery_method.delivery_type == 'fedex':
                        if len(self.account_number) != 9:
                            self.account_number = None
                            return {'warning': warning}
                    if self.delivery_method.delivery_type == 'ups':
                        if len(self.account_number) != 6:
                            self.account_number = None
                            return {'warning': warning}
        else:
            self.account_number = None
            warning = {
                'title': _('Error!'),
                'message': _('Please Select Delivery Method'),
            }
            return {'warning': warning}

    @api.onchange('account_number')
    def onchange_account(self):
        return self.check_account_number()

    @api.multi
    def get_tax_lines(self, multiship=False):
        tax = []
        taxes = []
        tax_list = []
        if multiship:
            ziplines = self.create_zipline()
            zipcodes = [zipline['zipcode'] for zipline in ziplines]
            tax = self.env['account.tax'].search([('name', '=', zipcodes)], order='amount desc')
            if tax:
                tax_list.append(tax[0])
            if not tax:
                tax = self.env['account.tax'].search(
                    [('zero_value', '=', True)])
                if tax:
                    tax_list.append(tax)
            if tax_list:
                taxes.append(tax_list[0].id)
        else:
            taxes = [tax.id for tax in self.env['account.tax'].search([('name', '=', self.zipcode)])]
            if not taxes:
                tax = self.env['account.tax'].search([('zero_value', '=', True)])
                if tax:
                    taxes.append(tax.id)
        return taxes

    @api.multi
    def create_shipping_charges_line(self, lines, po_id, multiship=False):
        if po_id:
            self.env['purchase.order.line'].search(
                [('shipping_id', '=', self.id)]).unlink()
        else:
            self.env['sale.order.line'].search(
                [('shipping_id', '=', self.id), ('is_delivery', '=', True)]).unlink()
            if multiship:
                taxes = self.get_tax_lines(multiship)
        for line in lines:
            delivery = line.delivery_id
            if not multiship:
                taxes = self.get_tax_lines()
            if po_id:
                if not self.vendor_account:
                    return
                product_sku = self.tmpl_id.product_sku
                if multiship:
                    name = product_sku + ' - ' + line.zipcode + ' - ' + delivery.name + ', ' + line.service_type
                else:
                    name = product_sku + ' - ' + delivery.name + ', ' + line.service_type
                values = {
                    'order_id': po_id.id,
                    'name': name,
                    'product_qty': 1,
                    'product_uom': delivery.uom_id.id,
                    'product_id': delivery.product_id.id,
                    'product_sku': delivery.product_id.product_sku,
                    'shipping_id': self.id,
                    'price_unit': line.cost,
                    'product_variant_name': delivery.name,
                    #'taxes_id': [(6, 0, taxes)],
                    'product_sku': delivery.product_sku
                }
                res = self.env['purchase.order.line'].create(values)
                # products = [product.product.id for product in self.product_line]
                # po_lines = self.env['purchase.order.line'].search(
                #     [('order_id', '=', po_id.id), ('product_id', 'in', products)])
                # for line in po_lines:
                #     line.taxes_id = [(6, 0, taxes)]

            else:
                product_sku = self.tmpl_id.product_sku
                if multiship:
                    name = str(product_sku) + ' - ' + line.zipcode + ' - ' + delivery.name + ', ' + line.service_type
                else:
                    name = str(product_sku) + ' - ' + delivery.name + ', ' + line.service_type
                values = {
                    'order_id': self.order.id,
                    'name': name,
                    'product_uom_qty': 1,
                    'product_uom': delivery.uom_id.id,
                    'product_id': delivery.product_id.id,
                    'shipping_id': self.id,
                    'price_unit': line.price,
                    'purchase_price': line.cost,
                    'tax_id': [(6, 0, taxes)],
                    'is_delivery': True,
                }
                if self.order.order_line:
                    values['sequence'] = self.order.order_line[-1].sequence + 1
                res = self.env['sale.order.line'].create(values)
                group_obj = self.env['sale.order.line.decoration.group']
                for order_line in self.shipping_sale_orders:
                    order_line.tax_id = [(6, 0, taxes)]
                    for decoration in order_line.decorations:
                        for group in decoration.decoration_group_id:
                            for charge_line in group.charge_lines:
                                for order_line in charge_line.sale_order_lines:
                                    order_line.tax_id = [(6, 0, taxes)]

    @api.multi
    def update_order_line(self):
        lines = []
        if self.delivery_line:
            [lines.append(line) if line.checked else '' for line in self.delivery_line]
            po_id = self.purchase_order_id
            if self.ship_to_location == 'no':
                if len(lines) > 1:
                    raise ValidationError('You can select only one delivery service')
                else:
                    self.create_shipping_charges_line(lines, po_id)
            if self.ship_to_location == 'yes':
                self.create_shipping_charges_line(lines, po_id, True)

    @api.multi
    def create_zipline(self):
        if self.zips:
            zipline = self.zips.split('\n')
            if zipline:
                ziplines = []
                for zips in zipline:
                    line = {}
                    zipline1 = zips.split()
                    if len(zipline1) == 0:
                        continue
                    if len(zipline1) == 3 or len(zipline1) == 4:
                        line['zipcode'] = zipline1[0] or ''
                        line['qty'] = zipline1[1] or 1
                        line['type'] = zipline1[2] or ''
                        line['country'] = zipline1[3] if len(zipline1) == 4 else ''
                        # if len(line['zipcode']):
                        #     if line['type'] not in ['INTERNATIONAL_ECONOMY', 'INTERNATIONAL_PRIORITY', 'FEDEX_GROUND', 'FEDEX_2_DAY', 'FEDEX_2_DAY_AM', 'FIRST_OVERNIGHT', 'PRIORITY_OVERNIGHT', 'STANDARD_OVERNIGHT']:
                        #         raise UserError("Service Type is Incorrect!")
                        ziplines.append(line)
                    else:
                        raise UserError("Please enter all the data in required format to calculate the price!")
                return ziplines
        else:
            raise UserError("Please enter all the data in required format to calculate the price!")

    @api.multi
    def create_delivery_lines(self, delivery=None, zipline=None, zipcode=None, delivery_name=None, delivery_id=None,
                              service_type=None, price=0.0, cost=0.0, shipping=None, transit_time=None, flag=True):
        line_ids = []
        result = None
        warnings = ''
        if delivery:
            if delivery.delivery_type == 'fedex':
                if delivery.country_ids.code == 'US':
                    result = self.fedex_get_shipping_price_from_sp(
                        delivery=delivery, zipline=zipline, service_type=service_type)
                else:
                    if zipline:
                        result = [
                            {'price': {'USD': 0.0}, 'transit_time': '', 'service_type': service_type}]
                    else:
                        result = [{'price': {'USD': 0.0}, 'transit_time': '', 'service_type': 'INTERNATIONAL_PRIORITY'},
                                  {'price': {'USD': 0.0}, 'transit_time': '', 'service_type': 'INTERNATIONAL_ECONOMY'}]
            elif delivery.delivery_type == 'ups':
                if not zipline:
                    if delivery.country_ids.code == 'US':
                        result = [{'price': {'USD': 0.0}, 'transit_time': '', 'service_type': 'UPS_EXPRESS_CRITICAL'},
                                  {'price': {'USD': 0.0}, 'transit_time': '',
                                   'service_type': 'UPS_NEXT_DAY_AIR_EARLY'},
                                  {'price': {'USD': 0.0}, 'transit_time': '',
                                   'service_type': 'UPS_NEXT_DAY_AIR'},
                                  {'price': {'USD': 0.0}, 'transit_time': '',
                                   'service_type': 'UPS_NEXT_DAY_AIR_SAVER'},
                                  {'price': {'USD': 0.0}, 'transit_time': '',
                                   'service_type': 'UPS_2ND_DAY_AIR_A.M.'},
                                  {'price': {'USD': 0.0}, 'transit_time': '',
                                   'service_type': 'UPS_2ND_DAY_AIR'},
                                  {'price': {'USD': 0.0}, 'transit_time': '',
                                   'service_type': 'UPS_3_DAY_SELECT'},
                                  {'price': {'USD': 0.0}, 'transit_time': '',
                                   'service_type': 'UPS_GROUND'},
                                  ]
                    else:
                        result = [{'price': {'USD': 0.0}, 'transit_time': '', 'service_type': 'UPS_EXPRESS_CRITICAL'},
                                  {'price': {'USD': 0.0}, 'transit_time': '',
                                   'service_type': 'UPS_WORLDWIDE_EXPRESS_PLUS'},
                                  {'price': {'USD': 0.0}, 'transit_time': '',
                                   'service_type': 'UPS_WORLDWIDE_EXPRESS'},
                                  {'price': {'USD': 0.0}, 'transit_time': '',
                                   'service_type': 'UPS_WORLDWIDE_EXPRESS_FREIGHT'},
                                  {'price': {'USD': 0.0}, 'transit_time': '',
                                   'service_type': 'UPS_WORLDWIDE_SAVER'},
                                  {'price': {'USD': 0.0}, 'transit_time': '',
                                   'service_type': 'UPS_WORLDWIDE_EXPEDITED'},
                                  {'price': {'USD': 0.0}, 'transit_time': '',
                                   'service_type': 'UPS_STANDARD'},
                                  ]
                else:
                    result = [{'price': {'USD': 0.0}, 'transit_time': '', 'service_type': service_type}]
            elif delivery == self.env.ref('pinnacle_sales.delivery_carrier_freight'):
                if zipline:
                    try:
                        qty = float(zipline['qty'])
                    except ValueError:
                        raise UserError('Incorrect quantity')
                    if zipline['qty'] >= 1: 
                        result = [{'price': {'USD': 0.0}, 'transit_time': '', 'service_type': 'FREIGHT'}]
                else:
                    result = [{'price': {'USD': 0.0}, 'transit_time': '', 'service_type': 'FREIGHT'}]
            else:
                result = [{'price': {'USD': 0.0}, 'transit_time': '', 'service_type': ''}]
            if result:
                for res in result:
                    if 'errors_message' in res:
                        transit_time = res['errors_message']
                        price = 0.0
                    else:
                        if not self.customer_account and not self.vendor_account:
                            if 'price' in res:
                                cost = res['price'][self.company_id.currency_id.name]
                            if delivery.margin > 0:
                                price = cost + ((cost * delivery.margin) / 100)
                            else:
                                price = cost
                        transit_time = res['transit_time'] if 'transit_time' in res else ''
                        service_type = res['service_type'] if 'service_type' in res else ''
                        if delivery.delivery_type == 'fedex' and (service_type == 'FREIGHT' or service_type.endswith('FREIGHT')):
                            delivery_method = self.env.ref('pinnacle_sales.delivery_carrier_freight') or None
                            line_tuple = (0, 0, {
                                'zipcode': zipcode,
                                'delivery_name': 'Freight',
                                'delivery_id': delivery_method.id,
                                'service_type': 'FREIGHT',
                                'price': 0.0, 'cost': 0.0,
                                'shipping': shipping,
                                'transit_time': '',
                                'flag': flag
                            })
                            warnings = warnings + '\n' + 'This shipment for Zip '+zipcode+' requires freight or alternate shipping method.'
                            line_ids.append(line_tuple)
                            continue
                    line_tuple = (0, 0, {
                        'zipcode': zipcode,
                        'delivery_name': delivery_name,
                        'delivery_id': delivery_id,
                        'service_type': service_type,
                        'price': price, 'cost': cost,
                        'shipping': shipping,
                        'transit_time': transit_time,
                        'flag': flag
                    })
                    line_ids.append(line_tuple)
                return {'vals': line_ids, 'warning': warnings}
        else:
            raise ValidationError('Please select a delivery method')

    @api.multi
    def validate_zipcode(self, zipcode=False):
        if self.contact_country_id.code == 'US':
            if zipcode:
                if len(str(zipcode)) != 5:
                    raise ValidationError(str(zipcode) +' is not a valid zipcode')

    @api.multi
    def generate_delivery_lines(self):
        self.check_account_number()
        delivery = self.delivery_method
        price = 0.0
        cost = 0.0
        transit_time = 'x'
        ziplines = None
        line_ids = []
        warnings = ''
        self.delivery_line.search([('shipping', '=', self.id)]).unlink()
        if self.ship_to_location == 'yes':
            ziplines = self.create_zipline()
            if ziplines:
                for zipline in ziplines:
                    self.validate_zipcode(zipline['zipcode'])
                    line = self.create_delivery_lines(delivery=delivery, zipline=zipline, zipcode=zipline['zipcode'],
                                                      delivery_name=delivery.name, delivery_id=delivery.id,
                                                      service_type=zipline['type'], price=price, cost=cost,
                                                      shipping=self.id, transit_time=transit_time)
                    if line:
                        if len(line['vals']):
                            line_ids.append(line['vals'][0])
                        warnings = warnings+line['warning']
        if self.ship_to_location == 'no':
            self.validate_zipcode(self.zipcode)
            line_ids = self.create_delivery_lines(delivery=delivery, zipcode=self.zipcode,
                                                  delivery_name=delivery.name,
                                                  delivery_id=delivery.id, price=price, cost=cost, shipping=self.id,
                                                  transit_time=transit_time)
            warnings = warnings+line_ids['warning']
            line_ids = line_ids['vals']

        ctx = dict(self._context)
        self.update({'delivery_line': line_ids})
        self._cr.commit()
        if warnings != '':
            raise Warning(warnings)

    @api.multi
    def fedex_get_shipping_price_from_sp(self, delivery=None, zipline=None, service_type=None):
        order = self.order
        price = 0.0
        try:
            qty = float(zipline['qty']) if zipline else self.product_qty
        except ValueError:
            raise UserError('Incorrect quantity')

        carton_dim = {}
        try:
            package = math.ceil(qty / self.items_per_carton)
            carton_weight = self.carton_weight
            weight_value = math.ceil(package * carton_weight)
            carton_dim['weight'] = carton_weight
            carton_dim['length'] = self.carton_length
            carton_dim['height'] = self.carton_height
            carton_dim['width'] = self.carton_width
        except:
            raise UserError('Please input correct carton dimension')
        # else:
        #     if self.supplier:
        #         try:
        #             package = math.ceil(qty / self.supplier.items_per_cartoon)
        #             carton_weight = self.supplier.carton_weight
        #             weight_value = math.ceil(
        #                 package * self.supplier.carton_weight)
        #             carton_dim['weight'] = carton_weight
        #             carton_dim['length'] = self.supplier.carton_length
        #             carton_dim['height'] = self.supplier.carton_height
        #             carton_dim['width'] = self.supplier.carton_width
        #         except:
        #             raise UserError(
        #                 'Please configure carton dimension on supplier')
        #     else:
        #         raise UserError('Please select a supplier')
        if package > 25:
            return [{'price': {'USD': 0.0}, 'transit_time': '', 'service_type': 'FREIGHT'}]
        dropoff_type = self.fedex_droppoff_type if self.fedex_droppoff_type else delivery.fedex_droppoff_type
        packaging_type = self.fedex_packaging_type if self.fedex_packaging_type else delivery.fedex_packaging_type
        service_type = zipline['type'] if zipline else service_type
        if service_type:
            service_type = service_type.upper()
        # Authentication stuff
        srme = FedexRequestExt(request_type="rating",
                               prod_environment=delivery.prod_environment)
        superself = delivery.sudo()
        srme.web_authentication_detail(superself.fedex_developer_key,
                                       superself.fedex_developer_password)
        srme.client_detail(superself.fedex_account_number,
                           superself.fedex_meter_number)

        # Build basic rating request and set addresses
        srme.transaction_detail(order.name)
        srme.shipment_request(dropoff_type,
                              service_type,
                              packaging_type,
                              delivery.fedex_weight_unit)
        order_currency = order.currency_id
        srme.set_currency(order_currency.name)
        if self.is_decoration_disable or self.purchase_order_id.po_type in ['sample', 'non_billable'] or \
                (self.purchase_order_id.for_blank and not self.purchase_order_id.for_decoration):
            srme.set_shipper(self.partner_supplier, self.partner_supplier)
        else:
            srme.set_shipper(self.finisher, self.finisher)
        if zipline:
            srme.set_recipient_ext(self.contact, zipline, False)
        else:
            srme.set_recipient_ext(self.contact, False, self)
        srme.add_package_ext(int(package), carton_dim)
        # srme.add_package(weight_value, mode="rating")
        srme.set_master_package(weight_value, int(package))
        request = srme.rate(shipping=self)
        return request

    def get_product_shipping_line(self, shipping_line):
        ship_line = [ship.product.name for ship in shipping_line]
        ship_line_name = ", ".join(ship_line)
        return ship_line_name

    def get_account_for_report(self):
        if self.customer_account:
            return self.account_number
        elif self.vendor_account:
            return "Ship On Vendor Account"
        else:
            return '176585072'

    def get_finisher_shipping_detail(self, line, description=False):
        attribute_code = line.product.product_sku
        supplier = False
        qty = self.product_qty
        for o_line in self.purchase_order_id.order_line:
            if o_line.product_id.id == line.product.id:
                supplier = self.purchase_order_id.partner_id
                qty = o_line.product_qty
                break
        if not supplier:
            for supp in self.purchase_order_id.product_receives:
                if supp.product_id.id == line.product.id and supp.blank_provider:
                    supplier = supp.blank_provider
                    qty = supp.product_qty
                    break
        if supplier:
            product_sku, product_variant_name, description, product_name = line.product.product_tmpl_id.fetch_seller_all_info(supplier, line.product)
        else:
            product_name = line.product.product_tmpl_id.name
            product_variant_name = str(", ".join(
                [v.name for v in line.product.attribute_value_ids]))
        if description:
            return "%s - [%s] %s (%s)" % (int(qty), attribute_code, product_name, product_variant_name )    
        return "%s - %s, %s [%s]" % (int(qty), attribute_code, product_name, product_variant_name )

    def get_shipping_information_sale_order(self, line):
        attribute_code = line.product.product_sku
        qty = self.product_qty
        supplier = False
        for o_line in self.order.order_line:
            if o_line.product_id.id == line.product.id:
                supplier = self.order.partner_id
                qty = o_line.product_uom_qty
                break
        if supplier:
            attribute_code = line.product.product_tmpl_id.fetch_vendor_product_sku(supplier, line.product)
        return "%s - %s, %s" % (int(qty), attribute_code, line.product.name_get()[0][1])


def _convert_weight(weight, unit='KG'):
    ''' Convert picking weight (always expressed in KG)
        into the specified unit'''
    if unit == 'KG':
        return weight
    elif unit == 'LB':
        return weight / 0.45359237
    else:
        raise ValueError


class FedexRequestExt(FedexRequest):
    def set_recipient_ext(self, recipient_partner, zipline, line):
        if zipline:
            country = 'US'
            zipcode = zipline['zipcode']
            # if 'country' in zipline:
            #     country = zipline['country'] if zipline['country'] != '' else 'US'
            # FedexRequest.set_recipient(self, recipient_partner)
            self.RequestedShipment.Recipient.Address.PostalCode = zipcode
            self.RequestedShipment.Recipient.Address.CountryCode = country
        else:
            Contact = self.client.factory.create('Contact')
            Contact.PersonName = line.address_name if not line.address_name else ''
            Contact.CompanyName = line.address_name if recipient_partner.is_company else ''
            Contact.PhoneNumber = recipient_partner.phone or ''

            Address = self.client.factory.create('Address')
            Address.StreetLines = ('%s %s') % (line.street1 or '', line.street2 or '')
            Address.City = recipient_partner.city or ''
            Address.StateOrProvinceCode = line.state_id.code or ''
            Address.PostalCode = line.zipcode or ''
            Address.CountryCode = line.contact_country_id.code or ''

            self.RequestedShipment.Recipient.Contact = Contact
            self.RequestedShipment.Recipient.Address = Address

    def add_package_ext(self, package_count, carton_dim):
        package = self.client.factory.create('RequestedPackageLineItem')
        package_weight = self.client.factory.create('Weight')
        dimensions = self.client.factory.create('Dimensions')
        dimensions.Length = int(carton_dim['length'])
        dimensions.Width = int(carton_dim['width'])
        dimensions.Height = int(carton_dim['height'])
        dimensions.Units.value = 'IN'
        package_weight.Value = carton_dim['weight']
        package_weight.Units = self.RequestedShipment.TotalWeight.Units
        package.PhysicalPackaging = 'BOX'
        package.Weight = package_weight
        package.Dimensions = dimensions
        package.GroupPackageCount = package_count
        self.RequestedShipment.RequestedPackageLineItems.append(package)

    def rate(self, shipping=None):
        result = []
        formatted_response = {'price': {}}
        del self.ClientDetail.Region
        # print self.RequestedShipment
        try:
            self.response = self.client.service.getRates(WebAuthenticationDetail=self.WebAuthenticationDetail,
                                                         ClientDetail=self.ClientDetail,
                                                         TransactionDetail=self.TransactionDetail,
                                                         Version=self.VersionId,
                                                         RequestedShipment=self.RequestedShipment,
                                                         ReturnTransitAndCommit=True)
            # print self.response
            if (self.response.HighestSeverity != 'ERROR' and self.response.HighestSeverity != 'FAILURE'):
                if 'RateReplyDetails' in self.response:
                    for rating in self.response.RateReplyDetails:
                        formatted_response = {'price': {}}
                        formatted_response['price'][
                            rating.RatedShipmentDetails[0].ShipmentRateDetail.TotalNetFedExCharge.Currency] = \
                            rating.RatedShipmentDetails[
                                0].ShipmentRateDetail.TotalNetFedExCharge.Amount
                        if 'DeliveryDayOfWeek' in rating:
                            formatted_response[
                                'transit_time'] = rating.DeliveryDayOfWeek
                        elif 'TransitTime' in rating:
                            formatted_response[
                                'transit_time'] = rating.TransitTime
                        else:
                            formatted_response['transit_time'] = ''
                        formatted_response['service_type'] = rating.ServiceType
                        result.append(formatted_response)
                    if len(self.response.RateReplyDetails[0].RatedShipmentDetails) == 1:
                        if 'CurrencyExchangeRate' in self.response.RateReplyDetails[0].RatedShipmentDetails[
                            0].ShipmentRateDetail:
                            formatted_response['price'][self.response.RateReplyDetails[0].RatedShipmentDetails[
                                0].ShipmentRateDetail.CurrencyExchangeRate.FromCurrency] = \
                                self.response.RateReplyDetails[0].RatedShipmentDetails[
                                    0].ShipmentRateDetail.TotalNetFedExCharge.Amount / \
                                self.response.RateReplyDetails[0].RatedShipmentDetails[
                                    0].ShipmentRateDetail.CurrencyExchangeRate.Rate
                else:
                    errors_message = '\n'.join(
                        [("%s: %s" % (n.Code, n.Message)) for n in self.response.Notifications])
                    _logger.exception(errors_message)
                    raise UserError(errors_message)
            else:
                errors_message = '\n'.join([("%s: %s" % (n.Code, n.Message)) for n in self.response.Notifications if
                                            (n.Severity == 'ERROR' or n.Severity == 'FAILURE')])
                formatted_response['errors_message'] = errors_message
                _logger.exception(errors_message)
                raise UserError(errors_message)

            if any([n.Severity == 'WARNING' for n in self.response.Notifications]):
                warnings_message = '\n'.join(
                    [("%s: %s" % (n.Code, n.Message)) for n in self.response.Notifications if n.Severity == 'WARNING'])
                formatted_response['warnings_message'] = warnings_message

        except suds.WebFault as fault:
            formatted_response['errors_message'] = fault
            _logger.exception(fault)
            raise UserError(fault)
        except URLError:
            formatted_response['errors_message'] = "Fedex Server Not Found"
            _logger.exception("Fedex Server Not Found")
            raise UserError("Fedex Server Not Found")
        return result


class ProductFinisher(models.Model):
    _inherit = 'product.finisher'
    _rec_name = 'finisher_id'

class ProductDecorationCustomerArt(models.Model):
    _inherit = 'product.decoration.customer.art'

    sale_order_decoration_lines = fields.One2many(
        'sale.order.line.decoration', 'customer_art',
        'Sale Order Decoration Lines')

    decorations = fields.One2many(
        'sale.order.line.decoration', 'customer_art', string="Decorations")

    @api.multi
    def unlink(self):
        if self.decorations:
            raise UserError(_(
                "It can not be deleted because it is used in some decorations on sale order lines.So, if you really want to delete it then first you have to unset it from all decorations that used this customer art."))
        result = super(ProductDecorationCustomerArt, self).unlink()
        return result


class SaleOrderLineDecoration(models.Model):
    _name = 'sale.order.line.decoration'

    @api.one
    @api.depends('product_id')
    def _compute_product_variant_name(self):
        if self.art_id and self.art_id.manual_artworksheet:
            attribute_ids = self.art_line_id and self.art_line_id.n_product_id.attribute_value_ids or []
        else:
            attribute_ids = self.product_id.attribute_value_ids
        self.product_variant_name = str(
            ", ".join([v.name for v in attribute_ids]))

    @api.one
    @api.depends('product_id')
    def _compute_vendor_product_sku(self):
        if self.art_id and self.art_id.manual_artworksheet:
            for seller in self.art_line_id.n_product_id.seller_ids:
                if seller.name.id == self.art_line_id.supplier_id.id:
                    self.vendor_product_sku = seller.product_code
        else:
            for seller in self.product_id.seller_ids:
                if seller.name.id == self.sale_order_line_id.change_supplier.name.id:
                    self.vendor_product_sku = seller.product_code

    select = fields.Boolean(string="Select", default=False, copy=False)
    imprint_location = fields.Many2one(
        'decoration.location', string="Decoration Location", copy=True)
    imprint_method = fields.Many2one(
        'decoration.method', string="Decoration Method", copy=True)
    customer_art = fields.Many2one(
        'product.decoration.customer.art', string="Art (file or text)", copy=True)
    finisher = fields.Many2one('res.partner', string="Finisher", copy=True)
    pms_code = fields.Many2many(
        'product.decoration.pms.code',
        relation="pms_code_sale_order_line_rel",
        column1="sale_order_line_decoration_id",
        column2="product_decoration_pms_code_id",
        string="PMS Code", copy=True)
    stock_color = fields.Many2many(
        'product.decoration.stock.color',
        relation="stock_color_sale_order_line_rel",
        column1="sale_order_line_decoration_id",
        column2="product_decoration_stock_color_id",
        string="Decoration Color", copy=True)
    pms_color_code = fields.Char(copy=True, string="Vendor Decoration Color")
    stock_color_char = fields.Char(copy=True, string="Decoration Color")
    sale_order_line_id = fields.Many2one(
        'sale.order.line', string='Sale Order Line', ondelete="cascade", copy=False)
    sale_order_id = fields.Many2one(
        related='sale_order_line_id.order_id', string='Sale Order', store=True, copy=False)
    decoration_sequence = fields.Char(
        string='Decoration Line Number', readonly=True, copy=True)
    decoration_sequence_id = fields.Integer(
        string='Decoration Line Id', readonly=True, copy=True)
    art_id = fields.Many2one('art.production',
                             string='Art Production', copy=False)
    art_line_id = fields.Many2one('art.production.line', string='Art Production Line', copy=False)
    area = fields.Char('Max Imprint Area', copy=True)
    min_pt_size = fields.Char('Min. Pt. Size', copy=True)
    note_id = fields.Many2one('art.notes', 'Notes', copy=False)
    product_id = fields.Many2one( 
        related="sale_order_line_id.product_id", string='Product', store=True, copy=False)
    product_sku = fields.Char(
        related="product_id.product_sku", string="Product ID")
    vendor_product_sku = fields.Char(compute='_compute_vendor_product_sku', string='Vendor Product SKU', copy=False)
    product_variant_name = fields.Char(
        compute="_compute_product_variant_name", string="Variant")
    # message_posted = fields.Boolean('Message Posted')
    so_number = fields.Char(related='sale_order_line_id.order_id.name')
    sale_order_line_product_uom_qty = fields.Float(
        related='sale_order_line_id.product_uom_qty', readonly=True)
    sale_order_line_product_sku = fields.Char(
        related='sale_order_line_id.product_sku', readonly=True)
    sale_order_line_product_variant_name = fields.Char(
        related='sale_order_line_id.product_variant_name', readonly=True)
    decoration_group_id = fields.Many2one(
        'sale.order.line.decoration.group', string="Decoration Group", readonly=True, ondelete="set null", copy=False)

    vendor_decoration_location = fields.Many2one(
        'vendor.decoration.location', 'Vendor Imprint Location', copy=True)
    vendor_decoration_method = fields.Many2one(
        'vendor.decoration.method', 'Vendor Imprint Method', copy=True)
    vendor_prod_specific = fields.Char(
        'Vendor Method Product Specific', copy=True)
    is_reorder = fields.Boolean(
        string="Is Reorder ?", default=False, copy=True)
    reorder_ref = fields.Char(string='Reorder Reference (SO)', copy=True)
    reorder_ref_po = fields.Char(string='Reorder Reference (PO)', copy=True)


    _rec_name = 'decoration_sequence'

    @api.onchange('customer_art')
    def _onchange_customer_art(self):
        customer_arts = self.sale_order_line_id.customer_arts.ids
        return {'domain': {
            'customer_art': [('id', 'in', customer_arts)],
        }}

    @api.model
    def create(self, vals):
        result = super(SaleOrderLineDecoration, self).create(vals)
        if result.sale_order_line_id and result.sale_order_line_id.order_id and not result.decoration_sequence:
            if len(result.sale_order_line_id.order_id.decorations.search(
                    [('sale_order_id', '=', result.sale_order_line_id.order_id.id),
                     ('decoration_sequence_id', '!=', 0)])) == 0:
                result.sale_order_line_id.order_id.decoration_sequence_id = 1
                result.decoration_sequence = "D" + \
                                             str(result.sale_order_line_id.order_id.decoration_sequence_id)
                result.decoration_sequence_id = result.sale_order_line_id.order_id.decoration_sequence_id
                result.sale_order_line_id.order_id.decoration_sequence_id += 1
            elif len(result.sale_order_line_id.order_id.decorations.search(
                    [('sale_order_id', '=', result.sale_order_line_id.order_id.id), ('decoration_sequence_id', '!=',
                                                                                     0)])) == result.sale_order_line_id.order_id.decoration_sequence_id - 1:
                result.decoration_sequence = "D" + \
                                             str(result.sale_order_line_id.order_id.decoration_sequence_id)
                result.decoration_sequence_id = result.sale_order_line_id.order_id.decoration_sequence_id
                result.sale_order_line_id.order_id.decoration_sequence_id += 1
            else:
                index = 1
                already_exited_ids = [decoration.decoration_sequence_id for decoration in
                                      result.sale_order_line_id.order_id.decorations.search(
                                          [('sale_order_id', '=', result.sale_order_line_id.order_id.id),
                                           ('decoration_sequence_id', '!=', 0)])]
                while index <= result.sale_order_line_id.order_id.decoration_sequence_id - 1:
                    if index not in already_exited_ids:
                        result.decoration_sequence = "D" + str(index)
                        result.decoration_sequence_id = index
                        break
                    index += 1
        if result.art_id and not result.decoration_sequence:
            if len(result.art_id.decorations.search(
                    [('art_id', '=', result.art_id.id),
                     ('decoration_sequence_id', '!=', 0)])) == 0:
                result.art_id.decoration_sequence_id = 1
                result.decoration_sequence = "D" + \
                                             str(result.art_id.decoration_sequence_id)
                result.decoration_sequence_id = result.art_id.decoration_sequence_id
                result.art_id.decoration_sequence_id += 1
            elif len(result.art_id.decorations.search(
                    [('art_id', '=', result.art_id.id), ('decoration_sequence_id', '!=',
                                                                                     0)])) == result.art_id.decoration_sequence_id - 1:
                result.decoration_sequence = "D" + \
                                             str(result.art_id.decoration_sequence_id)
                result.decoration_sequence_id = result.art_id.decoration_sequence_id
                result.art_id.decoration_sequence_id += 1
            else:
                index = 1
                already_exited_ids = [decoration.decoration_sequence_id for decoration in
                                      result.art_id.decorations.search(
                                          [('art_id', '=', result.art_id.id),
                                           ('decoration_sequence_id', '!=', 0)])]
                while index <= result.art_id.decoration_sequence_id - 1:
                    if index not in already_exited_ids:
                        result.decoration_sequence = "D" + str(index)
                        result.decoration_sequence_id = index
                        break
                    index += 1
        return result

    @api.multi
    def write(self, vals): 
        self = self.exists()
        if 'imprint_method' in vals or 'finisher' in vals or 'imprint_location' in vals or 'pms_code' in vals or 'stock_color' in vals or 'customer_art' in vals:
            imprint_method = vals.get('imprint_method', False)
            imprint_location = vals.get('imprint_location', False)
            customer_art = vals.get('customer_art', False)
            finisher = vals.get('finisher', False)
            pms_code = vals.get('pms_code', False)
            stock_color = vals.get('stock_color', False)
            for record in self:
                any_one_changed = False
                if (imprint_method and record.imprint_method.id != imprint_method) or (
                            finisher and record.finisher.id != finisher) or (
                            imprint_location and record.imprint_location.id != imprint_location) or (
                            customer_art and record.customer_art.id != customer_art):
                    any_one_changed = True
                if pms_code and not any_one_changed and self.env.context.get('user_response_about_change_to_decoration', False):
                    if pms_code[0][0] == 6:
                        if len(record.pms_code.ids) != len(pms_code[0][2]):
                            any_one_changed = True
                        else:
                            for val in record.pms_code.ids:
                                if val not in pms_code[0][2]:
                                    any_one_changed = True
                if stock_color and not any_one_changed and self.env.context.get('user_response_about_change_to_decoration', False):
                    if stock_color[0][0] == 6:
                        if len(record.stock_color.ids) != len(stock_color[0][2]):
                            any_one_changed = True
                        else:
                            for val in record.stock_color.ids:
                                if val not in stock_color[0][2]:
                                    any_one_changed = True
                if any_one_changed:
                    if len(record.decoration_group_id.decorations) > 1:
                        if record.decoration_group_id.charge_lines:
                            record.decoration_group_id.charge_lines.unlink()
                        record.decoration_group_id = False
                    else:
                        record.decoration_group_id.unlink()
        result = super(SaleOrderLineDecoration, self).write(vals) 
        if vals.get('customer_art', False):
            art_files_extensions = []
            for record in self:
                try:
                    if record.customer_art and record.customer_art.art_filename:
                        filename = record.customer_art.art_filename.split(".")[-1].lower()
                        art_files_extensions.append(str(filename))
                except:
                    pass
            art_type = False
            for file_type in art_files_extensions:
                if file_type not in ["eps", "ai"]:
                    art_type = 'raster_recreate'
                break
            if art_type:
                for record in self:
                    if record.art_id and record.art_id.exists():
                        record.art_id.write({'art_type': art_type})
        return result

    @api.multi
    def unlink(self):
        sale_order_line_obj = self.env['sale.order.line']
        sale_order_line_decoration_archives = []
        art_wroksheets_to_be_archived = []
        decorations_ids_to_be_updated_for_artworksheet = []
        decoration_groups = []
        for record in self:
            if record.decoration_group_id:
                decoration_groups.append(record.decoration_group_id.id)
            if record.art_id and record.art_id.id not in art_wroksheets_to_be_archived:
                for art_line in record.art_id.art_lines:
                    for decoration in art_line.decorations:
                        if decoration.id not in self.ids:
                            decorations_ids_to_be_updated_for_artworksheet.append(decoration.id)
                art_wroksheets_to_be_archived.append(decoration.art_id.id)
        if art_wroksheets_to_be_archived:
            sale_order_line_decoration_archives = sale_order_line_obj.copying_data_of_art_wroksheets_to_be_archived(art_wroksheets_to_be_archived)
        result = super(SaleOrderLineDecoration, self).unlink()
        if not result:
            if sale_order_line_decoration_archives:
                self.env['sale.order.line.decoration.archive'].browse(sale_order_line_decoration_archives).unlink()
        if result:
            decoration_group_deleted = []
            for decoration_group in self.env['sale.order.line.decoration.group'].browse(decoration_groups):
                if len(decoration_group.decorations) < 1:
                    decoration_group_deleted.append(decoration_group.id)
                else:
                    if decoration_group.charge_lines:
                        decoration_group.charge_lines.unlink()
            if decoration_group_deleted:
                self.env['sale.order.line.decoration.group'].browse(
                    list(set(decoration_group_deleted))).unlink()
            if art_wroksheets_to_be_archived:
                sale_order_line_obj.actual_archiving_of_artworksheets_with_handling_of_other_data(art_wroksheets_to_be_archived)
            if decorations_ids_to_be_updated_for_artworksheet:
                sale_order_line_obj.updating_other_decorations_of_archiving_artworksheets(decorations_ids_to_be_updated_for_artworksheet)
        return result

    @api.multi
    def decoration_compare(self, other_decoration):
        if len(self) != len(other_decoration):
            return False
        else:
            index = 0
            for record in self:
                if record.imprint_location.id != other_decoration[index].imprint_location.id:
                    return False
                if record.imprint_method.id != other_decoration[index].imprint_method.id:
                    return False
                if record.customer_art.id != other_decoration[index].customer_art.id:
                    return False
                if record.finisher.id != other_decoration[index].finisher.id:
                    return False
                if len(record.pms_code.ids) != len(other_decoration[index].pms_code.ids):
                    return False
                else:
                    for val in record.pms_code.ids:
                        if val not in other_decoration[index].pms_code.ids:
                            return False
                if len(record.stock_color.ids) != len(other_decoration[index].stock_color.ids):
                    return False
                else:
                    for val in record.stock_color.ids:
                        if val not in other_decoration[index].stock_color.ids:
                            return False
                if record.vendor_decoration_method.id != other_decoration[index].vendor_decoration_method.id:
                    return False
                if record.vendor_decoration_location.id != other_decoration[index].vendor_decoration_location.id:
                    return False
                if record.vendor_prod_specific != other_decoration[index].vendor_prod_specific:
                    return False
                index += 1
            return True

    @api.multi
    def decoration_group_compare(self, other_decoration):
        if len(self) != len(other_decoration):
            return False
        else:
            index = 0
            for record in self:
                if record.imprint_method.id != other_decoration[index].imprint_method.id:
                    return False
                if record.finisher.id != other_decoration[index].finisher.id:
                    return False
                index += 1
            return True

    @api.multi
    def decoration_group_compare_for_calulating_charges(self, other_decoration):
        if len(self) != len(other_decoration):
            return False
        else:
            index = 0
            for record in self:
                if record.imprint_method.id != other_decoration[index].imprint_method.id:
                    return False
                if record.imprint_location.id != other_decoration[index].imprint_location.id:
                    return False
                if record.finisher.id != other_decoration[index].finisher.id:
                    return False
                if len(record.pms_code.ids) != len(other_decoration[index].pms_code.ids):
                    return False
                else:
                    for val in record.pms_code.ids:
                        if val not in other_decoration[index].pms_code.ids:
                            return False
                if len(record.stock_color.ids) != len(other_decoration[index].stock_color.ids):
                    return False
                else:
                    for val in record.stock_color.ids:
                        if val not in other_decoration[index].stock_color.ids:
                            return False
                index += 1
            return True

    @api.multi
    def art_notes_edit(self):
        self.ensure_one()
        context = self.env.context.copy()
        context.update({'cfs_attachment': True})
        note_rec = False
        if self:
            note_rec = self.note_id and self.note_id.id or False
        form_view_id = self.env.ref(
            'pinnacle_sales.art_notes_form_edit', False)
        return {
            'name': _('Additional Reference Files'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'art.notes',
            'views': [(form_view_id.id, 'form')],
            'view_id': form_view_id.id,
            'target': 'new',
            'res_id': note_rec,
            'context': context
        }

    def get_pms_code(self, pms_code):
        pms_code_name = [pms.name for pms in pms_code]
        pms_name = " ".join(pms_code_name)
        return pms_name

    def get_stock_color(self, stock_color):
        stk_color_name = [stk.name for stk in stock_color]
        stk_name = ", ".join(stk_color_name)
        return stk_name

    def get_additional_note(self, decoration):
        addi_name = ''
        if decoration.note_id:
            additional_note = [
                file.name for file in decoration.note_id.files if file.name]
            addi_name = ", ".join(additional_note)
        return addi_name

    def get_additional_desc(self, decoration):
        addi_desc = ''
        if decoration.note_id:
            additional_desc = [
                file.description for file in decoration.note_id.files if file.description]
            addi_desc = ", ".join(additional_desc)
        return addi_desc


# Archived Fields For Maintaining Sale Order Details When Art Work Sheet
class SaleOrderLineDecorationArchive(models.Model):
    _name = 'sale.order.line.decoration.archive'

    imprint_location = fields.Many2one(
        'decoration.location', string="Decoration Location", copy=True)
    imprint_method = fields.Many2one(
        'decoration.method', string="Decoration Method", copy=True)
    customer_art = fields.Many2one(
        'product.decoration.customer.art', string="Art (file or text)", copy=True)
    finisher = fields.Many2one('res.partner', string="Finisher", copy=True)
    pms_code = fields.Many2many(
        'product.decoration.pms.code',
        relation="pms_code_sale_order_line_rel_archive",
        column1="sale_order_line_decoration_id",
        column2="product_decoration_pms_code_id",
        string="PMS Code", copy=True)
    stock_color = fields.Many2many(
        'product.decoration.stock.color',
        relation="stock_color_sale_order_line_rel_archive",
        column1="sale_order_line_decoration_id",
        column2="product_decoration_stock_color_id",
        string="Decoration Color", copy=True)
    pms_color_code = fields.Char(copy=True, string="Vendor Decoration Color")
    stock_color_char = fields.Char(copy=True, string="Decoration Color")
    area = fields.Char('Max Imprint Area', copy=True)
    min_pt_size = fields.Char('Min. Pt. Size', copy=True)
    note_id = fields.Many2one('art.notes', 'Notes', copy=False)
    product_sku = fields.Char(string='Product ID', copy=False)
    art_id = fields.Many2one(
        'art.production', string='Art Production', copy=False)
    art_line_id = fields.Many2one(
        'art.production.line', string='Art Production Line', copy=False)
    product_variant_name = fields.Char(string="Variant")
    vendor_decoration_location = fields.Many2one(
        'vendor.decoration.location', 'Vendor Imprint Location', copy=True)
    vendor_decoration_method = fields.Many2one(
        'vendor.decoration.method', 'Vendor Imprint Method', copy=True)
    vendor_prod_specific = fields.Char(
        'Vendor Method Product Specific', copy=True)
    vendor_product_sku = fields.Char(string='Vendor Product SKU', copy=True)

    @api.multi
    def art_notes_edit(self):
        self.ensure_one()
        note_rec = False
        if self:
            note_rec = self.note_id and self.note_id.id or False
        form_view_id = self.env.ref(
            'pinnacle_sales.art_notes_form_readonly', False)
        return {
            'name': _('Additional Reference Files'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'art.notes',
            'views': [(form_view_id.id, 'form')],
            'view_id': form_view_id.id,
            'target': 'new',
            'res_id': note_rec
        }

    def get_pms_code(self, pms_code):
        pms_code_name = [pms.name for pms in pms_code]
        pms_name = " ".join(pms_code_name)
        return pms_name
        
    def get_additional_note(self, decoration):
        addi_name = ''
        if decoration.note_id:
            additional_note = [
                file.name for file in decoration.note_id.files if file.name]
            addi_name = ", ".join(additional_note)
        return addi_name

    def get_additional_desc(self, decoration):
        addi_desc = ''
        if decoration.note_id:
            additional_desc = [
                file.description for file in decoration.note_id.files if file.description]
            addi_desc = ", ".join(additional_desc)
        return addi_desc



class SaleOrderlineDecorationGroup(models.Model):
    _name = 'sale.order.line.decoration.group'

    select = fields.Boolean(string="Select", default=False, copy=False)
    group_sequence = fields.Char(string='Decoration Group', readonly=True, copy=True)
    group_id = fields.Char(string='Decoration Group ID', copy=True)
    decorations = fields.One2many('sale.order.line.decoration', 'decoration_group_id', string='Decorations', readonly=True, copy=False)
    order_id = fields.Many2one('sale.order', string='Sale Order', readonly=True, ondelete="cascade", copy=False)
    charge_lines = fields.One2many('sale.order.line.decoration.group.charge.line',
                                   'decoration_group_id', string='Charge Lines', copy=False)
    order_id_state = fields.Selection(related="order_id.state")
    sequence_id = fields.Integer(string='Sequence', copy=True)
    is_reorder = fields.Boolean(string="Is Reorder ?", default=False, copy=True)
    reorder_ref = fields.Char(string='Reorder Reference (SO)', copy=True)
    reorder_ref_id = fields.Many2one('sale.order', string="Reorder Link", compute="_compute_reorder_ref_id", store=True)
    reorder_ref_po = fields.Char(string='Reorder Reference (PO)', copy=False)
    reorder_ref_po_id = fields.Many2one('purchase.order', string="Reorder Link", compute="_compute_reorder_ref_po_id", store=True)
    reorder_ref_po_id1 = fields.Many2one('purchase.order', string="Reorder Link")
    reorder_ref_po_id2 = fields.Many2one('purchase.order', string="Reorder Link", compute="_compute_reorder_ref_po_id2", store=True)
    reorder_ref_so_id = fields.Many2one(related='order_id.reorder_ref')
    art_id = fields.Many2one('art.production', 'Art')
    _rec_name = 'group_sequence'

    @api.one
    @api.depends('reorder_ref')
    def _compute_reorder_ref_id(self):
        self.reorder_ref_id = False
        if self.reorder_ref:
            if self.env['sale.order'].search([('name', '=', self.reorder_ref)]):
                self.reorder_ref_id = self.env['sale.order'].search(
                    [('name', '=', self.reorder_ref)])[0].id

    @api.one
    @api.depends('reorder_ref_po', 'reorder_ref_po_id1')
    def _compute_reorder_ref_po_id(self):
        if self.reorder_ref_po:
            self.reorder_ref_po_id = False
            po = self.env['purchase.order'].search([('name', '=', self.reorder_ref_po)], limit=1)
            if po:
                self.reorder_ref_po_id = po.id

    @api.one
    @api.depends('reorder_ref_po', 'reorder_ref_po_id1')
    def _compute_reorder_ref_po_id2(self):
        if not self.reorder_ref_po and self.reorder_ref_po_id1:
            self.reorder_ref_po_id2 = self.reorder_ref_po_id1.id

    @api.onchange('reorder_ref')
    def _onchange_reorder_ref(self):
        if self.reorder_ref:
            pos = self.env['purchase.order'].search([('sale_order_id', '=', self.reorder_ref_id.id)])
            po_ids = [po.id for po in pos]
            domain = [('id', 'in', po_ids)]
            return {'domain': {'reorder_ref_po_id1': domain}}

    @api.onchange('is_reorder')
    def _onchange_is_reorder(self):
        if not self.is_reorder:
            self.reorder_ref = False
            self.reorder_ref_po = False

    @api.multi
    def edit_decoration_group(self):
        form_view_id = self.env.ref(
            'pinnacle_sales.sale_order_line_decoration_group_form_view')
        if self and form_view_id:
            return {'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'name': "Decoration Group",
                    'target': 'new',
                    'view_id': form_view_id.id,
                    'res_model': 'sale.order.line.decoration.group',
                    'res_id': self[0].id,
                    }

    @api.multi
    def view_decoration_group(self):
        form_view_id = self.env.ref(
            'pinnacle_sales.sale_order_line_decoration_group_form_view_readonly_view')
        if self and form_view_id:
            return {'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'name': "Decoration Group",
                    'target': 'new',
                    'view_id': form_view_id.id,
                    'res_model': 'sale.order.line.decoration.group',
                    'res_id': self[0].id,
                    }

    @api.multi
    def write_with_add_charges_order_lines(self, vals):
        result = self.write(vals)
        if result:
            charge_line_ids = []
            for charge_line in self.charge_lines:
                if charge_line.select:
                    charge_line_ids.append(charge_line.id)
                    charge_line.select = False
            for charge_line in self.env['sale.order.line.decoration.group.charge.line'].browse(charge_line_ids):
                if not charge_line.sale_order_lines:
                    self.env['sale.order.line'].create({
                        'product_id': charge_line.product_id.id,
                        'name': charge_line.name,
                        'product_uom_qty': charge_line.product_uom_qty,
                        'product_uom': charge_line.product_uom.id,
                        'price_unit': charge_line.price_unit,
                        'purchase_price': charge_line.purchase_price,
                        'order_id': self.order_id.id,
                        'decoration_charge_group_id': charge_line.id,
                    })
                else:
                    charge_line.sale_order_lines[0].write({
                        'product_id': charge_line.product_id.id,
                        'name': charge_line.name,
                        'product_uom_qty': charge_line.product_uom_qty,
                        'product_uom': charge_line.product_uom.id,
                        'price_unit': charge_line.price_unit,
                        'purchase_price': charge_line.purchase_price,
                        'order_id': self.order_id.id,
                        'decoration_charge_group_id': charge_line.id,
                    })
                charge_line.added_to_order_lines = 'YES'
            if charge_line_ids:
                return {
                    'type': 'ir.actions.client',
                    'tag': 'reload',
                }
        return result

    # 12631 : [DEV] SO & PO - Decoration Charges Popu
    # @api.multi
    # def add_charges_order_lines(self):
    #     charge_line_ids = []
    #     for charge_line in self.charge_lines:
    #         if charge_line.select:
    #             charge_line_ids.append(charge_line.id)
    #             charge_line.select = False
    #     if not charge_line_ids:
    #         raise UserError(_('Please select at least one calculated charges line to add it to sale order lines.'))
    #     for charge_line in self.env['sale.order.line.decoration.group.charge.line'].browse(charge_line_ids):
    #         if not charge_line.sale_order_lines:
    #             charge_line.sale_order_lines = [[0, 0, {
    #                 'product_id': charge_line.product_id.id,
    #                 'name': charge_line.name,
    #                 'product_uom_qty': charge_line.product_uom_qty,
    #                 'product_uom': charge_line.product_uom.id,
    #                 'price_unit': charge_line.price_unit,
    #                 'purchase_price': charge_line.purchase_price,
    #                 'order_id': self.order_id.id,
    #             }]]
    #         else:
    #             charge_line.sale_order_lines = [[1, charge_line.sale_order_lines[0].id, {
    #                 'product_id': charge_line.product_id.id,
    #                 'name': charge_line.name,
    #                 'product_uom_qty': charge_line.product_uom_qty,
    #                 'product_uom': charge_line.product_uom.id,
    #                 'price_unit': charge_line.price_unit,
    #                 'purchase_price': charge_line.purchase_price,
    #                 'order_id': self.order_id.id,
    #             }]]
    #         charge_line.added_to_order_lines = 'YES'
    #     return {
    #             'type': 'ir.actions.client',
    #             'tag': 'reload',
    #             }

    @api.multi
    def name_get(self):
        if self._context.get('dec_group'):
            return [(dec_group.id, '%s' % dec_group.group_id) for dec_group in self]
        return super(SaleOrderlineDecorationGroup, self).name_get()

    @api.multi
    def unlink(self):
        sale_order_lines = []
        for record in self:
            for charge_line in record.charge_lines.exists():
                if charge_line.sale_order_lines:
                    for sale_order_line in charge_line.sale_order_lines:
                        sale_order_lines.append(sale_order_line.id)
        result = super(SaleOrderlineDecorationGroup, self).unlink()
        if result:
            if sale_order_lines:
                sale_order_lines = list(set(sale_order_lines))
                if self.env['sale.order.line'].browse(sale_order_lines).exists():
                    self.env['sale.order.line'].browse(sale_order_lines).exists().unlink()
        return result

    @api.multi
    def recalculatea_reorder_setup_charges(self):
        product_setup_sale_price = self.env.ref(
            'pinnacle_sales.product_product_setup_sale_price')
        if not product_setup_sale_price:
            raise UserError(_('Setup Sale Price Product Not Found'))
        for charge_line in self.charge_lines:
            if charge_line.product_id.id == product_setup_sale_price.id and charge_line.at_least_one_decoration_recalculate:
                charge_line.price_unit = 0.00
                charge_line.purchase_price = 0.00
                finisher_decorations = charge_line.at_least_one_decoration_recalculate.sale_order_line_id.product_id.finisher_ids.search(
                    ['&', ('product_id', '=',
                           charge_line.at_least_one_decoration_recalculate.sale_order_line_id.product_id.product_tmpl_id.id),
                     ('finisher_id', '=', charge_line.at_least_one_decoration_recalculate.finisher.id)])
                ltm_policy_applied = False
                ltm_policy_price_unit = 0.00
                ltm_policy_purchase_price = 0.00
                # LTM Policy Calculation For Set-Up Price Start
                if charge_line.at_least_one_decoration_recalculate.sale_order_line_id.change_supplier.product_vendor_tier_ids and charge_line.at_least_one_decoration_recalculate.sale_order_line_id.change_supplier and (
                            (
                                        charge_line.at_least_one_decoration_recalculate.sale_order_line_id.change_supplier.moq and charge_line.at_least_one_decoration_recalculate.sale_order_line_id.change_supplier.moq != 'not_allowed') or (
                                    charge_line.at_least_one_decoration_recalculate.sale_order_line_id.change_supplier.o_moq and charge_line.at_least_one_decoration_recalculate.sale_order_line_id.change_supplier.o_moq != 'not_allowed')):
                    if charge_line.at_least_one_decoration_recalculate.sale_order_line_id.change_supplier.o_ltm_policy_id:
                        if charge_line.at_least_one_decoration_recalculate.sale_order_line_id.change_supplier.o_moq in [
                            'half_column', 'other']:
                            if (
                                    charge_line.at_least_one_decoration_recalculate.sale_order_line_id.change_supplier.product_vendor_tier_ids.sorted(
                                        key=lambda r: r.min_qty)[
                                        0].min_qty) > charge_line.at_least_one_decoration_recalculate.sale_order_line_id.product_uom_qty:
                                if charge_line.at_least_one_decoration_recalculate.sale_order_line_id.change_supplier.o_setup_cost == 'other':
                                    ltm_policy_purchase_price = charge_line.at_least_one_decoration_recalculate.sale_order_line_id.change_supplier.o_other_setup_cost
                                    ltm_policy_applied = True
                                if charge_line.at_least_one_decoration_recalculate.sale_order_line_id.change_supplier.o_setup_price == 'other':
                                    ltm_policy_price_unit = charge_line.at_least_one_decoration_recalculate.sale_order_line_id.change_supplier.o_other_setup_price
                                    ltm_policy_applied = True
                    else:
                        if charge_line.at_least_one_decoration_recalculate.sale_order_line_id.change_supplier.moq in [
                            'half_column', 'other']:
                            if (
                                    charge_line.at_least_one_decoration_recalculate.sale_order_line_id.change_supplier.product_vendor_tier_ids.sorted(
                                        key=lambda r: r.min_qty)[
                                        0].min_qty) > charge_line.at_least_one_decoration_recalculate.sale_order_line_id.product_uom_qty:
                                if charge_line.at_least_one_decoration_recalculate.sale_order_line_id.change_supplier.setup_cost == 'other':
                                    ltm_policy_purchase_price = charge_line.at_least_one_decoration_recalculate.sale_order_line_id.change_supplier.other_setup_cost
                                    ltm_policy_applied = True
                                if charge_line.at_least_one_decoration_recalculate.sale_order_line_id.change_supplier.setup_price == 'other':
                                    ltm_policy_price_unit = charge_line.at_least_one_decoration_recalculate.sale_order_line_id.change_supplier.other_setup_price
                                    ltm_policy_applied = True
                # LTM Policy Calculation For Set-Up Price End
                if finisher_decorations or (
                    charge_line.at_least_one_decoration_recalculate.finisher and charge_line.at_least_one_decoration_recalculate.finisher.finisher_ids):
                    finisher_finishing_types = False
                    if finisher_decorations:
                        finisher_decoration = finisher_decorations[0]
                        finisher_finishing_types = finisher_decoration.product_imprint_ids.search(
                            ['&', ('product_finisher_id', '=', finisher_decoration.id), '&', (
                                'decoration_location', '=',
                                charge_line.at_least_one_decoration_recalculate.imprint_location.id), (
                             'decoration_method', '=',
                             charge_line.at_least_one_decoration_recalculate.imprint_method.id)])
                    if not finisher_finishing_types:
                        # Pull Charges From Vendor If They Are Not Available At
                        # Product
                        if charge_line.at_least_one_decoration_recalculate.finisher:
                            finisher_finishing_types = charge_line.at_least_one_decoration_recalculate.finisher.finisher_ids.search(
                                ['&', ('partner_id', '=', charge_line.at_least_one_decoration_recalculate.finisher.id),
                                 '&', (
                                     'decoration_location', '=',
                                     charge_line.at_least_one_decoration_recalculate.imprint_location.id), (
                                 'decoration_method', '=',
                                 charge_line.at_least_one_decoration_recalculate.imprint_method.id)])
                    if not finisher_finishing_types:
                        # Pull Charges From Vendor By Only Cosidering Decoration Method
                        if charge_line.at_least_one_decoration_recalculate.finisher and charge_line.at_least_one_decoration_recalculate.sale_order_line_id.product_id and charge_line.at_least_one_decoration_recalculate.sale_order_line_id.product_id.seller_ids:
                            vendor_ids = [seller_id.name and seller_id.name.id for seller_id in charge_line.at_least_one_decoration_recalculate.sale_order_line_id.product_id.seller_ids]
                            if charge_line.at_least_one_decoration_recalculate.finisher.id in vendor_ids:
                                finisher_finishing_types = charge_line.at_least_one_decoration_recalculate.finisher.finisher_ids.search(['&',('partner_id','=',charge_line.at_least_one_decoration_recalculate.finisher.id),('decoration_method','=',charge_line.at_least_one_decoration_recalculate.imprint_method.id)]) 
                    if finisher_finishing_types:
                        finisher_finishing_type = finisher_finishing_types[0]
                        if not ltm_policy_applied:
                            product_setup_sale_price.list_price = finisher_finishing_type.reorder_set_up_sale_price
                            charge_line.price_unit = finisher_finishing_type.reorder_set_up_sale_price
                            charge_line.purchase_price = finisher_finishing_type.reorder_set_up_fee_cost
                            charge_line.name = string.replace(
                                charge_line.name, 'Setup', 'Reorder Setup')
                            charge_line.name_second = string.replace(
                                charge_line.name_second, 'Setup', 'Reorder Setup')
                        else:
                            product_setup_sale_price.list_price = ltm_policy_price_unit
                            charge_line.price_unit = ltm_policy_price_unit
                            charge_line.purchase_price = ltm_policy_purchase_price
                            charge_line.name = string.replace(
                                charge_line.name, 'Setup', 'Reorder Setup')
                            charge_line.name_second = string.replace(
                                charge_line.name_second, 'Setup', 'Reorder Setup')
        form_view_id = self.env.ref(
            'pinnacle_sales.sale_order_line_decoration_group_form_view')
        if self and form_view_id:
            return {'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'name': "Decoration Group",
                    'target': 'new',
                    'view_id': form_view_id.id,
                    'res_model': 'sale.order.line.decoration.group',
                    'res_id': self[0].id,
                    }

class SaleOrderlineDecorationGroupChargeLine(models.Model):
    _name = 'sale.order.line.decoration.group.charge.line'

    select = fields.Boolean(string="Select", default=False, copy=False)
    name = fields.Text(string='Decoration Order Line Description', copy=True)
    name_second = fields.Text(string='Description', readonly=True, copy=True)
    product_id = fields.Many2one('product.product', string='Product', domain=[(
        'sale_ok', '=', True)], change_default=True, ondelete='restrict', required=True, copy=True)
    product_uom_qty = fields.Float(string='Quantity', digits=dp.get_precision(
        'Product Unit of Measure'), required=True, default=1.0, copy=True)
    product_uom = fields.Many2one(
        'product.uom', string='Unit of Measure', required=True, copy=True)
    price_unit = fields.Float('Price', required=True, digits=dp.get_precision(
        'Product Price'), default=0.0, copy=True)
    purchase_price = fields.Float('Cost', required=True, digits=dp.get_precision(
        'Vendor Level Cost'), default=0.0, copy=True)
    decoration_group_id = fields.Many2one(
        'sale.order.line.decoration.group', string="Decoration Group", ondelete="cascade", copy=False)
    added_to_order_lines = fields.Selection(selection=[(
        'NO', 'NO'), ('YES', 'YES')], string='Added To Order Lines', default="NO", copy=True)
    sale_order_lines = fields.One2many(
        'sale.order.line', 'decoration_charge_group_id', string="Sale Order Lines", copy=False)
    recalculate = fields.Boolean(
        compute="_compute_recalculate", string='Recalculate Charge Line')
    at_least_one_decoration_recalculate = fields.Many2one(
        'sale.order.line.decoration', string='Decoration', copy=False)
    order_id_state = fields.Selection(
        related="decoration_group_id.order_id_state", store=True)
    run_charge_multiplied = fields.Integer('Run Charge Multiply', copy=True)

    @api.multi
    def unlink(self):
        sale_order_lines = []
        for record in self:
            if record.sale_order_lines:
                for sale_order_line in record.sale_order_lines:
                    sale_order_lines.append(sale_order_line.id)
        result = super(SaleOrderlineDecorationGroupChargeLine, self).unlink()
        if result:
            if sale_order_lines:
                sale_order_lines = list(set(sale_order_lines))
                self.env['sale.order.line'].browse(sale_order_lines).unlink()
        return result

    @api.one
    @api.depends('product_id')
    def _compute_recalculate(self):
        product_run_charge = self.env.ref(
            'pinnacle_sales.product_product_run_charge')
        self.recalculate = False
        if product_run_charge:
            if product_run_charge.id == self.product_id.id:
                self.recalculate = True

    @api.multi
    def pull_in_decoration_charge(self):
        self.ensure_one()
        result = self.with_context(
            pull_in=True).recalculate_decoration_charge()
        return result

    @api.multi
    def recalculate_decoration_charge(self):
        self.ensure_one()
        from_pull_in_button = False
        if 'pull_in' in self.env.context:
            from_pull_in_button = self.env.context['pull_in']
        product_run_charge = self.env.ref(
            'pinnacle_sales.product_product_run_charge')
        if not product_run_charge:
            raise UserError(_('Run Charge Product Not Found'))
        if self.decoration_group_id and self.product_id.id == product_run_charge.id and self.at_least_one_decoration_recalculate:
            finisher_decorations = self.at_least_one_decoration_recalculate.sale_order_line_id.product_id.finisher_ids.search(
                ['&', ('product_id', '=',
                       self.at_least_one_decoration_recalculate.sale_order_line_id.product_id.product_tmpl_id.id),
                 ('finisher_id', '=', self.at_least_one_decoration_recalculate.finisher.id)])
            if finisher_decorations or (
                self.at_least_one_decoration_recalculate.finisher and self.at_least_one_decoration_recalculate.finisher.finisher_ids):
                finisher_finishing_types = False
                if finisher_decorations:
                    finisher_decoration = finisher_decorations[0]
                    finisher_finishing_types = finisher_decoration.product_imprint_ids.search(
                        ['&', ('product_finisher_id', '=', finisher_decoration.id), '&', (
                            'decoration_location', '=', self.at_least_one_decoration_recalculate.imprint_location.id),
                         ('decoration_method', '=', self.at_least_one_decoration_recalculate.imprint_method.id)])
                pull_charges_from_vendor = False
                if not finisher_finishing_types:
                    # Pull Charges From Vendor If They Are Not Available At
                    # Product
                    if self.at_least_one_decoration_recalculate.finisher:
                        pull_charges_from_vendor = True
                        finisher_finishing_types = self.at_least_one_decoration_recalculate.finisher.finisher_ids.search(
                            ['&', ('partner_id', '=', self.at_least_one_decoration_recalculate.finisher.id), '&', (
                                'decoration_location', '=',
                                self.at_least_one_decoration_recalculate.imprint_location.id),
                             ('decoration_method', '=', self.at_least_one_decoration_recalculate.imprint_method.id)])
                if not finisher_finishing_types:
                    # Pull Charges From Vendor By Only Cosidering Decoration Method
                    if self.at_least_one_decoration_recalculate.finisher and self.at_least_one_decoration_recalculate.sale_order_line_id.product_id and self.at_least_one_decoration_recalculate.sale_order_line_id.product_id.seller_ids:
                        vendor_ids = [seller_id.name and seller_id.name.id for seller_id in self.at_least_one_decoration_recalculate.sale_order_line_id.product_id.seller_ids]
                        if self.at_least_one_decoration_recalculate.finisher.id in vendor_ids:
                            pull_charges_from_vendor = True
                            finisher_finishing_types = self.at_least_one_decoration_recalculate.finisher.finisher_ids.search(['&',('partner_id','=',self.at_least_one_decoration_recalculate.finisher.id),('decoration_method','=',self.at_least_one_decoration_recalculate.imprint_method.id)]) 
                if finisher_finishing_types:
                    finisher_finishing_type = finisher_finishing_types[0]
                    if from_pull_in_button:
                        quantity_multiplier = (len(self.at_least_one_decoration_recalculate.stock_color.ids) + len(
                            self.at_least_one_decoration_recalculate.pms_code.ids))
                    else:
                        if pull_charges_from_vendor:
                            quantity_multiplier = (len(self.at_least_one_decoration_recalculate.stock_color.ids) + len(
                                self.at_least_one_decoration_recalculate.pms_code.ids))
                        else:
                            quantity_multiplier = (len(self.at_least_one_decoration_recalculate.stock_color.ids) + len(
                                self.at_least_one_decoration_recalculate.pms_code.ids) - finisher_finishing_type.colors_included)
                    if finisher_finishing_type.run_charges == 'quantity' and finisher_finishing_type.tier_catalog_qty_ids:
                        tier_catalog_qty_index = False
                        tier_catalog_qty = False
                        for tier_catalog_qty_id in finisher_finishing_type.tier_catalog_qty_ids.sorted(
                                key=lambda r: r.quantity):
                            if tier_catalog_qty_id.quantity <= self.product_uom_qty:
                                tier_catalog_qty_index = list(finisher_finishing_type.tier_catalog_qty_ids.sorted(
                                    key=lambda r: r.quantity)).index(tier_catalog_qty_id)
                        if tier_catalog_qty_index:
                            tier_catalog_qty = \
                                finisher_finishing_type.tier_catalog_qty_ids.sorted(key=lambda r: r.quantity)[
                                    tier_catalog_qty_index]
                        if tier_catalog_qty:
                            self.price_unit = tier_catalog_qty.sale_price if quantity_multiplier > 0 else 0.00
                            self.purchase_price = tier_catalog_qty.cost if quantity_multiplier > 0 else 0.00
                        else:
                            tier_catalog_qty = \
                                finisher_finishing_type.tier_catalog_qty_ids.sorted(
                                    key=lambda r: r.quantity)[0]
                            self.price_unit = (tier_catalog_qty and (
                                tier_catalog_qty.sale_price) or 0.00) if quantity_multiplier > 0 else 0.00
                            self.purchase_price = (
                                tier_catalog_qty and (
                                tier_catalog_qty.cost) or 0.00) if quantity_multiplier > 0 else 0.00
                    elif finisher_finishing_type.run_charges == 'color' and finisher_finishing_type.tier_catalog_color_ids:
                        tier_catalog_color_index = False
                        tier_catalog_color = False
                        if from_pull_in_button:
                            for tier_catalog_color_id in finisher_finishing_type.tier_catalog_color_ids.sorted(
                                    key=lambda r: r.quantity):
                                if tier_catalog_color_id.quantity <= self.product_uom_qty and int(
                                        tier_catalog_color_id.color) == (
                                            len(self.at_least_one_decoration_recalculate.stock_color.ids) + len(
                                            self.at_least_one_decoration_recalculate.pms_code.ids)):
                                    tier_catalog_color_index = list(
                                        finisher_finishing_type.tier_catalog_color_ids.sorted(
                                            key=lambda r: r.quantity)).index(tier_catalog_color_id)
                            if tier_catalog_color_index:
                                tier_catalog_color = \
                                    finisher_finishing_type.tier_catalog_color_ids.sorted(key=lambda r: r.quantity)[
                                        tier_catalog_color_index]
                            if tier_catalog_color:
                                self.price_unit = tier_catalog_color.sale_price
                                self.purchase_price = tier_catalog_color.cost
                            else:
                                for tier_catalog_color_id in finisher_finishing_type.tier_catalog_color_ids.sorted(
                                        key=lambda r: r.quantity):
                                    if int(tier_catalog_color_id.color) == (
                                                len(self.at_least_one_decoration_recalculate.stock_color.ids) + len(
                                                self.at_least_one_decoration_recalculate.pms_code.ids)):
                                        tier_catalog_color = tier_catalog_color_id
                                        break
                                self.price_unit = tier_catalog_color and tier_catalog_color.sale_price or 0.0
                                self.purchase_price = tier_catalog_color and tier_catalog_color.cost or 0.0
                        else:
                            for tier_catalog_color_id in finisher_finishing_type.tier_catalog_color_ids.sorted(
                                    key=lambda r: r.quantity):
                                if pull_charges_from_vendor:
                                    if tier_catalog_color_id.quantity <= self.product_uom_qty and int(
                                            tier_catalog_color_id.color) == (
                                        len(self.at_least_one_decoration_recalculate.stock_color.ids) + len(
                                            self.at_least_one_decoration_recalculate.pms_code.ids)):
                                        tier_catalog_color_index = list(
                                            finisher_finishing_type.tier_catalog_color_ids.sorted(
                                                key=lambda r: r.quantity)).index(tier_catalog_color_id)
                                else:
                                    if tier_catalog_color_id.quantity <= self.product_uom_qty and int(
                                            tier_catalog_color_id.color) == (
                                            len(self.at_least_one_decoration_recalculate.stock_color.ids) + len(
                                                self.at_least_one_decoration_recalculate.pms_code.ids) - finisher_finishing_type.colors_included):
                                        tier_catalog_color_index = list(
                                            finisher_finishing_type.tier_catalog_color_ids.sorted(
                                                key=lambda r: r.quantity)).index(tier_catalog_color_id)
                            if tier_catalog_color_index:
                                tier_catalog_color = \
                                    finisher_finishing_type.tier_catalog_color_ids.sorted(key=lambda r: r.quantity)[
                                        tier_catalog_color_index]
                            if tier_catalog_color:
                                self.price_unit = tier_catalog_color.sale_price
                                self.purchase_price = tier_catalog_color.cost
                            else:
                                for tier_catalog_color_id in finisher_finishing_type.tier_catalog_color_ids.sorted(
                                        key=lambda r: r.quantity):
                                    if pull_charges_from_vendor:
                                        if int(tier_catalog_color_id.color) == (
                                            len(self.at_least_one_decoration_recalculate.stock_color.ids) + len(
                                                self.at_least_one_decoration_recalculate.pms_code.ids)):
                                            tier_catalog_color = tier_catalog_color_id
                                            break
                                    else:
                                        if int(tier_catalog_color_id.color) == (
                                                len(self.at_least_one_decoration_recalculate.stock_color.ids) + len(
                                                    self.at_least_one_decoration_recalculate.pms_code.ids) - finisher_finishing_type.colors_included):
                                            tier_catalog_color = tier_catalog_color_id
                                            break
                                self.price_unit = tier_catalog_color and tier_catalog_color.sale_price or 0.0
                                self.purchase_price = tier_catalog_color and tier_catalog_color.cost or 0.0
                    elif finisher_finishing_type.run_charges == 'teir' and finisher_finishing_type.tier_catalog_name_ids:
                        product_vendor_tier_seq = False
                        if finisher_decoration.finisher_id:
                            for seller_id in self.at_least_one_decoration_recalculate.sale_order_line_id.product_id.product_tmpl_id.seller_ids:
                                if seller_id.name.id == finisher_decoration.finisher_id.id:
                                    if seller_id.product_vendor_tier_ids:
                                        for product_vendor_tier_id in seller_id.product_vendor_tier_ids.sorted(
                                                key=lambda r: r.min_qty):
                                            if self.run_charge_multiplied > 0:
                                                if product_vendor_tier_id.min_qty <= (
                                                            self.product_uom_qty / self.run_charge_multiplied):
                                                    product_vendor_tier_seq = product_vendor_tier_id.tier_sequence
                                        if not product_vendor_tier_seq:
                                            product_vendor_tier_seq = \
                                                seller_id.product_vendor_tier_ids.sorted(key=lambda r: r.min_qty)[
                                                    0].tier_sequence
                        if product_vendor_tier_seq:
                            for tier_catalog_name_id in finisher_finishing_type.tier_catalog_name_ids:
                                if product_vendor_tier_seq == tier_catalog_name_id.tier_sequence:
                                    self.price_unit = tier_catalog_name_id.sale_price if quantity_multiplier > 0 else 0.00
                                    self.purchase_price = tier_catalog_name_id.cost if quantity_multiplier > 0 else 0.00
                        else:
                            self.price_unit = 0.0
                            self.purchase_price = 0.0
                    else:
                        self.price_unit = 0.0
                        self.purchase_price = 0.0
        form_view_id = self.env.ref(
            'pinnacle_sales.sale_order_line_decoration_group_form_view')
        if self and self[0].decoration_group_id and form_view_id:
            return {'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'name': "Decoration Group",
                    'target': 'new',
                    'view_id': form_view_id.id,
                    'res_model': 'sale.order.line.decoration.group',
                    'res_id': self[0].decoration_group_id.id,
                    }


class ArtRecreateInfo(models.Model):
    _name = 'art.recreate.info'
    _description = 'Art Recreate Information'

    @api.one
    @api.depends('file')
    def _get_image_from_url(self):
        if self.file_url:
            cfs_url = str(self.env['ir.config_parameter'].get_param('cfs.web.url'))
            final_url = cfs_url + self.file_url
            url = url_fix(final_url)
            try:
                self.file_preview = base64.encodestring(urllib2.urlopen(url).read())
            except Exception as e:
                self.file_preview = ''

    name = fields.Char('Logo Name')
    aws_link = fields.Char('AWS Virtual Link')
    aws_recreate_link = fields.Char('AWS Recreate Link')
    file = fields.Binary('File', attachment=True)
    file_preview = fields.Binary('File', compute='_get_image_from_url')
    file_att = fields.Many2one(
        'ir.attachment', 'File Att', copy=True)
    file_url = fields.Char(
        related='file_att.url', store=True, string='File')
    notes = fields.Char('Notes')
    filename = fields.Char('Filename')
    art_id = fields.Many2one('art.production', 'Art Production')

    @api.model
    def create(self, vals):
        res = super(ArtRecreateInfo, self).create(vals)
        notes = self.create_note_create(vals)
        if notes:
            # Report as a mail.message
            attach_ids = []
            allow_pdf_generate = self.env['ir.values'].get_default('base.config.settings', 'pdf_generate_enable')

            if allow_pdf_generate:
                report_name = 'Art Worksheet'
                ctx = self._context.copy()
                ctx.update({'active_model': 'art.production',
                            'active_ids': [res.art_id.id], 'active_id': res.art_id.id})
                result, format = self.env['report'].sudo().with_context(ctx).get_pdf([res.art_id.id],'pinnacle_sales.report_art_worksheet'), 'pdf'
                ext = "." + format
                if not report_name.endswith(ext):
                    report_name += ext
                attach_ids.append((report_name, result))
            res.art_id.message_post(body=notes, attachments=attach_ids, message_type='comment')
        return res

    @api.multi
    def write(self, vals):
        notes = self.create_note_write(vals)
        res = super(ArtRecreateInfo, self).write(vals)
        for rec in self:
            if notes:
                # Report as a mail.message
                attach_ids = []
                allow_pdf_generate = self.env['ir.values'].get_default('base.config.settings', 'pdf_generate_enable')
                if allow_pdf_generate:
                    report_name = 'Art Worksheet'
                    ctx = self._context.copy()
                    ctx.update({'active_model': 'art.production',
                                'active_ids': [rec.art_id.id], 'active_id': rec.art_id.id})
                    
                    result, format = self.env['report'].sudo().with_context(ctx).get_pdf([rec.art_id.id],
                                                                                         'pinnacle_sales.report_art_worksheet'), 'pdf'
                    ext = "." + format
                    if not report_name.endswith(ext):
                        report_name += ext
                    attach_ids.append((report_name, result))
                rec.art_id.message_post(body=notes, attachments=attach_ids, message_type='comment')
        return res

    def create_note_create(self, vals):
        fields = self.fields_get()
        new_note = ""
        new_note += "In Recreate Information new input generated <br/>" % ()
        black_field_list = ['file', 'file_preview', 'file_att']
        for val in vals:
            if not fields.has_key(val) or val in black_field_list:
                continue
            if fields[val]['type'] not in ['one2many']:
                if fields[val]['type'] in ['many2one']:
                    new_value = vals[val]
                    model = self.env[fields[val]['relation']].browse(new_value)
                    new_value = model.name_get()
                    if new_value:
                        new_value = new_value[0][1]
                    else:
                        new_value = False
                else:
                    new_value = vals[val]
                if new_value:
                    new_note += "<b>%s: </b>%s. <br />" % (fields[val]['string'], new_value)
        return new_note

    def create_note_write(self, vals):
        fields = self.fields_get()
        new_note = "In Recreate Information. <br /> "
        black_field_list = ['file', 'file_preview', 'file_att']
        for val in vals:
            if not fields.has_key(val) or val in black_field_list:
                continue
            if fields[val]['type'] not in ['one2many']:
                if fields[val]['type'] in ['many2one']:
                    old_value = self.read([val])[0][val]
                    new_value = vals[val]
                    if old_value:
                        old_value = old_value[1]
                    model = self.env[fields[val]['relation']].browse(new_value)
                    new_value = model.name_get()
                    if new_value:
                        new_value = new_value[0][1]
                    else:
                        new_value = False
                else:
                    old_value = self.read([val])[0][val]
                    new_value = vals[val]
                new_note += " <b>%s: </b>" % (fields[val]['string'])
                new_note += "<br /><span >Change to <b>%s</b>, Previously it was <b>%s</b>.</span> <br />" % (
                new_value, old_value)
        return new_note


class ArtProduction(models.Model):
    _name = 'art.production'
    _inherit = ['mail.thread']
    _description = 'Art Worksheet'
    _order = 'create_date desc, id desc'

    @api.one
    def _get_art_worksheet_name(self):
        name = 'Art Worksheet '
        if self.job:
            name +=' ' + str(self.art_sequence)
        self.name = name

    @api.multi
    def _get_default_status(self):
        if self.env.context.get('default_manual_artworksheet', False):
            return 'store_image'

    # @api.multi
    # def write(self, vals):
    #     if vals.get('artwork_instruction', False):
    #         vals['artwork_message_post'] = True
    #         vals['message_posted'] = False
    #     if vals.get('art_type', False):
    #         vals['art_type_post'] = True
    #         vals['message_posted'] = False
    #     if vals.get('status', False):
    #         vals['artwork_request_post'] = True
    #         vals['message_posted'] = False
    #     return super(ArtProduction, self).write(vals)

    @api.one
    @api.depends('sale_id', 'artworksheet_number_for_sale_order', 'state')
    def _compute_art_sequence(self):
        if self.manual_artworksheet:
            self.art_sequence = 'AWS-' + str(self.id)
        else:
            if self.state == 'archive':
                sale_name = self.archive_sale_id or ''
            else:
                sale_name = self.sale_id and self.sale_id.name or ''
            self.art_sequence = (sale_name + '-' + (str(self.artworksheet_number_for_sale_order) or ''))

    name = fields.Char(compute='_get_art_worksheet_name',
                       string='Name', copy=True)
    status = fields.Selection([('new_order', 'New Order'),
                               ('reorder', 'Reorder w/ Revision'),
                               ('revision', 'Revision'),
                               ('option_delete_ftp', 'Option Delete'),
                               ('virtual_spec', 'Virtual Spec'),
                               ('store_image', 'Store Image')], string='Art Request Type', copy=False, default=_get_default_status)
    state = fields.Selection(
        [('draft', 'Draft'), ('submit', 'Submitted'), ('received', 'Received'), ('in_process', 'In Progress'),
         ('on_hold', 'On Hold'), ('completed', 'Completed'),
         ('archive', 'Archived'), ('hold', 'On Hold')], string='Status', default='draft', copy=False)
    partner_id = fields.Many2one('res.partner', 'Customer', copy=True)
    acc_manager_id = fields.Many2one('res.users', string='Account Manager',
                                     copy=False)
    internal_contact_id = fields.Many2one(
        'res.users', 'Internal Contact', copy=True)
    customer_type = fields.Selection([('new', 'New Customer'),
                                      ('current', 'Current Customer'),
                                      ('web_new', 'Web Order - New Customer'),
                                      ('web_current_customer',
                                       'Web Order - Current Customer'),
                                      ('program', 'Program')], copy=True)
    job = fields.Integer('Job #', copy=True)
    job1 = fields.Integer('Job1', copy=True)
    art_sequence = fields.Char(compute='_compute_art_sequence', string='Art Sequence')
    artworksheet_number_for_sale_order = fields.Integer(string="Artworksheet Number For Sale Order", default=1)
    entry_date = fields.Datetime('Submission Date', copy=False)
    finish_time = fields.Char('Finish Time')
    sale_id = fields.Many2one('sale.order', 'Linked Sale Order', copy=False)
    reorder_sale_id = fields.Many2one(
        'sale.order', 'Reorder Number', copy=False)
    day_requested = fields.Selection(
        [('monday', 'Monday'), ('tuesday', 'Tuesday'), ('wednesday', 'Wednesday'), ('thursday', 'Thursday'),
         ('friday', 'Friday')], 'Day Requested', copy=False)
    time_requested = fields.Selection([('30', '30 min'), ('1hr', '1 hour'), ('2hr', '2 Hour'),
                                       ('4hr', '4 Hour'), ('11',
                                                           '11:00'), ('11F', '11:00 - Firm'),
                                       ('12_30', '12:30'), ('12_30F',
                                                            '12:30 - Firm'), ('2', '2:00'),
                                       ('2F', '2:00 - Firm'), ('3', '3:00'), ('4',
                                                                              '4:00'), ('4_F', '4:00 - Firm'),
                                       ('5_30', '5:30'), ('5_30F', '5:30 - Firm')], string='Time Requested', copy=False)
    art_type = fields.Selection([('vector_perfect', 'Vector/Perfect'),
                                 ('raster_recreate', 'Raster/Recreate'),
                                 ('vector', 'Vector on File'),
                                 ('text', 'Text Only'),
                                 ('custom', 'Custom Design'),
                                 ('see_logobank', 'See Logobank')],
                                string='Art Type', copy=False)
    vendor_id = fields.Many2one('res.partner', 'Vendor Info', copy=True)
    order_total = fields.Float(
        compute="_compute_order_total", string='Order Total', store=True, copy=False)
    vendor_information = fields.Text('Vendor Imprint Information', copy=True)
    artwork_instruction = fields.Text('Artwork Instructions', copy=False)
    art_lines = fields.One2many(
        'art.production.line', 'art_id', 'Product Details', copy=False)
    product_ids = fields.One2many(
        'art.production.line', 'art_id', 'Product', copy=False)
    decorations = fields.One2many(
        'sale.order.line.decoration', 'art_id', 'Decorations', copy=False)
    decoration_group_ids = fields.One2many('sale.order.line.decoration.group', 'art_id', string="Decoration Groups", copy=False)
    designer_id = fields.Many2one('res.users', 'Designer', copy=False, track_visibility='onchange')
    initial_designer_id = fields.Many2one('res.users', 'Initial Designer', readonly=False, track_visibility='onchange', copy=False)
    # message_posted = fields.Boolean('Message Posted')
    imprint_file = fields.Binary(related='vendor_id.imprint_file', string='Imprint Information')
    imprint_filename = fields.Char(related='vendor_id.imprint_filename')
    # artwork_message_post = fields.Boolean('Artwork Instruction', default=True)
    # artwork_request_post = fields.Boolean('Artwork Status', default=True)
    # art_type_post = fields.Boolean('Art type Posted', default=True)
    revision = fields.Integer('Revision', copy=False) 
    approval_required = fields.Boolean('Approval Required', read=[
        'sales_team.group_sale_salesman_all_leads'], copy=True)
    start_early = fields.Boolean('Start Early', read=['sales_team.group_sale_salesman_all_leads'],
                                 write=['pinnacle_sales.group_art_worksheet_user'], copy=True)
    alter_logo = fields.Boolean('OK to Alter Logo', read=['sales_team.group_sale_salesman_all_leads'],
                                write=['pinnacle_sales.group_art_worksheet_user'], copy=False)
    recreate_ids = fields.One2many('art.recreate.info', 'art_id', 'Recreate Information',
                                   read=[
                                       'sales_team.group_sale_salesman_all_leads'],
                                   write=['pinnacle_sales.group_art_worksheet_user'], copy=True)
    sale_orders = fields.Char('References', copy=False)
    associated_file_ids = fields.One2many(
        'art.production.line', 'art_id', 'Associated Files', copy=False)
    # associated_file_thumb = fields.One2many(
    #     'art.production.line', 'art_id', 'Associated Files Technical field',
    #     domain=[('spec_file2', '!=', False), ('hide_file', '=', False)], copy=False)
    last_state = fields.Char()
    will_be_store_image = fields.Boolean('Will Be Store Image', copy=False)
    rush_request1 = fields.Boolean('Rush Request', copy=False)
    store_image = fields.Boolean('Store Image', copy=False)
    program_id = fields.Many2one('partner.program', string='Program')  # 12785
    complete_date = fields.Datetime('Complete Date')
    completed = fields.Boolean('Completed', copy=False)
    options = fields.Integer('Option', copy=True)
    # 13173: DEV: Art Worksheet - Issue Tab
    track_issue_ids = fields.One2many('track.issue', 'art_id', 'Track Issues')
    submitted = fields.Boolean('Submitted', copy=False)
    product_sku = fields.Char(related='art_lines.product_sku', string='Product ID')
    description = fields.Text(related='art_lines.description', string='Description')
    vendor_decoration_location = fields.Many2one('vendor.decoration.location', string='Vendor Imprint Location',
                                                 related="decorations.vendor_decoration_location")
    vendor_decoration_method = fields.Many2one('vendor.decoration.method', string='Vendor Imprint Method',
                                               related="decorations.vendor_decoration_method")
    customer_art = fields.Many2one('product.decoration.customer.art', related="decorations.customer_art",
                                   string="Art (file or text)", store=True)
    show_complete = fields.Boolean('Show Complete', copy=False)
    show_in_process = fields.Boolean('Show In Process', copy=False)
    ready1 = fields.Boolean('Ready', copy=False)
    analytic_tag_ids = fields.Many2many('account.analytic.tag', string='Analytic Tags',
                                        related='sale_id.analytic_tag_ids')
    manual_artworksheet = fields.Boolean('Manual Art Worksheet')
    decoration_sequence_id = fields.Integer(
        string='Decoration Line Sequence', readonly=True, default=1, copy=True)
    decoration_group_sequence_id = fields.Integer(
        string='Decoration Group Sequence', readonly=True, default=1, copy=False)

    # Archived Fields For Maintaining Sale Order Details When Art Work Sheet
    active = fields.Boolean(default=True)
    archive_sale_id = fields.Char('Archive Sale Order', copy=False)
    archive_order_total = fields.Float(
        string='Archive Order Total', copy=False)
    archive_art_lines = fields.One2many(
        'art.production.line', 'art_id', 'Archive Product Details', copy=False)
    archive_product_ids = fields.One2many(
        'art.production.line', 'art_id', 'Archive Products', copy=False)
    archive_decorations = fields.One2many(
        'sale.order.line.decoration.archive', 'art_id', 'Archive Decorations', copy=False)
    archive_associated_file_ids = fields.One2many(
        'art.production.line', 'art_id', 'Archive Associated Files', copy=False)
    archive_associated_file_thumb = fields.One2many(
        'art.production.line', 'art_id', 'Archive Associated Files Technical field',
        domain=[('spec_file2', '!=', False), ('hide_file', '=', False)], copy=False)
    archive_analytic_tag_ids = fields.Many2many('account.analytic.tag', string='Archive Analytic Tags')

    common_sale_order = fields.Char(compute="_compute_common_sale_order", string="Sale Order", store="True")
    currency_id = fields.Many2one(compute='_compute_currency_id', comodel_name="res.currency", string="Currency", store=True)
    artwork_status = fields.Selection([('aws_recreate', 'AWS - Recreate'), ('easy_recreate', 'Easy Recreate'),
                                        ('force_fonts', 'Force Fonts'), ('high_res', 'High Res')], string="Artwork Status")
    customer_company_name = fields.Many2one(related='partner_id.parent_id', string="Company Name", readonly="1")
    imprint_description = fields.Text(string="Imprint Description")

    @api.onchange('artwork_status')
    def onchange_artwork_status(self):
        if self.artwork_status:
            self.designer_id = False

    @api.one
    @api.depends('sale_id', 'sale_id.currency_id', 'sale_id.pricelist_id', 'archive_sale_id', 'state')
    def _compute_currency_id(self):
        self.currency_id = self.env.user.company_id and self.env.user.company_id.currency_id and self.env.user.company_id.currency_id.id or False
        if self.sale_id and self.state != 'archive':
            self.currency_id = self.sale_id.currency_id and self.sale_id.currency_id.id or False
        if self.archive_sale_id and self.state == 'archive':
            sale_order = self.env['sale.order'].search([('name', 'like', self.archive_sale_id)])
            if sale_order:
                self.currency_id = sale_order.currency_id and sale_order.currency_id.id or False


    @api.one
    @api.depends('sale_id', 'archive_sale_id')
    def _compute_common_sale_order(self):
        if self.active:
            self.common_sale_order = self.sale_id and self.sale_id.name or False
        else:
            self.common_sale_order = self.archive_sale_id

    @api.multi
    def action_hold(self):
        for art_worksheet in self:
            art_worksheet.last_state = art_worksheet.state
            art_worksheet.state = 'hold'
        return True

    @api.one
    @api.depends('order_line.analytic_tag_ids')
    def _get_analytic_tags(self):
        if self.order_line:
            self.analytic_tag_ids = self.order_line[0].analytic_tag_ids.ids

    @api.multi
    def in_process(self):
        for rec in self:
            rec.write({'state': 'in_process', 'show_in_process': False, 'show_complete': True})
            note = "<b>Initial Designer: </b>%s<br/><b>In hands Designer: </b>%s<br/><b>Status: </b>%s<br /><b>Changed By: </b>%s" % (
                    rec.initial_designer_id.name, rec.designer_id.name, ART_STATUS.get(rec.state), self.env.user.name)
            # Report as a mail.message
            attach_ids = []
            allow_pdf_generate = self.env['ir.values'].get_default('base.config.settings', 'pdf_generate_enable')

            if allow_pdf_generate:
                report_name = 'Art Worksheet'
                ctx = self._context.copy()
                ctx.update({'active_model': 'art.production',
                            'active_ids': [rec.id], 'active_id': rec.id})
                
                res, format = self.env['report'].sudo().with_context(ctx).get_pdf(rec._ids,
                                                                                  'pinnacle_sales.report_art_worksheet'), 'pdf'
                ext = "." + format
                if not report_name.endswith(ext):
                    report_name += ext
                attach_ids = [(report_name, res)]
            rec.message_post(body=note, attachments=attach_ids, message_type='comment')

    @api.onchange('options')
    def onchange_option(self):
        options = str(self.options)
        if len(options) == 1 or len(options) > 1:
            self.options = int(options[0])

    @api.one
    @api.depends('sale_id.amount_total')
    def _compute_order_total(self):
        self.order_total = 0.00
        if self.sale_id:
            self.order_total = self.sale_id.amount_total

    @api.onchange('partner_id')
    def _onchange_partner_id(self):
        self.customer_type = self.partner_id.customer_type
        self.acc_manager_id = self.partner_id.user_id.id
        self.program_id = self.partner_id.program_id.id

    @api.multi
    def _check_validation(self):
        for art in self:
            if not art.status:
                raise UserError(
                    _('Request Type required.'))
            if not art.day_requested:
                raise UserError(
                    _('Day Requested required.'))
            if not art.time_requested:
                raise UserError(
                    _('Time Requested required.'))
            if not art.art_type:
                raise UserError(
                    _('Art Type required.'))
            if not art.artwork_instruction:
                raise UserError(
                    _('Artwork Instructions required.'))
            if not art.imprint_description:
                raise UserError(
                    _('Imprint Description required.'))
            for prod_line in art.art_lines:
                if not prod_line.web_link:
                    raise UserError(
                        _('Weblink in Product Details required.'))
                if not prod_line.item_dimensions:
                    raise UserError(
                        _('Item Dimensions in Product Details required.'))
            for decoration in art.decorations:
                if not decoration.area:
                    raise UserError(
                        _('Max. Imprint Area in Decoration Details required.'))
                if not decoration.pms_code and not decoration.pms_color_code:
                    raise UserError(
                        _('Please enter a PMS code or Stock color in the decoration on the Sales Order.'))
        return True

    @api.multi
    def submit(self):
        self.ensure_one()
        self._check_validation()
        note = 'Art Request normally Submitted'
        attach_ids = []
        # if self._check_validation() and not self.message_posted:
        if self._check_validation():
            check_attach_ids = []
            attach_ids = []
            decoration_notes = []
            cfs_url = str(self.env['ir.config_parameter'].get_param('cfs.web.url'))
            for decoration in self.decorations:
                for file in decoration.note_id.files:
                    # if file.file_binary and not file.message_posted:
                    if file.file_binary_url:
                        final_url = cfs_url + file.file_binary_url
                        if file.name not in check_attach_ids:
                            template = """<left>\
                                            <a href=%s target='_blank' title=%s>\
                                                <img src=%s ></img>\
                                            </a><br/>\
                                            <a href=%s target='_blank'>Download</a>\
                                       </left>
                                    """ % (
                                (final_url + '?h=40'),
                                file.name,
                                (file.file_binary_url + '?h=40'),
                                (final_url + '?dl=1'))
                            note += "<br/>" + template
                            check_attach_ids.append(file.name)
                            # file.message_posted = True
                if decoration.customer_art.name_url:
                    final_url = cfs_url + decoration.customer_art.name_url
                    if decoration.customer_art.art_filename not in check_attach_ids:
                        template = """<left>\
                                            <a href=%s target='_blank' title=%s>\
                                                <img src=%s ></img>\
                                            </a><br/>\
                                            <a href=%s target='_blank'>Download</a>\
                                       </left>
                                    """ % (
                            (final_url + '?h=40'),
                            decoration.customer_art.art_filename,
                            (decoration.customer_art.name_url + '?h=40'),
                            (final_url + '?dl=1'))
                        note += "<br/>" + template
                        check_attach_ids.append(
                            decoration.customer_art.art_filename)
                        # decoration.message_posted = True
                # if decoration.note_id.name and not
                # decoration.note_id.message_posted:
                if decoration.note_id.name:
                    decoration_notes.append(decoration.note_id.name)
                    # decoration.note_id.message_posted = True
            if decoration_notes:
                note = "<b>Additional Notes: </b>" + \
                       "\n".join(decoration_notes)
        if self.artwork_instruction:
            note += "\n<b>Art worksheet Instruction: </b>%s" % self.artwork_instruction
            # self.write({'artwork_message_post': False})
        if self.status:
            if self.status == 'new_order':
                status = 'New Order'
            elif self.status == 'reorder':
                status = 'Reorder w/ Revision'
            elif self.status == 'revision':
                status = 'Revision'
            elif self.status == 'option_delete_ftp':
                status = 'Option Delete'
            elif self.status == 'virtual_spec':
                status = 'Virtual Spec'
            elif self.status == 'store_image':
                status = 'Store Image'
            else:
                status = 'Unknown'
            note += "\n<b>Art Request Type: </b>%s" % status
            # self.write({'artwork_request_post': False})
        if self.art_type:
            if self.art_type == 'vector_perfect':
                art_type = 'Vector/Perfect'
            elif self.art_type == 'raster_recreate':
                art_type = 'Raster/Recreate'
            elif self.art_type == 'vector':
                art_type = 'Vector on File'
            elif self.art_type == 'text':
                art_type = 'Text Only'
            elif self.art_type == 'custom':
                art_type = 'Custom Design'
            elif self.art_type == 'see_logobank':
                art_type = 'See Logobank'
            else:
                art_type = 'Unknown'
            note += "\n<b>Art Type: </b>%s" % art_type
            # self.write({'art_type_post': False})
        note += "\n\n<b>Art worksheet Submitted.</b>"
        new_note = "<p>" + note.replace('\n', '<p></p>') + "</p>"
        # Report as a mail.message
        report_name = 'Art Worksheet'
        ctx = self._context.copy()
        ctx.update({'active_model': 'art.production',
                    'active_ids': [self.id], 'active_id': self.id})
        result, format = self.env['report'].sudo().with_context(ctx).get_pdf(self._ids,
                                                                             'pinnacle_sales.report_art_worksheet'), 'pdf'
        ext = "." + format
        if not report_name.endswith(ext):
            report_name += ext
        if self.status == 'store_image':
            partner_obj = self.env['res.partner']
            self.store_image = True
            # 13505:DEV: Art Worksheet - Add Followers when Store Image
            follower1 = partner_obj.search([('email', '=', 'robyn.goldstein@pinnaclepromotions.com')])
            follower2 = partner_obj.search([('email', '=', 'shannon.lee@pinnaclepromotions.com')])
            follower3 = partner_obj.search([('email', '=', 'kelly.johnson@pinnaclepromotions.com')])
            if not follower1:
                follower1 = partner_obj.create(
                    {'name': 'Robyn Goldstein', 'email': 'robyn.goldstein@pinnaclepromotions.com'})
            if not follower2:
                follower2 = partner_obj.create({'name': 'Shannon Lee', 'email': 'shannon.lee@pinnaclepromotions.com'})
            if not follower3:
                follower3 = partner_obj.create({'name': 'Kelly Johnson', 'email': 'kelly.johnson@pinnaclepromotions.com'})
            followers = []
            followers.extend([follower1.id, follower2.id, follower3.id])
            self.message_subscribe(followers)
        attach_ids.append((report_name, result))
        self.message_post(body=new_note, attachments=attach_ids, subtype='mail.mt_comment', message_type='comment')
        
            # 13505:DEV: Art Worksheet - Add Followers when Store Image
        # 12801::should count up if art worksheet is submit a Revision, then the object is completed.
        revision = self.revision
        completed = self.completed
        if (self.status == 'revision' and revision < 1) or (self.status == 'revision' and not self.completed):
            revision += 1
        # 13173: DEV: Art Worksheet - Issue Tab
        if not self.completed:
            issue_dict = {'user_id': self.env.uid,
                          'revision': revision,
                          'request_type': self.status,
                          'art_id': self.id,
                          'auto': True}
            self.env['track.issue'].create(issue_dict)
            completed = True
        else:
            track_ids = []
            for track in self.track_issue_ids:
                if track.auto and track.rush:
                    track_ids.append(track)
            final_track_id = track_ids and track_ids[-1] or False
            if final_track_id:
                final_track_id.rush = False
        tz = False
        if self.env.user.tz:
            tz = pytz.timezone(self.env.user.tz)
        else:
            tz = pytz.timezone('US/Eastern')
        tzoffset = tz.utcoffset(datetime.datetime.strptime(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), DEFAULT_SERVER_DATETIME_FORMAT))
        finish_date = False
        date = datetime.datetime.strptime(fields.Datetime.now(), "%Y-%m-%d %H:%M:%S")
        if self.time_requested == '30':
            finish_date = ((date + datetime.timedelta(minutes=30)) + tzoffset)
        elif self.time_requested == '1hr':
            finish_date = ((date + datetime.timedelta(minutes=60)) + tzoffset)
        elif self.time_requested == '2hr':
            finish_date = ((date + datetime.timedelta(minutes=120)) + tzoffset)
        elif self.time_requested == '4hr':
            finish_date = ((date + datetime.timedelta(minutes=240)) + tzoffset)
        finish_time = False
        if finish_date:
            current_time = (date +  tzoffset).strftime('%H:%M %p')
            finish_time = finish_date.strftime('%H:%M %p')
            hours = int(finish_time[0:2])
            minutes = int(finish_time[3:5])
            add_hours = 0
            if hours >= 18:
                if minutes > 0 or hours >18:
                    add_hours = 15
            finish_time = ' - ' + ((finish_date + datetime.timedelta(hours=add_hours))).strftime('%I:%M %p')
            if int(current_time[0:2]) >= 18 or int(current_time[0:2]) >= 0 and int(current_time[0:2]) < 9:
                if hours >= 0 and hours <= 9 and int(current_time[0:2]) >= 18:
                    add_hours = 9 + hours + 24 - int(current_time[0:2])
                else:
                    add_hours = 9 + hours - int(current_time[0:2]) 
                if self.time_requested == '30':
                    add_hours = 9
                am_pm = 'AM'
                if add_hours > 12:
                    add_hours -= 12
                    am_pm = 'PM'
                if len(str(add_hours)) < 2:
                    add_hours = '0' + str(add_hours)
                else:
                    add_hours = str(add_hours)
                if self.time_requested == '30':
                    minutes = str(minutes)
                else:
                    minutes = '00'
                if int(hours) < 9 and self.time_requested == '30' or int(hours) >= 18 and self.time_requested == '30':
                    minutes = str(30)
                if int(hours) == 9 and int(minutes) < 30 and self.time_requested == '30':
                    minutes = str(30)  
                finish_time = ' - ' + add_hours + ':' + minutes + ' ' + am_pm
        rush_request1 = False
        today_day = datetime.datetime.now().strftime("%A")
        if self.day_requested == today_day.lower() and self.time_requested in ['30', '1hr', '2hr']:
            rush_request1 = True
        self.write(
            {'state': 'submit', 'rush_request1': rush_request1, 'revision': revision, 'completed': completed,
             'entry_date': time.strftime(DEFAULT_SERVER_DATETIME_FORMAT), 'submitted': True, 'finish_time': finish_time})  # 'message_posted': True
        if self.sale_id:
            if self.sale_id.state == 'draft':
                report_name = 'Quotation ' + self.sale_id.name
            else:
                report_name = 'Sales Order ' + self.sale_id.name
            result, format = self.env['report'].sudo().get_pdf(
                self.sale_id._ids, 'sale.report_saleorder'), 'pdf'
            ext = "." + format
            if not report_name.endswith(ext):
                report_name += ext
            attach_ids = [(report_name, result)]
            self.sale_id.message_post(attachments=attach_ids, message_type='comment')
        return True

    @api.multi
    def rush_request(self):
        self.ensure_one()
        self._check_validation()
        note = 'Art Request Submitted as a \'Rush\''
        attach_ids = []
        check_attach_ids = []
        if self._check_validation():
            attach_ids = []
            decoration_notes = []
            for decoration in self.decorations:
                for file in decoration.note_id.files:
                    if file.file_binary_url:
                        if file.name not in check_attach_ids:
                            template = """<left>\
                                            <a href=%s target='_blank' title=%s>\
                                                <img src=%s ></img>\
                                            </a><br/>\
                                            <a href=%s target='_blank'>Download</a>\
                                       </left>
                                    """ % (
                                (file.file_binary_url + '?h=40'),
                                file.name,
                                (file.file_binary_url + '?h=40'),
                                (file.file_binary_url + '?dl=1'))
                            note += "<br/>" + template
                            check_attach_ids.append(file.name)
                            # file.message_posted = True
                if decoration.customer_art.name_url:
                    if decoration.customer_art.art_filename not in check_attach_ids:
                        template = """<left>\
                                            <a href=%s target='_blank' title=%s>\
                                                <img src=%s ></img>\
                                            </a><br/>\
                                            <a href=%s target='_blank'>Download</a>\
                                       </left>
                                    """ % (
                            (decoration.customer_art.name_url + '?h=40'),
                            decoration.customer_art.art_filename,
                            (decoration.customer_art.name_url + '?h=40'),
                            (decoration.customer_art.name_url + '?dl=1'))
                        note += "<br/>" + template
                        check_attach_ids.append(
                            decoration.customer_art.art_filename)
                        # decoration.write({'message_posted': True})
                if decoration.note_id.name:
                    decoration_notes.append(decoration.note_id.name)
                    # decoration.note_id.write({'message_posted': True})
            if decoration_notes:
                note = "<b>Additional Notes: </b>" + \
                       "\n".join(decoration_notes)
        if self.artwork_instruction:
            note += "\n<b>Art worksheet Instruction: </b>%s" % self.artwork_instruction
            # self.write({'artwork_message_post': False})
        if self.status:
            if self.status == 'new_order':
                status = 'New Order'
            elif self.status == 'reorder':
                status = 'Reorder w/ Revision'
            elif self.status == 'revision':
                status = 'Revision'
            elif self.status == 'option_delete_ftp':
                status = 'Option Delete'
            elif self.status == 'virtual_spec':
                status = 'Virtual Spec'
            elif self.status == 'store_image':
                status = 'Store Image'
            else:
                status = 'Unknown'
            note += "\n<b>Art Request Type: </b>%s" % status
            # self.write({'artwork_request_post': False})
        if self.art_type:
            if self.art_type == 'vector_perfect':
                art_type = 'Vector/Perfect'
            elif self.art_type == 'raster_recreate':
                art_type = 'Raster/Recreate'
            elif self.art_type == 'vector':
                art_type = 'Vector on File'
            elif self.art_type == 'text':
                art_type = 'Text Only'
            elif self.art_type == 'custom':
                art_type = 'Custom Design'
            elif self.art_type == 'see_logobank':
                art_type = 'See Logobank'
            else:
                art_type = 'Unknown'
            note += "\n<b>Art Type: </b>%s" % art_type
            # self.write({'art_type_post': False})
        note += "\n\n<b>Rush Request Submitted.</b>"
        new_note = "<p>" + note.replace('\n', '<p></p>') + "</p>"
        # Report as a mail.message
        report_name = 'Art Worksheet'
        ctx = self._context.copy()
        ctx.update({'active_model': 'art.production',
                    'active_ids': [self.id], 'active_id': self.id})
        result, format = self.env['report'].sudo().with_context(ctx).get_pdf(self._ids,
                                                                             'pinnacle_sales.report_art_worksheet'), 'pdf'
        ext = "." + format
        if not report_name.endswith(ext):
            report_name += ext
        attach_ids.append((report_name, result))
        self.message_post(body=new_note, attachments=attach_ids, message_type='comment')
        if self.status == 'store_image':
            partner_obj = self.env['res.partner']
            self.store_image = True
            # 13505:DEV: Art Worksheet - Add Followers when Store Image
            follower1 = partner_obj.search([('email', '=', 'robyn.goldstein@pinnaclepromotions.com')])
            follower2 = partner_obj.search([('email', '=', 'shannon.lee@pinnaclepromotions.com')])
            if not follower1:
                follower1 = partner_obj.create(
                    {'name': 'Robyn Goldstein', 'email': 'robyn.goldstein@pinnaclepromotions.com'})
            if not follower2:
                follower2 = partner_obj.create({'name': 'Shannon Lee', 'email': 'shannon.lee@pinnaclepromotions.com'})
            followers = []
            if self.acc_manager_id:
                followers.append(self.acc_manager_id.partner_id.id)
            if self.sale_id.aam_id:
                for aam_id in self.sale_id.aam_id:
                    followers.append(aam_id.partner_id.id)
            if self.sale_id.soc_id:
                followers.append(self.sale_id.soc_id.partner_id.id)
            followers.extend([follower1.id, follower2.id])
            self.message_subscribe(followers)
            # 13505:DEV: Art Worksheet - Add Followers when Store Image
        # 12801::should count up if art worksheet is submit a Revision, then the object is completed.
        revision = self.revision
        completed = self.completed
        if (self.status == 'revision' and revision < 1) or (self.status == 'revision' and not self.completed):
            revision += 1
        # 13173: DEV: Art Worksheet - Issue Tab
        if not self.completed:
            issue_dict = {'user_id': self.env.uid,
                          'revision': revision,
                          'request_type': self.status,
                          'art_id': self.id,
                          'auto': True,
                          'rush': 'Rush'}
            self.env['track.issue'].create(issue_dict)
            completed = True
        else:
            track_ids = []
            for track in self.track_issue_ids:
                if track.auto and not track.rush:
                    track_ids.append(track)
            final_track_id = track_ids and track_ids[-1] or False
            if final_track_id:
                final_track_id.rush = 'Rush'
        self.write(
            {'state': 'submit', 'revision': revision, 'completed': completed, 'rush_request1': True,
             'entry_date': time.strftime(DEFAULT_SERVER_DATETIME_FORMAT), 'submitted': True})
        if self.sale_id.state == 'draft':
            report_name = 'Quotation ' + self.sale_id.name
        else:
            report_name = 'Sales Order ' + self.sale_id.name
        result, format = self.env['report'].sudo().get_pdf(
            self.sale_id._ids, 'sale.report_saleorder'), 'pdf'
        ext = "." + format
        if not report_name.endswith(ext):
            report_name += ext
        attach_ids = [(report_name, result)]
        self.sale_id.message_post(attachments=attach_ids, message_type='comment')
        return True

    @api.multi
    def req_completed(self):
        '''
        This function opens a window to compose an email, with the edi sale template message loaded by default
        '''
        mail_template = self.env.ref(
            'pinnacle_sales.' +
            'art_production_template_finished_notification')
        subject = mail_template.subject
        subject_updated = self.env['mail.template'].render_template(
            subject, mail_template.model, self._ids)
        new_context = dict(self.env.context).copy()
        emails = []
        emails.append(self.sale_id and self.sale_id.user_id and self.sale_id.user_id.partner_id.id or False)
        if self.sale_id and self.sale_id.aam_id:
            for aam_id in self.sale_id.aam_id:
                emails.append(aam_id.partner_id.id)
        # Adding All Followers
        for message_partner_id in self.message_follower_ids.mapped('partner_id'):
            emails.append(message_partner_id and message_partner_id.id or False)
        emails = [email for email in emails if email]
        new_context.update(
            {'art_id': self.id, 'default_subject': subject_updated.get(self.id), 'default_partner_ids': emails})
        form_view_id = self.env.ref(
            'pinnacle_sales.art_comment_form_view1')
        return {'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'target': 'new',
                'view_id': form_view_id.id,
                'res_model': 'art.comment',
                'name': 'Send Mail',
                'context': new_context,
                }
        
    @api.multi
    def kick_back(self):
        '''
        #12688:This function open a window to compose an email with edi template message loaded by default
        '''
        self.ensure_one()
        ir_model_data = self.env['ir.model.data']
        partner_obj = self.env['res.partner']
        try:
            template_id = \
            ir_model_data.get_object_reference('pinnacle_sales', 'art_production_template_kick_back_notification')[1]
        except ValueError:
            template_id = False
        try:
            compose_form_id = ir_model_data.get_object_reference('mail', 'email_compose_message_wizard_form')[1]
        except ValueError:
            compose_form_id = False
        ctx = dict(self.env.context or {})
        partner_ids = []
        if self.sale_id.soc_id and self.sale_id.soc_id.partner_id:
            partner_ids.append(self.sale_id.soc_id.partner_id.id)
        if self.sale_id.aam_id:
            for aam_id in self.sale_id.aam_id:
                partner_ids.append(aam_id.partner_id.id)
        if self.sale_id.user_id and self.sale_id.user_id.partner_id:
            partner_ids.append(self.sale_id.user_id.partner_id.id)
        cc_partner1_id = partner_obj.search([('email', '=', 'christina.donofrio@pinnaclepromotions.com')])
        cc_partner2_id = partner_obj.search([('email', '=', 'jodi.spong@pinnaclepromotions.com')])
        if not cc_partner1_id:
            cc_partner1_id = partner_obj.create(
                {'name': 'Christina D\'Onofrio', 'email': 'christina.donofrio@pinnaclepromotions.com'})
        if not cc_partner2_id:
            cc_partner2_id = partner_obj.create({'name': 'Jodi Spong', 'email': 'jodi.spong@pinnaclepromotions.com'})
        cc_partner_ids = [cc_partner1_id.id, cc_partner2_id.id]
        ctx.update({
            'default_model': 'art.production',
            'default_res_id': self.ids[0],
            'default_use_template': bool(template_id),
            'default_template_id': template_id,
            'default_composition_mode': 'comment',
            'default_partner_ids': partner_ids,
            'kick_back': True,
            'default_cc_partner_ids': cc_partner_ids
        })
        return {
            'name': _('Kick Back'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }

    @api.one
    def remove_hold(self):
        self.state = self.last_state

    @api.multi
    def bulk_upload(self):
        self.ensure_one()
        context_active_ids = []
        for associate_file in self.associated_file_ids:
            if associate_file.select:
                context_active_ids.append(associate_file.id)
                spec_file_url = associate_file.spec_file_url
                vendor_file_url = associate_file.vendor_file_url
                spec_file2_url = associate_file.spec_file2_url
        if len(context_active_ids) == 0:
            raise UserError(_('Please select at least one line.'))
        new_context = dict(self.env.context).copy()
        new_context.update({'art_id': [self.id],
                            'art_line_ids': context_active_ids,
                            'cfs_attachment': True,
                            'default_spec_file_url': spec_file_url,
                            'default_vendor_file_url': vendor_file_url,
                            'default_spec_file2_url': spec_file2_url})
        form_view_id = self.env.ref(
            'pinnacle_sales.art_upload_file_form_view')
        return {'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'target': 'new',
                'view_id': form_view_id.id,
                'res_model': 'art.upload.file',
                'name': 'Mass Upload',
                'context': new_context,
                }

    # TODO to be removed
    @api.model
    def hide_file(self, art_id):
        art_line_ids = self.env['art.production.line'].search(
            [('art_id', '=', art_id)])
        art_lines = []
        art1_line_ids = []
        for art_line in art_line_ids:
            if art_line.spec_file2_name not in art_lines:
                art_lines.append(art_line.spec_file2_name)
                art1_line_ids.append(art_line.id)
        for h_art_line in art_line_ids:
            if h_art_line.id not in art1_line_ids:
                h_art_line.hide_file = True
            else:
                h_art_line.hide_file = False

    @api.multi
    def write(self, vals):
        result = super(ArtProduction, self).write(vals)
        for art_worksheet in self:
            if vals.get('designer_id'):
                art_worksheet.message_subscribe([art_worksheet.designer_id.partner_id.id])
            if vals.get('state', False) and vals.get('state', False) != 'archive':
                if art_worksheet.sale_id:
                    art_worksheet.sale_id.automate_art_processing()
                    art_worksheet.sale_id.automate_pending_review()
                    if art_worksheet.sale_id.quote and art_worksheet.sale_id.order_line:
                        art_worksheet.sale_id.set_virtual_processing()
                        art_worksheet.sale_id.set_quote_review()
        return result

    @api.model
    def create(self, vals):
        record = super(ArtProduction, self).create(vals)
        # To Link All Archived AWS's Of The Sale Order
        if record and record.sale_id:
            archived_artworksheets = record.search(['&', ('common_sale_order', '=like', record.sale_id.name), ('active', '=', False)])
            if archived_artworksheets:
                archived_artworksheets = archived_artworksheets.sorted(key=lambda r: r.artworksheet_number_for_sale_order)
                body = "<h4> ~: Archived ArtWorkSheets :~ </h4>"
                for archived_artworksheet in archived_artworksheets:
                    archived_artworksheet_link_url = str(self.env['ir.config_parameter'].search([('key', '=', 'web.base.url')]).value) + '/web#id=' + str(archived_artworksheet.id) + '&view_type=form&model=art.production&action=' + str(self.env.ref('pinnacle_sales.action_art_worksheet_archived_tree').id) + '&menu_id='
                    archived_artworksheet_link_name = str(archived_artworksheet.art_sequence) + " : " + str(archived_artworksheet.vendor_id.name)
                    body += "<b><a href='%s' target='_blank'>%s</a></b> <br/>" % (archived_artworksheet_link_url, archived_artworksheet_link_name)
                record.message_post(body=body, attachments=[], message_type='comment')
        return record

    @api.onchange('designer_id')
    def onchange_designer_id(self):
        if self.designer_id:
            self.artwork_status = False

    @api.multi
    def received(self):
        try:
            request_url = str(self.env['ir.config_parameter'].get_param('artq.web.url'))
            request_url += "Received&worksheet_id=%s" % self.id
            req = urllib2.Request(request_url)
            parents = urllib2.urlopen(req).read()
            response = json.loads(parents)
            if response != 'success':
                raise UserError(_("Cannot update ArtQ from Odoo."))
        except urllib2.HTTPError:
            raise UserError(_("Cannot access ArtQ URL."))
        for art in self:
            art.state = 'received'
            # if art.art_type == 'raster_recreate':
            #     art.show_in_process = False
            # else:
            art.show_in_process = True
            note = "<b>Initial Designer: </b>%s<br/><b>In hands Designer: </b>%s<br/><b>Status: </b>%s<br /><b>Changed By: </b>%s" % (
                    art.initial_designer_id.name, art.designer_id.name, ART_STATUS.get(art.state), self.env.user.name)
            # Report as a mail.message
            report_name = 'Art Worksheet'
            ctx = self._context.copy()
            ctx.update({'active_model': 'art.production',
                        'active_ids': [art.id], 'active_id': art.id})
            res, format = self.env['report'].sudo().with_context(ctx).get_pdf(art._ids,
                                                                              'pinnacle_sales.report_art_worksheet'), 'pdf'
            ext = "." + format
            if not report_name.endswith(ext):
                report_name += ext
            attach_ids = [(report_name, res)]
            art.message_post(body=note, attachments=attach_ids, message_type='comment')
        return True

    @api.one
    @api.returns('self', lambda value: value.id)
    def copy(self, default={}):
        if self._context.get('art_copy', False):
            return super(ArtProduction, self).copy(default=default)
        raise UserError(_('You cannot duplicate an Art Worksheet'))

    @api.onchange('status')
    def onchange_status(self):
        if self.status == 'store_image':
            instruction = ''
            if self.artwork_instruction:
                instruction = self.artwork_instruction
            instruction += '''\nStore SKU:
Company store url:
Due Date:
Additional Notes:
Linked File:

Reference Programs Company Store Image Requirements for details:
https://docs.google.com/spreadsheets/d/169dojVAARIRVkx7mWGkpJy1_IIUZ_-J3YJhPwH_nzeI/'''
            self.artwork_instruction = instruction

    @api.multi
    def action_force_out(self):
        try:
            for art in self:
                request_url = str(self.env['ir.config_parameter'].get_param('artq.web.url'))
                request_url += "ForceOut&worksheet_id=%s" % art.id
                req = urllib2.Request(request_url)
                parents = urllib2.urlopen(req).read()
                response = json.loads(parents)
                if response != 'success':
                    raise UserError(_("Cannot update ArtQ from Odoo."))
                art.write({'state': 'draft', 'day_requested': False, 'time_requested': False, 'rush_request1': False,
                           'store_image': False, 'show_in_process': False, 'show_complete': False, 'ready1': False})
        except urllib2.HTTPError:
            raise UserError(_("Cannot access ArtQ URL."))
        return True

    @api.multi
    def ready(self):
        try:
            for art in self:
                request_url = str(self.env['ir.config_parameter'].get_param('artq.web.url'))
                request_url += "ready&worksheet_id=%s" % art.id
                req = urllib2.Request(request_url)
                parents = urllib2.urlopen(req).read()
                response = json.loads(parents)
                if response != 'success':
                    raise UserError(_("Cannot update ArtQ from Odoo."))
                art.write({'ready1': True})
                note = "<b>Initial Designer: </b>%s<br/><b>In hands Designer: </b>%s<br/><b>Status: </b>%s<br /><b>Changed By: </b>%s" % (
                    art.initial_designer_id.name, art.designer_id.name, ART_STATUS.get(art.state), self.env.user.name)
                # Report as a mail.message
                report_name = 'Art Worksheet'
                ctx = self._context.copy()
                ctx.update({'active_model': 'art.production',
                            'active_ids': [art.id], 'active_id': art.id})
                res, format = self.env['report'].sudo().with_context(ctx).get_pdf(art._ids,
                                                                                  'pinnacle_sales.report_art_worksheet'), 'pdf'
                ext = "." + format
                if not report_name.endswith(ext):
                    report_name += ext
                attach_ids = [(report_name, res)]
                art.message_post(body=note, attachments=attach_ids, message_type='comment')
        except urllib2.HTTPError:
            raise UserError(_("Cannot access ArtQ URL."))
        return True

    @api.multi
    def product_variant_selection_art(self):
        self.ensure_one()
        if not self.vendor_id:
            raise UserError(
                _('Input needed for Vendor Information !'))
        new_context = dict(self.env.context).copy()
        new_context.update({'art_id': self.id, 'vendor_id': self.vendor_id.id, 'manual_artworksheet': self.manual_artworksheet})
        form_view_id = self.env.ref(
            'pinnacle_sales.product_variant_selection_form_view_art')
        return {'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'target': 'new',
                'view_id': form_view_id.id,
                'res_model': 'product.variant.selection',
                'name': 'Select Product Variants With Their Quantites',
                'context': new_context,
                }

    @api.multi
    def add_decoration_art(self):
        # try:
        self.ensure_one()
        context_active_ids = []
        can_decorate = True
        for line in self.art_lines:
            if line.select_art_line:
                if not line.decorations:
                    context_active_ids.append(line.id)
                    if not line.n_product_id.can_decorate or line.n_product_id.type == 'service':
                        can_decorate = False
                else:
                    raise UserError(_('Please select all lines without any decoration to add decoration.'))
        # self.art_lines.write({'select_art_line': False})
        if not can_decorate:
            raise UserError(_('Please select products that can be' +
                              ' decorated and that are not type of' +
                              ' service.'))
        elif len(context_active_ids) >= 1 and self:
            new_context = dict(self.env.context).copy()
            new_context.update({'active_id': context_active_ids[0],
                                'active_ids': context_active_ids,
                                'art_lines': context_active_ids,
                                'art_id': self.id,
                                'manual_artworksheet': True,
                                'readonly_by_pass': True})
            form_view_id = self.env.ref(
                'pinnacle_sales.art_add_decoration_form_view')
            return {'type': 'ir.actions.act_window',
                    'view_mode': 'form',
                    'target': 'new',
                    'view_id': form_view_id.id,
                    'res_model': 'sale.order.add.decoration',
                    'name': 'Add Art Line Decoration',
                    'context': new_context,
                    }
        else:
            raise UserError(_('Please select a line to add decoration.'))
        # finally:
        #     self.art_lines.write({'select_art_line': False})

    @api.model
    def create_decoration_group(self, decoration_ids=[]):
        self.ensure_one()
        if len(self.decoration_group_ids) > 0:
            if len(self.decoration_group_ids) != 1:
                if len(self.decoration_group_ids) != self.decoration_group_ids.sorted(key=lambda r:r.sequence_id)[len(self.decoration_group_ids)-1].sequence_id:
                    if self.decoration_group_ids.sorted(key=lambda r:r.sequence_id)[0].sequence_id == 1:
                        index = 0
                        for decoration_group_id in self.decoration_group_ids.sorted(key=lambda r:r.sequence_id):
                            if decoration_group_id.sequence_id + 1 != self.decoration_group_ids.sorted(key=lambda r:r.sequence_id)[index + 1].sequence_id:
                                decoration_group = self.env['sale.order.line.decoration.group'].create({'art_id': self.id,'group_sequence': 'DG'+str(decoration_group_id.sequence_id + 1),'group_id': 'Decoration Group '+str(decoration_group_id.sequence_id + 1),'sequence_id':decoration_group_id.sequence_id + 1})
                                break
                            index += 1
                    else:
                        decoration_group = self.env['sale.order.line.decoration.group'].create({'art_id': self.id,'group_sequence': 'DG1','group_id': 'Decoration Group 1','sequence_id':1})
                else:
                    if (self.decoration_group_ids.sorted(key=lambda r:r.sequence_id)[len(self.decoration_group_ids)-1].sequence_id + 1) == self.env['sale.order.line.decoration'].browse(decoration_ids)[0].art_id.decoration_group_sequence_id:
                        decoration_group = self.env['sale.order.line.decoration.group'].create({'art_id': self.id,'group_sequence': 'DG'+str(self.env['sale.order.line.decoration'].browse(decoration_ids)[0].art_id.decoration_group_sequence_id),'group_id': 'Decoration Group '+str(self.env['sale.order.line.decoration'].browse(decoration_ids)[0].art_id.decoration_group_sequence_id),'sequence_id':self.env['sale.order.line.decoration'].browse(decoration_ids)[0].art_id.decoration_group_sequence_id})
                        self.env['sale.order.line.decoration'].browse(decoration_ids)[0].art_id.decoration_group_sequence_id += 1
                    else:
                        decoration_group = self.env['sale.order.line.decoration.group'].create({'art_id': self.id,'group_sequence': 'DG'+str(self.decoration_group_ids.sorted(key=lambda r:r.sequence_id)[len(self.decoration_group_ids)-1].sequence_id + 1),'group_id': 'Decoration Group '+str(self.decoration_group_ids.sorted(key=lambda r:r.sequence_id)[len(self.decoration_group_ids)-1].sequence_id + 1),'sequence_id':self.decoration_group_ids.sorted(key=lambda r:r.sequence_id)[len(self.decoration_group_ids)-1].sequence_id + 1})
            else:
                if self.decoration_group_ids[0].sequence_id == 1:
                    self.env['sale.order.line.decoration'].browse(decoration_ids)[0].art_id.decoration_group_sequence_id = 2
                    decoration_group = self.env['sale.order.line.decoration.group'].create({'art_id': self.id,'group_sequence': 'DG'+str(self.env['sale.order.line.decoration'].browse(decoration_ids)[0].art_id.decoration_group_sequence_id),'group_id': 'Decoration Group '+str(self.env['sale.order.line.decoration'].browse(decoration_ids)[0].art_id.decoration_group_sequence_id),'sequence_id':self.env['sale.order.line.decoration'].browse(decoration_ids)[0].art_id.decoration_group_sequence_id})
                else:
                    decoration_group = self.env['sale.order.line.decoration.group'].create({'art_id': self.id,'group_sequence': 'DG1','group_id': 'Decoration Group 1','sequence_id':1})
        else:
            # if len(decoration_ids) == len(self.decorations):
            self.env['sale.order.line.decoration'].browse(decoration_ids)[0].art_id.decoration_group_sequence_id = 1
            decoration_group = self.env['sale.order.line.decoration.group'].create({'art_id': self.id,'group_sequence': 'DG'+str(self.env['sale.order.line.decoration'].browse(decoration_ids)[0].art_id.decoration_group_sequence_id),'group_id': 'Decoration Group '+str(self.env['sale.order.line.decoration'].browse(decoration_ids)[0].art_id.decoration_group_sequence_id),'sequence_id':self.env['sale.order.line.decoration'].browse(decoration_ids)[0].art_id.decoration_group_sequence_id})
            self.env['sale.order.line.decoration'].browse(decoration_ids)[0].art_id.decoration_group_sequence_id += 1
            # else:
            #     self.env['sale.order.line.decoration'].browse(decoration_ids)[0].art_id.decoration_group_sequence_id = 1
            #     decoration_group = self.env['sale.order.line.decoration.group'].create({'art_id': self.id,'group_sequence': 'DG'+str(self.env['sale.order.line.decoration'].browse(decoration_ids)[0].art_id.decoration_group_sequence_id),'group_id': 'Decoration Group '+str(self.env['sale.order.line.decoration'].browse(decoration_ids)[0].art_id.decoration_group_sequence_id),'sequence_id':self.env['sale.order.line.decoration'].browse(decoration_ids)[0].art_id.decoration_group_sequence_id})
            #     self.env['sale.order.line.decoration'].browse(decoration_ids)[0].art_id.decoration_group_sequence_id += 1
        if decoration_group:
            for decoration in self.env['sale.order.line.decoration'].browse(decoration_ids):
                decoration.decoration_group_id = decoration_group.id

    @api.multi
    def edit_decoration_art(self):
        # try:
            self.ensure_one()
            context_active_ids = []
            can_decorate = True
            for line in self.art_lines:
                if line.select_art_line:
                    if line.decorations:
                        context_active_ids.append(line.id)
                        if not line.n_product_id.can_decorate or line.n_product_id.type == 'service':
                            can_decorate = False
                    else:
                        raise UserError(_('To edit decoration, please select lines that have already been decorated.'))
            self.art_lines.write({'select_art_line': False})
            if not can_decorate:
                raise UserError(_('Please select products that can be' +
                                  ' decorated and that are not type of' +
                                  ' service.'))
            elif len(context_active_ids) > 0:
                new_context = dict(self.env.context).copy()
                new_context.update({'active_id': context_active_ids[0],
                                    'active_ids': context_active_ids,
                                    'art_lines': context_active_ids,
                                    'art_id': self.id,
                                    'manual_artworksheet': True,
                                    'readonly_by_pass': True})
                if len(context_active_ids) == 1:
                    self.env['sale.order.edit.decoration'].search([]).unlink()
                    form_view_id = self.env.ref(
                        'pinnacle_sales.art_production_edit_decoration_form_view')
                    return {'type': 'ir.actions.act_window',
                            'view_mode': 'form',
                            'target': 'new', 'view_id': form_view_id.id,
                            'res_model': 'sale.order.edit.decoration',
                            'name': 'Edit Art Line Decoration',
                            'context': new_context,
                            }
                else:
                    decoration_lines_same = False
                    for line in self.env['art.production.line']. browse(context_active_ids):
                        decoration_lines_same = self.env['art.production.line'].browse(context_active_ids)[0].decorations.decoration_compare(line.decorations)
                    if not decoration_lines_same:
                        raise UserError(_('Please select all editable ' +
                                          'lines with similar ' +
                                          'decorations to edit their\'s' +
                                          ' decorations.'))
                    form_view_id = self.env.ref(
                        'pinnacle_sales.' +
                        'art_production_edit_multi_decoration_form_view')
                    return {'type': 'ir.actions.act_window',
                            'view_mode': 'form',
                            'target': 'new',
                            'view_id': form_view_id.id,
                            'res_model':
                            'sale.order.edit.multi.decoration',
                            'name':
                            'Edit Multi Sale Order Lines Decoration',
                            'context': new_context,
                            }
            else:
                raise UserError(_('Please select at least one line with' +
                                  ' any decoration to edit it\'s decoration.'))
        # finally:
        #     self.art_lines.write({'select_art_line': False})

    @api.multi
    def add_vendor_web_link(self):
        self.ensure_one()
        context_active_ids = []
        can_decorate = True
        for art_line in self.art_lines:
            if art_line.select_art_line:
                context_active_ids.append(art_line.id)
        self.clear_all_selection_for_art_production()
        if context_active_ids:
            vendor_web_links = self.art_lines.browse(context_active_ids).mapped('web_link')
            vendor_web_links = list(set(vendor_web_links))
            if len(vendor_web_links) == 1:
                new_context = dict(self.env.context).copy()
                new_context.update({'active_id': context_active_ids[0],
                                    'active_ids': context_active_ids,
                                    'default_vendor_web_link': vendor_web_links[0]})
                form_view_id = self.env.ref('pinnacle_sales.art_production_line_manage_vendor_web_link_form_view')
                if form_view_id:
                    return {'type': 'ir.actions.act_window',
                            'view_mode': 'form',
                            'target': 'new',
                            'view_id': form_view_id.id,
                            'res_model': 'art.production.line.manage.vendor.web.link',
                            'name': 'Update Vendor Web Line Field Value',
                            'context': new_context,
                            }
            else:
                raise UserError('Please select all art lines with similar value for Vendor Web Link Field.')    
        else:
            raise UserError('Please select at least one art line.')

    @api.multi
    def update_item_dimensions(self):
        self.ensure_one()
        context_active_ids = []
        can_decorate = True
        for art_line in self.art_lines:
            if art_line.select_art_line:
                context_active_ids.append(art_line.id)
        self.clear_all_selection_for_art_production()
        if context_active_ids:
            item_dimensions = self.art_lines.browse(context_active_ids).mapped('item_dimensions')
            item_dimensions = list(set(item_dimensions))
            if len(item_dimensions) == 1:
                new_context = dict(self.env.context).copy()
                new_context.update({'active_id': context_active_ids[0],
                                    'active_ids': context_active_ids,
                                    'default_item_dimensions': item_dimensions[0]})
                form_view_id = self.env.ref('pinnacle_sales.update_item_dimensions_wizard_form_view')
                if form_view_id:
                    return {'type': 'ir.actions.act_window',
                            'view_mode': 'form',
                            'target': 'new',
                            'view_id': form_view_id.id,
                            'res_model': 'update.item.dimensions.wizard',
                            'name': 'Update Item Dimensions Field Value',
                            'context': new_context,
                            }
            else:
                raise UserError('Please select all art lines with similar value for Item Dimensions field.')    
        else:
            raise UserError('Please select at least one art line.')

    @api.multi
    def clear_all_selection_for_art_production(self):
        for record in self:
            if record.art_lines:
                record.art_lines.write({'select': False, 'select_art_line': False})


class ArtProductionLines(models.Model): 
    _name = 'art.production.line'

    @api.one
    @api.depends('product_id', 'n_product_id', 'manual_artworksheet')
    def _compute_product_variant_name(self):
        if self.manual_artworksheet:
            self.product_variant_name = str(
                ", ".join([v.name for v in self.n_product_id.attribute_value_ids]))
        else:
            self.product_variant_name = str(
                ", ".join([v.name for v in self.product_id.attribute_value_ids]))

    @api.one
    @api.depends('spec_file')
    def _get_spec_image_from_url(self):
        if self.spec_file_url:
            cfs_url = str(self.env['ir.config_parameter'].get_param('cfs.web.url'))
            final_url = cfs_url + self.spec_file_url
            url = url_fix(final_url)
            try:
                self.spec_file_preview = base64.encodestring(urllib2.urlopen(url).read())
            except Exception as e:
                self.spec_file_preview = ''


    @api.one
    @api.depends('vendor_file')
    def _get_vendor_image_from_url(self):
        if self.vendor_file_url:
            cfs_url = str(self.env['ir.config_parameter'].get_param('cfs.web.url'))
            final_url = cfs_url + self.vendor_file_url
            url = url_fix(final_url)
            try:
                self.vendor_file_preview = base64.encodestring(urllib2.urlopen(url).read())
            except Exception as e:
                self.vendor_file_preview = ''

    @api.one
    @api.depends('spec_file2')
    def _get_spec_image2_from_url(self):
        if self.spec_file2_url:
            cfs_url = str(self.env['ir.config_parameter'].get_param('cfs.web.url'))
            final_url = cfs_url + self.spec_file2_url
            url = url_fix(final_url)
            try:
                self.spec_file2_preview = base64.encodestring(urllib2.urlopen(url).read())
            except Exception as e:
                self.spec_file2_preview = ''

    @api.one
    @api.depends('decorations.decoration_group_id')
    def _compute_decoration_group_sequences(self):
        decoration_ids = []
        for decoration in self.decorations:
            if decoration.decoration_group_id:
                decoration_ids.append(decoration.decoration_group_id.id)
        decoration_ids = list(set(decoration_ids))
        self.decoration_group_sequences = [[6, False, decoration_ids]]
        self.group = [[6, False, decoration_ids]]
        
    select_art_line = fields.Boolean('Select') 
    sale_line_id = fields.Many2one(
        'sale.order.line', 'Sale Line Reference', copy=False)
    sale_order_id = fields.Many2one(related="sale_line_id.order_id")
    select = fields.Boolean('Select')
    product_id = fields.Many2one('product.product', string='Product', copy=True)
    n_product_id = fields.Many2one('product.product', 'Product')
    product_sku = fields.Char(string="Vendor Product ID", copy=True)
    description = fields.Text(string='Vendor Description', copy=True)
    product_variant_name = fields.Char(
        compute="_compute_product_variant_name", string="Vendor Variant")
    item_dimensions = fields.Char(string='Item Dimensions', copy=True)
    web_link = fields.Char('Web Link', copy=False) 
    art_id = fields.Many2one('art.production', 'Art Production', copy=False)
    finisher_id = fields.Many2one(related='art_id.vendor_id', string="Finisher")
    art_state = fields.Selection(related='art_id.state')
    revision = fields.Integer(related='art_id.revision')
    manual_artworksheet = fields.Boolean(related='art_id.manual_artworksheet', string='Manual Artworksheet')
    # TODO to be removed
    hide_file = fields.Boolean(string='Hide Record')
    group = fields.Many2many('sale.order.line.decoration.group', compute="_compute_decoration_group_sequences", string='Decoration Groups', readonly=True, copy=False)
    decoration_group_sequences = fields.Many2many('sale.order.line.decoration.group', compute="_compute_decoration_group_sequences", string='Decoration Groups', readonly=True, copy=False)
    quantity = fields.Float('Quantity', copy=True)
    decorations = fields.One2many(
        'sale.order.line.decoration', 'art_line_id', 'Decorations', copy=False)
    reorder_sale_ids = fields.Char(
        compute='_compute_reorder_sale_orders', string='Reorder')
    spec_file = fields.Binary('Art Page 1 File(s)', copy=True, attachment=True)
    spec_file_preview = fields.Binary('Art Page 1 File(s)', compute='_get_spec_image_from_url')
    # attachment reference and URL(CFS integration)
    spec_file_att = fields.Many2one(
        'ir.attachment', 'Art Page 1 File(s)', copy=True)  # 12212
    spec_file_url = fields.Char(
        related='spec_file_att.url', string='Art Page 1 File(s)', store=True, copy=True)  # 12212
    spec_file_name = fields.Char('Art Page 1 File(s) Name', copy=True)
    vendor_file = fields.Binary('Vendor Files', copy=True, attachment=True)
    vendor_file_preview = fields.Binary('Vendor Files', compute='_get_vendor_image_from_url')
    vendor_file_att = fields.Many2one(
        'ir.attachment', 'Vendor Files', copy=True)  # 12212
    vendor_file_url = fields.Char(
        related='vendor_file_att.url', store=True, string='Vendor Files', copy=True)  # 12212
    vendor_file_name = fields.Char('Vendor File Name', copy=True)

    spec_file2 = fields.Binary(
        'Art Page 2 File(s)', copy=True, attachment=True)
    spec_file2_preview = fields.Binary('Art Page 2 File(s)', compute='_get_spec_image2_from_url')
    spec_file2_att = fields.Many2one(
        'ir.attachment', 'Art Page 2 File(s)', copy=True)  # 12212
    spec_file2_url = fields.Char(
        related='spec_file2_att.url', store=True, string='Art Page 2 File(s)', copy=True)  # 12212
    spec_file2_name = fields.Char('Art Page 2 File(s) Name', copy=True)
    supplier_id = fields.Many2one('res.partner', 'Supplier')
    # Archived Fields For Maintaining Sale Order Details When Art Work Sheet
    archive_product_sku = fields.Char(string='Archive Vendor Product ID', copy=False)
    archive_product_variant_name = fields.Char(string="Archive Vendor Variant", copy=False)
    archive_group = fields.Char(
        string='Archive Decoration Groups', readonly=True, copy=False)
    archive_reorder_sale_ids = fields.Char(string='Archive Reorder', copy=False)
    archive_description = fields.Text(string='Archive Vendor Description', copy=False)
    archive_decorations = fields.One2many('sale.order.line.decoration.archive', 'art_line_id', 'Archive Decorations',
                                          copy=False)

    @api.multi
    def remove_spec_file(self):
        if self.spec_file_url and self.art_id:
            self.spec_file_att.unlink()
            self.spec_file_url = False
        return True

    @api.multi
    def remove_spec_file2(self):
        if self.spec_file2_url and self.art_id:
            self.spec_file2_att.unlink()
            self.spec_file2_url = False
        return True

    @api.multi
    def remove_vendor_file(self):
        if self.vendor_file_url and self.art_id:
            self.vendor_file_att.unlink()
            self.vendor_file_url = False
        return True

    @api.multi
    def remove_all_files(self):
        if self.art_id:
            if self.vendor_file_url:
                self.vendor_file_att.unlink()
                self.vendor_file_url = False
            if self.spec_file2_url:
                self.spec_file2_att.unlink()
                self.spec_file2_url = False
            if self.spec_file_url:
                self.spec_file_att.unlink()
                self.spec_file_url = False
        return True

    @api.multi
    def view_decoration(self):
        form_view_id = self.env.ref('pinnacle_sales.view_order_line_form_view')
        new_context = self.env.context.copy()
        new_context['invisible_so_line'] = True
        if self:
            return {'type': 'ir.actions.act_window',
                    'view_mode': 'form',
                    'name': "Decoration Lines",
                    'target': 'new',
                    'view_id': form_view_id.id,
                    'res_model': 'art.production.line',
                    'res_id': self[0].id,
                    'nodestroy': True,
                    'action_buttons': True,
                    'context': new_context
                    }

    @api.one
    @api.depends('group')
    def _compute_reorder_sale_orders(self):
        reorder_ids = []
        for group in self.group:
            if group.is_reorder:
                reorder_ids.append(
                    group.reorder_ref and group.reorder_ref or '')
        self.reorder_sale_ids = ','.join(reorder_ids)

    def get_dec_grp_seq(self, grp_seq):
        grp_seq = [grp.group_id for grp in grp_seq]
        grp_seq_name = ", ".join(grp_seq)
        return grp_seq_name

class ArtNotesFiles(models.Model):
    _name = 'art.notes.files'
    _rec_name = 'file_binary'

    @api.one
    @api.depends('file_binary')
    def _get_image_from_url(self):
        if self.file_binary_url:
            cfs_url = str(self.env['ir.config_parameter'].get_param('cfs.web.url'))
            final_url = cfs_url + self.file_binary_url
            url = url_fix(final_url)
            try:
                self.file_binary_preview = base64.encodestring(urllib2.urlopen(url).read())
            except Exception as e:
                self.file_binary_preview = ''

    name = fields.Char(string='File name', copy=True)
    note_id = fields.Many2one(
        'art.notes', 'Note', ondelete='cascade', copy=False)
    file_binary = fields.Binary('File', copy=True, attachment=True)
    file_binary_preview = fields.Binary('File Preview', compute='_get_image_from_url')
    file_binary_att = fields.Many2one(
        'ir.attachment', 'File', copy=True)
    file_binary_url = fields.Char(
        related='file_binary_att.url', string='File URL', store=True, copy=True)
    description = fields.Char('Description', copy=True)
    # message_posted = fields.Boolean('Message Posted')

class ArtNotes(models.Model):
    _name = 'art.notes'

    name = fields.Text('Additional Notes', copy=True)
    files = fields.One2many('art.notes.files', 'note_id', 'Files', copy=True)

    # message_posted = fields.Boolean('Message Posted')

    @api.multi
    def edit(self):
        # self.ensure_one()
        # decoration_rec = self.env['sale.order.line.decoration'].search([('note_id', '=', self.id)])
        # decoration_rec.art_id.write({'message_posted': False})
        return True

    # @api.multi
    # def write(self, vals):
    #     if vals.get('name'):
    #         vals['message_posted'] = False
    #     return super(ArtNotes, self).write(vals)

    @api.multi
    def delete(self):
        self.ensure_one()
        self.unlink()
        return True

    @api.multi
    def submit(self):
        self.ensure_one()
        decoration_obj = self.env['sale.order.line.decoration']
        deco_rec = decoration_obj.browse(self._context.get('active_id', False))
        if deco_rec:
            deco_rec.write({'note_id': self.id})
            # deco_rec.art_id.write({'message_posted': False})
        return True


class SaleOrderApprovalLevel(models.Model):
    _name = 'sale.order.approval.level'

    name = fields.Char('Approval Level Name', required=True)
    lower_limit = fields.Float(
        'Lower Limit', help="Inclusive Lower Limit", required=True)
    approval_order = fields.Integer(string="Approval Order")
    upper_limit = fields.Float(
        'Upper Limit', help="Inclusive Upper Limit", required=True)
    users = fields.Many2many(
        'res.users', string='Approval Authorities/Officiales', required=True)

    @api.model
    def create(self, vals):
        if 'lower_limit' in vals:
            if len(self.search(['&', ('lower_limit', '<=',
                                      vals.get('lower_limit', False)),
                                ('upper_limit', '>=',
                                 vals.get('lower_limit', False))])) != 0:
                raise UserError(
                    _('You can not enter a lower limit that will be' +
                      ' included in other Approval Level.'))
        if 'upper_limit' in vals:
            if len(self.search(['&', ('lower_limit', '<=',
                                      vals.get('upper_limit', False)),
                                ('upper_limit', '>=',
                                 vals.get('upper_limit', False))])) != 0:
                raise UserError(
                    _('You can not enter a upper limit that will be' +
                      ' included in other Approval Level.'))
        if 'lower_limit' in vals and 'upper_limit' in vals:
            if len(self.search(['&', ('lower_limit', '>=',
                                      vals.get('lower_limit', False)),
                                ('upper_limit', '<=',
                                 vals.get('upper_limit', False))])) != 0:
                raise UserError(
                    _('You can not enter a range that will include the' +
                      ' range of other Approval Level.'))
            if vals.get('lower_limit', False) > vals.get('upper_limit', False):
                raise UserError(
                    _('You can not enter the lower limit greater than the' +
                      ' upper limit of an Approval Level.'))
        result = super(SaleOrderApprovalLevel, self).create(vals)
        if 'lower_limit' in vals:
            levels = self.search([]).sorted(key=lambda r: r.lower_limit)
            index = 1
            while index <= len(levels):
                levels[index - 1].approval_order = index
                index += 1
        return result

    @api.multi
    def write(self, vals):
        if 'lower_limit' in vals:
            for record in self:
                if len(self.search(['&', ('lower_limit', '<=',
                                          vals.get('lower_limit', False)), '&',
                                    ('upper_limit', '>=',
                                     vals.get('lower_limit', False)),
                                    ('id', '!=', record.id)])) != 0:
                    raise UserError(
                        _('You can not enter a lower limit will be' +
                          ' included in other Approval Level.'))
                if len(self.search(['&', ('lower_limit', '>=',
                                          vals.get('lower_limit', False)),
                                    ('upper_limit', '<=',
                                     record.upper_limit),
                                    ('id', '!=', record.id)])) != 0:
                    raise UserError(
                        _('You can not enter a range that will include the' +
                          ' range of other Approval Level.'))
                if vals.get('lower_limit', False) > record.upper_limit:
                    raise UserError(
                        _('You can not enter the lower limit greater than'
                          + ' the upper limit of an Approval Level.'))
        if 'upper_limit' in vals:
            for record in self:
                if len(self.search(['&', ('lower_limit', '<=',
                                          vals.get('upper_limit', False)), '&',
                                    ('upper_limit', '>=',
                                     vals.get('upper_limit', False)),
                                    ('id', '!=', record.id)])) != 0:
                    raise UserError(
                        _('You can not enter a upper limit that will be' +
                          ' included in other Approval Level.'))
                if len(self.search(['&', ('lower_limit', '>=',
                                          record.lower_limit),
                                    ('upper_limit', '<=',
                                     vals.get('upper_limit', False)),
                                    ('id', '!=', record.id)])) != 0:
                    raise UserError(
                        _('You can not enter a range that will include the' +
                          ' range of other Approval Level.'))
                if record.lower_limit > \
                        vals.get('upper_limit', False):
                    raise UserError(
                        _('You can not enter the lower limit greater than' +
                          ' the upper limit of an Approval Level.'))
        if 'lower_limit' in vals and 'upper_limit' in vals:
            for record in self:
                if len(self.search(['&', ('lower_limit', '>=',
                                          vals.get('lower_limit', False)),
                                    ('upper_limit', '<=',
                                     vals.get('upper_limit', False)),
                                    ('id', '!=', record.id)])) != 0:
                    raise UserError(
                        _('You can not enter a range that will include the' +
                          ' range of other Approval Level.'))
                if vals.get('lower_limit', False) > \
                        vals.get('upper_limit', False):
                    raise UserError(
                        _('You can not enter the lower limit greater than' +
                          ' the upper limit of an Approval Level.'))

        result = super(SaleOrderApprovalLevel, self).write(vals)
        if 'lower_limit' in vals:
            levels = self.search([]).sorted(key=lambda r: r.lower_limit)
            index = 1
            while index <= len(levels):
                levels[index - 1].approval_order = index
                index += 1
        return result

    @api.onchange('users')
    def _onchange_users(self):
        group_ref = self.env.ref('sales_team.group_sale_salesman_all_leads')
        if group_ref:
            return {'domain': {'users': [('id', 'in', group_ref.users.ids)]}}


class SaleOrderApprovalLevelHistory(models.Model):
    _name = 'sale.order.approval.level.history'

    approval_level = fields.Many2one(
        'sale.order.approval.level', string='Approval Level')
    approval_order = fields.Integer(related="approval_level.approval_order")
    approved_by = fields.Many2one('res.users', string="Action By")
    order_id = fields.Many2one('sale.order', string="Sale Order")
    status = fields.Char('Status')
    comments = fields.Text('Comments')


# class google_config(models.TransientModel):
#     _inherit = "base.config.settings"

#     @api.multi
#     def _get_api_key(self):
#         return self.env['ir.config_parameter'].get_param('google_api_key')

#     @api.multi
#     def _get_inventory_url(self):
#         return self.env['ir.config_parameter'].get_param('inventory_url')

#     google_api_key = fields.Char('Google API key', default=_get_api_key)
#     inventory_url = fields.Char('Base Get Inventory URL', default=_get_inventory_url)

#     @api.multi
#     def set_api_key(self):
#         self.env['ir.config_parameter'].set_param('google_api_key', self.google_api_key)

#     @api.multi
#     def set_inventory_url(self):
#         self.env['ir.config_parameter'].set_param('inventory_url', self.inventory_url)


class ArtDashboard(models.Model):
    _name = 'art.dashboard'

    def get_dashboard_data(self):
        aws = {}
        art_obj = self.env['art.production']
        revision = art_obj.search_count([('status', '=', 'revision')])
        in_progress = art_obj.search_count([('state', '=', 'in_process')])
        archived = art_obj.search_count([('active', '=', False)])
        recreated = art_obj.search_count(
            [('art_type', '=', 'raster_recreate')])
        submitted = art_obj.search_count([('state', '=', 'submit')])
        aws['revision'] = revision
        aws['in_progress'] = in_progress
        aws['archived'] = archived
        aws['recreated'] = recreated
        aws['submitted'] = submitted
        return aws


class PartnerChannel(models.Model):
    _name = 'partner.channel'
    _order = 'name'

    name = fields.Char()
    analytic_tag_ids = fields.Many2many('account.analytic.tag', string='Analytic Tags')

class PartnerProgram(models.Model):
    _name = 'partner.program'
    _order = 'name'

    name = fields.Char()
    analytic_tag_ids = fields.Many2many('account.analytic.tag', string='Analytic Tags')

class OrderType(models.Model):
    _name = 'order.type'

    name = fields.Char()


# 13173: DEV: Art Worksheet - Issue Tab
class TrackIssue(models.Model):
    _name = 'track.issue'
    _description = 'Art Worksheet Track Issues'

    @api.one
    @api.depends('request_type', 'revision')
    def _get_request_type(self):
        if self.request_type:
            if self.request_type == 'new_order':
                status = 'New Order'
            elif self.request_type == 'reorder':
                status = 'Reorder w/ Revision'
            elif self.request_type == 'revision':
                status = 'Revision' + ' ' + str(self.revision)
            elif self.request_type == 'option_delete_ftp':
                status = 'Option Delete'
            elif self.request_type == 'virtual_spec':
                status = 'Virtual Spec'
            elif self.request_type == 'store_image':
                status = 'Store Image'
            else:
                status = 'Unknown'
            self.request_type1 = status

    notes_team = fields.Selection([('art', 'Art'), ('sales', 'Sales')], 'Notes For - Team')
    request_type = fields.Selection([('new_order', 'New Order'),
                                     ('reorder', 'Reorder w/ Revision'),
                                     ('revision', 'Revision'),
                                     ('option_delete_ftp', 'Option Delete'),
                                     ('virtual_spec', 'Virtual Spec'),
                                     ('store_image', 'Store Image')], 'Request Type')
    request_type1 = fields.Char(compute='_get_request_type', string='Request Type')
    revision = fields.Integer('Revision #', copy=False)
    notes_user = fields.Many2one('res.users', 'Notes For - User')
    notes = fields.Text('Notes')
    user_id = fields.Many2one('res.users', 'Submitted By', default=lambda self: self.env.user)
    designer_id = fields.Many2one('res.users', 'Designer')
    art_id = fields.Many2one('art.production', 'Art')
    notes_added = fields.Boolean('Notes Added')
    auto = fields.Boolean('Auto Transaction')
    rush = fields.Char(string='Rush')

    @api.multi
    def write(self, vals):
        if vals.get('notes'):
            self.notes_added = True
        return super(TrackIssue, self).write(vals)

    @api.model
    def create(self, vals):
        res = super(TrackIssue, self).create(vals)
        if vals.get('notes'):
            res.notes_added = True
        if not vals.get('auto'):
            track_ids = []
            for track in res.art_id.track_issue_ids:
                if track.auto:
                    track_ids.append(track)
            final_track_id = track_ids and track_ids[-1] or False
            if final_track_id:
                if res.revision == final_track_id.revision and res.request_type == final_track_id.request_type:
                    res.designer_id = final_track_id.designer_id.id
                    res.user_id = final_track_id.user_id.id
        return res

    @api.multi
    def delete_issue(self):
        flag = False
        for rec in self:
            if not rec.auto:
                rec.unlink()
                flag = True
            else:
                rec.write({'notes_team': False,
                           'notes_user': False,
                           'notes': False,
                           })
        if flag:
            return {
                'type': 'ir.actions.client',
                'tag': 'reload',
            }
        else:
            return True


class SaleOrderLineNotes(models.Model):
    _name = 'sale.order.line.notes'
    _description = 'Sale Order Line Notes'

    product_tmpl_id = fields.Many2one('product.template', readonly=True, string='Product', copy=False)
    vendor = fields.Many2one('res.partner', readonly=True, string='Vendor', copy=False)
    product_notes = fields.Text(string="Product Notes", copy=False)
    vendor_notes = fields.Text(string="Vendor Notes", copy=False)
    proof_notes = fields.Text(string="Proof Notes", copy=False)
    sale_order_id = fields.Many2one('sale.order', ondelete="cascade")
    select = fields.Boolean(string="Select", default=False, copy=False)

    @api.multi
    @api.onchange('proof_notes')
    def _onchange_proof_notes(self):
        seller = self.product_tmpl_id._fetch_seller(self.vendor.id)
        if self.proof_notes and seller and seller.product_name and not self.proof_notes.startswith(
                        '[%s]' % seller.product_name):
            self.proof_notes = ('[%s] %s \n') % (seller.product_name, self.proof_notes or ' ')

    @api.multi
    @api.onchange('product_notes')
    def _onchange_product_notes(self):
        seller = self.product_tmpl_id._fetch_seller(self.vendor.id)
        if self.product_notes and seller and seller.product_name and not self.product_notes.startswith(
                        '[%s]' % seller.product_name):
            self.product_notes = ('[%s] %s \n') % (seller.product_name, self.product_notes or ' ')

class DecorationLocation(models.Model):
    _inherit = "decoration.location"

    is_it_created_at_so_or_po_level = fields.Boolean('Is it created at SO/PO level ?', default=False)

    @api.model
    def name_search(self, name='', args=None, operator='ilike', limit=100):
        vendor_decoration_locations = []
        if self.env.context.get('imprint_location', False) and not self._context.get('is_the_global_search_enabled', False):
            try:
                args_for_manipulation = [arg for arg in args]
                if args_for_manipulation[len(args_for_manipulation)-3] == '|':
                    vendor_decoration_locations = args_for_manipulation[len(args_for_manipulation)-1][2]
            except Exception:
                return super(DecorationLocation, self).name_search(name=name, args=args, operator=operator, limit=limit)
        result = super(DecorationLocation, (self.with_context(vendor_decoration_locations=vendor_decoration_locations))).name_search(name=name, args=args, operator=operator, limit=limit)
        return result

    @api.multi
    def name_get(self):
        """ name_get() -> [(id, name), ...]
        Returns a textual representation for the records in ``self``.
        By default this is the value of the ``display_name`` field.
        :return: list of pairs ``(id, text_repr)`` for each records
        :rtype: list(tuple)
        """
        result = []
        name = self._rec_name
        vendor_decoration_locations = self.env.context.get('vendor_decoration_locations', [])
        if name in self._fields:
            convert = self._fields[name].convert_to_display_name
            for record in self:
                if record.id in vendor_decoration_locations:
                    result.append((record.id, convert(record[name], record) + " (Vendor)"))
                else:
                    result.append((record.id, convert(record[name], record)))
        else:
            for record in self:
                if record.id in vendor_decoration_locations:
                    result.append((record.id, "%s,%s (Vendor)" % (record._name, record.id)))
                else:
                    result.append((record.id, "%s,%s" % (record._name, record.id)))
        return result

    @api.model
    def search_read(self, domain=None, fields=None, offset=0, limit=None, order=None):
        """
        Performs a ``search()`` followed by a ``read()``.

        :param domain: Search domain, see ``args`` parameter in ``search()``. Defaults to an empty domain that will match all records.
        :param fields: List of fields to read, see ``fields`` parameter in ``read()``. Defaults to all fields.
        :param offset: Number of records to skip, see ``offset`` parameter in ``search()``. Defaults to 0.
        :param limit: Maximum number of records to return, see ``limit`` parameter in ``search()``. Defaults to no limit.
        :param order: Columns to sort result, see ``order`` parameter in ``search()``. Defaults to no sort.
        :return: List of dictionaries containing the asked fields.
        :rtype: List of dictionaries.

        """
        records = self.search(domain or [], offset=offset, limit=limit, order=order)
        if not records:
            return []

        if fields and fields == ['id']:
            # shortcut read if we only want the ids
            return [{'id': record.id} for record in records]

        # read() ignores active_test, but it would forward it to any downstream search call
        # (e.g. for x2m or function fields), and this is not the desired behavior, the flag
        # was presumably only meant for the main search().
        # TODO: Move this to read() directly?
        if 'active_test' in self._context:
            context = dict(self._context)
            del context['active_test']
            records = records.with_context(context)

        result = records.read(fields)
        if len(result) <= 1:
            return result

        # reorder read
        index = {vals['id']: vals for vals in result}
        result = [index[record.id] for record in records if record.id in index]
        if self.env.context.get('imprint_location', False) and not self._context.get('is_the_global_search_enabled', False):
            vendor_decoration_locations = []
            try:
                if domain[len(domain)-3] == '|':
                    vendor_decoration_locations = domain[len(domain)-1][2]
            except Exception:
                return result
            for record in result:
                if record.get('id', False) and record['id'] in vendor_decoration_locations:
                    if record.get('name', False):
                        record['name'] += " (Vendor)"
        return result

    @api.model
    def create(self, vals):
        if self._context.get('is_the_global_search_enabled', False):
            if self._context.get('imprint_location', False):
                vals['is_it_created_at_so_or_po_level'] = True
        if 'name' in vals:
            existed_record_same_name = self.search([('name', '=ilike', vals['name'])])
            if existed_record_same_name:
                raise UserError(_("The Decoration Location already exists in system with same name."))
                # raise UserError(_("The Decoration Location already exists in system with same name. \n\nIf that is not visible to you, then please follow the below steps :"
                #     + "\n1. Go To Menu: Sales App -> Sales ->  Configuration -> Decoration -> Decoration Location."
                #     + "\n2. Search with the same name."
                #     + "\n3. Open that found record (Open the found Decoration Location in its form view) ."
                #     + "\n4. Uncheck the 'Is it created at SO/PO level ?' field.")) 
        result = super(DecorationLocation, self).create(vals)
        return result

    # 17355 [DEV] : Should use template when in Proforma status
    # @api.model
    # def _search(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):
    #     if not (self._context.get('imprint_location', False) and self._context.get('is_the_global_search_enabled', False)):
    #         if type(args) is list:
    #             if len(args) >= 1:
    #                 args.insert(0, '&')
    #             args.append(tuple(('is_it_created_at_so_or_po_level', '!=', True)))
    #     return super(DecorationLocation, self)._search(args, offset=offset, limit=limit, order=order,
    #                                         count=count, access_rights_uid=access_rights_uid)
    
    @api.model
    def _search(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):
        """
        Private implementation of search() method, allowing specifying the uid to use for the access right check.
        This is useful for example when filling in the selection list for a drop-down and avoiding access rights errors,
        by specifying ``access_rights_uid=1`` to bypass access rights check, but not ir.rules!
        This is ok at the security level because this method is private and not callable through XML-RPC.

        :param access_rights_uid: optional user ID to use when checking access rights
                                  (not for ir.rules, this is only for ir.model.access)
        :return: a list of record ids or an integer (if count is True)
        """
        self.sudo(access_rights_uid or self._uid).check_access_rights('read')

        # For transient models, restrict access to the current user, except for the super-user
        if self.is_transient() and self._log_access and self._uid != SUPERUSER_ID:
            args = expression.AND(([('create_uid', '=', self._uid)], args or []))

        query = self._where_calc(args)
        self._apply_ir_rules(query, 'read')
        order_by = self._generate_order_by(order, query)
        from_clause, where_clause, where_clause_params = query.get_sql()

        where_str = where_clause and (" WHERE %s" % where_clause) or ''

        if count:
            # Ignore order, limit and offset when just counting, they don't make sense and could
            # hurt performance
            query_str = 'SELECT count(1) FROM ' + from_clause + where_str
            self._cr.execute(query_str, where_clause_params)
            res = self._cr.fetchone()
            return res[0]

        limit_str = limit and ' limit %d' % limit or ''
        offset_str = offset and ' offset %d' % offset or ''
        query_str = 'SELECT "%s".id FROM ' % self._table + from_clause + where_str + order_by + limit_str + offset_str
        if self.env.context.get('imprint_location', False) and not self._context.get('is_the_global_search_enabled', False):
            try:
                temp_where_str = where_str[(where_str.rindex("OR") + 4) : len(where_str)]
                if temp_where_str.find('FALSE') == -1:
                    second_temp_where_str = temp_where_str[0 : temp_where_str.index(")") + 2]
                    order_by_case = " ORDER BY case when " + second_temp_where_str + " then 2 else 1 end, " + order_by[order_by.index("BY")+3: -1]
                    where_str_for_order_by_case = where_str[0 : (where_str.rindex("OR")) - 2] + where_str[where_str.rindex("OR") + 4 + len(second_temp_where_str) : len(where_str)]
                    query_str = 'SELECT "%s".id FROM ' % self._table + from_clause + where_str_for_order_by_case + order_by_case + limit_str + offset_str
            except Exception:
                query_str = 'SELECT "%s".id FROM ' % self._table + from_clause + where_str + order_by + limit_str + offset_str
        self._cr.execute(query_str, where_clause_params)
        res = self._cr.fetchall()

        # TDE note: with auto_join, we could have several lines about the same result
        # i.e. a lead with several unread messages; we uniquify the result using
        # a fast way to do it while preserving order (http://www.peterbe.com/plog/uniqifiers-benchmark)
        def _uniquify_list(seq):
            seen = set()
            return [x for x in seq if x not in seen and not seen.add(x)]

        return _uniquify_list([x[0] for x in res])

class DecorationMethod(models.Model):
    _inherit = "decoration.method"

    is_it_created_at_so_or_po_level = fields.Boolean('Is it created at SO/PO level ?', default=False)

    @api.model
    def name_search(self, name='', args=None, operator='ilike', limit=100):
        vendor_decoration_mathods = []
        if self.env.context.get('imprint_method', False) and not self._context.get('is_the_global_search_enabled', False):
            try:
                args_for_manipulation = [arg for arg in args]
                if args_for_manipulation[len(args_for_manipulation)-3] == '|':
                    vendor_decoration_mathods = args_for_manipulation[len(args_for_manipulation)-1][2]
            except Exception:
                return super(DecorationMethod, self).name_search(name=name, args=args, operator=operator, limit=limit)
        result = super(DecorationMethod, (self.with_context(vendor_decoration_mathods=vendor_decoration_mathods))).name_search(name=name, args=args, operator=operator, limit=limit)
        return result

    @api.multi 
    def name_get(self):
        """ name_get() -> [(id, name), ...]
        Returns a textual representation for the records in ``self``.
        By default this is the value of the ``display_name`` field.
        :return: list of pairs ``(id, text_repr)`` for each records
        :rtype: list(tuple)
        """
        result = []
        name = self._rec_name
        vendor_decoration_mathods = self.env.context.get('vendor_decoration_mathods', [])
        if name in self._fields:
            convert = self._fields[name].convert_to_display_name
            for record in self:
                if record.id in vendor_decoration_mathods:
                    result.append((record.id, convert(record[name], record) + " (Vendor)"))
                else:
                    result.append((record.id, convert(record[name], record)))
        else:
            for record in self:
                if record.id in vendor_decoration_mathods:
                    result.append((record.id, "%s,%s (Vendor)" % (record._name, record.id)))
                else:
                    result.append((record.id, "%s,%s" % (record._name, record.id)))
        return result


    @api.model
    def search_read(self, domain=None, fields=None, offset=0, limit=None, order=None):
        """
        Performs a ``search()`` followed by a ``read()``.

        :param domain: Search domain, see ``args`` parameter in ``search()``. Defaults to an empty domain that will match all records.
        :param fields: List of fields to read, see ``fields`` parameter in ``read()``. Defaults to all fields.
        :param offset: Number of records to skip, see ``offset`` parameter in ``search()``. Defaults to 0.
        :param limit: Maximum number of records to return, see ``limit`` parameter in ``search()``. Defaults to no limit.
        :param order: Columns to sort result, see ``order`` parameter in ``search()``. Defaults to no sort.
        :return: List of dictionaries containing the asked fields.
        :rtype: List of dictionaries.

        """
        records = self.search(domain or [], offset=offset, limit=limit, order=order)
        if not records:
            return []

        if fields and fields == ['id']:
            # shortcut read if we only want the ids
            return [{'id': record.id} for record in records]

        # read() ignores active_test, but it would forward it to any downstream search call
        # (e.g. for x2m or function fields), and this is not the desired behavior, the flag
        # was presumably only meant for the main search().
        # TODO: Move this to read() directly?
        if 'active_test' in self._context:
            context = dict(self._context)
            del context['active_test']
            records = records.with_context(context)

        result = records.read(fields)
        if len(result) <= 1:
            return result

        # reorder read
        index = {vals['id']: vals for vals in result}
        result = [index[record.id] for record in records if record.id in index]
        if self.env.context.get('imprint_method', False) and not self._context.get('is_the_global_search_enabled', False):
            vendor_decoration_mathods = []
            try:
                if domain[len(domain)-3] == '|':
                    vendor_decoration_mathods = domain[len(domain)-1][2]
            except Exception:
                return result
            for record in result:
                if record.get('id', False) and record['id'] in vendor_decoration_mathods:
                    if record.get('name', False):
                        record['name'] += " (Vendor)"
        return result

    @api.model
    def create(self, vals):
        if self._context.get('is_the_global_search_enabled', False):
            if self._context.get('imprint_method', False):
                vals['is_it_created_at_so_or_po_level'] = True
        if 'name' in vals:
            existed_record_same_name = self.search([('name', '=ilike', vals['name'])])
            if existed_record_same_name:
                raise UserError(_("The Decoration Method already exists in system with same name."))
                # raise UserError(_("The Decoration Method already exists in system with same name. \n\nIf that is not visible to you, then please follow the below steps :"
                #     + "\n1. Go To Menu: Sales App -> Sales ->  Configuration -> Decoration -> Decoration Method."
                #     + "\n2. Search with the same name."
                #     + "\n3. Open that found record (Open the found Decoration Method in its form view) ."
                #     + "\n4. Uncheck the 'Is it created at sale order level ?' field.")) 
        result = super(DecorationMethod, self).create(vals)
        return result

    # 17355 [DEV] : Should use template when in Proforma status
    # @api.model
    # def _search(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):
    #     if not (self._context.get('imprint_method', False) and self._context.get('is_the_global_search_enabled', False)):
    #         if type(args) is list:
    #             if len(args) >= 1:
    #                 args.insert(0, '&')
    #             args.append(tuple(('is_it_created_at_so_or_po_level', '!=', True)))
    #     return super(DecorationMethod, self)._search(args, offset=offset, limit=limit, order=order,
    #                                         count=count, access_rights_uid=access_rights_uid)

    @api.model
    def _search(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):
        """
        Private implementation of search() method, allowing specifying the uid to use for the access right check.
        This is useful for example when filling in the selection list for a drop-down and avoiding access rights errors,
        by specifying ``access_rights_uid=1`` to bypass access rights check, but not ir.rules!
        This is ok at the security level because this method is private and not callable through XML-RPC.

        :param access_rights_uid: optional user ID to use when checking access rights
                                  (not for ir.rules, this is only for ir.model.access)
        :return: a list of record ids or an integer (if count is True)
        """
        self.sudo(access_rights_uid or self._uid).check_access_rights('read')

        # For transient models, restrict access to the current user, except for the super-user
        if self.is_transient() and self._log_access and self._uid != SUPERUSER_ID:
            args = expression.AND(([('create_uid', '=', self._uid)], args or []))

        query = self._where_calc(args)
        self._apply_ir_rules(query, 'read')
        order_by = self._generate_order_by(order, query)
        from_clause, where_clause, where_clause_params = query.get_sql()

        where_str = where_clause and (" WHERE %s" % where_clause) or ''

        if count:
            # Ignore order, limit and offset when just counting, they don't make sense and could
            # hurt performance
            query_str = 'SELECT count(1) FROM ' + from_clause + where_str
            self._cr.execute(query_str, where_clause_params)
            res = self._cr.fetchone()
            return res[0]

        limit_str = limit and ' limit %d' % limit or ''
        offset_str = offset and ' offset %d' % offset or ''
        query_str = 'SELECT "%s".id FROM ' % self._table + from_clause + where_str + order_by + limit_str + offset_str
        if self.env.context.get('imprint_method', False) and not self._context.get('is_the_global_search_enabled', False):
            try:
                temp_where_str = where_str[(where_str.rindex("OR") + 4) : len(where_str)]
                if temp_where_str.find('FALSE') == -1:
                    second_temp_where_str = temp_where_str[0 : temp_where_str.index(")") + 2]
                    order_by_case = " ORDER BY case when " + second_temp_where_str + " then 2 else 1 end, " + order_by[order_by.index("BY")+3: -1]
                    where_str_for_order_by_case = where_str[0 : (where_str.rindex("OR")) - 2] + where_str[where_str.rindex("OR") + 4 + len(second_temp_where_str) : len(where_str)]
                    query_str = 'SELECT "%s".id FROM ' % self._table + from_clause + where_str_for_order_by_case + order_by_case + limit_str + offset_str
            except Exception:
                query_str = 'SELECT "%s".id FROM ' % self._table + from_clause + where_str + order_by + limit_str + offset_str
        self._cr.execute(query_str, where_clause_params)
        res = self._cr.fetchall()

        # TDE note: with auto_join, we could have several lines about the same result
        # i.e. a lead with several unread messages; we uniquify the result using
        # a fast way to do it while preserving order (http://www.peterbe.com/plog/uniqifiers-benchmark)
        def _uniquify_list(seq):
            seen = set()
            return [x for x in seq if x not in seen and not seen.add(x)]

        return _uniquify_list([x[0] for x in res])

class SaleCompaignTable(models.Model):
    _name = 'sale.campaign.table'
    _description = 'Sale Compaign'

    name = fields.Char(string="Compaign Name")
    active = fields.Boolean(string="Active", default=True)

class VendorDecorationLocation(models.Model):
    _inherit = "vendor.decoration.location"

    is_it_created_at_so_or_po_level = fields.Boolean('Is it created at SO/PO level ?', default=False)

    @api.model
    def create(self, vals):
        if self._context.get('is_the_global_search_enabled', False):
            if self._context.get('vendor_decoration_location', False):
                vals['is_it_created_at_so_or_po_level'] = True
        result = super(VendorDecorationLocation, self).create(vals)
        return result

class VendorDecorationMethod(models.Model):
    _inherit = "vendor.decoration.method"

    is_it_created_at_so_or_po_level = fields.Boolean('Is it created at SO/PO level ?', default=False)

    @api.model
    def create(self, vals):
        if self._context.get('is_the_global_search_enabled', False):
            if self._context.get('vendor_decoration_method', False):
                vals['is_it_created_at_so_or_po_level'] = True
        result = super(VendorDecorationMethod, self).create(vals)
        return result

class MonthlyGoals(models.Model):
    _name = 'monthly.goals'

    name = fields.Char('Name')
    month_start = fields.Date('Month Start', copy=True, required=True)
    month_end = fields.Date('Month End', copy=True, required=True)
    working_days = fields.Integer(compute="_compute_working_days")
    daily_gm_goal = fields.Integer('Daily GM Goal', copy=True,)
    monthly_gm_goal = fields.Integer('Monthly GM Goal', copy=True,)
    monthly_user_goals = fields.One2many('user.goals', 'monthly_goal_id', 'User Goals')
    description = fields.Html(compute="_compute_description", string="")

    @api.multi
    def copy(self, default=None):
        self.ensure_one()
        result = super(MonthlyGoals, self).copy(default)
        for line in self.monthly_user_goals:
            line.copy({'monthly_goal_id': result.id})
        return result

    @api.one
    @api.depends('monthly_user_goals.user_group', 'monthly_user_goals.amount')
    def _compute_description(self):
        dict_user_group = {}
        for user_goal in self.monthly_user_goals:
            if dict_user_group.has_key(user_goal.user_group.name):
                dict_user_group[user_goal.user_group.name] += user_goal.amount
            else:
                dict_user_group[user_goal.user_group.name] = user_goal.amount

        description = ''
        for group in dict_user_group:
            description = '%s <span style="font-weight:bold">%s:</span> </bold> <span style="font-size: 14px;">$ %s</span><br/>' % (description, group, str(dict_user_group[group]))
        self.description = description

    @api.multi
    @api.depends('month_start', 'month_end')
    def _compute_working_days(self):
        for this in self:
            this.working_days = 0
            if this.month_start and this.month_end:
                 this.working_days = (datetime.datetime.strptime(this.month_end, DEFAULT_SERVER_DATE_FORMAT)- datetime.datetime.strptime(this.month_start, DEFAULT_SERVER_DATE_FORMAT)).days


class UserGoals(models.Model):
    _name = 'user.goals'
    _order = 'user_group'

    monthly_goal_id = fields.Many2one('monthly.goals')
    user_id = fields.Many2one('res.users', 'User', copy=True)
    user_group = fields.Many2one('res.user.new.group','Group', store=True, compute='_compute_user_group')
    currency_id = fields.Many2one('res.currency', compute='_compute_currency', store=True)
    amount = fields.Monetary(currency_field='currency_id', string='Amount', copy=True)
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id)

    _sql_constraints = [
        ('user_goal_month_unique',
         'unique(monthly_goal_id, user_id)',
         'A user already set to current month goal.')
    ]

    @api.one
    @api.depends('user_id', 'user_id.new_res_user_group')
    def _compute_user_group(self):
        for this in self:
            this.user_group = this.user_id.new_res_user_group

    @api.multi
    @api.depends('user_id')
    def _compute_currency(self):
        for this in self:
            this.currency_id = this.company_id.currency_id