# -*- coding: utf-8 -*-
from odoo import api, models, fields

class IrExports(models.Model):
    _inherit = 'ir.exports'

    ir_exports_line_ids = fields.One2many('ir.exports.line', 'export_id', string="Ir Exports Lines")