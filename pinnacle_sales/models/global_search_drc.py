# -*- coding: utf-8 -*-
from odoo import api, models, fields, _
from openerp.exceptions import UserError

class GlobalSearchConfig(models.TransientModel):
    _inherit = 'global.search.config'

    group_global_search_attchment = fields.Boolean('Attachments', implied_group='pinnacle_sales.group_global_search_attchment')
    group_global_search_artworksheet = fields.Boolean('Art WorkSheets', implied_group='pinnacle_sales.group_global_search_artworksheet')
    group_global_search_products = fields.Boolean('Products', implied_group='pinnacle_sales.group_global_search_products')

class GlobalSearchResult(models.TransientModel):
    _inherit = 'global.search.result'

    attachment_ids = fields.Many2many('ir.attachment', string="Attachments", readonly=True)
    artworksheet_ids = fields.Many2many('art.production', string="Art WorkSheets", readonly=True)
    product_ids = fields.Many2many('product.template', string="Products", readonly=True)
    is_any_sale_order = fields.Boolean('Is any sale order ?', default=False)
    is_any_purchase_order = fields.Boolean('Is any purchase order ?', default=False)
    is_any_stock_picking = fields.Boolean('Is any stock picking ?', default=False)
    is_any_customer_invoice = fields.Boolean('Is any customer invoice ?', default=False)
    is_any_supplier_invoice = fields.Boolean('Is any supplier invoice ?', default=False)
    is_any_payment = fields.Boolean('Is any payment ?', default=False)
    is_any_calendar_event = fields.Boolean('Is any calendar event ?', default=False)
    is_any_partner = fields.Boolean('Is any partner ?', default=False)
    is_any_attachment = fields.Boolean('Is any attachment ?', default=False)
    is_any_artworksheet = fields.Boolean('Is any art worksheet ?', default=False)
    is_any_product = fields.Boolean('Is any Product ?', default=False)

    @api.multi
    def view_records(self):
        """Preparing domains for serch results and return their actions.
        Returns: Pivot and Graph view for Sales orders."""
        domain = []
        ir_model_data = self.env['ir.model.data']
        if 'sale_order' in self._context:
            domain = [('id', 'in', self.sale_order_ids.ids)]
            return {
                'name': _('Sales Orders'),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'tree,kanban,form,calendar,pivot,graph',
                'res_model': 'sale.order',
                'search_view_id': ir_model_data.get_object_reference('sale', 'sale_order_view_search_inherit_sale')[1],
                'context': dict(self.env.context or {}),
                'domain': domain,
            }
        if 'purchase_order' in self._context:
            domain = [('id', 'in', self.purchase_order_ids.ids)]
            return {
                'name': _('Purchase Orders'),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'tree,kanban,form,pivot,graph,calendar',
                'res_model': 'purchase.order',
                'search_view_id': ir_model_data.get_object_reference('purchase', 'view_purchase_order_filter')[1],
                'context': dict(self.env.context or {}),
                'domain': domain,
            }
        if 'picking' in self._context:
            domain = [('id', 'in', self.picking_ids.ids)]
            return {
                'name': _('Pickings'),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'tree,kanban,form,calendar',
                'res_model': 'stock.picking',
                'search_view_id': ir_model_data.get_object_reference('stock', 'view_picking_internal_search')[1],
                'context': dict(self.env.context or {}),
                'domain': domain,
            }
        if 'c_invoice' in self._context:
            domain = [('id', 'in', self.customer_invoice_ids.ids)]
            tree_view = ir_model_data.get_object_reference('account', 'invoice_tree')[1]
            form_view = ir_model_data.get_object_reference('account', 'invoice_form')[1]
            return {
                'name': _('Customer Invoices'),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'tree,kanban,form,calendar,pivot,graph',
                'res_model': 'account.invoice',
                'views': [(tree_view, 'tree'), (form_view, 'form')],
                'search_view_id': ir_model_data.get_object_reference('account', 'view_account_invoice_filter')[1],
                'context': dict(self.env.context or {}),
                'domain': domain,
            }
        if 's_invoice' in self._context:
            domain = [('id', 'in', self.supplier_invoice_ids.ids)]
            return {
                'name': _('Vendor Bills'),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'tree,kanban,form,calendar,pivot,graph',
                'res_model': 'account.invoice',
                'search_view_id': ir_model_data.get_object_reference('account', 'view_account_invoice_filter')[1],
                'context': dict(self.env.context or {}),
                'domain': domain,
            }
        if 'payment' in self._context:
            domain = [('id', 'in', self.payment_ids.ids)]
            return {
                'name': _('Payments'),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'tree,form,graph',
                'res_model': 'account.payment',
                'context': dict(self.env.context or {}),
                'domain': domain,
            }
        if 'meeting' in self._context:
            domain = [('id', 'in', self.calendar_event_ids.ids)]
            return {
                'name': _('Meetings'),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'tree,calendar,form',
                'res_model': 'calendar.event',
                'search_view_id': ir_model_data.get_object_reference('calendar', 'view_calendar_event_search')[1],
                'context': dict(self.env.context or {}),
                'domain': domain,
            }
        if 'partner' in self._context:
            domain = [('id', 'in', self.partner_ids.ids)]
            return {
                'name': _('Partners'),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'tree,kanban,form',
                'res_model': 'res.partner',
                'search_view_id': ir_model_data.get_object_reference('base', 'view_res_partner_filter')[1],
                'context': dict(self.env.context or {}),
                'domain': domain,
            }
        if 'attachment' in self._context:
            domain = [('id', 'in', self.attachment_ids.ids)]
            return {
                'name': _('Attachments'),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'ir.attachment',
                'context': dict(self.env.context or {}),
                'domain': domain,
            }
        if 'art' in self._context:
            domain = [('id', 'in', self.artworksheet_ids.ids)]
            return {
                'name': _('Art Worksheet'),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'art.production',
                'context': dict(self.env.context or {}),
                'domain': domain,
            }
        if 'product' in self._context:
            domain = [('id', 'in', self.product_ids.ids)]
            return {
                'name': _('Products'),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'tree,kanban,form',
                'res_model': 'product.template',
                'context': dict(self.env.context or {}),
                'domain': domain,
            }



class SearchQueryWizard(models.TransientModel):
    _inherit = 'search.query.wizard'

    @api.multi
    def perform_search(self): 
        self.ensure_one()
        if not self.search_query:
            raise UserError(_('Please enter at least one character to search.'))
        search_result_obj = self.env['global.search.result']
        search_result = \
            search_result_obj.create({'query_string': 'Search Results for -' +
                                      str(self.search_query),
                                      'search_result_summary': ''})
        if self.user_has_groups('global_search_drc.group_global_search_sale_order'
                                ) \
            and self.user_has_groups('sales_team.group_sale_salesman'
                                     ):
            sale_order_obj = self.env['sale.order']
            sale_orders = sale_order_obj.search([
                '|',
                ('name', 'ilike', self.search_query),
                '|',
                ('so_opportunity_id', 'ilike', self.search_query),
                '|',
                ('partner_id.name', 'ilike', self.search_query),
                '|',
                ('partner_id.parent_id.name', 'ilike', self.search_query),
                '|',
                ('partner_id.ref', 'ilike', self.search_query),
                '|',
                ('origin', 'ilike', self.search_query),
                '|',
                ('user_id.name', 'ilike', self.search_query),
                '|',
                ('order_line.name', 'ilike', self.search_query),
                '|',
                ('order_line.product_id.product_tmpl_id.name',
                 'ilike', self.search_query),
                ('order_line.product_id.default_code', 'ilike',
                 self.search_query),
                ])
            if sale_orders:
                search_result.is_any_sale_order = True
                search_result.sale_order_ids = [(6, 0,
                                                 sale_orders.ids)]
                search_result.search_result_summary += \
                    '\n Sale Orders : ' + str(len(sale_orders))
        if self.user_has_groups('global_search_drc.group_global_search_purchase_order'
                                ) \
            and self.user_has_groups('purchase.group_purchase_user'
                                     ):
            purchase_order_obj = self.env['purchase.order']
            purchase_orders = purchase_order_obj.search([
                '|',
                ('name', 'ilike', self.search_query),
                '|',
                ('partner_id.name', 'ilike', self.search_query),
                '|',
                ('partner_id.parent_id.name', 'ilike', self.search_query),
                '|',
                ('partner_id.ref', 'ilike', self.search_query),
                '|',
                ('so_partner_id.name', 'ilike', self.search_query),
                '|',
                ('so_partner_id.parent_id.name', 'ilike', self.search_query),
                '|',
                ('so_partner_id.ref', 'ilike', self.search_query),
                '|',
                ('origin', 'ilike', self.search_query),
                '|',
                ('order_line.name', 'ilike', self.search_query),
                '|',
                ('order_line.product_id.product_tmpl_id.name',
                 'ilike', self.search_query),
                ('order_line.product_id.default_code', 'ilike',
                 self.search_query),
                ])
            if purchase_orders:
                search_result.is_any_purchase_order = True
                search_result.purchase_order_ids = [(6, 0,
                                                     purchase_orders.ids)]
                search_result.search_result_summary += \
                    '\n Purchase Orders : ' \
                    + str(len(purchase_orders))

        if self.user_has_groups('global_search_drc.group_global_search_picking'
                                ) \
            and (self.user_has_groups('stock.group_stock_user')or
                 self.user_has_groups('purchase.group_purchase_user'
                                      )or
                 self.user_has_groups('base.group_sale_salesman'
                                      )):
            stock_picking_obj = self.env['stock.picking']
            stock_pickings = stock_picking_obj.search([
                '|',
                ('name', 'ilike', self.search_query),
                '|',
                ('partner_id.name', 'ilike', self.search_query),
                '|',
                ('partner_id.parent_id.name', 'ilike', self.search_query),
                '|',
                ('origin', 'ilike', self.search_query),
                '|',
                ('pack_operation_product_ids.product_id.product_tmpl_id.name',
                'ilike', self.search_query),
                ('pack_operation_product_ids.product_id.default_code',
                'ilike', self.search_query),
                ])
            if stock_pickings:
                search_result.is_any_stock_picking = True
                search_result.picking_ids = [(6, 0,
                        stock_pickings.ids)]
                search_result.search_result_summary += \
                    '\n Pickings : ' + str(len(stock_pickings))

        if self.user_has_groups('global_search_drc.group_global_search_invoice'
                                ) \
            and (self.user_has_groups('account.group_account_invoice'
                                      ) or self.user_has_groups('stock.group_stock_user'
                                                                )or
                 self.user_has_groups('purchase.group_purchase_user'
                                      ) or
                 self.user_has_groups('base.group_sale_salesman'
                                      )):
            account_invoice_obj = self.env['account.invoice']
            customer_invoice = account_invoice_obj.search([
                '|',
                ('origin', 'ilike', self.search_query),
                '|',
                ('name', 'ilike', self.search_query),
                '|',
                ('number', 'ilike', self.search_query),
                '|',
                ('partner_id.name', 'ilike', self.search_query),
                '|',
                ('partner_id.parent_id.name', 'ilike', self.search_query),
                '|',
                ('user_id.name', 'ilike', self.search_query),
                '|',
                ('invoice_line_ids.name', 'ilike',
                 self.search_query),
                '|',
                ('invoice_line_ids.product_id.product_tmpl_id.name', 'ilike', self.search_query),
                ('invoice_line_ids.product_id.default_code', 'ilike', self.search_query),
                ('type', 'in', ('out_invoice', 'out_refund')),
                ])
            if customer_invoice:
                search_result.is_any_customer_invoice = True
                search_result.customer_invoice_ids = [(6, 0,
                        customer_invoice.ids)]
                search_result.search_result_summary += \
                    '\n Customer Invoices : ' \
                    + str(len(customer_invoice))

            supplier_invoice = account_invoice_obj.search([
                '|',
                ('origin', 'ilike', self.search_query),
                '|',
                ('name', 'ilike', self.search_query),
                '|',
                ('number', 'ilike', self.search_query),
                '|',
                ('partner_id.name', 'ilike', self.search_query),
                '|',
                ('partner_id.parent_id.name', 'ilike', self.search_query),
                '|',
                ('user_id.name', 'ilike', self.search_query),
                '|',
                ('invoice_line_ids.name', 'ilike',
                 self.search_query),
                '|',
                ('invoice_line_ids.product_id.product_tmpl_id.name', 'ilike', self.search_query),
                ('invoice_line_ids.product_id.default_code', 'ilike', self.search_query),
                ('type', 'in', ('in_invoice', 'in_refund')),
                ])
            if supplier_invoice:
                search_result.is_any_supplier_invoice = True
                search_result.supplier_invoice_ids = [(6, 0,
                                                       supplier_invoice.ids)]
                search_result.search_result_summary += \
                    '\n Supplier Invoices : ' \
                    + str(len(supplier_invoice))

        if self.user_has_groups('global_search_drc.group_global_search_payment') and self.user_has_groups('account.group_account_invoice'):
            account_payment_obj = self.env['account.payment']
            payments = account_payment_obj.search(['|', ('name',
                    'ilike', self.search_query), '|',
                    ('communication', 'ilike',
                    self.search_query), ('partner_id.name',
                    'ilike', self.search_query)])
            if payments:
                search_result.is_any_payment = True
                search_result.payment_ids = [(6, 0, payments.ids)]
                search_result.search_result_summary += \
                    '\n Payments : ' + str(len(payments))

        if self.user_has_groups('global_search_drc.group_global_search_meeting'
                                ):
            calendar_event_obj = self.env['calendar.event']
            calendar_events = calendar_event_obj.search([
                '|',
                ('name', 'ilike', self.search_query),
                '|',
                ('partner_ids.name', 'ilike', self.search_query),
                '|',
                ('partner_ids.parent_id.name', 'ilike', self.search_query),
                '|',
                ('location', 'ilike', self.search_query),
                '|',
                ('description', 'ilike', self.search_query),
                ('user_id.name', 'ilike', self.search_query),
                ])
            if calendar_events:
                search_result.is_any_calendar_event = True
                search_result.calendar_event_ids = [(6, 0,
                        calendar_events.ids)]
                search_result.search_result_summary += \
                    '\n Meetings : ' + str(len(calendar_events))

        if self.user_has_groups('global_search_drc.group_global_search_partner'
                                ):
            res_partner_obj = self.env['res.partner']
            res_partners = res_partner_obj.search([
                '|',
                ('name', 'ilike', self.search_query),
                '|',
                ('street', 'ilike', self.search_query),
                '|',
                ('city', 'ilike', self.search_query),
                '|',
                ('zip', 'ilike', self.search_query),
                '|',
                ('phone', 'ilike', self.search_query),
                '|',
                ('email', 'ilike', self.search_query),
                ('website', 'ilike', self.search_query),
                ])
            if res_partners:
                search_result.is_any_partner = True
                search_result.partner_ids = [(6, 0,
                                              res_partners.ids)]
                search_result.search_result_summary += \
                    '\n Partners : ' + str(len(res_partners))
        if self.user_has_groups('pinnacle_sales.group_global_search_attchment'):
            ir_attachment_obj = self.env['ir.attachment']
            ir_attachments = ir_attachment_obj.search([
                '|',
                ('name', 'ilike', self.search_query),
                '|',
                ('mimetype', 'ilike', self.search_query),
                '|',
                ('create_uid.name', 'ilike', self.search_query),
                ('res_name', 'ilike', self.search_query)
                ])
            if ir_attachments:
                search_result.is_any_attachment = True
                search_result.attachment_ids = [(6, 0, ir_attachments.ids)]
                search_result.search_result_summary += '\n Attachments : ' + str(len(ir_attachments))
        if self.user_has_groups('pinnacle_sales.group_global_search_artworksheet'):
            ir_artworksheet_obj = self.env['art.production']
            ir_artworksheets = ir_artworksheet_obj.search([
                '|',
                ('common_sale_order', 'ilike', self.search_query),
                '|',
                ('sale_orders', 'ilike', self.search_query),
                '|',
                ('partner_id.name', 'ilike', self.search_query),
                '|',
                ('partner_id.parent_id.name', 'ilike', self.search_query),
                '|',
                ('partner_id.ref', 'ilike', self.search_query),
                '|',
                ('initial_designer_id.name', 'ilike', self.search_query),
                ('designer_id.name', 'ilike', self.search_query)
                ])
            if ir_artworksheets:
                search_result.is_any_artworksheet = True
                search_result.artworksheet_ids = [(6, 0, ir_artworksheets.ids)]
                search_result.search_result_summary += '\n Art WorkSheets : ' + str(len(ir_artworksheets))
        if self.user_has_groups('pinnacle_sales.group_global_search_products'):
            ir_product_obj = self.env['product.template']
            ir_products = ir_product_obj.search([
                '|',
                ('name', 'ilike', self.search_query),
                '|',
                ('pinnacle_default_code', 'ilike', self.search_query),
                '|',
                ('product_sku', 'ilike', self.search_query),
                '|',
                ('hs_code', 'ilike', self.search_query),
                '|',
                ('seller_ids.name.name', 'ilike', self.search_query),
                ('description', 'ilike', self.search_query)])
            if ir_products:
                search_result.is_any_product = True
                search_result.product_ids = [(6, 0, ir_products.ids)]
                search_result.search_result_summary += '\n Products : ' + str(len(ir_products))
        if search_result.search_result_summary == '':
            search_result.search_result_summary = \
                'Records not found'

        return {
            'name': 'Search Result',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'global.search.result',
            'type': 'ir.actions.act_window',
            'target': 'inline',
            'res_id': search_result.id,
            }
