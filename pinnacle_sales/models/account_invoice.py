# -*- coding: utf-8 -*-

from openerp import models, fields, api, _
from openerp.exceptions import UserError

class AccountInvoice(models.Model): 
    _inherit = "account.invoice"

    send_email_of_sale_invoice = fields.Boolean(string="Send Auto Email Of Invoice To Customer ?", default=False)
    customer_po_id = fields.Char('Customer PO ID')
    customer_po_file = fields.Binary('Customer PO File')
    customer_po_file_name = fields.Char('Customer PO File Name')
    accounting_department_users_email = fields.Char(compute="_compute_accounting_department_users_email",
                                       string='Accounting Department Users Email', store=True, copy=False)
    accounting_department_partners_id = fields.Char(compute="_compute_accounting_department_users_email",
                                       string='Accounting Department Users Id', store=True, copy=False)
    ship_to_multiple_locations = fields.Boolean('Ship To Multiple Locations')
    ship_to_location = fields.Many2one('res.partner', string="Ship To Location")
    ship_to_address_name = fields.Char('Shipping Address Name')
    ship_to_street1 = fields.Char('Shipping Street1')
    ship_to_street2 = fields.Char('Shipping Street2')
    ship_to_city = fields.Char('Shipping City')
    ship_to_state = fields.Many2one('res.country.state', string='Shipping State')
    ship_to_zipcode = fields.Char('Shipping Zipcode')
    ship_to_country = fields.Many2one('res.country', string='Shipping Country')
    internal_ref_so = fields.Char('Internal Reference SO', compute='_get_orders_ref')
    is_netknact = fields.Boolean("Is Netknact Channel")
    is_ship_to_alabama = fields.Boolean("Is all products to Alabama")
    date_create = fields.Date('Date Create')

    @api.depends('origin')
    def _get_orders_ref(self):
        """Compute Internal Ref. on Invoice
        Returns: String of Internal Ref. of Sales orders with comma seprated"""
        for rec in self:
            if rec.origin:
                ref = ''
                for order in self.env['sale.order'].search([('name', 'in', str(rec.origin).split(', '))]):
                    if order.internal_ref:
                        ref += order.internal_ref + ','
                rec.internal_ref_so = ref

    @api.one
    @api.depends('partner_id')
    def _compute_accounting_department_users_email(self):
        self.accounting_department_users_email = False
        self.accounting_department_partners_id = False
        accoutant_group_id = self.env.ref('account.group_account_user')
        if not accoutant_group_id:
            raise UserError(_('Accountant Group is not available'))
        if self.partner_id:
            for user in accoutant_group_id.users:
                if not self.accounting_department_users_email and user.partner_id.email:
                    self.accounting_department_users_email = user.partner_id.email
                    self.accounting_department_partners_id = str(user.partner_id.id)
                    continue
                if self.accounting_department_users_email and user.partner_id.email:
                    self.accounting_department_users_email = self.accounting_department_users_email + ',' + user.partner_id.email
                    self.accounting_department_partners_id = self.accounting_department_partners_id  + ',' + str(user.partner_id.id)

    @api.multi
    def action_invoice_open(self):
        res = super(AccountInvoice, self).action_invoice_open()
        for invoice in self:
            order_ids = [sl.order_id.id for sl in [line.sale_line_ids for line in invoice.invoice_line_ids]]
            order_ids = set(order_ids)
            for sale_order in self.env['sale.order'].browse(order_ids):
                if all(inv.state == 'open' for inv in sale_order.invoice_ids) and sale_order.invoice_status == 'invoiced':
                    if sale_order.state not in ['draft', 'art_processing', 'pending_review', 'to_approve', 'approve', 'reject', 'sent', 'confirmation_received',  'accounting_approval', 'accounting_approved', 'accounting_rejected', 'cancel', 'hold', 'inactive', 'sale', 'delivered', 'close', 'reopen', 'done']:
                        sale_order.state = 'invoiced'
        return res

    @api.multi
    def write(self, vals):
        if vals.get('partner_id', False):
            customer = self.env['res.partner'].browse([vals.get('partner_id', False)])
            if customer:
                vals['send_email_of_sale_invoice'] = customer.send_email_of_sale_invoice
        res = super(AccountInvoice, self).write(vals)
        return res

    @api.model
    def create(self, vals):
        if vals.get('partner_id', False):
            customer = self.env['res.partner'].browse([vals.get('partner_id', False)])
            if customer:
                vals['send_email_of_sale_invoice'] = customer.send_email_of_sale_invoice
        vals.update({'date_create': fields.Date.today()})
        res = super(AccountInvoice, self).create(vals)
        return res

    def is_list_has_all_values_similar(self, list):
        for item in list[1:]:
            if item != list[0]:
                return False
        return True
        
class SaleAdvancePaymentInv(models.TransientModel):
    _inherit = "sale.advance.payment.inv"

    @api.multi
    def _create_invoice(self, order, so_line, amount):
        result = super(SaleAdvancePaymentInv, self)._create_invoice(order, so_line, amount)
        sale_orders = self.env['sale.order'].browse(self._context.get('active_ids', []))
        if result:
            if len(sale_orders) == 1:
                result.send_email_of_sale_invoice = order.send_email_of_sale_invoice
                result.customer_po_id = order.customer_po_id
                result.customer_po_file = order.customer_po_file
                result.customer_po_file_name = order.customer_po_file_name
            set_value_of_ship_to_locations = []
            ship_to_address = {}
            ship_to_location = False
            is_netKnacks = []
            check_ship_to_alabama = []
            for sale_order in sale_orders:
                for shipping_line in sale_order.shipping_line:
                    if shipping_line.contact:
                        set_value_of_ship_to_locations.append(shipping_line.contact.id)
                        if not ship_to_address.keys():
                            ship_to_address['ship_to_address_name'] = shipping_line.address_name
                            ship_to_address['ship_to_street1'] = shipping_line.street1
                            ship_to_address['ship_to_street2'] = shipping_line.street2
                            ship_to_address['ship_to_city'] = shipping_line.city
                            ship_to_address['ship_to_state'] = shipping_line.state_id and shipping_line.state_id.id or False
                            ship_to_address['ship_to_zipcode'] = shipping_line.zipcode
                            ship_to_address['ship_to_country'] = shipping_line.contact_country_id and shipping_line.contact_country_id.id or False
                    if shipping_line.ship_to_location == 'yes' and not ship_to_location:
                        ship_to_location = True
                is_netKnacks.append(sale_order.is_netKnacks())
                check_ship_to_alabama.append(sale_order.check_ship_to_alabama())
            if ship_to_location:
                result.ship_to_multiple_locations = True
                result.ship_to_location = False
            else:
                if set_value_of_ship_to_locations:
                    if result.is_list_has_all_values_similar(set_value_of_ship_to_locations):
                        result.ship_to_multiple_locations = False
                        result.ship_to_location = set_value_of_ship_to_locations[0]
                        if ship_to_address.keys():
                            result.ship_to_address_name = ship_to_address['ship_to_address_name']
                            result.ship_to_street1 = ship_to_address['ship_to_street1']
                            result.ship_to_street2 = ship_to_address['ship_to_street2']
                            result.ship_to_city = ship_to_address['ship_to_city']
                            result.ship_to_state = ship_to_address['ship_to_state']
                            result.ship_to_zipcode = ship_to_address['ship_to_zipcode']
                            result.ship_to_country = ship_to_address['ship_to_country']
                    else:
                        result.ship_to_multiple_locations = True
                        result.ship_to_location = False
                else:
                    result.ship_to_multiple_locations = False
                    result.ship_to_location = False
            if is_netKnacks:
                if result.is_list_has_all_values_similar(is_netKnacks):
                    result.is_netknact = is_netKnacks[0]
                else:
                    result.is_netknact = False
            else:
                result.is_netknact = False
            if check_ship_to_alabama:
                if result.is_list_has_all_values_similar(check_ship_to_alabama):
                    result.is_ship_to_alabama = check_ship_to_alabama[0]
                else:
                    result.is_ship_to_alabama = False
            else:
                result.is_ship_to_alabama = False
        return result

    @api.multi
    def create_invoices(self):
        result = super(SaleAdvancePaymentInv, self).create_invoices()
        sale_orders = self.env['sale.order'].browse(self._context.get('active_ids', []))
        if result  and sale_orders and 'come_form_partial_invoice_button' in self.env.context:
            for order in sale_orders:
                order.state = 'partially_invoiced'
        return result

class AccountPaymentTerm(models.Model):
    _inherit = "account.payment.term"
    
    po_id_and_file_field_in_sale_order = fields.Boolean('Do you Want to put the "Required" constraint of PO ID And File fields in Sale Order With this Payment Term use ? ', help="It also populate the PO ID And File fields in Partner.")

class AccountTax(models.Model):
    _inherit = 'account.tax'

    @api.onchange('amount', 'zero_value')
    def onchange_amount(self):
        if self.zero_value:
            if self.amount != 0.0:
                raise UserError('Value of Amount field must be 0.0, please uncheck OOS - 0% to assign any value!')

    @api.constrains('zero_value')
    def check_default_customer_term(self):
        zero_value = self.env['account.tax'].search([('zero_value', '=', True)])
        if not zero_value:
            return
        if len(zero_value) == 1:
            return
        if len(zero_value) == 1 and zero_value == self:
            return
        else:
            raise UserError('You can only assign one tax record to be OOS - 0%!')

    zero_value = fields.Boolean('OOS - 0%')

