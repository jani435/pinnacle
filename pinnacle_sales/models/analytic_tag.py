# -*- coding: utf-8 -*-
from odoo import models, fields

class AccountAnalyticTag(models.Model):
    _inherit = 'account.analytic.tag'

    is_program_type_required = fields.Boolean('Is program type required ?')
    is_netknacks = fields.Boolean('Is Netknacks ?')
