# -*- coding: utf-8 -*-
from openerp import models, fields, api, _
from openerp.exceptions import UserError, AccessError
from odoo.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
import openerp.addons.decimal_precision as dp
from math import ceil
from odoo.tools.misc import formatLang
import pytz
import datetime
import time
from dateutil import relativedelta
from odoo import tools
import base64
import urlparse
import urllib2, cStringIO
from PIL import Image
import logging
_logger = logging.getLogger(__name__)
import requests, json
from odoo.tools import config

so_dict = {'quote_draft': 'Draft Quote',
               'quote_vir_processing': 'Virtual Processing',
               'quote_sent': 'Quote Sent',
               'closed_quote': 'Quote Closed',
               'quote_review': 'Review Quote',
               'quote_man_approval': 'Manager Approval',
               'quote_man_approved': 'Manager Approved',
               'quote_man_rejected': 'Manager Rejected',
               'quote_approved': 'Quote Approved',
               'quote_rejected': 'Quote Rejected',
               'draft': 'Draft',
               'art_processing': 'Art Processing',
               'pending_review': 'Pending Review',
               'to_approve': 'Manager Approval',
               'approve': 'Manager Approved',
               'reject': 'Manager Rejected',
               'sent': 'Confirmation Sent',
               'confirmation_received': 'Confirmation Received',
               'accounting_approval': 'Accounting Approval',
               'accounting_approved': 'Accounting Approved',
               'accounting_rejected': 'Accounting Rejected',
               'accounting_p_approval': 'Accounting Payment Approval',
               'accounting_p_approved': 'Accounting Payment Approved',
               'accounting_p_rejected': 'Accounting Payment Rejected',
               'sale': 'PO Generated',
               'shipped': 'Shipped',
               'partially_invoiced': 'Partially Invoiced',
               'invoiced': 'Invoiced',
               'delivered': 'Delivered',
               'close': 'Closed',
               'reopen': 'Re-Opened',
               'done': 'Re-Closed',
               'cancel': 'Cancelled',
               'hold': 'On Hold',
               'inactive': 'Inactive',
               'ready_for_fullfillment': 'Ready for Fulfillment'}
class CRMLead(models.Model):
    _inherit = 'crm.lead'

    @api.model
    def retrieve_sales_dashboard(self):
        """Return: dict of sales dashboard values like counts of different order types"""
        result = super(CRMLead, self).retrieve_sales_dashboard()
        so_obj = self.env['sale.order']
        pending = so_obj.search_count([('state', 'in', ['pending_review', 'approve']), '|', '|', ('user_id', '=', self.env.user.id), ('soc_id', '=', self.env.user.id), ('aam_id', 'in', [self.env.user.id])])
        outstanding = so_obj.search_count([('state', '=', 'sent'), '|', '|', ('user_id', '=', self.env.user.id), ('soc_id', '=', self.env.user.id), ('aam_id', 'in', [self.env.user.id])])
        priority = so_obj.search_count([('tag_ids', '=like', 'Priority'), '|', '|', ('user_id', '=', self.env.user.id), ('soc_id', '=', self.env.user.id), ('aam_id', 'in', [self.env.user.id])])
        issues = so_obj.search_count([('tag_ids', '=like', 'Orders with Issues'), '|', '|', ('user_id', '=', self.env.user.id), ('soc_id', '=', self.env.user.id), ('aam_id', 'in', [self.env.user.id])])
        open_orders = so_obj.search_count([('is_regular', '=', True), ('state', 'in', ('sale','shipped','partially_invoiced','invoiced','delivered','hold', 'reopen')), '|', '|', ('user_id', '=', self.env.user.id), ('soc_id', '=', self.env.user.id), ('aam_id', 'in', [self.env.user.id])])
        confirmation = so_obj.search_count([('state', '=', 'confirmation_received'), '|', '|', ('user_id', '=', self.env.user.id), ('soc_id', '=', self.env.user.id), ('aam_id', 'in', [self.env.user.id])])
        result['pending'] = pending
        result['outstanding'] = outstanding
        result['priority'] = priority
        result['issues'] = issues
        result['open'] = open_orders
        result['confirmation'] = confirmation
        return result

class SaleOrder(models.Model): 
    _inherit = 'sale.order'

    def _default_state(self):
        if 'quote_draft_state_default' in self._context:
            return 'quote_draft'
        else:
            return 'draft'

    def _default_state_duplicate(self):
        if 'quote_draft_state_default' in self._context:
            return 'quote_draft'

    @api.multi
    def _get_default_quote(self):
        if 'quote_draft_state_default' in self._context:
            return True
        else:
            return False

    @api.depends('order_line.price_total', 'order_line.discount_amount')
    def _amount_all(self):
        for order in self:
            amount_untaxed = amount_tax = 0.0
            for line in order.order_line:
                amount_untaxed += line.price_subtotal
                amount_tax += line.price_tax
            order.update({
                'amount_untaxed': order.pricelist_id.currency_id.round(amount_untaxed),
                'amount_tax': order.pricelist_id.currency_id.round(amount_tax),
                'amount_total': amount_untaxed + amount_tax - order.discount_total,
            })

    SALEORDER_STATES = [
        ('quote_draft', 'Draft Quote'),
        ('quote_vir_processing', 'Virtual Processing'),
        ('quote_sent', 'Quote Sent'),
        ('closed_quote', 'Quote Closed'),
        ('quote_review', 'Review Quote'),
        ('quote_man_approval', 'Manager Approval'),
        ('quote_man_approved', 'Manager Approved'),
        ('quote_man_rejected', 'Manager Rejected'),
        ('quote_approved', 'Quote Approved'),
        ('quote_rejected', 'Quote Rejected'),
        ('draft', 'Draft'),
        ('art_processing', 'Art Processing'),
        ('pending_review', 'Pending Review'),
        ('to_approve', 'Manager Approval'),
        ('approve', 'Manager Approved'),
        ('reject', 'Manager Rejected'),
        ('sent', 'Confirmation Sent'),
        ('confirmation_received', 'Confirmation Received'),
        ('accounting_approval', 'Accounting Approval'),
        ('accounting_approved', 'Accounting Approved'),
        ('accounting_rejected', 'Accounting Rejected'),
        ('accounting_p_approval', 'Accounting Payment Approval'),
        ('accounting_p_approved', 'Accounting Payment Approved'),
        ('accounting_p_rejected', 'Accounting Payment Rejected'),
        ('sale', 'PO Generated'),
        ('ready_for_fullfillment', 'Ready for Fulfillment'),
        ('shipped', 'Shipped'),
        ('partially_invoiced', 'Partially Invoiced'),
        ('invoiced', 'Invoiced'),
        ('delivered', 'Delivered'),
        ('close', 'Closed'),
        ('reopen', 'Re-Opened'),
        ('done', 'Re-Closed'),
        ('cancel', 'Cancelled'),
        ('hold', 'On Hold'),
        ('inactive', 'Inactive'),
    ]

    QUOTEORDER_STATES = [
        ('quote_draft', 'Draft Quote'),
        ('quote_vir_processing', 'Virtual Processing'),
        ('quote_sent', 'Quote Sent'),
        ('quote_review', 'Review Quote'),
        ('quote_man_approval', 'Manager Approval'),
        ('quote_man_approved', 'Manager Approved'),
        ('quote_man_rejected', 'Manager Rejected'),
        ('quote_approved', 'Quote Approved'),
        ('quote_rejected', 'Quote Rejected'),
        ('closed_quote', 'Quote Closed'),
    ]

    NOTREADONLY_STATES = {
        'quote_draft': [('readonly', False)], 
        'quote_sent': [('readonly', False)], 
        'closed_quote': [('readonly', False)], 
        'draft': [('readonly', False)], 
        'art_processing': [('readonly', False)], 
        'pending_review':[('readonly', False)], 
        'sent': [('readonly', False)], 
        'approve': [('readonly', False)], 
        'confirmation_received': [('readonly', False)], 
        'accounting_approved': [('readonly', False)], 
        'sale': [('readonly', False)], 
        'partially_invoiced': [('readonly', False)], 
        'shipped': [('readonly', False)],
        'reopen': [('readonly', False)], 
        'accounting_p_approved': [('readonly', False)]
    }

    @api.one
    def get_date_withtz(self):
        if self._context.get('tz', False) and self._context.get('lang', False):
            lang = self.env['res.lang'].search([('code','=', self._context.get('lang', False))], limit=1)
            tz = pytz.timezone(self._context.get('tz', False))
            if self.date_order:
                date_order_time = pytz.utc.localize(datetime.datetime.strptime(self.date_order, DEFAULT_SERVER_DATETIME_FORMAT))
                obj_date_order = date_order_time.astimezone(tz)
                date_string = obj_date_order.strftime(lang.date_format + ' %H:%M %p') + ' ET'
                return date_string

    @api.onchange('order_type', 'order_category')
    def onchange_order_type(self):
        if self.order_category == 'regular_order':
            if self.order_type == 'spec' or self.order_type == 'samples':
                self.update({'is_spec': False, 'is_regular': True, 'order_type': 'fo_non_web'})
            else:
                self.update({'is_spec': False, 'is_regular': True})
        if self.order_category == 'sample_order':
            self.update({'is_spec': False, 'is_regular': False, 'order_type': 'samples'})
        if self.order_category == 'spec_order':
            self.update({'is_spec': True, 'is_regular': True, 'order_type': 'spec'})

        if (self.is_regular and self.order_type == 'spec' and not self.is_spec) or (self.is_regular and self.order_type == 'samples' and not self.is_spec):
            return {'warning': {'title': 'Error!', 'message': 'You can not set this order type for normal orders'}}
        if not self.is_regular and self.order_type != 'samples' and not self.is_spec:
            return {'warning': {'title': 'Error!', 'message': 'You must select order type Sample for this order'}}
        if self.is_regular and self.order_type != 'spec' and self.is_spec:
            return {'warning': {'title': 'Error!', 'message': 'You must select order type Spec for this order'}}
        customer_type = False
        if self.order_type == 'fo_non_web' and self.partner_id.customer_type == 'new':
            customer_type = 'new'
        if self.order_type == 'fo_non_web' and self.partner_id.customer_type == 'program':
            customer_type = 'program'
        if self.order_type == 'repeat_non_web' and self.partner_id.customer_type == 'current':
            customer_type = 'current'
        if self.order_type == 'repeat_non_web' and self.partner_id.customer_type == 'program':
            customer_type = 'program'
        if self.order_type == 'fo_web_order' and self.partner_id.customer_type == 'new':
            customer_type = 'web_new'
        if self.order_type == 'fo_web_order' and self.partner_id.customer_type == 'program':
            customer_type = 'program'
        if self.order_type == 'repeat_web_order' and self.partner_id.customer_type == 'current':
            customer_type = 'web_current_customer'
        if self.order_type == 'repeat_web_order' and self.partner_id.customer_type == 'program':
            customer_type = 'program'
        if self.order_type == 'spec' and self.partner_id.customer_type == 'new':
            customer_type = 'new'
        if self.order_type == 'spec' and self.partner_id.customer_type == 'current':
            customer_type = 'current'
        if self.order_type == 'spec' and self.partner_id.customer_type == 'program':
            customer_type = 'program'
        art_ids = []
        for line in self.art_line:
            if line.art_id and line.art_id.id not in art_ids:
                art_ids.append(line.art_id.id)
        if art_ids:
            art_browse_ids = self.env['art.production'].browse(art_ids)
            art_browse_ids.write({'customer_type': customer_type})

    @api.depends('order_line')
    def _compute_art_submitted(self):
        for this in self:
            this.art_submitted = False
            for line in this.art_line:
                if line.art_id.submitted:
                    this.art_submitted = True
                    break

    @api.multi
    def set_virtual_processing(self):
        if self.quote:
            vp = []
            # art_ids = []
            for line in self.art_line:
                # if line.art_id and line.art_id.id not in art_ids:
                #     art_ids.append(line.art_id.id)
                if line.art_id.state == 'submit':
                    vp.append(True)
                else:
                    vp.append(False)
            if all(vp):
                # if art_ids:
                #     art_browse_ids = self.env['art.production'].browse(art_ids)
                #     art_browse_ids.write({'status': 'virtual_spec'})
                self.state = 'quote_vir_processing'

    @api.multi
    def set_quote_review(self):
        if self.quote:
            vp = []
            # art_ids = []
            for line in self.art_line:
                # if line.art_id and line.art_id.id not in art_ids:
                    # art_ids.append(line.art_id.id)
                if line.art_id.state == 'completed':
                    vp.append(True)
                else:
                    vp.append(False)
            if all(vp):
                # if art_ids:
                #     art_browse_ids = self.env['art.production'].browse(art_ids)
                #     art_browse_ids.write({'status': 'virtual_spec'})
                self.state = 'quote_review'

    @api.one
    @api.depends('amount_total')
    def _compute_review_quote_flag(self):
        if self.amount_total > 5000:
            self.review_quote_flag = True
        else:
            self.review_quote_flag = False

    @api.one 
    @api.depends('decoration_group_ids')
    def _get_decoration_group(self):
        self.decoration_group_read_ids = [(6, 0, self.decoration_group_ids.ids)]

    is_spec = fields.Boolean("Spec Sale order", default=False)
    is_regular = fields.Boolean("Sample Sale order", default=True)
    decoration_line = fields.One2many('sale.order.line', 'order_id',
                                      string='Decoration Lines',
                                      states={'cancel': [('readonly', True)],
                                              'done': [('readonly', True)]},
                                      domain=[('can_decorate', '=', True),
                                              ('product_type', '!=', 'service')
                                              ])
    decorations = fields.One2many('sale.order.line.decoration','sale_order_id',string='Decoration Lines', copy=False)
    decoration_group_ids = fields.One2many('sale.order.line.decoration.group', 'order_id', string="Decoration Groups", copy=False)
    decoration_group_read_ids = fields.One2many('sale.order.line.decoration.group', 'order_id', compute="_get_decoration_group", string="Decoration Groups")
    decoration_sequence_id = fields.Integer(
        string='Decoration Line Sequence', readonly=True, default=1, copy=True)
    decoration_group_sequence_id = fields.Integer(
        string='Decoration Group Sequence', readonly=True, default=1, copy=False)
    shipping_line = fields.One2many('shipping.line', 'order', copy=False)
    confirmation_sent = fields.Datetime('Confirmation Sent', copy=False)
    confirmation_received = fields.Datetime('Confirmation Received', copy=False)
    feedback_lines = fields.One2many('feedback.lines', 'order_id', string='Summary', copy=False)
    art_line = fields.One2many('art.production.line', 'sale_order_id', 'ArtWorkSheet Lines')
    artworksheets = fields.One2many('art.production', 'sale_id', 'ArtWorkSheet Lines')
    # attachment_line = fields.One2many('sale.order.line', 'order_id', string='Attachments',
    #                                   domain=[('can_decorate', '=', True),
    #                                           ('product_type', '!=', 'service'), ('spec_file2', '!=', False),
    #                                           ('hide_file', '=', False), ('decorations.decoration_group_id', '!=', False)])
    approval_level = fields.Many2one(
        'sale.order.approval.level', string='Approval Level', copy=False)
    approval_history = fields.One2many(
        'sale.order.approval.level.history', 'order_id',
        string='Approval History', copy=False)
    state = fields.Selection(SALEORDER_STATES, string='Status',
        readonly=True,
        copy=False,
        index=True, track_visibility=False,
        default=lambda self: self._default_state())
    state_quote = fields.Selection(QUOTEORDER_STATES, string='Status',
        readonly=True,
        copy=False,
        index=True, track_visibility=False,
        default=lambda self: self._default_state_duplicate())
    order_line = fields.One2many('sale.order.line', 'order_id', string='Order Lines',readonly=True, states=NOTREADONLY_STATES, copy=False)
    approval_users_email = fields.Char(compute="_compute_approval_user_email",
                                       string='Approval Users Email', store=True, copy=False)
    accounting_approval_users_email = fields.Char(compute="_compute_accounting_approval_user_email",
                                       string='Accounting Approval Users Email', store=True, copy=False)
    approval_url = fields.Char("Approval URL", copy=False)
    reorder_ref = fields.Many2one('sale.order', string="Reorder Reference", default=False, copy=False)
    splttedorder_ref = fields.Many2one('sale.order', string="Splitted Reference", default=False, copy=False)
    margin_in_percentage = fields.Float(compute='_compute_margin_in_percentage', string="Blended Margin", glp="It gives profitability in percentage by calculating the difference between the Unit Price and the cost.", digits=dp.get_precision('Product Price'), store=True, copy=False, group_operator="avg")
    margin_in_percentage_duplicate = fields.Float(compute='_compute_margin_in_percentage', string="Blended Margin", help="It gives profitability in percentage by calculating the difference between the Unit Price and the cost.", digits=dp.get_precision('Product Price'), store=True, copy=False)
    product_margin_in_percentage = fields.Float(compute='_compute_product_margin_in_percentage', string="Margin Of Product", help="It gives profitability in percentage by calculating the difference between the Unit Price and the cost.", digits=dp.get_precision('Product Price'), store=True, copy=False)
    product_margin_in_percentage_duplicate = fields.Float(compute='_compute_product_margin_in_percentage', string="Margin Of Product", help="It gives profitability in percentage by calculating the difference between the Unit Price and the cost.", digits=dp.get_precision('Product Price'), store=True, copy=False)
    shipping_margin_in_percentage = fields.Float(compute='_compute_shipping_margin_in_percentage', string="Margin Of Shipping", help="It gives profitability in percentage by calculating the difference between the Unit Price and the cost.", digits=dp.get_precision('Product Price'), store=True, copy=False)
    shipping_margin_in_percentage_duplicate = fields.Float(compute='_compute_shipping_margin_in_percentage', string="Margin Of Shipping", help="It gives profitability in percentage by calculating the difference between the Unit Price and the cost.", digits=dp.get_precision('Product Price'), store=True, copy=False)
    aam_id = fields.Many2many('res.users', string="AAM", copy=False)
    soc_id = fields.Many2one('res.users', string="Vendor Point of Contact", copy=False)
    user_id = fields.Many2one('res.users', string='Account Manager', index=True, default=False, copy=False, track_visibility=False)
    last_state = fields.Char()
    not_account_user = fields.Boolean('Not Account User', compute='_check_user', default=False)
    send_email_of_sale_invoice = fields.Boolean(string="Allow Account Team to Invoice", default=True, readonly=True, copy=False)
    po_id_and_file_field_in_sale_order = fields.Boolean(related="payment_term_id.po_id_and_file_field_in_sale_order", store=True, readonly=True)
    customer_po_id = fields.Char('Customer PO ID', help='Customer PO ID required for creating Purchase Ordes From the Sale Order, Otherwise approval needed from accounting department', copy=False)
    customer_po_file = fields.Binary('Customer PO File', help='Customer PO File required for creating Purchase Ordes From the Sale Order, Otherwise approval needed from accounting department', copy=False)
    customer_po_file_name = fields.Char('Customer PO File Name', copy=False)
    partner_id = fields.Many2one('res.partner', string='Customer', readonly=True, states=NOTREADONLY_STATES, required=True, change_default=True, index=True, track_visibility=False)
    internal_ref = fields.Char('Internal Reference', readonly=True, states=NOTREADONLY_STATES,
                                help='Reference of the internal order number such as Magento Order Number or Pinnacle Promotions Website Order Number.', copy=False)
    #date_order = fields.Datetime(string='Order Date', required=False, readonly=True, index=True, states=NOTREADONLY_STATES, copy=False, default=False)
    date_order = fields.Datetime(string='Order Date', required=False, readonly=True, index=True, copy=False, default=False)
    validity_date = fields.Date(string='Expiration Date', readonly=True, states=NOTREADONLY_STATES,
        help="Manually set the expiration date of your quotation (offer), or it will set the date automatically based on the template if online quotation is installed.")
    pricelist_id = fields.Many2one('product.pricelist', string='Pricelist', required=True, readonly=True, states=NOTREADONLY_STATES, help="Pricelist for current sales order.",  default=lambda self: self.env.ref('product.list0').id)
    partner_invoice_id = fields.Many2one('res.partner', string='Invoice Address', readonly=True, required=True, states=NOTREADONLY_STATES, help="Invoice address for current sales order.")
    skipped_confirmation_step = fields.Boolean('Skipped Confirmation Step', default=False, copy=False)
    skipped_pending_review = fields.Boolean('Skipped Pending Review Step', default=False, copy=False)
    art_processing_needed = fields.Boolean(compute='_compute_art_processing_needed', string='Skipped Art Processing')
    margin_duplicate = fields.Monetary(compute='_product_margin', help="It gives profitability by calculating the difference between the Unit Price and the cost.", currency_field='currency_id', digits=dp.get_precision('Product Price'), store=True)
    promo_code = fields.Char('Promo Code', copy=True)
    old_pricelist_id = fields.Many2one('product.pricelist', string='Old Pricelist', readonly=True, states=NOTREADONLY_STATES, help="Old Pricelist for current sales order.", copy=True)
    is_promo_code_applied = fields.Selection([('Yes','Yes'),('No','No')], string='Is Promo Code applied ?', readonly=True, copy=True, default='No')
    applied_promo_codes = fields.Char(string='Applied Promo Codes', readonly=True, copy=True)
    approval_status = fields.Char('Status', default='Approved - No Changes - No Comments')
    program_id = fields.Many2one('partner.program', 'Program', copy=False)
    channel_id = fields.Many2one('partner.channel', 'Channel', copy=False)
    program_type = fields.Selection([('custom', 'Custom'), ('store', 'Store'), ('inventory', 'Inventory'), ('nk_inventory', 'NK Inventory')], copy=False)
    so_opportunity_id = fields.Char('Opportunity ID', copy=False)
    reorder_email = fields.Boolean('Send Reorder Email', default=True) # As per Task #14177 default True
    review_email = fields.Boolean('Send Product Review Email', default=True) # As per Task #14177 default True
    magento_order = fields.Boolean('Magento Order')
    billing_info = fields.Text('Specific Billing Information')
    discount_total = fields.Float(compute="_compute_discount_total", string='Total Discount', digits=dp.get_precision('Product Price'), store=True)
    order_type = fields.Selection([('fo_non_web', 'First Order - Non Web'), ('repeat_non_web', 'Repeat - Non Web'), ('fo_web_order', 'First Order - Web Order'),
                                     ('repeat_web_order', 'Repeat - Web Order'), ('samples', 'Samples'), ('spec', 'Spec')], string="Order Type", copy=False)

    is_any_approval_history = fields.Boolean(compute="_compute_is_any_approval_history", string='Is any approval history?')
    customer_company = fields.Char('Company', compute='_get_customer_details')
    art_complete_date = fields.Datetime('Art Complete Date', related='art_line.art_id.complete_date') 
    so_label = fields.Char('Label', copy=False)
    contact_email = fields.Char('Contact Email', compute='_get_customer_details')
    contact_phone = fields.Char('Contact Phone', compute='_get_customer_details')
    in_hand_date = fields.Date('In Hands Date', related='shipping_line.delivery_date')
    category_sales = fields.Many2one('product.category', 'Category', related='order_line.product_id.categ_id')
    inv_date = fields.Date('Invoice Date', compute='_get_invoice_data')
    inv_amount = fields.Float('Invoice Amount', compute='_get_invoice_data')
    remaining_balance = fields.Float('Balance remaining', compute='_get_invoice_data')
    order_conf_html = fields.Html('Order Confirmation HTML')
    promo_notes = fields.Text(readonly=True)
    art_submitted = fields.Boolean(compute='_compute_art_submitted')
    sale_order_notes = fields.One2many('sale.order.line.notes', 'sale_order_id', string="Sale Order Line Notes")
    follow_up_complete = fields.Boolean('Follow Up Complete')
    sale_order_delivery_date = fields.Datetime('Delivery Date')
    payment_term_id = fields.Many2one('account.payment.term', string='Payment Terms', oldname='payment_term', copy=False)
    review_quote_flag = fields.Boolean(compute='_compute_review_quote_flag', store=True)
    quote = fields.Boolean(default=_get_default_quote)
    skip_send = fields.Boolean(default=True)
    skip_send_review = fields.Boolean(default=True)
    tag_ids = fields.Many2many('crm.lead.tag', 'sale_order_tag_rel', 'order_id', 'tag_id', string='Tags', copy=False)
    not_needed_accounting_approval_flag = fields.Boolean(string='Accounting Approval Not Needed')
    account_approval_disp_generate_po = fields.Boolean(string="Generate PO Flag", default=True)
    close_date = fields.Datetime('Closed Date', copy=False)
    reopen_date = fields.Datetime('Re-Opened Date', copy=False)
    reclose_date = fields.Datetime('Re-Closed Date', copy=False)
    customer_art = fields.Many2one('product.decoration.customer.art', related="order_line.decorations.customer_art", string="Art (file or text)", store=True)
    analytic_tag_ids = fields.Many2many('account.analytic.tag', string='Analytic Tags')
    is_program_type_required = fields.Boolean('Is program type required ?', compute='_check_is_program_required')
    accounting_approval_or_rejection_comment = fields.Text('Accounting Approval Or Rejection Comment')
    #is_netknacks = fields.Boolean('Is Netknacks ?', compute='_check_is_netknacks')
    is_customer_po_id_required = fields.Boolean(related="partner_id.is_customer_po_id_required", store=True, readonly=True)
    is_customer_po_file_required = fields.Boolean(related="partner_id.is_customer_po_file_required", store=True, readonly=True)
    amount_untaxed = fields.Monetary(string='Untaxed Amount', store=True, readonly=True, compute='_amount_all', track_visibility=False)
    amount_tax = fields.Monetary(string='Taxes', store=True, readonly=True, compute='_amount_all', track_visibility=False)
    amount_total = fields.Monetary(string='Total', store=True, readonly=True, compute='_amount_all', track_visibility=False)
    is_invoiced = fields.Boolean('Is Invoiced', compute='_get_invoice_data', store=True)
    confirmation_date = fields.Datetime(string='Confirmation Date', readonly=True, index=True, help="Date on which the sale order is confirmed.", oldname="date_confirm", copy=False)
    order_category = fields.Selection([('regular_order','Standard Order'),('sample_order','Sample Order'),('spec_order','Spec Order')],string="Order Category", default="regular_order")
    partner_cc_ids = fields.Char('Partner CC', compute='_get_partner_cc')
    partner_to_ids = fields.Char('Partner CC', compute='_get_partner_to')
    product_margin_in_dollor = fields.Monetary(compute='_compute_product_margin_in_percentage', string="Margin Of Product", help="It gives profitability in percentage by calculating the difference between the Unit Price and the cost.", digits=dp.get_precision('Product Price'), currency_field='currency_id', store=True, copy=False)
    product_margin_in_dollor_duplicate = fields.Monetary(compute='_compute_product_margin_in_percentage', string="Margin Of Product", help="It gives profitability in amount by calculating the difference between the Unit Price and the cost.", currency_field='currency_id', digits=dp.get_precision('Product Price'), store=True, copy=False)
    shipping_margin_in_dollor = fields.Monetary(compute='_compute_shipping_margin_in_percentage', string="Margin Of Shipping", help="It gives profitability in amount by calculating the difference between the Unit Price and the cost.", currency_field='currency_id', digits=dp.get_precision('Product Price'), store=True, copy=False)
    shipping_margin_in_dollor_duplicate = fields.Monetary(compute='_compute_shipping_margin_in_percentage', string="Margin Of Shipping", help="It gives profitability in amount by calculating the difference between the Unit Price and the cost.", currency_field='currency_id', digits=dp.get_precision('Product Price'), store=True, copy=False)
    sale_campaign_id = fields.Many2one('sale.campaign.table', string="Campaign")
    delivery_price = fields.Float(string='Estimated Delivery Price', compute='_compute_delivery_price', store=True)
    number_of_artworksheets_count = fields.Integer(string="Number Of ArtWorkSheets")
    is_order_issue_mail_sent = fields.Boolean('Order Issue mail sent.')
    is_mail_need_to_sent = fields.Boolean('Is Mail need to sent')

    @api.multi
    @api.onchange('tag_ids')
    def _onchange_tag_ids(self):
        for this in self:
            this.is_mail_need_to_sent = False
            order_issue = self.search([('tag_ids', '=like', 'Orders with Issues')])
            if len(order_issue) > 0:
                this.is_mail_need_to_sent = True

    @api.multi
    def see_order_preview(self):
        order_id = str(self.id)
        customer_id = str(self.partner_id.id)
        system_para = config.sysparams.get('sale.order.confirmation.web.url', False)
        if system_para:
            url = str(system_para) + '/confirmation/confirmation_odoo_preview/' + customer_id + '/' + order_id + '/'
            if url:
                return {
                    'res_model': 'ir.actions.act_url',
                    'type'    : 'ir.actions.act_url',
                    'target'  : 'new',
                    'url'     : url,
                }
        else:
            raise UserError('System parameter for "sale.order.confirmation.web.url" is not configured!')

    @api.multi
    def see_order_review(self):
        order_id = str(self.id)
        system_para = config.sysparams.get('sale.order.confirmation.web.url', False)
        if system_para:
            url = str(system_para) + '/cms/odoo/order_review?soid=' + order_id
            if url:
                return {
                    'res_model': 'ir.actions.act_url',
                    'type'    : 'ir.actions.act_url',
                    'target'  : 'new',
                    'url'     : url,
                }
        else:
            raise UserError('System parameter for "sale.order.confirmation.web.url" is not configured!')


    @api.depends('carrier_id', 'order_line')
    def _compute_delivery_price(self):
        return True
        # for order in self:
        #     if order.state != 'draft':
        #         # We do not want to recompute the shipping price of an already validated/done SO
        #         continue
        #     elif order.carrier_id.delivery_type != 'grid' and not order.order_line:
        #         # Prevent SOAP call to external shipping provider when SO has no lines yet
        #         continue
        #     else:
        #         continue
        #         # order.delivery_price = order.carrier_id.with_context(order_id=order.id).price
    

    @api.multi
    @api.onchange('shipping_line')
    @api.depends('shipping_line')
    def onchange_shipping_line(self):
        for this in self:
            check = False
            for shipping_line in this.shipping_line:
                if shipping_line.ship_to_location == 'yes':
                    check = True
                    break
            if check and self.payment_term_id.id == self.env.ref('pinnacle_account.account_payment_term_100pre_cc').id:
                this.send_email_of_sale_invoice = False

    @api.one
    def _get_partner_to(self):
        """Computes values for mail partner_to
        Returns: tuple of ids to character field"""
        ids = []
        if self.user_id and self.user_id.partner_id:
            ids.append(self.user_id.partner_id.id)
        if self.soc_id and self.soc_id.partner_id:
            ids.append(self.soc_id.partner_id.id)
        for aam_id in self.aam_id:
            ids.append(aam_id.partner_id.id)
        if len(ids) == 0:
            self.partner_to_ids = False
        elif len(ids) == 1:
            self.partner_to_ids = str(ids[0])
        else:
            self.partner_to_ids = tuple(ids)

    @api.one
    def _get_partner_cc(self):
        """Computes values for mail partner_cc
        Returns: tuple of ids to character field"""
        ids = []
        if self.user_id and self.user_id.partner_id:
            ids.append(self.user_id.partner_id.id)
        for aam_id in self.aam_id:
            ids.append(aam_id.partner_id.id)
        if len(ids) == 0:
            self.partner_cc_ids = False
        elif len(ids) == 1:
            self.partner_cc_ids = str(ids[0])
        else:
            self.partner_cc_ids = tuple(ids)

    @api.one
    def _check_user(self):
        """Computes values for not an account user flag.
        If user does not contain any group belongs to Account than set value of not_account_user False
        otherwise set True"""
        group_categ = self.env.ref('base.module_category_accounting_and_finance')
        acc_groups = self.env['res.groups'].search([('category_id', '=', group_categ.id)])
        groups_user = self.env.user.groups_id
        flag = True
        for acc_group in acc_groups:
            if acc_group in groups_user:
                flag = False
        self.not_account_user = flag


    @api.multi
    def get_stock_color_name(self, line):
        if len(line.stock_color) == 1:
            return line.stock_color.name
        if len(line.stock_color) > 1:
            colors = ", ".join([color.name for color in line.stock_color])
            return colors

    @api.multi
    def get_pms_code_name(self, line):
        if len(line.pms_code) == 1:
            return line.pms_code.name
        if len(line.pms_code) > 1:
            codes = ", ".join([code.name for code in line.pms_code])
            return codes


    def is_alabama(self, zip_code):
        url = ('http://maps.googleapis.com/maps/api/geocode/json?address=%s')%(zip_code)
        check = False
        response = requests.get(url)
        try:
            response = requests.get(url)
            datas = response.json()
            check = False
            for data in datas.get('results'):
                for val in data.keys():
                    if val == 'address_components':
                        for kk in data[val]:
                            if kk.get('long_name') == 'Alabama':
                                check = True
            return check
        except Exception:
            return False
        return False

    def check_ship_to_alabama(self):
        if not len(self.shipping_line.ids):
            return False
        for ship in self.shipping_line:
            if ship.ship_to_location == 'no':
                ans = self.is_alabama(ship.contact.zip)
                if not (self.is_alabama(ship.contact.zip)):
                    return False
            else:
                zip_codes = ship.zips.split('\n')
                for zip_line in zip_codes:
                    zipline1 = zip_line.split(' ')
                    why = self.is_alabama(zipline1[0])
                    if not self.is_alabama(zipline1[0]):
                        return False
        return True

    def is_netKnacks(self):
        return self.channel_id.name == 'NetKnacks'

    @api.one
    @api.depends('analytic_tag_ids')
    def _check_is_program_required(self):
        if self.analytic_tag_ids:
            if any(tag.is_program_type_required for tag in self.analytic_tag_ids):
                self.is_program_type_required = True

    @api.depends('magento_order')
    @api.onchange('magento_order')
    def onchange_magento_order(self):
        if self.magento_order:
            self.send_email_of_sale_invoice = False


    @api.onchange('program_type')
    def onchange_program_type(self):
        if not self.is_regular and self.program_type == 'inventory':
            self.program_type = False
            return {'warning': {'title': 'Error', 'message': 'You can not select Program Type: Inventory for Sample Orders!'}}

    @api.multi
    def _get_service_name(self, line=None):
        if line:
            for service in line.delivery_line:
                if service.checked:
                    return service.service_type

    @api.model
    def get_unique_images(self):
        spec_file_exist = []
        spec_file2_exist = []
        unique_file = []
        for lines in self.art_line:
            if lines.spec_file_url and lines.spec_file_name and lines.spec_file_name not in spec_file_exist:
                unique_file.append([lines.spec_file_url, lines.spec_file_name, False])
                spec_file_exist.append(lines.spec_file_name)
            if lines.spec_file2_url and lines.spec_file2_name and lines.spec_file2_name not in spec_file2_exist:
                unique_file.append([ lines.spec_file2_url, lines.spec_file2_name, True])
                spec_file2_exist.append(lines.spec_file2_name)
        return unique_file

    @api.model
    def get_unique_images_qwa(self):
        spec_file_exist = []
        spec_file2_exist = []
        unique_file = []
        product_ids = []
        for lines in self.art_line:
            if (lines.spec_file and lines.spec_file_url and lines.spec_file_name and lines.spec_file_name not in spec_file_exist) or lines.product_id.product_tmpl_id.id not in product_ids:
                unique_file.append([lines.spec_file_url, lines.spec_file_name, False, lines.product_id.name])
                spec_file_exist.append(lines.spec_file_name)
            if (lines.spec_file2 and lines.spec_file2_url and lines.spec_file2_name and lines.spec_file2_name not in spec_file2_exist) or lines.product_id.product_tmpl_id.id not in product_ids:
                unique_file.append([lines.spec_file2_url, lines.spec_file2_name, True, lines.product_id.name])
                spec_file2_exist.append(lines.spec_file2_name)
            product_ids.append(lines.product_id.product_tmpl_id.id)
        return unique_file

    @api.model
    def get_unique_images_by_group(self):
        spec_file_exist = []
        spec_file2_exist = []
        unique_file = []
        group = []
        for lines in self.art_line:
            if lines.decoration_group_sequences.id not in group:
                unique_file.append([lines.spec_file_url, lines.decoration_group_sequences.group_id, False, lines.product_id.name])
                spec_file_exist.append(lines.spec_file_name)
                unique_file.append([lines.spec_file2_url, lines.decoration_group_sequences.group_id, True, lines.product_id.name])
                spec_file2_exist.append(lines.spec_file2_name)
                group.append(lines.decoration_group_sequences.id)
                
        return unique_file

    @api.model
    def get_image_from_cfs(self, url, image_tag=False):
        cfs_url = str(self.env['ir.config_parameter'].get_param('cfs.web.url'))
        if cfs_url:
            url = cfs_url+url
        def unicodifier(val):
            if val is None or val is False:
                return u''
            if isinstance(val, str):
                return val.decode('utf-8')
            return unicode(val)
        try:
            req = urllib2.urlopen(url, timeout=50)
            image = Image.open(cStringIO.StringIO(req.read()))
            image.load()
        except Exception:
            _logger.exception("Failed to load remote image %r", url)
            return None
        out = cStringIO.StringIO()
        image.save(out, image.format)
        if not image_tag:
            return out.getvalue().encode('base64')
        else:
            return unicodifier('<img style="max-height:700px; max-width:500px" src="data:%s;base64,%s">' % (Image.MIME[image.format], out.getvalue().encode('base64')))

    @api.multi
    def unlink(self):
        sale_admin_group = self.env.ref('pinnacle_sales.group_sales_pinnacle_admin')
        if sale_admin_group and self.env.user.id in sale_admin_group.users.ids:
            for order in self:
                # Archiving Art Work Sheets
                art_wroksheets_to_be_archived = []
                for order_line in order.order_line:
                    for decoration in order_line.decorations:
                        if decoration.art_id and decoration.art_id.id not in art_wroksheets_to_be_archived:
                            art_wroksheets_to_be_archived.append(decoration.art_id.id)
                if art_wroksheets_to_be_archived:
                    order.order_line.copying_data_of_art_wroksheets_to_be_archived(art_wroksheets_to_be_archived)
                    order.order_line.actual_archiving_of_artworksheets_with_handling_of_other_data(art_wroksheets_to_be_archived)
                purchae_orders = self.env['purchase.order'].search([('sale_order_id','=', order.id)])
                for purchae_order in purchae_orders:
                    purchae_order.with_context(super_admin_delete_sale_order=True).action_cancel()
                    purchae_order.write({'notes': 'This purchase order has been automatically cancelled due to deletion of its related sale order by super admin of sales.'})
                order.with_context(super_admin_delete_sale_order=True).action_cancel()
            result = super(SaleOrder, self).unlink() 
            return result
        else:
            raise AccessError('You do not have permissions to delete. Please contact Administrator.')

    def _get_invoice_data(self):
        """Compute values for inv_date, inv_amount, remaining_balance and is_invoiced
        Param: date inv_date: store first invice's Date Invoice
        Param: float inv_amount: store first invoice's Total Amount
        Param: float remaining_balance: store All Invoices total residual (Amount due)
        Param: bool is_invoiced: stores flag to check so invoiced or not"""
        for rec in self:
            if rec.invoice_ids:
                if rec.state == 'partially_invoiced':
                    rec.inv_date = rec.invoice_ids[0].date_invoice
                    rec.inv_amount = rec.invoice_ids[0].amount_total
                balance = 0.0
                for invoice in rec.invoice_ids:
                    balance += invoice.residual
                rec.remaining_balance = balance
                rec.is_invoiced = True
                rec.write({'is_invoiced': True})

    @api.multi
    def view_analysis(self):
        """Preparing domains for Sales Analysis report.
        Returns: Pivot and Graph view for Sales orders."""
        domain = []
        ir_model_data = self.env['ir.model.data']
        pivot_id = ir_model_data.get_object_reference('pinnacle_sales', 'view_order_product_pivot_management')[1]
        tz = False
        if self.env.user.tz:
            tz = pytz.timezone(self.env.user.tz)
        else:
            tz = pytz.utc
        tzoffset = tz.utcoffset(datetime.datetime.strptime(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), DEFAULT_SERVER_DATETIME_FORMAT))
        start_date = datetime.datetime.strptime(datetime.datetime.now().strftime('%Y-%m-%d 00:00:00'), DEFAULT_SERVER_DATETIME_FORMAT) - tzoffset
        end_date = datetime.datetime.strptime(datetime.datetime.now().strftime('%Y-%m-%d 23:59:59'), DEFAULT_SERVER_DATETIME_FORMAT) - tzoffset
        start_date = start_date.strftime("%Y-%m-%d %H:%M:%S")
        end_date = end_date.strftime("%Y-%m-%d %H:%M:%S")
        start_mo_date = datetime.datetime.strptime(time.strftime('%Y-%m-01 00:00:00'), DEFAULT_SERVER_DATETIME_FORMAT) - tzoffset
        end_mo_date = datetime.datetime.strptime(str(datetime.datetime.now() + relativedelta.relativedelta(months=+1, day=1, days=-1))[:10] + ' 23:59:59', DEFAULT_SERVER_DATETIME_FORMAT) - tzoffset
        start_mo_date = start_mo_date.strftime("%Y-%m-%d %H:%M:%S")
        end_mo_date = end_mo_date.strftime("%Y-%m-%d %H:%M:%S")
        if self.date_order >= start_date and self.date_order <= end_date and 'today_sales' in self._context or 'management_today' in self._context:
            domain = [('date_order', '>=', start_date), ('date_order', '<=', end_date)]
            if 'today_sales' in self._context:
                pivot_id = ir_model_data.get_object_reference('pinnacle_sales', 'view_order_product_pivot_today_sales')[1]
        if self.date_order >= start_mo_date and self.date_order <= end_mo_date and 'month_sales' in self._context or 'management_month' in self._context:
            domain = [('date_order', '>=', start_mo_date), ('date_order', '<=', end_mo_date)]
            if 'month_sales' in self._context:
                pivot_id = ir_model_data.get_object_reference('pinnacle_sales', 'view_order_product_pivot_month_sales')[1]
        return {
            'name': _('Sales Analysis'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'pivot,graph',
            'res_model': 'sale.order',
            'views': [(pivot_id, 'pivot')],
            'view_id': pivot_id,
            'context': dict(self.env.context or {}),
            'domain': domain,
        }

    @api.depends('partner_id')
    def _get_customer_details(self):
        """Computes contact_email, contact_phone and customer_company values in SO using partner_id"""
        for rec in self:
            if rec.partner_id: 
                rec.contact_email = rec.partner_id.email or False
                rec.contact_phone = rec.partner_id.phone or False
                if rec.partner_id.parent_id:
                    rec.customer_company = rec.partner_id.parent_id.name
                else:
                    rec.customer_company = 'Personal'
                
    @api.one
    @api.depends('approval_history')
    def _compute_is_any_approval_history(self):
        self.is_any_approval_history = False
        if self.approval_history:
            self.is_any_approval_history = True

    @api.one
    @api.depends('order_line.discount_amount', 'applied_promo_codes')
    def _compute_discount_total(self):
        self.discount_total = 0.00
        for order_line in self.order_line:
            self.discount_total += order_line.discount_amount

    @api.multi
    def apply_promo_code(self):
        for sale_order in self:
            if sale_order.promo_code:
                promocode = self.env['product.pricelist'].search([('lower_case_code', '=', sale_order.promo_code.lower())], limit=1)
                if not promocode:
                    raise UserError(_('Please Enter Valid Promo Code.'))
                elif sale_order.applied_promo_codes and promocode.lower_case_code in [applied_promo_code.lower() for applied_promo_code in sale_order.applied_promo_codes.split(',')]:
                    raise UserError(_('You have already applied this Promo Code.'))
                else:
                    sale_order.old_pricelist_id = sale_order.pricelist_id.id
                    self.with_context(not_call_onchange_of_pricelist_id=True).write({'pricelist_id': promocode.id})
                    if promocode.notes:
                        self.promo_notes = promocode.notes
                    for sale_order_line in sale_order.order_line:
                        sale_order_line.product_id.sale_order_price_without_tax = sale_order.amount_untaxed
                        sale_order_line.product_id.sale_order_price_with_tax = sale_order.amount_total
                        sale_order_line.product_id.old_currency_id = self.old_pricelist_id.currency_id.id
                        # Copying Price Unit and Discount when applying first Promo Code
                        if not sale_order.applied_promo_codes:
                            sale_order_line.before_promocode_price_unit = sale_order_line.price_unit
                            sale_order_line.before_promocode_discount = sale_order_line.discount
                            sale_order_line.before_promocode_discount_amount = sale_order_line.discount_amount

                    applied_promo_codes = sale_order.applied_promo_codes
                    if applied_promo_codes:
                        applied_promo_codes += ',' + str(sale_order.promo_code)
                    else:
                        applied_promo_codes = sale_order.promo_code
                    sale_order.with_context(promo_code=True).applying_sale_pricelist_by_combining_quantity()
                    self.with_context(not_call_onchange_of_pricelist_id=True).write({'pricelist_id': sale_order.old_pricelist_id.id, 'is_promo_code_applied': 'Yes', 'applied_promo_codes': applied_promo_codes , 'promo_code': False})
            else:
                raise UserError(_('Please Enter Promo Code.'))

    @api.multi
    def remove_promo_code(self):
        for sale_order in self:
            if sale_order.promo_code:
                promocode = self.env['product.pricelist'].search([('lower_case_code', '=', sale_order.promo_code.lower())], limit=1)
                if not promocode:
                    raise UserError(_('Please Enter Valid Promo Code.'))
                elif sale_order.applied_promo_codes and promocode.lower_case_code not in [applied_promo_code.lower() for applied_promo_code in sale_order.applied_promo_codes.split(',')]:
                    raise UserError(_('Entered Promo Code doesn\'t exist in applied Promo Code list.'))
                else:
                    for sale_order_line in sale_order.order_line:
                        sale_order_line.price_unit = sale_order_line.before_promocode_price_unit
                        sale_order_line.discount = sale_order_line.before_promocode_discount
                        sale_order_line.discount_amount = sale_order_line.before_promocode_discount_amount

                    applied_promo_codes = sale_order.applied_promo_codes
                    self.with_context(not_call_onchange_of_pricelist_id=True).write({'is_promo_code_applied': 'No', 'applied_promo_codes': False , 'promo_code': False})
                    for applied_promo_code in applied_promo_codes.split(','):
                        if applied_promo_code.lower() != promocode.lower_case_code:
                            sale_order.promo_code = applied_promo_code
                            sale_order.apply_promo_code()
            else:
                raise UserError(_('Please Enter Promo Code.')) 

    @api.depends('state', 'order_line.invoice_status')
    def _get_invoiced(self):
        """
        Compute the invoice status of a SO. Possible statuses:
        - no: if the SO is not in status 'sale' or 'done', we consider that there is nothing to
          invoice. This is also hte default value if the conditions of no other status is met.
        - to invoice: if any SO line is 'to invoice', the whole SO is 'to invoice'
        - invoiced: if all SO lines are invoiced, the SO is invoiced.
        - upselling: if all SO lines are invoiced or upselling, the status is upselling.

        The invoice_ids are obtained thanks to the invoice lines of the SO lines, and we also search
        for possible refunds created directly from existing invoices. This is necessary since such a
        refund is not directly linked to the SO.
        """
        for order in self:
            invoice_ids = order.order_line.mapped('invoice_lines').mapped('invoice_id').filtered(lambda r: r.type in ['out_invoice', 'out_refund'])
            # Search for invoices which have been 'cancelled' (filter_refund = 'modify' in
            # 'account.invoice.refund')
            # use like as origin may contains multiple references (e.g. 'SO01, SO02')
            refunds = invoice_ids.search([('origin', 'like', order.name)])
            invoice_ids |= refunds.filtered(lambda r: order.name in [origin.strip() for origin in r.origin.split(',')])
            # Search for refunds as well
            refund_ids = self.env['account.invoice'].browse()
            if invoice_ids:
                for inv in invoice_ids:
                    refund_ids += refund_ids.search([('type', '=', 'out_refund'), ('origin', '=', inv.number), ('origin', '!=', False), ('journal_id', '=', inv.journal_id.id)])

            line_invoice_status = [line.invoice_status for line in order.order_line]

            if order.state not in ('draft', 'art_processing', 'pending_review', 'to_approve', 'approve', 'reject', 'sent', 'confirmation_received',  'accounting_approval', 'accounting_approved', 'accounting_rejected', 'ready_for_fullfillment', 'cancel', 'hold', 'inactive', 'sale', 'partially_invoiced', 'shipped', 'invoiced', 'delivered', 'close', 'reopen', 'done'):
                invoice_status = 'no'
            elif any(invoice_status == 'to invoice' for invoice_status in line_invoice_status):
                invoice_status = 'to invoice'
            elif all(invoice_status == 'invoiced' for invoice_status in line_invoice_status):
                invoice_status = 'invoiced'
            elif all(invoice_status in ['invoiced', 'upselling'] for invoice_status in line_invoice_status):
                invoice_status = 'upselling'
            else:
                invoice_status = 'no'

            order.update({
                'invoice_count': len(set(invoice_ids.ids + refund_ids.ids)),
                'invoice_ids': invoice_ids.ids + refund_ids.ids,
                'invoice_status': invoice_status
            })

    @api.depends('order_line.margin', 'currency_id', 'program_type')
    def _product_margin(self):
        for order in self:
            order.margin = sum(order.order_line.filtered(lambda r: r.state != 'cancel').filtered(lambda r: r.product_id.exclude_from_computing_sale_margins == False).filtered(lambda r: r.product_id.inventory_control_code not in ['inv_co', 'inv_cs'] or r.order_id.program_type != 'store').mapped('margin'))
            order.margin_duplicate = sum(order.order_line.filtered(lambda r: r.state != 'cancel').filtered(lambda r: r.product_id.exclude_from_computing_sale_margins == False).filtered(lambda r: r.product_id.inventory_control_code not in ['inv_co', 'inv_cs'] or r.order_id.program_type != 'store').mapped('margin'))

    @api.one
    @api.depends('order_line','order_line.is_decoration_disable') 
    def _compute_art_processing_needed(self):
        self.art_processing_needed = False
        for order_line in self.order_line:
            if order_line.product_id.can_decorate and order_line.product_type != 'service' and not order_line.is_decoration_disable:
                self.art_processing_needed = True
                break

    @api.multi
    @api.onchange('partner_id')
    def onchange_partner_id(self):
        super(SaleOrder, self).onchange_partner_id()
        addr = self.partner_id.address_get(['invoice'])
        if 'invoice' != self.env['res.partner'].browse(addr['invoice']).type:
            self.update({
                'partner_invoice_id':  self.partner_id.id,
            })
        if self.partner_id:
            self.send_email_of_sale_invoice = self.partner_id.send_email_of_sale_invoice
            self.user_id = self.partner_id.user_id
            program_id = False
            channel_id = False
            if self.partner_id.parent_id:
                program_id = self.partner_id.program_contact_id.id
            else:
                program_id = self.partner_id.program_id.id
            if self.partner_id.parent_id:
                channel_id = self.partner_id.channel_contact_id.id
            else:
                channel_id = self.partner_id.channel_id.id
            self.program_id = program_id
            self.channel_id = channel_id
            customer_type = False
            if self.order_type == 'fo_non_web' and self.partner_id.customer_type == 'new':
                customer_type = 'new'
            if self.order_type == 'fo_non_web' and self.partner_id.customer_type == 'program':
                customer_type = 'program'
            if self.order_type == 'repeat_non_web' and self.partner_id.customer_type == 'current':
                customer_type = 'current'
            if self.order_type == 'repeat_non_web' and self.partner_id.customer_type == 'program':
                customer_type = 'program'
            if self.order_type == 'fo_web_order' and self.partner_id.customer_type == 'new':
                customer_type = 'web_new'
            if self.order_type == 'fo_web_order' and self.partner_id.customer_type == 'program':
                customer_type = 'program'
            if self.order_type == 'repeat_web_order' and self.partner_id.customer_type == 'current':
                customer_type = 'web_current_customer'
            if self.order_type == 'repeat_web_order' and self.partner_id.customer_type == 'program':
                customer_type = 'program'
            if self.order_type == 'spec' and self.partner_id.customer_type == 'new':
                customer_type = 'new'
            if self.order_type == 'spec' and self.partner_id.customer_type == 'current':
                customer_type = 'current'
            if self.order_type == 'spec' and self.partner_id.customer_type == 'program':
                customer_type = 'program'
            art_ids = []
            for line in self.art_line:
                if line.art_id and line.art_id.id not in art_ids:
                    art_ids.append(line.art_id.id)
            if art_ids:
                art_browse_ids = self.env['art.production'].browse(art_ids)
                art_browse_ids.write({'customer_type': customer_type})
            if channel_id:
                if any(tag.is_netknacks for tag in self.partner_id.analytic_tag_ids):
                    channel = self.env['partner.channel'].search([('name', '=', 'NetKnacks')])
                    if channel:
                        self.channel_id = channel.id
            self.analytic_tag_ids = [(6, 0, self.partner_id.analytic_tag_ids.ids)]   

    @api.multi
    def get_ship_alabama_details_from_sale_order(self):
        inv_obj = self.env['account.invoice']
        set_value_of_ship_to_locations = []
        check_ship_to_location = False
        ship_to_location = False
        ship_to_multiple_locations = False
        is_netKnacks = []
        check_ship_to_alabama = []
        is_netknact = False
        is_ship_to_alabama = False
        ship_to_address = {}
        for sale_order in self:
            for shipping_line in sale_order.shipping_line:
                if shipping_line.contact:
                    set_value_of_ship_to_locations.append(shipping_line.contact.id)
                    if not ship_to_address.keys():
                        ship_to_address['ship_to_address_name'] = shipping_line.address_name
                        ship_to_address['ship_to_street1'] = shipping_line.street1
                        ship_to_address['ship_to_street2'] = shipping_line.street2
                        ship_to_address['ship_to_city'] = shipping_line.city
                        ship_to_address['ship_to_state'] = shipping_line.state_id and shipping_line.state_id.id or False
                        ship_to_address['ship_to_zipcode'] = shipping_line.zipcode
                        ship_to_address['ship_to_country'] = shipping_line.contact_country_id and shipping_line.contact_country_id.id or False
                if shipping_line.ship_to_location == 'yes' and not check_ship_to_location:
                    check_ship_to_location = True
            is_netKnacks.append(sale_order.is_netKnacks())
            check_ship_to_alabama.append(sale_order.check_ship_to_alabama())
        if check_ship_to_location:
            ship_to_multiple_locations = True
            ship_to_location = False
        else:
            if set_value_of_ship_to_locations:
                if inv_obj.is_list_has_all_values_similar(set_value_of_ship_to_locations):
                    ship_to_multiple_locations = False
                    ship_to_location = set_value_of_ship_to_locations[0]
                else:
                    ship_to_multiple_locations = True
                    ship_to_location = False
            else:
                ship_to_multiple_locations = False
                ship_to_location = False
        if is_netKnacks:
            if inv_obj.is_list_has_all_values_similar(is_netKnacks):
                is_netknact = is_netKnacks[0]
            else:
                is_netknact = False
        else:
            is_netknact = False
        if check_ship_to_alabama:
            if inv_obj.is_list_has_all_values_similar(check_ship_to_alabama):
                is_ship_to_alabama = check_ship_to_alabama[0]
            else:
                is_ship_to_alabama = False
        else:
            is_ship_to_alabama = False
        return ship_to_multiple_locations, ship_to_location, ship_to_address, is_netknact, is_ship_to_alabama

    @api.multi
    def action_invoice_create(self, grouped=False, final=False):
        res = []
        if self._context.get('consolidated_invoice', False):
            res = super(SaleOrder, self).action_invoice_create(grouped, final)
        else:
            res = super(SaleOrder, self).action_invoice_create(True, final)
        inv_obj = self.env['account.invoice']
        for sale_order in self:
            ship_to_multiple_locations, ship_to_location, ship_to_address, is_netknact, is_ship_to_alabama = False, False, {}, False, False
            invoices = inv_obj.search([('origin','=',sale_order.name)])
            # One Invoice is made from multiple sale orders
            if not invoices:
                invoices = inv_obj.search([('origin','like',sale_order.name)])
                ship_to_multiple_locations, ship_to_location, ship_to_address, is_netknact, is_ship_to_alabama = self.get_ship_alabama_details_from_sale_order()
            else:
                ship_to_multiple_locations, ship_to_location, ship_to_address, is_netknact, is_ship_to_alabama = sale_order.get_ship_alabama_details_from_sale_order()
            if invoices:
                invoices.write({'send_email_of_sale_invoice': sale_order.send_email_of_sale_invoice, 
                    'customer_po_id': sale_order.customer_po_id,
                    'customer_po_file': sale_order.customer_po_file,
                    'customer_po_file_name': sale_order.customer_po_file_name,
                    'ship_to_multiple_locations': ship_to_multiple_locations,
                    'ship_to_location': ship_to_location,
                    'is_netknact': is_netknact,
                    'is_ship_to_alabama': is_ship_to_alabama,})
                if ship_to_location and ship_to_address.keys():
                    invoices.write({'ship_to_address_name': ship_to_address['ship_to_address_name'], 
                            'ship_to_street1': ship_to_address['ship_to_street1'],
                            'ship_to_street2': ship_to_address['ship_to_street2'],
                            'ship_to_city': ship_to_address['ship_to_city'],
                            'ship_to_state': ship_to_address['ship_to_state'],
                            'ship_to_zipcode': ship_to_address['ship_to_zipcode'],
                            'ship_to_country': ship_to_address['ship_to_country'],})
                if 'create_send_invoice' in self.env.context:
                    invoices.write({'send_email_of_sale_invoice': True})
            if 'come_form_partial_invoice_button' not in self.env.context:
                for invoice in invoices:
                    if invoice.send_email_of_sale_invoice or 'create_send_invoice' in self.env.context:
                        invoice.action_invoice_open()
                        domain = [('account_id', '=', invoice.account_id.id), ('partner_id', '=', invoice.env['res.partner']._find_accounting_partner(invoice.partner_id).id), ('reconciled', '=', False), ('amount_residual', '!=', 0.0)]
                        if invoice.type in ('out_invoice', 'in_refund'):
                            domain.extend([('credit', '>', 0), ('debit', '=', 0)])
                            type_payment = _('Outstanding credits')
                        lines = self.env['account.move.line'].search(domain)
                        move_lines = [payment.move_line_ids for payment in sale_order.payment_ids]
                        move_line_ids = []
                        for move_line in move_lines:
                            for mline in move_line:
                                move_line_ids.append(mline.id)
                        for line in lines:
                            if line.id in move_line_ids:
                                invoice.assign_outstanding_credit(line.id)
                        invoice.send_so_invoice()
        if 'create_send_invoice' in self.env.context:
            mail_template = self.env.ref('pinnacle_account.email_template_edi_invoice_pinnacle')
            for r in res:
                mail_id = mail_template.send_mail(r)
                self.env['mail.mail'].browse([mail_id]).send()
        return res

    @api.multi
    def action_cancel(self):
        self.ensure_one()
        if self.state in ['draft', 'art_processing', 'pending_review', 'to_approve' , 'approve', 'reject' , 'sent', 'confirmation_received', 'cancel', 'accounting_rejected'] or (self.env.context.get('super_admin_delete_sale_order', False)):
            self.write({'skipped_confirmation_step': False, 'skipped_pending_review': False})
            super(SaleOrder, self).action_cancel()
            return True
        else:
            check_cancel = True
            for po in self.purchase_orders:
                if po.state != 'cancel':
                    check_cancel = False
            if check_cancel:
                self.write({'skipped_confirmation_step': False, 'skipped_pending_review': False})
                super(SaleOrder, self).action_cancel()
                return True   
            else:
                raise UserError(_('A Sales Order cannot be cancelled once Purchase Orders are created, until all Purchase Orders are cancelled.'))

    @api.multi
    def action_hold(self):
        for order in self:
            order.last_state = order.state
            order.state = 'hold'
        return True
            
    @api.multi
    def action_mark_inactive(self):
        self.ensure_one()
        if self.state in ['draft', 'art_processing', 'pending_review', 'to_approve' , 'approve', 'reject' , 'sent', 'confirmation_received', 'inactive', 'accounting_rejected']:
            self.write({'state': 'inactive', 'skipped_confirmation_step': False, 'skipped_pending_review': False})
            return True
        else:
            raise UserError(_('A Sales Order cannot be marked inactive once Purchase Orders are created.'))

    @api.multi
    def action_draft(self):
        orders = self.filtered(lambda s: s.state in ['cancel', 'sent', 'inactive'])
        orders.write({
            'state': 'draft',
            'procurement_group_id': False,
            'skipped_confirmation_step': False,
        })
        orders.mapped('order_line').mapped('procurement_ids').write({'sale_line_id': False})

    @api.multi
    def set_to_quote_draft(self):
        art_ids = []
        for rec in self:
            for line in rec.order_line:
                if line.art_id and line.art_id.id not in art_ids:
                    art_ids.append(line.art_id.id)
        if art_ids:
            art_browse_ids = self.env['art.production'].browse(art_ids)
            art_browse_ids.write({'status': 'virtual_spec'})
        self.write({'state': 'quote_draft', 'skip_send': True})


    @api.multi
    def set_to_draft_sale_order(self):
        self.ensure_one()
        if self.state == 'inactive':
            self.action_mark_inactive()
            self.action_draft()
        else:
            self.action_cancel()
            self.action_draft()

    @api.multi
    def set_to_invoiced(self): 
        self.ensure_one()
        if self.invoice_status != 'invoiced':
            raise UserError(_('You can not move the unfully invoiced sale order to invoiced state.'))
        else:
            for invoice_id in self.invoice_ids:
                if invoice_id.state not in ['open','paid']:
                    raise UserError(_('Please validate all invoices to move the sale order to invoiced state.'))    
        self.state = 'invoiced'

    @api.multi
    def set_to_confirmation_received(self):
        self.write({'state': 'confirmation_received', 'confirmation_received': fields.datetime.now()})
        # 15448 : SO - No email generation when Confirmation Received button clicked
        # self.ensure_one()
        # ir_model_data = self.env['ir.model.data']
        # try:
        #     template_id = ir_model_data.get_object_reference('pinnacle_sales', 'mail_customer_approval_shipdates')[1]
        # except ValueError:
        #     template_id = False
        # try:
        #     compose_form_id = ir_model_data.get_object_reference('mail', 'email_compose_message_wizard_form')[1]
        # except ValueError:
        #     compose_form_id = False
        # ctx = dict(self.env.context or {})
        # ctx.update({
        #     'default_model': 'sale.order',
        #     'default_res_id': self.ids[0],
        #     'default_use_template': bool(template_id),
        #     'default_template_id': template_id,
        #     'default_composition_mode': 'comment',
        #     'default_partner_id': self.partner_id.id,
        #     'mark_confirmation_received': True,
        # })
        # return {
        #     'name': _('Compose Email'),
        #     'type': 'ir.actions.act_window',
        #     'view_type': 'form',
        #     'view_mode': 'form',
        #     'res_model': 'mail.compose.message',
        #     'views': [(compose_form_id, 'form')],
        #     'view_id': compose_form_id,
        #     'target': 'new',
        #     'context': ctx,
        # }

    @api.multi
    def email_customer(self):
        """Retuns: Wizard for mail dialog with email template values and other parameters."""
        self.ensure_one()
        ir_model_data = self.env['ir.model.data']
        try:
            template_id = ir_model_data.get_object_reference('pinnacle_sales', 'mail_customer_blank_email_to_customer')[1]
        except ValueError:
            template_id = False
        try:
            compose_form_id = ir_model_data.get_object_reference('mail', 'email_compose_message_wizard_form')[1]
        except ValueError:
            compose_form_id = False
        email_from = self.env.user.partner_id.email
        ctx = dict(self.env.context or {})
        ctx.update({
            'default_model': 'sale.order',
            'default_res_id': self.ids[0],
            'default_use_template': bool(template_id),
            'default_template_id': template_id,
            'default_composition_mode': 'comment',
            'default_partner_id': self.partner_id.id,
            'default_email_from': email_from,
            'mark_confirmation_received': True,
        })
        return {
            'name': _('Compose Email'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }

    @api.multi
    def email_confirmation_received(self):
        """Retuns: Wizard for mail dialog with email template values and other parameters."""
        self.ensure_one()
        ir_model_data = self.env['ir.model.data']
        try:
            template_id = ir_model_data.get_object_reference('pinnacle_sales', 'mail_customer_approval_shipdates')[1]
        except ValueError:
            template_id = False
        try:
            compose_form_id = ir_model_data.get_object_reference('mail', 'email_compose_message_wizard_form')[1]
        except ValueError:
            compose_form_id = False
        ctx = dict(self.env.context or {})
        ctx.update({
            'default_model': 'sale.order',
            'default_res_id': self.ids[0],
            'default_use_template': bool(template_id),
            'default_template_id': template_id,
            'default_composition_mode': 'comment',
            'default_partner_id': self.partner_id.id,
            'mark_confirmation_received': True,
        })
        return {
            'name': _('Compose Email'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }

    @api.multi
    def action_quotation_send(self):
        '''
        This function opens a window to compose an email, with the edi sale template message loaded by default
        '''
        if not self.user_id:
            raise UserError("Please select Account Manager for this order.")
        self.ensure_one()
        ir_model_data = self.env['ir.model.data']
        try:
            template_id = ir_model_data.get_object_reference('pinnacle_sales', 'email_template_edi_sale_extended')[1]
        except ValueError:
            template_id = False
        try:
            compose_form_id = ir_model_data.get_object_reference('mail', 'email_compose_message_wizard_form')[1]
        except ValueError:
            compose_form_id = False
        ctx = dict()
        ctx.update({
            'default_model': 'sale.order',
            'default_res_id': self.ids[0],
            'default_use_template': bool(template_id),
            'default_template_id': template_id,
            'default_composition_mode': 'comment',
            'mark_so_as_sent': True,
            'custom_layout': "pinnacle_sales.mail_template_data_notification_email_sale_order"
        })
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }

    @api.multi
    def action_send_survey(self):
        '''
        This function opens a window to compose an email, with the edi sale template message loaded by default
        '''
        self.ensure_one()
        ir_model_data = self.env['ir.model.data']
        try:
            template_id = ir_model_data.get_object_reference('pinnacle_sales', 'mail_template_trust_pilot')[1]
        except ValueError:
            template_id = False
        try:
            compose_form_id = ir_model_data.get_object_reference('mail', 'email_compose_message_wizard_form')[1]
        except ValueError:
            compose_form_id = False
        ctx = dict()
        ctx.update({
            'default_model': 'sale.order',
            'default_res_id': self.ids[0],
            'default_use_template': bool(template_id),
            'default_template_id': template_id,
            'default_composition_mode': 'comment',
        })
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }

    @api.multi
    def skip_confirmation_process(self):
        self.write({'skipped_confirmation_step': True , 'skipped_pending_review': False, 'confirmation_received': fields.datetime.now()})

    @api.multi
    def quote_sent(self):
        '''
        This function opens a window to compose an email, with the edi sale template message loaded by default
        '''
        self.ensure_one()
        ir_model_data = self.env['ir.model.data']
        try:
            template_id = ir_model_data.get_object_reference('pinnacle_sales', 'email_template_edi_sale_quote')[1]
        except ValueError:
            template_id = False
        try:
            compose_form_id = ir_model_data.get_object_reference('mail', 'email_compose_message_wizard_form')[1]
        except ValueError:
            compose_form_id = False
        ctx = dict()
        ctx.update({
            'default_model': 'sale.order',
            'default_res_id': self.ids[0],
            'default_use_template': bool(template_id),
            'default_template_id': template_id,
            'default_composition_mode': 'comment',
            'mark_so_as_quote_sent': True,
            'custom_layout': "pinnacle_sales.mail_template_data_notification_email_sale_order"
        })
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }

    @api.multi
    def approve_quote(self):
        self.ensure_one()
        res = self.env['crm.lead.tag'].search([('name', '=', 'Quote Closed - Won')],limit=1)
        if not res:
            res = self.env['crm.lead.tag'].create({'name': 'Quote Closed - Won'})
        self.write({'state': 'draft', 'quote': False, 'tag_ids': [(6, 0, [res.id])]})
        url = str(self.env['ir.config_parameter'].search([('key', '=', 'web.base.url')]).value) + '/web?#id=' + str(self.id) + '&view_type=form&model=sale.order&action=' + str(self.env.ref('sale.action_quotations').id) + '&menu_id='
        if url:
            return {
                  'res_model': 'ir.actions.act_url',
                  'type'    : 'ir.actions.act_url',
                  'target'  : 'self',
                  'url'     : url,
               }

    @api.multi
    def reject_quote(self):
        # art_ids = []
        # for rec in self:
        #     for line in rec.order_line:
        #         if line.art_id and line.art_id.id not in art_ids:
        #             art_ids.append(line.art_id.id)
        # if art_ids:
            # art_browse_ids = self.env['art.production'].browse(art_ids)
            # art_browse_ids.write({'status': 'virtual_spec'})
        self.write({'state': 'quote_rejected'})

    @api.multi
    def skip_virtual(self):
        # art_ids = []
        # for rec in self:
        #     for line in rec.order_line:
        #         if line.art_id and line.art_id.id not in art_ids:
        #             art_ids.append(line.art_id.id)
        # if art_ids:
        #     art_browse_ids = self.env['art.production'].browse(art_ids)
        #     art_browse_ids.write({'status': 'virtual_spec'})
        self.write({'state': 'quote_review'})

    @api.multi
    def quote_send_approval(self):
        # art_ids = []
        # for rec in self:
        #     for line in rec.order_line:
        #         if line.art_id and line.art_id.id not in art_ids:
        #             art_ids.append(line.art_id.id)
        # if art_ids:
        #     art_browse_ids = self.env['art.production'].browse(art_ids)
        #     art_browse_ids.write({'status': 'virtual_spec'})
        self.write({'state': 'quote_man_approval', 'skip_send': True})

    @api.multi
    def quote_manager_approved(self):
        # art_ids = []
        # for rec in self:
        #     for line in rec.order_line:
        #         if line.art_id and line.art_id.id not in art_ids:
        #             art_ids.append(line.art_id.id)
        # if art_ids:
        #     art_browse_ids = self.env['art.production'].browse(art_ids)
        #     art_browse_ids.write({'status': 'virtual_spec'})
        self.write({'state': 'quote_man_approved', 'skip_send': False})                

    @api.multi
    def quote_manager_rejected(self):
        # art_ids = []
        # for rec in self:
        #     for line in rec.order_line:
        #         if line.art_id and line.art_id.id not in art_ids:
        #             art_ids.append(line.art_id.id)
        # if art_ids:
        #     art_browse_ids = self.env['art.production'].browse(art_ids)
        #     art_browse_ids.write({'status': 'virtual_spec'})
        self.write({'state': 'quote_man_rejected'})

    @api.multi
    def quote_skip_send(self):
        self.write({'skip_send': True})

    @api.multi
    def quote_skip_send2(self):                
        # art_ids = []
        # for rec in self:
        #     for line in rec.order_line:
        #         if line.art_id and line.art_id.id not in art_ids:
        #             art_ids.append(line.art_id.id)
        # if art_ids:
        #     art_browse_ids = self.env['art.production'].browse(art_ids)
        #     art_browse_ids.write({'status': 'virtual_spec'})
        self.write({'state': 'quote_review', 'skip_send_review': False})

    @api.multi
    def quote_set_closed(self):
        # art_ids = []
        # for rec in self:
        #     for line in rec.order_line:
        #         if line.art_id and line.art_id.id not in art_ids:
        #             art_ids.append(line.art_id.id)
        # if art_ids:
        #     art_browse_ids = self.env['art.production'].browse(art_ids)
        #     art_browse_ids.write({'status': 'virtual_spec'})
        res = self.env['crm.lead.tag'].search([('name', '=', 'Quote Closed - Lost')], limit=1)
        if not res:
            res = self.env['crm.lead.tag'].create({'name': 'Quote Closed - Lost'})
        self.write({'state': 'closed_quote', 'tag_ids': [(6, 0, [res.id])]})

    @api.multi
    def set_to_close(self):
        # Validating All Invoices When Closing Order
        for sale_order in self:
            for invoice_id in sale_order.invoice_ids:
                if invoice_id.state == 'draft':
                    invoice_id.action_invoice_open()
            if not sale_order.close_date:
                sale_order.close_date = fields.Datetime.now()
            purchae_orders = self.env['purchase.order'].search([('sale_order_id','=', sale_order.id)])
            for purchae_order in purchae_orders:
                if purchae_order.state not in ['close', 'reopen', 'done', 'cancel']:
                    raise UserError(_("You can't close a sale order (#" + sale_order.name + ") whose all purchase orders are not closed."))
        self.write({'state': 'close'})
        # 16515 [DEV] : AWS - Undo Logic of Auto Archiving
        # Archiving Art Work Sheets In Draft State When Closing Order
        # art_wroksheets_to_be_archived = []
        # decorations_ids_to_be_updated_for_artworksheet = []
        # sale_order_line_obj = self.env['sale.order.line']
        # decoration_obj = self.env['sale.order.line.decoration']
        # for sale_order in self:
        #     for art_worksheet in sale_order.artworksheets:
        #         if art_worksheet.id not in art_wroksheets_to_be_archived and not art_worksheet.entry_date:
        #             art_wroksheets_to_be_archived.append(art_worksheet.id)
        #             for art_line in art_worksheet.art_lines:
        #                 decorations_ids_to_be_updated_for_artworksheet += art_line.decorations.ids
        # if art_wroksheets_to_be_archived:
        #     sale_order_line_obj.copying_data_of_art_wroksheets_to_be_archived(art_wroksheets_to_be_archived)
        #     sale_order_line_obj.actual_archiving_of_artworksheets_with_handling_of_other_data(art_wroksheets_to_be_archived)
        # if decorations_ids_to_be_updated_for_artworksheet:
        #     for decorations_id_to_be_updated_for_artworksheet in decorations_ids_to_be_updated_for_artworksheet:
        #         decoration = decoration_obj.browse(decorations_id_to_be_updated_for_artworksheet).exists()
        #         if decoration:
        #             decoration.write({'art_id': False ,'art_line_id': False})


    @api.multi
    def action_done(self):
        for order in self:
            order.reclose_date = fields.Datetime.now()
            order.state = 'done'
            purchae_orders = self.env['purchase.order'].search([('sale_order_id','=', order.id)])
            for purchae_order in purchae_orders:
                if not purchae_order.reclose_date:
                    purchae_order.reclose_date = fields.Datetime.now()
                purchae_order.state = 'done'

    @api.multi
    def set_to_reopen(self):
        for order in self:
            order.reopen_date = fields.Datetime.now()
            if not order.close_date:
                order.close_date = fields.Datetime.now()
            order.state = 'reopen'
            purchae_orders = self.env['purchase.order'].search([('sale_order_id','=', order.id)])
            for purchae_order in purchae_orders:
                if not purchae_order.close_date:
                    purchae_order.close_date = fields.Datetime.now()
                if not purchae_order.reopen_date:
                    purchae_order.reopen_date = fields.Datetime.now()
                purchae_order.state = 'reopen'

    @api.one
    def remove_hold(self):
        self.state = self.last_state

    @api.multi
    def print_quotation(self):
        self.ensure_one()
        return self.env['report'].get_action(self, 'sale.report_saleorder')

    @api.depends('order_line.price_subtotal', 'margin', 'currency_id', 'program_type')
    def _compute_margin_in_percentage(self):
        for order in self:
            order.margin_in_percentage = 0.00
            order.margin_in_percentage_duplicate = 0.00
            amount_untaxed = 0.00
            for order_line in order.order_line:
                if order_line.product_id and not order_line.product_id.exclude_from_computing_sale_margins and (order_line.product_id.inventory_control_code not in ['inv_co', 'inv_cs'] or order_line.order_id.program_type != 'store'):
                    amount_untaxed += order_line.price_subtotal
            if amount_untaxed:
                order.margin_in_percentage = order.margin * 100 / amount_untaxed
                order.margin_in_percentage_duplicate = order.margin * 100 / amount_untaxed

    @api.depends('order_line.product_id', 'order_line.purchase_price', 'order_line.product_uom_qty', 'order_line.price_subtotal', 'currency_id', 'program_type')
    def _compute_product_margin_in_percentage(self):
        for order in self:
            total_saleprice = 0.00
            total_cost = 0.00
            delivery_carrier_products = []
            for delivery_carrier in self.env['delivery.carrier'].search([]):
                if delivery_carrier.product_id:
                    delivery_carrier_products.append(delivery_carrier.product_id.id)
            delivery_carrier_products = list(set(delivery_carrier_products))
            for order_line in order.order_line:
                if order_line.product_id.id not in delivery_carrier_products and not order_line.product_id.exclude_from_computing_sale_margins and (order_line.product_id.inventory_control_code not in ['inv_co', 'inv_cs'] or order_line.order_id.program_type != 'store'):
                    total_saleprice += order_line.price_subtotal
                    total_cost += (order_line.purchase_price or order_line.product_id.standard_price) * order_line.product_uom_qty
            order.product_margin_in_percentage = 0.00
            order.product_margin_in_percentage_duplicate = 0.00
            order.product_margin_in_dollor_duplicate = 0.00
            order.product_margin_in_dollor = 0.00
            if total_saleprice:
                total = (total_saleprice - total_cost)
                order.product_margin_in_dollor_duplicate = total
                order.product_margin_in_dollor = total
                order.product_margin_in_percentage = total / total_saleprice * 100
                order.product_margin_in_percentage_duplicate = total / total_saleprice * 100

    @api.depends('order_line.product_id', 'order_line.purchase_price', 'order_line.product_uom_qty', 'order_line.price_subtotal', 'currency_id', 'program_type')
    def _compute_shipping_margin_in_percentage(self):
        for order in self:
            total_saleprice = 0.00
            total_cost = 0.00
            delivery_carrier_products = []
            for delivery_carrier in self.env['delivery.carrier'].search([]):
                if delivery_carrier.product_id:
                    delivery_carrier_products.append(delivery_carrier.product_id.id)
            delivery_carrier_products = list(set(delivery_carrier_products))
            for order_line in order.order_line:
                if order_line.product_id.id in delivery_carrier_products and not order_line.product_id.exclude_from_computing_sale_margins and (order_line.product_id.inventory_control_code not in ['inv_co', 'inv_cs'] or order_line.order_id.program_type != 'store'):
                    total_saleprice += order_line.price_subtotal
                    total_cost += (order_line.purchase_price or order_line.product_id.standard_price) * order_line.product_uom_qty
            order.shipping_margin_in_percentage = 0.00
            order.shipping_margin_in_percentage_duplicate = 0.00
            order.shipping_margin_in_dollor_duplicate = 0.00
            order.shipping_margin_in_dollor = 0.00
            if total_saleprice:
                total = (total_saleprice - total_cost)
                order.shipping_margin_in_dollor_duplicate = total
                order.shipping_margin_in_dollor = total
                order.shipping_margin_in_percentage = total / total_saleprice * 100
                order.shipping_margin_in_percentage_duplicate = total / total_saleprice * 100

    #15443 [DEV] : Sales order - Different method to toggle between type
    # @api.multi
    # def toggle_is_sample(self):
    #     for this in self:
    #         this.is_spec = False
    #         if len(this.decorations.ids) and this.is_regular == True:
    #             raise UserError(_("A sample order cannot have decoration."))
    #         else:
    #             this.order_line.unlink()
    #         this.is_regular = not this.is_regular
    #         if this.is_regular:
    #             this.order_type = 'fo_non_web'
    #         if not this.is_regular:
    #             this.order_type = 'samples'
    #     return True

    # @api.multi
    # def toggle_is_spec(self):
    #     '''
    #     Calculate tier pricing from here becuase LTM charges depends on
    #     order type 'spec'.
    #     '''
    #     for this in self:
    #         this.is_spec = not this.is_spec
    #         if this.is_spec:
    #             this.order_type = 'spec'
    #         else:
    #             this.order_type = False
    #     self.update_tier_price()

    @api.one
    @api.depends('approval_level')
    def _compute_approval_user_email(self):
        self.approval_users_email = False
        if self.approval_level:
            for user in self.approval_level.users:
                if not self.approval_users_email:
                    if user.partner_id.email:
                        self.approval_users_email = user.partner_id.email
                        continue
                if self.approval_users_email and user.partner_id.email:
                    self.approval_users_email = self.approval_users_email + ',' + user.partner_id.email

    @api.one
    @api.depends('customer_po_id', 'customer_po_file')
    def _compute_accounting_approval_user_email(self):
        self.accounting_approval_users_email = False
        accountant_group_id = self.env.ref('account.group_account_user')
        if not accountant_group_id:
            raise UserError(_('Accountant Group is not available'))
        if self.customer_po_id == False or self.customer_po_file == False:
            for user in accountant_group_id.users:
                if not self.accounting_approval_users_email and user.partner_id.email:
                    self.accounting_approval_users_email = user.partner_id.email
                    continue
                if self.accounting_approval_users_email and user.partner_id.email:
                    self.accounting_approval_users_email = self.accounting_approval_users_email + ',' + user.partner_id.email

    @api.model
    def create(self, vals):
        seq = self.env['ir.sequence'].next_by_code('sale.purchase.order')
        vals['name'] = '%s%s' % ('SO', seq)
        if 'magento_order' in vals:
            if vals['magento_order'] == True:
                vals['send_email_of_sale_invoice'] = False
        result = super(SaleOrder, self).create(vals)
        result.check_order_with_issue()
        if result:
            result.date_order = result.create_date
        s_followers = []
        if vals.get('user_id', False):
            s_followers.append(result.user_id.partner_id.id)
        if vals.get('aam_id', False):
            for aam_id in result.aam_id:
                s_followers.append(aam_id.partner_id.id)
        if vals.get('soc_id', False):
            s_followers.append(result.soc_id.partner_id.id)
        allow_pdf_generate = self.env['ir.values'].get_default('base.config.settings', 'pdf_generate_enable')
        result.message_subscribe(s_followers)
        if 'state' in vals:
            attach_ids = []
            if allow_pdf_generate:
                report_name = 'Sales Order ' + result.name
                res, format = self.env['report'].sudo().get_pdf(
                    result._ids, 'sale.report_saleorder'), 'pdf'
                ext = "." + format
                if not report_name.endswith(ext):
                    report_name += ext
                attach_ids = [(report_name, res)]
            new_state = result.state
            body = 'Order Created. Status: %s. Created by %s' % (so_dict.get(new_state), self.env.user.name)
            result.message_post(body=body, attachments=attach_ids, message_type='comment')
        for order in result:
            analytic_tag_obj = self.env['account.analytic.tag']
            order_tag = analytic_tag_obj.search([('name', '=', order.name)])
            analytic_tag_ids = []
            if order_tag:
                analytic_tag_ids += order_tag.ids
            else:
                tag = analytic_tag_obj.create({'name': order.name})
                analytic_tag_ids += tag.ids
            for analytic_tag_id in analytic_tag_ids:
                order.analytic_tag_ids = [(4, analytic_tag_id, 0, analytic_tag_ids)]
        return result

    def check_order_with_issue(self):
        if not self.is_order_issue_mail_sent and self.is_mail_need_to_sent and not self.env.context.get('no_recurrion', False):
            template_name = 'pinnacle_purchase.sale_order_issue_with_order_mail_update'
            self.with_context(no_recurrion=True).write({'is_order_issue_mail_sent': True})
            self.message_post_with_template((self.env.ref(template_name)).id)

    @api.multi
    def write(self, vals):
        self = self.exists()
        artworksheet_ids, remove_followers = [], []
        remove_art_followers = []
        if vals.get('state', False):
            if vals['state'] in [item[0] for item in SaleOrder.QUOTEORDER_STATES]:
                vals['state_quote'] = vals['state']
        for record in self:
            old_state = record.state
            if 'user_id' in vals:
                if record.user_id.id not in  record.aam_id.ids + [record.soc_id.id]:
                    remove_art_followers.append(record.user_id.partner_id.id)
                    remove_followers.append(record.user_id.partner_id.id)
            if 'aam_id' in vals:
                if len(set(record.aam_id.ids) and set([record.user_id.id, record.soc_id.id])) == 0:
                    for aam_id in record.aam_id:
                        remove_art_followers.append(aam_id.partner_id.id)
                        remove_followers.append(aam_id.partner_id.id)
            if 'soc_id' in vals:
                if record.soc_id.id not in [record.user_id.id] + record.aam_id.ids:
                    remove_followers.append(record.soc_id.partner_id.id)
            # if ('user_id' in vals and 'aam_id' in vals and 'soc_id' in vals) and (record.user_id.id == record.aam_id.id and record.user_id.id == record.soc_id.id and record.soc_id.id == record.aam_id.id):
            #     remove_followers.append(record.soc_id.partner_id.id)
            for art_line in record.art_line:
                if art_line.art_id and art_line.art_id.id not in artworksheet_ids:
                    artworksheet_ids.append(art_line.art_id.id)
        if vals.get('decoration_line', False) and vals.get('order_line', False):
            del vals['decoration_line']
        if vals.get('decorations', False):
            del vals['decorations']
        not_computed = []
        if vals.get('order_line', False):
            order_lines = vals['order_line']
            order_line_groups = {}
            for order_line in self.order_line:
                if order_line.product_id.product_tmpl_id.id not in order_line_groups:
                    order_line_groups[order_line.product_id.product_tmpl_id.id] = [order_line.id]
                else:
                    order_line_groups[order_line.product_id.product_tmpl_id.id].append(order_line.id)
            for order_line in order_lines:
                if order_line[0] == 2 and self.env['sale.order.line'].browse(order_line[1]) and self.env['sale.order.line'].browse(order_line[1]).exists():
                    if len(order_line_groups[self.env['sale.order.line'].browse(order_line[1]).product_id.product_tmpl_id.id]) == 1: 
                        not_computed.append(order_line[1])
        if 'magento_order' in vals:
            if vals['magento_order'] == True:
                vals['send_email_of_sale_invoice'] = False
        if vals.get('order_category', False) == 'sample_order':
            vals['order_line'] = [[6, 0 ,[]]]
        decoration_group_ids = []
        result = super(SaleOrder, self).write(vals)
        if vals.get('order_category', False):
            for order in self:
                order.update_tier_price()
        account_tax = self.env['account.tax']
        all_sale_orders = self
        artworksheet_records = []
        if artworksheet_ids:
            artworksheet_records = self.env['art.production'].browse(artworksheet_ids).exists()
        allow_pdf_generate = self.env['ir.values'].get_default('base.config.settings', 'pdf_generate_enable')

        for self in all_sale_orders:
            self.check_order_with_issue()
            attach_ids = []
            if vals.get('state'):
                if allow_pdf_generate:
                    report_name = 'Sales Order ' + self.name
                    res, format = self.env['report'].sudo().get_pdf(
                        self._ids, 'sale.report_saleorder'), 'pdf'
                    ext = "." + format
                    if not report_name.endswith(ext):
                        report_name += ext
                    attach_ids = [(report_name, res)]
                new_state = self.state
                if old_state == new_state:
                    continue
                body = '<b>Status</b> change from <b>%s</b> to <b>%s</b>.<br />' % (so_dict.get(old_state), so_dict.get(new_state))
                self.message_post(body=body, attachments=attach_ids, message_type='comment')
            if vals.get('partner_id', False):
                if artworksheet_records:
                    artworksheet_records.write({'partner_id': self.partner_id and self.partner_id.id or False})
                analytic_tag_obj = self.env['account.analytic.tag']
                analytic_tag_ids = []
                order_tag = analytic_tag_obj.search([('name', '=', self.name)])
                if order_tag:
                    analytic_tag_ids += order_tag.ids
                else:
                    tag = analytic_tag_obj.create({'name': self.name})
                    analytic_tag_ids += tag.ids
                for analytic_tag_id in analytic_tag_ids:
                    self.analytic_tag_ids = [(4, analytic_tag_id, 0)]
            if vals.get('analytic_tag_ids', False):
                self.order_line.write({'analytic_tag_ids': [(6, 0, self.analytic_tag_ids.ids)]})
            if vals.get('program_id', False):
                if artworksheet_records:
                    artworksheet_records.write({'program_id': self.program_id and self.program_id.id or False})
            if vals.get('user_id', False):
                if artworksheet_records:
                    artworksheet_records.write({'acc_manager_id': self.user_id and self.user_id.id or False})
            if vals.get('partner_id', False) or vals.get('order_type', False):
                customer_type = self.calculate_customer_type_for_artworksheet()
                if artworksheet_records:
                    artworksheet_records.write({'customer_type': customer_type})
            if vals.get('user_id', False) or vals.get('aam_id', False) or vals.get('soc_id', False):
                followers, s_followers = [], []
                if vals.get('user_id', False):
                    followers.append(self.user_id.partner_id.id)
                    s_followers.append(self.user_id.partner_id.id)
                if vals.get('aam_id', False):
                    for aam_id in self.aam_id:
                        followers.append(aam_id.partner_id.id)
                        s_followers.append(aam_id.partner_id.id)
                if vals.get('soc_id', False):
                    s_followers.append(self.soc_id.partner_id.id)
                if artworksheet_records:
                    artworksheet_records.message_subscribe(followers)
                self.message_subscribe(s_followers)
            if remove_followers:
                self.sudo().message_unsubscribe(remove_followers)
            if remove_art_followers:
                if artworksheet_records:
                    artworksheet_records.sudo().message_unsubscribe(remove_art_followers)
            # Deleting applied_promo_codes when there is nothing added to sale order
            if not self.order_line and self.applied_promo_codes:
                self.applied_promo_codes = False
                self.promo_code = False
                self.is_promo_code_applied = 'No'
            compute_price = False
            if vals.get('order_line', False):
                order_lines = vals['order_line']
                for order_line in order_lines:
                    if order_line[0] == 2:
                        if order_line[1] not in not_computed:
                            compute_price = True
                    if order_line[0] == 1:
                        if 'product_uom_qty' in order_line[2] or 'product_uom' in order_line[2]:
                            compute_price = True
                        if 'product_uom_qty' in order_line[2] and self.env.context.get('user_response_about_change_to_qty', False):
                            if self.env['sale.order.line'].browse(order_line[1]) and self.env['sale.order.line'].browse(order_line[1]).exists():
                                for decoration in self.env['sale.order.line'].browse(order_line[1]).decorations:
                                    if decoration.decoration_group_id:
                                        decoration_group_ids.append(decoration.decoration_group_id.id)
            # Applying Sale PriceList/PromoCode by combining quantity of each order line
            # if vals.get('pricelist_id', False) or compute_price:
            #     if self.pricelist_id and self.partner_id:
            #         if 'not_call_onchange_of_pricelist_id' not in self.env.context:
            #             for sale_order_line in self.order_line:
            #                 sale_order_line.product_id.sale_order_price_without_tax = self.amount_untaxed
            #                 sale_order_line.product_id.sale_order_price_with_tax = self.amount_total
            #             self.applying_sale_pricelist_by_combining_quantity()
            # Removing Template Wise Quantity Concept
            # Not Applying Pricelist Concept
            # if vals.get('pricelist_id', False):
            #     if self.pricelist_id and self.partner_id:
            #         if 'not_call_onchange_of_pricelist_id' not in self.env.context:
            #             for sale_order_line in self.order_line:
            #                 sale_order_line.product_id.sale_order_price_without_tax = self.amount_untaxed
            #                 sale_order_line.product_id.sale_order_price_with_tax = self.amount_total
            #             self.applying_sale_pricelist_by_combining_quantity()
        if vals.get('partner_id', False) and not self._context.get('consolidated_invoice', False):
            shipping_lines = record.env['shipping.line'].search([('order', '=', self.id)])
            for line in shipping_lines:
                line.delivery_line.unlink() 
                for orderline in line.shipping_sale_orders.exists():
                    orderline.tax_id = [(6, 0, [])]
                self.env['sale.order.line'].search([('shipping_id', '=', line.id)]).unlink()
        if decoration_group_ids:
            for decoration_group  in self.env['sale.order.line.decoration.group'].browse(list(set(decoration_group_ids))):
                if decoration_group.charge_lines.exists():
                    decoration_group.charge_lines.exists().unlink()
        return result

    customer_arts = fields.Many2many(
        'product.decoration.customer.art', compute="_compute_customer_arts")

    @api.one
    @api.multi
    @api.depends('order_line')
    def _compute_customer_arts(self):
        customer_arts = []
        for order_line in self.order_line:
            for decoration in order_line.decorations:
                customer_arts += decoration.customer_art.ids
        self.customer_arts = [(6, 0, customer_arts)]

    @api.multi
    def action_confirm(self):
        for record in self:
            levels = self.env['sale.order.approval.level'].search(
                []).sorted(key=lambda r: r.approval_order)
            history_level = [
                h.approval_level.id for h in record.approval_history]
            for level in levels:
                if record.amount_total >= level.lower_limit:
                    if level.id not in history_level:
                        raise UserError(
                            _('Please send this order to a manager for approval.'))
            if record.partner_id.customer_type == 'new':
                record.partner_id.customer_type = 'current'
        result = super(SaleOrder, self).action_confirm()
        return result

    @api.multi
    def set_to_to_approve(self):
        self.ensure_one()
        new_context = dict(self.env.context).copy()
        new_context.update({'active_id': self[0].id,
                            'active_ids': [self[0].id],
                            'is_a_manager_approval': True})
        form_view_id = self.env.ref(
            'pinnacle_sales.notes_when_request_for_approval_view_form')
        return {'type': 'ir.actions.act_window', 'view_mode': 'form',
                'target': 'new', 'view_id': form_view_id.id,
                'res_model': 'notes.when.request.for.approval',
                'name': 'Notes When Requesting For Manager Approval',
                'context': new_context,
                }

    @api.multi
    def set_to_to_accounting_approve(self):
        self.ensure_one()
        new_context = dict(self.env.context).copy()
        new_context.update({'active_id': self[0].id,
                            'active_ids': [self[0].id],
                            'is_a_accounting_approval': True})
        form_view_id = self.env.ref(
            'pinnacle_sales.notes_when_request_for_approval_view_form')
        return {'type': 'ir.actions.act_window', 'view_mode': 'form',
                'target': 'new', 'view_id': form_view_id.id,
                'res_model': 'notes.when.request.for.approval',
                'name': 'Notes When Requesting For Accounting Approval',
                'context': new_context,
                }
    
    send_for_approval = fields.Boolean(
        compute="_compute_send_for_approval", store=True, copy=False)

    @api.one
    @api.depends('amount_total','state')
    def _compute_send_for_approval(self):
        self.send_for_approval = False
        if self.amount_total:
            levels = self.env['sale.order.approval.level'].search(
                []).sorted(key=lambda r: r.approval_order)
            for level in levels:
                if level:
                    if self.amount_total >= level.lower_limit:
                        self.send_for_approval = True
                        break
        if self.state in ['approve', 'reject', 'sent', 'confirmation_received', 'invoiced', 'partially_invoiced', 'shipped', 'delivered', 'reopen', 'cancel']:
            self.send_for_approval = False
                        
    @api.multi
    def set_to_approve(self):
        self.ensure_one()
        if self:
            new_context = dict(self.env.context).copy()
            new_context.update({'active_id': self[0].id,
                                'active_ids': [self[0].id]})
            form_view_id = self.env.ref(
                'pinnacle_sales.sale_order_approve_form_view')
            return {'type': 'ir.actions.act_window', 'view_mode': 'form',
                    'target': 'new', 'view_id': form_view_id.id,
                    'res_model': 'sale.order.approve.and.reject',
                    'name': 'Approve the sales order at specific level',
                    'context': new_context,
                    }

    @api.multi
    def set_to_accounting_approve(self):
        self.ensure_one()
        return {
                'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'target': 'new',
                'res_model': 'accouting.approved.wizard',
                'name': 'Accounting Approve',
                'context': {'default_order_id': self.id}
                }

    @api.multi
    def set_to_accounting_reject(self):
        self.ensure_one()
        return {
                'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'target': 'new',
                'res_model': 'accouting.rejected.wizard',
                'name': 'Accounting Reject',
                'context': {'default_order_id': self.id}
                }

    @api.multi
    def set_to_reject(self):
        self.ensure_one()
        if self:
            new_context = dict(self.env.context).copy()
            new_context.update({'active_id': self[0].id,
                                'active_ids': [self[0].id]})
            form_view_id = self.env.ref(
                'pinnacle_sales.sale_order_reject_form_view')
            return {'type': 'ir.actions.act_window', 'view_mode': 'form',
                    'target': 'new', 'view_id': form_view_id.id,
                    'res_model': 'sale.order.approve.and.reject',
                    'name': 'Reject the sale Order at specific level',
                    'context': new_context,
                    }

        return True

    def _create_blank_product_shipping_line(self):
        blank_product = {}
        for order_line in self.order_line:
            if order_line.is_decoration_disable and order_line.product_id.product_tmpl_id.type != 'service':
                key = order_line.product_id.product_tmpl_id.id
                supplier = order_line.change_supplier.id
                
                if key not in blank_product.keys():
                    blank_product[key] = {}
                    blank_product[key][supplier] = {}
                    blank_product[key][supplier]['qty'] = order_line.product_uom_qty
                    blank_product[key][supplier]['product'] = [order_line.product_id.id]
                    blank_product[key][supplier]['order_line'] = [order_line.id]
                elif supplier not in  blank_product[key].keys():
                    blank_product[key][supplier] = {}
                    blank_product[key][supplier]['qty'] = order_line.product_uom_qty
                    blank_product[key][supplier]['product'] = [order_line.product_id.id]
                    #blank_product[key][supplier]['order_line'].append(order_line.id)
                    blank_product[key][supplier]['order_line'] = [order_line.id]
                else:
                    blank_product[key][supplier]['qty']  += order_line.product_uom_qty
                    blank_product[key][supplier]['product'].append(order_line.product_id.id)
                    blank_product[key][supplier]['order_line'].append(order_line.id)
        shipping_line = self.env['shipping.line']
        product_supplier = self.env['product.supplierinfo']

        for key in blank_product:
            value = blank_product[key]
            for supplier in value:
                browse_supplier = product_supplier.browse(supplier)
                supplier_name = browse_supplier.name.id
                check = False
                line = shipping_line.search(
                            [('tmpl_id', '=', key), ('order', '=', self.id), ('is_decoration_disable','=', True), ('supplier', '=', supplier)], limit=1)
                if not self.is_regular:
                    if browse_supplier.override_sample_policy_id:
                        shipping_info = browse_supplier.shipping_info
                    else:
                        shipping_info = browse_supplier.name.shipping_info
                    if shipping_info in ['vendor_account_bill', 'vendor_account_free']:
                        check = True
                val_to_create = {
                            'product_qty': value[supplier]['qty'],
                            'supplier': supplier,
                            'partner_supplier': supplier_name,
                            'order': self.id, 
                            'tmpl_id': key,
                            'is_decoration_disable': True,
                            'vendor_account': check,
                            'shipping_sale_orders': [(6, 0, value[supplier]['order_line'])],
                         }
                if line:
                    line.write(val_to_create)
                    line.product_line.unlink()
                    shipping = line
                    line.onchange_carton_dimension()
                else:
                    val_to_create['contact'] = self.partner_id.id
                    shipping = shipping_line.create(val_to_create)
                    shipping.onchange_contact()
                    shipping.onchange_carton_dimension()
                    # 15659 : [FIX] Sales order >> Blank decorated product should not be deleted from order lines when its shipping line is deleted from shipping tab.
                    # if self.is_regular:
                    #     for soline in shipping.shipping_sale_orders:
                    #         soline.shipping_id = shipping.id
                for pro in list(set(value[supplier]['product'])):
                    self.env['product.shipping.line'].create(
                        {'product': pro, 'shipping_id': shipping.id})
        return True

    @api.multi
    def generate_shipping_vals_dict(self, vals, shipping=False, supplier=False, finisher=False):
        vals_dict = {}
        if vals:
            is_supplier_line = supplier
            is_finisher_line = finisher
            is_shipping_line = shipping

            vals_dict = {
                            'product_id': vals['product_id'],
                            'qty': vals['qty'],
                            'finisher': vals['finisher'],
                            'supplier': vals['supplier'],
                            'supplier_id': vals['supplier_id'],
                            'is_supplier_line': is_supplier_line,
                            'is_finisher_line': is_finisher_line,
                            'order_lines': vals['order_lines'],
                            'product_tmpl_id': vals['product_tmpl_id'],
                            'contact': vals['contact'],
                            'decoration_line_id': vals['decoration_line_id'],
                    }
        return vals_dict

    @api.multi
    def create_supplier_line_dict(self, vals):
        supplier_line = {}
        if vals:
            for val in vals:
                if val['supplier'] in supplier_line:
                    supplier_line[val['supplier']].append(val)
                else:
                    supplier_line[val['supplier']] = [val]
        x_line = self.create_finisher_line_dict(supplier_line)
        return x_line

    @api.multi
    def create_finisher_line_dict(self, lines):
        x_line = {}
        decoration_obj = self.env['sale.order.line.decoration']
        for key, value in lines.iteritems():
            finisher_line = {}
            for index, val in enumerate(value):
                if val['finisher'] in finisher_line:
                    finisher_line[val['finisher']]['product_id'].append(val['product_id'][0])
                    finisher_line[val['finisher']]['order_lines'].append(val['order_lines'][0])
                    if decoration_obj.browse(finisher_line[val['finisher']]['decoration_line_id']).sale_order_line_id.id != decoration_obj.browse(val['decoration_line_id']).sale_order_line_id.id:
                        finisher_line[val['finisher']]['qty'] += val['qty']
                    # else:
                    #     finisher_line[val['finisher']]['qty'] += val['qty']
                    finisher_line[val['finisher']]['product_tmpl_id'] = val['product_tmpl_id']
                    finisher_line[val['finisher']]['supplier_id'] = val['supplier_id']
                    finisher_line[val['finisher']]['finisher'] = val['finisher']
                    finisher_line[val['finisher']]['decoration_line_id'] = val['decoration_line_id']
                    finisher_line[val['finisher']]['supplier'] = val['supplier']
                    finisher_line[val['finisher']]['contact'] = val['contact']
                else:
                    finisher_line[val['finisher']]= val
            finisher_line[val['finisher']]['product_id'] = list(set(finisher_line[val['finisher']]['product_id']))
            x_line[key] = finisher_line
        return x_line

    @api.multi
    def create_shipping_lines_dict(self, lines):
        if lines:
            for key, value in lines.iteritems():
                supplier_line = self.create_supplier_line_dict(value)
                lines[key] = supplier_line
        return lines

    @api.multi
    def update_shipping_line(self): 
        shipping_line = {}
        shipping_lines = [sline.tmpl_id.id for sline in self.env['shipping.line'].search(
            [('order', '=', self.id), ('is_finisher_line', '=', False), ('is_supplier_line', '=', False)])]
        temp_line = {}
        if self.order_line.exists():
            self._create_blank_product_shipping_line()
            if not self.is_regular:
                return
            qty = 0
            for line in self.decorations:
                if line.sale_order_line_id.is_decoration_disable:
                    continue

                if line.product_id.product_tmpl_id.id in shipping_line:
                    shipping_line[line.product_id.product_tmpl_id.id].append({'product_id': [line.product_id.id],
                                                                           'qty': line.sale_order_line_product_uom_qty,
                                                                           'finisher': line.finisher.id,
                                                                           'supplier': line.sale_order_line_id.change_supplier.id,
                                                                           'supplier_id': line.sale_order_line_id.change_supplier.name.id,
                                                                           'is_finisher_line': False,
                                                                           'order_lines':[line.sale_order_line_id.id],
                                                                           'contact': self.partner_id.id,
                                                                           'decoration_line_id': line.id,
                                                                           'product_tmpl_id': line.product_id.product_tmpl_id.id,
                                                                           'inv_code': line.sale_order_line_id.inventory_code
                                                                          })
                else:
                    shipping_line[line.product_id.product_tmpl_id.id] = [{'product_id': [line.product_id.id],
                                                                           'qty': line.sale_order_line_product_uom_qty,
                                                                           'finisher': line.finisher.id,
                                                                           'supplier': line.sale_order_line_id.change_supplier.id,
                                                                           'supplier_id': line.sale_order_line_id.change_supplier.name.id,
                                                                           'is_finisher_line': False,
                                                                           'order_lines':[line.sale_order_line_id.id],
                                                                           'contact': self.partner_id.id,
                                                                           'decoration_line_id': line.id,
                                                                           'product_tmpl_id': line.product_id.product_tmpl_id.id,
                                                                           'inv_code': line.sale_order_line_id.inventory_code
                                                                          }]
                            
        lines = self.create_shipping_lines_dict(shipping_line)
        if len(lines):
            self.generate_supplier_lines(lines)
        return True

    @api.multi
    def generate_supplier_lines(self, supplier_line):
        if self.shipping_line:
            shipping_lines = [line.decoration_line_id.id for line in self.env[
            'shipping.line'].search([('order', '=', self.id), 
                                     ('is_decoration_disable','=', False)])]
            if shipping_lines:
                for key, value in supplier_line.iteritems():
                    for key, val in value.iteritems():
                        for k ,v in val.iteritems():
                            flag = self.env['shipping.line'].search([('order', '=', self.id), ('decoration_line_id', '=', v['decoration_line_id'])])
                            if v['decoration_line_id'] not in shipping_lines:
                                shipping = self.env['shipping.line'].create({'supplier': int(v['supplier']), 'product_qty': v['qty'], 'finisher': v['finisher'],
                                                                             'order': self.id, 'tmpl_id': v['product_tmpl_id'], 'inv_code': v['inv_code'],
                                                                             'shipping_sale_orders':[(6, 0,  v['order_lines'])], 'contact': v['contact'],
                                                                             'decoration_line_id': v['decoration_line_id'], 'partner_supplier': v['supplier_id']})
                                shipping.onchange_contact()
                                shipping.onchange_carton_dimension()
                                for pro in v['product_id']:
                                    self.env['product.shipping.line'].create(
                                        {'product': pro, 'shipping_id': shipping.id})
                            else:
                                lines = self.env['shipping.line'].search(
                                        [('tmpl_id', '=', v['product_tmpl_id']),  ('order', '=', self.id), ('decoration_line_id', '=', v['decoration_line_id']),
                                         ('is_decoration_disable','=', False), ('finisher', '=', v['finisher']), ('supplier', '=', int(v['supplier']))])
                                if lines:
                                    line_ids = [line.decoration_line_id.id for line in lines]
                                    for sline in lines:
                                        if sline.product_qty != v['qty']:
                                            sline.write({'supplier': int(v['supplier']), 'product_qty': v['qty'], 'finisher': v['finisher'],
                                                                     'order': self.id, 'tmpl_id': v['product_tmpl_id'], 'inv_code': v['inv_code'], 
                                                                     'shipping_sale_orders':[(6, 0,  v['order_lines'])], 'contact': v['contact'],
                                                                     'decoration_line_id': v['decoration_line_id'], 'partner_supplier': int(v['supplier_id'])})
                                            sline.onchange_contact()
                                            sline.onchange_carton_dimension()
                                        # else:
                                        #     sline.unlink()
                                    shipping_lines = list(set(shipping_lines).difference(set(line_ids)))
                                    #lines.unlink()
                                    # shipping = self.env['shipping.line'].create({'supplier': int(v['supplier']), 'product_qty': v['qty'], 'finisher': v['finisher'],
                                    #                                  'order': self.id, 'tmpl_id': v['product_tmpl_id'],  
                                    #                                  'shipping_sale_orders':[(6, 0,  v['order_lines'])], 'contact': v['contact'],
                                    #                                  'decoration_line_id': v['decoration_line_id'], 'partner_supplier': int(v['supplier_id'])})
                                    
                                    # for pro in v['product_id']:
                                    #     self.env['product.shipping.line'].create(
                                    #         {'product': pro, 'shipping_id': shipping.id})
            else:
                for key, value in supplier_line.iteritems():
                    for key, val in value.iteritems():
                        for k ,v in val.iteritems():
                            shipping = self.env['shipping.line'].create({'supplier': int(v['supplier']), 'product_qty': v['qty'], 'finisher': v['finisher'],
                                                                         'order': self.id, 'tmpl_id': v['product_tmpl_id'], 'inv_code': v['inv_code'],
                                                                         'shipping_sale_orders':[(6, 0,  v['order_lines'])], 'contact': v['contact'],
                                                                         'decoration_line_id': v['decoration_line_id'], 'partner_supplier': v['supplier_id']})
                            shipping.onchange_contact()
                            shipping.onchange_carton_dimension()
                            for pro in v['product_id']:
                                self.env['product.shipping.line'].create(
                                    {'product': pro, 'shipping_id': shipping.id})
        else:
            for key, value in supplier_line.iteritems():
                for key, val in value.iteritems():
                    for k ,v in val.iteritems():
                        shipping = self.env['shipping.line'].create({'supplier': int(v['supplier']), 'product_qty': v['qty'], 'finisher': v['finisher'],
                                                                     'order': self.id, 'tmpl_id': v['product_tmpl_id'], 'inv_code': v['inv_code'], 
                                                                     'shipping_sale_orders':[(6, 0,  v['order_lines'])], 'contact': v['contact'],
                                                                     'decoration_line_id': v['decoration_line_id'], 'partner_supplier': int(v['supplier_id'])})
                        shipping.onchange_contact()
                        shipping.onchange_carton_dimension()
                        for pro in v['product_id']:
                            self.env['product.shipping.line'].create(
                                {'product': pro, 'shipping_id': shipping.id})

    @api.multi
    def calcalute_only_cost(self):
        if self.is_regular:
            res_cost = self._set_tier_cost_unit_by_web_api()
        else:
            res_cost = self._set_tier_cost_unit_by_web_api(sample_order=True)
        return True

    def _set_tier_cost_unit_by_web_api(self, sample_order=None):
        product_product_qty = {}
        warnings = []
        blank_product = self.env.ref('pinnacle_sales.product_product_blank_order')
        for order_line in self.order_line:
            if order_line.change_supplier and (order_line.product_id.product_tmpl_id.type != 'service' or order_line.product_id.product_tmpl_id.id == blank_product.id):
                key = order_line.product_id.product_tmpl_id.id
                supplier_key = order_line.change_supplier.id
                if key and supplier_key:
                    if not product_product_qty.has_key(key):
                        product_product_qty[key] = {}
                        product_product_qty[key][supplier_key] = {}
                        product_product_qty[key][supplier_key]['qty'] = order_line.product_uom_qty
                    elif not product_product_qty[key].has_key(supplier_key):
                        product_product_qty[key][supplier_key] = {}
                        product_product_qty[key][supplier_key]['qty'] = order_line.product_uom_qty
                    else:
                        product_product_qty[key][supplier_key]['qty'] = product_product_qty[key][supplier_key]['qty'] + order_line.product_uom_qty

        for order_line in self.order_line:
            if not order_line.change_supplier and order_line.product_id.product_tmpl_id.type != 'service':
                # If supplier not define no need to check in tiers assign cost price of product
                order_line.update({'purchase_price': order_line.product_id.standard_price})
            if order_line.change_supplier and (order_line.product_id.product_tmpl_id.type != 'service' or order_line.product_id.product_tmpl_id.id == blank_product.id):
                key = order_line.product_id.product_tmpl_id.id
                qty = product_product_qty[key][order_line.change_supplier.id]['qty']
                res = order_line.product_id.get_cost_price(order_line.change_supplier, qty, sample_order=sample_order, spec_order=True)
                cost = res['cost']
                supplier = order_line.change_supplier
                if sample_order:
                    user = supplier
                    if user.name.company_type == "person" or user.name.customer == True:
                        raise UserError(_('This supplier\'s sample policy has not been set.'))
                    else:
                        discount = None
                        discount_price = 0
                        if user.override_sample_policy_id.id:
                            policy = user.override_sample_policy_id
                        else:
                            policy = user.name.sample_policy_id
                        if policy.discount == "free":
                            cost = 0
                        elif policy.discount_price:
                            cost = cost - cost * policy.discount_price / 100.0
                order_line.update({'purchase_price': cost})

    @api.multi
    def update_tier_price(self, product_added=False):
        if not isinstance(product_added, int):
            product_added = False
        if self.is_regular:
            res_cost = self._set_tier_cost_unit(product_added=product_added)
            res_price = self._set_tier_price_unit(product_added=product_added)
        else:
            res_cost = self._set_tier_cost_unit(sample_order=True, product_added=product_added)
            res_price = self._set_tier_price_unit(sample_order=True, product_added=product_added)



        product_less_than_minimum_quantity = self.env.ref('pinnacle_sales.product_product_less_than_minimum_quantity')
        product_uom_unit = self.env.ref('product.product_uom_unit')
        if not product_less_than_minimum_quantity or not product_uom_unit:
                UserError(_('Less than minimum quantity Product  or Product\'s unit uom Not Found'))
        groups = []
        to_be_deleted = []
        for order_line in self.order_line:
            if order_line.product_id.id == product_less_than_minimum_quantity.id:
                to_be_deleted.append(order_line.id)
                continue
            exited_in_group = False
            for group in groups:
                if order_line.product_id.id in [record.product_id.id for record in self.env['sale.order.line'].browse(group)] and order_line.change_supplier.id in [record.change_supplier.id for record in self.env['sale.order.line'].browse(group)]:
                    exited_in_group = group
                    break
            if exited_in_group:
                groups[groups.index(exited_in_group)].append(order_line.id)
            else:
                groups.append([order_line.id])
        self.env['sale.order.line'].browse(to_be_deleted).unlink()
        for group in groups:
            total_qty = 0.00
            if self.env['sale.order.line'].browse(group):
                order_line_desc = ''
                for order_line in self.env['sale.order.line'].browse(group):
                    if not order_line_desc:
                        order_line_desc = order_line.name
                    else:
                        order_line_desc = order_line_desc + ', ' + order_line.name
                    total_qty += order_line.product_uom_qty   
                first_order_line = self.env['sale.order.line'].browse(group)[0] 
                if first_order_line.change_supplier and first_order_line.change_supplier.product_vendor_tier_ids and ((first_order_line.change_supplier.moq and first_order_line.change_supplier.moq != 'not_allowed') or (first_order_line.change_supplier.o_moq and first_order_line.change_supplier.o_moq != 'not_allowed')) :
                    price_unit = 0.00
                    purchase_price = 0.00
                    add_line = False
                    if first_order_line.change_supplier.o_ltm_policy_id:
                        if first_order_line.change_supplier.o_moq == 'half_column':
                            if (first_order_line.change_supplier.product_vendor_tier_ids.sorted(key=lambda r:r.min_qty)[0].min_qty) > total_qty and (first_order_line.change_supplier.product_vendor_tier_ids.sorted(key=lambda r:r.min_qty)[0].min_qty/2) <= total_qty:
                                price_unit = first_order_line.change_supplier.o_minimum_fee_price
                                purchase_price = first_order_line.change_supplier.o_minimum_fee_cost
                                add_line = True
                        if first_order_line.change_supplier.o_moq == 'other':
                            if (first_order_line.change_supplier.product_vendor_tier_ids.sorted(key=lambda r:r.min_qty)[0].min_qty) > total_qty and (first_order_line.change_supplier.o_other_qty) <= total_qty:
                                price_unit = first_order_line.change_supplier.o_minimum_fee_price
                                purchase_price = first_order_line.change_supplier.o_minimum_fee_cost
                                add_line = True
                    else:
                        if first_order_line.change_supplier.moq == 'half_column':
                            if (first_order_line.change_supplier.product_vendor_tier_ids.sorted(key=lambda r:r.min_qty)[0].min_qty) > total_qty and (first_order_line.change_supplier.product_vendor_tier_ids.sorted(key=lambda r:r.min_qty)[0].min_qty/2) <= total_qty:
                                price_unit = first_order_line.change_supplier.minimum_fee_price
                                purchase_price = first_order_line.change_supplier.minimum_fee_cost
                                add_line = True
                        if first_order_line.change_supplier.moq == 'other':
                            if (first_order_line.change_supplier.product_vendor_tier_ids.sorted(key=lambda r:r.min_qty)[0].min_qty) > total_qty and (first_order_line.change_supplier.other_qty) <= total_qty:
                                price_unit = first_order_line.change_supplier.minimum_fee_price
                                purchase_price = first_order_line.change_supplier.minimum_fee_cost
                                add_line = True
                    if add_line and False: # Commmeted as there should be only one LTM line.
                        created_record = order_line.create({
                            'product_id': product_less_than_minimum_quantity.id, 
                            'name': product_less_than_minimum_quantity.name,
                            'product_uom_qty': 1,
                            'product_uom': product_uom_unit.id, 
                            'price_unit': price_unit,
                            'purchase_price': purchase_price,
                            'order_id': self.id,
                            })
                        created_record.name = created_record.name + ' For ' + order_line_desc
        if any(res_cost): 
                warning = ''
                for res in res_cost:
                    if res:
                        warning += res
                ctx = dict(self._context)
                ctx['warning'] = warning
                return {
                            'type': 'ir.actions.act_window',
                            'name': _('LTM Warning'),
                            'view_type': 'form',
                            'view_mode': 'form',
                            'res_model': 'warning.wizard',
                            'view_id': False,
                            'target': 'new',
                            'nodestroy': True, 
                            'context': ctx,
                        }

    def _set_tier_price_unit(self, sample_order=None, product_added=False):
        product_temp_qty = {}
        product_product_qty = {}
        product_supplier_temp = {}
        blank_product = self.env.ref('pinnacle_sales.product_product_blank_order')
        for order_line in self.order_line:
            if product_added and product_added != order_line.product_id.product_tmpl_id.id:
                continue
            if not order_line.change_supplier and order_line.product_id.product_tmpl_id.type != 'service':
                # If supplier not define no need to check in tiers assign list price of product
                order_line.update({'price_unit': order_line.product_id.list_price})
            if order_line.change_supplier and (order_line.product_id.product_tmpl_id.type != 'service'  or order_line.product_id.product_tmpl_id.id == blank_product.id):
                key = order_line.product_id.product_tmpl_id.id
                product_key = order_line.product_id.id
                supplier_key = order_line.change_supplier.id
                if key:
                    if not product_temp_qty.has_key(key):
                        product_temp_qty[key] = {}
                        product_temp_qty[key]['qty'] = order_line.product_uom_qty
                        product_temp_qty[key]['supplier'] = order_line.change_supplier
                    else:
                        product_temp_qty[key]['qty'] = product_temp_qty[key]['qty'] + order_line.product_uom_qty
                        product_temp_qty[key]['supplier'] = order_line.change_supplier
                if product_key:
                    if not product_product_qty.has_key(product_key):
                        product_product_qty[product_key] = {}
                        product_product_qty[product_key]['qty'] = order_line.product_uom_qty
                        product_product_qty[product_key]['supplier'] = order_line.change_supplier
                    else:
                        product_product_qty[product_key]['qty'] = product_product_qty[product_key]['qty'] + order_line.product_uom_qty
                        product_product_qty[product_key]['supplier'] = order_line.change_supplier
        
        product_temp_tier = {}
        product_template = self.env['product.template']

        for key,value in product_temp_qty.iteritems():
            #qty = product_temp_qty[product_tmp_id]
            qty = value['qty']
            product_temp_tier[key] = product_template.browse(key).get_sale_price(value['supplier'], qty, sample_order=sample_order,spec_order=self.order_type == 'spec')
        group_price_by = self.calculate_variants()
        for order_line in self.order_line:
            if product_added and product_added != order_line.product_id.product_tmpl_id.id:
                continue
            if order_line.change_supplier and (order_line.product_id.product_tmpl_id.type != 'service'  or order_line.product_id.product_tmpl_id.id == blank_product.id):
                additional_price = 0
                base_price = product_temp_tier[order_line.product_id.product_tmpl_id.id]
                for attrib in order_line.product_id.attribute_value_ids.ids:
                    if group_price_by[order_line.product_id.product_tmpl_id.id].has_key(attrib):
                        additional_price = additional_price + group_price_by[order_line.product_id.product_tmpl_id.id][attrib]
                if not order_line.is_override:
                    order_line.update({'price_unit' : base_price + additional_price })

    def calculate_variants(self):
        group__by = {}
        def update_dict(attb_id, line, group__by):
            tmpl_id = line.product_id.product_tmpl_id.id
            if not group__by.has_key(tmpl_id):
                group__by[tmpl_id] = {}
                group__by[tmpl_id][attb_id] =  line.product_uom_qty
            else:
                if group__by[tmpl_id].has_key(attb_id):
                    group__by[tmpl_id][attb_id] = group__by[tmpl_id][attb_id] + line.product_uom_qty
                else:
                    group__by[tmpl_id][attb_id] =  line.product_uom_qty

        for line in self.order_line:
            for attb_id in line.product_id.attribute_value_ids.ids:
                update_dict(attb_id, line, group__by)

        group_price_by = {}
        attrib_value = self.env['attribute.value']
        product_tmp = self.env['product.template']
        for temp_id in group__by:
            group_price_by[temp_id] = {}
            for key in group__by[temp_id]:
                line = attrib_value.search([('tmpl_id','=', temp_id),('value_id','=',key)], limit=1)
                if line:
                    tier = self.env['product.template'].browse(temp_id)._get_tier(group__by[temp_id][key] or 0)[1]
                    if tier ==1:
                        group_price_by[temp_id][key] = line.tier1
                    elif tier ==2:
                        group_price_by[temp_id][key] = line.tier2
                    elif tier ==3:
                        group_price_by[temp_id][key] = line.tier3
                    elif tier ==4:
                        group_price_by[temp_id][key] = line.tier4
                    elif tier ==5:
                        group_price_by[temp_id][key] = line.tier5
                    elif tier ==6:
                        group_price_by[temp_id][key] = line.tier6
                else:
                    group_price_by[key] = 0
        return group_price_by

    def _set_tier_cost_unit(self, sample_order=None, product_added=False):
        product_ltm_fee = self.env.ref('pinnacle_sales.product_product_ltm_fee')
        ltm_processed = []
        def update_ltm():
            ltm_dic = {}
            for ltm in self.env['sale.order.line'].search([('order_id', '=', self.id), ('product_id', '=', product_ltm_fee.id)]):
                supplier_key = ltm.ltm_parent.change_supplier.id
                temp_id = ltm.ltm_parent.product_id.product_tmpl_id.id
                if not ltm_dic.has_key(temp_id):
                    ltm_dic[temp_id] = {}
                    ltm_dic[temp_id][supplier_key] = [ltm]
                else:
                    if not ltm_dic[temp_id].has_key(supplier_key):
                        ltm_dic[temp_id][supplier_key] = [ltm]
                    else:
                        ltm_dic[temp_id][supplier_key].append(ltm)
            return ltm_dic
        ltm_dic = update_ltm()

        product_product_qty = {}
        warnings = []

        order_to_process = []
        blank_product = self.env.ref('pinnacle_sales.product_product_blank_order')
        for order_line in self.order_line:
            if product_added and product_added != order_line.product_id.product_tmpl_id.id:
                continue
            if order_line.change_supplier and (order_line.product_id.product_tmpl_id.type != 'service' or order_line.product_id.product_tmpl_id.id == blank_product.id):
                key = order_line.product_id.product_tmpl_id.id
                supplier_key = order_line.change_supplier.id
                if key and supplier_key:
                    order_to_process.append(order_line)
                    if not product_product_qty.has_key(key):
                        product_product_qty[key] = {}
                        product_product_qty[key][supplier_key] = {}
                        product_product_qty[key][supplier_key]['qty'] = order_line.product_uom_qty
                    elif not product_product_qty[key].has_key(supplier_key):
                        product_product_qty[key][supplier_key] = {}
                        product_product_qty[key][supplier_key]['qty'] = order_line.product_uom_qty
                    else:
                        product_product_qty[key][supplier_key]['qty'] = product_product_qty[key][supplier_key]['qty'] + order_line.product_uom_qty

        for order_line in order_to_process:
            if not order_line.change_supplier and order_line.product_id.product_tmpl_id.type != 'service':
                # If supplier not define no need to check in tiers assign cost price of product
                order_line.update({'purchase_price': order_line.product_id.standard_price})
            if order_line.change_supplier and (order_line.product_id.product_tmpl_id.type != 'service' or order_line.product_id.product_tmpl_id.id == blank_product.id):
                key = order_line.product_id.product_tmpl_id.id
                qty = product_product_qty[key][order_line.change_supplier.id]['qty']
                res = order_line.product_id.get_cost_price(order_line.change_supplier, qty, sample_order=sample_order, spec_order=self.order_type == 'spec')
                cost = res['cost']
                supplier = order_line.change_supplier
                if sample_order:
                    user = supplier
                    if user.name.company_type == "person" or user.name.customer == True:
                        raise UserError(_('This supplier\'s sample policy has not been set. '))
                    else:
                        discount = None
                        discount_price = 0
                        if user.override_sample_policy_id.id:
                            policy = user.override_sample_policy_id
                        else:
                            policy = user.name.sample_policy_id
                        if policy.discount == "free":
                            cost = 0
                        elif policy.discount_price:
                            cost = cost - cost * policy.discount_price / 100.0
                if res['fee']:
                    if (not supplier.o_ltm_policy_id and supplier.moq == 'not_allowed') or (supplier.o_ltm_policy_id and supplier.o_moq == 'not_allowed'):
                        price_unit = 0.0
                        purchase_price = 0.0
                    else:
                        price_unit = supplier.o_minimum_fee_price if supplier.o_ltm_policy_id else supplier.minimum_fee_price
                        purchase_price = supplier.o_minimum_fee_cost if supplier.o_ltm_policy_id else supplier.minimum_fee_cost
                    product_name = product_ltm_fee.name +' for '+ order_line.product_id.product_tmpl_id.name
                    if (ltm_dic.has_key(key) and ltm_dic[key].has_key(supplier.id)):
                        for ltms in ltm_dic[key][supplier.id]:
                            ltm_processed.append(ltms.id)
                            ltms.write({'name': product_name, 'price_unit': price_unit, 
                                'purchase_price': purchase_price})
                    else:
                        create_ltm = order_line.create({'product_id': product_ltm_fee.id, 
                                            'name': product_name,
                                            'product_uom_qty': 1,
                                            'product_uom': product_ltm_fee.uom_id.id, 
                                            'price_unit': price_unit,
                                            'purchase_price': purchase_price,
                                            'order_id': self.id,
                                            'ltm_fee': True,
                                            'ltm_parent': order_line.id
                                         })
                        ltm_processed.append(create_ltm.id)
                        ltm_dic = update_ltm()
                if not order_line.is_override:
                    order_line.update({'purchase_price': cost})
                if res['warning'] and res['warning'] not in warnings:
                    warnings.append(res['warning'])
                else:
                    warnings.append(False)
        for line in self.env['sale.order.line'].search([('order_id', '=', self.id), ('product_id', '=', product_ltm_fee.id)]):
            if (line.id not in ltm_processed and not product_added) or (product_added and line.ltm_parent.product_id.product_tmpl_id.id == product_added and
                line.id not in ltm_processed):
                line.unlink()
        return warnings
                
                
    @api.multi
    def create_setup_sale_price_and_run_charge_with_zero(self, decoration , decoration_name, total_qty, number_of_colors, number_of_codes, product_setup_sale_price, ltm_policy_applied, ltm_policy_price_unit, ltm_policy_purchase_price, product_run_charge, product_pms_patching, decoration_group_id):
        if ltm_policy_applied: 
            created_record = self.env['sale.order.line.decoration.group.charge.line'].create(
                                        {'product_id': product_setup_sale_price.id,
                                         'product_uom_qty': number_of_colors + number_of_codes,
                                         'price_unit': ltm_policy_price_unit,
                                         'purchase_price': ltm_policy_purchase_price,
                                         'product_uom': product_setup_sale_price.uom_id.id,
                                         'decoration_group_id': decoration_group_id.id,
                                         'at_least_one_decoration_recalculate': decoration.id,
                                         })
            if decoration.sale_order_line_id.product_id.product_sku:
                created_record.name = decoration_group_id.group_sequence  + ' - ' + decoration.sale_order_line_id.product_id.product_sku + ' - Setup - ' + str(decoration.imprint_method and decoration.imprint_method.name or False) +  ' - ' + str(decoration.imprint_location and decoration.imprint_location.name or False) +  ' - ' + decoration_name + ' - ' + 'LTM'
                created_record.name_second = decoration.sale_order_line_id.product_id.product_sku + ' - Setup - ' + str(decoration.imprint_location and decoration.imprint_location.name or False) +  ' - '+ decoration_name + ' - ' + 'LTM'
            else:
                created_record.name = decoration_group_id.group_sequence  + ' - Setup - ' + str(decoration.imprint_method and decoration.imprint_method.name or False) + ' - ' + str(decoration.imprint_location and decoration.imprint_location.name or False) +  ' - ' + decoration_name + ' - ' + 'LTM'
                created_record.name_second = ' Setup - ' + str(decoration.imprint_location and decoration.imprint_location.name or False) + ' - '+ decoration_name + ' - ' + 'LTM'
        else:   
            created_record = self.env['sale.order.line.decoration.group.charge.line'].create(
                                        {'product_id': product_setup_sale_price.id,
                                         'product_uom_qty': number_of_colors + number_of_codes,
                                         'price_unit': 0.00,
                                         'purchase_price': 0.00,
                                         'product_uom': product_setup_sale_price.uom_id.id,
                                         'decoration_group_id': decoration_group_id.id,
                                         'at_least_one_decoration_recalculate': decoration.id,
                                         })
            if decoration.sale_order_line_id.product_id.product_sku:
                created_record.name = decoration_group_id.group_sequence  + ' - ' + decoration.sale_order_line_id.product_id.product_sku + ' - Setup - ' + str(decoration.imprint_method and decoration.imprint_method.name or False) +  ' - ' + str(decoration.imprint_location and decoration.imprint_location.name or False) +  ' - ' + decoration_name
                created_record.name_second = decoration.sale_order_line_id.product_id.product_sku + ' - Setup - ' + str(decoration.imprint_location and decoration.imprint_location.name or False) +  ' - '+ decoration_name
            else:
                created_record.name = decoration_group_id.group_sequence  + ' - Setup - ' + str(decoration.imprint_method and decoration.imprint_method.name or False) + ' - ' + str(decoration.imprint_location and decoration.imprint_location.name or False) +  ' - ' + decoration_name
                created_record.name_second = ' Setup - ' + str(decoration.imprint_location and decoration.imprint_location.name or False) + ' - '+ decoration_name
        #Need to handle code when quantity_multiplier is zero
        quantity_multiplier = (number_of_colors + number_of_codes)        
        created_record = self.env['sale.order.line.decoration.group.charge.line'].create(
                                {'product_id': product_run_charge.id,
                                 'product_uom_qty': total_qty * (quantity_multiplier if quantity_multiplier > 0 else 1 ),
                                 'run_charge_multiplied': quantity_multiplier if quantity_multiplier > 0 else 1,
                                 'price_unit': 0.0,
                                 'purchase_price': 0.0,
                                 'product_uom': product_run_charge.uom_id.id,
                                 'decoration_group_id': decoration_group_id.id,
                                 'at_least_one_decoration_recalculate': decoration.id,
                                 })
        if decoration.sale_order_line_id.product_id.product_sku:
            created_record.name = decoration_group_id.group_sequence  + ' - ' + decoration.sale_order_line_id.product_id.product_sku + ' - Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - ' + str(decoration.imprint_location and decoration.imprint_location.name or False) +  ' - ' + decoration_name
            created_record.name_second = decoration.sale_order_line_id.product_id.product_sku + ' - Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - '+ decoration_name
        else:
            created_record.name = decoration_group_id.group_sequence  + ' - Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - ' + str(decoration.imprint_location and decoration.imprint_location.name or False) +  ' - ' + decoration_name
            created_record.name_second = ' Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - '+ decoration_name
        created_record = self.env['sale.order.line.decoration.group.charge.line'].create(
                                        {'product_id': product_pms_patching.id,
                                         'product_uom_qty': number_of_codes,
                                         'price_unit': 0.00,
                                         'purchase_price': 0.00,
                                         'product_uom': product_pms_patching.uom_id.id,
                                         'decoration_group_id': decoration_group_id.id,
                                         'at_least_one_decoration_recalculate': decoration.id,
                                         })
        if decoration.sale_order_line_id.product_id.product_sku:
            created_record.name = decoration_group_id.group_sequence  + ' - ' + decoration.sale_order_line_id.product_id.product_sku + ' - PMS Matching - ' + str(decoration.imprint_method and decoration.imprint_method.name or False) +  ' - ' + str(decoration.imprint_location and decoration.imprint_location.name or False) +  ' - ' + decoration_name
            created_record.name_second = decoration.sale_order_line_id.product_id.product_sku + ' - PMS Matching - ' + str(decoration.imprint_location and decoration.imprint_location.name or False) +  ' - '+ decoration_name
        else:
            created_record.name = decoration_group_id.group_sequence  + ' - PMS Matching - ' + str(decoration.imprint_method and decoration.imprint_method.name or False) + ' - ' + str(decoration.imprint_location and decoration.imprint_location.name or False) +  ' - ' + decoration_name
            created_record.name_second = 'PMS Matching - ' + str(decoration.imprint_location and decoration.imprint_location.name or False) + ' - '+ decoration_name

    @api.multi
    def create_decoration_group(self):
        self.ensure_one()
        decoration_ids = []
        presets = []
        for decoration in self.decorations:
            if decoration.select:
                decoration_ids.append(decoration.id)
        self.clear_all_selection_for_sale_order()
        same_decoration_group = True
        if not decoration_ids:
            raise UserError(_('Please select at least one decoration line for creating decoration group.'))
        for decoration in self.env['sale.order.line.decoration'].browse(decoration_ids):
            same_decoration_group = same_decoration_group and decoration.decoration_group_compare(self.env['sale.order.line.decoration'].browse(decoration_ids)[0])
            if decoration.sale_order_line_id.product_id.present_decoration:
                presets.append(decoration.customer_art.id)
        if presets and len(presets) > 1 and len(presets) == len(set(presets)):
            raise UserError(_('Decoration details for pre-set decoration items cannot be changed.'))    
        if not same_decoration_group:
            raise UserError(_('Following fields must be set and same for creating the Decoration Group \n\n - Imprint Method \n - Finisher'))
        else:
            if self.env['sale.order.line.decoration'].browse(decoration_ids):
                po_reorder_ref = []
                so_reorder_ref = []
                is_reorder = False
                for decoration in self.env['sale.order.line.decoration'].browse(decoration_ids):
                    if decoration.decoration_group_id:
                        if len(decoration.decoration_group_id.decorations) <= 1:
                            decoration.decoration_group_id.unlink()
                        else:
                            decoration.decoration_group_id.charge_lines.unlink()
                            decoration.decoration_group_id = False
                    if decoration.is_reorder:
                        po_reorder_ref.append(decoration.reorder_ref_po)
                        so_reorder_ref.append(decoration.reorder_ref)
                        is_reorder = True
                reorder_ref_po = False
                reorder_ref = False
                def is_list_has_all_values_similar(list):
                    for item in list[1:]:
                        if item != list[0]:
                            return False
                    return True
                if po_reorder_ref:
                    if is_list_has_all_values_similar(po_reorder_ref):
                        reorder_ref_po = po_reorder_ref[0]
                if so_reorder_ref:
                    if is_list_has_all_values_similar(so_reorder_ref):
                        reorder_ref = so_reorder_ref[0]
                if len(decoration_ids) == len(self.decorations):
                    self.decoration_group_ids.unlink()
                decoration_group = False
                if len(self.decoration_group_ids) > 0:
                    if len(self.decoration_group_ids) != 1:
                        if len(self.decoration_group_ids) != self.decoration_group_ids.sorted(key=lambda r:r.sequence_id)[len(self.decoration_group_ids)-1].sequence_id:
                            if self.decoration_group_ids.sorted(key=lambda r:r.sequence_id)[0].sequence_id == 1:
                                index = 0
                                for decoration_group_id in self.decoration_group_ids.sorted(key=lambda r:r.sequence_id):
                                    if decoration_group_id.sequence_id + 1 != self.decoration_group_ids.sorted(key=lambda r:r.sequence_id)[index + 1].sequence_id:
                                        decoration_group = self.env['sale.order.line.decoration.group'].create({'order_id': self.id,'group_sequence': 'DG'+str(decoration_group_id.sequence_id + 1),'group_id': 'Decoration Group '+str(decoration_group_id.sequence_id + 1),'sequence_id':decoration_group_id.sequence_id + 1, 'reorder_ref': reorder_ref, 'is_reorder': is_reorder, 'reorder_ref_po': reorder_ref_po})     
                                        break
                                    index += 1
                            else:
                                decoration_group = self.env['sale.order.line.decoration.group'].create({'order_id': self.id,'group_sequence': 'DG1','group_id': 'Decoration Group 1','sequence_id':1, 'reorder_ref': reorder_ref, 'is_reorder': is_reorder, 'reorder_ref_po': reorder_ref_po})
                        else:
                            if (self.decoration_group_ids.sorted(key=lambda r:r.sequence_id)[len(self.decoration_group_ids)-1].sequence_id + 1) == self.env['sale.order.line.decoration'].browse(decoration_ids)[0].sale_order_line_id.order_id.decoration_group_sequence_id:
                                decoration_group = self.env['sale.order.line.decoration.group'].create({'order_id': self.id,'group_sequence': 'DG'+str(self.env['sale.order.line.decoration'].browse(decoration_ids)[0].sale_order_line_id.order_id.decoration_group_sequence_id),'group_id': 'Decoration Group '+str(self.env['sale.order.line.decoration'].browse(decoration_ids)[0].sale_order_line_id.order_id.decoration_group_sequence_id),'sequence_id':self.env['sale.order.line.decoration'].browse(decoration_ids)[0].sale_order_line_id.order_id.decoration_group_sequence_id, 'reorder_ref': reorder_ref, 'is_reorder': is_reorder, 'reorder_ref_po': reorder_ref_po})     
                                self.env['sale.order.line.decoration'].browse(decoration_ids)[0].sale_order_line_id.order_id.decoration_group_sequence_id += 1
                            else:
                                decoration_group = self.env['sale.order.line.decoration.group'].create({'order_id': self.id,'group_sequence': 'DG'+str(self.decoration_group_ids.sorted(key=lambda r:r.sequence_id)[len(self.decoration_group_ids)-1].sequence_id + 1),'group_id': 'Decoration Group '+str(self.decoration_group_ids.sorted(key=lambda r:r.sequence_id)[len(self.decoration_group_ids)-1].sequence_id + 1),'sequence_id':self.decoration_group_ids.sorted(key=lambda r:r.sequence_id)[len(self.decoration_group_ids)-1].sequence_id + 1, 'reorder_ref': reorder_ref, 'is_reorder': is_reorder, 'reorder_ref_po': reorder_ref_po})
                    else:
                        if self.decoration_group_ids[0].sequence_id == 1:
                            self.env['sale.order.line.decoration'].browse(decoration_ids)[0].sale_order_line_id.order_id.decoration_group_sequence_id = 2
                            decoration_group = self.env['sale.order.line.decoration.group'].create({'order_id': self.id,'group_sequence': 'DG'+str(self.env['sale.order.line.decoration'].browse(decoration_ids)[0].sale_order_line_id.order_id.decoration_group_sequence_id),'group_id': 'Decoration Group '+str(self.env['sale.order.line.decoration'].browse(decoration_ids)[0].sale_order_line_id.order_id.decoration_group_sequence_id),'sequence_id':self.env['sale.order.line.decoration'].browse(decoration_ids)[0].sale_order_line_id.order_id.decoration_group_sequence_id, 'reorder_ref': reorder_ref, 'is_reorder': is_reorder, 'reorder_ref_po': reorder_ref_po})     
                        else:
                            decoration_group = self.env['sale.order.line.decoration.group'].create({'order_id': self.id,'group_sequence': 'DG1','group_id': 'Decoration Group 1','sequence_id':1, 'reorder_ref': reorder_ref, 'is_reorder': is_reorder, 'reorder_ref_po': reorder_ref_po})
                else:
                    if len(decoration_ids) == len(self.decorations):
                        self.env['sale.order.line.decoration'].browse(decoration_ids)[0].sale_order_line_id.order_id.decoration_group_sequence_id = 1
                        decoration_group = self.env['sale.order.line.decoration.group'].create({'order_id': self.id,'group_sequence': 'DG'+str(self.env['sale.order.line.decoration'].browse(decoration_ids)[0].sale_order_line_id.order_id.decoration_group_sequence_id),'group_id': 'Decoration Group '+str(self.env['sale.order.line.decoration'].browse(decoration_ids)[0].sale_order_line_id.order_id.decoration_group_sequence_id),'sequence_id':self.env['sale.order.line.decoration'].browse(decoration_ids)[0].sale_order_line_id.order_id.decoration_group_sequence_id, 'reorder_ref': reorder_ref, 'is_reorder': is_reorder, 'reorder_ref_po': reorder_ref_po})     
                        self.env['sale.order.line.decoration'].browse(decoration_ids)[0].sale_order_line_id.order_id.decoration_group_sequence_id += 1
                    else:
                        self.env['sale.order.line.decoration'].browse(decoration_ids)[0].sale_order_line_id.order_id.decoration_group_sequence_id = 1
                        decoration_group = self.env['sale.order.line.decoration.group'].create({'order_id': self.id,'group_sequence': 'DG'+str(self.env['sale.order.line.decoration'].browse(decoration_ids)[0].sale_order_line_id.order_id.decoration_group_sequence_id),'group_id': 'Decoration Group '+str(self.env['sale.order.line.decoration'].browse(decoration_ids)[0].sale_order_line_id.order_id.decoration_group_sequence_id),'sequence_id':self.env['sale.order.line.decoration'].browse(decoration_ids)[0].sale_order_line_id.order_id.decoration_group_sequence_id, 'reorder_ref': reorder_ref, 'is_reorder': is_reorder, 'reorder_ref_po': reorder_ref_po})     
                        self.env['sale.order.line.decoration'].browse(decoration_ids)[0].sale_order_line_id.order_id.decoration_group_sequence_id += 1
                if decoration_group:
                    for decoration in self.env['sale.order.line.decoration'].browse(decoration_ids):
                        decoration.decoration_group_id = decoration_group.id
                        if decoration.sale_order_line_id.product_id.present_decoration and not decoration.sale_order_line_id.product_id.customer_arts:
                            if not decoration_group.reorder_ref:
                                decoration_group.is_reorder = decoration.is_reorder
                                if decoration.sale_order_id.reorder_ref:
                                    decoration_group.reorder_ref = decoration.sale_order_id.reorder_ref.name
                        if decoration.sale_order_line_id.product_id.present_decoration and decoration.sale_order_line_id.product_id.customer_arts:
                            if not decoration_group.reorder_ref:
                                decoration_group.is_reorder = decoration.customer_art.is_reorder
                                decoration_group.reorder_ref = decoration.customer_art.reorder_ref
                                decoration_group.reorder_ref_po = decoration.customer_art.reorder_ref_po
                    self.with_context(decoration_ids=decoration_ids).add_art_worksheet()
                    for decoration in self.env['sale.order.line.decoration'].browse(decoration_ids):
                        if decoration.sale_order_line_id.product_id.present_decoration and decoration.art_line_id:
                            if decoration.customer_art.id in decoration.sale_order_line_id.product_id.customer_arts.ids:
                                decoration.art_line_id.spec_file = decoration.customer_art.spec_file
                                decoration.art_line_id.spec_file_att = decoration.customer_art.spec_file_att
                                decoration.art_line_id.spec_file_name = decoration.customer_art.spec_file_name
                                decoration.art_line_id.spec_file2 = decoration.customer_art.spec_file2
                                decoration.art_line_id.spec_file2_att = decoration.customer_art.spec_file2_att
                                decoration.art_line_id.spec_file2_name = decoration.customer_art.spec_file2_name
                                decoration.art_line_id.vendor_file = decoration.customer_art.vendor_file
                                decoration.art_line_id.vendor_file_att = decoration.customer_art.vendor_file_att
                                decoration.art_line_id.vendor_file_name = decoration.customer_art.vendor_file_name
                            elif not decoration.sale_order_line_id.product_id.customer_arts:
                                for finisher_decoration in decoration.sale_order_line_id.product_id.finisher_ids:
                                    if finisher_decoration.finisher_id.id == decoration.finisher.id:
                                        for product_imprint_id in finisher_decoration.product_imprint_ids:
                                            if product_imprint_id.decoration_location.id == decoration.imprint_location.id and product_imprint_id.decoration_method.id == decoration.imprint_method.id:
                                                decoration.art_line_id.spec_file = product_imprint_id.spec_file
                                                decoration.art_line_id.spec_file_att = product_imprint_id.spec_file_att
                                                decoration.art_line_id.spec_file_name = product_imprint_id.spec_file_name
                                                decoration.art_line_id.spec_file2 = product_imprint_id.spec_file2
                                                decoration.art_line_id.spec_file2_att = product_imprint_id.spec_file2_att
                                                decoration.art_line_id.spec_file2_name = product_imprint_id.spec_file2_name
                                                decoration.art_line_id.vendor_file = product_imprint_id.vendor_file
                                                decoration.art_line_id.vendor_file_att = product_imprint_id.vendor_file_att
                                                decoration.art_line_id.vendor_file_name = product_imprint_id.vendor_file_name
                else:
                    raise UserError(_('Decoration group could not be created.'))

    @api.multi
    def calculate_decoration_group_charges(self):
        self.ensure_one()
        product_setup_sale_price = self.env.ref('pinnacle_sales.product_product_setup_sale_price')
        product_run_charge = self.env.ref('pinnacle_sales.product_product_run_charge')
        product_pms_patching = self.env.ref('pinnacle_sales.product_product_pms_patching')
        if not product_setup_sale_price:
            raise UserError(_('Setup Sale Price Product Not Found'))
        if not product_run_charge:
            raise UserError(_('Run Charge Product Not Found'))
        if not product_pms_patching:
            raise UserError(_('PMS Matching Product Not Found'))
        not_assigned_group_ids = []
        for decoration_group_id in self.decoration_group_ids:
            if not decoration_group_id.decorations:
                not_assigned_group_ids.append(decoration_group_id.id)
        self.env['sale.order.line.decoration.group'].browse(not_assigned_group_ids).unlink()               
        decoration_group_ids = []
        for decoration_group_id in self.decoration_group_ids:
            if decoration_group_id.select:
                decoration_group_ids.append(decoration_group_id.id)
        self.clear_all_selection_for_sale_order()
        if not decoration_group_ids:
            raise UserError(_('Please select at least one decoration group line to calculate charges.'))
        for decoration_group_id in self.env['sale.order.line.decoration.group'].browse(decoration_group_ids):
            decoration_group_id.charge_lines.unlink()
            groups = []
            index = 0
            while index < len(decoration_group_id.decorations):
                decoration = decoration_group_id.decorations[index]
                already_grouped = False
                for group in groups:
                    if decoration.id in group:
                        already_grouped = True
                if already_grouped:
                    index += 1
                    continue
                decoration_group = []
                for second_decoration in decoration_group_id.decorations:
                    if decoration.sale_order_line_id.product_id.product_tmpl_id.id == second_decoration.sale_order_line_id.product_id.product_tmpl_id.id and decoration.decoration_group_compare_for_calulating_charges(second_decoration):
                        decoration_group.append(second_decoration.id)
                if decoration_group:
                    groups.append(decoration_group)
                index += 1
            for group in groups:
                total_qty = 0
                decoration_name = False
                for decoration in self.env['sale.order.line.decoration'].browse(group):
                    total_qty += decoration.sale_order_line_id.product_uom_qty
                    if not decoration_name:
                        decoration_name = decoration.decoration_sequence
                    else:
                        decoration_name += ','+decoration.decoration_sequence 
                decoration = self.env['sale.order.line.decoration'].browse(group)[0]
                if decoration:
                    finisher_decorations = decoration.sale_order_line_id.product_id.finisher_ids.search(['&',('product_id','=',decoration.sale_order_line_id.product_id.product_tmpl_id.id),('finisher_id','=',decoration.finisher.id)])
                    ltm_policy_applied = False
                    ltm_policy_price_unit = 0.00
                    ltm_policy_purchase_price = 0.00 
                    # LTM Policy Calculation For Set-Up Price Start
                    if decoration.sale_order_line_id.change_supplier.product_vendor_tier_ids and decoration.sale_order_line_id.change_supplier and ((decoration.sale_order_line_id.change_supplier.moq and decoration.sale_order_line_id.change_supplier.moq != 'not_allowed') or (decoration.sale_order_line_id.change_supplier.o_moq and decoration.sale_order_line_id.change_supplier.o_moq != 'not_allowed')):
                        if decoration.sale_order_line_id.change_supplier.o_ltm_policy_id:
                            if decoration.sale_order_line_id.change_supplier.o_moq in ['half_column', 'other']:
                                if (decoration.sale_order_line_id.change_supplier.product_vendor_tier_ids.sorted(key=lambda r:r.min_qty)[0].min_qty) > decoration.sale_order_line_id.product_uom_qty:
                                    if decoration.sale_order_line_id.change_supplier.o_setup_cost == 'other':
                                        ltm_policy_purchase_price = decoration.sale_order_line_id.change_supplier.o_other_setup_cost
                                        ltm_policy_applied = True
                                    if decoration.sale_order_line_id.change_supplier.o_setup_price == 'other':
                                        ltm_policy_price_unit = decoration.sale_order_line_id.change_supplier.o_other_setup_price 
                                        ltm_policy_applied = True
                        else:
                            if decoration.sale_order_line_id.change_supplier.moq in ['half_column', 'other']:
                                if (decoration.sale_order_line_id.change_supplier.product_vendor_tier_ids.sorted(key=lambda r:r.min_qty)[0].min_qty) > decoration.sale_order_line_id.product_uom_qty:
                                    if decoration.sale_order_line_id.change_supplier.setup_cost == 'other':
                                        ltm_policy_purchase_price = decoration.sale_order_line_id.change_supplier.other_setup_cost
                                        ltm_policy_applied = True
                                    if decoration.sale_order_line_id.change_supplier.setup_price == 'other':
                                        ltm_policy_price_unit = decoration.sale_order_line_id.change_supplier.other_setup_price 
                                        ltm_policy_applied = True
                    # LTM Policy Calculation For Set-Up Price End
                    if finisher_decorations or (decoration.finisher and decoration.finisher.finisher_ids):
                        finisher_finishing_types = False
                        if finisher_decorations:
                            finisher_decoration = finisher_decorations[0]
                            finisher_finishing_types = finisher_decoration.product_imprint_ids.search(['&',('product_finisher_id','=',finisher_decoration.id),'&',('decoration_location','=',decoration.imprint_location.id),('decoration_method','=',decoration.imprint_method.id)])
                        pull_charges_from_vendor = False
                        if not finisher_finishing_types:
                            # Pull Charges From Vendor If They Are Not Available At Product
                            if decoration.finisher:
                                pull_charges_from_vendor = True
                                finisher_finishing_types = decoration.finisher.finisher_ids.search(['&',('partner_id','=',decoration.finisher.id),'&',('decoration_location','=',decoration.imprint_location.id),('decoration_method','=',decoration.imprint_method.id)])
                        if not finisher_finishing_types:
                            # Pull Charges From Vendor By Only Cosidering Decoration Method
                            if decoration.finisher and decoration.sale_order_line_id.product_id and decoration.sale_order_line_id.product_id.seller_ids:
                                vendor_ids = [seller_id.name and seller_id.name.id for seller_id in decoration.sale_order_line_id.product_id.seller_ids]
                                if decoration.finisher.id in vendor_ids:
                                    pull_charges_from_vendor = True
                                    finisher_finishing_types = decoration.finisher.finisher_ids.search(['&',('partner_id','=',decoration.finisher.id),('decoration_method','=',decoration.imprint_method.id)]) 
                        if finisher_finishing_types:
                            finisher_finishing_type = finisher_finishing_types[0] 
                            if not ltm_policy_applied:
                                product_setup_sale_price.list_price = finisher_finishing_type.setup_sale_price
                                created_record = self.env['sale.order.line.decoration.group.charge.line'].create(
                                            {'product_id': product_setup_sale_price.id,
                                             'product_uom_qty': len(decoration.stock_color.ids) + len(decoration.pms_code.ids),
                                             'price_unit': finisher_finishing_type.setup_sale_price,
                                             'purchase_price': finisher_finishing_type.setup_fee,
                                             'product_uom': product_setup_sale_price.uom_id.id,
                                             'decoration_group_id': decoration_group_id.id,
                                             'at_least_one_decoration_recalculate': decoration.id,
                                             })
                                if decoration.sale_order_line_id.product_id.product_sku:
                                    created_record.name = decoration_group_id.group_sequence + ' - ' + decoration.sale_order_line_id.product_id.product_sku + ' - Setup - ' + str(decoration.imprint_method and decoration.imprint_method.name or False) +  ' - ' + str(decoration.imprint_location and decoration.imprint_location.name or False) +  ' - ' + decoration_name
                                    created_record.name_second = decoration.sale_order_line_id.product_id.product_sku + ' - Setup - ' + str(decoration.imprint_location and decoration.imprint_location.name or False) +  ' - '+ decoration_name
                                else:
                                    created_record.name = decoration_group_id.group_sequence  + ' - Setup - ' + str(decoration.imprint_method and decoration.imprint_method.name or False) + ' - ' + str(decoration.imprint_location and decoration.imprint_location.name or False) +  ' - ' + decoration_name
                                    created_record.name_second = ' Setup - ' + str(decoration.imprint_location and decoration.imprint_location.name or False) + ' - '+ decoration_name
                            else:
                                product_setup_sale_price.list_price = ltm_policy_price_unit
                                created_record = self.env['sale.order.line.decoration.group.charge.line'].create(
                                            {'product_id': product_setup_sale_price.id,
                                             'product_uom_qty': len(decoration.stock_color.ids) + len(decoration.pms_code.ids),
                                             'price_unit': ltm_policy_price_unit,
                                             'purchase_price': ltm_policy_purchase_price,
                                             'product_uom': product_setup_sale_price.uom_id.id,
                                             'decoration_group_id': decoration_group_id.id,
                                             'at_least_one_decoration_recalculate': decoration.id,
                                             })
                                if decoration.sale_order_line_id.product_id.product_sku:
                                    created_record.name = decoration_group_id.group_sequence + ' - ' + decoration.sale_order_line_id.product_id.product_sku + ' - Setup - ' + str(decoration.imprint_method and decoration.imprint_method.name or False) +  ' - ' + str(decoration.imprint_location and decoration.imprint_location.name or False) +  ' - ' + decoration_name + ' - ' + 'LTM'
                                    created_record.name_second = decoration.sale_order_line_id.product_id.product_sku + ' - Setup - ' + str(decoration.imprint_location and decoration.imprint_location.name or False) +  ' - '+ decoration_name + ' - ' + 'LTM'
                                else:
                                    created_record.name = decoration_group_id.group_sequence  + ' - Setup - ' + str(decoration.imprint_method and decoration.imprint_method.name or False) + ' - ' + str(decoration.imprint_location and decoration.imprint_location.name or False) +  ' - ' + decoration_name + ' - ' + 'LTM'
                                    created_record.name_second = ' Setup - ' + str(decoration.imprint_location and decoration.imprint_location.name or False) + ' - '+ decoration_name + ' - ' + 'LTM'
                            if finisher_finishing_type.run_charges == 'quantity' and finisher_finishing_type.tier_catalog_qty_ids:
                                tier_catalog_qty_index = False
                                tier_catalog_qty = False
                                for tier_catalog_qty_id in finisher_finishing_type.tier_catalog_qty_ids.sorted(key=lambda r: r.quantity):
                                    if tier_catalog_qty_id.quantity <= total_qty:
                                        tier_catalog_qty_index = list(finisher_finishing_type.tier_catalog_qty_ids.sorted(key=lambda r: r.quantity)).index(tier_catalog_qty_id)
                                if tier_catalog_qty_index:
                                    tier_catalog_qty = finisher_finishing_type.tier_catalog_qty_ids.sorted(key=lambda r: r.quantity)[tier_catalog_qty_index]
                                if tier_catalog_qty:
                                    product_run_charge.list_price = tier_catalog_qty.sale_price
                                    #Need to handle code when quantity_multiplier is zero
                                    if pull_charges_from_vendor:
                                        quantity_multiplier = (len(decoration.stock_color.ids) + len(decoration.pms_code.ids))
                                    else:
                                        quantity_multiplier = (len(decoration.stock_color.ids) + len(decoration.pms_code.ids) - finisher_finishing_type.colors_included)
                                    created_record = self.env['sale.order.line.decoration.group.charge.line'].create(
                                                {'product_id': product_run_charge.id,
                                                 'product_uom_qty': total_qty,
                                                 'run_charge_multiplied': quantity_multiplier if quantity_multiplier > 0 else 1,
                                                 'price_unit': tier_catalog_qty.sale_price if quantity_multiplier > 0 else 0.00,
                                                 'purchase_price': tier_catalog_qty.cost if quantity_multiplier > 0 else 0.00,
                                                 'product_uom': product_run_charge.uom_id.id,
                                                 'decoration_group_id': decoration_group_id.id,
                                                 'at_least_one_decoration_recalculate': decoration.id,
                                                 })
                                    if decoration.sale_order_line_id.product_id.product_sku:
                                        created_record.name = decoration_group_id.group_sequence  + ' - ' + decoration.sale_order_line_id.product_id.product_sku + ' - Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - ' + str(decoration.imprint_location and decoration.imprint_location.name or False) +  ' - ' + decoration_name
                                        created_record.name_second = decoration.sale_order_line_id.product_id.product_sku + ' - Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - '+ decoration_name
                                    else:
                                        created_record.name = decoration_group_id.group_sequence  + ' - Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) + ' Color - ' + str(decoration.imprint_location and decoration.imprint_location.name or False) +  ' - ' + decoration_name
                                        created_record.name_second = ' Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) + ' Color - '+ decoration_name
                                else:
                                    tier_catalog_qty = finisher_finishing_type.tier_catalog_qty_ids.sorted(key=lambda r: r.quantity)[0]
                                    product_run_charge.list_price = tier_catalog_qty and tier_catalog_qty.sale_price or 0.0
                                    #Need to handle code when quantity_multiplier is zero
                                    if pull_charges_from_vendor:
                                        quantity_multiplier = (len(decoration.stock_color.ids) + len(decoration.pms_code.ids))
                                    else:
                                        quantity_multiplier = (len(decoration.stock_color.ids) + len(decoration.pms_code.ids) - finisher_finishing_type.colors_included)
                                    created_record = self.env['sale.order.line.decoration.group.charge.line'].create(
                                                {'product_id': product_run_charge.id,
                                                 'product_uom_qty': total_qty,
                                                 'run_charge_multiplied': quantity_multiplier if quantity_multiplier > 0 else 1,
                                                 'price_unit': tier_catalog_qty and (tier_catalog_qty.sale_price if quantity_multiplier > 0 else 0.00) or 0.0,
                                                 'purchase_price': tier_catalog_qty and (tier_catalog_qty.cost if quantity_multiplier > 0 else 0.00) or 0.0,
                                                 'product_uom': product_run_charge.uom_id.id,
                                                 'decoration_group_id': decoration_group_id.id,
                                                 'at_least_one_decoration_recalculate': decoration.id,
                                                 })
                                    if decoration.sale_order_line_id.product_id.product_sku:
                                        created_record.name = decoration_group_id.group_sequence  + ' - ' + decoration.sale_order_line_id.product_id.product_sku + ' - Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - ' + str(decoration.imprint_location and decoration.imprint_location.name or False) +  ' - ' + decoration_name
                                        created_record.name_second = decoration.sale_order_line_id.product_id.product_sku + ' - Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - '+ decoration_name
                                    else:
                                        created_record.name = decoration_group_id.group_sequence  + ' - Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - ' + str(decoration.imprint_location and decoration.imprint_location.name or False) +  ' - ' + decoration_name
                                        created_record.name_second = ' Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - '+ decoration_name
                            elif finisher_finishing_type.run_charges == 'color' and finisher_finishing_type.tier_catalog_color_ids:
                                tier_catalog_color_index = False
                                tier_catalog_color = False
                                if pull_charges_from_vendor:
                                    quantity_multiplier = (len(decoration.stock_color.ids) + len(decoration.pms_code.ids))
                                else:
                                    quantity_multiplier = (len(decoration.stock_color.ids) + len(decoration.pms_code.ids) - finisher_finishing_type.colors_included)
                                for tier_catalog_color_id in finisher_finishing_type.tier_catalog_color_ids.sorted(key=lambda r: r.quantity):
                                    if pull_charges_from_vendor:
                                        if tier_catalog_color_id.quantity <= total_qty and int(tier_catalog_color_id.color) == (len(decoration.stock_color.ids) + len(decoration.pms_code.ids)):
                                            tier_catalog_color_index = list(finisher_finishing_type.tier_catalog_color_ids.sorted(key=lambda r: r.quantity)).index(tier_catalog_color_id)
                                    else:
                                        if tier_catalog_color_id.quantity <= total_qty and int(tier_catalog_color_id.color) == (len(decoration.stock_color.ids) + len(decoration.pms_code.ids) - finisher_finishing_type.colors_included):
                                            tier_catalog_color_index = list(finisher_finishing_type.tier_catalog_color_ids.sorted(key=lambda r: r.quantity)).index(tier_catalog_color_id)
                                if tier_catalog_color_index:
                                    tier_catalog_color = finisher_finishing_type.tier_catalog_color_ids.sorted(key=lambda r: r.quantity)[tier_catalog_color_index]
                                if tier_catalog_color:
                                    product_run_charge.list_price = tier_catalog_color.sale_price
                                    created_record = self.env['sale.order.line.decoration.group.charge.line'].create(
                                                {'product_id': product_run_charge.id,
                                                 'product_uom_qty': total_qty,
                                                 'run_charge_multiplied': 1,
                                                 'price_unit': tier_catalog_color.sale_price,
                                                 'purchase_price': tier_catalog_color.cost,
                                                 'product_uom': product_run_charge.uom_id.id,
                                                 'decoration_group_id': decoration_group_id.id,
                                                 'at_least_one_decoration_recalculate': decoration.id,
                                                 })
                                    if decoration.sale_order_line_id.product_id.product_sku:
                                        created_record.name = decoration_group_id.group_sequence  + ' - ' + decoration.sale_order_line_id.product_id.product_sku + ' - Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - ' + str(decoration.imprint_location and decoration.imprint_location.name or False) +  ' - ' + decoration_name
                                        created_record.name_second = decoration.sale_order_line_id.product_id.product_sku + ' - Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - '+ decoration_name
                                    else:
                                        created_record.name = decoration_group_id.group_sequence  + ' - Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - ' + str(decoration.imprint_location and decoration.imprint_location.name or False) +  ' - ' + decoration_name
                                        created_record.name_second = ' Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - '+ decoration_name
                                else:
                                    for tier_catalog_color_id in finisher_finishing_type.tier_catalog_color_ids.sorted(key=lambda r: r.quantity):
                                        if pull_charges_from_vendor:
                                            if int(tier_catalog_color_id.color) == (len(decoration.stock_color.ids) + len(decoration.pms_code.ids)):
                                                tier_catalog_color = tier_catalog_color_id
                                                break
                                        else:
                                            if int(tier_catalog_color_id.color) == (len(decoration.stock_color.ids) + len(decoration.pms_code.ids) - finisher_finishing_type.colors_included):
                                                tier_catalog_color = tier_catalog_color_id
                                                break
                                    product_run_charge.list_price = tier_catalog_color and tier_catalog_color.sale_price or 0.0
                                    created_record = self.env['sale.order.line.decoration.group.charge.line'].create(
                                                {'product_id': product_run_charge.id,
                                                 'product_uom_qty': total_qty,
                                                 'run_charge_multiplied': 1,
                                                 'price_unit': tier_catalog_color and tier_catalog_color.sale_price or 0.0,
                                                 'purchase_price': tier_catalog_color and tier_catalog_color.cost or 0.0,
                                                 'product_uom': product_run_charge.uom_id.id,
                                                 'decoration_group_id': decoration_group_id.id,
                                                 'at_least_one_decoration_recalculate': decoration.id,
                                                 })
                                    if decoration.sale_order_line_id.product_id.product_sku:
                                        created_record.name = decoration_group_id.group_sequence  + ' - ' + decoration.sale_order_line_id.product_id.product_sku + ' - Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - ' + str(decoration.imprint_location and decoration.imprint_location.name or False) +  ' - ' + decoration_name
                                        created_record.name_second = decoration.sale_order_line_id.product_id.product_sku + ' - Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - '+ decoration_name
                                    else:
                                        created_record.name = decoration_group_id.group_sequence  + ' - Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - ' + str(decoration.imprint_location and decoration.imprint_location.name or False) +  ' - ' + decoration_name
                                        created_record.name_second = ' Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - '+ decoration_name
                            elif finisher_finishing_type.run_charges == 'teir' and finisher_finishing_type.tier_catalog_name_ids:
                                product_vendor_tier_seq = False
                                if finisher_decoration.finisher_id:
                                    for seller_id in decoration.sale_order_line_id.product_id.product_tmpl_id.seller_ids:
                                        if seller_id.name.id == finisher_decoration.finisher_id.id:
                                            if seller_id.product_vendor_tier_ids:
                                                for product_vendor_tier_id in seller_id.product_vendor_tier_ids.sorted(key=lambda r: r.min_qty):
                                                    if product_vendor_tier_id.min_qty <= total_qty:
                                                        product_vendor_tier_seq = product_vendor_tier_id.tier_sequence
                                                if not product_vendor_tier_seq:
                                                    product_vendor_tier_seq = seller_id.product_vendor_tier_ids.sorted(key=lambda r: r.min_qty)[0].tier_sequence
                                if product_vendor_tier_seq:
                                    for tier_catalog_name_id in finisher_finishing_type.tier_catalog_name_ids:
                                        if product_vendor_tier_seq == tier_catalog_name_id.tier_sequence:
                                            product_run_charge.list_price = tier_catalog_name_id.sale_price
                                            #Need to handle code when quantity_multiplier is zero
                                            if pull_charges_from_vendor:
                                                quantity_multiplier = (len(decoration.stock_color.ids) + len(decoration.pms_code.ids))
                                            else:
                                                quantity_multiplier = (len(decoration.stock_color.ids) + len(decoration.pms_code.ids) - finisher_finishing_type.colors_included)
                                            created_record = self.env['sale.order.line.decoration.group.charge.line'].create(
                                                        {'product_id': product_run_charge.id,
                                                         'product_uom_qty': total_qty  * (quantity_multiplier if quantity_multiplier > 0 else 1), 
                                                         'run_charge_multiplied': quantity_multiplier if quantity_multiplier > 0 else 1,
                                                         'price_unit': tier_catalog_name_id.sale_price if quantity_multiplier > 0 else 0.00,
                                                         'purchase_price': tier_catalog_name_id.cost if quantity_multiplier > 0 else 0.00,
                                                         'product_uom': product_run_charge.uom_id.id,
                                                         'decoration_group_id': decoration_group_id.id,
                                                         'at_least_one_decoration_recalculate': decoration.id,
                                                         })
                                            if decoration.sale_order_line_id.product_id.product_sku:
                                                created_record.name = decoration_group_id.group_sequence  + ' - ' + decoration.sale_order_line_id.product_id.product_sku + ' - Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - ' + str(decoration.imprint_location and decoration.imprint_location.name or False) +  ' - ' + decoration_name
                                                created_record.name_second = decoration.sale_order_line_id.product_id.product_sku + ' - Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - '+ decoration_name
                                            else:
                                                created_record.name = decoration_group_id.group_sequence  + ' - Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - ' + str(decoration.imprint_location and decoration.imprint_location.name or False) +  ' - ' + decoration_name
                                                created_record.name_second = ' Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - '+ decoration_name
                                else:
                                    #Need to handle code when quantity_multiplier is zero
                                    if pull_charges_from_vendor:
                                        quantity_multiplier = (len(decoration.stock_color.ids) + len(decoration.pms_code.ids))
                                    else:
                                        quantity_multiplier = (len(decoration.stock_color.ids) + len(decoration.pms_code.ids) - finisher_finishing_type.colors_included)
                                    created_record = self.env['sale.order.line.decoration.group.charge.line'].create(
                                                    {'product_id': product_run_charge.id,
                                                     'product_uom_qty': total_qty * (quantity_multiplier if quantity_multiplier > 0 else 1),
                                                     'run_charge_multiplied': quantity_multiplier if quantity_multiplier > 0 else 1,
                                                     'price_unit': 0.0,
                                                     'purchase_price': 0.0,
                                                     'product_uom': product_run_charge.uom_id.id,
                                                     'decoration_group_id': decoration_group_id.id,
                                                     'at_least_one_decoration_recalculate': decoration.id,
                                                     })
                                    if decoration.sale_order_line_id.product_id.product_sku:
                                        created_record.name = decoration_group_id.group_sequence  + ' - ' + decoration.sale_order_line_id.product_id.product_sku + ' - Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - ' + str(decoration.imprint_location and decoration.imprint_location.name or False) +  ' - ' + decoration_name
                                        created_record.name_second = decoration.sale_order_line_id.product_id.product_sku + ' - Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - '+ decoration_name
                                    else:
                                        created_record.name = decoration_group_id.group_sequence  + ' - Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - ' + str(decoration.imprint_location and decoration.imprint_location.name or False) +  ' - ' + decoration_name
                                        created_record.name_second = ' Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - '+ decoration_name
                            else:
                                #Need to handle code when quantity_multiplier is zero
                                if pull_charges_from_vendor:
                                    quantity_multiplier = (len(decoration.stock_color.ids) + len(decoration.pms_code.ids))    
                                else:
                                    quantity_multiplier = (len(decoration.stock_color.ids) + len(decoration.pms_code.ids) - finisher_finishing_type.colors_included)
                                created_record = self.env['sale.order.line.decoration.group.charge.line'].create(
                                                    {'product_id': product_run_charge.id,
                                                     'product_uom_qty': total_qty * (quantity_multiplier if quantity_multiplier > 0 else 1),
                                                     'run_charge_multiplied': quantity_multiplier if quantity_multiplier > 0 else 1,
                                                     'price_unit': 0.0,
                                                     'purchase_price': 0.0,
                                                     'product_uom': product_run_charge.uom_id.id,
                                                     'decoration_group_id': decoration_group_id.id,
                                                     'at_least_one_decoration_recalculate': decoration.id,
                                                     })
                                if decoration.sale_order_line_id.product_id.product_sku:
                                    created_record.name = decoration_group_id.group_sequence  + ' - ' + decoration.sale_order_line_id.product_id.product_sku + ' - Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - ' + str(decoration.imprint_location and decoration.imprint_location.name or False) +  ' - ' + decoration_name
                                    created_record.name_second = decoration.sale_order_line_id.product_id.product_sku + ' - Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - '+ decoration_name
                                else:
                                    created_record.name = decoration_group_id.group_sequence  + ' - Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - ' + str(decoration.imprint_location and decoration.imprint_location.name or False) +  ' - ' + decoration_name
                                    created_record.name_second = ' Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - '+ decoration_name
                            product_pms_patching.list_price = finisher_finishing_type.pms_matching_sale_price
                            created_record = self.env['sale.order.line.decoration.group.charge.line'].create(
                                        {'product_id': product_pms_patching.id,
                                         'product_uom_qty': len(decoration.pms_code.ids),
                                         'price_unit': finisher_finishing_type.pms_matching_sale_price,
                                         'purchase_price': finisher_finishing_type.pms_matching_fee,
                                         'product_uom': product_pms_patching.uom_id.id,
                                         'decoration_group_id': decoration_group_id.id,
                                         'at_least_one_decoration_recalculate': decoration.id,
                                         })
                            if decoration.sale_order_line_id.product_id.product_sku:
                                created_record.name = decoration_group_id.group_sequence  + ' - ' + decoration.sale_order_line_id.product_id.product_sku + ' - PMS Matching - ' + str(decoration.imprint_method and decoration.imprint_method.name or False) +  ' - ' + str(decoration.imprint_location and decoration.imprint_location.name or False) +  ' - ' + decoration_name
                                created_record.name_second = decoration.sale_order_line_id.product_id.product_sku + ' - PMS Matching - ' + str(decoration.imprint_location and decoration.imprint_location.name or False) +  ' - '+ decoration_name
                            else:
                                created_record.name = decoration_group_id.group_sequence  + ' - PMS Matching - ' + str(decoration.imprint_method and decoration.imprint_method.name or False) + ' - ' + str(decoration.imprint_location and decoration.imprint_location.name or False) +  ' - ' + decoration_name
                                created_record.name_second = ' PMS Matching - ' + str(decoration.imprint_location and decoration.imprint_location.name or False) + ' - '+ decoration_name
                        else:
                            if not ltm_policy_applied:
                                self.create_setup_sale_price_and_run_charge_with_zero(decoration, decoration_name, total_qty, len(decoration.stock_color.ids), len(decoration.pms_code.ids), product_setup_sale_price, ltm_policy_applied, 0.00, 0.00, product_run_charge, product_pms_patching, decoration_group_id)
                            else:
                                self.create_setup_sale_price_and_run_charge_with_zero(decoration, decoration_name, total_qty, len(decoration.stock_color.ids), len(decoration.pms_code.ids), product_setup_sale_price, ltm_policy_applied, ltm_policy_price_unit, ltm_policy_purchase_price, product_run_charge, product_pms_patching, decoration_group_id)
                    else:
                        self.create_setup_sale_price_and_run_charge_with_zero(decoration, decoration_name, total_qty, len(decoration.stock_color.ids), len(decoration.pms_code.ids),product_setup_sale_price, ltm_policy_applied, ltm_policy_price_unit, ltm_policy_purchase_price, product_run_charge, product_pms_patching, decoration_group_id)

    @api.multi
    def basic_calculations(self):
        context_active_ids = []
        self.ensure_one()
        finisher_id = False
        for sale_order_line in self.order_line:
            if sale_order_line.select:
                finisher_id = sale_order_line.finisher_id and sale_order_line.finisher_id.id or False
                context_active_ids.append(sale_order_line.id)
        if len(context_active_ids) == 0:
            raise UserError(_('Please select at least one line.'))
        return finisher_id, context_active_ids

    @api.multi
    def edit_sales_order_line(self):
        self.ensure_one()
        finisher_id, context_active_ids = self.basic_calculations()
        new_context = dict(self.env.context).copy()
        new_context.update({'sale_order': self.env.context.get('active_id', False),
                            'active_ids': context_active_ids})

        blank_line = False
        normal_line = False
        for sale_order_line in self.order_line:
            if sale_order_line.select:
                if sale_order_line.blank_line or sale_order_line.rush_shipping_id.id:
                    blank_line = True
                else:
                    normal_line = True
        if blank_line and normal_line:
            raise UserError(_('Blank or Rush lines cannot be edited with other order lines.'))
        self.clear_all_selection_for_sale_order()
        return {'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'target': 'new',
                'view_id': self.env.ref('pinnacle_sales.sale_order_wizard_change_form_blank_rush' if blank_line else 'pinnacle_sales.sale_order_wizard_change_form').id,
                'res_model': 'sale.order.wizard.change',
                'name': 'Edit Sales Order',
                'context': new_context,
                }

    @api.multi
    def split_sales_order_line(self):
        try:
            self.ensure_one()
            finisher_id, context_active_ids = self.basic_calculations()
            self.clear_all_selection_for_sale_order()
            if len(context_active_ids) == 1:
                new_context = dict(self.env.context).copy()
                new_context.update({'active_id': context_active_ids[0],
                                    'active_ids': context_active_ids})
                form_view_id = self.env.ref(
                    'pinnacle_sales.product_variant_split_form_view')
                return {'type': 'ir.actions.act_window',
                        'view_mode': 'form',
                        'target': 'new',
                        'view_id': form_view_id.id,
                        'res_model': 'product.variant.split',
                        'name': 'Split Quantity',
                        'context': new_context,
                        }
            else:
                raise UserError(_('Please select only one line at a time to split.'))
        finally:
            self.clear_all_selection_for_sale_order()

    @api.multi
    def add_decoration(self):
        try:
            self.ensure_one()
            context_active_ids = []
            can_decorate = True
            for sale_order_line in self.decoration_line:
                if sale_order_line.select_decoration_line:
                    if not sale_order_line.added_decoration and not sale_order_line.is_decoration_disable:
                        context_active_ids.append(sale_order_line.id)
                        if not sale_order_line.product_id.can_decorate or sale_order_line.product_id.type == 'service':
                            can_decorate = False
                    else:
                        raise UserError(_('Please select all lines without any decoration to add decoration.'))
            self.clear_all_selection_for_sale_order()
            if not can_decorate:
                raise UserError(_('Please select products that can be' +
                                  ' decorated and that are not type of' +
                                  ' service.'))
            elif len(context_active_ids) >= 1 and self:
                new_context = dict(self.env.context).copy()
                new_context.update({'active_id': context_active_ids[0],
                                    'active_ids': context_active_ids,
                                    'sale_order_lines': context_active_ids,
                                    'order_id': self.id,
                                    'readonly_by_pass': True})
                form_view_id = self.env.ref(
                    'pinnacle_sales.sale_order_add_decoration_form_view')
                return {'type': 'ir.actions.act_window',
                        'view_mode': 'form',
                        'target': 'new',
                        'view_id': form_view_id.id,
                        'res_model': 'sale.order.add.decoration',
                        'name': 'Add Sale Order Line Decoration',
                        'context': new_context,
                        }
            else:
                raise UserError(_('Please select a line to add decoration.'))
        finally:
            self.clear_all_selection_for_sale_order()

    @api.multi
    def edit_decoration(self):
        try:
            self.ensure_one()
            context_active_ids = []
            can_decorate = True
            for sale_order_line in self.decoration_line:
                if sale_order_line.select_decoration_line:
                    if sale_order_line.added_decoration or sale_order_line.is_decoration_disable:
                        context_active_ids.append(sale_order_line.id)
                        if not sale_order_line.product_id.can_decorate or sale_order_line.product_id.type == 'service':
                            can_decorate = False
                    else:
                        raise UserError(_('To edit decoration, please select lines that have already been decorated.'))
            self.clear_all_selection_for_sale_order()
            if not can_decorate:
                raise UserError(_('Please select products that can be' +
                                  ' decorated and that are not type of' +
                                  ' service.'))
            elif len(context_active_ids) > 0:
                new_context = dict(self.env.context).copy()
                new_context.update({'active_id': context_active_ids[0],
                                    'active_ids': context_active_ids,
                                    'sale_order_lines': context_active_ids,
                                    'order_id': self.id,
                                    'readonly_by_pass': True})
                if len(context_active_ids) == 1:
                    self.env['sale.order.edit.decoration'].search([]).unlink()
                    sale_order_line = self.env['sale.order.line']. browse(context_active_ids)
                    form_view_id = self.env.ref(
                        'pinnacle_sales.sale_order_edit_decoration_form_view')
                    new_context.update({'is_decoration_disable': sale_order_line.is_decoration_disable})
                    return {'type': 'ir.actions.act_window',
                            'view_mode': 'form',
                            'target': 'new', 'view_id': form_view_id.id,
                            'res_model': 'sale.order.edit.decoration',
                            'name': 'Edit Sale Order Line Decoration',
                            'context': new_context,
                            }
                else:
                    decoration_lines_same = False
                    sale_order_lines = self.env['sale.order.line']. browse(context_active_ids)
                    is_decoration_disable_same = True
                    for sale_order_line in sale_order_lines:
                        if sale_order_lines[0].is_decoration_disable != sale_order_line.is_decoration_disable:
                            is_decoration_disable_same = False
                    for sale_order_line in sale_order_lines:
                        decoration_lines_same = self.env['sale.order.line'].browse(context_active_ids)[0].decorations.decoration_compare(sale_order_line.decorations)   
                    if not decoration_lines_same or not is_decoration_disable_same:
                        raise UserError(_('Please select all editable ' +
                                          'lines with similar ' +
                                          'decorations to edit their\'s' +
                                          ' decorations.'))
                    form_view_id = self.env.ref(
                        'pinnacle_sales.' +
                        'sale_order_edit_multi_decoration_form_view')
                    new_context.update({'is_decoration_disable': sale_order_lines[0].is_decoration_disable})
                    return {'type': 'ir.actions.act_window',
                            'view_mode': 'form',
                            'target': 'new',
                            'view_id': form_view_id.id,
                            'res_model':
                            'sale.order.edit.multi.decoration',
                            'name':
                            'Edit Multi Sale Order Lines Decoration',
                            'context': new_context,
                            }
            else:
                raise UserError(_('Please select at least one line with' +
                                  ' any decoration to edit it\'s decoration.'))
        finally:
            self.clear_all_selection_for_sale_order()

    @api.multi
    def calculate_customer_type_for_artworksheet(self):
        self.ensure_one()
        customer_type = False
        customer_type = self.partner_id.customer_type
        if self.order_type == 'fo_non_web' and self.partner_id.customer_type == 'new':
            customer_type = 'new'
        if self.order_type == 'fo_non_web' and self.partner_id.customer_type == 'program':
            customer_type = 'program'
        if self.order_type == 'repeat_non_web' and self.partner_id.customer_type == 'current':
            customer_type = 'current'
        if self.order_type == 'repeat_non_web' and self.partner_id.customer_type == 'program':
            customer_type = 'program'
        if self.order_type == 'fo_web_order' and self.partner_id.customer_type == 'new':
            customer_type = 'web_new'
        if self.order_type == 'fo_web_order' and self.partner_id.customer_type == 'program':
            customer_type = 'program'
        if self.order_type == 'repeat_web_order' and self.partner_id.customer_type == 'current':
            customer_type = 'web_current_customer'
        if self.order_type == 'repeat_web_order' and self.partner_id.customer_type == 'program':
            customer_type = 'program'
        if self.order_type == 'spec' and self.partner_id.customer_type == 'new':
            customer_type = 'new'
        if self.order_type == 'spec' and self.partner_id.customer_type == 'current':
            customer_type = 'current'
        if self.order_type == 'spec' and self.partner_id.customer_type == 'program':
            customer_type = 'program'
        return customer_type

    @api.multi
    def add_art_worksheet(self):
        self.ensure_one()
        context = self.env.context.copy()
        context['cfs_attachment'] = True
        context_active_ids = []
        if self._context.get('decoration_ids', []):
            context_active_ids = self._context.get('decoration_ids', [])
        art_obj = self.env['art.production']
        art_line_obj = self.env['art.production.line']
        for decoration in self.env['sale.order.line.decoration'].browse(context_active_ids):
            if decoration.finisher and not decoration.art_line_id and not decoration.art_id:
                artworksheet_already_exist = False
                artworksheet_record = False
                for artworksheet in self.artworksheets:
                    if decoration.finisher.id == artworksheet.vendor_id.id:
                        artworksheet_already_exist = artworksheet
                if not artworksheet_already_exist:
                    status = 'new_order'
                    if self.state == 'quote_draft':
                        status = 'virtual_spec'
                    customer_type = self.calculate_customer_type_for_artworksheet()
                    new_artworksheet = art_obj.create({
                                                        'sale_id': self.id,
                                                        'partner_id': self.partner_id.id,
                                                        'customer_type': customer_type,
                                                        'status': status,
                                                        'vendor_id': decoration.finisher.id,
                                                        'acc_manager_id': self.user_id and self.user_id.id or False,
                                                        'program_id': self.program_id and self.program_id.id or False,
                                                        'internal_contact_id': self.env.uid,
                                                        'artworksheet_number_for_sale_order': self.number_of_artworksheets_count + 1
                                                    })
                    if new_artworksheet:
                        self.number_of_artworksheets_count += 1
                    followers = []
                    if self.user_id:
                        followers.append(self.user_id.partner_id.id)
                    if self.aam_id:
                        for aam_id in self.aam_id:
                            followers.append(aam_id.partner_id.id)
                    new_artworksheet.message_subscribe(followers)
                    artworksheet_record = new_artworksheet
                else:
                    artworksheet_record = artworksheet_already_exist
                item_dimensions, description, product_sku, product_id, product_uom_qty, vendor_link = decoration.sale_order_line_id.calculate_itemdimensions_and_get_sale_order_line_info_for_artworksheet()
                product_dict = {
                                'quantity': product_uom_qty,
                                'art_id': artworksheet_record.id,
                                'sale_line_id': decoration.sale_order_line_id.id,
                                'item_dimensions': item_dimensions,
                                'description': description,
                                'product_sku': product_sku,
                                'product_id': product_id,
                                'web_link': vendor_link
                                }
                new_artworksheet_line = art_line_obj.create(product_dict)
                decoration.write({'art_line_id': new_artworksheet_line.id, 'art_id': artworksheet_record.id})
                art_files_extension = False
                if decoration.customer_art and decoration.customer_art.art_filename:
                    try:
                        filename = decoration.customer_art.art_filename.split(".")[-1].lower()
                        art_files_extension = str(filename)
                    except:
                        pass
                if art_files_extension and art_files_extension not in ["eps", "ai"]:
                    art_type = 'raster_recreate'
                    artworksheet_record.write({'art_type': art_type})
                if not artworksheet_already_exist:
                    artworksheet_record.write({'job': len(self.artworksheets), 'art_sequence': self.name + ' - ' + str(len(self.artworksheets))})
                self.artworksheets.write({'job1': len(self.artworksheets)})

    @api.multi
    def show_art_worksheet(self):
        context = self.env.context.copy()
        imd = self.env['ir.model.data']
        artworksheet_ids = []
        for art_line in self.art_line:
            if art_line.select_art_line and art_line.art_id:
                artworksheet_ids.append(art_line.art_id.id)
        self.clear_all_selection_for_sale_order()
        action = imd.xmlid_to_object('pinnacle_sales.art_production_action1')
        tree_view_id = imd.xmlid_to_res_id('pinnacle_sales.view_art_production_tree')
        form_view_id = imd.xmlid_to_res_id('pinnacle_sales.art_production_form_view1')
        context['cfs_attachment'] = True
        if len(artworksheet_ids) == 0:
            raise UserError(_('Please select at least one line.'))
        if len(artworksheet_ids) == 1:
            view_mode = 'form'
            views = [[form_view_id, 'form']]
            res_id = artworksheet_ids[0]
        else:
            view_mode = 'tree, form'
            views = [[tree_view_id, 'tree'], [form_view_id, 'form']]
            res_id = False
        return {
            'name': action.name,
            'help': action.help,
            'type': action.type,
            'views': views,
            'target': action.target,
            'res_model': action.res_model,
            'context': context,
            'view_type': 'form',
            'view_mode': view_mode,
            'domain': [('id', 'in', artworksheet_ids)],
            'res_id': res_id
        }

    @api.multi
    def copy(self, default=None):
        self.ensure_one()
        if self.state == 'cancel':
            raise UserError(_('You can\'t duplicate a cancelled sales order.'))
        default = default or {}
        if not self._context.get('split_sale_order',False):
            default['reorder_ref'] = self.id
        if self.partner_id:
            program_id = False
            channel_id = False
            if self.partner_id.parent_id:
                program_id = self.partner_id.parent_id.program_id.id
            else:
                program_id = self.partner_id.program_id.id
            if self.partner_id.parent_id:
                channel_id = self.partner_id.parent_id.channel_id.id
            else:
                channel_id = self.partner_id.channel_id.id
            default['user_id'] = self.partner_id.user_id.id
            default['program_id'] = program_id
            default['channel_id'] = channel_id
            default['send_email_of_sale_invoice'] = self.partner_id.send_email_of_sale_invoice
        result = super(SaleOrder, self).copy(default)
        if result and not self._context.get('split_sale_order',False):
            new_context = dict(self.env.context).copy()
            new_context.update({'art_copy': True})
            self.env.context = new_context
            delivery_carrier_products = []
            for delivery_carrier in self.env['delivery.carrier'].search([]):
                if delivery_carrier.product_id:
                    delivery_carrier_products.append(delivery_carrier.product_id.id)
            delivery_carrier_products = list(set(delivery_carrier_products))
            product_rush_policy = self.env.ref('pinnacle_sales.product_product_rush_policy_price_update_change')
            charge_lines_products = []
            if not product_rush_policy:
                raise UserError(_('Rush Policy Product Not Found'))
            order_line_mapping = {}
            for order_line in self.order_line:
                if order_line.product_id.id not in delivery_carrier_products and order_line.product_id.id != product_rush_policy.id:
                    order_line_copied = order_line.with_context(sale_order_copy= True).copy({'order_id':result.id})
                    order_line_mapping[order_line.id] = order_line_copied.id                
            decoration_group_id_mapping = {}
            for decoration_group_id in self.decoration_group_ids:
                ref_po = ''
                if self.purchase_orders:
                    for po in self.purchase_orders:
                        if po.for_decoration:
                            for decoration in po.decorations:
                                if decoration.decoration_group_id.group_sequence == decoration_group_id.group_sequence:
                                    ref_po = po.name
                decoration_group_id_copied = decoration_group_id.copy({'order_id': result.id, 'is_reorder': True, 'reorder_ref': self.name, 'reorder_ref_po': ref_po})
                decoration_group_id_mapping[decoration_group_id.id] = decoration_group_id_copied.id
            decoration_mapping = {}
            for order_line in self.order_line:
                for decoration in order_line.decorations:
                    if order_line.id in order_line_mapping:
                        if decoration.decoration_group_id and decoration.decoration_group_id.id in decoration_group_id_mapping:
                            decoration_copied = decoration.copy({'sale_order_line_id': order_line_mapping[order_line.id], 'decoration_group_id': decoration_group_id_mapping[decoration.decoration_group_id.id]})
                        else:
                            decoration_copied = decoration.copy({'sale_order_line_id': order_line_mapping[order_line.id]})
                        decoration_mapping[decoration.id] = decoration_copied.id
            charge_lines_mapping = {}
            for decoration_group_id in self.decoration_group_ids:
                for charge_line in decoration_group_id.charge_lines:
                    if charge_line.decoration_group_id and charge_line.decoration_group_id.id in decoration_group_id_mapping:
                        if charge_line.at_least_one_decoration_recalculate and charge_line.at_least_one_decoration_recalculate.id in decoration_mapping:
                            charge_line_copied = charge_line.copy({'decoration_group_id': decoration_group_id_mapping[charge_line.decoration_group_id.id], 'at_least_one_decoration_recalculate': decoration_mapping[charge_line.at_least_one_decoration_recalculate.id]})
                        else:
                            charge_line_copied = charge_line.copy({'decoration_group_id': decoration_group_id_mapping[charge_line.decoration_group_id.id]})
                        charge_lines_mapping[charge_line.id] = charge_line_copied.id
            for order_line in self.order_line:
                if order_line.id in order_line_mapping:
                    if order_line.decoration_charge_group_id and order_line.decoration_charge_group_id.id in charge_lines_mapping:                    
                        self.env['sale.order.line'].browse([order_line_mapping[order_line.id]]).decoration_charge_group_id = charge_lines_mapping[order_line.decoration_charge_group_id.id]
            # Calculating Decoration Reorder Charges
            for decoration_group_id in result.decoration_group_ids:
                decoration_group_id.recalculatea_reorder_setup_charges()
                for charge_line in decoration_group_id.charge_lines:
                    if charge_line.sale_order_lines:
                        charge_line.select = True
                decoration_group_id.with_context(sale_order_copy= True).write_with_add_charges_order_lines({})
            art_mapping = {}
            art_line_mapping = {}
            for order_line in self.order_line:
                if order_line.id in order_line_mapping:
                    for decoration in order_line.decorations:
                        if decoration.art_id and decoration.art_id.id not in art_mapping:
                            art_copied = decoration.art_id.with_context(art_copy=True).copy({'status': 'reorder','sale_id': result.id,'reorder_sale_id': self.id})
                            art_mapping[decoration.art_id.id] = art_copied.id
                            for art_line in decoration.art_id.art_lines:
                                if art_line.id not in art_line_mapping:
                                    if art_line.art_id and art_line.art_id.id in art_mapping:
                                        art_line_copied = art_line.copy({'art_id': art_mapping[art_line.art_id.id],'sale_line_id': order_line_mapping[art_line.sale_line_id.id]})
                                    else:
                                        art_line_copied = False
                                    if art_line_copied:
                                        art_line_mapping[art_line.id] = art_line_copied.id
                            for product_id in decoration.art_id.product_ids:
                                if product_id.id not in art_line_mapping:
                                    if product_id.art_id and product_id.art_id.id in art_mapping:
                                        art_line_copied = product_id.copy({'art_id': art_mapping[product_id.art_id.id],'sale_line_id': order_line_mapping[product_id.sale_line_id.id]})
                                    else:
                                        art_line_copied = False
                                    if art_line_copied:
                                        art_line_mapping[product_id.id] = art_line_copied.id
            for order_line in self.order_line:
                if order_line.id in order_line_mapping:
                    for decoration in order_line.decorations:
                        if decoration.art_line_id and decoration.art_line_id.id not in art_line_mapping:
                            if decoration.art_id and decoration.art_id.id in art_mapping and order_line.id in order_line_mapping:
                                art_line_copied = decoration.art_line_id.copy({'art_id': art_mapping[decoration.art_id.id],'sale_line_id': order_line_mapping[decoration.art_line_id.sale_line_id.id]})
                            else:
                                art_line_copied = False
                            if art_line_copied:
                                art_line_mapping[decoration.art_line_id.id] = art_line_copied.id
            art_note_mapping = {}
            for order_line in self.order_line:
                if order_line.id in order_line_mapping:
                    for decoration in order_line.decorations:
                        if decoration.note_id:
                            art_note_copied = decoration.note_id.copy()
                            art_note_mapping[decoration.note_id.id] = art_note_copied.id
            for order_line in self.order_line:
                for decoration in order_line.decorations:
                    if decoration.id in decoration_mapping:
                        if decoration.art_id and decoration.art_id.id in art_mapping:                    
                            self.env['sale.order.line.decoration'].browse([decoration_mapping[decoration.id]]).art_id = art_mapping[decoration.art_id.id]
                        if decoration.art_line_id and decoration.art_line_id.id in art_line_mapping:                    
                            self.env['sale.order.line.decoration'].browse([decoration_mapping[decoration.id]]).art_line_id = art_line_mapping[decoration.art_line_id.id]
                        if decoration.note_id and decoration.note_id.id in art_note_mapping:                    
                            self.env['sale.order.line.decoration'].browse([decoration_mapping[decoration.id]]).note_id = art_note_mapping[decoration.note_id.id]
        if result and not self._context.get('split_sale_order', False) and result.reorder_ref and self.env['sale.order'].search([['splttedorder_ref', '=', result.reorder_ref.id]]):
            order_ids = [result.id]
            for order in self.env['sale.order'].search([['splttedorder_ref', '=', result.reorder_ref.id]]):
                copied_order = order.copy()
                copied_order.splttedorder_ref = result.id
                order_ids.append(copied_order.id)
        return result

    @api.multi
    def _get_amount_untaxed(self):
        self.ensure_one()
        amount_untaxed = 0
        for line in self.order_line:
            if line.line_changed:
                amount_untaxed += line.price_subtotal
        return amount_untaxed

    @api.multi
    def _get_tax_amount(self):
        self.ensure_one()
        res = {}
        currency = self.currency_id or self.company_id.currency_id
        for line in self.order_line:
            if line.line_changed:
                for tax in line.tax_id:
                    group = tax.tax_group_id
                    res.setdefault(group, 0.0)
                    res[group] += tax.compute_all(line.price_unit, quantity=line.product_uom_qty)['taxes'][0]['amount']
        res = sorted(res.items(), key=lambda l: l[0].sequence)
        res = map(lambda l: (l[0].name, formatLang(self.env, l[1], currency_obj=currency)), res)
        return res

    @api.multi
    def _get_amount_total(self):
        self.ensure_one()
        amount_total = 0
        for line in self.order_line:
            if line.line_changed:
                amount_total += line.price_total
        return amount_total

    @api.multi
    def _check_order_line(self):
        self.ensure_one()
        for line in self.order_line:
            if line.line_changed:
                return True
        return False

    @api.multi
    def art_processing(self):
        for order in self:
            for order_line in order.order_line:
                if order_line.product_id.can_decorate and order_line.product_type != 'service':
                    for decoration in order_line.decorations:
                        if decoration.art_id:
                            if decoration.art_id.state not in ['submit', 'in_process', 'completed']:
                                raise UserError(_("Please submit all ArtWorks before going to Art Processing."))
                        else:
                            raise UserError(_("Please do Art work with all decoration lines."))
                    if not order_line.is_decoration_disable and not order_line.decorations:
                        raise UserError(_("Please decorate all order lines whose product have True value for the field 'Can Be Decorated' OR make them as Blank Product for Decoration"))
        self.state = 'art_processing' 
        return True

    @api.multi
    def skip_art_processing(self):
        self.state = 'pending_review' 
        return True

    @api.multi
    def automate_art_processing(self):
        for order in self:
            if order.state == 'draft':
                move_to_art_processing = True
                for order_line in order.order_line:
                    if order_line.product_id.can_decorate and order_line.product_type != 'service':
                        for decoration in order_line.decorations:
                            if decoration.art_id:
                                if decoration.art_id.state not in ['submit', 'in_process', 'completed']:
                                    move_to_art_processing = False
                                    break
                            else:
                                move_to_art_processing = False
                                break
                        if not order_line.is_decoration_disable and not order_line.decorations:
                            move_to_art_processing = False
                            break
                if move_to_art_processing:
                    order.state = 'art_processing' 
        return True

    @api.multi
    def pending_review(self):
        # (#13833) DEV: Pending Review Button
        # for order in self:
        #     for order_line in order.order_line:
        #         if order_line.product_id.can_decorate:
        #             if order_line.art_id:
        #                 if order_line.art_id.state != 'completed':
        #                     raise UserError(_("Please complete all ArtWorks before going to Pending Review."))
        self.state = 'pending_review'
        return True

    @api.multi
    def set_to_pending_review(self):
        self.state = 'pending_review'
        return True

    @api.multi
    def automate_pending_review(self):
        for order in self:
            if order.state in ['draft', 'art_processing']:
                move_to_pending_review = False
                for order_line in order.order_line:
                    if order_line.product_id.can_decorate:
                        for decoration in order_line.decorations:
                            if decoration.art_id:
                                if decoration.art_id.state == 'completed':
                                    move_to_pending_review = True
                                    break
                if move_to_pending_review:
                    order.state = 'pending_review'
        return True

    @api.multi
    def skip_pending_review(self):
        self.skipped_pending_review = True
        return True

    def create_promo_code_line(self):
        product_id = self.env.ref('pinnacle_sales.product_product_promocode_apply')
        self.env['sale.order.line'].create({'product_id': product_id.id, 
                                           'name': 'PROMOCODE',
                                           'product_uom_qty': 1,
                                           'product_uom': product_id.uom_id.id,
                                           'blank_line': True,
                                            'order_id': self.id,
                                            'price_unit': product_id.lst_price
                                           })
    def create_blank_order_line(self):
        product_id = self.env.ref('pinnacle_sales.product_product_blank_order')
        self.env['sale.order.line'].create({'product_id': product_id.id, 
                                            'name': str(product_id.with_context(product_name_get=True).name_get()[0][1]),
                                            'product_uom_qty': 1,
                                            'product_uom': product_id.uom_id.id,
                                            'blank_line': True,
                                            'order_id': self.id,
                                           })

    @api.multi
    def applying_sale_pricelist_by_combining_quantity(self):
        for sale_order in self:
            if sale_order.pricelist_id and sale_order.partner_id:
                # Removing Template Wise Quantity Concept
                # template_wise_quantity = {}
                # for order_line in sale_order.order_line:
                #     if order_line.product_id.product_tmpl_id.id not in template_wise_quantity:
                #         template_wise_quantity[order_line.product_id.product_tmpl_id.id] = order_line.product_uom_qty
                #     else:
                #         template_wise_quantity[order_line.product_id.product_tmpl_id.id] += order_line.product_uom_qty
                for order_line in sale_order.order_line:
                    if 'promo_code' in self.env.context:
                        # order_line.with_context(sale_order_id=sale_order.id, template_wise_quantity=template_wise_quantity[order_line.product_id.product_tmpl_id.id]).product_uom_change()
                        # order_line.with_context(sale_order_id=sale_order.id, template_wise_quantity=template_wise_quantity[order_line.product_id.product_tmpl_id.id])._onchange_discount()
                        # Removing Template Wise Quantity Concept
                        order_line.with_context(sale_order_id=sale_order.id, sale_order_line_id=order_line.id).product_uom_change()
                        order_line.with_context(sale_order_id=sale_order.id, sale_order_line_id=order_line.id)._onchange_discount()
                    else:
                        if order_line.product_id and order_line.product_id.type != 'service':
                            # order_line.with_context(sale_order_id=sale_order.id, template_wise_quantity=template_wise_quantity[order_line.product_id.product_tmpl_id.id]).product_uom_change()
                            # order_line.with_context(sale_order_id=sale_order.id, template_wise_quantity=template_wise_quantity[order_line.product_id.product_tmpl_id.id])._onchange_discount()
                            # Removing Template Wise Quantity Concept
                            order_line.with_context(sale_order_id=sale_order.id, sale_order_line_id=order_line.id).product_uom_change()
                            order_line.with_context(sale_order_id=sale_order.id, sale_order_line_id=order_line.id)._onchange_discount()

    @api.multi
    def html_content_load(self):
        new_context = dict(self.env.context).copy()
        new_context.update(
            {'default_order_conf_html': self.order_conf_html})
        form_view_id = self.env.ref(
            'pinnacle_sales.wizard_test_form')
        return {'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'target': 'new',
                'view_id': form_view_id.id,
                'res_model': 'feedback.wizard',
                'name': 'Order Confirmation HTML',
                'context': new_context,
                }

    @api.multi
    def update_po_notes(self):
        self.ensure_one()
        self.sale_order_notes.unlink()
        vendor_keys = []
        so_notes = self.env['sale.order.line.notes']

        def create_notes(line, product_sku, key, finisher=False):
            exists = so_notes.search([('sale_order_id', '=',self.id),
                        ('vendor','=', key),
                        ('product_tmpl_id','=',line.product_id.product_tmpl_id.id)], limit=1)
            if exists:return
            value  = {
                'product_tmpl_id': line.product_id.product_tmpl_id.id,
                'vendor': key,
                'sale_order_id': self.id,
            }
            if not finisher:
                value['product_notes'] = ('[%s] %s \n') % (product_sku, line.change_supplier.po_notes or ' ')
                value['vendor_notes'] = line.change_supplier.name.po_notes
            else:
                value['vendor_notes'] = finisher.po_notes or ' '
            return  so_notes.create(value)

        for line in self.order_line:
            product_sku =  line.product_id.product_tmpl_id.fetch_seller_product_name(line.change_supplier.name)
            if line.product_id.type == 'service':
                pass
            elif (line.change_supplier.id):
                create_notes(line, product_sku, line.change_supplier.name.id)
                if not line.is_decoration_disable:
                    for dec_line in line.decorations:
                        create_notes(line, product_sku, dec_line.finisher.id, finisher=dec_line.finisher)

    @api.multi
    def bulk_upload_so(self):
        self.ensure_one()
        context_active_ids = []
        art_ids = []
        new_context = dict(self.env.context).copy()
        for line in self.art_line:
            if line.select_art_line:
                context_active_ids.append(line.id)
                art_ids.append(line.art_id.id)
                spec_file_url = line.spec_file_url
                vendor_file_url = line.vendor_file_url
                spec_file2_url = line.spec_file2_url
        self.clear_all_selection_for_sale_order()
        if not context_active_ids:
            raise UserError(_('Please select at least one line.'))
        new_context.update({'art_id': art_ids,
                            'art_line_ids': context_active_ids,
                            'cfs_attachment': True,
                            'so_line': True,
                            'default_spec_file_url': spec_file_url,
                            'default_vendor_file_url': vendor_file_url, 
                            'default_spec_file2_url': spec_file2_url})
        form_view_id = self.env.ref(
            'pinnacle_sales.art_upload_file_form_view')
        return {'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'target': 'new',
                'view_id': form_view_id.id,
                'res_model': 'art.upload.file',
                'name': 'File Upload',
                'context': new_context,
                }

    @api.multi
    def clear_all_selection_for_sale_order(self):
        for record in self:
            if record.order_line:
                record.order_line.write({'select': False, 'select_decoration_line': False, 'select_art_line': False})
            if record.decorations:
                record.decorations.write({'select': False})
            if record.decoration_group_ids:
                record.decoration_group_ids.write({'select': False})
            if record.art_line:
                record.art_line.write({'select': False, 'select_art_line': False})

    @api.multi
    def get_base_url(self):
        url = str(self.env['ir.config_parameter'].search([('key', '=', 'web.base.url')]).value)
        if url:
            return url
        
class SaleOrderLine(models.Model):  
    _inherit = 'sale.order.line'


    @api.depends('product_uom_qty', 'sale_price', 'tax_id')
    def _compute_amount_change(self):
        """
        Compute the amounts of the SO line.
        """
        price_tax = 0.0
        for line in self:
            taxes = line.tax_id.compute_all(line.sale_price, line.order_id.currency_id, line.product_uom_qty, product=line.product_id, partner=line.order_id.partner_id)
            if (line.product_id.non_taxable or line.order_id.partner_id.partner_non_taxable or line.order_id.partner_id.parent_id.partner_non_taxable) and taxes:
               price_tax = 0.0
            else:
                price_tax = taxes['total_included'] - taxes['total_excluded']
            line.update({
                'price_tax': price_tax,
                'price_total': taxes['total_included'],
                'price_subtotal': taxes['total_excluded'],
            })


    product_variant_name = fields.Char(
        compute="_compute_product_variant_name", store=True,
        readonly=True, string="Variant")
    product_sku = fields.Char(
        related="product_id.product_sku", string="Product ID") 
    decorations = fields.One2many(
        'sale.order.line.decoration', 'sale_order_line_id',
        string='Decorations', copy=False)
    customer_arts = fields.Many2many(related='order_id.customer_arts')
    select = fields.Boolean(string="Select", default=False, copy=False)
    select_decoration_line = fields.Boolean(string="Select Decoration Line", default=False, copy=False)
    select_art_line = fields.Boolean(string="Select Art Line", default=False, copy=False)
    can_decorate = fields.Boolean(related="product_id.can_decorate", store=True)
    product_type = fields.Selection(related="product_id.type", store=True)
    inventory_code = fields.Char(compute='compute_inventory_code')
    finisher_id = fields.Many2one('res.partner', string="Finisher") 
    art_state = fields.Selection(related='art_id.state', string='Status')
    revision = fields.Integer(related='art_id.revision', string='Revisions')
    spec_file = fields.Binary(related='art_line_id.spec_file', string='Art Page 1 File(s)')
    spec_file_name = fields.Char(related='art_line_id.spec_file_name', string='Art Page 1 File(s) Name')
    # attachment URL reference(CFS integration)
    spec_file_url = fields.Char(related='art_line_id.spec_file_att.url', string='Art Page 1 File(s)', copy=True)  # 12212
    spec_file2 = fields.Binary(related='art_line_id.spec_file2', string='Art Page 2 File(s)')
    spec_file2_url = fields.Char(related='art_line_id.spec_file2_att.url', string='Art Page 2 File(s)', copy=True)  # 12212
    spec_file2_name = fields.Char(related='art_line_id.spec_file2_name', string='Art Page 2 File(s) Name')
    vendor_file = fields.Binary(related='art_line_id.vendor_file', string='Vendor File(s)')
    vendor_file_url = fields.Char(related='art_line_id.vendor_file_att.url', string='Vendor Files', copy=True)  # 12212
    vendor_file_name = fields.Char(related='art_line_id.vendor_file_name', string='Vendor File Name')
    art_id = fields.Many2one('art.production', 'Art Worksheet', copy=False)
    art_line_id = fields.Many2one('art.production.line', 'Art Worksheet Line', copy=False)
    hide_file = fields.Boolean(related='art_line_id.hide_file', store="True", string='Hide Record')
    shipping_id = fields.Many2one('shipping.line',  default=False, copy=True, ondelete='cascade')
    change_supplier = fields.Many2one('product.supplierinfo', string="Supplier")
    changer_supplier_partner = fields.Many2one('res.partner', related="change_supplier.name", store=True)
    purchase_price = fields.Float(string="Cost", digits=dp.get_precision('Vendor Level Cost'))
    is_decoration_disable = fields.Boolean(string="Blank Product")
    decoration_group_sequences = fields.Many2many('sale.order.line.decoration.group', compute="_compute_decoration_group_sequences",string='Decoration Groups', readonly=True, copy=False)
    ltm_fee = fields.Boolean(default=False)
    decoration_charge_group_id = fields.Many2one('sale.order.line.decoration.group.charge.line', string="Decoration Group", copy=False)
    margin_in_percentage = fields.Float(compute='_compute_margin_in_percentage', string="SO Margin(%)", digits=dp.get_precision('Product Price'), store=True, copy=False)
    margin_in_percentage_string = fields.Char(compute='_compute_margin_in_percentage', string="Blended Margin", glp="It gives profitability in percentage by calculating the difference between the Unit Price and the cost.")
    margin_in_percentage_duplicate = fields.Float(compute='_compute_margin_in_percentage', string="SO Margin(%)", digits=dp.get_precision('Product Price'), store=True, copy=False)
    tax_id = fields.Many2many('account.tax', string='Taxes', domain=['|', ('active', '=', False), ('active', '=', True)], copy=False)
    rush_finisher = fields.Many2one('res.partner', string="Rush Finisher")
    rush_supplier = fields.Many2one('res.partner', string="Rush Supplier")
    rush_shipping_id = fields.Many2one('shipping.line',ondelete="set null" , default=False, copy=False)
    blank_line = fields.Boolean(default=False)
    other_supplier_id = fields.Many2one("res.partner") 
    state = fields.Selection([
        ('quote_draft', 'Draft Quote'),
        ('quote_sent', 'Quote Sent'),
        ('closed_quote', 'Quote Closed'),
        ('quote_vir_processing', 'Virtual Processing'),
        ('quote_review', 'Review Quote'),
        ('quote_man_approval', 'Manager Approval'),
        ('quote_man_approved', 'Manager Approved'),
        ('quote_man_rejected', 'Manager Rejected'),
        ('quote_approved', 'Quote Approved'),
        ('quote_rejected', 'Quote Rejected'),
        ('draft', 'Draft'),
        ('art_processing', 'Art Processing'),
        ('pending_review', 'Pending Review'),
        ('to_approve', 'Manager Approval'),
        ('approve', 'Manager Approved'),
        ('reject', 'Manager Rejected'),
        ('sent', 'Confirmation Sent'),
        ('confirmation_received', 'Confirmation Received'),
        ('accounting_approval', 'Accounting Approval'),
        ('accounting_approved', 'Accounting Approved'),
        ('accounting_rejected', 'Accounting Rejected'),
        ('sale', 'PO Generated'),
        ('ready_for_fullfillment', 'Ready for Fulfillment'),
        ('shipped', 'Shipped'),
        ('partially_invoiced', 'Partially Invoiced'),
        ('invoiced', 'Invoiced'),
        ('delivered', 'Delivered'),
        ('close', 'Closed'),
        ('reopen', 'Re-Opened'),
        ('done', 'Re-Closed'),
        ('cancel', 'Cancelled'),
        ('hold', 'On Hold'),
        ('inactive','Inactive'),
    ], related='order_id.state', string='Order Status', readonly=True, copy=False, store=True, default='draft')
    before_promocode_price_unit = fields.Float('Unit Price', required=True, digits=dp.get_precision('Product Price'), default=0.0)
    before_promocode_discount = fields.Float(string='Discount (%)', digits=dp.get_precision('Discount'), default=0.0)
    before_promocode_discount_amount = fields.Float(string='Discount Amount', digits=dp.get_precision('Product Price'), default=0.0)
    price_subtotal = fields.Monetary(compute='_compute_amount_change', string='Subtotal', readonly=True, store=True)
    price_tax = fields.Monetary(compute='_compute_amount_change', string='Taxes', readonly=True, store=True)
    price_total = fields.Monetary(compute='_compute_amount_change', string='Total', readonly=True, store=True)
    program_id = fields.Many2one(relation='partner.program', related='order_id.program_id', string='Program')# 12785
    discount_amount = fields.Float(string='Discount Amount', digits=dp.get_precision('Product Price'))
    is_assigned_decoration_charge_group_id = fields.Boolean(compute='_compute_is_assigned_decoration_charge_group_id', string='Is it a decoration charge line ?', store=True)
    is_override = fields.Boolean(string="Lock")
    discount = fields.Float(compute='_compute_amount_change', string='Discount (%)', digits=dp.get_precision('Discount'),
        default=0.0)
    sale_price = fields.Float('Sale Price', required=True, digits=dp.get_precision('Product Price'), default=0.0) 
    ltm_parent = fields.Many2one('sale.order.line', ondelete='cascade')
    line_partner_to_tree = fields.Many2one('res.partner', compute="_line_partner_to_tree_method")
    has_any_charge = fields.Boolean(compute="_compute_has_charge", string="Has Any Charge ?", store=True)
    has_any_decoration = fields.Boolean(compute="_compute_has_has_any_decoration", string="Has Any Decoration ?", store=True)
    has_any_decoration_charge_or_any_shipping_line = fields.Boolean(compute="_compute_has_any_decoration_charge_or_any_shipping_line", string="Has Any Decoration Charge Or Shipping Line ?", store=True)
    shipping_lines = fields.Many2many(
        "shipping.line", 
        relation="sale_order_line_shipping_line_rel", 
        column1="sale_order_line_id", 
        column2="shipping_line_id", 
        string="Shipping lines", copy=True)

    @api.one
    @api.depends('product_uom_qty', 'decorations.decoration_group_id', 'decorations.decoration_group_id.charge_lines', 'shipping_lines', 'shipping_lines.delivery_line')
    def _compute_has_charge(self):
        self.has_any_charge = False
        for decoration in self.decorations:
            if decoration.decoration_group_id and decoration.decoration_group_id.charge_lines:
                self.has_any_charge = True
                break
        for shipping_line in self.shipping_lines:
            if shipping_line.delivery_line:
                self.has_any_charge = True
                break

    @api.one
    @api.depends('decorations')
    def _compute_has_has_any_decoration(self):
        self.has_any_decoration = False
        if self.decorations:
            self.has_any_decoration = True

    @api.one
    @api.depends('product_uom_qty', 'decorations.decoration_group_id', 'decorations.decoration_group_id.charge_lines', 'shipping_lines')
    def _compute_has_any_decoration_charge_or_any_shipping_line(self):
        self.has_any_decoration_charge_or_any_shipping_line = False
        for decoration in self.decorations:
            if decoration.decoration_group_id and decoration.decoration_group_id.charge_lines:
                self.has_any_decoration_charge_or_any_shipping_line = True
                break
        if self.shipping_lines:
            self.has_any_decoration_charge_or_any_shipping_line = True

    @api.one
    @api.depends('change_supplier', 'other_supplier_id')
    def _line_partner_to_tree_method(self):
        if self.other_supplier_id:
            self.line_partner_to_tree = self.other_supplier_id.id
        if self.change_supplier:
            self.line_partner_to_tree = self.change_supplier.name.id

    @api.one
    @api.depends('product_id')
    def compute_inventory_code(self):
        code =  self.product_id.product_tmpl_id.inventory_control_code
        self.inventory_code = False
        if code:
            self.inventory_code = dict(self.env['product.template'].fields_get(['inventory_control_code'])['inventory_control_code']['selection'])[self.product_id.product_tmpl_id.inventory_control_code]

    @api.multi
    @api.onchange('price_unit')
    def _onchange_sale_price(self):
        self.sale_price = self.price_unit

    @api.one
    @api.depends('decoration_charge_group_id')
    def _compute_is_assigned_decoration_charge_group_id(self):
        if self.decoration_charge_group_id:
            self.is_assigned_decoration_charge_group_id = True
        else:
            self.is_assigned_decoration_charge_group_id = False


    @api.depends('state', 'product_uom_qty', 'qty_delivered', 'qty_to_invoice', 'qty_invoiced')
    def _compute_invoice_status(self):
        """
        Compute the invoice status of a SO line. Possible statuses:
        - no: if the SO is not in status 'sale' or 'done', we consider that there is nothing to
          invoice. This is also hte default value if the conditions of no other status is met.
        - to invoice: we refer to the quantity to invoice of the line. Refer to method
          `_get_to_invoice_qty()` for more information on how this quantity is calculated.
        - upselling: this is possible only for a product invoiced on ordered quantities for which
          we delivered more than expected. The could arise if, for example, a project took more
          time than expected but we decided not to invoice the extra cost to the client. This
          occurs onyl in state 'sale', so that when a SO is set to done, the upselling opportunity
          is removed from the list.
        - invoiced: the quantity invoiced is larger or equal to the quantity ordered.
        """
        precision = self.env['decimal.precision'].precision_get('Product Unit of Measure')
        for line in self:
            if line.state not in ('draft', 'art_processing', 'pending_review', 'to_approve', 'approve', 'reject', 'sent', 'confirmation_received',  'accounting_approval', 'accounting_approved', 'accounting_rejected', 'ready_for_fullfillment', 'cancel', 'hold', 'inactive', 'sale', 'partially_invoiced', 'shipped', 'invoiced', 'delivered', 'close', 'reopen', 'done'):
                line.invoice_status = 'no'
            elif not float_is_zero(line.qty_to_invoice, precision_digits=precision):
                line.invoice_status = 'to invoice'
            elif line.state in ('draft', 'art_processing', 'pending_review', 'to_approve', 'approve', 'reject', 'sent', 'confirmation_received',  'accounting_approval', 'accounting_approved', 'accounting_rejected', 'ready_for_fullfillment', 'cancel', 'hold', 'inactive', 'partially_invoiced', 'shipped', 'invoiced', 'delivered', 'close', 'reopen', 'sale') and line.product_id.invoice_policy == 'order' and\
                    float_compare(line.qty_delivered, line.product_uom_qty, precision_digits=precision) == 1:
                line.invoice_status = 'upselling'
            elif float_compare(line.qty_invoiced, line.product_uom_qty, precision_digits=precision) >= 0:
                line.invoice_status = 'invoiced'
            else:
                line.invoice_status = 'no'

    @api.depends('qty_invoiced', 'qty_delivered', 'product_uom_qty', 'order_id.state')
    def _get_to_invoice_qty(self):
        """
        Compute the quantity to invoice. If the invoice policy is order, the quantity to invoice is
        calculated from the ordered quantity. Otherwise, the quantity delivered is used.
        """
        for line in self:
            if line.order_id.state in ['draft', 'art_processing', 'pending_review', 'to_approve', 'approve', 'reject', 'sent', 'confirmation_received',  'accounting_approval', 'accounting_approved', 'accounting_rejected', 'ready_for_fullfillment', 'cancel', 'hold', 'inactive', 'sale', 'partially_invoiced', 'close', 'reopen', 'shipped', 'invoiced', 'delivered', 'done']:
                if line.product_id.invoice_policy == 'order':
                    line.qty_to_invoice = line.product_uom_qty - line.qty_invoiced
                else:
                    line.qty_to_invoice = line.qty_delivered - line.qty_invoiced
            else:
                line.qty_to_invoice = 0

    @api.depends('product_id', 'purchase_price', 'product_uom_qty', 'price_subtotal', 'order_id.currency_id', 'order_id.program_type')
    def _product_margin(self):
        for line in self:
            if line.product_id and not line.product_id.exclude_from_computing_sale_margins and (line.product_id.inventory_control_code not in ['inv_co', 'inv_cs'] or line.order_id.program_type != 'store'):
                currency = line.order_id.pricelist_id.currency_id
                line.margin = currency.round(line.price_subtotal - ((line.purchase_price or line.product_id.standard_price) * line.product_uom_qty))

    @api.one
    @api.depends('product_id', 'purchase_price', 'product_uom_qty', 'price_subtotal', 'order_id.currency_id', 'order_id.program_type')
    def _compute_margin_in_percentage(self):
        self.margin_in_percentage = 0.00
        self.margin_in_percentage_duplicate = 0.00
        self.margin_in_percentage_string = '0%'
        if self.product_id and not self.product_id.exclude_from_computing_sale_margins and (self.product_id.inventory_control_code not in ['inv_co', 'inv_cs'] or self.order_id.program_type != 'store'):
            if self.price_subtotal:
                self.margin_in_percentage = ((self.price_subtotal - (self.product_uom_qty * (self.purchase_price or self.product_id.standard_price))) / self.price_subtotal) * 100
                self.margin_in_percentage_string = str(int(self.margin_in_percentage)) + '%'
                self.margin_in_percentage_duplicate = ((self.price_subtotal - (self.product_uom_qty * (self.purchase_price or self.product_id.standard_price))) / self.price_subtotal) * 100

    @api.one
    @api.depends('decorations.decoration_group_id')
    def _compute_decoration_group_sequences(self):
        decoration_ids = []
        for decoration in self.decorations:
            if decoration.decoration_group_id:
                decoration_ids.append(decoration.decoration_group_id.id)
        decoration_ids = list(set(decoration_ids))
        self.decoration_group_sequences = [[6, False, decoration_ids]]

    def _compute_margin(self, order_id, product_id, product_uom_id, price=None):
        # cost price calculate through tiers of change_suppliers
        if price is None:
            price = self.purchase_price
        frm_cur = self.env.user.company_id.currency_id
        to_cur = order_id.pricelist_id.currency_id
        purchase_price = price # Todo Vidhin Mehta (This is not pulling correct cost price while adding item from product when supplier is not set)
        if product_uom_id != product_id.uom_id:
            purchase_price = self.env['product.uom']._compute_price(product_id.uom_id.id, purchase_price, to_uom_id=product_uom_id.id)
        ctx = self.env.context.copy()
        ctx['date'] = order_id.date_order
        price = frm_cur.with_context(ctx).compute(purchase_price, to_cur, round=False)
        return price
        
    @api.one
    @api.depends('product_id')
    def _compute_product_variant_name(self):
        if self.rush_finisher.id or self.rush_supplier.id:
            self.product_variant_name = self.product_id.name_get()[0][1]
            return
        if self.product_id:
            self.product_variant_name = str(", ".join(
                [v.name for v in self.product_id.attribute_value_ids]))
        else:
            self.product_variant_name = ''
    added_decoration = fields.Boolean(
        compute="_compute_added_decoration",
        string="Added Decoration ?",
        store=True)

    @api.one
    @api.depends('decorations')
    def _compute_added_decoration(self):
        self.added_decoration = False
        if self.decorations: 
            self.added_decoration = True

    @api.multi
    def view_decoration(self):
        form_view_id = self.env.ref('pinnacle_sales.view_order_line_form_view')
        new_context = self.env.context.copy()
        new_context['invisible_so_line'] = True
        if self:
            return {'type': 'ir.actions.act_window',
                    'view_mode': 'form',
                    'name': "Decoration Lines",
                    'target': 'new',
                    'view_id': form_view_id.id,
                    'res_model': 'sale.order.line',
                    'res_id': self[0].id,
                    'context': new_context
                    }

    @api.multi
    def write(self, vals):
        self = self.exists()
        old_value  = {}
        for record in self:
            old_sale_price = record.sale_price
            old_price_unit = record.price_unit
            if old_sale_price == 0:
                old_sale_price = old_price_unit
            old_value[record.id] = [old_sale_price, old_price_unit]
        flag = self.get_flag(vals)
        result = super(SaleOrderLine, self).write(vals)
        if result:
            for record in self:
                if vals.get('product_id', False) and self._context.get('sale_order_copy', False):
                    product_setup_sale_price = self.env.ref('pinnacle_sales.product_product_setup_sale_price')
                    if not product_setup_sale_price:
                        raise UserError(_('Setup Sale Price Product Not Found'))
                    if vals.get('product_id', False) == product_setup_sale_price.id:
                        record.sale_price = record.price_unit
                #14249
                if vals.get('price_unit') and not vals.get('sale_price'):
                    old_sale_price, old_price_unit = old_value.get(record.id, [0, 0])
                    if not record.is_override and  old_sale_price == old_price_unit:
                        record.sale_price = vals.get('price_unit')
                if record.decoration_charge_group_id:
                    if 'name' in vals:
                        record.decoration_charge_group_id.name = vals['name']
                    if 'product_uom_qty' in vals:
                        record.decoration_charge_group_id.product_uom_qty = vals['product_uom_qty']
                    if 'price_unit' in vals:
                        record.decoration_charge_group_id.price_unit = vals['price_unit']
                    if 'purchase_price' in vals:
                        record.decoration_charge_group_id.purchase_price = vals['purchase_price']
                # Updating artworksheet lines when its related sale order lines changed
                if vals.get('product_id', False) or vals.get('change_supplier', False) or vals.get('name', False) or vals.get('product_sku', False) or vals.get('product_uom_qty', False):
                    if record.decorations and flag:
                        for decoration in record.decorations:
                            if decoration.art_line_id and decoration.art_line_id.exists():
                                item_dimensions, description, product_sku, product_id, product_uom_qty, vendor_link = record.calculate_itemdimensions_and_get_sale_order_line_info_for_artworksheet()
                                decoration.art_line_id.description = description
                                decoration.art_line_id.product_sku = product_sku
                                decoration.art_line_id.product_id = product_id
                                decoration.art_line_id.quantity = product_uom_qty
        if 'product_uom_qty' in vals and self.env.context.get('user_response_about_change_to_qty', False):
            shipping_lines = self.env['shipping.line'].search([('order', '=', self.order_id.id)])
            for line in shipping_lines:
                olines = [oline.id for oline in line.shipping_sale_orders.exists()]
                if self.id in olines:
                    for orderline in line.shipping_sale_orders.exists():
                        orderline.tax_id = [(6, 0, [])]
                        line.delivery_line.unlink()
                    for order_line in self.order_id.order_line:
                        if order_line.shipping_id.id == line.id:
                            self._cr.execute("delete from sale_order_line where id = %s" % (order_line.id,))
        return result

    @api.multi
    def copying_data_of_art_wroksheets_to_be_archived(self, art_wroksheets_to_be_archived):
        sale_order_line_decoration_archives = []
        art_wroksheets_to_be_archived = list(set(art_wroksheets_to_be_archived))
        art_wroksheets = self.env['art.production'].browse(art_wroksheets_to_be_archived)
        for art_wroksheet in art_wroksheets:
            art_wroksheet.archive_sale_id = art_wroksheet.sale_id.name
            art_wroksheet.archive_order_total = art_wroksheet.order_total
            art_wroksheet.archive_analytic_tag_ids = [[6, 0, art_wroksheet.analytic_tag_ids.ids]]
            for art_line in art_wroksheet.art_lines:
                art_line.archive_product_sku = art_line.product_sku
                art_line.archive_group = ','.join([group.group_id for group in art_line.group])
                art_line.archive_product_variant_name = art_line.product_variant_name
                art_line.archive_reorder_sale_ids = art_line.reorder_sale_ids
                art_line.archive_description = art_line.description
            sale_order_line_decoration_archive_obj = self.env['sale.order.line.decoration.archive']
            for decoration in art_wroksheet.decorations:
                sale_order_line_decoration_archive_obj.create({'art_id':decoration.art_id.id,
                                 'art_line_id': decoration.art_line_id.id,
                                 'note_id': decoration.note_id.id,
                                 'product_sku': decoration.product_sku,
                                 'product_variant_name': decoration.product_variant_name,
                                 'imprint_location': decoration.imprint_location.id,
                                 'imprint_method': decoration.imprint_method.id,
                                 'customer_art': decoration.customer_art.id,
                                 'pms_color_code': decoration.pms_color_code,
                                 'area': decoration.area,
                                 'min_pt_size': decoration.min_pt_size,
                                 'stock_color_char': decoration.stock_color_char,
                                 'stock_color': [[6, 0, decoration.stock_color.ids]],
                                 'pms_code': [[6, 0, decoration.pms_code.ids]],
                                 'vendor_decoration_location': decoration.vendor_decoration_location.id,
                                 'vendor_decoration_method': decoration.vendor_decoration_method.id,
                                 'vendor_prod_specific': decoration.vendor_prod_specific,
                                 'vendor_product_sku': decoration.vendor_product_sku})
                decoration.note_id = False
                if sale_order_line_decoration_archive_obj:
                    sale_order_line_decoration_archives.append(sale_order_line_decoration_archive_obj.id)
        return sale_order_line_decoration_archives

    @api.multi
    def actual_archiving_of_artworksheets_with_handling_of_other_data(self, art_wroksheets_to_be_archived):
        art_wroksheets_to_be_archived = list(set(art_wroksheets_to_be_archived))
        art_worksheets = self.env['art.production'].browse(art_wroksheets_to_be_archived)
        # Deleting Duplicated Archied ArtWorkSheets
        sale_orders = []
        for art_worksheet in art_worksheets:
            if art_worksheet:
                if art_worksheet.sale_id and art_worksheet.vendor_id:
                    sale_orders.append(art_worksheet.sale_id)
                    duplicated_archived_artworksheets = self.env['art.production'].search(['&', ('archive_sale_id', '=', art_worksheet.sale_id.name), '&', ('vendor_id', '=', art_worksheet.vendor_id.id), '&', ('id', '!=', art_worksheet.id), ('active', '=', False)])
                    if duplicated_archived_artworksheets:
                        duplicated_archived_artworksheets.unlink()
        for art_worksheet in art_worksheets:
            art_worksheet.write({'active': False, 'state': 'archive', 'job': 1, 'job1': 1, 'art_sequence': str(art_worksheet.archive_sale_id) + ' - 1', 'sale_id': False})  
            for art_line in art_worksheet.art_lines:
                art_line.write({'sale_line_id': False})
        for sale_order in sale_orders:
            index = 1
            art_worksheets = sale_order.artworksheets.sorted(key=lambda r: r.id)
            for art_worksheet in art_worksheets:
                art_worksheet.write({'job': index, 'art_sequence': sale_order.name + ' - ' + str(index)})
                index += 1
            if sale_order.artworksheets:    
                sale_order.artworksheets.write({'job1': len(sale_order.artworksheets)})
        return True

    @api.multi
    def updating_other_decorations_of_archiving_artworksheets(self, decorations_ids_to_be_updated_for_artworksheet):
        decorations_ids_to_be_updated_for_artworksheet = list(set(decorations_ids_to_be_updated_for_artworksheet))
        for decorations_id_to_be_updated_for_artworksheet in decorations_ids_to_be_updated_for_artworksheet:
            sale_order = False
            decoration = self.env['sale.order.line.decoration'].browse(decorations_id_to_be_updated_for_artworksheet).exists()
            if decoration:
                if decoration.sale_order_id:
                    sale_order = decoration.sale_order_id
                    decoration.write({'art_id': False ,'art_line_id': False})
            if sale_order:
                sale_order.with_context(decoration_ids=[decorations_id_to_be_updated_for_artworksheet]).add_art_worksheet()
        return True


    @api.model
    def unlink(self):
        self = self.exists()
        decoration_groups = []
        for record in self:
            if record.decoration_charge_group_id:
                record.decoration_charge_group_id.added_to_order_lines = 'NO'
            for decoration in record.decorations:
                if decoration.decoration_group_id:
                    decoration_groups.append(decoration.decoration_group_id.id)
            shipping_lines = self.env['shipping.line'].search([('order', '=', record.order_id.id)])
            if shipping_lines:
                for line in shipping_lines:
                    order_lines = [orderline.id for orderline in line.shipping_sale_orders]
                    if record.id in order_lines:
                        shipping_charge_order_lines = record.search([('shipping_id', '=', line.id)])
                        for shipping_charge_order_line in shipping_charge_order_lines:
                            if shipping_charge_order_line.id not in self.ids:
                                shipping_charge_order_line.unlink()
                        line.unlink()
        # Archiving Fields For Maintaining Sale Order Details When Art Work Sheet Archived
        sale_order_line_decoration_archives = []
        art_wroksheets_to_be_archived = [] 
        decorations_ids_to_be_updated_for_artworksheet = [] 
        changed_state_ids = []
        for record in self:
            for decoration in record.decorations:
                if decoration.art_id and decoration.art_id.id not in art_wroksheets_to_be_archived:    
                    for art_line in decoration.art_id.art_lines:
                        if art_line.sale_line_id.id and art_line.sale_line_id.id not in self.ids:
                            decorations_ids_to_be_updated_for_artworksheet += art_line.decorations.ids
                    art_wroksheets_to_be_archived.append(decoration.art_id.id)
            # Force fulling deleting service type products when the sale order line state in sale
            if record.product_id and record.product_id.type == 'service' and record.state == 'sale':
                record.state = 'draft'
                changed_state_ids.append(record.id)
        if self.filtered(lambda x: x.state in ('partially_invoiced', 'invoiced', 'close', 'reopen', 'done')):
            raise UserError(_('You can not remove a sale order line.\nDiscard changes and try setting the quantity to 0.'))
        if art_wroksheets_to_be_archived:
            sale_order_line_decoration_archives = self.copying_data_of_art_wroksheets_to_be_archived(art_wroksheets_to_be_archived)
        result = super(SaleOrderLine, self).unlink()
        if not result:
            for record in self:
                if record.decoration_charge_group_id:
                    record.decoration_charge_group_id.added_to_order_lines = 'YES'
                if record.id in changed_state_ids:
                    record.state = 'sale'
            if sale_order_line_decoration_archives:
                self.env['sale.order.line.decoration.archive'].browse(sale_order_line_decoration_archives).unlink()
        if result:
            if  art_wroksheets_to_be_archived:
                self.actual_archiving_of_artworksheets_with_handling_of_other_data(art_wroksheets_to_be_archived)
            if decorations_ids_to_be_updated_for_artworksheet:
                self.updating_other_decorations_of_archiving_artworksheets(decorations_ids_to_be_updated_for_artworksheet)
            decoration_group_deleted = []
            for decoration_group  in self.env['sale.order.line.decoration.group'].browse(decoration_groups).exists():
                if len(decoration_group.decorations.exists()) < 1:
                    decoration_group_deleted.append(decoration_group.id)
                else:
                    if decoration_group.charge_lines.exists():
                        decoration_group.charge_lines.exists().unlink()
            if decoration_group_deleted:
                self.env['sale.order.line.decoration.group'].browse(list(set(decoration_group_deleted))).unlink()
        return result

    @api.model
    def create(self, vals):
        if vals.get('price_unit') and not (self._context.get('sale_order_copy', False) or self._context.get('sale_order_split', False)):
            vals['sale_price'] = vals.get('price_unit')
        result = super(SaleOrderLine, self).create(vals)
        for order_line in result:
            if order_line.order_id:
                order_line.analytic_tag_ids = [(6, 0, order_line.order_id.analytic_tag_ids.ids)]
        #Not Applying Pricelist Concept
        # Applying Sale PriceList/PromoCode by combining quantity of each order line
        # if result.product_id and result.product_id.type != 'service':
        #     for sale_order_line in result.order_id.order_line:
        #         sale_order_line.product_id.sale_order_price_without_tax = result.order_id.amount_untaxed
        #         sale_order_line.product_id.sale_order_price_with_tax = result.order_id.amount_total
        #     result.order_id.applying_sale_pricelist_by_combining_quantity()
        return result

    @api.multi
    @api.onchange('product_id')
    def product_id_change(self):
        if not self.product_id:
            return {'domain': {'product_uom': []}}

        vals = {}
        domain = {'product_uom': [('category_id', '=', self.product_id.uom_id.category_id.id)]}
        if not self.product_uom or (self.product_id.uom_id.id != self.product_uom.id):
            vals['product_uom'] = self.product_id.uom_id
            vals['product_uom_qty'] = 1.0

        product = self.product_id.with_context(
                lang=self.order_id.partner_id.lang,
                partner=self.order_id.partner_id.id,
                quantity=vals.get('product_uom_qty') or self.product_uom_qty,
                date=self.order_id.date_order,
                pricelist=self.order_id.pricelist_id.id,
                uom=self.product_uom.id
            )
        # Removing Template Wise Quantity Concept
        # if 'template_wise_quantity' in self.env.context:
        #     product = self.product_id.with_context(
        #         lang=self.order_id.partner_id.lang,
        #         partner=self.order_id.partner_id.id,
        #         quantity=self.env.context['template_wise_quantity'],
        #         date=self.order_id.date_order,
        #         pricelist=self.order_id.pricelist_id.id,
        #         uom=self.product_uom.id
        #     )

        name = product.name_get()[0][1]
        vals['name'] = name

        self._compute_tax_id()

        if self.order_id.pricelist_id and self.order_id.partner_id:
            vals['price_unit'] = self.env['account.tax']._fix_tax_included_price(self._get_display_price(product), product.taxes_id, self.tax_id)
        self.update(vals)

        title = False
        message = False
        warning = {}
        if product.sale_line_warn != 'no-message':
            title = _("Warning for %s") % product.name
            message = product.sale_line_warn_msg
            warning['title'] = title
            warning['message'] = message
            if product.sale_line_warn == 'block':
                self.product_id = False
            return {'warning': warning}
        return {'domain': domain}

    @api.onchange('product_uom', 'product_uom_qty')
    def product_uom_change(self):
        if not self.product_uom:
            #self.price_unit = 0.0
            return
        if self.order_id.pricelist_id and self.order_id.partner_id:
            product = self.product_id.with_context(
                lang=self.order_id.partner_id.lang,
                partner=self.order_id.partner_id.id,
                quantity=self.product_uom_qty,
                date_order=self.order_id.date_order,
                pricelist=self.order_id.pricelist_id.id,
                uom=self.product_uom.id,
                fiscal_position=self.env.context.get('fiscal_position')
            )
        # Removing Template Wise Quantity Concept
        # if 'template_wise_quantity' in self.env.context:
        #     product = self.product_id.with_context(
        #         lang=self.order_id.partner_id.lang,
        #         partner=self.order_id.partner_id.id,
        #         quantity=self.env.context['template_wise_quantity'],
        #         date=self.order_id.date_order,
        #         pricelist=self.order_id.pricelist_id.id,
        #         uom=self.product_uom.id
        #     )
        
        #task 13980 price unit and sale price shall not change on product_uom_qty
        #self.price_unit = self.env['account.tax']._fix_tax_included_price(self._get_display_price(product), product.taxes_id, self.tax_id)

    @api.onchange('product_id', 'price_unit', 'product_uom', 'product_uom_qty', 'tax_id')
    def _onchange_discount(self):
        if 'promo_code' not in self.env.context:
            self.discount = 0.0
        if not (self.product_id and self.product_uom and
                self.order_id.partner_id and self.order_id.pricelist_id and
                self.order_id.pricelist_id.discount_policy == 'without_discount' and
                self.env.user.has_group('sale.group_discount_per_so_line')):
            return
        context_partner = dict(self.env.context, partner_id=self.order_id.partner_id.id)
        pricelist_context = dict(context_partner, uom=self.product_uom.id, date=self.order_id.date_order)

        product_uom_qty = self.product_uom_qty
        # Removing Template Wise Quantity Concept
        # if 'template_wise_quantity' in self.env.context:
        #     product_uom_qty=self.env.context['template_wise_quantity']
               
        price, rule_id = self.order_id.pricelist_id.with_context(pricelist_context).get_product_price_rule(self.product_id, product_uom_qty or 1.0, self.order_id.partner_id)
        new_list_price, currency_id = self.with_context(context_partner)._get_real_price_currency(self.product_id, rule_id, product_uom_qty, self.product_uom, self.order_id.pricelist_id.id)
        new_list_price = self.env['account.tax']._fix_tax_included_price(new_list_price, self.product_id.taxes_id, self.tax_id)
        # [FIX] sale: 100 % discount
        #if price != 0 and new_list_price != 0:
        if new_list_price != 0:
            if 'promo_code' not in self.env.context:
                if self.product_id.company_id and self.order_id.pricelist_id.currency_id != self.product_id.company_id.currency_id:
                    # new_list_price is in company's currency while price in pricelist currency
                    ctx = dict(context_partner, date=self.order_id.date_order)
                    new_list_price = self.env['res.currency'].browse(currency_id).with_context(ctx).compute(new_list_price, self.order_id.pricelist_id.currency_id)
            else:
                if self.product_id.company_id and self.order_id.pricelist_id.currency_id != self.product_id.old_currency_id:
                    # new_list_price is in company's currency while price in pricelist currency
                    ctx = dict(context_partner, date=self.order_id.date_order)
                    new_list_price = self.env['res.currency'].browse(currency_id).with_context(ctx).compute(new_list_price, self.order_id.pricelist_id.currency_id)

            discount = (new_list_price - price) / new_list_price * 100
            if discount > 0:
                if 'promo_code' not in self.env.context:
                    self.discount_amount = (new_list_price - price) * self.product_uom_qty
                else:
                    discount_amount = (new_list_price - price) * self.product_uom_qty
                    if self.discount_amount + discount_amount <= self.price_unit * self.product_uom_qty:
                        self.discount_amount += discount_amount 


    @api.multi
    def _get_display_price(self, product):
        if self.order_id.pricelist_id.discount_policy == 'without_discount':
            from_currency = self.order_id.company_id.currency_id
            if 'promo_code' not in self.env.context:
                return from_currency.compute(product.lst_price, self.order_id.pricelist_id.currency_id)
            else:
                return self.price_unit
        return product.with_context(pricelist=self.order_id.pricelist_id.id).price

    @api.multi
    def calculate_itemdimensions_and_get_sale_order_line_info_for_artworksheet(self):
        self.ensure_one()
        item_dimensions = ''
        product_name = ''
        product_sku = ''
        supplier_id = self.change_supplier and self.change_supplier.name.id or False
        vendor_link = ''
        if self.change_supplier.product_length or self.change_supplier.product_width or self.change_supplier.product_height or self.change_supplier.product_diameter:
            length = str(self.change_supplier.product_length) + 'Lx'
            width = str(self.change_supplier.product_width) + 'Wx'
            height = str(self.change_supplier.product_height) + 'Hx'
            diameter = str(self.change_supplier.product_diameter) + 'D'
            item_dimensions = length + width + height + diameter
        else:
            item_dimensions = self.change_supplier.product_dimensions
        product_name = self.name or ''
        product_sku = self.product_sku or ''
        product_id = self.product_id and self.product_id.id or False
        product_uom_qty = self.product_uom_qty
        for seller in self.product_id.seller_ids:
            if seller.name.id == supplier_id:
                product_name = seller.product_name
                product_sku = seller.product_code
                vendor_link = seller.vendor_product_link
                break
        return item_dimensions, product_name, product_sku, product_id, product_uom_qty, vendor_link

    @api.multi
    def get_flag(self, vals=None):
        record = self
        flag = False
        if vals.get('change_supplier', False):
            if record.change_supplier.id != vals.get('change_supplier', False):
                flag = True
        if vals.get('product_uom_qty', False):
            if record.product_uom_qty != vals.get('product_uom_qty', False):
                flag = True
        if vals.get('product_id', False):
            if record.product_id.id != vals.get('product_id', False):
                flag = True
        if vals.get('product_sku', False):
            if record.product_sku != vals.get('product_sku', False):
                flag = True
        return flag

    def _get_real_price_currency(self, product, rule_id, qty, uom, pricelist_id):
        """Retrieve the price before applying the pricelist
            :param obj product: object of current product record
            :parem float qty: total quentity of product
            :param tuple price_and_rule: tuple(price, suitable_rule) coming from pricelist computation
            :param obj uom: unit of measure of current order line
            :param integer pricelist_id: pricelist id of sale order"""
        PricelistItem = self.env['product.pricelist.item']
        field_name = 'lst_price'
        currency_id = None
        if rule_id:
            pricelist_item = PricelistItem.browse(rule_id)
            if pricelist_item.base == 'standard_price':
                field_name = 'standard_price'
            currency_id = pricelist_item.pricelist_id.currency_id

        product_currency = (product.company_id and product.company_id.currency_id) or self.env.user.company_id.currency_id
        if 'promo_code' in self.env.context:
            product_currency = self.order_id.old_pricelist_id.currency_id
        if not currency_id:
            currency_id = product_currency
            cur_factor = 1.0
        else:
            if currency_id.id == product_currency.id:
                cur_factor = 1.0
            else:
                cur_factor = currency_id._get_conversion_rate(product_currency, currency_id)

        product_uom = self.env.context.get('uom') or product.uom_id.id
        if uom and uom.id != product_uom:
            # the unit price is in a different uom
            uom_factor = uom._compute_price(1.0, product.uom_id)
        else:
            uom_factor = 1.0
        if 'promo_code' in self.env.context:
            return self.price_unit * uom_factor * cur_factor, currency_id.id
        return product[field_name] * uom_factor * cur_factor, currency_id.id

    def get_dec_group_seq(self, dec_group_seq):
        grp_seq = [grp.group_sequence for grp in dec_group_seq]
        grp_seq_name = ", ".join(grp_seq)
        return grp_seq_name

    def artworksheet_test_for_checking_submitted(self):
        """To test that all artworksheets with the sale order line 
            are not in submitted state or after that states.
        """
        self.ensure_one()
        for decoration in self.decorations:
            if decoration.art_id:
                if decoration.art_id.state in ['submit', 'received', 'in_process', 'on_hold', 'completed']:
                    return False
        return True

class BaseConfigSettings(models.TransientModel):
    _inherit = 'base.config.settings'

    pdf_generate_enable = fields.Boolean(
        string='Enable PDF generate',
        help="""This allows to generate pdf on state change of SO/PO/Art.""")

    @api.model
    def get_default_pdf_generate_enable(self, fields):
        return {
            'pdf_generate_enable': self.env['ir.values'].get_default('base.config.settings', 'pdf_generate_enable')
        }

    @api.multi
    def set_default_pdf_generate_enable(self):
        IrValues = self.env['ir.values']
        if self.env['res.users'].has_group('base.group_erp_manager'):
            IrValues = IrValues.sudo()
        IrValues.set_default('base.config.settings', 'pdf_generate_enable', self.pdf_generate_enable)