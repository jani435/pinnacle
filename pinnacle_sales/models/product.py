# -*- coding: utf-8 -*-

from openerp import models, fields, api, _
from openerp.exceptions import UserError

import odoo.addons.decimal_precision as dp

def check_moq_validity(supplier=None, qty=None, min_qty=None, product=None, cost_price=None, sale_price=None, sample_order=None, spec_order=None):
    fee = False
    warning = False
    moq = supplier.o_moq if supplier.o_ltm_policy_id else supplier.moq
    other_qty = supplier.o_other_qty if supplier.o_ltm_policy_id else supplier.other_qty
    supplier_name = supplier.name.name
    item_cost = False
    item_price = False
    other_item_cost = False
    other_item_price = False
    validation = True
    qty = int(qty)
    if sample_order:
        return {'warning': warning, 'item_cost': item_cost, 'other_item_cost': other_item_cost, 'fee': fee, 
                    'item_price': item_price, 'other_item_price': other_item_price,}
    if not spec_order:
        if moq == 'not_allowed':
            if qty < min_qty:
                warning = ('\nProduct: %s, Vendor: %s, LTM not allowed.\n\n') % (product.name, supplier_name)
                fee = True
        if moq == 'half_column':
            half_qty = min_qty/2
            half_qty = half_qty if half_qty > 1 else 1
            if (qty > half_qty and qty < min_qty) or qty == half_qty:
                warning = ('The quantity entered for the product below is subject to LTM pricing. See product record for more details. \nProduct: %s, Vendor: %s \n\n') % (product.name, supplier_name)
                fee = True
            elif qty < half_qty:
                warning = ('\nProduct: %s, Vendor: %s, LTM not allowed.\n\n') % (product.name, supplier_name)
                fee = True
            else:
                validation = False
            if qty == min_qty and min_qty == half_qty:
                warning = False
                fee = False

        if moq == 'other':
            #if (qty > other_qty and qty < min_qty) or qty == other_qty:
            if (qty > other_qty and qty < min_qty):
                warning = ('The quantity entered for the product below is subject to LTM pricing. See product record for more details. \nProduct: %s, Vendor: %s \n\n') % (product.name, supplier_name)
                fee = True
            elif qty < other_qty:
                warning = ('\nProduct: %s, Vendor: %s, LTM not allowed.\n\n') % (product.name, supplier_name)
                fee = True
            else:
                validation = False
            if qty == min_qty and min_qty == other_qty:
                warning = False
                fee = False
    if cost_price:
        item_cost = supplier.o_item_cost if supplier.o_ltm_policy_id else supplier.item_cost
        other_item_cost = supplier.o_other_item_cost if supplier.o_ltm_policy_id else supplier.other_item_cost
        if spec_order:item_price, other_item_price = 0, 0
        if validation:
            return {'warning': warning, 'item_cost': item_cost, 'other_item_cost': other_item_cost, 'fee': fee}
        else:
            return {'warning': warning, 'item_cost': item_cost, 'other_item_cost': other_item_cost, 'fee': fee, 
                    'item_price': item_price, 'other_item_price': other_item_price}
    if sale_price:
        item_price = supplier.o_item_price if supplier.o_ltm_policy_id else supplier.item_price
        other_item_price = supplier.o_other_item_price if supplier.o_ltm_policy_id else supplier.other_item_price
        if spec_order:item_price, other_item_price = 0, 0
        if validation:
            return {'warning': warning, 'item_price': item_price, 'other_item_price': other_item_price, 'fee': fee}
        else:
            return {'warning': warning, 'item_cost': item_cost, 'other_item_cost': other_item_cost, 'fee': fee, 
                    'item_price': item_price, 'other_item_price': other_item_price}

class ProductTemplate(models.Model):
    _inherit = 'product.template'

    inventory_control_code = fields.Selection([('ds', 'DS'), ('inv_po', 'INV-PO'),
                                               ('inv_co', 'INV-CO'), ('inv_cs', 'INV-CS'),
                                               ('inv_podec', 'INV-PODEC'), ('inv_codec', 'INV-CODEC'),
                                               ('ds_nk', 'DS-NK'), ('inv_aa', 'INV-AA'), ('inv_hi', 'INV-HI'),
                                               ('inv_nk', 'INV-NK'), ('inv_pods', 'INV-PODS')],
                                              'Inventory Control Code', default="ds")
    
    def get_supplier(self, variant=None):
        ids = []
        preferred = False
        if not variant:
            ids = [seller.id for seller in self.seller_ids]
        else:
            for seller in self.seller_ids:
                for attrib in seller.attribute_vendor_attb_value_ids:
                    if attrib.product_id.id == variant.id:
                        ids.append(seller.id)
                    if seller.preferred_vendor:
                        preferred = seller.id
        return list(set(ids)), preferred

    def get_supplier_id(self, variant=None):
        ids = []
        supp_dict = {}
        preferred = False
        if not variant:
            ids = [seller.name.id for seller in self.seller_ids]
        else:
            for seller in self.seller_ids:
                for attrib in seller.attribute_vendor_attb_value_ids:
                    if attrib.product_id.id == variant.id:
                        ids.append(seller.name.id)
                        supp_dict[seller.name.id] = seller.id
                    if seller.preferred_vendor:
                        preferred = seller.name.id
        return list(set(ids)), preferred, supp_dict

    @api.multi
    def name_get(self):
        if not len(self):
            return []
        if self.env.context is None:
            self.env.context = {}
        if 'partner_id' in self.env.context:
            pass
        result = []
        for product in self:
            if product.product_sku:
                result.append(
                    (product.id, product.product_sku+" - "+product.name))
            else:
                result.append((product.id, product.name))
        return result

    def _get_tier(self, qty, supplier=None):
        if supplier is None:
            tier_loop = self.pricing_ids
        else:
            tier_loop = supplier
        tier = {}
        for i,rec in enumerate(tier_loop):
            if supplier is None:
                sale_price = rec.sales_price
            else:
                sale_price = rec.cost
            tier[i+1] = {
                        'min_qty' : rec.min_qty,
                        'sale_price' : sale_price
                        }
        limit = len(tier.keys())-1
        if limit == -1:return [0, 0]
        for index in tier:
            if limit >= index:
                next_record = tier[index + 1]
                tier[index]['max_qty'] = next_record['min_qty'] - 1
            else:
                tier[index]['max_qty'] = None
        
        for rec in tier:
            record =  tier[rec]
            if rec == 1 and record.get('min_qty') >= qty:
                #need to add addition price when qty is less than minimum
                return [record.get('sale_price'), rec]
            if record.get('min_qty') <= qty and record.get('max_qty') >= qty or record.get('max_qty') == None:
                return [record.get('sale_price'), rec]

    def get_sale_price(self, supplier, qty, sample_order=None, spec_order=None):
        res = {}
        tier = self._get_tier(qty)[0]
        warning = False
        min_qty = supplier.product_vendor_tier_ids[0].min_qty if supplier.product_vendor_tier_ids else 0
        price = 0.0
        res = check_moq_validity(supplier, qty, min_qty, self, False, True, sample_order=sample_order, spec_order=spec_order)
        if res.get('item_price') == 'other' and res.get('fee'):
            price += res.get('other_item_price', 0)
            return price
        else: 
            return self._get_tier(qty)[0]

    @api.multi
    def write(self, vals):
        if vals.get('type', False):
            if vals.get('type', False) == 'service':
                vals['invoice_policy'] = 'order'
        result = super(ProductTemplate, self).write(vals)
        return result

    @api.model
    def create(self, vals):
        if vals.get('type', False):
            if vals.get('type', False) == 'service':
                vals['invoice_policy'] = 'order'
        record = super(ProductTemplate, self).create(vals)
        return record

    @api.onchange('type')
    def _onchange_type(self):
        if self.type == 'service':
            self.invoice_policy = 'order'


class ProductProduct(models.Model):
    _inherit = 'product.product'

    sale_order_price_with_tax = fields.Float('Sale Order Taxed Total', digits=dp.get_precision('Product Price'))
    sale_order_price_without_tax = fields.Float('Sale Order Untaxed Total', digits=dp.get_precision('Product Price'))
    old_currency_id = fields.Many2one('res.currency', string='Old Currency')
    product_vendor_value_extend_ids = fields.One2many('product.vendor.value.extend', 'product_id', 'Supplier(s)')
    exclude_from_computing_sale_margins = fields.Boolean("Exclude From Sale Margins", help="By checking this checkbox, you agree that the product variant's prices are not considered while computing different margins in sale order.")

    def get_sale_price(self, supplier, qty):
        tier = self.product_tmpl_id._get_tier(qty)[1]
        warning = False
        if tier == 0:return tier
        attribute_value = self.env['attribute.value']
        addition_price = 0.00
        for attribute in self.attribute_value_ids.ids:
            rec = attribute_value.search([('tmpl_id','=',self.product_tmpl_id.id),('value_id','=', attribute)], limit=1)
            if rec.id:
                if tier == 1:addition_price = addition_price + rec.tier1
                elif tier == 2:addition_price = addition_price + rec.tier2
                elif tier == 3:addition_price = addition_price + rec.tier3
                elif tier == 4:addition_price = addition_price + rec.tier4
                elif tier == 5:addition_price = addition_price + rec.tier5
                elif tier == 6:addition_price = addition_price + rec.tier6
        return addition_price

    def get_cost_price(self, supplier, qty, only_addtion_cost=None, sample_order=None, spec_order=None):
        if supplier:
            res = {}
            tier = self.product_tmpl_id._get_tier(qty, supplier.product_vendor_tier_ids)
            min_qty = supplier.product_vendor_tier_ids[0].min_qty if supplier.product_vendor_tier_ids else 0
            cost = 0.0
            attribute_value = self.env['product.vendor.attributes.extend'].search([('product_supplierinfo_id','=', supplier.id),
                                            ('product_attribute_id','in',self.attribute_value_ids.ids)])
            res = check_moq_validity(supplier, qty, min_qty, self, True, sample_order=sample_order,spec_order=spec_order)
            warning = False
            if res.get('item_cost') == 'other' and res.get('fee'):
                cost += res.get('other_item_cost', 0)
            else:
                if only_addtion_cost is None:
                    cost = tier[0]
                else:
                    cost = 0.00
            if attribute_value.ids:
                if tier[1] == 1:
                    for attrib in attribute_value:
                        cost = cost + attrib.tier_cost1
                elif tier[1] == 2:
                    for attrib in attribute_value:
                        cost = cost + attrib.tier_cost2
                elif tier[1] == 3:
                    for attrib in attribute_value:
                        cost = cost + attrib.tier_cost3
                elif tier[1] == 4:
                    for attrib in attribute_value:
                        cost = cost + attrib.tier_cost4
                elif tier[1] == 5:
                    for attrib in attribute_value:
                        cost = cost + attrib.tier_cost5
                elif tier[1] == 6:
                    for attrib in attribute_value:
                        cost = cost + attrib.tier_cost6
            return {'cost': cost, 'fee': res.get('fee',0), 'warning': res.get('warning')}
            
    @api.model
    def name_search(self, name='', args=None, operator='ilike', limit=100):
        if not self.env.context.get('attribute_search') or not name:
            return super(ProductProduct, self).name_search(name=name, args=args, operator=operator, limit=limit)
        attrib_list = []
        current_variants = self.search(args)
        for sub_name in name.split(','):
            if sub_name:
                product_attribute_values = self.env['product.attribute.value'].search([('name', operator, sub_name.strip())])
                attrib_list.append(product_attribute_values.ids)
        product_list = []
        for val in current_variants:
            check = True
            for list_1 in attrib_list:
                if not len([id_1 for id_1 in val.attribute_value_ids.ids if id_1 in list_1]):
                    check = False
                    break
            if check:
                product_list.append(val.id)

        #Odoo Framework issue not searching in domain thats why manually filtering here
        final_list = []
        for kk in self.search([('id', 'in', product_list)], limit=limit).name_get():
            if kk[0] in product_list:
                final_list.append(kk)
        return final_list
       

    @api.multi
    def name_get(self):
        # Preserving what super class was meant to output.
        result = super(ProductProduct, self).name_get()
        is_blank = self.env.context.get('is_blank', False)
        if is_blank == False:
            if self.env.context.get('change_variant') and len(result):
                new_context = dict(self.env.context).copy()
                new_context.pop('change_variant')
                supplier = self.env['product.supplierinfo'].browse(self.env.context.get('change_variant'))
                products = []
                for prod in supplier.attribute_vendor_attb_value_ids:
                    if prod.product_id:
                        products.append([prod.product_id.id, prod.product_id.with_context(new_context).name_get()[0][1]])
                result = products
                return result
        if self.env.context.get('product_name_get'):
            customized_result = []
            for product in self:
                for record in result:
                    try:
                        if record[0] == product.id:
                            variant = ", ".join([v.attribute_id.name + ': ' +
                                          v.name for v in
                                          product.attribute_value_ids.
                                          sorted(key=lambda r: r.
                                                 attribute_id.id)])
                            if product.attribute_value_ids:
                                if self.env.context.get('product_full_name_get'):
                                    name =  "%s (%s)" % (product.name, variant) or product.name
                                    customized_result.append((product.id, name))
                                else:
                                    customized_result.append((product.id, variant))
                            else:
                                customized_result.append((product.id, record[1]))
                    except IndexError:
                        # Just pass code as custom result will be automatically
                        # returned as blank list.
                        pass
            return customized_result
        return result

    @api.multi
    def price_compute(self, price_type, uom=False, currency=False, company=False):
        # TDE FIXME: delegate to template or not ? fields are reencoded here ...
        # compatibility about context keys used a bit everywhere in the code
        if not uom and self._context.get('uom'):
            uom = self.env['product.uom'].browse(self._context['uom'])
        if not currency and self._context.get('currency'):
            currency = self.env['res.currency'].browse(self._context['currency'])

        products = self
        if price_type == 'standard_price':
            # standard_price field can only be seen by users in base.group_user
            # Thus, in order to compute the sale price from the cost for users not in this group
            # We fetch the standard price as the superuser
            products = self.with_context(force_company=company and company.id or self._context.get('force_company', self.env.user.company_id.id)).sudo()

        prices = dict.fromkeys(self.ids, 0.0)
        for product in products:
            prices[product.id] = product[price_type] or 0.0
            if price_type == 'list_price':
                prices[product.id] += product.price_extra
                if 'promo_code' in self.env.context:
                    if 'sale_order_line_id' in self.env.context:
                        sale_order_line = self.env['sale.order.line'].browse([self.env.context['sale_order_line_id']])
                        if sale_order_line:
                            prices[product.id] = sale_order_line[0].price_unit
            if uom:
                prices[product.id] = product.uom_id._compute_price(prices[product.id], uom)

            # Convert from current user company currency to asked one
            # This is right cause a field cannot be in more than one currency
            if currency:
                if 'promo_code' not in self.env.context:
                    prices[product.id] = product.old_currency_id.compute(prices[product.id], currency)
                else:
                    prices[product.id] = product.currency_id.compute(prices[product.id], currency)
        return prices

    @api.multi
    def read(self, fields=None, load='_classic_read'):
        result = super(ProductProduct, self).read(fields=fields, load=load)
        if self.env.context.get('tree_view_ref', False)  == 'pinnacle_sales.product_product_tree_view_for_add_an_item_in_order' and 'product_vendor_value_extend_ids' in fields:
            for rec in result:
                if rec.get('id' , False):
                    record = self.browse(rec['id'])
                    if record.product_vendor_value_extend_ids:
                        supplier_ids = []
                        for product_vendor_value_extend_id in record.product_vendor_value_extend_ids:
                            if product_vendor_value_extend_id.product_supplierinfo_id and product_vendor_value_extend_id.product_supplierinfo_id.name:
                                supplier_ids.append(product_vendor_value_extend_id.product_supplierinfo_id.name.id)
                        if supplier_ids:
                            supplier_ids = list(set(supplier_ids))
                            product_vendor_value_extend_suppliers = ''
                            for supplier_id in supplier_ids:
                                supplier_rec = self.env['res.partner'].browse([supplier_id])
                                if product_vendor_value_extend_suppliers:
                                    product_vendor_value_extend_suppliers = product_vendor_value_extend_suppliers + ',' + str(supplier_rec.name)
                                else:
                                    product_vendor_value_extend_suppliers = str(supplier_rec.name)
                            if product_vendor_value_extend_suppliers:
                                rec['product_vendor_value_extend_ids'] = product_vendor_value_extend_suppliers
        return result
