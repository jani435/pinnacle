from odoo import api, fields, models, _
from openerp.exceptions import UserError
import urllib2
import json

class MailComposeMessage(models.TransientModel):
    _inherit = 'mail.compose.message'

    @api.multi
    def send_mail(self, auto_commit=False):
        #15448 : SO - No email generation when Confirmation Received button clicked
        # if self._context.get('default_model') == 'sale.order' and self._context.get('default_res_id') and self._context.get('mark_confirmation_received'):
        #     order = self.env['sale.order'].browse([self._context['default_res_id']])
        #     if order.state == 'sent':
        #         order.write({'state': 'confirmation_received', 'confirmation_received': fields.datetime.now()})
        #     self = self.with_context(mail_post_autofollow=True)
        if self._context.get('default_model') == 'sale.order' and self._context.get('default_res_id') and self._context.get('mark_so_as_quote_sent'):
            order = self.env['sale.order'].browse([self._context['default_res_id']])
            # art_ids = []
            # for line in order.order_line:
            #     if line.art_id and line.art_id.id not in art_ids:
            #         art_ids.append(line.art_id.id)
            # if art_ids:
            #     art_browse_ids = self.env['art.production'].browse(art_ids)
            #     art_browse_ids.write({'status': 'virtual_spec'})
            order.write({'state': 'quote_sent', 'skip_send': True})
            self = self.with_context(mail_post_autofollow=True)
        #Update the status of Sales order to Pending Review if context value is Mark to sent 
        if self._context.get('default_model') == 'sale.order' and self._context.get('default_res_id') and self._context.get('mark_so_as_sent'):
            order = self.env['sale.order'].browse([self._context['default_res_id']])
            if order.state == 'pending_review':
                order.write({'state': 'sent', 'confirmation_sent': fields.datetime.now()})
            self = self.with_context(mail_post_autofollow=True)
        # if self._context.get('default_model') == 'art.production' and self._context.get('default_res_id') and self._context.get('art_id'):
        #     common().comment(self.env['art.production'].browse(self._context.get('art_id')))
        return super(MailComposeMessage, self).send_mail(auto_commit=auto_commit)


    @api.multi
    def send_mail_action(self):
        '''
        overridden method to set to draft in case of kick back AWS:Fix: When to Count Up#13158
        '''
        model = self.env.context.get('default_model')
        res_id = self.env.context.get('default_res_id')
        if model == 'art.production' and res_id and self.env.context.get('kick_back'):
            try:
                request_url = str(self.env['ir.config_parameter'].get_param('artq.web.url'))
                request_url += "Kickback&worksheet_id=%s" % res_id
                req = urllib2.Request(request_url)
                parents = urllib2.urlopen(req).read()
                response = json.loads(parents)
                if response != 'success':
                    raise UserError(_("Cannot update ArtQ from Odoo."))
            except urllib2.HTTPError:
                raise UserError(_("Cannot access ArtQ URL."))
            art_browse = self.env['art.production'].browse(res_id)
            art_browse.write({'state': 'draft', 'day_requested': False, 'time_requested': False, 'rush_request1': False,
                              'store_image': False, 'show_in_process': False, 'show_complete': False, 'ready1': False})
            return super(MailComposeMessage, self).send_mail_action()
        return super(MailComposeMessage, self).send_mail_action()
