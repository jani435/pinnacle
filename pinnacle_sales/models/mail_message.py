# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import logging
import base64

from email.utils import formataddr

from odoo import _, api, fields, models, SUPERUSER_ID, tools
from odoo.exceptions import UserError, AccessError
from odoo.osv import expression


_logger = logging.getLogger(__name__)

class MailFollowers(models.Model):
    _inherit = 'mail.followers'

    @api.model
    def create(self, vals):
        """Overwrite create method of Mail Followers.
        If User's Partner is customer (sales Order) , Vendor (Purchase Order) and Customer/Supplier
        In Invoice then restrict to add as a follower."""
        if self.env.context.get('mail_post_autofollow') == True:
            pass
        elif vals.get('res_model') == 'sale.order':
            so = self.env['sale.order'].search([('id', '=', vals.get('res_id'))])
            if so and so.partner_id.id == vals.get('partner_id'):
                pass
            else:
                return super(MailFollowers, self).create(vals)
        elif vals.get('res_model') == 'purchase.order':
            so = self.env['purchase.order'].search([('id', '=', vals.get('res_id'))])
            if so and so.partner_id.id == vals.get('partner_id'):
                pass
            else:
                return super(MailFollowers, self).create(vals)
        elif vals.get('res_model') == 'account.invoice':
            so = self.env['account.invoice'].search([('id', '=', vals.get('res_id'))])
            if so and so.partner_id.id == vals.get('partner_id'):
                pass
            else:
                return super(MailFollowers, self).create(vals)
        else:
            return super(MailFollowers, self).create(vals)

class Message(models.Model):
    _inherit = 'mail.message'

    @api.multi
    def _notify(self, force_send=False, send_after_commit=True, user_signature=True):
        """ Add the related record followers to the destination partner_ids if is not a private message.
            Call mail_notification.notify to manage the email sending
        """
        group_user = self.env.ref('base.group_user')
        # have a sudoed copy to manipulate partners (public can go here with 
        # website modules like forum / blog / ...
        self_sudo = self.sudo()

        # TDE CHECK: add partners / channels as arguments to be able to notify a message with / without computation ??
        self.ensure_one()  # tde: not sure, just for testinh, will see
        partners = self.env['res.partner'] | self.partner_ids
        channels = self.env['mail.channel'] | self.channel_ids

        # all followers of the mail.message document have to be added as partners and notified
        # and filter to employees only if the subtype is internal
        if self_sudo.subtype_id and self.model and self.res_id:
            followers = self.env['mail.followers'].sudo().search([
                ('res_model', '=', self.model),
                ('res_id', '=', self.res_id)
            ]).filtered(lambda fol: self.subtype_id in fol.subtype_ids)
            if self_sudo.subtype_id.internal:
                followers = followers.filtered(lambda fol: fol.channel_id or (fol.partner_id.user_ids and group_user in fol.partner_id.user_ids[0].mapped('groups_id')))
            if not self._context.get('remove_followers_of_the_document', False):
                channels = self_sudo.channel_ids | followers.mapped('channel_id')
                partners = self_sudo.partner_ids | followers.mapped('partner_id')
            else:
                channels = self_sudo.channel_ids
                partners = self_sudo.partner_ids
        else:
            channels = self_sudo.channel_ids
            partners = self_sudo.partner_ids

        # remove author from notified partners
        if not self._context.get('mail_notify_author', False) and self_sudo.author_id:
            partners = partners - self_sudo.author_id

        # update message, with maybe custom values
        message_values = {
            'channel_ids': [(6, 0, channels.ids)],
            'needaction_partner_ids': [(6, 0, partners.ids)]
        }
        if self.model and self.res_id and hasattr(self.env[self.model], 'message_get_message_notify_values'):
            message_values.update(self.env[self.model].browse(self.res_id).message_get_message_notify_values(self, message_values))
        self.write(message_values)

        # notify partners and channels
        partners._notify(self, force_send=force_send, send_after_commit=send_after_commit, user_signature=user_signature)
        channels._notify(self)

        # Discard cache, because child / parent allow reading and therefore
        # change access rights.
        if self.parent_id:
            self.parent_id.invalidate_cache()

        return True

    @api.model
    def create(self, values):
        res = super(Message, self).create(values)
        sale_id = self.env.context.get('sale_id')
        art_status = {'draft': 'Draft',
                      'submit': 'Submitted',
                      'received': 'Received',
                      'in_process': 'In Progress',
                      'on_hold': 'On Hold',
                      'completed': 'Completed',
                      'archive': 'Archived',
                      'hold': 'On Hold'}
        if self.env.context.get('msg_post') and sale_id and values.get('model') in ['art.production', 'purchase.order']:
            model = values.get('model')
            rec = self.env[model].browse(values.get('res_id'))
            report_name = ''
            result = False
            if model == 'art.production':

                body = '<b>Art Worksheet Job: %s</b>:<br />' % rec.art_sequence
                body += values.get('body',
                                   '') + "<b>Initial Designer: </b>%s<br/><b>In hands Designer: </b>%s<br/><b>Status: </b>%s<br /><b>Changed By: </b>%s" % (
                    rec.initial_designer_id.name, rec.designer_id.name, art_status.get(rec.state),
                    self.env.user.name)
                report_name = 'Art Worksheet'
                ctx = self.env.context.copy()
                ctx.update({'active_model': model,
                            'active_ids': [rec.id], 'active_id': rec.id})
                result, format = self.env['report'].sudo().with_context(ctx).get_pdf(rec._ids,
                                                                                  'pinnacle_sales.report_art_worksheet'), 'pdf'
                ext = "." + format
                if not report_name.endswith(ext):
                    report_name += ext
            else:
                body = '<b>%s</b>: .<br />' % rec.name
                body += values.get('body')
            attach_ids = []
            for attach in values.get('attachment_ids'):
                attach_ids.append(attach[1])
            attach_obj = self.env['ir.attachment']
            attach_rec = attach_obj.browse(attach_ids)
            attachments = [(a['datas_fname'], base64.b64decode(a['datas']))
                           for a in attach_rec.sudo().read(['datas_fname', 'datas'])]
            if report_name and result:
                attachments.append((report_name, result))
            if model == 'art.production':
                rec.sale_id.message_post(body=body, attachments=attachments)
            else:
                rec.sale_order_id.message_post(body=body, attachments=attachments)
            return res
        return res
