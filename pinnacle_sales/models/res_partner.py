from openerp import models, fields, api, _
from odoo.exceptions import ValidationError,UserError, Warning

class ResPartner(models.Model):
    _inherit = 'res.partner'

    @api.multi
    def _get_default_country(self):
        country = self.env['res.country'].search([('code', '=', 'US')])
        return country

    customer_type = fields.Selection([('new', 'New Customer'),
                                      ('current', 'Current Customer'),
                                      ('program', 'Program')], default='new')
    agreement_filename = fields.Char()
    agreement_file = fields.Binary('Agreement')
    country_id = fields.Many2one('res.country', string='Country', ondelete='restrict', default=_get_default_country)
    not_account_user = fields.Boolean('Not Account User', compute='_check_user', default=False)
    send_email_of_sale_invoice = fields.Boolean(string="Allow Account Team to Invoice", default=True)
    program_id = fields.Many2one('partner.program', 'Program')
    channel_id = fields.Many2one('partner.channel', 'Channel')
    previous_order = fields.Boolean('Previous Order')
    partner_non_taxable = fields.Boolean('Non-Taxable')
    analytic_tag_ids = fields.Many2many('account.analytic.tag', string='Analytic Tags')
    analytic_contact_tag_ids = fields.Many2many('account.analytic.tag', string='Analytic Tags', compute='_get_contact_analytic_tag', inverse='_set_contact_analytic_tag')
    channel_contact_id = fields.Many2one('partner.channel', string='Channel', compute='_get_contact_channel', inverse='_set_contact_channel')
    program_contact_id = fields.Many2one('partner.program', string='Program', compute='_get_contact_program', inverse='_set_contact_program')

    is_payment_term_endwith_po = fields.Boolean(compute='_check_payment_term_endwith_po')
    is_customer_po_id_required = fields.Boolean(string="Is Customer PO ID Required?", default=True)
    is_customer_po_file_required = fields.Boolean(string="Is Customer PO File Required?", default=True)
    customer_contact_po_id_required = fields.Boolean(string="Is Customer PO ID Required?", compute='_check_customer_contact_po_id_required', inverse='_set_customer_contact_po_id_required')
    customer_contact_po_file_required = fields.Boolean(string="Is Customer PO File Required?", compute='_check_customer_contact_po_file_required', inverse='_set_customer_contact_po_file_required')
    override_payment_term = fields.Boolean('Override Payment Term', default=True)
    fedex_customer_account = fields.Char('FedEx Customer Shipping Account', size=9)
    ups_customer_account = fields.Char('UPS Customer Shipping Account', size=6)
    type = fields.Selection(
        [('contact', 'Contact'),
         ('invoice', 'Invoice address'),
         ('delivery', 'Shipping address'),
         ('other', 'Rush address'),
         ('other_addres', 'Other address')], string='Address Type',
        default='contact',
        help="Used to select automatically the right address according to the context in sales and purchases documents.")
    primary_contact = fields.Boolean('Is a primary contact ?', default=False)
    street = fields.Char(size=30)
    street2 = fields.Char(size=30)
    zip = fields.Char(change_default=True, size=10)
    city = fields.Char(size=30)

    @api.onchange('program_id', 'channel_id')
    def _onchange_for_pulling_analytic_tag_ids(self):
        analytic_tag_ids = []
        if self.program_id and self.program_id.analytic_tag_ids:
            analytic_tag_ids = self.program_id.analytic_tag_ids.ids
        if self.channel_id and self.channel_id.analytic_tag_ids:
            analytic_tag_ids += self.channel_id.analytic_tag_ids.ids
        self.analytic_tag_ids = [[6, 0, analytic_tag_ids]]

    @api.one
    def _check_user(self):
        """Computes values for not an account user flag.
        If user does not contain any group belongs to Account than set value of not_account_user False
        otherwise set True"""
        group_categ = self.env.ref('base.module_category_accounting_and_finance')
        acc_groups = self.env['res.groups'].search([('category_id', '=', group_categ.id)])
        groups_user = self.env.user.groups_id
        flag = True
        for acc_group in acc_groups:
            if acc_group in groups_user:
                flag = False
        self.not_account_user = flag

    @api.onchange('property_payment_term_id')
    def onchange_payment_term(self):
        if not self.is_payment_term_endwith_po:
            self.is_customer_po_id_required = True
            self.is_customer_po_file_required = True

    def _check_customer_contact_po_id_required(self):
        for partner in self:
            partner.customer_contact_po_id_required = partner.is_customer_po_id_required

    def _set_customer_contact_po_id_required(self):
        for partner in self:
            partner.is_customer_po_id_required = partner.customer_contact_po_id_required

    def _check_customer_contact_po_file_required(self):
        for partner in self:
            partner.customer_contact_po_file_required = partner.is_customer_po_file_required

    def _set_customer_contact_po_file_required(self):
        for partner in self:
            partner.is_customer_po_file_required = partner.customer_contact_po_file_required

    @api.one
    @api.depends('program_id')
    def _get_contact_program(self):
        self.program_contact_id = self.program_id.id

    @api.one
    def _set_contact_program(self):
        self.program_id = self.program_contact_id.id

    @api.one
    @api.depends('channel_id')
    def _get_contact_channel(self):
        self.channel_contact_id = self.channel_id.id

    @api.one
    def _set_contact_channel(self):
        self.channel_id = self.channel_contact_id.id

    @api.one
    @api.depends('analytic_tag_ids')
    def _get_contact_analytic_tag(self):
        self.analytic_contact_tag_ids = self.analytic_tag_ids.ids

    @api.one
    def _set_contact_analytic_tag(self):
        self.analytic_tag_ids = [(6, 0, self.analytic_contact_tag_ids.ids)]

    @api.one
    @api.constrains('analytic_tag_ids')
    def check_anaytics_tag_length(self):
        if len(self.analytic_tag_ids.ids) > 2:
            raise ValidationError('You cannot add more then two Analytic Tags')

    @api.onchange('customer')
    def onchange_customer(self):
        if self.customer:
            payment_term = self.env['account.payment.term'].search([('name', '=', '100% Prepay Credit Card')], limit=1)
            if payment_term:
                self.property_payment_term_id = payment_term.id

    @api.onchange('parent_id')
    def _onchange_parent_id(self):
        if self.parent_id:
            self.program_id = self.parent_id.program_id.id
            self.channel_id = self.parent_id.channel_id.id
            self.user_id = self.parent_id.user_id.id
            self.send_email_of_sale_invoice = self.parent_id.send_email_of_sale_invoice
            self.analytic_contact_tag_ids = self.parent_id.analytic_contact_tag_ids.ids
            self.customer = self.parent_id.customer
            self.supplier = self.parent_id.supplier

    @api.multi
    def name_get(self):
        res = []
        product_finishers = self.env.context.get('product_finishers', [])        
        for partner in self:
            if 'partner_name' in partner._context:
                if partner.parent_id:
                    company  = partner.parent_id.name+', '
                else:
                    company = ''
                if partner.email:
                    email = ', '+partner.email
                else:
                    email = ''
                if partner.name:
                    partner_name = partner.name
                elif partner.parent_id and not partner.name:
                    partner_name = partner.parent_id.name
                else:
                    partner_name = ''
                res.append([partner.id, "%s" % (company+partner_name+email)])
            elif 'ship_to' in partner._context:
                partner_name = ''
                street = ''
                if partner.type == 'delivery':
                    if partner.name:
                        partner_name = partner.name
                    else:
                        partner_name = partner.parent_id.name
                    if partner.street:
                        street = ', '+partner.street
                    else:
                        if partner.parent_id.street:
                            street = ', '+partner.parent_id.street    
                    res.append([partner.id, "%s" % (partner_name+street)])
                    return res
                else:
                    if partner.name:
                        partner_name = partner.name
                    elif partner.parent_id and not partner.name:
                        partner_name = partner.parent_id.name
                    else:
                        partner_name = ''

                if partner.parent_id:
                    company  = partner.parent_id.name+', '
                else:
                    company = ''
                if partner.street:
                    street = ', '+partner.street
                else:
                    street = ''
                res.append([partner.id, "%s" % (company+partner_name+street)])
            elif product_finishers:
                name = self._rec_name
                if name in self._fields:
                    convert = self._fields[name].convert_to_display_name
                    if partner.id in product_finishers:
                        res.append((partner.id, convert(partner[name], partner) + " (Product)"))
                    else:
                        res.append((partner.id, convert(partner[name], partner)))
                else:
                    if partner.id in product_finishers:
                        res.append((partner.id, "%s,%s (Product)" % (partner._name, partner.id)))
                    else:
                        res.append((partner.id, "%s,%s" % (partner._name, partner.id)))
            elif 'clr_partner' in partner._context:
                res.append([partner.id, "%s" % (partner.name)])
            else:
                res = super(ResPartner, self).name_get()
        return res

    @api.model
    def create(self, vals):
        if vals.get('parent_id', False):
            parent = self.browse([vals.get('parent_id', False)])
            if parent:
                if not vals.get('program_id', False): 
                    vals['program_id'] = parent.program_id.id
                if not vals.get('channel_id', False): 
                    vals['channel_id'] = parent.channel_id.id
                if not vals.get('user_id', False): 
                    vals['user_id'] = parent.user_id.id
                if not vals.get('analytic_contact_tag_ids', False): 
                    vals['analytic_contact_tag_ids'] = parent.analytic_contact_tag_ids.ids
                vals['send_email_of_sale_invoice'] = parent.send_email_of_sale_invoice
                if vals.get('primary_contact', False):
                    for child_id in parent.child_ids:
                        if child_id.primary_contact:
                            raise UserError('You can not set more than one primary contact for a partner.\n Here '+str(child_id.name)+' is already set as a primary contact for '+str(parent.name)+'.')
        result = super(ResPartner, self).create(vals)
        return result

    @api.multi
    def write(self, vals):
        if vals.get('primary_contact', False) or vals.get('parent_id', False):
            for partner in self:
                parent_partner = False
                if vals.get('parent_id', False):
                    parent_partner = self.browse([vals.get('parent_id', False)])
                else:
                    parent_partner = partner.parent_id or False
                if parent_partner:
                    for child_id in parent_partner.child_ids:
                        if child_id.primary_contact and vals.get('primary_contact', False):
                            raise UserError('You can not set more than one primary contact for a partner.\n Here '+str(child_id.name)+' is already set as a primary contact for '+str(parent_partner.name)+'.')
        result = super(ResPartner, self).write(vals)
        return result

    @api.multi
    def unlink(self):
        """Overwrite Unlink method of Partner.
        If user have a Administration Setting rights than user can delete the product.
        For other users it will raise warning like product cannot be deleted."""
        group = self.env.ref('base.group_system')
        groups = self.env.user.groups_id
        if group not in groups:
            raise Warning(_('Contact cannot be deleted.'))
        return super(ResPartner, self).unlink()

    @api.model
    def name_search(self, name='', args=None, operator='ilike', limit=100):
        product_finishers = []
        if self.env.context.get('finisher', False) and self.env.context.get('always_show_all_finisher', False):
            try:
                args_for_manipulation = [arg for arg in args]
                if args_for_manipulation[len(args_for_manipulation)-3] == '|':
                    product_finishers = args_for_manipulation[len(args_for_manipulation)-1][2]
            except Exception:
                return super(ResPartner, self).name_search(name=name, args=args, operator=operator, limit=limit)
        result = super(ResPartner, (self.with_context(product_finishers=product_finishers))).name_search(name=name, args=args, operator=operator, limit=limit)
        return result

    @api.model
    def search_read(self, domain=None, fields=None, offset=0, limit=None, order=None):
        """
        Performs a ``search()`` followed by a ``read()``.

        :param domain: Search domain, see ``args`` parameter in ``search()``. Defaults to an empty domain that will match all records.
        :param fields: List of fields to read, see ``fields`` parameter in ``read()``. Defaults to all fields.
        :param offset: Number of records to skip, see ``offset`` parameter in ``search()``. Defaults to 0.
        :param limit: Maximum number of records to return, see ``limit`` parameter in ``search()``. Defaults to no limit.
        :param order: Columns to sort result, see ``order`` parameter in ``search()``. Defaults to no sort.
        :return: List of dictionaries containing the asked fields.
        :rtype: List of dictionaries.

        """
        records = self.search(domain or [], offset=offset, limit=limit, order=order)
        if not records:
            return []

        if fields and fields == ['id']:
            # shortcut read if we only want the ids
            return [{'id': record.id} for record in records]

        # read() ignores active_test, but it would forward it to any downstream search call
        # (e.g. for x2m or function fields), and this is not the desired behavior, the flag
        # was presumably only meant for the main search().
        # TODO: Move this to read() directly?
        if 'active_test' in self._context:
            context = dict(self._context)
            del context['active_test']
            records = records.with_context(context)

        result = records.read(fields)
        if len(result) <= 1:
            return result

        # reorder read
        index = {vals['id']: vals for vals in result}
        result = [index[record.id] for record in records if record.id in index]
        if self.env.context.get('finisher', False) and self._context.get('always_show_all_finisher', False):
            product_finishers = []
            try:
                if domain[len(domain)-3] == '|':
                    product_finishers = domain[len(domain)-1][2]
            except Exception:
                return result
            for record in result:
                if record.get('id', False) and record['id'] in product_finishers:
                    if record.get('name', False):
                        record['name'] += " (Product)"
                    if record.get('display_name', False):
                        record['display_name'] += " (Product)"
        return result

    @api.model
    def _search(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):
        """
        Private implementation of search() method, allowing specifying the uid to use for the access right check.
        This is useful for example when filling in the selection list for a drop-down and avoiding access rights errors,
        by specifying ``access_rights_uid=1`` to bypass access rights check, but not ir.rules!
        This is ok at the security level because this method is private and not callable through XML-RPC.

        :param access_rights_uid: optional user ID to use when checking access rights
                                  (not for ir.rules, this is only for ir.model.access)
        :return: a list of record ids or an integer (if count is True)
        """
        self.sudo(access_rights_uid or self._uid).check_access_rights('read')

        # For transient models, restrict access to the current user, except for the super-user
        if self.is_transient() and self._log_access and self._uid != SUPERUSER_ID:
            args = expression.AND(([('create_uid', '=', self._uid)], args or []))

        query = self._where_calc(args)
        self._apply_ir_rules(query, 'read')
        order_by = self._generate_order_by(order, query)
        from_clause, where_clause, where_clause_params = query.get_sql()

        where_str = where_clause and (" WHERE %s" % where_clause) or ''

        if count:
            # Ignore order, limit and offset when just counting, they don't make sense and could
            # hurt performance
            query_str = 'SELECT count(1) FROM ' + from_clause + where_str
            self._cr.execute(query_str, where_clause_params)
            res = self._cr.fetchone()
            return res[0]

        limit_str = limit and ' limit %d' % limit or ''
        offset_str = offset and ' offset %d' % offset or ''
        query_str = 'SELECT "%s".id FROM ' % self._table + from_clause + where_str + order_by + limit_str + offset_str
        if self.env.context.get('finisher', False) and self._context.get('always_show_all_finisher', False):
            try:
                temp_where_str = where_str[(where_str.rindex("OR") + 4) : len(where_str)]
                if temp_where_str.find('FALSE') == -1:
                    second_temp_where_str = temp_where_str[0 : temp_where_str.index(")") + 2]
                    order_by_case = " ORDER BY case when " + second_temp_where_str + " then 1 else 2 end, " + order_by[order_by.index("BY")+3: -1]
                    where_str_for_order_by_case = where_str[0 : (where_str.rindex("OR")) - 2] + where_str[where_str.rindex("OR") + 4 + len(second_temp_where_str) : len(where_str)]
                    query_str = 'SELECT "%s".id FROM ' % self._table + from_clause + where_str_for_order_by_case + order_by_case + limit_str + offset_str
            except Exception:
                query_str = 'SELECT "%s".id FROM ' % self._table + from_clause + where_str + order_by + limit_str + offset_str
        self._cr.execute(query_str, where_clause_params)
        res = self._cr.fetchall()

        # TDE note: with auto_join, we could have several lines about the same result
        # i.e. a lead with several unread messages; we uniquify the result using
        # a fast way to do it while preserving order (http://www.peterbe.com/plog/uniqifiers-benchmark)
        def _uniquify_list(seq):
            seen = set()
            return [x for x in seq if x not in seen and not seen.add(x)]

        return _uniquify_list([x[0] for x in res])
