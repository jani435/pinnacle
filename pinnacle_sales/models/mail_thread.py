# -*- coding: utf-8 -*-

import base64
import datetime
import dateutil
import email
import hashlib
import hmac
import lxml
import logging
import pytz
import re
import socket
import time
import xmlrpclib

from collections import namedtuple
from email.message import Message
from email.utils import formataddr
from lxml import etree
from werkzeug import url_encode

from odoo import _, api, exceptions, fields, models, tools
from odoo.tools.safe_eval import safe_eval


_logger = logging.getLogger(__name__)


class MailThread(models.AbstractModel):
    _inherit = 'mail.thread'

    @api.multi
    def message_get_recipient_values(self, notif_message=None, recipient_ids=None):
        if self._context.get('remove_followers_of_the_document', False):
            if notif_message:
                return {
                    'recipient_ids': ([(4, rid.id) for rid in notif_message.partner_ids]),
                    'cc_recipient_ids': [(4, ccrid.id) for ccrid in notif_message.cc_partner_ids],
                    'bcc_recipient_ids': [(4, bccrid.id) for bccrid in notif_message.bcc_partner_ids],
                }
        else:
            return super(MailThread, self).message_get_recipient_values(notif_message, recipient_ids)
