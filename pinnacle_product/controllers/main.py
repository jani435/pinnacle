import base64
import json
import logging
import odoo
import functools
import werkzeug.utils
import werkzeug.wrappers
import odoo.modules.registry
from odoo.tools.translate import _
from openerp import http
from odoo.http import request, serialize_exception as _serialize_exception
from odoo.addons import web

_logger = logging.getLogger(__name__)

def serialize_exception(f):
    @functools.wraps(f)
    def wrap(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except Exception, e:
            _logger.exception("An exception occured during an http request")
            se = _serialize_exception(e)
            error = {
                'code': 200,
                'message': "Odoo Server Error",
                'data': se
            }
            return werkzeug.exceptions.InternalServerError(json.dumps(error))
    return wrap

class Binary(web.controllers.main.Binary):
    
    @http.route('/web/binary/upload_attachment', type='http', auth="user")
    @serialize_exception
    def upload_attachment(self, callback, model, id, ufile, multi=False, cfs_attachment=False, field_name=False):
        if cfs_attachment == 'true':
            ctx = dict(request.context)
            ctx.update({'cfs_attachment': True})
            request.context = ctx
        vals = {}
        Model = request.env['ir.attachment']
        out = """<script language="javascript" type="text/javascript">
                    var win = window.top.window;
                    win.jQuery(win).trigger(%s, %s);
                </script>"""
        try:
            vals = {
                    'name': ufile.filename,
                    'datas': base64.encodestring(ufile.read()),
                    'datas_fname': ufile.filename,
                    'res_model': model,
                    'res_id': int(id),
                }
            if cfs_attachment == 'true':  
                if field_name:
                    if model == 'purchase.order':
                        vals['res_model'] = 'sale.order.line.decoration'
                        art_line = request.env['sale.order.line.decoration'].browse(int(id))
                    else:
                        vals['res_model'] = 'art.production.line'
                        art_line = request.env['art.production.line'].browse(int(id))
                    if field_name == 'spec_file_url':
                        vals['res_field'] = 'spec_file'
                        art_line.spec_file_name = ufile.filename
                    if field_name == 'spec_file2_url':
                        vals['res_field'] = 'spec_file2'
                        art_line.spec_file2_name = ufile.filename
                    if field_name == 'vendor_file_url':
                        vals['res_field'] = 'vendor_file'
                        art_line.vendor_file_name = ufile.filename
            attachment = Model.create(vals)
            args = {
                'filename': ufile.filename,
                'mimetype': ufile.mimetype,
                'id':  attachment.id
            }
        except Exception:
            args = {'error': _("Something horrible happened")}
            _logger.exception("Fail to upload attachment %s" % ufile.filename)
        if multi:
            return str(attachment.id)
        else:
            return out % (json.dumps(callback), json.dumps(args))

    @http.route('/web/upload_product_multi_images', type='http', auth="user")
    @serialize_exception
    def upload_multi_images(self, callback, model, id, ufile):
        try:
            model = request.env['product.template.multi.images']
            ctx = dict(request.context)
            ctx.update({'cfs_attachment': True})
            request.context = ctx
            res = model.create({'product_tmpl_id': int(id), 'image_filename': ufile.filename})
            vals = {
                        'name': ufile.filename,
                        'datas': base64.encodestring(ufile.read()),
                        'datas_fname': ufile.filename,
                        'res_model': 'product.template.multi.images',
                        'res_id': res.id,
                        'res_field': 'image',
                        'cfs_attachment': True,
                    }
            attachment = request.env['ir.attachment'].create(vals)
        except Exception:
            _logger.exception("Fail to upload attachment %s" % ufile.filename)
        return str(res.id)
