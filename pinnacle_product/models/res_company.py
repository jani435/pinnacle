# -*- coding: utf-8 -*-

from openerp import api, fields, models, _
from openerp.exceptions import UserError, ValidationError
import datetime
from datetime import date
from dateutil.relativedelta import relativedelta
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT

import odoo.addons.decimal_precision as dp
class ResCompany(models.Model):
    _inherit = 'res.company'

    invoice_email = fields.Char('Invoices Email')
