# -*- coding: utf-8 -*-
##############################################################################
#
#  Bista Solutions Inc.
#  Website: www.bistasolutions.com
#
##############################################################################
from openerp import api, fields, models, _
from openerp.exceptions import UserError


###########################################################
# Product Properties
###########################################################
class ProductPropertyGroup(models.Model):
    _name = 'product.property.group'
    _description = 'Property group'

    sequence = fields.Integer('Sequence', help="Determine the display order")
    name = fields.Char('Property Group')
    property_ids = fields.One2many('product.property', 'group_id', string='Properties')

    @api.multi
    def unlink(self):
        context = self._context or {}
        ctx = dict(context or {}, active_test=False)
        product_ids = self.env['product.template'].with_context(ctx).search(
            [('property_line_ids.property_id.group_id', 'in', self._ids)])
        if product_ids:
            raise UserError(_('The operation cannot be completed. You are trying to delete a property group with a reference on a product.'))
        return super(ProductPropertyGroup, self).unlink()

class ProductProperty(models.Model):
    _name = "product.property"
    _description = "Product Property"
    _order = 'sequence, name'

    name = fields.Char('Name', translate=True, required=True)
    value_ids = fields.One2many('product.property.value', 'property_id', 'Values', copy=True)
    sequence = fields.Integer('Sequence', help="Determine the display order")
    property_line_ids = fields.One2many('product.property.line', 'property_id', 'Lines')
    group_id = fields.Many2one('product.property.group', 'Property Group')

    @api.multi
    def unlink(self):
        context = self._context or {}
        ctx = dict(context or {}, active_test=False)
        product_ids = self.env['product.template'].with_context(ctx).search(
            [('property_line_ids.property_id', 'in', self._ids)])
        if product_ids:
            raise UserError(_('The operation cannot be completed. You are trying to delete a property with a reference on a product.'))
        return super(ProductProperty, self).unlink()

    @api.multi
    def name_get(self):
        context = self._context or {}
        if context and not context.get('show_group', True):
            return super(ProductProperty, self).name_get()
        res = []
        for prop in self:
            res.append([prop.id, "%s: %s" % (prop.group_id.name, prop.name)])
        return res

class ProductPropertyValue(models.Model):
    _name = "product.property.value"
    _order = 'sequence'

    @api.multi
    def name_get(self):
        context = self._context or {}
        if context and not context.get('show_property', True):
            return super(ProductPropertyValue, self).name_get()
        res = []
        for value in self:
            res.append([value.id, "%s: %s" % (value.property_id.name, value.name)])
        return res

    sequence = fields.Integer('Sequence', help="Determine the display order")
    name = fields.Char('Value', translate=True, required=True)
    property_id = fields.Many2one('product.property', 'Property', required=True, ondelete='cascade')
    product_ids = fields.Many2many('product.product', id1='prop_id', id2='prod_id', string='Variants',
                                   readonly=True)
    _sql_constraints = [
        ('pro_value_company_uniq', 'unique (name,property_id)', 'This property value already exists !')
    ]

    @api.multi
    def unlink(self):
        context = self._context or {}
        ctx = dict(context or {}, active_test=False)
        product_ids = self.env['product.template'].with_context(ctx).search(
            [('property_line_ids.value_ids', 'in', self._ids)])
        if product_ids:
            raise UserError(_('The operation cannot be completed. You are trying to delete a property value with a reference on a product.'))
        return super(ProductPropertyValue, self).unlink()


class ProductPropertyLine(models.Model):
    _name = "product.property.line"
    _rec_name = 'property_id'

    product_tmpl_id = fields.Many2one('product.template', 'Product Template', required=True, ondelete='cascade')
    property_id = fields.Many2one('product.property', 'Property', required=True, ondelete='restrict')
    value_ids = fields.Many2many('product.property.value', id1='line_id', id2='val_id', string='Property Values')

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        # TDE FIXME: currently overriding the domain; however as it includes a
        # search on a m2o and one on a m2m, probably this will quickly become
        # difficult to compute - check if performance optimization is required
        if name and operator in ('=', 'ilike', '=ilike', 'like', '=like'):
            new_args = ['|', ('property_id', operator, name), ('value_ids', operator, name)]
        else:
            new_args = args
        return new_args.name_get()
