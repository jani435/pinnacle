# -*- coding: utf-8 -*-
##############################################################################
#
#  Bista Solutions Inc.
#  Website: www.bistasolutions.com
#
##############################################################################
import lxml
import datetime
import odoo.addons.decimal_precision as dp

from openerp import api, fields, models, _
from openerp.exceptions import UserError, ValidationError, Warning
from datetime import date
from dateutil.relativedelta import relativedelta
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
from lxml import etree
import urllib2
import base64
import urllib
import urlparse

def url_fix(s, charset='utf-8'):
    if isinstance(s, unicode):
        s = s.encode(charset, 'ignore')
    scheme, netloc, path, qs, anchor = urlparse.urlsplit(s)
    path = urllib.quote(path, '/%')
    qs = urllib.quote_plus(qs, ':&=')
    return urlparse.urlunsplit((scheme, netloc, path, qs, anchor))



class ProductPricing(models.Model):
    _name = 'product.pricing'
    _description = 'Product Pricing'

    min_qty = fields.Float('Minimum Quantity')
    sales_price = fields.Float('Sales Price')
    msrp_price = fields.Float('MSRP')
    product_id = fields.Many2one('product.template', 'Product Template')
    preferred_vendor = fields.Boolean(related='product_id.preferred_vendor', string='Preferred Vendor')
    tier_sequence = fields.Integer(string="Tier")

    @api.onchange('min_qty', 'sales_price', 'msrp_price')
    def _onchange_pricing(self):
        if self.product_id.preferred_vendor:
            self.preferred_vendor = False

class Product(models.Model):
    _inherit = 'product.template'

    sales_count = fields.Integer(compute='_sales_count', string='# Sales', store=True)
    seller_ids = fields.One2many('product.supplierinfo', 'product_tmpl_id', 'Vendors')
    seller_name = fields.Char(compute='_calculate_seller_name', string="Vendors")
    attribute_line_ids = fields.One2many('product.attribute.line', 'product_tmpl_id', 'Product Attributes')
    create_date = fields.Datetime('Created on', index=True, readonly=True, track_visibility='onchange')
    create_uid = fields.Many2one('res.users', string='Created by', index=True, readonly=True, track_visibility='onchange')
    write_date = fields.Datetime('Update on', index=True, readonly=True, track_visibility='onchange')
    write_uid = fields.Many2one('res.users', string='Updated by', index=True, readonly=True, track_visibility='always')
    #15147 : Product - Map images in images tab to specific attribute
    #multi_image = fields.Many2many('ir.attachment')
    multiple_images = fields.One2many('product.template.multi.images', 'product_tmpl_id', string="Multiple Images")

    #Warehouse Tab 
    warehouse_action = fields.Selection([('Add Item','Add Item'),('Change Item','Change Item')], string="Action")
    warehouse_color_size_options = fields.Text(string="Color/Size Options [SKU-COLOR-SIZE]")
    warehouse_uom = fields.Selection([('EA','EA'),('PK of 5 = 1','PK of 5 = 1'),('PK of 10 = 1','PK of 10 = 1'),('PK of 15 = 1','PK of 15 = 1'),('PK of 20 = 1','PK of 20 = 1'),('PK of 25 = 1','PK of 25 = 1'),
                                    ('PK of 30 = 1','PK of 30 = 1'),('PK of 35 = 1','PK of 35 = 1'),('PK of 40 = 1','PK of 40 = 1'),('PK of 45 = 1','PK of 45 = 1'),('PK of 50 = 1','PK of 50 = 1'),('PK of 60 = 1','PK of 60 = 1'),
                                    ('PK of 70 = 1','PK of 70 = 1'),('PK of 80 = 1','PK of 80 = 1'),('PK of 90 = 1','PK of 90 = 1'),('PK of 100 = 1','PK of 100 = 1'),('PK of 250 = 1','PK of 250 = 1'),('PK of 500 = 1','PK of 500 = 1')], string="Unit of Measure")
    warehouse_reorder_point = fields.Char(string="Reorder Point")
    warehouse_store = fields.Many2one('warehouse.store', string="Store")
    warehouse_product_category = fields.Selection([('APL Apparel','APL Apparel'),('POP Point Of Purchase','POP Point Of Purchase'),('SWG Swag','SWG Swag'),('DEC Decor','DEC Decor'),
                                    ('HDW Headwear','HDW Headwear'),('DKW Drinkware','DKW Drinkware'),('GVW Giveaways','GVW Giveaways'),('MIS Misc','MIS Misc'),
                                    ('OFF Office','OFF Office'),('BAG Bags','BAG Bags'),('OTD Outdoor','OTD Outdoor'),('TSM Tradeshow Mateial','TSM Tradeshow Mateial'),
                                    ('GFT Gifts','GFT Gifts'),('FAD Food and Drink','FAD Food and Drink'),('TOL Tools','TOL Tools'),('TMP Timepieces','TMP Timepieces'),
                                    ('GLF Golf','GLF Golf'),('HAB Heath and Beauty','HAB Heath and Beauty'),('TVL Travel','TVL Travel'),('PEN Pens','PEN Pens'),
                                    ('JRL Journals','JRL Journals'),('TOY Toys','TOY Toys'),('PRT Printed Materials','PRT Printed Materials')], string="Product Category at Warehouse")
    warehouse_inventory_type = fields.Selection([('ICO Client Owned Inv','ICO Client Owned Inv'),('ICS Client Supplied Inv','ICS Client Supplied Inv'),('IPO Pinacle Owned Inv','IPO Pinacle Owned Inv')], string="Inventory Type")
    warehouse_quarter_added = fields.Selection([('First Quarter','First Quarter'),('Second Quarter','Second Quarter'),('Third Quarter','Third Quarter'),('Fourth Quarter','Fourth Quarter')], string="Quarter Added")
    warehouse_sub_program_billing = fields.Selection([('AOH Angel Oak','AOH Angel Oak'),('AOM Angel Oak Mortgage','AOM Angel Oak Mortgage'),('EMB EMBKITS','EMB EMBKITS'),('LIE Liebherr Construction','LIE Liebherr Construction'),
                                    ('LME Liebherr Mining','LME Liebherr Mining'),('FES Festivals','FES Festivals'),('RF Sunoco Racing Fuels','RF Sunoco Racing Fuels'),('FLX Flex League','FLX Flex League'),
                                    ('LFT Lifetime Fitness','LFT Lifetime Fitness'),('PGR Programs','PGR Programs'),('OTH Other','OTH Other'),('JTT Junior Tennis','JTT Junior Tennis'),
                                    ('SCH Schools','SCH Schools'),('TCW Tennis Coach Workshops','TCW Tennis Coach Workshops'),('Trustwave GOVSOL','Trustwave GOVSOL'),('10U 10 Under','10U 10 Under')], string="Sub-Program Billing")
    warehouse_are_these_components_sold_separately = fields.Selection([('Yes','Yes'),('No','No')],string="Are these components sold separately ?")
    warehouse_kit_component_grids = fields.One2many('warehouse.kit.component.grid', 'product_template_id', string="Kit Component Grids")
    first_tier_sale_price = fields.Float(compute="_compute_first_tier_sale_price", string='First Tier Sale Price', digits=dp.get_precision('Product Price'), store=True)
    warehouse_are_backorders_allowed_on_this_product = fields.Selection([('Yes', 'Yes'), ('No', 'No')], string="Are Backorders Allowed on this Product ?")

    @api.model
    def fields_view_get(self, view_id=None, view_type=False, toolbar=False, submenu=False):
        res = super(Product, self).fields_view_get(
            view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        doc = etree.XML(res['arch'])
        notebook = doc.xpath("//notebook[@name='seq_en']")
        if view_type == 'form' and len(notebook):
            sort_pages = sorted(notebook[0].getchildren(), key=lambda k: int(k.get('sequence', 0)),reverse=True)
            for node in notebook[0].getchildren():
                node.getparent().remove(node)
            for index, page in enumerate(sort_pages):
                seq = 0
                if page.get('sequence') is None:
                    seq = index + 1
                notebook[0].insert(seq, page)
            res['arch'] = etree.tostring(doc)
        return res

    @api.multi
    @api.depends('seller_ids')
    def _calculate_seller_name(self):
        for this in self:
            seller_name = ''
            for seller in this.seller_ids:
                if this.seller_ids[-1].id == seller.id:
                    seller_name += seller.name.name
                else:
                    seller_name += seller.name.name + ", "
            this.seller_name = seller_name
    @api.one
    @api.depends('pricing_ids')
    def _compute_first_tier_sale_price(self):
        self.first_tier_sale_price = 0.00
        if self.pricing_ids:
            self.first_tier_sale_price = self.pricing_ids.sorted(key=lambda r:r.min_qty)[0].sales_price

    @api.multi
    def print_item_maintenance_form(self):
        self.ensure_one()
        return self.env['report'].get_action(self, 'pinnacle_product.report_item_maintenance_form')

    @api.one
    @api.constrains('pricing_ids')
    def _check_product_pricing_ids(self):
        if len(self.pricing_ids.ids) > 6:
            raise ValidationError(_('No more than 6 price tiers allowed.'))

    @api.depends('pricing_ids')
    def _compute_min_price(self):
        for this in self:
            if this.pricing_ids:
                res = this.pricing_ids.search([('product_id', '=', this.id)], order='sales_price asc', limit=1)
                if res:
                    this.min_price = res.sales_price
                else:
                    this.min_price = 0.0
            else:
                this.min_price = 0.0

    @api.depends('seller_ids')
    def _compute_vendor_code(self):
        for this in self:
            for vendor in this.seller_ids:
                if vendor.product_code:
                    if not this.vendor_code:
                        this.vendor_code = ''
                    this.vendor_code = this.vendor_code+', '+vendor.product_code

    @api.one
    @api.returns('self', lambda value: value.id)
    def copy(self, default=None):
        default = dict(default or {})
        default.update(active=False)
        result = super(Product, self).copy(default)

        for res in self.attribute_line_ids:
            res.copy({'product_tmpl_id':result.id})
        result.with_context(no_variant_create=True).create_variant_ids()
        for res in self.pricing_ids:
            res.copy({'product_id':result.id})
        for res in self.value_ids:
            res.copy({'tmpl_id':result.id})
        for res in self.seller_ids:
            supplierinfo = res.copy({'product_tmpl_id':result.id})
            for attrib in res.product_vendor_tier_ids:
                attrib.copy({'product_supplier_info_id': supplierinfo.id})
            for attrib in res.attribute_vendor_attb_value_ids:
                for attrib_search in result.product_variant_ids:
                    if set(attrib_search.attribute_value_ids.ids) == set(attrib.product_attribute_values_ids.ids):
                        attrib.copy({'product_supplierinfo_id':supplierinfo.id, 'product_id':attrib_search.id})
            for attrib in res.attribute_vendor_attb_ids:
                attrib.copy({'product_tmp_id': result.id, 'product_supplierinfo_id': supplierinfo.id})

        for res in self.finisher_ids:
            value1 = res.copy({'product_id': result.id})
            for imprint in res.product_imprint_ids:
                imprint.copy({'product_finisher_id': value1.id})
        for res in self.customer_arts:
            res.copy({'product_template_id': result.id})
        return result

    product_sku = fields.Char(string='Product ID', copy=False)
    preferred_vendor = fields.Boolean('Use Preferred Vendor',copy=True)
    pricing_ids = fields.One2many('product.pricing', 'product_id', 'Product Pricing')
    property_line_ids = fields.One2many('product.property.line', 'product_tmpl_id', 'Product Properties', copy=False)
    property_group_id = fields.Many2one('product.property.group', 'Property Group', copy=False)
    internal_note = fields.Text('Internal Notes', copy=True)
    channel_id = fields.Many2many('product.channel', string='Program Store', copy=False)
    sample_ok = fields.Boolean('Can be Sampled',copy=True)
    value_ids = fields.One2many('attribute.value', 'tmpl_id', 'Values')
    can_decorate = fields.Boolean('Can be Decorated', default=True, copy=True)
    pinnacle_default_code = fields.Char('Pinnacle Internal Reference', copy=True)
    min_price = fields.Float(compute='_compute_min_price', store=True, copy=True)
    category_id = fields.Many2many('res.partner.category', column1='product_id',
                                    column2='category_id', string='Tags', copy=True)
    pla_description = fields.Char('PLA Description')
    gtin = fields.Char('GTIN', copy=True)
    keywords = fields.Char('Keywords', copy=True)
    seo_override = fields.Boolean('SEO Override', copy=True)
    html_title = fields.Char('HTML Title', copy=True)
    meta_keywords = fields.Char('META Keywords', copy=True)
    seo_text = fields.Text('SEO Text', copy=True)
    vendor_code = fields.Char(compute='_compute_vendor_code', store=True, default='', copy=True)
    present_decoration = fields.Boolean('Pre-set Decoration', copy=True)
    # Add flag for check Magento Orders & CGP Inventoried 
    is_magento_inventory = fields.Boolean(string="Can be Inventoried at a Warehouse")
    #Overwrite the Invoicing Policy for default Purchase set.
    purchase_method = fields.Selection([
        ('purchase', 'On ordered quantities'),
        ('receive', 'On received quantities'),
        ], string="Control Purchase Bills",
        help="On ordered quantities: control bills based on ordered quantities.\n"
        "On received quantities: control bills based on received quantity.", default="purchase")

    @api.multi
    def get_preferred_vendor(self):
        if len(self.seller_ids) > 1:
            for seller in self.seller_ids:
                if seller.preferred_vendor:
                    return seller
        else:
            return self.seller_ids

    @api.multi
    def get_preferred_vendor_name(self):
        seller = self.get_preferred_vendor()
        if seller:
            return seller.name.name

    @api.multi
    def get_preferred_vendor_link(self):
        seller = self.get_preferred_vendor()
        if seller:
            if seller.vendor_product_link:
                return seller.vendor_product_link

    @api.multi
    def unlink(self):
        """Overwrite Unlink method of Product Template.
        If user have a Administration Setting rights than user can delete the product.
        For other users it will raise warning like product cannot be deleted."""
        group = self.env.ref('base.group_system')
        groups = self.env.user.groups_id
        if group not in groups:
            raise Warning(_('Products cannot be deleted.'))
        return super(Product, self).unlink()


    @api.onchange('preferred_vendor')
    def onchange_preferred_vendor(self):
        pricing_ids = []
        if self.preferred_vendor:
            for seller in self.seller_ids:
                if seller.preferred_vendor:
                    for tier in seller.product_vendor_tier_ids:
                        pricing_dict = {
                            'min_qty': tier.min_qty,
                            'sales_price': tier.sale_price,
                            'msrp_price': tier.msrp,
                            'tier_sequence': tier.tier_sequence
                        }
                        pricing_ids.extend([(0, 0, pricing_dict)])
        else:
            for pricing in self.pricing_ids:
                pricing_ids.extend([(3, pricing.id)])

        self.pricing_ids = pricing_ids
        
    @api.multi
    def trigger_preferred_vendor(self):
        pricing_ids = []
        if self.preferred_vendor:
            for seller in self.seller_ids:
                if seller.preferred_vendor:
                    for tier in seller.product_vendor_tier_ids:
                        pricing_dict = {
                            'min_qty': tier.min_qty,
                            'sales_price': tier.sale_price,
                            'msrp_price': tier.msrp,
                            'tier_sequence': tier.tier_sequence
                        }
                        pricing_ids.extend([(0, 0, pricing_dict)])
        else:
            for pricing in self.pricing_ids:
                pricing_ids.extend([(3, pricing.id)])

        self.pricing_ids = pricing_ids

        return True
        
    @api.multi
    def _check_property_value_ids(self):
        for product in self:
            properties = set()
            for prop_line in product.property_line_ids:
                if prop_line.property_id.id in properties:
                    return False
                else:
                    properties.add(prop_line.property_id.id)
        return True

    _constraints = [
        (_check_property_value_ids, 'Error! It is not allowed to choose same property for a given property.',
         ['property_line_ids'])
    ]

    @api.multi
    def create_variant_ids(self):
        res = super(Product, self).create_variant_ids()
        if self.env.context.get('no_variant_create'):
            return res
        attribute_obj = self.env['product.attribute.value']
        value_obj = self.env["attribute.value"]
        for tmpl_id in self.with_context(active_test=False, create_product_variant=True):
            value_ids = []
            att_ids = []
            prod_variant_ids = [var.id for var in tmpl_id.product_variant_ids]
            attribute_ids = attribute_obj.search([('product_ids', 'in', prod_variant_ids)])
            for value in tmpl_id.value_ids:
                value_ids.append(value.value_id.id)
            for attribute in attribute_ids:
                att_ids.append(attribute.id)
                if attribute.id not in value_ids:
                    value_obj.create({
                        'attribute_id': attribute.attribute_id.id,
                        'value_id': attribute.id,
                        'internal_mapping_id': attribute.internal_mapping_id.id,
                        'tmpl_id': tmpl_id.id})
            val_ids = [val.value_id.id for val in tmpl_id.value_ids]
            inactive_attribute = list(set(val_ids) ^ set(att_ids))
            if inactive_attribute:
                for value in tmpl_id.value_ids:
                    if value.value_id.id in inactive_attribute:
                        value.unlink()
        return res

    @api.model
    def name_search(self, name='', args=None, operator='ilike', limit=100):
        old_products = super(Product, self).name_search(name=name, args=args, operator=operator, limit=limit)
        products = self.search(args + [('product_sku', operator, name)], limit=limit)
        new_products = products.name_get()
        for each_product in old_products:
            if each_product not in new_products:
                new_products.append(each_product)
        return new_products

    @api.model
    def create(self, vals):
        if 'taxes_id' in vals:
            vals['taxes_id'] = None
        if 'supplier_taxes_id' in vals:
            vals['supplier_taxes_id'] = None
        res = super(Product, self).create(vals)
        return res

class ProductProduct(models.Model):
    _inherit = 'product.product'

    property_value_ids = fields.Many2many('product.property.value', id1='prod_id', id2='prop_id', string='Properties',
                                          ondelete='restrict')

class ProductTemplateMultiImages(models.Model):
    _name = 'product.template.multi.images'

    @api.one
    @api.depends('image')
    def _get_image_from_url(self):
        if self.image_url:
            cfs_url = str(self.env['ir.config_parameter'].get_param('cfs.web.url'))
            final_url = cfs_url + self.image_url
            url = url_fix(final_url)
            try:
                self.image_preview = base64.encodestring(urllib2.urlopen(url).read())
            except Exception as e:
                self.image_preview = ''
        else:
            self.image_preview = ''

    image = fields.Binary(string='Image File', attachment=True, copy=False)
    image_att = fields.Many2one('ir.attachment', 'Image File Attribute', copy=False)
    image_url = fields.Char(related='image_att.url', string='Image File', store=True, copy=False)
    image_filename = fields.Char(string='Image File Name', copy=False)
    image_preview = fields.Binary('Image File Preview', compute='_get_image_from_url')

    product_tmpl_id = fields.Many2one('product.template', string='Product Template', ondelete='cascade', required=True)
    attribute_id = fields.Many2one('product.attribute', string='Attribute')
    attribute_value_id = fields.Many2one('product.attribute.value', string='Attribute Value')

    @api.onchange('attribute_id', 'attribute_value_id')
    def _onchnage_attribute_id(self):
        attribute_ids = []
        attribute_value_ids = []
        if not self.attribute_id and not self.attribute_value_id:
            if self.product_tmpl_id:
                for attribute_line_id in self.product_tmpl_id.attribute_line_ids:
                    attribute_ids.append(attribute_line_id.attribute_id.id)
                    for value_id in attribute_line_id.value_ids:
                        attribute_value_ids.append(value_id.id)
        elif self.attribute_id and not self.attribute_value_id:
            if self.product_tmpl_id:
                for attribute_line_id in self.product_tmpl_id.attribute_line_ids:
                    attribute_ids.append(attribute_line_id.attribute_id.id)
                    if self.attribute_id.id == attribute_line_id.attribute_id.id:
                        for value_id in attribute_line_id.value_ids:
                            attribute_value_ids.append(value_id.id)
        elif not self.attribute_id and self.attribute_value_id:
            if self.product_tmpl_id:
                for attribute_line_id in self.product_tmpl_id.attribute_line_ids:
                    for value_id in attribute_line_id.value_ids:
                        if self.attribute_value_id.id == value_id.id:
                            attribute_ids.append(attribute_line_id.attribute_id.id)
                        attribute_value_ids.append(value_id.id)
        else:
            if self.product_tmpl_id:
                for attribute_line_id in self.product_tmpl_id.attribute_line_ids:
                    if self.attribute_id.id == attribute_line_id.attribute_id.id:
                        attribute_ids.append(attribute_line_id.attribute_id.id)
                        for value_id in attribute_line_id.value_ids:
                            attribute_value_ids.append(value_id.id)
        return {'domain': {
            'attribute_id': [('id', 'in', attribute_ids)],
            'attribute_value_id': [('id', 'in', attribute_value_ids)],
        }}

    @api.multi
    def unlink_attachment(self):
        if self.env.context.get('image'):
            self.image = False

class AttributeInternalMapping(models.Model):
    _name = 'attribute.internal.mapping'
    _description = 'Attribute Internal Mapping'

    name = fields.Char('Internal Mapping')
    color = fields.Char("HTML Color Index", help="Here you can set a specific HTML color index (e.g. #ff0000) to "
                                                 "display the color on the website if the attribute type is 'Color'.")


class ProductAttributeValue(models.Model):
    _inherit = 'product.attribute.value'
    _order = 'attribute_id'
    
    internal_mapping_id = fields.Many2one('attribute.internal.mapping', 'Internal Mapping')

    def _variant_name(self, variable_attributes):
        pass

    @api.onchange('internal_mapping_id')
    def onchange_internal_mapping_id(self):
        for val in self:
            if val.internal_mapping_id:
                val.html_color = val.internal_mapping_id and val.internal_mapping_id.color or False
            else:
                val.html_color = False


class ProductChannel(models.Model):
    _name = 'product.channel'
    _description = 'Product Channel'

    name = fields.Char(string="Name", required=True)
    sequence = fields.Integer('Sequence')
    fonts = fields.Many2many('res.font', relation="product_channel_with_res_font_rel", column1="product_channel_id", column2="res_font_id", string="Fonts")
    product_templates = fields.One2many('product.template', 'channel_id', string="Product Templates")

class ResFont(models.Model):
    _inherit = "res.font"

    path = fields.Char(required=True, default="all")
    mode = fields.Char(required=True, default="/dev/null")
    product_channel = fields.Many2many('product.channel',relation="product_channel_with_res_font_rel", column1="res_font_id", column2="product_channel_id", string='Program Stores')

class AttributeValue(models.Model):
    _name = 'attribute.value'
    _description = 'Variant Attribute Value'

    attribute_id = fields.Many2one('product.attribute', 'Attribute')
    value_id = fields.Many2one('product.attribute.value', 'Value')
    internal_mapping_id = fields.Many2one('attribute.internal.mapping', 'Internal Mapping')
    override_value = fields.Boolean(string="Override Value")
    attribute_img = fields.Binary('Image')
    tier1 = fields.Float('Tier 1')
    tier2 = fields.Float('Tier 2')
    tier3 = fields.Float('Tier 3')
    tier4 = fields.Float('Tier 4')
    tier5 = fields.Float('Tier 5')
    tier6 = fields.Float('Tier 6')
    tmpl_id = fields.Many2one('product.template', 'Template', ondelete='cascade')


class ProductDashboard(models.Model):
    _name = 'product.dashboard'

    @api.multi
    def _get_recent_products(self):
        today = fields.Datetime.now()
        backdate = datetime.datetime.strptime(fields.Datetime().now(), '%Y-%m-%d %H:%M:%S') - datetime.timedelta(days=7)
        counts = self.env['product.template'].search_count([('create_date', '<', today), ('create_date', '>', backdate.strftime(DEFAULT_SERVER_DATETIME_FORMAT))])
        return counts

    @api.multi
    def _get_active_products(self):
        count = self.env['product.template'].search_count([('active', '=', True)])
        return count

    @api.multi
    def _get_top_products(self):
        for this in self:
            if this.flag_top:
                products = this.env['product.template'].search([('active', '=', True), ('type', '!=', 'service')], order='sales_count desc', limit=5)
                this.top1 = products[0].name if products[0] else ''
                this.top2 = products[1].name if products[1] else ''
                this.top3 = products[2].name if products[2] else ''
                this.top4 = products[3].name if products[3] else ''
                this.top5 = products[4].name if products[4] else ''

    name = fields.Char()
    recent_product = fields.Integer(default=_get_recent_products)
    active_products = fields.Integer(default=_get_active_products)
    tops = fields.Text(compute='_get_top_products')
    top1 = fields.Char(compute='_get_top_products')
    top2 = fields.Char(compute='_get_top_products')
    top3 = fields.Char(compute='_get_top_products')
    top4 = fields.Char(compute='_get_top_products')
    top5 = fields.Char(compute='_get_top_products')
    flag = fields.Boolean()
    flag_top = fields.Boolean()


    @api.multi
    def get_dashboard_data(self): 
        res = {}

        date_today = date.today()
        
        res['invoiced'] = {
            'this_month': 0,
            'last_month': 0,
        }
        res['currency_id'] = self.env['res.users'].browse(self.env.uid).company_id.currency_id.id
        res['sold'] = {
            'this_month': 0,
            'last_month': 0,
        }
        account_invoice_domain = [
            ('state', 'in', ['open', 'paid']),
            ('user_id', '=', self.env.uid),
            ('date', '>=', date_today.replace(day=1) - relativedelta(months=+1)),
            ('type', 'in', ['out_invoice', 'out_refund'])
        ]
        invoice_data = self.env['account.invoice'].search_read(account_invoice_domain, ['date', 'amount_untaxed_signed'])
        backdate = datetime.datetime.strptime(fields.Datetime().now(), '%Y-%m-%d %H:%M:%S') - datetime.timedelta(days=30)
        sales_order_domain = [
            ('state', 'in', ['invoiced', 'delivered', 'close', 'reopen', 'done']),
            #('date_order', '<', today), ('date_order', '>', backdate)
            ('create_date', '<', date_today.strftime(DEFAULT_SERVER_DATE_FORMAT)), ('create_date', '>', backdate.strftime(DEFAULT_SERVER_DATETIME_FORMAT))
        ]
        order_data = self.env['sale.order'].search(sales_order_domain)
        for invoice in invoice_data:
            if invoice['date']:
                invoice_date = fields.Date.from_string(invoice['date'])
                if invoice_date <= date_today and invoice_date >= date_today.replace(day=1):
                    res['invoiced']['this_month'] += invoice['amount_untaxed_signed']
                elif invoice_date < date_today.replace(day=1) and invoice_date >= date_today.replace(day=1) - relativedelta(months=+1):
                    res['invoiced']['last_month'] += invoice['amount_untaxed_signed']
        for order in order_data:
            order_date = fields.Date.from_string(order.date_order)
            if order_date <= date_today and order_date >= date_today.replace(day=1):
                for line in order.order_line:
                    res['sold']['this_month'] += line.product_uom_qty
            elif order_date < date_today.replace(day=1) and order_date >= date_today.replace(day=1) - relativedelta(months=+1):
                for line in order.order_line:
                    res['sold']['last_month'] += line.product_uom_qty
        return res

class WarehouseKitComponentGrid(models.Model):
    _name = 'warehouse.kit.component.grid'
    _description = 'Warehouse Kit Component Grid'

    name = fields.Char(string="Component Item/SKU")
    qty_per_kit = fields.Float(string='Quantity Per Kit', digits=dp.get_precision('Product Unit of Measure'), default=1.0)
    date_available = fields.Date(string="Date Component Available") 
    product_template_id = fields.Many2one('product.template', strign="Product Template")

class warehouse_store(models.Model):
    _name = 'warehouse.store'
    _description = 'Warehouse Store'
    _order = 'name'

    name = fields.Char(string="Name")

    _sql_constraints = [
        ('name_uniq', 'unique (name)', 'Warehouse Store must be unique!')
    ]