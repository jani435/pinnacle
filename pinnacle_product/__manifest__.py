# -*- coding: utf-8 -*-
##############################################################################
#
#  Bista Solutions Inc.
#  Website: www.bistasolutions.com
#
##############################################################################
{
    'name': 'Pinnacle Product',
    'version': '1.17',
    'category': 'Other',
    "author": 'Bista Solutions Pvt. Ltd.',
    'summary': """
    This module extends product feature
    """,
    'depends': [
        'pinnacle_update',
        'website_sale',
        'account_asset',
    ],
    'data': [
        'security/ir.model.access.csv',
        'data/dashboard_data.xml',
        'data/mail_template.xml',
        'views/view.xml',
        'views/product_property_view.xml',
        'views/product_view.xml',
        'views/item_maintenance_form.xml',
        'views/report.xml',
        'views/res_company_view.xml',
        'views/product_data_management_report.xml',
        'views/website_category.xml',
        'data/warehouse_store.xml'
    ],
    'qweb': ['static/src/xml/product_dashboard.xml'],
    'demo_xml': [],
    'test': [],
    'installable': True,
}
