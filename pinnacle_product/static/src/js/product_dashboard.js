odoo.define('pinnacle_product.dashboard1', function (require) {
"use strict";

var core = require('web.core');
var formats = require('web.formats');
var Model = require('web.Model');
var session = require('web.session');
var KanbanView = require('web_kanban.KanbanView');
var data = require('web.data');
var QWeb = core.qweb;
var _t = core._t;
var _lt = core._lt;

var ProductDashboardView = KanbanView.extend({
    display_name: _lt('Dashboard'),
    icon: 'fa-dashboard',
    searchview_hidden: true,
    
    fetch_data: function() {
        // Overwrite this function with useful data
        return new Model('product.dashboard')
            .call('get_dashboard_data', [[], {}]);
        //return $.when();
    },

    render: function() {
        var super_render = this._super;
        var self = this;

        return this.fetch_data().then(function(result){

            var product_dashboard = QWeb.render('pinnacle_product.ProductDashboard', {
                widget: self,
                values: result,
            });
            super_render.call(self);
            $(product_dashboard).prependTo(self.$el);
        });
    },
    

    render_monetary_field: function(value, currency_id) {
        var currency = session.get_currency(currency_id);
        var digits_precision = currency && currency.digits;
        value = formats.format_value(value || 0, {type: "float", digits: digits_precision});
        if (currency) {
            if (currency.position === "after") {
                value += currency.symbol;
            } else {
                value = currency.symbol + value;
            }
        }
        return value;
    },
});

core.view_registry.add('product_dashboard', ProductDashboardView);

return ProductDashboardView;

});

odoo.define('pinnacle_product.form_widgets', function (require) {
    var core = require('web.core');
    var data = require('web.data');
    var Model = require('web.DataModel');
    var session = require('web.session');
    var utils = require('web.utils');
    var ViewManager = require('web.ViewManager');

    var _t = core._t;
    var QWeb = core.qweb;
    var list_widget_registry = core.list_widget_registry;
    var FieldMany2ManyBinaryMultiFiles = core.form_widget_registry.get('many2many_binary')
    var element_height = 0
    var element_width = 0
    FieldMany2ManyBinaryMultiFiles.include({
        
        template: "FieldBinaryFileUploader",
        events: {
            'click .o_attach': function(e) {
                this.$('.o_form_input_file').click();
            },
            'dragover .o_attach': function(e){
                e.preventDefault();e.stopPropagation();
            
                if (!$('.o_attach').hasClass('adjust_sheet')){
                    $('.o_attach').addClass('adjust_sheet');
                    element_height = $('.o_attach').height();
                    element_width = $('.o_attach').width();
                    $('.o_attach').height(200).width(200).css({"background": "#8F8F8F", "padding-bottom": "50px"});
                    //$(document).animate({ scrollTop: $('.o_attach').parent().height() }, 1000);
                }
            },
            "dragleave .o_attach": "toggle_effect",
            "dragexit .o_attach": "toggle_effect",
            "dragend .o_attach": "toggle_effect",

            'drop .o_attach': function(e){
                e.preventDefault();e.stopPropagation();
                var self = this;
                _.each(e.originalEvent.dataTransfer.files, function(file){
                   /* var filename = file.name.replace(/.*[\\\/]/, '');
                    if(self.node.attrs.blockui > 0) { // block UI or not
                        framework.blockUI();
                    }*/
                    var filename = file.name;
                    for(var id in self.get('value')) {
                        // if the files exits, delete the file before upload (if it's a new file)
                        if(self.data[id] && (self.data[id].filename || self.data[id].name) == filename && !self.data[id].no_unlink) {
                            self.ds_file.unlink([id]);
                        }
                    }
                    var querydata = new FormData();
                    querydata.append('csrf_token',core.csrf_token);
                    querydata.append('callback', 'oe_fileupload_temp86');
                    querydata.append('ufile',file);
                    querydata.append('model', self.model);
                    querydata.append('id', '0');
                    querydata.append('multi', 'true');
                    $.ajax({
                        url: '/web/binary/upload_attachment',
                        type: 'POST',
                        data: querydata,
                        cache: false,
                        processData: false,  
                        contentType: false,
                        async: false,
                        success: function(id){
                            self.data[id] = {
                                'id': parseInt(id),
                                'name': file.name,
                                'filename': file.name,
                                'url': '',
                                'upload': false,
                                'mimetype': file.type
                            };
                            
                        },
                    });
                });
                // submit file
                this.$('form.o_form_binary_form').submit();
                //this.$(".oe_fileupload").hide();
                this.toggle_effect(e);
            },
            'change .o_form_input_file': function(e) {
                e.stopPropagation();
                var $target = $(e.target);
                var value = $target.val();
                if(value !== '') {
                    if(this.data[0] && this.data[0].upload) { // don't upload more of one file in same time
                        return false;
                    }
                    var filename = value.replace(/.*[\\\/]/, '');
                    for(var id in this.get('value')) {
                        // if the files exits, delete the file before upload (if it's a new file)
                        if(this.data[id] && (this.data[id].filename || this.data[id].name) == filename && !this.data[id].no_unlink) {
                            this.ds_file.unlink([id]);
                        }
                    }

                    if(this.node.attrs.blockui > 0) { // block UI or not
                        framework.blockUI();
                    }
                    var self = this;
                    // TODO : unactivate send on wizard and form
                    _.each($target[0].files, function(file){
                        var querydata = new FormData();
                        querydata.append('csrf_token',core.csrf_token);
                        querydata.append('callback', 'oe_fileupload_temp86');
                        querydata.append('ufile',file);
                        querydata.append('model', self.model);
                        querydata.append('id', '0');
                        querydata.append('multi', 'true');
                        $.ajax({
                            url: '/web/binary/upload_attachment',
                            type: 'POST',
                            data: querydata,
                            cache: false,
                            processData: false,  
                            contentType: false,
                            success: function(id){
                                self.data[id] = {
                                    'id': parseInt(id),
                                    'name': file.name,
                                    'filename': filename.name,
                                    'url': '',
                                    'upload': false,
                                    'mimetype': file.type
                                };
                            },
                        });
                    });
                    // submit file
                    this.$('form.o_form_binary_form').submit();
                    
                    this.$(".oe_fileupload").hide();
                    
                }
            },
            'click .oe_delete': function(e) {
                e.preventDefault();
                e.stopPropagation();

                var file_id = $(e.currentTarget).data("id");
                if(file_id) {
                    var files = _.without(this.get('value'), file_id);
                    if(!this.data[file_id].no_unlink) {
                        this.ds_file.unlink([file_id]);
                    }
                    delete this.data[file_id];
                    this.set({'value': files});
                }
            },
        },
        init: function(field_manager, node) {
            this._super.apply(this, arguments);
            this.session = session;
            if(this.field.type != "many2many" || this.field.relation != 'ir.attachment') {
                throw _.str.sprintf(_t("The type of the field '%s' must be a many2many field with a relation to 'ir.attachment' model."), this.field.string);
            }
            this.data = {};
            this.set_value([]);
            this.ds_file = new data.DataSetSearch(this, 'ir.attachment');
            this.fileupload_id = _.uniqueId('oe_fileupload_temp');
            $(window).on(this.fileupload_id, _.bind(this.on_file_loaded, this));
        },
        get_file_url: function(attachment) {
            return '/web/content/' + attachment.id + '?download=true';
        },
        read_name_values : function() {
            var self = this;
            // don't reset know values
            var ids = this.get('value');
            var _value = _.filter(ids, function(id) { return self.data[id] === undefined; });
            // send request for get_name
            if(_value.length) {
                return this.ds_file.call('read', [_value, ['id', 'name', 'datas_fname', 'mimetype']])
                                   .then(process_data);
            } else {
                return $.when(ids);
            }

            function process_data(datas) {
                _.each(datas, function(data) {
                    data.no_unlink = true;
                    data.url = self.get_file_url(data);
                    self.data[data.id] = data;
                });
                return ids;
            }
        },
        render_value: function() {
            var self = this;
            this.read_name_values().then(function (ids) {
                self.$('.oe_placeholder_files, .oe_attachments')
                    .replaceWith($(QWeb.render('FieldBinaryFileUploader.files', {'widget': self, 'values': ids})));

                // reinit input type file
                var $input = self.$('.o_form_input_file');
                $input.after($input.clone(true)).remove();
                self.$(".oe_fileupload").show();

                // display image thumbnail
                self.$(".o_image[data-mimetype^='image']").each(function () {
                    var $img = $(this);
                    if (/gif|jpe|jpg|png/.test($img.data('mimetype')) && $img.data('src')) {
                        $img.css('background-image', "url('" + $img.data('src') + "')");
                    }
                });
            });
        },
       on_file_loaded: function (event, result) {
            if(this.node.attrs.blockui>0) {
                instance.web.unblockUI();
            }

            if (result.error || !result.id ) {
                this.do_warn( _t('Uploading Error'), result.error);
                delete this.data[0];
            } else {
                var values = []
                _.each(this.data, function(file){
                     values.push(file.id);
                });
                this.set({'value': values});                
            }
            this.render_value();
        },  
        toggle_effect: function(e){
            e.preventDefault();e.stopPropagation();
            $('.o_attach').removeClass('adjust_sheet');
            $('.note-editor').removeClass('dragover');
            $('.o_attach').height(element_height).width(element_width)
                            .css({"background": "#ffffff"});
        },
       
    });

    core.form_widget_registry.add('many2many_binary', FieldMany2ManyBinaryMultiFiles);
    return FieldMany2ManyBinaryMultiFiles
});


odoo.define('pinnacle_product.productdrop', function (require) {

    var core = require('web.core');
    var data = require('web.data');
    var args = {}
    var chatter_height = 0
    var form_common = require('web.form_common');
    var DropImages = form_common.FormWidget.extend(form_common.ReinitializeWidgetMixin, {
        
        events: {
            "dragover div.drop_image_area": "product_dragging",
            "drop div.drop_image_area": "product_dropping",
            "dragleave div.drop_image_area": "product_dragleaving",
            "dragexit div.drop_image_area": "product_dragexiting",
            "dragend div.drop_image_area": "product_dragending",
        },

        template: "DropImages",

        init: function () {
            this._super.apply(this, arguments);
        },
        toggle_effect: function(e){
            e.preventDefault();e.stopPropagation();
            $('div.drop_image_area').removeClass('adjust_sheet');
            $('div.drop_image_area').height(chatter_height).css("background", "#ffffff");
        },
        product_dragging: function(e){
            e.preventDefault();e.stopPropagation();
            
            if (!$('div.drop_image_area').hasClass('adjust_sheet')){
                $('div.drop_image_area').addClass('adjust_sheet');
                chatter_height = $('div.drop_image_area').height();
                $('div.drop_image_area').height(200).css("background", "#8F8F8F");
            }
        },
        product_dragleaving: function(e){
            this.toggle_effect(e)
        },
        product_dragexiting: function(e){
            this.toggle_effect(e)
        },
        product_dragending: function(e){
            this.toggle_effect(e)
        },
        product_dropping: function(e){
            e.preventDefault();e.stopPropagation();
            if(e.originalEvent.dataTransfer && e.originalEvent.dataTransfer.files.length){
                var self = this
                var arglist = []
                self.toggle_effect(e)
                _.each(e.originalEvent.dataTransfer.files, function(file){
                    var querydata = new FormData();
                    querydata.append('csrf_token',core.csrf_token);
                    querydata.append('callback', 'oe_fileupload_temp86');
                    querydata.append('ufile', file);
                    querydata.append('model', 'product.template.multi.images');
                    querydata.append('id', self.field_manager.datarecord.id);
                    $.ajax({
                        url: '/web/upload_product_multi_images',
                        type: 'POST',
                        data: querydata,
                        cache: false,
                        processData: false,  
                        contentType: false,
                        async: false,
                        success: function(id){
                            arglist.push(parseInt(id))
                        },
                        error: function(xhr, textStatus, errorThrown){
                           console.log(errorThrown);
                        },
                    });
                });
                res_ids = self.field_manager.fields.multiple_images.dataset.ids
                self.field_manager.set_values({'multiple_images': res_ids.concat(arglist)})
            }

        },

    });

    core.form_custom_registry.add('drop_images', DropImages);
});
