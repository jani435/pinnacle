# -*- coding: utf-8 -*-
from odoo import tools
from odoo import api, fields, models, _
import time

class PurchaseReport(models.Model):
    _inherit = "purchase.report"

    state = fields.Selection([
        ('draft', 'Draft'),
        ('ipo_to_approve', 'IPO Approval'),
        ('ipo_approved', 'IPO Approved'),
        ('ipo_rejected', 'IPO Rejected'),
        ('sent', 'Submitted'),
        ('to approve', 'To Approve'),
        ('purchase', 'Confirmed'),
        ('shipped', 'Shipped'),
        ('deliver', 'Delivered'),
        ('close', 'Closed'),
        ('reopen', 'Re-Opened'),
        ('done', 'Re-Closed'),
        ('cancel', 'Cancelled'),
        ('hold', 'On Hold'),
        ], 'Order Status', readonly=True)

class VendorReport(models.Model):
    _name = "vendor.report"
    _description = "Vendor Report"
    _auto = False
    _order = 'date_order desc'

    partner_id = fields.Many2one('res.partner', 'Vendor', readonly=True)
    number_of_orders = fields.Integer('# of Orders', readonly=True)
    cost = fields.Float('Cost', readonly=True)
    average_order_value = fields.Float('Avg Order Value', readonly=True, group_operator='avg')
    channel_id = fields.Many2one('partner.channel', 'Channel', readonly=True)
    date_order = fields.Datetime('Order Date', readonly=True)

    def _select(self):
        """Returns select str for vendor report data"""
        select_str = """
             SELECT min(p.id) as id,
                    p.date_order as date_order,
                    p.partner_id as partner_id,
                    p.channel_id as channel_id,
                    count(*) as number_of_orders,
                    sum(p.amount_total) as cost,
                    sum(p.amount_total) as average_order_value
        """
        return select_str

    def _from(self):
        """Returns from str for vendor report data"""
        from_str = """
                purchase_order p
                left join res_partner r on p.partner_id = r.id
                left join partner_channel c on p.channel_id = c.id
        """
        return from_str

    def _group_by(self):
        """Returns group by str for vendor report data"""
        group_by_str = """
            GROUP BY p.date_order,
                p.partner_id,
                p.channel_id       
        """
        return group_by_str

    @api.model_cr
    def init(self):
        """Create or Replace database view for vendor report. This method will call
        when we install module or upgrade this module"""
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""CREATE or REPLACE VIEW %s as (
            %s
            FROM (%s)
            %s
            )""" % (self._table, self._select(), self._from(), self._group_by()))

class VendorReportWizard(models.TransientModel):
    _name = 'vendor.report.wizard'

    partner_id = fields.Many2one('res.partner', 'Vendor')

    @api.multi
    def display_data(self):
        """Returns : Pivot view using filtered data for Vendor report"""
        domain = [('date_order', '>=',time.strftime('%Y-01-01 00:00:00')), ('date_order', '<=',time.strftime('%Y-12-31 23:59:59')),('partner_id', '=', self.partner_id.id)]

        return {
            'name': _('Vendor Report'),
            'type': 'ir.actions.act_window',
            'res_model': 'vendor.report',
            'view_mode': 'pivot',
            'view_type': 'form',
            'domain': domain,
        }