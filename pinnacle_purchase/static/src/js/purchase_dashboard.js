// Javascript for Purchase Dashboard.
// Fetching data from the Puython method and render the dashboard view.
odoo.define('pinnacle_purchase.purchase_dashboard', function (require) {
"use strict";

var core = require('web.core');
var formats = require('web.formats');
var Model = require('web.Model');
var session = require('web.session');
var KanbanView = require('web_kanban.KanbanView');
var data = require('web.data');
var QWeb = core.qweb;
var PurchaseDashboardView = KanbanView.extend({
    display_name: 'Dashboard',
    icon: 'fa-dashboard',
    searchview_hidden: true,
    events: {
        'click .o_dashboard_action': 'on_dashboard_action_clicked',
        'click .o_target_to_set': 'on_dashboard_target_clicked',
    },
    
    fetch_data: function() {
        return new Model('purchase.order')
            .call('get_dashboard_data', [[], {}]);
    },

    render: function() {
        var super_render = this._super;
        var self = this;

        return this.fetch_data().then(function(result){

            var purchase_dashboard = QWeb.render('pinnacle_purchase.PinnaclePurchaseDashboard', {
                widget: self,
                values: result,
            });
            super_render.call(self);
            $(purchase_dashboard).prependTo(self.$el);
        });
    },

    on_dashboard_action_clicked: function(ev){
        ev.preventDefault();

        var $action = $(ev.currentTarget);
        var action_name = $action.attr('name');

        this.do_action(action_name, {});
    },    

});

core.view_registry.add('purchase_dashboard', PurchaseDashboardView);

return PurchaseDashboardView;

});
