odoo.define('pinnacle_purchase.pinnacle_purchase_widgets', function (require) {
    "use strict";
    var FormView = require('web.FormView');

    FormView.include({ 
        _actualize_mode: function() {
            if (this.get("actual_mode") == "edit")
            {   
                if (this.model == 'purchase.order.edit.decoration.line' || this.model == 'purchase.order.edit.multi.decoration.line'
                            || this.model == 'purchase.order.add.decoration.line' || this.model == 'purchase.order.add.packing.slip'
                            || this.model == 'purchase.order.packing.slip')
                {
                    this.do_onchange(null).done(function(){
                        if (this._super != null)
                        {this._super.apply(this, arguments);}
                    });
                }
                else
                {
                    this._super.apply(this, arguments);
                }
            }
            else
            {
                this._super.apply(this, arguments);
            }
        },
        
    });

});