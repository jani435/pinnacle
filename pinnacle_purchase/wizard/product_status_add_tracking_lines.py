# -*- coding: utf-8 -*-
from openerp import models, fields, api, _
from openerp.addons.pinnacle_purchase.wizard.purchase_common import purchase_common
from openerp.exceptions import UserError
import urlparse


class ProductStatusAddTracking(models.TransientModel):
    _name = 'product.status.add.tracking'

    name = fields.Char(string='Add Tracking Lines To')
    product_status_add_tracking_line_ids = fields.One2many(
        'product.status.add.tracking.line',
        'product_status_add_tracking_id',
        'Product Status Add Tracking Lines')

    @api.model
    def default_get(self, fields):
        res = super(ProductStatusAddTracking,self).default_get(fields)
        name = False
        if self.env.context.get('active_ids', False) and self._context.get('product_status_line', False):
            for product_status_line in self.env['purchase.order.line.for.tracking'].browse(self.env.context.get('active_ids', False)):
                if name:
                    if product_status_line.product_sku and product_status_line.name and product_status_line.product_variant_name:
                        name = name + ' <br/> ' + product_status_line.product_sku + " - " + product_status_line.name + " - " + product_status_line.product_variant_name
                    elif product_status_line.product_sku and product_status_line.name and (not product_status_line.product_variant_name):
                        name = name + ' <br/> ' + product_status_line.product_sku + " - " + product_status_line.name
                    elif product_status_line.product_sku and (not product_status_line.name) and product_status_line.product_variant_name:
                        name = name + ' <br/> ' + product_status_line.product_sku + " - " + product_status_line.product_variant_name
                    elif (not product_status_line.product_sku) and product_status_line.name and product_status_line.product_variant_name:
                        name = name + ' <br/> ' + product_status_line.name + " - " + product_status_line.product_variant_name
                    elif (not product_status_line.product_sku) and (not product_status_line.name) and product_status_line.product_variant_name:
                        name = name + ' <br/> ' + product_status_line.product_variant_name
                    elif (not product_status_line.product_sku) and product_status_line.name and (not product_status_line.product_variant_name):
                        name = name + ' <br/> ' + product_status_line.name
                    elif product_status_line.product_sku and (not product_status_line.name) and (not product_status_line.product_variant_name):
                        name = name + ' <br/> ' + product_status_line.product_sku
                    else:
                        pass
                else:
                    if product_status_line.product_sku and product_status_line.name and product_status_line.product_variant_name:
                        name = product_status_line.product_sku + " - " + product_status_line.name + " - " + product_status_line.product_variant_name
                    elif product_status_line.product_sku and product_status_line.name and (not product_status_line.product_variant_name):
                        name = product_status_line.product_sku + " - " + product_status_line.name
                    elif product_status_line.product_sku and (not product_status_line.name) and product_status_line.product_variant_name:
                        name = product_status_line.product_sku + " - " + product_status_line.product_variant_name
                    elif (not product_status_line.product_sku) and product_status_line.name and product_status_line.product_variant_name:
                        name = product_status_line.name + " - " + product_status_line.product_variant_name
                    elif (not product_status_line.product_sku) and (not product_status_line.name) and product_status_line.product_variant_name:
                        name = product_status_line.product_variant_name
                    elif (not product_status_line.product_sku) and product_status_line.name and (not product_status_line.product_variant_name):
                        name = product_status_line.name
                    elif product_status_line.product_sku and (not product_status_line.name) and (not product_status_line.product_variant_name):
                        name = product_status_line.product_sku
                    else:
                        pass
        if self.env.context.get('active_ids', False) and self._context.get('purchase_decoration_groups', False):
            for purchase_decoration_group in self.env['purchase.decoration.group.for.tracking'].browse(self.env.context.get('active_ids', False)):
                if name:
                    if purchase_decoration_group.group_sequence and purchase_decoration_group.group_id:
                        name = name + ' <br/> ' + purchase_decoration_group.group_sequence + " - " + purchase_decoration_group.group_id
                    elif purchase_decoration_group.group_sequence and (not purchase_decoration_group.group_id):
                        name = name + ' <br/> ' + purchase_decoration_group.group_sequence
                    elif not (purchase_decoration_group.group_sequence) and purchase_decoration_group.group_id:
                        name = name + ' <br/> ' + purchase_decoration_group.group_id
                    else:
                        pass
                else:
                    if purchase_decoration_group.group_sequence and purchase_decoration_group.group_id:
                        name = purchase_decoration_group.group_sequence + " - " + purchase_decoration_group.group_id
                    elif purchase_decoration_group.group_sequence and (not purchase_decoration_group.group_id):
                        name = purchase_decoration_group.group_sequence
                    elif not (purchase_decoration_group.group_sequence) and purchase_decoration_group.group_id:
                        name = purchase_decoration_group.group_id
                    else:
                        pass
        res.update({'name':name})
        return res

    @api.multi
    def create_product_tracking_numbers(self):
        if self.env.context.get('active_ids', False) and self._context.get('product_status_line', False):
            for product_status_line in self.env['purchase.order.line.for.tracking'].browse(self.env.context.get('active_ids', False)):
                for product_status_add_tracking_line_id in self.product_status_add_tracking_line_ids:
                    self.env['product.tracking.numbers'].create({
                        'delivery_carrier': product_status_add_tracking_line_id.delivery_carrier.id,
                        'tracking_number': product_status_add_tracking_line_id.tracking_number,
                        'tracking_number_url': product_status_add_tracking_line_id.tracking_number_url,
                        'purchase_order_line_for_tracking_id': product_status_line.id,
                    })
        if self.env.context.get('active_ids', False) and self._context.get('purchase_decoration_groups', False):
            for purchase_decoration_group in self.env['purchase.decoration.group.for.tracking'].browse(self.env.context.get('active_ids', False)):
                for product_status_add_tracking_line_id in self.product_status_add_tracking_line_ids:
                    self.env['product.tracking.numbers'].create({
                        'delivery_carrier': product_status_add_tracking_line_id.delivery_carrier.id,
                        'tracking_number': product_status_add_tracking_line_id.tracking_number,
                        'tracking_number_url': product_status_add_tracking_line_id.tracking_number_url,
                        'purchase_group_id': purchase_decoration_group.id,
                    })

class ProductStatusAddTrackingLine(models.TransientModel):
    _name = 'product.status.add.tracking.line'

    product_status_add_tracking_id = fields.Many2one(
        'product.status.add.tracking',
        string='Product Status Add Tracking',
        ondelete="cascade")
    delivery_carrier = fields.Many2one('delivery.carrier','Delivery Method', required="True")
    delivery_carrier_url = fields.Char(related="delivery_carrier.tracking_url", string="Base URL", readonly=True)
    tracking_number = fields.Char(string="Tracking Number")
    tracking_number_url = fields.Char(string="Tracking URL")
    generated_tracking_number_url = fields.Char(compute="_compute_generated_tracking_number_urll",string="Tracking URL", store=True)

    @api.one
    @api.depends('delivery_carrier_url', 'tracking_number', 'tracking_number_url')
    def _compute_generated_tracking_number_urll(self):
        self.generated_tracking_number_url = ''
        if self.delivery_carrier_url and self.tracking_number:
            self.generated_tracking_number_url = self.delivery_carrier_url + self.tracking_number
        if not self.delivery_carrier_url:
            if self.tracking_number_url:
                self.generated_tracking_number_url = self.tracking_number_url

    def _clean_tracking_number_url(self, tracking_number_url):
        (scheme, netloc, path, params, query, fragment) = urlparse.urlparse(tracking_number_url)
        if not scheme:
            if not netloc:
                netloc, path = path, ''
            tracking_number_url = urlparse.urlunparse(('http', netloc, path, params, query, fragment))
        return tracking_number_url

    @api.multi
    def write(self, vals):
        if vals.get('tracking_number_url'):
            vals['tracking_number_url'] = self._clean_tracking_number_url(vals['tracking_number_url'])
        result = super(ProductStatusAddTrackingLine, self).write(vals)
        return result

    @api.model
    def create(self, vals):
        if vals.get('tracking_number_url'):
            vals['tracking_number_url'] = self._clean_tracking_number_url(vals['tracking_number_url'])
        result = super(ProductStatusAddTrackingLine, self).create(vals)
        return result