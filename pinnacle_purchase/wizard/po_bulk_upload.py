# -*- coding: utf-8 -*-
from openerp import models, fields, api, _
import openerp.addons.decimal_precision as dp
from openerp.exceptions import UserError
from odoo import tools

class ArtUploadFile(models.TransientModel):
    _inherit = 'art.upload.file'

    @api.multi
    def bulk_upload(self):
        self.ensure_one()
        if not self.env.context.get('po_line'):
            return super(ArtUploadFile, self).bulk_upload()
        for po_line in self.env['sale.order.line.decoration'].browse(self.env.context.get('po_line_ids')):
            if self.spec_file:
                po_line.spec_file_name = self.spec_file_name
                po_line.spec_file = self.spec_file
            else:
                po_line.spec_file_url = self.spec_file_url
            if self.vendor_file:
                po_line.vendor_file_name = self.vendor_file_name
                po_line.vendor_file = self.vendor_file
            else:
                po_line.vendor_file_url = self.vendor_file_url
            if self.spec_file2:
                po_line.spec_file2_name = self.spec_file2_name
                po_line.spec_file2 = self.spec_file2
            else:
                po_line.spec_file2_url = self.spec_file2_url
        return {'type': 'ir.actions.act_window_close'}