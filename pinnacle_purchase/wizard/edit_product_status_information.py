# -*- coding: utf-8 -*-
from openerp import models, fields, api, _
from openerp.addons.pinnacle_purchase.wizard.purchase_common import purchase_common
from openerp.exceptions import UserError
import urlparse


class EditProductStatusLineInfo(models.TransientModel):
    _name = 'edit.product.status.line.info'

    name = fields.Char(string='Add Status Line Information Of')
    product_status_status = fields.Many2one('purchase.product.status.status', string="Status")
    product_status_status_name = fields.Char(related="product_status_status.name", store=True)
    edit_product_status_line_info_line_ids = fields.One2many(
        'edit.product.status.line.info.line',
        'edit_product_status_line_info_id',
        'Edit Product Status Line Info Lines')

    @api.model
    def default_get(self, fields):
        res = super(EditProductStatusLineInfo,self).default_get(fields)
        name = False
        records = []
        product_status_status_id = False
        if self.env.context.get('active_ids', False) and self._context.get('product_status_line', False):
            for product_status_line in self.env['purchase.order.line.for.tracking'].browse(self.env.context.get('active_ids', False)):
                if name:
                    if product_status_line.product_sku and product_status_line.name and product_status_line.product_variant_name:
                        name = name + ' <br/> ' + product_status_line.product_sku + " - " + product_status_line.name + " - " + product_status_line.product_variant_name
                    elif product_status_line.product_sku and product_status_line.name and (not product_status_line.product_variant_name):
                        name = name + ' <br/> ' + product_status_line.product_sku + " - " + product_status_line.name
                    elif product_status_line.product_sku and (not product_status_line.name) and product_status_line.product_variant_name:
                        name = name + ' <br/> ' + product_status_line.product_sku + " - " + product_status_line.product_variant_name
                    elif (not product_status_line.product_sku) and product_status_line.name and product_status_line.product_variant_name:
                        name = name + ' <br/> ' + product_status_line.name + " - " + product_status_line.product_variant_name
                    elif (not product_status_line.product_sku) and (not product_status_line.name) and product_status_line.product_variant_name:
                        name = name + ' <br/> ' + product_status_line.product_variant_name
                    elif (not product_status_line.product_sku) and product_status_line.name and (not product_status_line.product_variant_name):
                        name = name + ' <br/> ' + product_status_line.name
                    elif product_status_line.product_sku and (not product_status_line.name) and (not product_status_line.product_variant_name):
                        name = name + ' <br/> ' + product_status_line.product_sku
                    else:
                        pass
                else:
                    if product_status_line.product_sku and product_status_line.name and product_status_line.product_variant_name:
                        name = product_status_line.product_sku + " - " + product_status_line.name + " - " + product_status_line.product_variant_name
                    elif product_status_line.product_sku and product_status_line.name and (not product_status_line.product_variant_name):
                        name = product_status_line.product_sku + " - " + product_status_line.name
                    elif product_status_line.product_sku and (not product_status_line.name) and product_status_line.product_variant_name:
                        name = product_status_line.product_sku + " - " + product_status_line.product_variant_name
                    elif (not product_status_line.product_sku) and product_status_line.name and product_status_line.product_variant_name:
                        name = product_status_line.name + " - " + product_status_line.product_variant_name
                    elif (not product_status_line.product_sku) and (not product_status_line.name) and product_status_line.product_variant_name:
                        name = product_status_line.product_variant_name
                    elif (not product_status_line.product_sku) and product_status_line.name and (not product_status_line.product_variant_name):
                        name = product_status_line.name
                    elif product_status_line.product_sku and (not product_status_line.name) and (not product_status_line.product_variant_name):
                        name = product_status_line.product_sku
                    else:
                        pass
            for product_status_line in self.env['purchase.order.line.for.tracking'].browse(self.env.context.get('active_ids', False)):
                index = 0
                product_status_status_id = product_status_line.product_status_status.id
                for product_tracking_line in product_status_line.product_tracking_lines:
                        product_tracking_lines_ids = []
                        for inner_product_status_line in self.env['purchase.order.line.for.tracking'].browse(self.env.context.get('active_ids', False)):
                            product_tracking_lines_ids += [inner_product_status_line.product_tracking_lines[index].id]
                        index += 1
                        record = {
                                    'tracking_line_ids': [[6, 0, product_tracking_lines_ids]],
                                    'delivery_carrier': product_tracking_line.delivery_carrier.id,
                                    'delivery_carrier_url': product_tracking_line.delivery_carrier_url,
                                    'tracking_number': product_tracking_line.tracking_number,
                                    'tracking_number_url': product_tracking_line.tracking_number_url,
                                    'generated_tracking_number_url': product_tracking_line.generated_tracking_number_url,
                                }
                        records.append([0,0,record])
                break
        if self.env.context.get('active_ids', False) and self._context.get('purchase_decoration_groups', False):
            for purchase_decoration_group in self.env['purchase.decoration.group.for.tracking'].browse(self.env.context.get('active_ids', False)):
                if name:
                    if purchase_decoration_group.group_sequence and purchase_decoration_group.group_id:
                        name = name + ' <br/> ' + purchase_decoration_group.group_sequence + " - " + purchase_decoration_group.group_id
                    elif purchase_decoration_group.group_sequence and (not purchase_decoration_group.group_id):
                        name = name + ' <br/> ' + purchase_decoration_group.group_sequence
                    elif not (purchase_decoration_group.group_sequence) and purchase_decoration_group.group_id:
                        name = name + ' <br/> ' + purchase_decoration_group.group_id
                    else:
                        pass
                else:
                    if purchase_decoration_group.group_sequence and purchase_decoration_group.group_id:
                        name = purchase_decoration_group.group_sequence + " - " + purchase_decoration_group.group_id
                    elif purchase_decoration_group.group_sequence and (not purchase_decoration_group.group_id):
                        name = purchase_decoration_group.group_sequence
                    elif not (purchase_decoration_group.group_sequence) and purchase_decoration_group.group_id:
                        name = purchase_decoration_group.group_id
                    else:
                        pass
            for purchase_decoration_group in self.env['purchase.decoration.group.for.tracking'].browse(self.env.context.get('active_ids', False)):
                index = 0
                product_status_status_id = purchase_decoration_group.product_status_status.id
                for product_tracking_line in purchase_decoration_group.product_tracking_lines:
                        product_tracking_lines_ids = []
                        for inner_purchase_decoration_group in self.env['purchase.decoration.group.for.tracking'].browse(self.env.context.get('active_ids', False)):
                            product_tracking_lines_ids += [inner_purchase_decoration_group.product_tracking_lines[index].id]
                        index += 1
                        record = {
                                    'tracking_line_ids': [[6, 0, product_tracking_lines_ids]],
                                    'delivery_carrier': product_tracking_line.delivery_carrier.id,
                                    'delivery_carrier_url': product_tracking_line.delivery_carrier_url,
                                    'tracking_number': product_tracking_line.tracking_number,
                                    'tracking_number_url': product_tracking_line.tracking_number_url,
                                    'generated_tracking_number_url': product_tracking_line.generated_tracking_number_url,
                                }
                        records.append([0,0,record])
                break
        res.update({'name':name, 'product_status_status': product_status_status_id, 'edit_product_status_line_info_line_ids': records})
        return res

    @api.multi
    def edit_product_status_lines(self):
        if self.env.context.get('active_ids', False) and self._context.get('product_status_line', False):
            for edit_product_status_line_info_line_id in self.edit_product_status_line_info_line_ids:
                product_status_lines = self.env['purchase.order.line.for.tracking'].browse(self.env.context.get('active_ids', False))
                if not edit_product_status_line_info_line_id.tracking_line_ids:
                    for product_status_line in product_status_lines:
                        self.env['product.tracking.numbers'].create({
                            'delivery_carrier': edit_product_status_line_info_line_id.delivery_carrier.id,
                            'tracking_number': edit_product_status_line_info_line_id.tracking_number,
                            'tracking_number_url': edit_product_status_line_info_line_id.tracking_number_url,
                            'purchase_order_line_for_tracking_id': product_status_line.id,
                        })
                else:
                    self.env['product.tracking.numbers'].browse(edit_product_status_line_info_line_id.tracking_line_ids.ids).write({
                            'delivery_carrier': edit_product_status_line_info_line_id.delivery_carrier.id,
                            'tracking_number': edit_product_status_line_info_line_id.tracking_number,
                            'tracking_number_url': edit_product_status_line_info_line_id.tracking_number_url,
                        })
                product_status_lines.write({'product_status_status': self.product_status_status.id})
        if self.env.context.get('active_ids', False) and self._context.get('purchase_decoration_groups', False):
            for edit_product_status_line_info_line_id in self.edit_product_status_line_info_line_ids:
                purchase_decoration_groups = self.env['purchase.decoration.group.for.tracking'].browse(self.env.context.get('active_ids', False))
                if not edit_product_status_line_info_line_id.tracking_line_ids:
                    for purchase_decoration_group in purchase_decoration_groups:
                        self.env['product.tracking.numbers'].create({
                            'delivery_carrier': edit_product_status_line_info_line_id.delivery_carrier.id,
                            'tracking_number': edit_product_status_line_info_line_id.tracking_number,
                            'tracking_number_url': edit_product_status_line_info_line_id.tracking_number_url,
                            'purchase_group_id': purchase_decoration_group.id,
                        })
                else:
                    self.env['product.tracking.numbers'].browse(edit_product_status_line_info_line_id.tracking_line_ids.ids).write({
                            'delivery_carrier': edit_product_status_line_info_line_id.delivery_carrier.id,
                            'tracking_number': edit_product_status_line_info_line_id.tracking_number,
                            'tracking_number_url': edit_product_status_line_info_line_id.tracking_number_url,
                        })
                purchase_decoration_groups.write({'product_status_status': self.product_status_status.id})

class EditProductStatusLineInfoLine(models.TransientModel):
    _name = 'edit.product.status.line.info.line'

    edit_product_status_line_info_id = fields.Many2one(
        'edit.product.status.line.info',
        string='Edit Product Status Line Info',
        ondelete="cascade")
    delivery_carrier = fields.Many2one('delivery.carrier','Delivery Method', required="True")
    delivery_carrier_url = fields.Char(related="delivery_carrier.tracking_url", string="Base URL", readonly=True)
    tracking_number = fields.Char(string="Tracking Number")
    tracking_number_url = fields.Char(string="Tracking URL")
    generated_tracking_number_url = fields.Char(compute="_compute_generated_tracking_number_urll",string="Tracking URL", store=True)
    tracking_line_ids = fields.Many2many('product.tracking.numbers', 
        relation= "edit_product_status_line_info_line_with_tracking_line_rel",
        column1= "edit_product_status_line_info_line_id",
        column2= "tracking_line_id",
        string= "Tracking Lines")
        
    @api.one
    @api.depends('delivery_carrier_url', 'tracking_number', 'tracking_number_url')
    def _compute_generated_tracking_number_urll(self):
        self.generated_tracking_number_url = ''
        if self.delivery_carrier_url and self.tracking_number:
            self.generated_tracking_number_url = self.delivery_carrier_url + self.tracking_number
        if not self.delivery_carrier_url:
            if self.tracking_number_url:
                self.generated_tracking_number_url = self.tracking_number_url

    def _clean_tracking_number_url(self, tracking_number_url):
        (scheme, netloc, path, params, query, fragment) = urlparse.urlparse(tracking_number_url)
        if not scheme:
            if not netloc:
                netloc, path = path, ''
            tracking_number_url = urlparse.urlunparse(('http', netloc, path, params, query, fragment))
        return tracking_number_url

    @api.multi
    def write(self, vals):
        if vals.get('tracking_number_url'):
            vals['tracking_number_url'] = self._clean_tracking_number_url(vals['tracking_number_url'])
        result = super(EditProductStatusLineInfoLine, self).write(vals)
        return result

    @api.model
    def create(self, vals):
        if vals.get('tracking_number_url'):
            vals['tracking_number_url'] = self._clean_tracking_number_url(vals['tracking_number_url'])
        result = super(EditProductStatusLineInfoLine, self).create(vals)
        return result