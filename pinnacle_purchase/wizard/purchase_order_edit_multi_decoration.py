# -*- coding: utf-8 -*-
from openerp import models, fields, api, _
from openerp.addons.pinnacle_purchase.wizard.purchase_common import purchase_common
from openerp.exceptions import UserError
import base64
import urllib2

class PurchaseOrderEditMultiDecoration(models.TransientModel):
    _name = 'purchase.order.edit.multi.decoration'

    name = fields.Char(string='Edit Decoration To')
    purchase_order_edit_decoration_ids = fields.One2many(
        'purchase.order.edit.multi.decoration.line',
        'purchase_order_edit_decoration_id',
        'Purchase Order Add Decoration Lines',
        required=True)
    is_decoration_disable = fields.Boolean(string="Blank Product")

    @api.model
    def default_get(self, fields):
        res = super(PurchaseOrderEditMultiDecoration,self).default_get(fields)
        name = False
        records = []
        if self.env.context.get('active_ids', False):
            for purchase_order_line in self.env['purchase.order.line'].browse(self.env.context.get('active_ids', False)):
                index = 0
                for decoration in purchase_order_line.decorations:
                        decoration_ids = []
                        for order_line in self.env['purchase.order.line'].browse(self.env.context.get
                                       ('active_ids', False)):
                            decoration_ids += [
                                order_line.decorations[index].id]
                        index += 1
                        record = {'purchase_order_line_decoration_ids':
                                     [[6, 0, decoration_ids]],
                                     'imprint_location':
                                     decoration.imprint_location and
                                     decoration.imprint_location.id or False,
                                     'imprint_method':
                                     decoration.imprint_method and
                                     decoration.imprint_method.id or False,
                                     'customer_art':
                                     decoration.customer_art and
                                     decoration.customer_art.id or False,
                                     'finisher':
                                     decoration.finisher and
                                     decoration.finisher.id or False,
                                     'pms_code':
                                     decoration.pms_code.ids and
                                     [[6, 0, decoration.pms_code.ids]] or False,
                                     'stock_color':
                                     decoration.stock_color.ids and
                                     [[6, 0, decoration.stock_color.ids]] or
                                     False,
                                     'vendor_decoration_location':
                                     decoration.vendor_decoration_location and
                                     decoration.vendor_decoration_location.id or
                                     False,
                                     'vendor_decoration_method':
                                     decoration.vendor_decoration_method and
                                     decoration.vendor_decoration_method.id or
                                     False,
                                     'vendor_prod_specific': decoration.vendor_prod_specific,
                                     'already_existed_decoration_line': True,
                                     'pms_color_code': decoration.pms_color_code,
                                     'stock_color_char': decoration.stock_color_char
                                }
                        records.append([0,0,record])
                break
        if self.env.context.get('active_ids', False) and not name:
            for purchase_order_line in self.env['purchase.order.line'].browse(self.env.context.get('active_ids', False)):
                if name:
                    if purchase_order_line.product_sku and purchase_order_line.name and purchase_order_line.product_variant_name:
                        name = name + ' <br/> ' + purchase_order_line.product_sku + " - " + purchase_order_line.name + " - " + purchase_order_line.product_variant_name
                    elif purchase_order_line.product_sku and purchase_order_line.name and (not purchase_order_line.product_variant_name):
                        name = name + ' <br/> ' + purchase_order_line.product_sku + " - " + purchase_order_line.name
                    elif purchase_order_line.product_sku and (not purchase_order_line.name) and purchase_order_line.product_variant_name:
                        name = name + ' <br/> ' + purchase_order_line.product_sku + " - " + purchase_order_line.product_variant_name
                    elif (not purchase_order_line.product_sku) and purchase_order_line.name and purchase_order_line.product_variant_name:
                        name = name + ' <br/> ' + purchase_order_line.name + " - " + purchase_order_line.product_variant_name
                    elif (not purchase_order_line.product_sku) and (not purchase_order_line.name) and purchase_order_line.product_variant_name:
                        name = name + ' <br/> ' + purchase_order_line.product_variant_name
                    elif (not purchase_order_line.product_sku) and purchase_order_line.name and (not purchase_order_line.product_variant_name):
                        name = name + ' <br/> ' + purchase_order_line.name
                    elif purchase_order_line.product_sku and (not purchase_order_line.name) and (not purchase_order_line.product_variant_name):
                        name = name + ' <br/> ' + purchase_order_line.product_sku
                    else:
                        pass
                else:
                    if purchase_order_line.product_sku and purchase_order_line.name and purchase_order_line.product_variant_name:
                        name = purchase_order_line.product_sku + " - " +purchase_order_line.name + " - " + purchase_order_line.product_variant_name
                    elif purchase_order_line.product_sku and purchase_order_line.name and (not purchase_order_line.product_variant_name):
                        name = purchase_order_line.product_sku + " - " + purchase_order_line.name
                    elif purchase_order_line.product_sku and (not purchase_order_line.name) and purchase_order_line.product_variant_name:
                        name = purchase_order_line.product_sku + " - " + purchase_order_line.product_variant_name
                    elif (not purchase_order_line.product_sku) and purchase_order_line.name and purchase_order_line.product_variant_name:
                        name = purchase_order_line.name + " - " + purchase_order_line.product_variant_name
                    elif (not purchase_order_line.product_sku) and (not purchase_order_line.name) and purchase_order_line.product_variant_name:
                        name = purchase_order_line.product_variant_name
                    elif (not purchase_order_line.product_sku) and purchase_order_line.name and (not purchase_order_line.product_variant_name):
                        name = purchase_order_line.name
                    elif purchase_order_line.product_sku and (not purchase_order_line.name) and (not purchase_order_line.product_variant_name):
                        name = purchase_order_line.product_sku
                    else:
                        pass
        res.update({'name':name, 'purchase_order_edit_decoration_ids':records, 'is_decoration_disable': self.env.context.get('is_decoration_disable', False)})
        return res

    @api.multi
    def edit_purchase_order_line_decoration(self):
        if self.env['purchase.order'].browse(self.env.context.get('order_id', False)):
            for purchase_order_line in self.env['purchase.order'].browse(self.env.context.get('order_id', False)).order_line:
                purchase_order_line.select_decoration_line = False
        imprint_obj = self.env['product.imprint']
        for purchase_order_edit_decoration_id in self.purchase_order_edit_decoration_ids:
            order_line_index = 0
            while order_line_index < len(self.env['purchase.order.line'].browse(self.env.context.get('active_ids', False))):
                if purchase_order_edit_decoration_id.imprint_location and purchase_order_edit_decoration_id.imprint_method and purchase_order_edit_decoration_id.finisher and (self.env['purchase.order.line'].browse(self.env.context.get('active_ids', False))[order_line_index]):
                    finisher_decorations = (self.env['purchase.order.line'].browse(self.env.context.get('active_ids', False))[order_line_index]).product_id.finisher_ids.search(['&',('product_id','=',(self.env['purchase.order.line'].browse(self.env.context.get('active_ids', False))[order_line_index]).product_id.product_tmpl_id.id),('finisher_id','=',purchase_order_edit_decoration_id.finisher.id)])
                    if finisher_decorations:
                        finisher_decoration = finisher_decorations[0]
                        finisher_finishing_types = finisher_decoration.product_imprint_ids.search(['&',('product_finisher_id','=',finisher_decoration.id),'&',('decoration_location','=',purchase_order_edit_decoration_id.imprint_location.id),('decoration_method','=',purchase_order_edit_decoration_id.imprint_method.id)])
                        if finisher_finishing_types:
                            finisher_finishing_type = finisher_finishing_types[0]
                            if (len(purchase_order_edit_decoration_id.stock_color.ids) + len(purchase_order_edit_decoration_id.pms_code.ids)) > finisher_finishing_type.color_limit and finisher_finishing_type.color_limit != 0:
                                product_tmpl_name = str((self.env['purchase.order.line'].browse(self.env.context.get('active_ids', False))[order_line_index]).product_id.product_tmpl_id.name)
                                raise UserError(_('You can only enter total '+str(finisher_finishing_type.color_limit)+' colors for \n\n - Product Template : '+ product_tmpl_name +'\n - Finisher : '+purchase_order_edit_decoration_id.finisher.name+'\n - Decoration Location : '+purchase_order_edit_decoration_id.imprint_location.name+ '\n - Decoration Method : '+purchase_order_edit_decoration_id.imprint_method.name ))
                order_line_index += 1
        if self.env.context.get('active_ids', False):
            to_be_deleted = []
            for purchase_order_line in self.env['purchase.order.line'].browse(self.env.context.get('active_ids', False)):
                purchase_order_line.is_decoration_disable = self.is_decoration_disable
                if self.is_decoration_disable:
                    purchase_order_line.decorations.unlink()
                    continue
                for decoration in purchase_order_line.decorations:
                    delete = True
                    for purchase_order_edit_decoration_line_id in self.purchase_order_edit_decoration_ids:
                        if decoration.id in purchase_order_edit_decoration_line_id.purchase_order_line_decoration_ids.ids:
                            delete = False
                    if delete:
                        to_be_deleted.append(decoration.id)
            if not self.is_decoration_disable:
                self.env['sale.order.line.decoration'].browse(
                    to_be_deleted).unlink()
                for purchase_order_edit_decoration_line_id in self.purchase_order_edit_decoration_ids:
                    for purchase_order_line in self.env['purchase.order.line'].browse(self.env.context.get('active_ids', False)):
                        if not purchase_order_edit_decoration_line_id.purchase_order_line_decoration_ids:
                            self.env['sale.order.line.decoration'].create(
                                {'imprint_location':
                                 purchase_order_edit_decoration_line_id.
                                 imprint_location and
                                 purchase_order_edit_decoration_line_id.
                                 imprint_location.id or
                                 False,
                                 'imprint_method':
                                 purchase_order_edit_decoration_line_id.
                                 imprint_method and
                                 purchase_order_edit_decoration_line_id.
                                 imprint_method.id or
                                 False,
                                 'customer_art':
                                 purchase_order_edit_decoration_line_id.
                                 customer_art and
                                 purchase_order_edit_decoration_line_id.
                                 customer_art.id or
                                 False,
                                 'finisher': purchase_order_edit_decoration_line_id.
                                 finisher and
                                 purchase_order_edit_decoration_line_id.
                                 finisher.id or
                                 False,
                                 'pms_code': purchase_order_edit_decoration_line_id.
                                 pms_code.ids and
                                 [[6, 0, purchase_order_edit_decoration_line_id.
                                   pms_code.ids]] or False,
                                 'stock_color':
                                 purchase_order_edit_decoration_line_id.
                                 stock_color.ids and
                                 [[6, 0, purchase_order_edit_decoration_line_id.
                                   stock_color.ids]] or False,
                                 'vendor_decoration_location':
                                 purchase_order_edit_decoration_line_id.vendor_decoration_location and
                                 purchase_order_edit_decoration_line_id.vendor_decoration_location.id or
                                 False,
                                 'vendor_decoration_method':
                                 purchase_order_edit_decoration_line_id.vendor_decoration_method and
                                 purchase_order_edit_decoration_line_id.vendor_decoration_method.id or
                                 False,
                                 'vendor_prod_specific': purchase_order_edit_decoration_line_id.vendor_prod_specific,
                                 'purchase_order_line_id': purchase_order_line.id,
                                 'purchase_order_id': purchase_order_line.order_id and purchase_order_line.order_id.id or False,
                                 'pms_color_code': purchase_order_edit_decoration_line_id.pms_color_code,
                                 'stock_color_char': purchase_order_edit_decoration_line_id.stock_color_char})
                    else:
                        if purchase_order_edit_decoration_line_id.purchase_order_line_decoration_ids:
                            old_decoration_lines = self.env['sale.order.line.decoration'].browse(purchase_order_edit_decoration_line_id.purchase_order_line_decoration_ids.ids)
                            if old_decoration_lines:
                                if old_decoration_lines[0].imprint_location != purchase_order_edit_decoration_line_id.imprint_location or old_decoration_lines[0].imprint_method != purchase_order_edit_decoration_line_id.imprint_method or old_decoration_lines[0].finisher != purchase_order_edit_decoration_line_id.finisher or old_decoration_lines[0].stock_color.ids != purchase_order_edit_decoration_line_id.stock_color.ids:                                
                                    for old_decoration_line in old_decoration_lines:
                                        old_decoration_line.purchase_order_line_id.decoration_group_sequences = False
                            self.env['sale.order.line.decoration'].browse(
                                purchase_order_edit_decoration_line_id.
                                purchase_order_line_decoration_ids.ids).write({
                                    'imprint_location':
                                    purchase_order_edit_decoration_line_id.
                                    imprint_location and
                                    purchase_order_edit_decoration_line_id.
                                    imprint_location.id or
                                    False,
                                    'imprint_method':
                                    purchase_order_edit_decoration_line_id.
                                    imprint_method and
                                    purchase_order_edit_decoration_line_id.
                                    imprint_method.id or
                                    False,
                                    'customer_art':
                                    purchase_order_edit_decoration_line_id.
                                    customer_art and
                                    purchase_order_edit_decoration_line_id.
                                    customer_art.id or
                                    False,
                                    'finisher':
                                    purchase_order_edit_decoration_line_id.
                                    finisher and
                                    purchase_order_edit_decoration_line_id.
                                    finisher.id or
                                    False,
                                    'pms_code':
                                    purchase_order_edit_decoration_line_id.
                                    pms_code.ids and
                                    [[6, 0,
                                      purchase_order_edit_decoration_line_id.
                                      pms_code.ids]] or [[6, 0, []]],
                                    'stock_color':
                                    purchase_order_edit_decoration_line_id.
                                    stock_color.ids and
                                    [[6, 0,
                                      purchase_order_edit_decoration_line_id.
                                      stock_color.ids]] or [[6, 0, []]],
                                    'vendor_decoration_location':
                                    purchase_order_edit_decoration_line_id.vendor_decoration_location and
                                    purchase_order_edit_decoration_line_id.vendor_decoration_location.id or
                                    False,
                                    'vendor_decoration_method':
                                    purchase_order_edit_decoration_line_id.vendor_decoration_method and
                                    purchase_order_edit_decoration_line_id.vendor_decoration_method.id or
                                    False,
                                    'vendor_prod_specific': purchase_order_edit_decoration_line_id.vendor_prod_specific,
                                    'pms_color_code': purchase_order_edit_decoration_line_id.pms_color_code,
                                    'stock_color_char': purchase_order_edit_decoration_line_id.stock_color_char})


class PurchaseOrderEditMultiDecorationLine(models.TransientModel):
    _name = 'purchase.order.edit.multi.decoration.line'

    @api.multi
    @api.depends('stock_color')
    def _get_pms_color_code(self):
        for this in self:
            pms_color_code = purchase_common().compute_pms_color_code(this)
            this.pms_color_code = pms_color_code

    @api.multi
    @api.depends('stock_color')
    def _get_stock_color(self):
        for rec in self:
            stock_color = []
            for stk in rec.stock_color:
                stock_color.append(stk.name)
            rec.stock_color_char = ", ".join(stock_color)

    @api.model
    def _default_finisher(self):
        return purchase_common().get_default_value_for_finisher_field(self)
        
    purchase_order_line_decoration_ids = fields.Many2many(
        'sale.order.line.decoration',
        relation="purchase_order_line_decoration_multi_decoration_line_rel",
        column1="purchase_order_edit_multi_decoration_line_id",
        column2="purchase_order_line_decoration_id",
        string='Decoration Line Numbers')
    purchase_order_edit_decoration_id = fields.Many2one(
        'purchase.order.edit.multi.decoration',
        string='Purchase Order Edit Decoration',
        ondelete="cascade")
    imprint_location = fields.Many2one(
        'decoration.location', string="Imprint Location", domain=lambda self: self._domain_filter_decoration('imprint_location'))
    imprint_method = fields.Many2one(
        'decoration.method', string="Imprint Method", domain=lambda self: self._domain_filter_decoration('imprint_method'))
    customer_art = fields.Many2one(
        'product.decoration.customer.art', string="Art (file or text)", domain=lambda self: self._domain_filter_decoration('customer_art') )
    finisher = fields.Many2one(
        'res.partner', string="Finisher", domain=lambda self: self._domain_filter_decoration('finisher'), default=_default_finisher)
    pms_code = fields.Many2many(
        'product.decoration.pms.code',
        relation="pms_code_edit_multi_decoration_line_of_purchase_rel",
        column1="purchase_order_edit_multi_decoration_line_id",
        column2="product_decoration_pms_code_id",
        string="PMS Code")
    pms_color_code = fields.Char(compute="_get_pms_color_code")
    stock_color_char = fields.Char(compute='_get_stock_color')
    stock_color = fields.Many2many(
        'product.decoration.stock.color',
        relation="stock_color_edit_multi_decoration_line_of_purchase_rel",
        column1="purchase_order_edit_multi_decoration_line_id",
        column2="product_decoration_stock_color_id",
        string="Stock Color")
    customer_arts = fields.Many2many(
        'product.decoration.customer.art', compute="_compute_customer_arts")
    already_existed_decoration_line = fields.Boolean(default=False)
    global_search = fields.Boolean(
        'All options',
        help='Global Search On Location, Method, Finisher',
        default=False)
    vendor_decoration_location = fields.Many2one('vendor.decoration.location', 'Vendor Imprint Location')
    vendor_decoration_method = fields.Many2one('vendor.decoration.method', 'Vendor Imprint Method')
    vendor_prod_specific = fields.Char('Vendor Imprint Product Specific')
    image_preview = fields.Binary(
        compute="_compute_image_preview", string='Image Preview')
    preview_text = fields.Html(
        compute="_compute_preview_text", string="Text Preview", store=True)

    @api.one
    @api.depends('customer_art', 'customer_art.name_url')
    def _compute_image_preview(self):
        self.image_preview = False
        if self.customer_art and self.customer_art.name_url:
            cfs_url = str(self.env['ir.config_parameter'].get_param('cfs.web.url'))
            final_url = cfs_url + self.customer_art.name_url
            url = purchase_common().url_fix(final_url)
            try:
                self.image_preview = base64.encodestring(urllib2.urlopen(url).read())
            except Exception as e:
                self.image_preview = ''

    @api.one
    @api.depends('customer_art', 'customer_art.text', 'customer_art.font')
    def _compute_preview_text(self):
        if self.customer_art and self.customer_art.font and self.customer_art.text:
            self.preview_text = "<font style=\"font-family: '" + self.customer_art.font.name + "'\"><h2>" + self.customer_art.text + "</h2></font>"
            
    @api.onchange('customer_art')
    def _onchange_customer_art(self):
        new_context = dict(self.env.context).copy()
        new_context.update({'model':'purchase.order.edit.multi.decoration'})
        self.env.context = new_context
        customer_arts = purchase_common().onchange_customer_art(self) 
        return {'domain': {
            'customer_art': [('id', 'in', customer_arts)],
        }}

    @api.model
    def _domain_filter_decoration(self, field=False):
        if field:
            res = self._onchange_imprint_method()
            if field == 'customer_art':
                res = self._onchange_customer_art()
            return res and res['domain'][field] or False
    
    
    @api.onchange('imprint_method', 'imprint_location', 'finisher',
                  'global_search', 'customer_art')
    def _onchange_imprint_method(self):
        if self and not self.finisher:
            self.imprint_method = False
            self.imprint_location = False
        imprint_methods, vendor_imprint_methods, imprint_locations, vendor_imprint_locations, finishers, all_finishers_assigned_to_product = purchase_common().onchange_imprint_method(self)
        stock_colors = purchase_common().onchange_stock_color(self)
        pms_codes = purchase_common().onchange_pms_code(self)
        if 'imprint_method' in self.env.context or 'imprint_location' in self.env.context or 'finisher' in self.env.context and self:
            vendor_decoration_location, vendor_decoration_method, vendor_prod_specific = purchase_common().return_vendor_decoration_value(self)
            self.vendor_decoration_method = vendor_decoration_method
            self.vendor_decoration_location = vendor_decoration_location
            self.vendor_prod_specific = vendor_prod_specific
        return {'domain': {
            'imprint_method': ['|', ('id', 'in', imprint_methods), ('id', 'in', vendor_imprint_methods)],
            'imprint_location': ['|', ('id', 'in', imprint_locations), ('id', 'in', vendor_imprint_locations)],
            'finisher': ['|', ('id', 'in', finishers), ('id', 'in', all_finishers_assigned_to_product)],
            'stock_color': [('id', 'in', stock_colors)],
            'pms_code': [('id', 'in', pms_codes)]
        }}

    @api.onchange('imprint_method', 'finisher', 'vendor_decoration_method', 'global_search')
    def _onchange_imprint_method_or_finisher(self):
        if not self.global_search:
            vendor_decoration_methods, vendor_decoration_locations = purchase_common().return_vendor_decoration_domain(self)
            return {    
                        'domain': {
                            'vendor_decoration_method': [('id', 'in', vendor_decoration_methods)],
                            'vendor_decoration_location': [('id', 'in', vendor_decoration_locations)]
                        }
                    }
        else:
            vendor_decoration_methods = self.env['vendor.decoration.method'].search([]).ids
            vendor_decoration_locations = self.env['vendor.decoration.location'].search([]).ids
            return {    
                        'domain': {
                            'vendor_decoration_method': [('id', 'in', vendor_decoration_methods)],
                            'vendor_decoration_location': [('id', 'in', vendor_decoration_locations)]
                        }
                    }
                
    @api.multi
    def _compute_customer_arts(self):
        if self.env.context.get('order_id', False):
            sale_order = self.env['purchase.order'].browse(
                self.env.context.get('order_id', False))
            if sale_order:
                self.customer_arts = [(6, 0, sale_order.customer_arts.ids)]
        else:
            self.customer_arts = [(6, 0, [])]
