# -*- coding: utf-8 -*-
from openerp import models, fields, api, _
import openerp.addons.decimal_precision as dp
from openerp.exceptions import UserError
import datetime

class PurchaseOrderWizardEditChange(models.TransientModel):
    _name = 'purchase.order.wizard.change'

    name = fields.Char(string="Purchase order", readonly=True)
    purchase_order_lines = fields.One2many(
        'purchase.order.line.wizard.change',
        'purchase_order_id',
        'Edit Purchase Order',
        required=True)
    purchase_order_id = fields.Many2one("purchase.order", readonly="1")
    partner_id = fields.Many2one('res.partner', related='purchase_order_id.partner_id')
    po_type = fields.Selection([('regular', 'Regular'), ('sample', 'Sample'), ('non_billable', 'Non Billable Sample'), ('internal_purchase_order','Internal Purchase Order')], related="purchase_order_id.po_type")

    @api.model
    def default_get(self, fields):
        res = super(PurchaseOrderWizardEditChange, self).default_get(fields) 
        def create_dict(value):
            return {
                'change_qty': value.product_qty,
                'change_variant': value.product_id.id,
                'product_template': value.product_id.product_tmpl_id.id,
                'purchase_order_line_id': value.id,
                'price_unit': value.price_unit,
                'is_blank': value.blank_line,
                'product_sku': value.product_sku,
                'product_variant_name': value.product_variant_name,
                'description': value.name
            }
        res.update({'purchase_order_id':self.env.context.get('purchase_order'),
                    'name':  self.env['purchase.order'].browse(self.env.context.get('purchase_order')).name,
                    'purchase_order_lines':[(0, 0, create_dict(rec)) for rec in self.env['purchase.order.line'].browse(self.env.context.get('active_ids'))]})
        return res

    @api.multi
    def save_change(self):
        PurchaseOrderLineForTracking = self.env['purchase.order.line.for.tracking'] 
        for this in self.purchase_order_lines:
            value = {}
            po = this.purchase_order_line_id
            product_qty_changed = False
            product_variant_changed = False
            seller_product_sku, seller_product_variant_name, seller_description, seller_product_name = this.product_template.fetch_seller_all_info(self.purchase_order_id.partner_id, this.change_variant)
            if this.change_qty != this.purchase_order_line_id.product_qty:
                value['product_qty'] = this.change_qty
                product_qty_changed = True
            if this.change_variant.id != this.purchase_order_line_id.product_id.id:
                self.purchase_order_id.onchange_order_line_notes(this.change_variant)
                value['product_id'] = this.change_variant.id
                value['name'] = seller_description
                value['product_variant_name'] = seller_product_variant_name
                value['product_sku'] = seller_product_sku
                product_variant_changed = True
            po.write(value)
            track_line = PurchaseOrderLineForTracking.search([('purchase_order_line_id', '=', po.id)])
            if product_variant_changed:
                if po.decorations and po.decorations.exists():
                    po.decorations.exists().unlink()
            elif product_qty_changed:
                for decoration in po.decorations:
                    if decoration.decoration_group_id:
                        if decoration.decoration_group_id.charge_lines.exists():
                            decoration.decoration_group_id.charge_lines.exists().unlink()
            if track_line:
                track_line.write({
                    'name': seller_description,
                    'product_sku': seller_product_sku,
                    'product_variant_name': seller_product_variant_name,
                    })

class PurchaseOrderLineWizardEditChange(models.TransientModel):
    _name = 'purchase.order.line.wizard.change'

    purchase_order_id = fields.Many2one('purchase.order.wizard.change', string='Purchase Order')
    change_qty = fields.Float(string='Quantity', digits=dp.get_precision('Product Unit of Measure'), 
                        required=True, default=1.0)
    change_variant = fields.Many2one("product.product", string="Variants", required=True)
    product_template = fields.Many2one("product.template", string="Product Template")
    purchase_order_line_id = fields.Many2one("purchase.order.line", string="Purchase Order Line")
    price_unit = fields.Float(string='Cost Price', required=True, digits=dp.get_precision('Vendor Level Cost'))
    description = fields.Char()
    is_blank = fields.Boolean(default=False)
    product_sku = fields.Char()
    product_variant_name = fields.Char()

    @api.onchange('change_variant')
    def _onchange_change_variant_id(self):
        if self.change_variant.id:
            purchase_id = self.purchase_order_line_id.order_id
            self.product_sku, self.product_variant_name, self.description, test = self.change_variant.product_tmpl_id.fetch_seller_all_info(purchase_id.partner_id, self.change_variant, purchase_id.sale_order_id.name)
        else:
            self.description, self.product_sku, self.product_variant_name = ["","",""]


    @api.onchange('product_template')
    def _onchange_product_template_id(self):
        self.change_variant = False
