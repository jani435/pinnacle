# -*- coding: utf-8 -*-
from openerp import models, fields, api, _
from openerp.addons.pinnacle_purchase.wizard.purchase_common import purchase_common
from openerp.exceptions import UserError
import openerp.addons.decimal_precision as dp


class PurchaseOrderAddPackingSlip(models.TransientModel):
    _name = 'purchase.order.add.packing.slip'

    purchase_order_id = fields.Many2one('purchase.order', string="Purchase Order")
    ship_to_location = fields.Many2one('shipping.line', string="Ship To Location")
    ship_to_address_name = fields.Char(string="Address Name")
    ship_to_address_street = fields.Char(string="Street")
    ship_to_address_street2 = fields.Char(string="Street2")
    ship_to_address_zip = fields.Char(string="Zip")
    ship_to_address_city = fields.Char(string="City")
    ship_to_address_state_id = fields.Many2one("res.country.state", string='State', ondelete='restrict')
    ship_to_address_country_id = fields.Many2one('res.country', string='Country', ondelete='restrict')
    ship_to_address_phone = fields.Char(string="Phone")
    ship_date = fields.Date(string="Ship Date")
    delivery_date = fields.Date(string="Delivery Date")
    delivery_method = fields.Many2one('delivery.carrier', string="Delivery Method")
    order_lines = fields.One2many('purchase.order.add.packing.slip.order.line', 'purchase_order_add_packing_slip_id', string="Order Lines")
 
    @api.model
    def default_get(self, fields):
        res = super(PurchaseOrderAddPackingSlip,self).default_get(fields)
        if self.env.context.get('active_id', False):
            res.update({'purchase_order_id' : self.env['purchase.order'].browse(self.env.context.get('active_id', False)).id})
        return res

    @api.onchange('ship_to_location')
    def _onchange_ship_to_location(self):
        self.ship_to_address_street = False
        self.ship_to_address_street2 = False
        self.ship_to_address_zip = False
        self.ship_to_address_city = False
        self.ship_to_address_state_id = False
        self.ship_to_address_country_id = False
        self.ship_to_address_phone = False
        self.ship_date = False
        self.delivery_date = False
        self.delivery_method = False
        records = []
        if self.ship_to_location and self.purchase_order_id:
            for shipping_line in self.purchase_order_id.shipping_lines.sorted(key=lambda r: r.id):
                if self.ship_to_location.id == shipping_line.id:
                    self.ship_to_address_name = shipping_line.address_name
                    self.ship_to_address_street = shipping_line.street1
                    self.ship_to_address_street2 = shipping_line.street2
                    self.ship_to_address_zip = shipping_line.zipcode
                    self.ship_to_address_city = shipping_line.city
                    self.ship_to_address_state_id = shipping_line.state_id.id
                    self.ship_to_address_country_id = shipping_line.contact_country_id.id
                    self.ship_to_address_phone = shipping_line.contact and shipping_line.contact.phone or False
                    self.ship_date = shipping_line.ship_date
                    self.delivery_date = shipping_line.delivery_date
                    self.delivery_method = shipping_line.delivery_method.id
                    product_ids = []
                    if shipping_line.product_line:
                        product_ids = [product_line.product.id for product_line in shipping_line.product_line]
                    for order_line in self.purchase_order_id.order_line:
                        if order_line.product_id.id in product_ids:
                            record = {
                                'quantity': order_line.product_qty,
                                'name': order_line.name,
                                'product_sku': order_line.product_sku,
                                'variant': order_line.product_variant_name,
                            }
                            records.append([0,0,record])
                    for product_receive in self.purchase_order_id.product_receives:
                        if product_receive.product_id.id in product_ids:
                            record = {
                                'quantity': product_receive.product_qty,
                                'name': product_receive.name,
                                'product_sku': product_receive.product_sku,
                                'variant': product_receive.product_variant_name,
                            }
                            records.append([0,0,record])
        ship_to_location = []
        if self.purchase_order_id:
            for shipping_line in self.purchase_order_id.shipping_lines:
                if shipping_line.ship_to_location == 'no':
                    ship_to_location.append(shipping_line.id)
        self.update({'order_lines': records})
        return {'domain': {
            'ship_to_location': [('id', 'in', ship_to_location)],
        }}

    @api.multi
    def create_purchase_order_packing_slip(self):
        if self.ship_to_location:
            order_lines = []
            for order_line in self.order_lines:
                record = {
                    'quantity': order_line.quantity,
                    'name': order_line.name,
                    'product_sku': order_line.product_sku,
                    'variant': order_line.variant,
                }
                order_lines.append([0, 0, record])
            packing_slip = self.env['purchase.order.packing.slip'].create({
                'purchase_order_id' : self.purchase_order_id.id,
                'ship_to_location' : self.ship_to_location.id,
                'ship_to_address_name': self.ship_to_address_name,
                'ship_to_address_street': self.ship_to_address_street,
                'ship_to_address_street2': self.ship_to_address_street2,
                'ship_to_address_zip': self.ship_to_address_zip,
                'ship_to_address_city': self.ship_to_address_city,
                'ship_to_address_state_id' : self.ship_to_address_state_id.id,
                'ship_to_address_country_id' : self.ship_to_address_country_id.id,
                'ship_to_address_phone': self.ship_to_address_phone,
                'ship_date': self.ship_date,
                'delivery_date': self.delivery_date,
                'delivery_method': self.delivery_method.id,
                'order_lines': order_lines,
                })

class PurchaseOrderAddPackingSlipOrder(models.TransientModel):
    _name = 'purchase.order.add.packing.slip.order.line'

    purchase_order_add_packing_slip_id = fields.Many2one('purchase.order.add.packing.slip', string="Packing Slip", ondelete="cascade")
    quantity = fields.Float(string='Quantity', digits=dp.get_precision('Product Unit of Measure'))
    name = fields.Text(string='Description')
    product_sku = fields.Char(string="Product ID") 
    variant = fields.Char(string="Variant") 

