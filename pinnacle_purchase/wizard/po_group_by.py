from openerp import models, fields, api, _

class PurchaseTrackingWiz(models.TransientModel):
    _name = 'purchase.tracking.wiz'

    wiz_id = fields.Many2one('purchase.order.line.wiz', 'PO line Wizard')
    tracking_url = fields.Char('Tracking Number')

class purchase_order_line_wizard(models.TransientModel):
    _name = 'purchase.order.line.wiz'

    purchase_order = fields.Many2one('purchase.order', string="Purchase Order", readonly=1)
    sale_order = fields.Many2one('sale.order', string="Sale Order")
    vendor = fields.Many2one("res.partner", string="Vendor", readonly=1)
    status = fields.Char(string="Status")
    schedules_date = fields.Date('Scheduled Date')
    product_sku = fields.Char(string='Product ID')
    variant = fields.Char('Variant')
    #status = fields.Selection(string='Status', related='purchase_order.state', default='draft')
    tracking_number = fields.Char('Tracking')
    tracking_url_ids = fields.One2many('purchase.tracking.wiz', 'wiz_id', 'Tracking URLs')

    @api.multi
    def view_purchase_order(self):
        form_view_id = self.env.ref('purchase.purchase_order_form')
        if self.purchase_order:
            return {'type': 'ir.actions.act_window',
                    'view_mode': 'form',
                    'name': "Purchase Order",
                    'view_id': form_view_id.id,
                    'res_model': 'purchase.order',
                    'res_id': self.purchase_order.id,
                    }

    @api.multi
    def track(self):
        form_view_id = self.env.ref('pinnacle_purchase.purchase_order_wizard_form')
        if self:
            return {
                'name': _('Tracking'),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'purchase.order.line.wiz',
                'views': [(form_view_id.id, 'form')],
                'view_id': form_view_id.id,
                'target': 'new',
                'res_id': self[0].id,
            }
