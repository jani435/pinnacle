# -*- coding: utf-8 -*-
from openerp import models, fields, api, _
import openerp.addons.decimal_precision as dp
from openerp.exceptions import UserError
import datetime

class PurchaseOrderWizardChange(models.TransientModel):
    _name = 'purchase.order.wizard.change.add'

    name = fields.Char(string="Purchase order", readonly=True)
    product_template = fields.Many2one("product.template", string="Product Template")
    purchase_order_lines = fields.One2many(
        'purchase.order.line.wizard.change.add',
        'purchase_order_id',
        'Product Variants',
        required=True)
    purchase_order_id = fields.Many2one("purchase.order", readonly="1")
    partner_id = fields.Many2one('res.partner', related='purchase_order_id.partner_id')
    po_type = fields.Selection([('regular', 'Regular'), ('sample', 'Sample'), ('non_billable', 'Non Billable Sample'), ('internal_purchase_order','Internal Purchase Order')], related="purchase_order_id.po_type")


    @api.model
    def default_get(self, fields):
        res = super(PurchaseOrderWizardChange, self).default_get(fields)
        res.update({'purchase_order_id':self.env.context.get('purchase_order')})
        return res

    @api.multi
    def save_change(self):
        po_line = self.env['purchase.order.line']
        PurchaseOrderLineForTracking = self.env['purchase.order.line.for.tracking']
        for this in self.purchase_order_lines:
            seller_product_sku, seller_product_variant_name, seller_description, seller_product_name = self.product_template.fetch_seller_all_info(self.purchase_order_id.partner_id, this.change_variant)
            purchase_order_line = po_line.create({
                'name':  seller_description,
                'order_id': self.env.context.get('purchase_order'),
                'product_qty': this.change_qty,
                'product_id': this.change_variant.id or False,
                'product_variant_name': this.change_variant.with_context(product_name_get=True).name_get()[0][1],
                'product_uom':this.change_variant.uom_po_id.id or this.change_variant.uom_id.id,
                'price_unit': this.price_unit,
                'date_planned': datetime.datetime.now(),
                'product_variant_name': seller_product_variant_name,
                'product_sku': seller_product_sku,
                'can_decorate': True,
            })
            self.purchase_order_id.onchange_order_line_notes(this.change_variant)
            PurchaseOrderLineForTracking.create({
                        'purchase_order_line_id': purchase_order_line.id, 
                        'purchase_order_id':self.env.context.get('purchase_order'),
                        'auto_generate': True,
                        'product_sku': seller_product_sku,
                        'product_variant_name': seller_product_variant_name,
                        'name':  seller_description,
                        'qty': purchase_order_line.product_qty
                        })

class PurchaseOrderLineWizardChange(models.TransientModel):
    _name = 'purchase.order.line.wizard.change.add'

    purchase_order_id = fields.Many2one('purchase.order.wizard.change.add', string='Purchase Order')
    change_qty = fields.Float(string='Quantity', digits=dp.get_precision('Product Unit of Measure'), 
                        required=True, default=1.0)
    change_variant = fields.Many2one("product.product", string="Variants", required=True)
    price_unit = fields.Float(string='Cost Price', required=True, digits=dp.get_precision('Vendor Level Cost'))
