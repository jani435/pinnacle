from odoo import api, models, fields, _

class ProductTemplate(models.Model):
    _inherit = 'product.template'

    @api.model
    def name_search(self, name='', args=None, operator='ilike', limit=100):
        if self.env.context.get('search_seller_product'):
            if args is None:args = []
            domain = [('name', '=', self.env.context.get('search_seller_product'))]
            if self.env.context.get('po_type') in ["non_billable", "sample"]: domain+= [('sample_ok','=',True)]
            args+= [('id','in', list(set([supplier.product_tmpl_id.id for supplier in self.env['product.supplierinfo'].search(domain)])))] 
        return super(ProductTemplate, self).name_search(name=name, args=args, operator=operator, limit=limit)

    def _fetch_seller(self, partner_id):
        """
        This method is used for fetch Vendor Product Seller.

        :param partner_id: Integer.
        :returns: SupplierInfo Object or []
        """
        domain = [('product_tmpl_id', '=', self.id),('name','=', partner_id)]
        return self.env['product.supplierinfo'].search(domain, limit=1)

    def fetch_vendor_product_sku(self, partner_id, product_id, seller=None):
        """
        This method is used for fetch Vendor Product SKU.

        :param partner_id: Res Partner Id.
        :param product_id: Prouct Variants Id.
        :returns: Char
        """
        if seller is None:seller = self._fetch_seller(partner_id.id)
        attribute_code = product_id.product_sku
        variant_available = self.env['product.vendor.value.extend']

        if seller.id:
            domain = [('product_supplierinfo_id', '=', seller.id),
                        ('product_id', '=', product_id.id)]
            variant_exist = variant_available.search(domain)
            attribute_code = variant_exist.vendor_value_code if variant_exist.vendor_value_code else seller.product_code
        return attribute_code

    def fetch_vendor_attribute_value(self, partner_id , product_id, seller=None):
        """
        This method is used for fetch Vendor Product Attribute Value.

        :param partner_id: Res Partner Id.
        :param product_id: Prouct Variants Id.
        :returns: Char
        """
        if seller is None:seller = self._fetch_seller(partner_id.id)
        vendor_att_obj = self.env['product.vendor.attributes.extend']
        values = []
        if seller:
            domain = [('product_supplierinfo_id', '=', seller.id),
                    ('product_attribute_id', 'in', product_id.attribute_value_ids.ids)]
            for variant in vendor_att_obj.search(domain):
                values.append(variant.product_attribute_id.attribute_id.name + ': ' + (variant.vendor_attb_value if variant.vendor_attb_value else variant.product_attribute_id.name))
            return ', '.join(values)
        for variant in product_id.attribute_value_ids:
            values.append(variant.attribute_id.name +': '+ variant.name)
        return ', '.join(values)
    def fetch_seller_product_name(self, partner_id, seller=None):
        """
        This method is used for fetch Vendor Product Name.

        :param partner_id: Res Partner Id.
        :returns: Char or False
        """
        if seller is None:seller = self._fetch_seller(partner_id.id)
        if seller:
            return seller.product_name

        return self.name_get()[0][1]

    def fetch_seller_all_info(self, partner_id , product_id, sale_order=None):
        """
        This method is used for fetch All information of Seller.

        :param partner_id: Res Partner Id.
        :returns: Char or False
        """
        seller = self._fetch_seller(partner_id.id)
        seller_product_sku = self.fetch_vendor_product_sku(partner_id, product_id, seller)
        seller_product_variant_name  = self.fetch_vendor_attribute_value(partner_id, product_id, seller) or product_id.name_get()[0][1]
        seller_product_name = self.fetch_seller_product_name(partner_id, seller) or sale_order or ' '
        seller_description = ("[%s] %s (%s)")%(seller_product_sku,seller_product_name, seller_product_variant_name)
        return seller_product_sku, seller_product_variant_name, seller_description, seller_product_name
