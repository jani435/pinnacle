# -*- coding: utf-8 -*-

from openerp import models, fields, api, _
from openerp.exceptions import UserError
import openerp.addons.decimal_precision as dp
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from datetime import datetime
import string

class ShippingLine(models.Model):
    _inherit = 'shipping.line'
    _order = 'ship_date'

    @api.multi
    def add_blank_shipping_line(self):
        product_ref = self.env.ref('pinnacle_sales.product_product_blank_shipment_line')
        purchase_order_line = self.env['purchase.order.line']
        for this in self:
            line_cost = 0.00
            if self.delivery_line:
                for line in self.delivery_line:
                    if line.checked:
                        line_cost = line_cost + line.cost

                purchase_order_line.create({
                    'order_id': self.purchase_order_id.id,
                    'name': product_ref.name,
                    'product_qty': 1,
                    'product_uom': product_ref.uom_id.id,
                    'product_id': product_ref.id,
                    'product_sku': product_ref.product_sku,
                    'shipping_id': self.id,
                    'price_unit': line_cost,
                    'product_variant_name': product_ref.name,
                    'product_sku': product_ref.product_sku
                })

    @api.multi
    def name_get(self):
        result = super(ShippingLine, self).name_get()
        if self.env.context.get('purchase_order_packing_slip', False):
            customized_result = []
            for shipping_line in self:
                customized_result.append((shipping_line.id, shipping_line.address_name or ''))
            return customized_result
        return result

    @api.multi
    def write(self, vals):
        result = super(ShippingLine, self).write(vals)
        for this in self:
            this.shipping_sale_orders.write({'rush_supplier': self.rush_finisher.id,
                                            'rush_finisher': self.rush_supplier.id
                                            })
        return result

    @api.depends('delivery_line')
    def _compute_service_name(self):
        for this in self:
            if this.ship_to_location == 'no':
                if this.delivery_line:
                    service = this.delivery_line.search([('checked', '=', True), ('shipping', '=', this.id)])
                    if service:
                        this.service_names = service[0].service_type
                    else:
                        this.service_names = ''
                else:
                    this.method_name = ''
            if this.ship_to_location == 'yes':
                if this.delivery_line:
                    names = ''
                    for line in this.delivery_line:
                        if line.checked:
                            if names == '':
                                names = line.service_type
                                continue
                            if len(names) > 1:
                                names += ', '+line.service_type
                    this.service_names = names

    @api.one
    @api.depends('address_name', 'street1')
    def _compute_ship_to_name(self):
        if self.address_name:
            self.ship_to_name = self.address_name
            if self.street1:
                self.ship_to_name += ' '+self.street1

    purchase_order_id = fields.Many2one("purchase.order")
    po_type = fields.Char(compute="_compute_po_type")
    group_id = fields.Many2one('purchase.decoration.group.for.tracking')
    service_names = fields.Char(compute='_compute_service_name')
    ship_to_name = fields.Char(compute='_compute_ship_to_name')

    @api.multi
    def _compute_po_type(self):
        for this in self:
            this.po_type = this.purchase_order_id.po_type

    def get_supplier_shipping_detail(self, line, description=False):
        attribute_code = line.product.product_tmpl_id.fetch_vendor_product_sku(self.purchase_order_id.partner_id, line.product)
        name = line.product.product_tmpl_id.fetch_seller_product_name(self.purchase_order_id.partner_id)
        product_variant_name = line.product.product_tmpl_id.fetch_vendor_attribute_value(self.purchase_order_id.partner_id, line.product)
        qty = 0.00
        if self.purchase_order_id.po_type in ['sample', 'non_billable']:
            for o_line in self.purchase_order_id.order_line:
                if o_line.product_id.id == line.product.id:
                    supplier = self.purchase_order_id.partner_id
                    qty = o_line.product_qty
                    break
        else:
            for order in self.shipping_sale_orders:
                if order.product_id.id == line.product.id:
                    qty = order.product_qty
                    break
        if description:
            return "%s - [%s] %s (%s)" % (int(qty), attribute_code, name, product_variant_name )    
        return "%s - %s, %s [%s]"%(int(qty), attribute_code, name, product_variant_name)

class SaleOrderLineDecoration(models.Model):
    _inherit = 'sale.order.line'

    art_work_purchase_id = fields.Many2one('purchase.order', string="Purchase Order")
    decoration_line_id = fields.Many2one("purchase.order")
    name_purchase = fields.Char('Description')
    product_sku_purchase = fields.Char('Product ID')
    product_variant_name_purchase = fields.Char('Variant')
    purchase_order_lines = fields.One2many('purchase.order.line', 'sale_order_line_id', string="Created Purchase Order Lines")
    actual_margin_in_percentage = fields.Float(compute='_compute_actual_margin_in_percentage', string="SO + PO Margin(%)", digits=dp.get_precision('Product Price'), store=True, copy=False)
    actual_margin_in_percentage_duplicate = fields.Float(compute='_compute_actual_margin_in_percentage', string="SO + PO Margin(%)", digits=dp.get_precision('Product Price'), store=True, copy=False)
    actual_margin = fields.Float(compute='_compute_actual_margin', string="Actual Margin", digits=dp.get_precision('Product Price'), store=True, copy=False)

    @api.one
    @api.depends('product_id', 'purchase_order_lines', 'purchase_order_lines.price_unit', 'product_uom_qty', 'price_subtotal', 'order_id.currency_id', 'order_id.program_type')
    def _compute_actual_margin_in_percentage(self):
        self.actual_margin_in_percentage = 0.00
        self.actual_margin_in_percentage_duplicate = 0.00
        if self.product_id and not self.product_id.exclude_from_computing_sale_margins and (self.product_id.inventory_control_code not in ['inv_co', 'inv_cs'] or self.order_id.program_type != 'store'):
            if self.price_subtotal:
                price_unit = 0.00
                if self.purchase_order_lines:
                    price_unit = sum([self.order_id.currency_id.compute(purchase_order_line.price_unit, self.currency_id, round=False) for purchase_order_line in self.purchase_order_lines]) / len(self.purchase_order_lines)
                self.actual_margin_in_percentage = ((self.price_subtotal - (price_unit * self.product_uom_qty)) / self.price_subtotal) * 100
                self.actual_margin_in_percentage_duplicate = ((self.price_subtotal - (price_unit * self.product_uom_qty)) / self.price_subtotal) * 100

    @api.one
    @api.depends('product_id', 'purchase_order_lines', 'purchase_order_lines.price_unit', 'product_uom_qty', 'price_subtotal', 'order_id.currency_id', 'order_id.program_type')
    def _compute_actual_margin(self):
        price_unit = 0.00
        self.actual_margin = 0.00
        if self.product_id and not self.product_id.exclude_from_computing_sale_margins and (self.product_id.inventory_control_code not in ['inv_co', 'inv_cs'] or self.order_id.program_type != 'store'):
            if self.purchase_order_lines:
                price_unit = sum([self.order_id.currency_id.compute(purchase_order_line.price_unit, self.currency_id, round=False) for purchase_order_line in self.purchase_order_lines]) / len(self.purchase_order_lines)
            self.actual_margin = (self.price_subtotal - (price_unit * self.product_uom_qty))

    @api.multi
    def view_decoration_po(self):
        form_view_id = self.env.ref('pinnacle_sales.view_order_line_form_view')
        new_context = self.env.context.copy()
        new_context['invisible_po_line'] = True
        if self:
            return {'type': 'ir.actions.act_window',
                    'view_mode': 'form',
                    'name': "Decoration Lines",
                    'target': 'new',
                    'view_id': form_view_id.id,
                    'res_model': 'sale.order.line',
                    'res_id': self[0].id,
                    'context': new_context
                    }

    def get_group_sequence(self, group):
        group_sequence = [grp.group_sequence for grp in group]
        sequence = ", ".join(group_sequence)
        return sequence

class SaleOrder(models.Model):
    _inherit = 'sale.order'

    purchase_orders = fields.One2many('purchase.order', 'sale_order_id', string="Purchase Orders", copy=False)
    is_shipped_order = fields.Boolean(compute="_compute_is_shipped_order", string='Is Shipped Order ?', store=True)
    is_delivered_order = fields.Boolean(compute="_compute_is_delivered_order", string='Is Delivered Order ?', store=True)
    actual_margin = fields.Monetary(compute='_actual_product_margin', help="It gives profitability by calculating the difference between the Unit Price and the cost of the purchase order.", currency_field='currency_id', digits=dp.get_precision('Product Price'), store=True)
    actual_margin_in_percentage = fields.Float(compute='_compute_actual_margin_in_percentage', string="Blended Margin", help="It gives profitability in percentage by calculating the difference between the Unit Price and the cost of the purchase order.", digits=dp.get_precision('Product Price'), store=True, copy=False)
    actual_product_margin_in_percentage = fields.Float(compute='_compute_actual_product_margin_in_percentage', string="Margin Of Product", help="It gives profitability in percentage by calculating the difference between the Unit Price and the cost of the purchase order.", digits=dp.get_precision('Product Price'), store=True, copy=False)
    actual_shipping_margin_in_percentage = fields.Float(compute='_compute_actual_shipping_margin_in_percentage', string="Margin Of Shipping", help="It gives profitability in percentage by calculating the difference between the Unit Price and the cost of the purchase order.", digits=dp.get_precision('Product Price'), store=True, copy=False)
    actual_margin_duplicate = fields.Monetary(compute='_actual_product_margin', help="It gives profitability by calculating the difference between the Unit Price and the cost of the purchase order.", currency_field='currency_id', digits=dp.get_precision('Product Price'), store=True)
    actual_margin_in_percentage_duplicate = fields.Float(compute='_compute_actual_margin_in_percentage', string="Blended Margin", help="It gives profitability in percentage by calculating the difference between the Unit Price and the cost of the purchase order.", digits=dp.get_precision('Product Price'), store=True, copy=False)
    actual_product_margin_in_percentage_duplicate = fields.Float(compute='_compute_actual_product_margin_in_percentage', string="Margin Of Product", help="It gives profitability in percentage by calculating the difference between the Unit Price and the cost of the purchase order.", digits=dp.get_precision('Product Price'), store=True, copy=False)
    actual_shipping_margin_in_percentage_duplicate = fields.Float(compute='_compute_actual_shipping_margin_in_percentage', string="Margin Of Shipping", help="It gives profitability in percentage by calculating the difference between the Unit Price and the cost of the purchase order.", digits=dp.get_precision('Product Price'), store=True, copy=False)
    actual_product_margin_in_dollor = fields.Monetary(compute='_compute_actual_product_margin_in_percentage', string="Margin Of Product", help="It gives profitability in amount by calculating the difference between the Unit Price and the cost of the purchase order.", currency_field='currency_id', digits=dp.get_precision('Product Price'), store=True, copy=False)
    actual_product_margin_in_dollor_duplicate = fields.Monetary(compute='_compute_actual_product_margin_in_percentage', string="Margin Of Product", help="It gives profitability in amount by calculating the difference between the Unit Price and the cost of the purchase order.", currency_field='currency_id', digits=dp.get_precision('Product Price'), store=True, copy=False)
    actual_shipping_margin_in_dollor = fields.Monetary(compute='_compute_actual_shipping_margin_in_percentage', string="Margin Of Shipping", help="It gives profitability in amount by calculating the difference between the Unit Price and the cost of the purchase order.", currency_field='currency_id', digits=dp.get_precision('Product Price'), store=True, copy=False)
    actual_shipping_margin_in_dollor_duplicate = fields.Monetary(compute='_compute_actual_shipping_margin_in_percentage', string="Margin Of Shipping", help="It gives profitability in amount by calculating the difference between the Unit Price and the cost of the purchase order.", currency_field='currency_id', digits=dp.get_precision('Product Price'), store=True, copy=False)

    first_po_shipped = fields.Datetime('First PO Shipped', readonly=True)
    most_recent_po_shipped = fields.Datetime('Most Recent PO Shipped', readonly=True)
    diff_between_first_and_most_recent_po_shipped = fields.Char(compute="_compute_diff_between_first_and_most_recent_po_shipped", string='Difference Between PO Ship Datetimes', help="Difference Between First PO Shipped And Most Recent PO Shipped Datetimes In Days")
    # add fields all_po_delivered , po_total and bill_total for sales order to close report
    all_po_delivered = fields.Selection('All PO Delivered?', related='purchase_orders.state')
    po_total = fields.Float('Purchase Order Total', compute='_get_po_total')
    bill_total = fields.Float('Vendor Bill Total', compute='_get_po_total')
    purchase_order_exist = fields.Boolean(compute="_get_purchase_order_exist")
    field_warning =  fields.Html('Shipping Warning', compute='_compute_shipping_line')
    vendor_ids = fields.Many2many('res.partner', string='Vendor', compute="_get_po_total", store=True)
    supplier_finishers = fields.Many2many('res.partner', string='Vendor', compute="_get_po_total", store=True)

    def check_sale_order_shipping_info(self):
        """
        This method check shipping data in sale order and sort and store in 'ship_group_by'
        according to supplier to finisher OR finisher-1 to finisher-2, Which is used in 
        shipping line creation in purchase order.

        :returns: None
        """
        sale_obj = self.env['sale.order.line']
        def make_seq(so_line, check_supplier, decoration_list, get_from, sequence=1):
            if sequence == 1:
                if check_supplier in decoration_list:
                    for item in ship_group_by[so_line.id]:
                        if item['current_supplier'] == item['sent_to'] == check_supplier:
                            item['sequence'] = 1
                            item['get_from'] = get_from
                            if len(decoration_list) == 1:
                                decoration_list.remove(check_supplier)
                                item['sent_to'] = self.partner_id.id
                                return
                            make_seq(so_line, item['sent_to'], decoration_list, item['current_supplier'], sequence+1)
                            return

                else:
                    for item in ship_group_by[so_line.id]:
                        if item['current_supplier'] ==  check_supplier:
                            item['sequence'] = 1
                            item['get_from'] = get_from
                            if item['sent_to'] == False:
                                item['sent_to'] = self.partner_id.id
                                return
                            make_seq(so_line, item['sent_to'], decoration_list, item['current_supplier'], sequence+1)
                            return


            for item in ship_group_by[so_line.id]:
                if item['current_supplier'] ==  check_supplier and item['sequence'] == False:
                    item['sequence'] = sequence
                    item['get_from'] = get_from
                    decoration_list.remove(check_supplier)
                    make_seq(so_line, item['sent_to'], decoration_list, item['current_supplier'], sequence+1)

        ship_group_by = {}
        for ship_line in self.shipping_line:
            for so_line in ship_line.shipping_sale_orders:
                if not ship_group_by.has_key(so_line.id):
                    ship_group_by[so_line.id] = []
                ship_group_by[so_line.id].append({
                        'current_supplier': ship_line.partner_supplier.id,
                        'sent_to': ship_line.finisher.id,
                        'sequence': False,
                        'shipping_line': ship_line.id,
                        'get_from': False,
                    })
        for line_id in ship_group_by.keys():
            so_line = sale_obj.browse(line_id)
            decorators = list(set([x.finisher.id for x in so_line.decorations]))
            make_seq(so_line, so_line.changer_supplier_partner.id, decorators, so_line.changer_supplier_partner.id, 1)
            ship_group_by[line_id] = sorted(ship_group_by[line_id], key=lambda k: k['sequence'])
            if any(x['sequence'] == False for x in ship_group_by[line_id]):
                raise UserError(_('PO config is wrong'))


    @api.one
    @api.depends('shipping_line')
    def _compute_shipping_line(self):
        self.field_warning = ''
        try:
            self.check_sale_order_shipping_info()
        except:
            self.field_warning = "<div style='color:red'>Something wrong in Shipping line configuration !! It may effect on Purchase Orders Generations. PLEASE CHECK SUPPLIER AND FINISHER IN LINES.</div>"

    @api.one
    def _get_purchase_order_exist(self):
        self.purchase_order_exist = False
        if self.purchase_orders:
            self.purchase_order_exist = True
    
    @api.one
    def _get_po_total(self):
        if self.purchase_orders:
            po_total = 0.0
            bill_total = 0.0
            vendor_ids = []
            for po in self.purchase_orders:
                bill_total = po.invoice_total
                po_total += po.amount_total
                vendor_ids.append(po.partner_id.id)
            self.po_total = po_total
            self.bill_total = bill_total
            self.vendor_ids = [(6,0, vendor_ids)]
            self.supplier_finishers = [(6,0, vendor_ids)]
            self.write({'supplier_finishers': [(6,0, vendor_ids)]})

    @api.one
    @api.depends('first_po_shipped', 'most_recent_po_shipped')
    def _compute_diff_between_first_and_most_recent_po_shipped(self):
        self.diff_between_first_and_most_recent_po_shipped = ''
        if self.first_po_shipped and self.most_recent_po_shipped:
            first_po_shipped_obj = datetime.strptime(self.first_po_shipped, DEFAULT_SERVER_DATETIME_FORMAT)
            most_recent_po_shipped_obj = datetime.strptime(self.most_recent_po_shipped, DEFAULT_SERVER_DATETIME_FORMAT)
            difference = most_recent_po_shipped_obj - first_po_shipped_obj
            self.diff_between_first_and_most_recent_po_shipped = str(difference.days) + ' Days'

    @api.depends('order_line.actual_margin', 'currency_id', 'program_type')
    def _actual_product_margin(self):
        for order in self:
            order.actual_margin = sum(order.order_line.filtered(lambda r: r.state != 'cancel').filtered(lambda r: r.product_id.exclude_from_computing_sale_margins == False).filtered(lambda r: r.product_id.inventory_control_code not in ['inv_co', 'inv_cs'] or r.order_id.program_type != 'store').mapped('actual_margin'))
            order.actual_margin_duplicate = sum(order.order_line.filtered(lambda r: r.state != 'cancel').filtered(lambda r: r.product_id.exclude_from_computing_sale_margins == False).filtered(lambda r: r.product_id.inventory_control_code not in ['inv_co', 'inv_cs'] or r.order_id.program_type != 'store').mapped('actual_margin'))

    @api.depends('order_line.price_subtotal', 'actual_margin', 'currency_id', 'program_type')
    def _compute_actual_margin_in_percentage(self):
        for order in self:
            order.actual_margin_in_percentage = 0.00
            order.actual_margin_in_percentage_duplicate = 0.00
            amount_untaxed = 0.00
            for order_line in order.order_line:
                if order_line.product_id and not order_line.product_id.exclude_from_computing_sale_margins and (order_line.product_id.inventory_control_code not in ['inv_co', 'inv_cs'] or order_line.order_id.program_type != 'store'):
                    amount_untaxed += order_line.price_subtotal
            if amount_untaxed:
                order.actual_margin_in_percentage = order.actual_margin * 100 / amount_untaxed
                order.actual_margin_in_percentage_duplicate = order.actual_margin * 100 / amount_untaxed

    @api.depends('order_line.product_id', 'order_line.purchase_price', 'order_line.product_uom_qty', 'order_line.price_subtotal', 'order_line.purchase_order_lines', 'order_line.purchase_order_lines.price_unit', 'currency_id', 'program_type')
    def _compute_actual_product_margin_in_percentage(self):
        for order in self:
            total_saleprice = 0.00
            total_cost = 0.00
            delivery_carrier_products = []
            for delivery_carrier in self.env['delivery.carrier'].search([]):
                if delivery_carrier.product_id:
                    delivery_carrier_products.append(delivery_carrier.product_id.id)
            delivery_carrier_products = list(set(delivery_carrier_products))
            for order_line in order.order_line:
                if order_line.product_id.id not in delivery_carrier_products and not order_line.product_id.exclude_from_computing_sale_margins and (order_line.product_id.inventory_control_code not in ['inv_co', 'inv_cs'] or order_line.order_id.program_type != 'store'):
                    total_saleprice += order_line.price_subtotal
                    if order_line.purchase_order_lines:
                        total_cost += sum([order_line.order_id.currency_id.compute(purchase_order_line.price_unit, order_line.currency_id, round=False) for purchase_order_line in order_line.purchase_order_lines]) / len(order_line.purchase_order_lines) * order_line.product_uom_qty
            order.actual_product_margin_in_percentage = 0.00
            order.actual_product_margin_in_percentage_duplicate = 0.00
            order.actual_product_margin_in_dollor = 0.00
            order.actual_product_margin_in_dollor_duplicate = 0.00
            if total_saleprice:
                total = (total_saleprice - total_cost)
                order.actual_product_margin_in_dollor = total
                order.actual_product_margin_in_dollor_duplicate = total
                order.actual_product_margin_in_percentage =  total / total_saleprice * 100
                order.actual_product_margin_in_percentage_duplicate = total / total_saleprice * 100

    @api.depends('order_line.product_id', 'order_line.purchase_price', 'order_line.product_uom_qty', 'order_line.price_subtotal', 'order_line.purchase_order_lines', 'order_line.purchase_order_lines.price_unit', 'currency_id', 'program_type')
    def _compute_actual_shipping_margin_in_percentage(self):
        for order in self:
            total_saleprice = 0.00
            total_cost = 0.00
            delivery_carrier_products = []
            for delivery_carrier in self.env['delivery.carrier'].search([]):
                if delivery_carrier.product_id:
                    delivery_carrier_products.append(delivery_carrier.product_id.id)
            delivery_carrier_products = list(set(delivery_carrier_products))
            for order_line in order.order_line:
                if order_line.product_id.id in delivery_carrier_products and not order_line.product_id.exclude_from_computing_sale_margins and (order_line.product_id.inventory_control_code not in ['inv_co', 'inv_cs'] or order_line.order_id.program_type != 'store'):
                    total_saleprice += order_line.price_subtotal
                    if order_line.purchase_order_lines:
                        total_cost += sum([order_line.order_id.currency_id.compute(purchase_order_line.price_unit, order_line.currency_id, round=False) for purchase_order_line in order_line.purchase_order_lines]) / len(order_line.purchase_order_lines) * order_line.product_uom_qty
            order.actual_shipping_margin_in_percentage = 0.00
            order.actual_shipping_margin_in_percentage_duplicate = 0.00
            order.actual_shipping_margin_in_dollor = 0.00
            order.actual_shipping_margin_in_dollor_duplicate = 0.00
            if total_saleprice:
                total = (total_saleprice - total_cost)
                order.actual_shipping_margin_in_dollor = total
                order.actual_shipping_margin_in_dollor_duplicate = total
                order.actual_shipping_margin_in_percentage = total / total_saleprice * 100
                order.actual_shipping_margin_in_percentage_duplicate = total / total_saleprice * 100

    @api.one
    @api.depends('purchase_orders', 'purchase_orders.state', 'state')
    def _compute_is_shipped_order(self):
        if self.purchase_orders:
            self.is_shipped_order = True
            for purchase_order in self.purchase_orders:
                if (purchase_order.state == 'hold' and purchase_order.last_state not in ['close', 'deliver', 'reopen', 'done', 'shipped', 'cancel']):
                    self.is_shipped_order = False
                    break
                if purchase_order.state not in ['close', 'deliver', 'reopen', 'done', 'shipped', 'cancel']:
                    self.is_shipped_order = False
                    break
            if self.state not in ['sale', 'partially_invoiced']:
                self.is_shipped_order = False
        else:
            self.is_shipped_order = False

    @api.one
    @api.depends('purchase_orders', 'purchase_orders.state', 'state')
    def _compute_is_delivered_order(self):
        if self.purchase_orders:
            self.is_delivered_order = True
            for purchase_order in self.purchase_orders:
                if (purchase_order.state == 'hold' and purchase_order.last_state not in ['close', 'deliver', 'reopen', 'done', 'cancel']):
                    self.is_delivered_order = False
                    break
                if purchase_order.state not in ['close', 'deliver', 'reopen', 'done', 'cancel']:
                    self.is_delivered_order = False
                    break
            # (13018) Dev: Close button on SOs
            # if self.state == 'partially_invoiced':
            #     self.is_delivered_order = True
            #     for purchase_order in self.purchase_orders:
            #         if (purchase_order.state == 'hold' and purchase_order.last_state not in ['draft', 'sent', 'to approve', 'close', 'deliver', 'reopen', 'done']):
            #             self.is_delivered_order = False
            #             break
            #         if purchase_order.state not in ['draft', 'sent', 'to approve', 'close', 'deliver', 'reopen', 'done']:
            #             self.is_delivered_order = False
            #             break
            # if self.state == 'sale':
            #     self.is_delivered_order = True
            #     for purchase_order in self.purchase_orders:
            #         if (purchase_order.state == 'hold' and purchase_order.last_state not in ['draft', 'sent', 'to approve', 'close', 'deliver', 'reopen', 'done']):
            #             self.is_delivered_order = False
            #             break
            #         if purchase_order.state not in ['draft', 'sent', 'to approve', 'close', 'deliver', 'reopen', 'done']:
            #             self.is_delivered_order = False
            #             break
        else:
            self.is_delivered_order = False

    @api.multi
    def set_to_action_confirm(self):
        self.ensure_one()
        if not self.is_shipped_order:
            raise UserError(_('You can not move the sale order that has any unshipped PO OR that does not has at least one PO.'))
        self.state = 'shipped'

    @api.multi
    def set_to_delivered(self):
        self.ensure_one()
        if not self.is_delivered_order:
            raise UserError(_('Please close all Purchase Orders before closing a Sales Order.'))
        self.state = 'delivered'

    @api.multi
    def write(self, vals):
        self = self.exists()
        result = super(SaleOrder, self).write(vals)
        for order in self:
            if vals.get('analytic_tag_ids', False):
                for purchase_order in order.purchase_orders:
                    purchase_order.order_line.write({'analytic_tag_ids': [(6, 0, order.analytic_tag_ids.ids)]})
        return result

class SaleOrderlineDecorationGroupChargeLine(models.Model):
    _inherit = 'sale.order.line.decoration.group.charge.line'

    purchase_order_lines = fields.One2many(
        'purchase.order.line', 'decoration_charge_group_id', string="Purchase Order Lines", copy=False)
    charge_id_sale_ref = fields.Many2one('sale.order.line.decoration.group.charge.line', string="Sale Order Reference")

    @api.multi
    def unlink(self):
        purchase_order_lines = []
        for record in self:
            if record.purchase_order_lines:
                for purchase_order_line in record.purchase_order_lines:
                    purchase_order_lines.append(purchase_order_line.id)
        result = super(SaleOrderlineDecorationGroupChargeLine, self).unlink()
        if result:
            if purchase_order_lines:
                purchase_order_lines = list(set(purchase_order_lines))
                self.env['purchase.order.line'].browse(purchase_order_lines).unlink()
        return result

    @api.multi
    def pull_in_decoration_charge_purchase(self):
        self.ensure_one()
        result = self.with_context(
            pull_in=True).recalculate_decoration_charge_purchase()
        return result

    @api.multi
    def recalculate_decoration_charge_purchase(self):
        self.ensure_one()
        from_pull_in_button = False
        if 'pull_in' in self.env.context:
            from_pull_in_button = self.env.context['pull_in']
        product_run_charge = self.env.ref(
            'pinnacle_sales.product_product_run_charge')
        if not product_run_charge:
            raise UserError(_('Run Charge Product Not Found'))
        if self.decoration_group_id and self.product_id.id == product_run_charge.id and self.at_least_one_decoration_recalculate:
            finisher_decorations = self.at_least_one_decoration_recalculate.purchase_order_line_id.product_id.finisher_ids.search(
                ['&', ('product_id', '=', self.at_least_one_decoration_recalculate.purchase_order_line_id.product_id.product_tmpl_id.id), ('finisher_id', '=', self.at_least_one_decoration_recalculate.finisher.id)])
            if finisher_decorations or (self.at_least_one_decoration_recalculate.finisher and self.at_least_one_decoration_recalculate.finisher.finisher_ids):
                finisher_finishing_types = False
                if finisher_decorations:
                    finisher_decoration = finisher_decorations[0]
                    finisher_finishing_types = finisher_decoration.product_imprint_ids.search(['&', ('product_finisher_id', '=', finisher_decoration.id), '&', (
                        'decoration_location', '=', self.at_least_one_decoration_recalculate.imprint_location.id), ('decoration_method', '=', self.at_least_one_decoration_recalculate.imprint_method.id)])
                pull_charges_from_vendor = False
                if not finisher_finishing_types:
                    # Pull Charges From Vendor If They Are Not Available At
                    # Product
                    if self.at_least_one_decoration_recalculate.finisher:
                        pull_charges_from_vendor = True
                        finisher_finishing_types = self.at_least_one_decoration_recalculate.finisher.finisher_ids.search(['&', ('partner_id', '=', self.at_least_one_decoration_recalculate.finisher.id), '&', (
                            'decoration_location', '=', self.at_least_one_decoration_recalculate.imprint_location.id), ('decoration_method', '=', self.at_least_one_decoration_recalculate.imprint_method.id)])
                if not finisher_finishing_types:
                    # Pull Charges From Vendor By Only Cosidering Decoration Method
                    if self.at_least_one_decoration_recalculate.finisher and self.at_least_one_decoration_recalculate.purchase_order_line_id.product_id and self.at_least_one_decoration_recalculate.purchase_order_line_id.product_id.seller_ids:
                        vendor_ids = [seller_id.name and seller_id.name.id for seller_id in self.at_least_one_decoration_recalculate.purchase_order_line_id.product_id.seller_ids]
                        if self.at_least_one_decoration_recalculate.finisher.id in vendor_ids:
                            pull_charges_from_vendor = True
                            finisher_finishing_types = self.at_least_one_decoration_recalculate.finisher.finisher_ids.search(['&',('partner_id','=',self.at_least_one_decoration_recalculate.finisher.id),('decoration_method','=',self.at_least_one_decoration_recalculate.imprint_method.id)])
                if finisher_finishing_types:
                    finisher_finishing_type = finisher_finishing_types[0]
                    if from_pull_in_button:
                        quantity_multiplier = (len(self.at_least_one_decoration_recalculate.stock_color.ids) + len(
                            self.at_least_one_decoration_recalculate.pms_code.ids))
                    else:
                        if pull_charges_from_vendor:
                            quantity_multiplier = (len(self.at_least_one_decoration_recalculate.stock_color.ids) + len(
                                self.at_least_one_decoration_recalculate.pms_code.ids))
                        else:
                            quantity_multiplier = (len(self.at_least_one_decoration_recalculate.stock_color.ids) + len(
                                self.at_least_one_decoration_recalculate.pms_code.ids) - finisher_finishing_type.colors_included)
                    if finisher_finishing_type.run_charges == 'quantity' and finisher_finishing_type.tier_catalog_qty_ids:
                        tier_catalog_qty_index = False
                        tier_catalog_qty = False
                        for tier_catalog_qty_id in finisher_finishing_type.tier_catalog_qty_ids.sorted(
                                key=lambda r: r.quantity):
                            if tier_catalog_qty_id.quantity <= self.product_uom_qty:
                                tier_catalog_qty_index = list(finisher_finishing_type.tier_catalog_qty_ids.sorted(
                                    key=lambda r: r.quantity)).index(tier_catalog_qty_id)
                        if tier_catalog_qty_index:
                            tier_catalog_qty = \
                                finisher_finishing_type.tier_catalog_qty_ids.sorted(key=lambda r: r.quantity)[
                                    tier_catalog_qty_index]
                        if tier_catalog_qty:
                            self.price_unit = tier_catalog_qty.sale_price if quantity_multiplier > 0 else 0.00
                            self.purchase_price = tier_catalog_qty.cost if quantity_multiplier > 0 else 0.00
                        else:
                            tier_catalog_qty = \
                                finisher_finishing_type.tier_catalog_qty_ids.sorted(
                                    key=lambda r: r.quantity)[0]
                            self.price_unit = (tier_catalog_qty and (
                                tier_catalog_qty.sale_price) or 0.00) if quantity_multiplier > 0 else 0.00
                            self.purchase_price = (
                                tier_catalog_qty and (tier_catalog_qty.cost) or 0.00) if quantity_multiplier > 0 else 0.00
                    elif finisher_finishing_type.run_charges == 'color' and finisher_finishing_type.tier_catalog_color_ids:
                        tier_catalog_color_index = False
                        tier_catalog_color = False
                        if from_pull_in_button:
                            for tier_catalog_color_id in finisher_finishing_type.tier_catalog_color_ids.sorted(
                                    key=lambda r: r.quantity):
                                if tier_catalog_color_id.quantity <= self.product_uom_qty and int(
                                        tier_catalog_color_id.color) == (
                                    len(self.at_least_one_decoration_recalculate.stock_color.ids) + len(
                                        self.at_least_one_decoration_recalculate.pms_code.ids)):
                                    tier_catalog_color_index = list(
                                        finisher_finishing_type.tier_catalog_color_ids.sorted(
                                            key=lambda r: r.quantity)).index(tier_catalog_color_id)
                            if tier_catalog_color_index:
                                tier_catalog_color = \
                                    finisher_finishing_type.tier_catalog_color_ids.sorted(key=lambda r: r.quantity)[
                                        tier_catalog_color_index]
                            if tier_catalog_color:
                                self.price_unit = tier_catalog_color.sale_price
                                self.purchase_price = tier_catalog_color.cost
                            else:
                                for tier_catalog_color_id in finisher_finishing_type.tier_catalog_color_ids.sorted(
                                        key=lambda r: r.quantity):
                                    if int(tier_catalog_color_id.color) == (
                                        len(self.at_least_one_decoration_recalculate.stock_color.ids) + len(
                                            self.at_least_one_decoration_recalculate.pms_code.ids)):
                                        tier_catalog_color = tier_catalog_color_id
                                        break
                                self.price_unit = tier_catalog_color and tier_catalog_color.sale_price or 0.0
                                self.purchase_price = tier_catalog_color and tier_catalog_color.cost or 0.0
                        else:
                            for tier_catalog_color_id in finisher_finishing_type.tier_catalog_color_ids.sorted(key=lambda r: r.quantity):
                                if pull_charges_from_vendor:
                                    if tier_catalog_color_id.quantity <= self.product_uom_qty and int(tier_catalog_color_id.color) == (len(self.at_least_one_decoration_recalculate.stock_color.ids) + len(self.at_least_one_decoration_recalculate.pms_code.ids)):
                                        tier_catalog_color_index = list(finisher_finishing_type.tier_catalog_color_ids.sorted(
                                            key=lambda r: r.quantity)).index(tier_catalog_color_id)
                                else:
                                    if tier_catalog_color_id.quantity <= self.product_uom_qty and int(tier_catalog_color_id.color) == (len(self.at_least_one_decoration_recalculate.stock_color.ids) + len(self.at_least_one_decoration_recalculate.pms_code.ids) - finisher_finishing_type.colors_included):
                                        tier_catalog_color_index = list(finisher_finishing_type.tier_catalog_color_ids.sorted(
                                            key=lambda r: r.quantity)).index(tier_catalog_color_id)
                            if tier_catalog_color_index:
                                tier_catalog_color = \
                                    finisher_finishing_type.tier_catalog_color_ids.sorted(key=lambda r: r.quantity)[
                                        tier_catalog_color_index]
                            if tier_catalog_color:
                                self.price_unit = tier_catalog_color.sale_price
                                self.purchase_price = tier_catalog_color.cost
                            else:
                                for tier_catalog_color_id in finisher_finishing_type.tier_catalog_color_ids.sorted(key=lambda r: r.quantity):
                                    if pull_charges_from_vendor:
                                        if int(tier_catalog_color_id.color) == (len(self.at_least_one_decoration_recalculate.stock_color.ids) + len(self.at_least_one_decoration_recalculate.pms_code.ids)):
                                            tier_catalog_color = tier_catalog_color_id
                                            break
                                    else:
                                        if int(tier_catalog_color_id.color) == (len(self.at_least_one_decoration_recalculate.stock_color.ids) + len(self.at_least_one_decoration_recalculate.pms_code.ids) - finisher_finishing_type.colors_included):
                                            tier_catalog_color = tier_catalog_color_id
                                            break
                                self.price_unit = tier_catalog_color and tier_catalog_color.sale_price or 0.0
                                self.purchase_price = tier_catalog_color and tier_catalog_color.cost or 0.0
                    elif finisher_finishing_type.run_charges == 'teir' and finisher_finishing_type.tier_catalog_name_ids:
                        product_vendor_tier_seq = False
                        if finisher_decoration.finisher_id:
                            for seller_id in self.at_least_one_decoration_recalculate.purchase_order_line_id.product_id.product_tmpl_id.seller_ids:
                                if seller_id.name.id == finisher_decoration.finisher_id.id:
                                    if seller_id.product_vendor_tier_ids:
                                        for product_vendor_tier_id in seller_id.product_vendor_tier_ids.sorted(
                                                key=lambda r: r.min_qty):
                                            if self.run_charge_multiplied > 0:
                                                if product_vendor_tier_id.min_qty <= (
                                                        self.product_uom_qty / self.run_charge_multiplied):
                                                    product_vendor_tier_seq = product_vendor_tier_id.tier_sequence
                                        if not product_vendor_tier_seq:
                                            product_vendor_tier_seq = \
                                                seller_id.product_vendor_tier_ids.sorted(key=lambda r: r.min_qty)[
                                                    0].tier_sequence
                        if product_vendor_tier_seq:
                            for tier_catalog_name_id in finisher_finishing_type.tier_catalog_name_ids:
                                if product_vendor_tier_seq == tier_catalog_name_id.tier_sequence:
                                    self.price_unit = tier_catalog_name_id.sale_price if quantity_multiplier > 0 else 0.00
                                    self.purchase_price = tier_catalog_name_id.cost if quantity_multiplier > 0 else 0.00
                        else:
                            self.price_unit = 0.0
                            self.purchase_price = 0.0
                    else:
                        self.price_unit = 0.0
                        self.purchase_price = 0.0
        form_view_id = self.env.ref(
            'pinnacle_purchase.sale_order_line_decoration_group_form_view_purchase')
        if self and self[0].decoration_group_id and form_view_id:
            return {'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'name': "Decoration Group",
                    'target': 'new',
                    'view_id': form_view_id.id,
                    'res_model': 'sale.order.line.decoration.group',
                    'res_id': self[0].decoration_group_id.id,
                    }

class SaleOrderlineDecorationGroup(models.Model):
    _inherit = 'sale.order.line.decoration.group'

    purchase_order_id = fields.Many2one("purchase.order", string='Purchase Order', store=True, copy=False)
    purchase_order_id_state = fields.Selection(related="purchase_order_id.state")
    group_id_sale_ref =  fields.Many2one('sale.order.line.decoration.group', string="Sale Order Reference")
    purchase_decorations = fields.One2many('sale.order.line.decoration', 'decoration_group_id', string='Decorations', readonly=True, copy=False)

    @api.multi
    def unlink(self):
        purchase_order_lines = []
        for record in self:
            for charge_line in record.charge_lines.exists():
                if charge_line.purchase_order_lines:
                    for purchase_order_line in charge_line.purchase_order_lines:
                        purchase_order_lines.append(purchase_order_line.id)
        result = super(SaleOrderlineDecorationGroup, self).unlink()
        if result:
            if purchase_order_lines:
                purchase_order_lines = list(set(purchase_order_lines))
                if self.env['purchase.order.line'].browse(purchase_order_lines).exists():
                    self.env['purchase.order.line'].browse(purchase_order_lines).exists().unlink()
        return result

    @api.multi
    def edit_decoration_group_purchase(self):
        form_view_id = self.env.ref(
            'pinnacle_purchase.sale_order_line_decoration_group_form_view_purchase')
        if self and form_view_id:
            return {'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'name': "Decoration Group",
                    'target': 'new',
                    'view_id': form_view_id.id,
                    'res_model': 'sale.order.line.decoration.group',
                    'res_id': self[0].id,
                    }

    @api.multi
    def view_decoration_group_purchase(self):
        form_view_id = self.env.ref(
            'pinnacle_purchase.sale_order_line_decoration_group_form_view_readonly_purchase')
        if self and form_view_id:
            return {'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'name': "Decoration Group",
                    'target': 'new',
                    'view_id': form_view_id.id,
                    'res_model': 'sale.order.line.decoration.group',
                    'res_id': self[0].id,
                    }

    @api.multi
    def purchase_write_with_add_charges_order_lines(self, vals):
        result = self.write(vals)
        if result:
            charge_line_ids = []
            for charge_line in self.charge_lines:
                if charge_line.select:
                    charge_line_ids.append(charge_line.id)
                    charge_line.select = False
            for charge_line in self.env['sale.order.line.decoration.group.charge.line'].browse(charge_line_ids):
                if not charge_line.purchase_order_lines:
                    self.env['purchase.order.line'].create({
                        'product_id': charge_line.product_id.id,
                        'name': charge_line.name,
                        'product_qty': charge_line.product_uom_qty,
                        'product_uom': charge_line.product_uom.id,
                        'price_unit': charge_line.purchase_price,
                        'order_id': self.purchase_order_id.id,
                        'decoration_charge_group_id': charge_line.id,
                    })
                else:
                    charge_line.purchase_order_lines[0].write({
                        'product_id': charge_line.product_id.id,
                        'name': charge_line.name,
                        'product_qty': charge_line.product_uom_qty,
                        'product_uom': charge_line.product_uom.id,
                        'price_unit': charge_line.purchase_price,
                        'order_id': self.purchase_order_id.id,
                        'decoration_charge_group_id': charge_line.id,
                    })
                charge_line.added_to_order_lines = 'YES'
            if charge_line_ids:
                return {
                    'type': 'ir.actions.client',
                    'tag': 'reload',
                }
        return result

    @api.multi
    def recalculatea_reorder_setup_charges_purchase(self):
        product_setup_sale_price = self.env.ref(
            'pinnacle_sales.product_product_setup_sale_price')
        if not product_setup_sale_price:
            raise UserError(_('Setup Sale Price Product Not Found'))
        for charge_line in self.charge_lines:
            if charge_line.product_id.id == product_setup_sale_price.id and charge_line.at_least_one_decoration_recalculate:
                charge_line.price_unit = 0.00
                charge_line.purchase_price = 0.00
                finisher_decorations = charge_line.at_least_one_decoration_recalculate.purchase_order_line_id.product_id.finisher_ids.search(
                    ['&', ('product_id', '=',
                           charge_line.at_least_one_decoration_recalculate.purchase_order_line_id.product_id.product_tmpl_id.id),
                     ('finisher_id', '=', charge_line.at_least_one_decoration_recalculate.finisher.id)])
                ltm_policy_applied = False
                ltm_policy_price_unit = 0.00
                ltm_policy_purchase_price = 0.00
                # LTM Policy Calculation For Set-Up Price Start
                if charge_line.at_least_one_decoration_recalculate.purchase_order_line_id.change_supplier.product_vendor_tier_ids and charge_line.at_least_one_decoration_recalculate.purchase_order_line_id.change_supplier and (
                    (
                        charge_line.at_least_one_decoration_recalculate.purchase_order_line_id.change_supplier.moq and charge_line.at_least_one_decoration_recalculate.purchase_order_line_id.change_supplier.moq != 'not_allowed') or (
                        charge_line.at_least_one_decoration_recalculate.purchase_order_line_id.change_supplier.o_moq and charge_line.at_least_one_decoration_recalculate.purchase_order_line_id.change_supplier.o_moq != 'not_allowed')):
                    if charge_line.at_least_one_decoration_recalculate.purchase_order_line_id.change_supplier.o_ltm_policy_id:
                        if charge_line.at_least_one_decoration_recalculate.purchase_order_line_id.change_supplier.o_moq in [
                                'half_column', 'other']:
                            if (
                                    charge_line.at_least_one_decoration_recalculate.purchase_order_line_id.change_supplier.product_vendor_tier_ids.sorted(
                                        key=lambda r: r.min_qty)[
                                    0].min_qty) > charge_line.at_least_one_decoration_recalculate.purchase_order_line_id.product_qty:
                                if charge_line.at_least_one_decoration_recalculate.purchase_order_line_id.change_supplier.o_setup_cost == 'other':
                                    ltm_policy_purchase_price = charge_line.at_least_one_decoration_recalculate.purchase_order_line_id.change_supplier.o_other_setup_cost
                                    ltm_policy_applied = True
                                if charge_line.at_least_one_decoration_recalculate.purchase_order_line_id.change_supplier.o_setup_price == 'other':
                                    ltm_policy_price_unit = charge_line.at_least_one_decoration_recalculate.purchase_order_line_id.change_supplier.o_other_setup_price
                                    ltm_policy_applied = True
                    else:
                        if charge_line.at_least_one_decoration_recalculate.purchase_order_line_id.change_supplier.moq in [
                                'half_column', 'other']:
                            if (
                                    charge_line.at_least_one_decoration_recalculate.purchase_order_line_id.change_supplier.product_vendor_tier_ids.sorted(
                                        key=lambda r: r.min_qty)[
                                    0].min_qty) > charge_line.at_least_one_decoration_recalculate.purchase_order_line_id.product_qty:
                                if charge_line.at_least_one_decoration_recalculate.purchase_order_line_id.change_supplier.setup_cost == 'other':
                                    ltm_policy_purchase_price = charge_line.at_least_one_decoration_recalculate.purchase_order_line_id.change_supplier.other_setup_cost
                                    ltm_policy_applied = True
                                if charge_line.at_least_one_decoration_recalculate.purchase_order_line_id.change_supplier.setup_price == 'other':
                                    ltm_policy_price_unit = charge_line.at_least_one_decoration_recalculate.purchase_order_line_id.change_supplier.other_setup_price
                                    ltm_policy_applied = True
                # LTM Policy Calculation For Set-Up Price End
                if finisher_decorations or (charge_line.at_least_one_decoration_recalculate.finisher and charge_line.at_least_one_decoration_recalculate.finisher.finisher_ids):
                    finisher_finishing_types = False
                    if finisher_decorations:
                        finisher_decoration = finisher_decorations[0]
                        finisher_finishing_types = finisher_decoration.product_imprint_ids.search(['&', ('product_finisher_id', '=', finisher_decoration.id), '&', (
                            'decoration_location', '=', charge_line.at_least_one_decoration_recalculate.imprint_location.id), ('decoration_method', '=', charge_line.at_least_one_decoration_recalculate.imprint_method.id)])
                    if not finisher_finishing_types:
                        # Pull Charges From Vendor If They Are Not Available At
                        # Product
                        if charge_line.at_least_one_decoration_recalculate.finisher:
                            finisher_finishing_types = charge_line.at_least_one_decoration_recalculate.finisher.finisher_ids.search(['&', ('partner_id', '=', charge_line.at_least_one_decoration_recalculate.finisher.id), '&', (
                                'decoration_location', '=', charge_line.at_least_one_decoration_recalculate.imprint_location.id), ('decoration_method', '=', charge_line.at_least_one_decoration_recalculate.imprint_method.id)])
                    if not finisher_finishing_types:
                        # Pull Charges From Vendor By Only Cosidering Decoration Method
                        if charge_line.at_least_one_decoration_recalculate.finisher and charge_line.at_least_one_decoration_recalculate.purchase_order_line_id.product_id and charge_line.at_least_one_decoration_recalculate.purchase_order_line_id.product_id.seller_ids:
                            vendor_ids = [seller_id.name and seller_id.name.id for seller_id in charge_line.at_least_one_decoration_recalculate.purchase_order_line_id.product_id.seller_ids]
                            if charge_line.at_least_one_decoration_recalculate.finisher.id in vendor_ids:
                                finisher_finishing_types = charge_line.at_least_one_decoration_recalculate.finisher.finisher_ids.search(['&',('partner_id','=',charge_line.at_least_one_decoration_recalculate.finisher.id),('decoration_method','=',charge_line.at_least_one_decoration_recalculate.imprint_method.id)])
                    if finisher_finishing_types:
                        finisher_finishing_type = finisher_finishing_types[0]
                        if not ltm_policy_applied:
                            product_setup_sale_price.list_price = finisher_finishing_type.reorder_set_up_sale_price
                            charge_line.price_unit = finisher_finishing_type.reorder_set_up_sale_price
                            charge_line.purchase_price = finisher_finishing_type.reorder_set_up_fee_cost
                            charge_line.name = string.replace(
                                charge_line.name, 'Setup', 'Reorder Setup')
                            charge_line.name_second = string.replace(
                                charge_line.name_second, 'Setup', 'Reorder Setup')
                        else:
                            product_setup_sale_price.list_price = ltm_policy_price_unit
                            charge_line.price_unit = ltm_policy_price_unit
                            charge_line.purchase_price = ltm_policy_purchase_price
                            charge_line.name = string.replace(
                                charge_line.name, 'Setup', 'Reorder Setup')
                            charge_line.name_second = string.replace(
                                charge_line.name_second, 'Setup', 'Reorder Setup')
        form_view_id = self.env.ref(
            'pinnacle_purchase.sale_order_line_decoration_group_form_view_purchase')
        if self and form_view_id:
            return {'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'name': "Decoration Group",
                    'target': 'new',
                    'view_id': form_view_id.id,
                    'res_model': 'sale.order.line.decoration.group',
                    'res_id': self[0].id,
                    }


class SaleOrderLineDecoration(models.Model):
    _inherit = 'sale.order.line.decoration'

    select_art_line = fields.Boolean(string="Select Art Line", default=False, copy=False)
    purchase_order_line_id = fields.Many2one("purchase.order.line", string='Purchase Order Line', ondelete="cascade", copy=False)
    purchase_order_id = fields.Many2one("purchase.order", string='Purchase Order', store=True, copy=False)
    purchase_order_line_product_uom_qty = fields.Float(
        related='purchase_order_line_id.product_qty', readonly=True)
    purchase_order_line_product_sku = fields.Char(related='purchase_order_line_id.product_sku', readonly=True)
    purchase_order_line_product_variant_name = fields.Char(
        related='purchase_order_line_id.product_variant_name', readonly=True)
    name_ref = fields.Text(
        related='purchase_order_line_id.name', readonly=True)
    decoration_id_sale_ref = fields.Many2one('sale.order.line.decoration', string="Sale Order Reference")
    spec_file = fields.Binary('Art Page 1 File(s)', copy=True, attachment=True)
    spec_file_att = fields.Many2one(
        'ir.attachment', 'Art Page 1 File(s)', copy=True)  # 12212
    spec_file_url = fields.Char(
        related='spec_file_att.url', string='Art Page 1 File(s)', copy=True)  # 12212
    spec_file_name = fields.Char('Art Page 1 File(s) Name', copy=True)
    vendor_file = fields.Binary('Vendor Files', copy=True, attachment=True)
    vendor_file_att = fields.Many2one(
        'ir.attachment', 'Vendor Files', copy=True)  # 12212
    vendor_file_url = fields.Char(
        related='vendor_file_att.url', string='Vendor Files', copy=True)  # 12212
    vendor_file_name = fields.Char('Vendor File Name', copy=True)
    spec_file2 = fields.Binary(
        'Art Page 2 File(s)', copy=True, attachment=True)
    spec_file2_att = fields.Many2one(
        'ir.attachment', 'Art Page 2 File(s)', copy=True)  # 12212
    spec_file2_url = fields.Char(
        related='spec_file2_att.url', string='Art Page 2 File(s)', copy=True)  # 12212
    spec_file2_name = fields.Char('Art Page 2 File(s) Name', copy=True)

    decoration_group_sequences = fields.Char(compute="_compute_group_seq")

    @api.multi
    def _compute_group_seq(self):
        sequence = ''
        for this in self:
            group_sequence = [grp.group_sequence for grp in this.purchase_order_line_id.decoration_group_sequences]
            sequence += ", ".join(group_sequence)
            this.decoration_group_sequences = sequence
        return

    @api.multi
    def get_art_file_name(self):
        for so_line in  self.purchase_order_id.art_lines:
            if self.id in  so_line.decorations.ids:
                return so_line.vendor_file_name
        return ' '

    @api.multi
    def get_pms_name(self):
        return  '%s, %s' % (", ".join([x.name_get()[0][1] for x in self.pms_code]), self.pms_color_code or ", ".join([x.name_get()[0][1] for x in self.stock_color]))

    @api.model
    def create(self, vals):
        result = super(SaleOrderLineDecoration, self).create(vals)
        if result.purchase_order_line_id and result.purchase_order_line_id.order_id and not result.decoration_sequence:
            if len(result.purchase_order_line_id.order_id.decorations.search(
                    [('purchase_order_id', '=', result.purchase_order_line_id.order_id.id),
                     ('decoration_sequence_id', '!=', 0)])) == 0:
                result.purchase_order_line_id.order_id.decoration_sequence_id = 1
                result.decoration_sequence = "D" + \
                    str(result.purchase_order_line_id.order_id.decoration_sequence_id)
                result.decoration_sequence_id = result.purchase_order_line_id.order_id.decoration_sequence_id
                result.purchase_order_line_id.order_id.decoration_sequence_id += 1
            elif len(result.purchase_order_line_id.order_id.decorations.search(
                    [('purchase_order_id', '=', result.purchase_order_line_id.order_id.id), ('decoration_sequence_id', '!=',
                                                                                     0)])) == result.purchase_order_line_id.order_id.decoration_sequence_id - 1:
                result.decoration_sequence = "D" + \
                    str(result.purchase_order_line_id.order_id.decoration_sequence_id)
                result.decoration_sequence_id = result.purchase_order_line_id.order_id.decoration_sequence_id
                result.purchase_order_line_id.order_id.decoration_sequence_id += 1
            else:
                index = 1
                already_exited_ids = [decoration.decoration_sequence_id for decoration in
                                      result.purchase_order_line_id.order_id.decorations.search(
                                          [('purchase_order_id', '=', result.purchase_order_line_id.order_id.id),
                                           ('decoration_sequence_id', '!=', 0)])]
                while index <= result.purchase_order_line_id.order_id.decoration_sequence_id - 1:
                    if index not in already_exited_ids:
                        result.decoration_sequence = "D" + str(index)
                        result.decoration_sequence_id = index
                        break
                    index += 1
        return result
