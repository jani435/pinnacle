from openerp import models, fields, api, _
import openerp.addons.decimal_precision as dp
from odoo.tools.float_utils import float_compare
from openerp.exceptions import UserError
from email.utils import formataddr
import base64
import urlparse
from openerp.exceptions import UserError
import urllib2, cStringIO
from PIL import Image
import logging
import datetime
from lxml import etree
_logger = logging.getLogger(__name__)
import string

po_dict = {'draft': 'Draft',
           'ipo_to_approve': 'IPO Approval',
           'ipo_approved': 'IPO Approved',
           'ipo_rejected': 'IPO Rejected',
           'sent': 'Submitted',
           'to approve': 'To Approve',
           'purchase': 'Confirmed',
           'shipped': 'Shipped',
           'deliver': 'Delivered',
           'close': 'Closed',
           'reopen': 'Re-Opened',
           'done': 'Re-Closed',
           'cancel': 'Cancelled',
           'hold': 'On Hold'}

class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    READONLY_STATES = {
        'ipo_to_approve': [('readonly', True)],
        'to approve': [('readonly', True)],
        'deliver': [('readonly', True)],
        'close': [('readonly', True)],
        'done': [('readonly', True)],
        'cancel': [('readonly', True)],
    }

    PRODUCT_STATUS_LINE_READONLY_STATES = {
        'ipo_to_approve': [('readonly', True)],
        'to approve': [('readonly', True)],
        'close': [('readonly', True)],
        'done': [('readonly', True)],
        'cancel': [('readonly', True)],
    }
    def get_dashboard_data(self):
        """Return: dict of purchase dashboard values like counts of different order types"""
        purchase_dash = {}
        po_obj = self.env['purchase.order']
        so_obj = self.env['sale.order']
        all_order = po_obj.search_count([('po_type', '!=', 'non_billable'), ('state', 'in', ['sent', 'purchase', 'shipped', 'deliver', 'draft']), '|', '|', ('account_manager', '=', self.env.user.id), ('soc_id', '=', self.env.user.id), ('ass_account_manager', 'in', [self.env.user.id])])
        samples = po_obj.search_count([('po_type', '=', 'non_billable'), ('state', 'in', ['sent', 'purchase', 'shipped', 'deliver']), '|', '|', ('account_manager', '=', self.env.user.id), ('soc_id', '=', self.env.user.id), ('ass_account_manager', 'in', [self.env.user.id])])
        waiting = po_obj.search_count([('state', '=', 'sent'), ('po_type','not in', ('sample', 'non_billable')), '|', '|', ('account_manager', '=', self.env.user.id), ('soc_id', '=', self.env.user.id), ('ass_account_manager', 'in', [self.env.user.id])])
        proof = po_obj.search_count([('proof_required', '=', True), ('state', 'in', ['sent', 'purchase', 'shipped', 'deliver']), '|', '|', ('account_manager', '=', self.env.user.id), ('soc_id', '=', self.env.user.id), ('ass_account_manager', 'in', [self.env.user.id])])
        shipped = po_obj.search_count([('state', '=', 'shipped'), '|', '|', ('account_manager', '=', self.env.user.id), ('soc_id', '=', self.env.user.id), ('ass_account_manager', 'in', [self.env.user.id])])
        close = so_obj.search_count(['|', '&', ('state', '=', 'invoiced'), ('all_po_delivered', '=', 'deliver'), '&', ('state', '=', 'delivered'), ('invoice_status', '=', 'invoiced'), '|', '|', ('user_id', '=', self.env.user.id), ('soc_id', '=', self.env.user.id), ('aam_id', 'in', [self.env.user.id])])
        purchase_dash['all_order'] = all_order
        purchase_dash['samples'] = samples
        purchase_dash['waiting'] = waiting
        purchase_dash['proof'] = proof
        purchase_dash['shipped'] = shipped
        purchase_dash['close'] = close
        return purchase_dash


    @api.multi
    def copy(self, default=None):
        self.ensure_one()
        decoration_line_copy = {}
        purchase_order_line_copy = {}
        default['program_id'] = self.program_id.id
        default['channel_id'] = self.channel_id.id
        default['program_type'] = self.program_type
        default['origin'] = self.origin
        default['partner_ref'] = self.partner_ref
        result = super(PurchaseOrder, self).copy(default)

        for purchase_line in self.product_receives:
            new_po_line = purchase_line.copy({
                    'order_id': result.id,
                })
            purchase_order_line_copy[purchase_line.id] = new_po_line
            for decoration_line in purchase_line.decorations:
                new_dec_line = decoration_line.copy({
                    'purchase_order_line_id': new_po_line.id,
                    'purchase_order_id': result.id,
                    })
                decoration_line_copy[decoration_line.id] = new_dec_line

        for purchase_line in self.order_line:
            new_po_line = purchase_line.copy({
                    'order_id': result.id
                })
            purchase_order_line_copy[purchase_line.id] = new_po_line
            for decoration_line in purchase_line.decorations:
                new_dec_line = decoration_line.copy({
                    'purchase_order_line_id': new_po_line.id,
                    'purchase_order_id': result.id,
                    })
                decoration_line_copy[decoration_line.id] = new_dec_line

        for decoration_group in self.decoration_group_read_ids:
            new_group_id = decoration_group.copy({'purchase_order_id': result.id})
            for group_one in decoration_group:
                for decoration_line in group_one.purchase_decorations:
                    new_dec_line = decoration_line_copy.get(decoration_line.id)
                    if new_dec_line:
                        new_dec_line.write({'decoration_group_id': new_group_id.id})
            for decoration_charge_line in decoration_group.charge_lines:
                new_dec_charge_line = decoration_charge_line.copy({
                    'decoration_group_id': new_group_id.id,
                    })
                for po_line in decoration_charge_line.purchase_order_lines:
                    new_line = purchase_order_line_copy.get(po_line.id)
                    if new_line:
                        new_line.write({
                            'decoration_charge_group_id': new_dec_charge_line.id,
                            })
        return result

    @api.multi
    def calcalute_only_cost(self):
        if self.po_type not in ['sample', 'non_billable']:
            res_cost = self._set_tier_cost_unit_by_web_api()
        else:
            res_cost = self._set_tier_cost_unit_by_web_api(sample_order=True)
        return True

    def _set_tier_cost_unit_by_web_api(self, sample_order=None):
        product_product_qty = {}
        blank_product = self.env.ref('pinnacle_sales.product_product_blank_order')
        for order_line in self.order_line:
            if (order_line.product_id.product_tmpl_id.type != 'service' or order_line.product_id.product_tmpl_id.id == blank_product.id):
                key = order_line.product_id.product_tmpl_id.id
                supplier_key = order_line.sale_order_line_id.change_supplier.id or (order_line.product_id.product_tmpl_id._fetch_seller(self.partner_id.id)).id or False
                if key and supplier_key:
                    if not product_product_qty.has_key(key):
                        product_product_qty[key] = {}
                        product_product_qty[key][supplier_key] = {}
                        product_product_qty[key][supplier_key]['qty'] = order_line.product_qty
                    elif not product_product_qty[key].has_key(supplier_key):
                        product_product_qty[key][supplier_key] = {}
                        product_product_qty[key][supplier_key]['qty'] = order_line.product_qty
                    else:
                        product_product_qty[key][supplier_key]['qty'] = product_product_qty[key][supplier_key]['qty'] + order_line.product_qty

        for order_line in self.order_line:
            if (order_line.product_id.product_tmpl_id.type != 'service' or order_line.product_id.product_tmpl_id.id == blank_product.id):
                key = order_line.product_id.product_tmpl_id.id

                supplier_key = order_line.sale_order_line_id.change_supplier or (order_line.product_id.product_tmpl_id._fetch_seller(self.partner_id.id)) or False
                if not supplier_key:
                    continue
                qty = product_product_qty[key][supplier_key.id]['qty']
                res = order_line.product_id.get_cost_price(supplier_key, qty, sample_order=sample_order, spec_order=True)
                cost = res['cost']
                supplier = order_line.change_supplier
                if sample_order:
                    if supplier_key.name.company_type == "person" or supplier_key.name.customer == True:
                        raise UserError(_('This supplier\'s sample policy has not been set. '))
                    else:
                        discount = None
                        discount_price = 0
                        if supplier_key.override_sample_policy_id.id:
                            policy = supplier_key.override_sample_policy_id
                        else:
                            policy = supplier_key.name.sample_policy_id
                        if policy.discount == "free":
                            cost = 0
                        elif policy.discount_price:
                            cost = cost - cost * policy.discount_price / 100.0
                if not order_line.is_override:
                    order_line.update({'price_unit': cost})
        return True


    @api.depends('po_type', 'partner_id')
    def compute_cutoff_time(self):
        for this in self:
            if this.po_type != 'regular':
                product_tmpl_ids = [line.product_id.product_tmpl_id.id for line in this.order_line]
                suppliers = self.env['product.supplierinfo'].search([('name', '=', this.partner_id.id), ('product_tmpl_id', 'in', product_tmpl_ids)])
                if not suppliers:
                    return
                cutoffs = []
                am_cutoffs = []
                pm_cutoffs = []
                for supplier in suppliers:
                    if not supplier.override_sample_policy_id:
                        cutoffs.append(supplier.cut_off)
                    else:
                        cutoffs.append(supplier.override_cut_off)
                for cutoff in cutoffs:
                    if cutoff:
                        if cutoff[-2:] == 'PM':
                            pm_cutoffs.append(cutoff)
                        if cutoff[-2:] == 'AM':
                            am_cutoffs.append(cutoff)
                if len(am_cutoffs) != 0:
                    order_cutoff = sorted([int(cutoff[:-2]) for cutoff in am_cutoffs])
                    if (order_cutoff):
                        this.order_cutoff_time = str(order_cutoff[0])+'AM'
                else:
                    order_cutoff = sorted([int(cutoff[:-2]) for cutoff in pm_cutoffs])
                    if (order_cutoff):
                        this.order_cutoff_time = str(order_cutoff[0])+'PM'
            else:
                this.order_cutoff_time = this.partner_id.order_cutoff_time

    @api.one 
    @api.depends('decoration_group_ids')
    def _get_decoration_group(self):
        self.decoration_group_read_ids = [(6, 0, self.decoration_group_ids.ids)]


    sale_order_id = fields.Many2one("sale.order", readonly=True, string="Sale Order")
    for_blank = fields.Boolean(string="For Blanks")
    for_decoration = fields.Boolean(string="For Decorations", readonly=True)
    shipping_lines = fields.One2many('shipping.line', 'purchase_order_id', string='Shipping', states=READONLY_STATES, copy=True)
    decoration_line =  fields.One2many('purchase.order.line', 'order_id', string='Decoration Lines', states=READONLY_STATES, domain=[('can_decorate', '=', True),('product_type', '!=', 'service')])
    art_lines =  fields.One2many('sale.order.line.decoration', 'purchase_order_id', string="Art Work", states=READONLY_STATES)

    decorations = fields.One2many('sale.order.line.decoration', 'purchase_order_id',
                                      string='Decoration Lines', states=READONLY_STATES)
    decoration_group_ids = fields.One2many('sale.order.line.decoration.group', 'purchase_order_id', string="Decoration Groups", copy=False)
    decoration_group_read_ids = fields.One2many('sale.order.line.decoration.group', 'purchase_order_id', compute="_get_decoration_group", string="Decoration Groups")
    po_type = fields.Selection([('regular', 'Regular'), ('sample', 'Sample'),('non_billable', 'Non Billable Sample'), ('internal_purchase_order','Internal Purchase Order')], 'Type', default='regular')
    auto_generate = fields.Boolean('Auto Generate', default=False) 
    order_cutoff_time = fields.Char(compute="compute_cutoff_time", string='Vendor Cut Off Time')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('ipo_to_approve', 'IPO Approval'),
        ('ipo_approved', 'IPO Approved'),
        ('ipo_rejected', 'IPO Rejected'),
        ('sent', 'Submitted'),
        ('to approve', 'To Approve'),
        ('purchase', 'Confirmed'),
        ('shipped', 'Shipped'),
        ('deliver', 'Delivered'),
        ('close', 'Closed'),
        ('reopen', 'Re-Opened'),
        ('done', 'Re-Closed'),
        ('cancel', 'Cancelled'),
        ('hold', 'On Hold'),
        ], string='Status', readonly=True, index=True, copy=False, default='draft', track_visibility=False)

    partner_id = fields.Many2one('res.partner', string='Vendor', required=True, states=READONLY_STATES, change_default=True, track_visibility=False)
    order_line = fields.One2many('purchase.order.line', 'order_id', string='Order Lines', states=READONLY_STATES, domain=[('incoming', '=', False)], copy=False)
    incoterm_id = fields.Many2one('stock.incoterms', 'Incoterm', states={'deliver': [('readonly', True)], 'done': [('readonly', True)]}, help="International Commercial Terms are a series of predefined commercial terms used in international transactions.")
    product_status_line = fields.One2many('purchase.order.line.for.tracking', 'purchase_order_id', string='Product Status Lines', states=PRODUCT_STATUS_LINE_READONLY_STATES, copy=True, track_visibility=False)
    partner_invoice_id = fields.Many2one('res.partner', 'Invoice Address', states=READONLY_STATES)
    vendor_note = fields.Text('Vendor Notes')
    product_note = fields.Text('Product Notes')
    internal_note = fields.Text('Internal Notes')
    proof_note = fields.Text('Proof Notes')
    internal_so_ref = fields.Char('Internal Reference SO', help='Reference of the internal order number such as Magento Order Number or Pinnacle Promotions Website Order Number.')
    purchase_decoration_groups = fields.One2many(comodel_name='purchase.decoration.group.for.tracking', inverse_name='purchase_order_id', string="Decoration Groups", states=PRODUCT_STATUS_LINE_READONLY_STATES,copy=True, track_visibility=False)
    last_state = fields.Char()
    rush_finisher = fields.Many2one('res.partner', readonly="1")
    rush_supplier = fields.Many2one('res.partner', readonly="1")
    art_button_enable = fields.Boolean(compute="_compute_art_rfq_button_enable")
    ass_account_manager = fields.Many2many('res.users', string='Assistant Account Manager')
    soc_id = fields.Many2one('res.users', 'Vendor Point of Contact')
    account_manager = fields.Many2one('res.users', string='Account Manager', index=True, track_visibility=False, default=lambda self: self.env.user, copy=False)
    proof_required = fields.Boolean(default=False, string="Proof Required")
    po_send = fields.Boolean("Is PO Send", default=False)
    art_send = fields.Boolean("Is Art PO Send", default=False)
    program_id = fields.Many2one('partner.program', 'Program', copy=False)
    program_type = fields.Selection([('custom', 'Custom'), ('store', 'Store'), ('inventory', 'Inventory'), ('nk_inventory', 'NK Inventory')], copy=False)
    channel_id = fields.Many2one('partner.channel', 'Channel', copy=False)
    # Define Finisher, Product line status and decoration method for displaying in Reports
    finisher = fields.Many2one('res.partner', 'Finisher', related='shipping_lines.finisher')
    product_line_status = fields.Many2one('purchase.product.status.status', 'Production Line', related='purchase_decoration_groups.product_status_status')
    decoration_method = fields.Many2one('decoration.method' ,'Decoration Method', related='decoration_line.decorations.imprint_method')
    date_order = fields.Datetime('Order Date', readonly=True, index=True, copy=False, default=False,\
        help="Depicts the date where the Quotation should be validated and converted into a purchase order.")
    # Add field Tape complete for Purchase reports not consider if it was completed
    tape_complete = fields.Boolean('Tape Complete')
    # Add fields invoice_total, bill_creation date , difference for Vendor Report 
    invoice_total = fields.Float('invoice total', compute='_get_bill', default=False)
    bill_creation_date = fields.Date('Bill Create Date', related='order_line.invoice_lines.invoice_id.date_create')
    difference = fields.Float('Difference', compute='_get_bill', default=False)
    fulfillment_receiving_notes = fields.Text(string="Fulfillment Receiving Notes")
    inventory_flag = fields.Selection([('Sent to Warehouse','Sent to Warehouse'),('Received at Warehouse','Received at Warehouse')], string="Invetory Flag")
    inventory_date_stamp = fields.Date(string="Invetory Date Stamp")
    inventory_sent_to_warehouse_flag = fields.Boolean(string="Sent to Warehouse")
    inventory_received_to_warehouse_flag = fields.Boolean(string="Received at Warehouse")
    inventory_reconciled_to_warehouse_flag = fields.Boolean(string="Reconciled at Warehouse")
    inventory_sent_to_warehouse_date_stamp = fields.Datetime(string="Invetory Sent Date Stamp")
    inventory_received_to_warehouse_date_stamp = fields.Datetime(string="Invetory Received Date Stamp")
    inventory_reconciled_to_warehouse_date_stamp = fields.Datetime(string="Invetory Reconciled Date Stamp")
    partner_ref = fields.Char('Vendor Reference', copy=False,\
        help="Reference of the sales order or bid sent by the vendor. "
             "It's used to do the matching when you receive the "
             "products as this reference is usually written on the "
             "delivery order sent by your vendor.", states=READONLY_STATES)
    currency_id = fields.Many2one('res.currency', 'Currency', required=True, states=READONLY_STATES,\
        default=lambda self: self.env.user.company_id.currency_id.id)
    origin = fields.Char('Source Document', copy=False,\
        help="Reference of the document that generated this purchase order "
             "request (e.g. a sale order or an internal procurement request)", states=READONLY_STATES)

    customer_arts = fields.Many2many(
        'product.decoration.customer.art', compute="_compute_customer_arts")
    decoration_sequence_id = fields.Integer(
        string='Decoration Line Sequence', readonly=True, default=1, copy=True)
    decoration_group_sequence_id = fields.Integer(
        string='Decoration Group Sequence', readonly=True, default=1, copy=False)
    sent_to_warehouse_flag = fields.Boolean('Is Sent to warehouse ?')
    packing_slips = fields.One2many('purchase.order.packing.slip', 'purchase_order_id', string="Packing Slips")
    tag_ids = fields.Many2many('crm.lead.tag', 'crm_lead_purchase_tag_rel', 'lead_id', 'tag_id')
    so_partner_id = fields.Many2one('res.partner', domain="[('customer', '=', True)]", string="Customer")
    customer_art = fields.Many2one('product.decoration.customer.art', related="decoration_line.decorations.customer_art", string="Art (file or text)", store=True)
    is_reorder =  fields.Boolean(compute="_compute_is_reorder", default=False)
    is_cgp =  fields.Boolean(compute="_compute_is_cgp", default=False)
    # add Analytic tags and its values to display in Account Invoice list view
    analytic_tag_ids = fields.Many2many('account.analytic.tag', string='Analytic Tags', compute='_get_analytic_tags', store=True)
    analytic_tag_ids_value = fields.Char(string='Analytic Tags ID', compute='_get_analytic_tags')
    overrun = fields.Selection([
        ('overrun_1', 'Exact Quantity Only - No Overruns'),
        ('overrun_2', 'Send 1-2 Overruns to Pinnacle Promotions'),
        ('overrun_3', 'No Unders, up to 5% Over OK - Send All Overs to Pinnacle Promotions'),
        ('overrun_4', 'No Overs, up to 5% Under OK')], string='Overrun')
    close_date = fields.Datetime('Closed Date', copy=False)
    reopen_date = fields.Datetime('Re-Opened Date', copy=False)
    reclose_date = fields.Datetime('Re-Closed Date', copy=False)
    shipped_date = fields.Datetime('Shipped Date', copy=False)
    delivered_date = fields.Datetime('Delivered Date', copy=False)
    product_receives  = fields.One2many('purchase.order.line', 'order_id', domain=[('can_decorate', '=', True), ('incoming', '=', True)])
    po_sent_date = fields.Datetime('PO Sent Date')
    art_sent_date = fields.Datetime('Art Sent Date')
    date_po_confirm = fields.Datetime('PO Confirm Date')
    so_amount_total = fields.Float('SO Amount Total', compute='compute_priority_orders')
    priority_order = fields.Boolean('Is Priority Order', compute='compute_priority_orders', store=True)
    non_priority_order = fields.Boolean('Is Non Priority Order', compute='compute_priority_orders', store=True)
    non_priority_trigger_reason = fields.Text('Non Priority Trigger Reason', compute='compute_priority_orders')
    priority_trigger_reason = fields.Text('Priority Trigger Reason', compute='compute_priority_orders')
    cgp_blank = fields.Boolean('CGP Blank Report', compute='compute_priority_orders', store=True)
    cgp_production = fields.Boolean('CGP Blank Report', compute='compute_priority_orders', store=True)
    digitization = fields.Boolean('Digitization', compute='compute_priority_orders', store=True)
    exception_report = fields.Boolean('Exception', compute='compute_priority_orders', store=True)
    earliest_ship_date = fields.Date('Earliest Ship Date', related='shipping_lines.ship_date')
    purchase_order_name_updated = fields.Boolean('Purchase Order Name updated due to selection of Internal Purchase Order ?', default=False)
    send_for_ipo_approval = fields.Many2one(compute="_compute_send_for_ipo_approval", comodel_name="internal.purchase.order.approval.slab", store=True, copy=False)
    ipo_approved_or_rejected_by = fields.Many2one('res.users', 'IPO Approved/Rejected By')

    _sql_constraints = [('purchase_name_uniq', 'unique (name, company_id)', 'Purchase name must be !')]
    @api.multi
    def similarity_check_of_product_status_lines(self, product_status_line_ids):
        if product_status_line_ids:
            product_status_lines = self.env['purchase.order.line.for.tracking'].browse(product_status_line_ids)
            if product_status_lines:
                first_product_status_line = product_status_lines[0]
                product_tracking_line_id_groups = []
                for product_status_line in product_status_lines:
                    if first_product_status_line.product_status_status.id != product_status_line.product_status_status.id:
                        return False
                    if len(first_product_status_line.product_tracking_lines.ids) != len(product_status_line.product_tracking_lines.ids):
                        return False
                    product_tracking_line_id_groups.append(product_status_line.product_tracking_lines.ids)
                if product_tracking_line_id_groups:
                    result = True
                    number_of_product_tracking_lines_each_have = len(product_tracking_line_id_groups[0])
                    index = 0
                    while index < number_of_product_tracking_lines_each_have:
                        group_of_product_tracking_lines_at_same_index = []
                        for product_tracking_line_id in product_tracking_line_id_groups:
                            group_of_product_tracking_lines_at_same_index.append(product_tracking_line_id[index])
                        result &= self.similarity_check_of_product_tracking_lines(group_of_product_tracking_lines_at_same_index)
                        index += 1
                    if not result:
                        return False
        return True

    @api.multi
    def similarity_check_of_decoration_group_status_lines(self, purchase_decoration_groups):
        if purchase_decoration_groups:
            purchase_decoration_groups = self.env['purchase.decoration.group.for.tracking'].browse(purchase_decoration_groups)
            if purchase_decoration_groups:
                first_purchase_decoration_group = purchase_decoration_groups[0]
                product_tracking_line_id_groups = []
                for purchase_decoration_group in purchase_decoration_groups:
                    if first_purchase_decoration_group.product_status_status.id != purchase_decoration_group.product_status_status.id:
                        return False
                    if len(first_purchase_decoration_group.product_tracking_lines.ids) != len(purchase_decoration_group.product_tracking_lines.ids):
                        return False
                    product_tracking_line_id_groups.append(purchase_decoration_group.product_tracking_lines.ids)
                if product_tracking_line_id_groups:
                    result = True
                    number_of_product_tracking_lines_each_have = len(product_tracking_line_id_groups[0])
                    index = 0
                    while index < number_of_product_tracking_lines_each_have:
                        group_of_product_tracking_lines_at_same_index = []
                        for product_tracking_line_id in product_tracking_line_id_groups:
                            group_of_product_tracking_lines_at_same_index.append(product_tracking_line_id[index])
                        result &= self.similarity_check_of_product_tracking_lines(group_of_product_tracking_lines_at_same_index)
                        index += 1
                    if not result:
                        return False
        return True

    @api.multi
    def similarity_check_of_product_tracking_lines(self, product_tracking_line_ids):
        if product_tracking_line_ids:
            product_tracking_lines = self.env['product.tracking.numbers'].browse(product_tracking_line_ids)
            if product_tracking_lines:
                first_product_tracking_line = product_tracking_lines[0]
                for product_tracking_line in product_tracking_lines:
                    if first_product_tracking_line.delivery_carrier.id != product_tracking_line.delivery_carrier.id:
                        return False
                    if first_product_tracking_line.tracking_number != product_tracking_line.tracking_number:
                        return False
                    if first_product_tracking_line.generated_tracking_number_url != product_tracking_line.generated_tracking_number_url:
                        return False
        return True

    @api.multi
    def add_tracking_lines_to_product_status_line(self):
        self.ensure_one()
        context_active_ids = []
        if self._context.get('product_status_line', False):
            for product_status_line in self.product_status_line:
                if product_status_line.select:
                    if product_status_line.product_tracking_lines:
                        raise UserError(_('Please select all lines without any tracking line.'))
                    else:
                        context_active_ids.append(product_status_line.id)
        if self._context.get('purchase_decoration_groups', False):
            for purchase_decoration_group in self.purchase_decoration_groups:
                if purchase_decoration_group.select:
                    if purchase_decoration_group.product_tracking_lines:
                        raise UserError(_('Please select all lines without any tracking line.'))
                    else:
                        context_active_ids.append(purchase_decoration_group.id)
        self.clear_all_selection_for_purchase_order()
        if len(context_active_ids) >= 1:
            new_context = dict(self.env.context).copy()
            new_context.update({'active_id': context_active_ids[0],
                                'active_ids': context_active_ids,
                                'order_id': self.id})
            form_view_id = self.env.ref(
                'pinnacle_purchase.product_status_add_tracking_form_view')
            name = ''
            if self._context.get('product_status_line', False):
                name = 'Add Product/Service Status Line Tracking Lines'
            if self._context.get('purchase_decoration_groups', False):
                name = 'Add Decoration Group Status Line Tracking Lines'
            return {'type': 'ir.actions.act_window',
                    'view_mode': 'form',
                    'target': 'new',
                    'view_id': form_view_id.id,
                    'res_model': 'product.status.add.tracking',
                    'name': name,
                    'context': new_context,
                    }
        else:
            raise UserError(_('Please select at least one product status line without any tracking line.'))

    @api.multi
    def edit_product_status_line_information(self):
        self.ensure_one()
        context_active_ids = []
        if self._context.get('product_status_line', False):
            for product_status_line in self.product_status_line:
                if product_status_line.select:
                    context_active_ids.append(product_status_line.id)
        if self._context.get('purchase_decoration_groups', False):
            for purchase_decoration_group in self.purchase_decoration_groups:
                if purchase_decoration_group.select:
                    context_active_ids.append(purchase_decoration_group.id)
        self.clear_all_selection_for_purchase_order()
        if len(context_active_ids) >= 1:
            similarity_check = False
            if self._context.get('product_status_line', False):
               similarity_check = self.similarity_check_of_product_status_lines(context_active_ids) 
            if self._context.get('purchase_decoration_groups', False):
                similarity_check = self.similarity_check_of_decoration_group_status_lines(context_active_ids) 
            if similarity_check:
                new_context = dict(self.env.context).copy()
                new_context.update({'active_id': context_active_ids[0],
                                    'active_ids': context_active_ids,
                                    'order_id': self.id})
                form_view_id = self.env.ref(
                    'pinnacle_purchase.edit_product_status_line_info_form_view')
                name = ''
                if self._context.get('product_status_line', False):
                    name = 'Edit Product/Service Status Line Information'
                if self._context.get('purchase_decoration_groups', False):
                    name = 'Edit Decoration Group Status Line Information'
                return {'type': 'ir.actions.act_window',
                        'view_mode': 'form',
                        'target': 'new',
                        'view_id': form_view_id.id,
                        'res_model': 'edit.product.status.line.info',
                        'name': name,
                        'context': new_context,
                        }
            else:
                raise UserError(_('Please select product status lines that have following values common \n -> Status value \n -> Number of tracking lines \n -> Each tracking line should be to common all other product status lines .'))    
        else:
            raise UserError(_('Please select at least one product status line.'))

    @api.multi
    def email_tracking_lines(self):
        self.ensure_one()
        context_active_ids = []
        if self._context.get('product_status_line', False):
            for product_status_line in self.product_status_line:
                if product_status_line.select:
                    if not product_status_line.product_tracking_lines:
                        raise UserError(_('Please select all lines with at least one tracking line.'))
                    else:
                        context_active_ids.append(product_status_line.id)
        if self._context.get('purchase_decoration_groups', False):
            for purchase_decoration_group in self.purchase_decoration_groups:
                if purchase_decoration_group.select:
                    if not purchase_decoration_group.product_tracking_lines:
                        raise UserError(_('Please select all lines with at least one tracking line.'))
                    else:
                        context_active_ids.append(purchase_decoration_group.id)
        self.clear_all_selection_for_purchase_order()
        if len(context_active_ids) >= 1:
            new_context = dict(self.env.context).copy()
            new_context.update({'active_id': context_active_ids[0],
                                'active_ids': context_active_ids,
                                'order_id': self.id})
            ir_model_data = self.env['ir.model.data']
            try:
                template_id = ir_model_data.get_object_reference('pinnacle_purchase', 'mail_customer_of_multiple_tracking_numbers')[1]
            except ValueError:
                template_id = False
            try:
                compose_form_id = ir_model_data.get_object_reference('mail', 'email_compose_message_wizard_form')[1]
            except ValueError:
                compose_form_id = False
            new_context.update({
                'default_model': 'purchase.order',
                'default_res_id': self.id,
                'default_use_template': bool(template_id),
                'default_template_id': template_id,
                'default_composition_mode': 'comment',
                'default_partner_id': self.partner_id.id
            })
            return {
                'name': _('Compose Email'),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'mail.compose.message',
                'views': [(compose_form_id, 'form')],
                'view_id': compose_form_id,
                'target': 'new',
                'context': new_context,
            }
        else:
            raise UserError(_('Please select at least one product status line with at least one tracking line.'))

    @api.one
    @api.depends('order_line.analytic_tag_ids')
    def _get_analytic_tags(self):
        """Compute the values for analytic tags
        Returns: String of Analytic tags IDS with comma sepration"""
        if self.order_line:
            self.analytic_tag_ids = self.order_line[0].analytic_tag_ids.ids
            value = ''
            for tag in self.analytic_tag_ids:
                value += str(tag.id) + ','
            self.analytic_tag_ids_value = value

    @api.onchange('so_partner_id', 'po_type' , 'sale_order_id', 'order_line')
    def _onchange_sale_order_id_and_po_type(self):
        if self.so_partner_id and not self.sale_order_id:
            program_id = False
            channel_id = False
            analytic_tag_ids = []
            if self.so_partner_id.parent_id:
                program_id = self.so_partner_id.program_contact_id.id
                channel_id = self.so_partner_id.channel_contact_id.id
                analytic_tag_ids = self.so_partner_id.analytic_contact_tag_ids.ids
            else:
                program_id = self.so_partner_id.program_id.id
                channel_id = self.so_partner_id.channel_id.id
                analytic_tag_ids = self.so_partner_id.analytic_tag_ids.ids
            self.program_id = program_id
            self.channel_id = channel_id
            for order_line in self.order_line:
                order_line.analytic_tag_ids = [[6, 0, analytic_tag_ids]]

    @api.one
    @api.depends('partner_id')
    def _compute_is_cgp(self):
        self.is_cgp = False
        if self.partner_id and self.partner_id.name and (self.partner_id.name.lower()).find('cgp') != -1:
            self.is_cgp = True

    @api.one
    @api.depends('decoration_group_read_ids.reorder_ref_po')
    def _compute_is_reorder(self):
        self.is_reorder = False
        for this in self.decoration_group_read_ids:
            if this.reorder_ref_po:
                self.is_reorder = True
                return
    @api.multi
    def name_get(self):
        self = self.exists()
        res = []
        for po in self:
            if po._context.get('ref_po', False):
                vendor = po.partner_id.name
                res.append([po.id, "%s" % (po.name+', '+vendor)])
            else:
                res = super(PurchaseOrder, self).name_get()
        return res

    @api.onchange('sale_order_id')
    def onchange_sale_order_id(self):
        if self.sale_order_id and self.auto_generate == False:
            self.so_partner_id = self.sale_order_id.partner_id.id
            self.account_manager = self.sale_order_id.user_id.id
            self.ass_account_manager = [(6, 0, self.sale_order_id.aam_id.ids)]
            self.soc_id = self.sale_order_id.soc_id.id
            self.program_id = self.sale_order_id.program_id.id
            self.channel_id = self.sale_order_id.channel_id.id
            self.channel_id = self.sale_order_id.channel_id.id
            self.program_type = self.sale_order_id.program_type
            for order_line in self.order_line:
                order_line.analytic_tag_ids = [[6, 0, self.sale_order_id.analytic_tag_ids.ids]]

    @api.onchange('so_partner_id')
    def onchange_so_partner_id(self):
        if self.so_partner_id:
            self.account_manager = self.so_partner_id.user_id.id
            if self.shipping_lines and self.po_type != 'regular':
                for line in self.shipping_lines:
                    line.contact = self.so_partner_id.id

    @api.onchange('po_type')
    def _onchange_po_type(self):
        if self.po_type == 'non_billable':
            self.for_blank = True

    @api.multi
    def get_account_number(self, line):
        if line:
            if line.customer_account:
                return line.account_number
            elif line.vendor_account:
                return 'Vendor'
            elif not line.vendor_account and not line.customer_account and line.delivery_method == self.env.ref('delivery_fedex.delivery_carrier_fedex_us'):
                return line.delivery_method.sudo().fedex_account_number
            else:
                return ''

    @api.onchange('program_type')
    def onchange_program_type(self):
        if self.po_type == 'non_billable' and self.program_type == 'inventory':
            self.program_type = False
            return {'warning': {'title': 'Error', 'message': 'You can not select Program Type: Inventory for Non Billable Sample Orders!'}}

    @api.multi
    def clear_all_selection_for_purchase_order(self):
        for record in self:
            if record.order_line:
                record.order_line.write({'select': False, 'select_decoration_line': False})
            if record.decorations:
                record.decorations.write({'select': False})
            if record.decoration_group_ids:
                record.decoration_group_ids.write({'select': False})
            if record.product_status_line:
                record.product_status_line.write({'select': False})
            if record.purchase_decoration_groups:
                record.purchase_decoration_groups.write({'select': False})
            if record.art_lines:
                record.art_lines.write({ 'select_art_line': False})

    @api.multi
    def create_packing_slips(self):
        self.ensure_one()
        new_context = dict(self.env.context).copy()
        new_context.update({'active_id': self.id,
                            'active_ids': [self.id]})
        form_view_id = self.env.ref(
            'pinnacle_purchase.purchase_order_add_packing_slip_form_view')
        return {'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'target': 'new',
                'view_id': form_view_id.id,
                'res_model': 'purchase.order.add.packing.slip',
                'name': 'Add Purchase Order Packing Slip',
                'context': new_context,
                }

    @api.multi
    def unlink(self):
        if self.user_has_groups('base.group_erp_manager'):
            res = super(PurchaseOrder, self).unlink() 
            return res
        else:
            raise UserError('Purchase Order cannot be deleted')

    @api.multi
    def bulk_upload_po(self):
        self.ensure_one()
        context_active_ids = []
        new_context = dict(self.env.context).copy()
        for line in self.art_lines:
            if line.select_art_line:
                context_active_ids.append(line.id)
                spec_file_url = line.spec_file_url
                vendor_file_url = line.vendor_file_url
                spec_file2_url = line.spec_file2_url
        self.clear_all_selection_for_purchase_order()
        if not context_active_ids:
            raise UserError(_('Please select at least one line.'))
        new_context.update({'po_line_ids': context_active_ids,
                            'cfs_attachment': True,
                            'po_line': True,
                            'default_spec_file_url': spec_file_url,
                            'default_vendor_file_url': vendor_file_url,
                            'default_spec_file2_url': spec_file2_url})
        form_view_id = self.env.ref(
            'pinnacle_sales.art_upload_file_form_view')
        return {'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'target': 'new',
                'view_id': form_view_id.id,
                'res_model': 'art.upload.file',
                'name': 'File Upload',
                'context': new_context,
                }

    @api.one
    @api.depends('order_line')
    def _compute_customer_arts(self):
        customer_arts = []
        for order_line in self.order_line:
            for decoration in order_line.decorations:
                customer_arts += decoration.customer_art.ids
        self.customer_arts = [(6, 0, customer_arts)]

    @api.multi
    def create_decoration_group(self):
        decoration_ids = []
        for decoration in self.decorations:
            if decoration.select:
                decoration_ids.append(decoration.id)
        self.clear_all_selection_for_purchase_order()
        same_decoration_group = True
        if not decoration_ids:
            raise UserError(_('Please select at least one decoration line for creating decoration group.'))
        for decoration in self.env['sale.order.line.decoration'].browse(decoration_ids):
            same_decoration_group = same_decoration_group and decoration.decoration_group_compare(self.env['sale.order.line.decoration'].browse(decoration_ids)[0])
        if not same_decoration_group:
            raise UserError(_('Imprint Method and Finisher must be the same to create a Decoration Group.'))
        else:
            if self.env['sale.order.line.decoration'].browse(decoration_ids):
                for decoration in self.env['sale.order.line.decoration'].browse(decoration_ids):
                    if decoration.decoration_group_id:
                        if len(decoration.decoration_group_id.decorations) <= 1:
                            decoration.decoration_group_id.unlink()
                        else:
                            decoration.decoration_group_id.charge_lines.unlink()
                            decoration.decoration_group_id = False
                if len(decoration_ids) == len(self.decorations):
                    self.decoration_group_ids.unlink()
                decoration_group = False
                if len(self.decoration_group_ids) > 0:
                    if len(self.decoration_group_ids) != 1:
                        if len(self.decoration_group_ids) != self.decoration_group_ids.sorted(key=lambda r:r.sequence_id)[len(self.decoration_group_ids)-1].sequence_id:
                            if self.decoration_group_ids.sorted(key=lambda r:r.sequence_id)[0].sequence_id == 1:
                                index = 0
                                for decoration_group_id in self.decoration_group_ids.sorted(key=lambda r:r.sequence_id):
                                    if decoration_group_id.sequence_id + 1 != self.decoration_group_ids.sorted(key=lambda r:r.sequence_id)[index + 1].sequence_id:
                                        decoration_group = self.env['sale.order.line.decoration.group'].create({'purchase_order_id': self.id,'group_sequence': 'DG'+str(decoration_group_id.sequence_id + 1),'group_id': 'Decoration Group '+str(decoration_group_id.sequence_id + 1),'sequence_id':decoration_group_id.sequence_id + 1})     
                                        break
                                    index += 1
                            else:
                                decoration_group = self.env['sale.order.line.decoration.group'].create({'purchase_order_id': self.id,'group_sequence': 'DG1','group_id': 'Decoration Group 1','sequence_id':1})
                        else:
                            if (self.decoration_group_ids.sorted(key=lambda r:r.sequence_id)[len(self.decoration_group_ids)-1].sequence_id + 1) == self.env['sale.order.line.decoration'].browse(decoration_ids)[0].purchase_order_line_id.order_id.decoration_group_sequence_id:
                                decoration_group = self.env['sale.order.line.decoration.group'].create({'purchase_order_id': self.id,'group_sequence': 'DG'+str(self.env['sale.order.line.decoration'].browse(decoration_ids)[0].purchase_order_line_id.order_id.decoration_group_sequence_id),'group_id': 'Decoration Group '+str(self.env['sale.order.line.decoration'].browse(decoration_ids)[0].purchase_order_line_id.order_id.decoration_group_sequence_id),'sequence_id':self.env['sale.order.line.decoration'].browse(decoration_ids)[0].purchase_order_line_id.order_id.decoration_group_sequence_id})     
                                self.env['sale.order.line.decoration'].browse(decoration_ids)[0].purchase_order_line_id.order_id.decoration_group_sequence_id += 1
                            else:
                                decoration_group = self.env['sale.order.line.decoration.group'].create({'purchase_order_id': self.id,'group_sequence': 'DG'+str(self.decoration_group_ids.sorted(key=lambda r:r.sequence_id)[len(self.decoration_group_ids)-1].sequence_id + 1),'group_id': 'Decoration Group '+str(self.decoration_group_ids.sorted(key=lambda r:r.sequence_id)[len(self.decoration_group_ids)-1].sequence_id + 1),'sequence_id':self.decoration_group_ids.sorted(key=lambda r:r.sequence_id)[len(self.decoration_group_ids)-1].sequence_id + 1})
                    else:
                        if self.decoration_group_ids[0].sequence_id == 1:
                            self.env['sale.order.line.decoration'].browse(decoration_ids)[0].purchase_order_line_id.order_id.decoration_group_sequence_id = 2
                            decoration_group = self.env['sale.order.line.decoration.group'].create({'purchase_order_id': self.id,'group_sequence': 'DG'+str(self.env['sale.order.line.decoration'].browse(decoration_ids)[0].purchase_order_line_id.order_id.decoration_group_sequence_id),'group_id': 'Decoration Group '+str(self.env['sale.order.line.decoration'].browse(decoration_ids)[0].purchase_order_line_id.order_id.decoration_group_sequence_id),'sequence_id':self.env['sale.order.line.decoration'].browse(decoration_ids)[0].purchase_order_line_id.order_id.decoration_group_sequence_id})     
                        else:
                            decoration_group = self.env['sale.order.line.decoration.group'].create({'purchase_order_id': self.id,'group_sequence': 'DG1','group_id': 'Decoration Group 1','sequence_id':1})
                else:
                    if len(decoration_ids) == len(self.decorations):
                        self.env['sale.order.line.decoration'].browse(decoration_ids)[0].purchase_order_line_id.order_id.decoration_group_sequence_id = 1
                        decoration_group = self.env['sale.order.line.decoration.group'].create({'purchase_order_id': self.id,'group_sequence': 'DG'+str(self.env['sale.order.line.decoration'].browse(decoration_ids)[0].purchase_order_line_id.order_id.decoration_group_sequence_id),'group_id': 'Decoration Group '+str(self.env['sale.order.line.decoration'].browse(decoration_ids)[0].purchase_order_line_id.order_id.decoration_group_sequence_id),'sequence_id':self.env['sale.order.line.decoration'].browse(decoration_ids)[0].purchase_order_line_id.order_id.decoration_group_sequence_id})     
                        self.env['sale.order.line.decoration'].browse(decoration_ids)[0].purchase_order_line_id.order_id.decoration_group_sequence_id += 1
                    else:
                        self.env['sale.order.line.decoration'].browse(decoration_ids)[0].purchase_order_line_id.order_id.decoration_group_sequence_id = 1
                        decoration_group = self.env['sale.order.line.decoration.group'].create({'purchase_order_id': self.id,'group_sequence': 'DG'+str(self.env['sale.order.line.decoration'].browse(decoration_ids)[0].purchase_order_line_id.order_id.decoration_group_sequence_id),'group_id': 'Decoration Group '+str(self.env['sale.order.line.decoration'].browse(decoration_ids)[0].purchase_order_line_id.order_id.decoration_group_sequence_id),'sequence_id':self.env['sale.order.line.decoration'].browse(decoration_ids)[0].purchase_order_line_id.order_id.decoration_group_sequence_id})     
                        self.env['sale.order.line.decoration'].browse(decoration_ids)[0].purchase_order_line_id.order_id.decoration_group_sequence_id += 1
                if decoration_group:
                    for decoration in self.env['sale.order.line.decoration'].browse(decoration_ids):
                        decoration.decoration_group_id = decoration_group.id
                else:
                    raise UserError(_('Decoration group could not be created.'))

    @api.multi
    def get_vendor_attribute_code(self, line, product_attrb_ids):
        product_supplier_sku = line.change_supplier.product_code
        for attrib in line.change_supplier.attribute_vendor_attb_value_ids:
            if set(attrib.product_attribute_values_ids.ids) == set(product_attrb_ids) and attrib.vendor_value_code:
                product_supplier_sku = attrib.vendor_value_code
                break
        return product_supplier_sku

    @api.multi
    def create_setup_sale_price_and_run_charge_with_zero(self, decoration , decoration_name, total_qty, number_of_colors, number_of_codes, product_setup_sale_price, ltm_policy_applied, ltm_policy_price_unit, ltm_policy_purchase_price, product_run_charge, product_pms_patching, decoration_group_id):
        if ltm_policy_applied: 
            created_record = self.env['sale.order.line.decoration.group.charge.line'].create(
                                        {'product_id': product_setup_sale_price.id,
                                         'product_uom_qty': number_of_colors + number_of_codes,
                                         'price_unit': ltm_policy_price_unit,
                                         'purchase_price': ltm_policy_purchase_price,
                                         'product_uom': product_setup_sale_price.uom_id.id,
                                         'decoration_group_id': decoration_group_id.id,
                                         'at_least_one_decoration_recalculate': decoration.id,
                                         })
            if self.get_vendor_attribute_code(decoration.purchase_order_line_id, decoration.purchase_order_line_id.product_id.attribute_value_ids.ids):
                created_record.name = decoration_group_id.group_sequence  + ' - ' + self.get_vendor_attribute_code(decoration.purchase_order_line_id, decoration.purchase_order_line_id.product_id.attribute_value_ids.ids) + ' - Setup - ' + str(decoration.vendor_decoration_method and decoration.vendor_decoration_method.name or False) +  ' - ' + str(decoration.vendor_decoration_location and decoration.vendor_decoration_location.name or False) +  ' - ' + decoration_name + ' - ' + 'LTM'
                created_record.name_second = self.get_vendor_attribute_code(decoration.purchase_order_line_id, decoration.purchase_order_line_id.product_id.attribute_value_ids.ids) + ' - Setup - ' + str(decoration.vendor_decoration_location and decoration.vendor_decoration_location.name or False) +  ' - '+ decoration_name + ' - ' + 'LTM'
            else:
                created_record.name = decoration_group_id.group_sequence  + ' - Setup - ' + str(decoration.vendor_decoration_method and decoration.vendor_decoration_method.name or False) + ' - ' + str(decoration.vendor_decoration_location and decoration.vendor_decoration_location.name or False) +  ' - ' + decoration_name + ' - ' + 'LTM'
                created_record.name_second = ' Setup - ' + str(decoration.vendor_decoration_location and decoration.vendor_decoration_location.name or False) + ' - '+ decoration_name + ' - ' + 'LTM'
        else:   
            created_record = self.env['sale.order.line.decoration.group.charge.line'].create(
                                        {'product_id': product_setup_sale_price.id,
                                         'product_uom_qty': number_of_colors + number_of_codes,
                                         'price_unit': 0.00,
                                         'purchase_price': 0.00,
                                         'product_uom': product_setup_sale_price.uom_id.id,
                                         'decoration_group_id': decoration_group_id.id,
                                         'at_least_one_decoration_recalculate': decoration.id,
                                         })
            if self.get_vendor_attribute_code(decoration.purchase_order_line_id, decoration.purchase_order_line_id.product_id.attribute_value_ids.ids):
                created_record.name = decoration_group_id.group_sequence  + ' - ' + self.get_vendor_attribute_code(decoration.purchase_order_line_id, decoration.purchase_order_line_id.product_id.attribute_value_ids.ids) + ' - Setup - ' + str(decoration.vendor_decoration_method and decoration.vendor_decoration_method.name or False) +  ' - ' + str(decoration.vendor_decoration_location and decoration.vendor_decoration_location.name or False) +  ' - ' + decoration_name
                created_record.name_second = self.get_vendor_attribute_code(decoration.purchase_order_line_id, decoration.purchase_order_line_id.product_id.attribute_value_ids.ids) + ' - Setup - ' + str(decoration.vendor_decoration_location and decoration.vendor_decoration_location.name or False) +  ' - '+ decoration_name
            else:
                created_record.name = decoration_group_id.group_sequence  + ' - Setup - ' + str(decoration.vendor_decoration_method and decoration.vendor_decoration_method.name or False) + ' - ' + str(decoration.vendor_decoration_location and decoration.vendor_decoration_location.name or False) +  ' - ' + decoration_name
                created_record.name_second = ' Setup - ' + str(decoration.vendor_decoration_location and decoration.vendor_decoration_location.name or False) + ' - '+ decoration_name
        #Need to handle code when quantity_multiplier is zero
        quantity_multiplier = (number_of_colors + number_of_codes)        
        created_record = self.env['sale.order.line.decoration.group.charge.line'].create(
                                {'product_id': product_run_charge.id,
                                 'product_uom_qty': total_qty * (quantity_multiplier if quantity_multiplier > 0 else 1 ),
                                 'run_charge_multiplied': quantity_multiplier if quantity_multiplier > 0 else 1,
                                 'price_unit': 0.0,
                                 'purchase_price': 0.0,
                                 'product_uom': product_run_charge.uom_id.id,
                                 'decoration_group_id': decoration_group_id.id,
                                 'at_least_one_decoration_recalculate': decoration.id,
                                 })
        if self.get_vendor_attribute_code(decoration.purchase_order_line_id, decoration.purchase_order_line_id.product_id.attribute_value_ids.ids):
            created_record.name = decoration_group_id.group_sequence  + ' - ' + self.get_vendor_attribute_code(decoration.purchase_order_line_id, decoration.purchase_order_line_id.product_id.attribute_value_ids.ids) + ' - Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - ' + str(decoration.vendor_decoration_location and decoration.vendor_decoration_location.name or False) +  ' - ' + decoration_name
            created_record.name_second = self.get_vendor_attribute_code(decoration.purchase_order_line_id, decoration.purchase_order_line_id.product_id.attribute_value_ids.ids) + ' - Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - '+ decoration_name
        else:
            created_record.name = decoration_group_id.group_sequence  + ' - Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - ' + str(decoration.vendor_decoration_location and decoration.vendor_decoration_location.name or False) +  ' - ' + decoration_name
            created_record.name_second = ' Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - '+ decoration_name
        created_record = self.env['sale.order.line.decoration.group.charge.line'].create(
                                        {'product_id': product_pms_patching.id,
                                         'product_uom_qty': number_of_codes,
                                         'price_unit': 0.00,
                                         'purchase_price': 0.00,
                                         'product_uom': product_pms_patching.uom_id.id,
                                         'decoration_group_id': decoration_group_id.id,
                                         'at_least_one_decoration_recalculate': decoration.id,
                                         })
        if self.get_vendor_attribute_code(decoration.purchase_order_line_id, decoration.purchase_order_line_id.product_id.attribute_value_ids.ids):
            created_record.name = decoration_group_id.group_sequence  + ' - ' + self.get_vendor_attribute_code(decoration.purchase_order_line_id, decoration.purchase_order_line_id.product_id.attribute_value_ids.ids) + ' - PMS Matching - ' + str(decoration.vendor_decoration_method and decoration.vendor_decoration_method.name or False) +  ' - ' + str(decoration.vendor_decoration_location and decoration.vendor_decoration_location.name or False) +  ' - ' + decoration_name
            created_record.name_second = self.get_vendor_attribute_code(decoration.purchase_order_line_id, decoration.purchase_order_line_id.product_id.attribute_value_ids.ids) + ' - PMS Matching - ' + str(decoration.vendor_decoration_location and decoration.vendor_decoration_location.name or False) +  ' - '+ decoration_name
        else:
            created_record.name = decoration_group_id.group_sequence  + ' - PMS Matching - ' + str(decoration.vendor_decoration_method and decoration.vendor_decoration_method.name or False) + ' - ' + str(decoration.vendor_decoration_location and decoration.vendor_decoration_location.name or False) +  ' - ' + decoration_name
            created_record.name_second = 'PMS Matching - ' + str(decoration.vendor_decoration_location and decoration.vendor_decoration_location.name or False) + ' - '+ decoration_name

    @api.multi
    def calculate_decoration_group_charges(self):
        product_setup_sale_price = self.env.ref('pinnacle_sales.product_product_setup_sale_price')
        product_run_charge = self.env.ref('pinnacle_sales.product_product_run_charge')
        product_pms_patching = self.env.ref('pinnacle_sales.product_product_pms_patching')
        if not product_setup_sale_price:
            raise UserError(_('Setup Sale Price Product Not Found'))
        if not product_run_charge:
            raise UserError(_('Run Charge Product Not Found'))
        if not product_pms_patching:
            raise UserError(_('PMS Matching Product Not Found'))
        not_assigned_group_ids = []
        for decoration_group_id in self.decoration_group_ids:
            if not decoration_group_id.decorations:
                not_assigned_group_ids.append(decoration_group_id.id)
        self.env['sale.order.line.decoration.group'].browse(not_assigned_group_ids).unlink()               
        decoration_group_ids = []
        for decoration_group_id in self.decoration_group_ids:
            if decoration_group_id.select:
                decoration_group_ids.append(decoration_group_id.id)
        self.clear_all_selection_for_purchase_order()
        if not decoration_group_ids:
            raise UserError(_('Please select at least one decoration group line to calculate charges.'))
        for decoration_group_id in self.env['sale.order.line.decoration.group'].browse(decoration_group_ids):
            decoration_group_id.charge_lines.unlink()
            groups = []
            index = 0
            while index < len(decoration_group_id.decorations):
                decoration = decoration_group_id.decorations[index]
                already_grouped = False
                for group in groups:
                    if decoration.id in group:
                        already_grouped = True
                if already_grouped:
                    index += 1
                    continue
                decoration_group = []
                for second_decoration in decoration_group_id.decorations:
                    if decoration.purchase_order_line_id.product_id.product_tmpl_id.id == second_decoration.purchase_order_line_id.product_id.product_tmpl_id.id and decoration.decoration_group_compare_for_calulating_charges(second_decoration):
                        decoration_group.append(second_decoration.id)
                if decoration_group:
                    groups.append(decoration_group)
                index += 1
            for group in groups:
                total_qty = 0
                decoration_name = False
                for decoration in self.env['sale.order.line.decoration'].browse(group):
                    total_qty += decoration.purchase_order_line_id.product_qty
                    if not decoration_name:
                        decoration_name = decoration.decoration_sequence
                    else:
                        decoration_name += ','+decoration.decoration_sequence 
                decoration = self.env['sale.order.line.decoration'].browse(group)[0]
                if decoration:
                    finisher_decorations = decoration.purchase_order_line_id.product_id.finisher_ids.search(['&',('product_id','=',decoration.purchase_order_line_id.product_id.product_tmpl_id.id),('finisher_id','=',decoration.finisher.id)])
                    ltm_policy_applied = False
                    ltm_policy_price_unit = 0.00
                    ltm_policy_purchase_price = 0.00 
                    # LTM Policy Calculation For Set-Up Price Start
                    if decoration.purchase_order_line_id.change_supplier.product_vendor_tier_ids and decoration.purchase_order_line_id.change_supplier and ((decoration.purchase_order_line_id.change_supplier.moq and decoration.purchase_order_line_id.change_supplier.moq != 'not_allowed') or (decoration.purchase_order_line_id.change_supplier.o_moq and decoration.purchase_order_line_id.change_supplier.o_moq != 'not_allowed')):
                        if decoration.purchase_order_line_id.change_supplier.o_ltm_policy_id:
                            if decoration.purchase_order_line_id.change_supplier.o_moq in ['half_column', 'other']:
                                if (decoration.purchase_order_line_id.change_supplier.product_vendor_tier_ids.sorted(key=lambda r:r.min_qty)[0].min_qty) > decoration.purchase_order_line_id.product_qty:
                                    if decoration.purchase_order_line_id.change_supplier.o_setup_cost == 'other':
                                        ltm_policy_purchase_price = decoration.purchase_order_line_id.change_supplier.o_other_setup_cost
                                        ltm_policy_applied = True
                                    if decoration.purchase_order_line_id.change_supplier.o_setup_price == 'other':
                                        ltm_policy_price_unit = decoration.purchase_order_line_id.change_supplier.o_other_setup_price 
                                        ltm_policy_applied = True
                        else:
                            if decoration.purchase_order_line_id.change_supplier.moq in ['half_column', 'other']:
                                if (decoration.purchase_order_line_id.change_supplier.product_vendor_tier_ids.sorted(key=lambda r:r.min_qty)[0].min_qty) > decoration.purchase_order_line_id.product_qty:
                                    if decoration.purchase_order_line_id.change_supplier.setup_cost == 'other':
                                        ltm_policy_purchase_price = decoration.purchase_order_line_id.change_supplier.other_setup_cost
                                        ltm_policy_applied = True
                                    if decoration.purchase_order_line_id.change_supplier.setup_price == 'other':
                                        ltm_policy_price_unit = decoration.purchase_order_line_id.change_supplier.other_setup_price 
                                        ltm_policy_applied = True
                    # LTM Policy Calculation For Set-Up Price End
                    if finisher_decorations or (decoration.finisher and decoration.finisher.finisher_ids):
                        finisher_finishing_types = False
                        if finisher_decorations:
                            finisher_decoration = finisher_decorations[0]
                            finisher_finishing_types = finisher_decoration.product_imprint_ids.search(['&',('product_finisher_id','=',finisher_decoration.id),'&',('decoration_location','=',decoration.imprint_location.id),('decoration_method','=',decoration.imprint_method.id)])
                        pull_charges_from_vendor = False
                        if not finisher_finishing_types:
                            # Pull Charges From Vendor If They Are Not Available At Product
                            if decoration.finisher:
                                pull_charges_from_vendor = True
                                finisher_finishing_types = decoration.finisher.finisher_ids.search(['&',('partner_id','=',decoration.finisher.id),'&',('decoration_location','=',decoration.imprint_location.id),('decoration_method','=',decoration.imprint_method.id)])
                        if not finisher_finishing_types:
                            # Pull Charges From Vendor By Only Cosidering Decoration Method
                            if decoration.finisher and decoration.purchase_order_line_id.product_id and decoration.purchase_order_line_id.product_id.seller_ids:
                                vendor_ids = [seller_id.name and seller_id.name.id for seller_id in decoration.purchase_order_line_id.product_id.seller_ids]
                                if decoration.finisher.id in vendor_ids:
                                    pull_charges_from_vendor = True
                                    finisher_finishing_types = decoration.finisher.finisher_ids.search(['&',('partner_id','=',decoration.finisher.id),('decoration_method','=',decoration.imprint_method.id)]) 
                        if finisher_finishing_types:
                            finisher_finishing_type = finisher_finishing_types[0] 
                            if not ltm_policy_applied:
                                product_setup_sale_price.list_price = finisher_finishing_type.setup_sale_price
                                created_record = self.env['sale.order.line.decoration.group.charge.line'].create(
                                            {'product_id': product_setup_sale_price.id,
                                             'product_uom_qty': len(decoration.stock_color.ids) + len(decoration.pms_code.ids),
                                             'price_unit': finisher_finishing_type.setup_sale_price,
                                             'purchase_price': finisher_finishing_type.setup_fee,
                                             'product_uom': product_setup_sale_price.uom_id.id,
                                             'decoration_group_id': decoration_group_id.id,
                                             'at_least_one_decoration_recalculate': decoration.id,
                                             })
                                if self.get_vendor_attribute_code(decoration.purchase_order_line_id, decoration.purchase_order_line_id.product_id.attribute_value_ids.ids):
                                    created_record.name = decoration_group_id.group_sequence + ' - ' + self.get_vendor_attribute_code(decoration.purchase_order_line_id, decoration.purchase_order_line_id.product_id.attribute_value_ids.ids) + ' - Setup - ' + str(decoration.vendor_decoration_method and decoration.vendor_decoration_method.name or False) +  ' - ' + str(decoration.vendor_decoration_location and decoration.vendor_decoration_location.name or False) +  ' - ' + decoration_name
                                    created_record.name_second = self.get_vendor_attribute_code(decoration.purchase_order_line_id, decoration.purchase_order_line_id.product_id.attribute_value_ids.ids) + ' - Setup - ' + str(decoration.vendor_decoration_location and decoration.vendor_decoration_location.name or False) +  ' - '+ decoration_name
                                else:
                                    created_record.name = decoration_group_id.group_sequence  + ' - Setup - ' + str(decoration.vendor_decoration_method and decoration.vendor_decoration_method.name or False) + ' - ' + str(decoration.vendor_decoration_location and decoration.vendor_decoration_location.name or False) +  ' - '  + decoration_name
                                    created_record.name_second = ' Setup - ' + str(decoration.vendor_decoration_location and decoration.vendor_decoration_location.name or False) + ' - '+ decoration_name
                            else:
                                product_setup_sale_price.list_price = ltm_policy_price_unit
                                created_record = self.env['sale.order.line.decoration.group.charge.line'].create(
                                            {'product_id': product_setup_sale_price.id,
                                             'product_uom_qty': len(decoration.stock_color.ids) + len(decoration.pms_code.ids),
                                             'price_unit': ltm_policy_price_unit,
                                             'purchase_price': ltm_policy_purchase_price,
                                             'product_uom': product_setup_sale_price.uom_id.id,
                                             'decoration_group_id': decoration_group_id.id,
                                             'at_least_one_decoration_recalculate': decoration.id,
                                             })
                                if self.get_vendor_attribute_code(decoration.purchase_order_line_id, decoration.purchase_order_line_id.product_id.attribute_value_ids.ids):
                                    created_record.name = decoration_group_id.group_sequence + ' - ' + self.get_vendor_attribute_code(decoration.purchase_order_line_id, decoration.purchase_order_line_id.product_id.attribute_value_ids.ids) + ' - Setup - ' + str(decoration.vendor_decoration_method and decoration.vendor_decoration_method.name or False) +  ' - ' + str(decoration.vendor_decoration_location and decoration.vendor_decoration_location.name or False) +  ' - ' + decoration_name + ' - ' + 'LTM'
                                    created_record.name_second = self.get_vendor_attribute_code(decoration.purchase_order_line_id, decoration.purchase_order_line_id.product_id.attribute_value_ids.ids) + ' - Setup - ' + str(decoration.vendor_decoration_location and decoration.vendor_decoration_location.name or False) +  ' - '+ decoration_name + ' - ' + 'LTM'
                                else:
                                    created_record.name = decoration_group_id.group_sequence  + ' - Setup - ' + str(decoration.vendor_decoration_method and decoration.vendor_decoration_method.name or False) + ' - ' + str(decoration.vendor_decoration_location and decoration.vendor_decoration_location.name or False) +  ' - ' + decoration_name + ' - ' + 'LTM'
                                    created_record.name_second = ' Setup - ' + str(decoration.vendor_decoration_location and decoration.vendor_decoration_location.name or False) + ' - '+ decoration_name + ' - ' + 'LTM'
                            if finisher_finishing_type.run_charges == 'quantity' and finisher_finishing_type.tier_catalog_qty_ids:
                                tier_catalog_qty_index = False
                                tier_catalog_qty = False
                                for tier_catalog_qty_id in finisher_finishing_type.tier_catalog_qty_ids.sorted(key=lambda r: r.quantity):
                                    if tier_catalog_qty_id.quantity <= total_qty:
                                        tier_catalog_qty_index = list(finisher_finishing_type.tier_catalog_qty_ids.sorted(key=lambda r: r.quantity)).index(tier_catalog_qty_id)
                                if tier_catalog_qty_index:
                                    tier_catalog_qty = finisher_finishing_type.tier_catalog_qty_ids.sorted(key=lambda r: r.quantity)[tier_catalog_qty_index]
                                if tier_catalog_qty:
                                    product_run_charge.list_price = tier_catalog_qty.sale_price
                                    #Need to handle code when quantity_multiplier is zero
                                    if pull_charges_from_vendor:
                                        quantity_multiplier = (len(decoration.stock_color.ids) + len(decoration.pms_code.ids))
                                    else:
                                        quantity_multiplier = (len(decoration.stock_color.ids) + len(decoration.pms_code.ids) - finisher_finishing_type.colors_included)
                                    created_record = self.env['sale.order.line.decoration.group.charge.line'].create(
                                                {'product_id': product_run_charge.id,
                                                 'product_uom_qty': total_qty,
                                                 'run_charge_multiplied': quantity_multiplier if quantity_multiplier > 0 else 1,
                                                 'price_unit': tier_catalog_qty.sale_price if quantity_multiplier > 0 else 0.00,
                                                 'purchase_price': tier_catalog_qty.cost if quantity_multiplier > 0 else 0.00,
                                                 'product_uom': product_run_charge.uom_id.id,
                                                 'decoration_group_id': decoration_group_id.id,
                                                 'at_least_one_decoration_recalculate': decoration.id,
                                                 })
                                    if self.get_vendor_attribute_code(decoration.purchase_order_line_id, decoration.purchase_order_line_id.product_id.attribute_value_ids.ids):
                                        created_record.name = decoration_group_id.group_sequence  + ' - ' + self.get_vendor_attribute_code(decoration.purchase_order_line_id, decoration.purchase_order_line_id.product_id.attribute_value_ids.ids) + ' - Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - ' + str(decoration.vendor_decoration_location and decoration.vendor_decoration_location.name or False) +  ' - ' + decoration_name
                                        created_record.name_second = self.get_vendor_attribute_code(decoration.purchase_order_line_id, decoration.purchase_order_line_id.product_id.attribute_value_ids.ids) + ' - Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - '+ decoration_name
                                    else:
                                        created_record.name = decoration_group_id.group_sequence  + ' - Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) + ' Color - ' + str(decoration.vendor_decoration_location and decoration.vendor_decoration_location.name or False) +  ' - ' + decoration_name
                                        created_record.name_second = ' Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) + ' Color - '+ decoration_name
                                else:
                                    tier_catalog_qty = finisher_finishing_type.tier_catalog_qty_ids.sorted(key=lambda r: r.quantity)[0]
                                    product_run_charge.list_price = tier_catalog_qty and tier_catalog_qty.sale_price or 0.0
                                    #Need to handle code when quantity_multiplier is zero
                                    if pull_charges_from_vendor:
                                        quantity_multiplier = (len(decoration.stock_color.ids) + len(decoration.pms_code.ids))
                                    else:
                                        quantity_multiplier = (len(decoration.stock_color.ids) + len(decoration.pms_code.ids) - finisher_finishing_type.colors_included)
                                    created_record = self.env['sale.order.line.decoration.group.charge.line'].create(
                                                {'product_id': product_run_charge.id,
                                                 'product_uom_qty': total_qty,
                                                 'run_charge_multiplied': quantity_multiplier if quantity_multiplier > 0 else 1,
                                                 'price_unit': tier_catalog_qty and (tier_catalog_qty.sale_price if quantity_multiplier > 0 else 0.00) or 0.0,
                                                 'purchase_price': tier_catalog_qty and (tier_catalog_qty.cost if quantity_multiplier > 0 else 0.00) or 0.0,
                                                 'product_uom': product_run_charge.uom_id.id,
                                                 'decoration_group_id': decoration_group_id.id,
                                                 'at_least_one_decoration_recalculate': decoration.id,
                                                 })
                                    if self.get_vendor_attribute_code(decoration.purchase_order_line_id, decoration.purchase_order_line_id.product_id.attribute_value_ids.ids):
                                        created_record.name = decoration_group_id.group_sequence  + ' - ' + self.get_vendor_attribute_code(decoration.purchase_order_line_id, decoration.purchase_order_line_id.product_id.attribute_value_ids.ids) + ' - Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - ' + str(decoration.vendor_decoration_location and decoration.vendor_decoration_location.name or False) +  ' - ' + decoration_name
                                        created_record.name_second = self.get_vendor_attribute_code(decoration.purchase_order_line_id, decoration.purchase_order_line_id.product_id.attribute_value_ids.ids) + ' - Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - '+ decoration_name
                                    else:
                                        created_record.name = decoration_group_id.group_sequence  + ' - Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - ' + str(decoration.vendor_decoration_location and decoration.vendor_decoration_location.name or False) +  ' - ' + decoration_name
                                        created_record.name_second = ' Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - '+ decoration_name
                            elif finisher_finishing_type.run_charges == 'color' and finisher_finishing_type.tier_catalog_color_ids:
                                tier_catalog_color_index = False
                                tier_catalog_color = False
                                if pull_charges_from_vendor:
                                    quantity_multiplier = (len(decoration.stock_color.ids) + len(decoration.pms_code.ids))
                                else:
                                    quantity_multiplier = (len(decoration.stock_color.ids) + len(decoration.pms_code.ids) - finisher_finishing_type.colors_included)
                                for tier_catalog_color_id in finisher_finishing_type.tier_catalog_color_ids.sorted(key=lambda r: r.quantity):
                                    if pull_charges_from_vendor:
                                        if tier_catalog_color_id.quantity <= total_qty and int(tier_catalog_color_id.color) == (len(decoration.stock_color.ids) + len(decoration.pms_code.ids)):
                                            tier_catalog_color_index = list(finisher_finishing_type.tier_catalog_color_ids.sorted(key=lambda r: r.quantity)).index(tier_catalog_color_id)
                                    else:
                                        if tier_catalog_color_id.quantity <= total_qty and int(tier_catalog_color_id.color) == (len(decoration.stock_color.ids) + len(decoration.pms_code.ids) - finisher_finishing_type.colors_included):
                                            tier_catalog_color_index = list(finisher_finishing_type.tier_catalog_color_ids.sorted(key=lambda r: r.quantity)).index(tier_catalog_color_id)
                                if tier_catalog_color_index:
                                    tier_catalog_color = finisher_finishing_type.tier_catalog_color_ids.sorted(key=lambda r: r.quantity)[tier_catalog_color_index]
                                if tier_catalog_color:
                                    product_run_charge.list_price = tier_catalog_color.sale_price
                                    created_record = self.env['sale.order.line.decoration.group.charge.line'].create(
                                                {'product_id': product_run_charge.id,
                                                 'product_uom_qty': total_qty,
                                                 'run_charge_multiplied': 1,
                                                 'price_unit': tier_catalog_color.sale_price,
                                                 'purchase_price': tier_catalog_color.cost,
                                                 'product_uom': product_run_charge.uom_id.id,
                                                 'decoration_group_id': decoration_group_id.id,
                                                 'at_least_one_decoration_recalculate': decoration.id,
                                                 })
                                    if self.get_vendor_attribute_code(decoration.purchase_order_line_id, decoration.purchase_order_line_id.product_id.attribute_value_ids.ids):
                                        created_record.name = decoration_group_id.group_sequence  + ' - ' + self.get_vendor_attribute_code(decoration.purchase_order_line_id, decoration.purchase_order_line_id.product_id.attribute_value_ids.ids) + ' - Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - ' + str(decoration.vendor_decoration_location and decoration.vendor_decoration_location.name or False) +  ' - ' + decoration_name
                                        created_record.name_second = self.get_vendor_attribute_code(decoration.purchase_order_line_id, decoration.purchase_order_line_id.product_id.attribute_value_ids.ids) + ' - Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - '+ decoration_name
                                    else:
                                        created_record.name = decoration_group_id.group_sequence  + ' - Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - ' + str(decoration.vendor_decoration_location and decoration.vendor_decoration_location.name or False) +  ' - ' + decoration_name
                                        created_record.name_second = ' Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - '+ decoration_name
                                else:
                                    for tier_catalog_color_id in finisher_finishing_type.tier_catalog_color_ids.sorted(key=lambda r: r.quantity):
                                        if pull_charges_from_vendor:
                                            if int(tier_catalog_color_id.color) == (len(decoration.stock_color.ids) + len(decoration.pms_code.ids)):
                                                tier_catalog_color = tier_catalog_color_id
                                                break
                                        else:
                                            if int(tier_catalog_color_id.color) == (len(decoration.stock_color.ids) + len(decoration.pms_code.ids) - finisher_finishing_type.colors_included):
                                                tier_catalog_color = tier_catalog_color_id
                                                break
                                    product_run_charge.list_price = tier_catalog_color and tier_catalog_color.sale_price or 0.0
                                    created_record = self.env['sale.order.line.decoration.group.charge.line'].create(
                                                {'product_id': product_run_charge.id,
                                                 'product_uom_qty': total_qty,
                                                 'run_charge_multiplied': 1,
                                                 'price_unit': tier_catalog_color and tier_catalog_color.sale_price or 0.0,
                                                 'purchase_price': tier_catalog_color and tier_catalog_color.cost or 0.0,
                                                 'product_uom': product_run_charge.uom_id.id,
                                                 'decoration_group_id': decoration_group_id.id,
                                                 'at_least_one_decoration_recalculate': decoration.id,
                                                 })
                                    if self.get_vendor_attribute_code(decoration.purchase_order_line_id, decoration.purchase_order_line_id.product_id.attribute_value_ids.ids):
                                        created_record.name = decoration_group_id.group_sequence  + ' - ' + self.get_vendor_attribute_code(decoration.purchase_order_line_id, decoration.purchase_order_line_id.product_id.attribute_value_ids.ids) + ' - Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - ' + str(decoration.vendor_decoration_location and decoration.vendor_decoration_location.name or False) +  ' - ' + decoration_name
                                        created_record.name_second = self.get_vendor_attribute_code(decoration.purchase_order_line_id, decoration.purchase_order_line_id.product_id.attribute_value_ids.ids) + ' - Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - '+ decoration_name
                                    else:
                                        created_record.name = decoration_group_id.group_sequence  + ' - Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - ' + str(decoration.vendor_decoration_location and decoration.vendor_decoration_location.name or False) +  ' - ' + decoration_name
                                        created_record.name_second = ' Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - '+ decoration_name
                            elif finisher_finishing_type.run_charges == 'teir' and finisher_finishing_type.tier_catalog_name_ids:
                                product_vendor_tier_seq = False
                                if finisher_decoration.finisher_id:
                                    for seller_id in decoration.purchase_order_line_id.product_id.product_tmpl_id.seller_ids:
                                        if seller_id.name.id == finisher_decoration.finisher_id.id:
                                            if seller_id.product_vendor_tier_ids:
                                                for product_vendor_tier_id in seller_id.product_vendor_tier_ids.sorted(key=lambda r: r.min_qty):
                                                    if product_vendor_tier_id.min_qty <= total_qty:
                                                        product_vendor_tier_seq = product_vendor_tier_id.tier_sequence
                                                if not product_vendor_tier_seq:
                                                    product_vendor_tier_seq = seller_id.product_vendor_tier_ids.sorted(key=lambda r: r.min_qty)[0].tier_sequence
                                if product_vendor_tier_seq:
                                    for tier_catalog_name_id in finisher_finishing_type.tier_catalog_name_ids:
                                        if product_vendor_tier_seq == tier_catalog_name_id.tier_sequence:
                                            product_run_charge.list_price = tier_catalog_name_id.sale_price
                                            #Need to handle code when quantity_multiplier is zero
                                            if pull_charges_from_vendor:
                                                quantity_multiplier = (len(decoration.stock_color.ids) + len(decoration.pms_code.ids))
                                            else:
                                                quantity_multiplier = (len(decoration.stock_color.ids) + len(decoration.pms_code.ids) - finisher_finishing_type.colors_included)
                                            created_record = self.env['sale.order.line.decoration.group.charge.line'].create(
                                                        {'product_id': product_run_charge.id,
                                                         'product_uom_qty': total_qty  * (quantity_multiplier if quantity_multiplier > 0 else 1), 
                                                         'run_charge_multiplied': quantity_multiplier if quantity_multiplier > 0 else 1,
                                                         'price_unit': tier_catalog_name_id.sale_price if quantity_multiplier > 0 else 0.00,
                                                         'purchase_price': tier_catalog_name_id.cost if quantity_multiplier > 0 else 0.00,
                                                         'product_uom': product_run_charge.uom_id.id,
                                                         'decoration_group_id': decoration_group_id.id,
                                                         'at_least_one_decoration_recalculate': decoration.id,
                                                         })
                                            if self.get_vendor_attribute_code(decoration.purchase_order_line_id, decoration.purchase_order_line_id.product_id.attribute_value_ids.ids):
                                                created_record.name = decoration_group_id.group_sequence  + ' - ' + self.get_vendor_attribute_code(decoration.purchase_order_line_id, decoration.purchase_order_line_id.product_id.attribute_value_ids.ids) + ' - Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - ' + str(decoration.vendor_decoration_location and decoration.vendor_decoration_location.name or False) +  ' - ' + decoration_name
                                                created_record.name_second = self.get_vendor_attribute_code(decoration.purchase_order_line_id, decoration.purchase_order_line_id.product_id.attribute_value_ids.ids) + ' - Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - '+ decoration_name
                                            else:
                                                created_record.name = decoration_group_id.group_sequence  + ' - Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - ' + str(decoration.vendor_decoration_location and decoration.vendor_decoration_location.name or False) +  ' - ' + decoration_name
                                                created_record.name_second = ' Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - '+ decoration_name
                                else:
                                    #Need to handle code when quantity_multiplier is zero
                                    if pull_charges_from_vendor:
                                        quantity_multiplier = (len(decoration.stock_color.ids) + len(decoration.pms_code.ids))
                                    else:
                                        quantity_multiplier = (len(decoration.stock_color.ids) + len(decoration.pms_code.ids) - finisher_finishing_type.colors_included)
                                    created_record = self.env['sale.order.line.decoration.group.charge.line'].create(
                                                    {'product_id': product_run_charge.id,
                                                     'product_uom_qty': total_qty * (quantity_multiplier if quantity_multiplier > 0 else 1),
                                                     'run_charge_multiplied': quantity_multiplier if quantity_multiplier > 0 else 1,
                                                     'price_unit': 0.0,
                                                     'purchase_price': 0.0,
                                                     'product_uom': product_run_charge.uom_id.id,
                                                     'decoration_group_id': decoration_group_id.id,
                                                     'at_least_one_decoration_recalculate': decoration.id,
                                                     })
                                    if self.get_vendor_attribute_code(decoration.purchase_order_line_id, decoration.purchase_order_line_id.product_id.attribute_value_ids.ids):
                                        created_record.name = decoration_group_id.group_sequence  + ' - ' + self.get_vendor_attribute_code(decoration.purchase_order_line_id, decoration.purchase_order_line_id.product_id.attribute_value_ids.ids) + ' - Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - ' + str(decoration.vendor_decoration_location and decoration.vendor_decoration_location.name or False) +  ' - ' + decoration_name
                                        created_record.name_second = self.get_vendor_attribute_code(decoration.purchase_order_line_id, decoration.purchase_order_line_id.product_id.attribute_value_ids.ids) + ' - Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - '+ decoration_name
                                    else:
                                        created_record.name = decoration_group_id.group_sequence  + ' - Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - ' + str(decoration.vendor_decoration_location and decoration.vendor_decoration_location.name or False) +  ' - ' + decoration_name
                                        created_record.name_second = ' Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - '+ decoration_name
                            else:
                                #Need to handle code when quantity_multiplier is zero
                                if pull_charges_from_vendor:
                                    quantity_multiplier = (len(decoration.stock_color.ids) + len(decoration.pms_code.ids))    
                                else:
                                    quantity_multiplier = (len(decoration.stock_color.ids) + len(decoration.pms_code.ids) - finisher_finishing_type.colors_included)
                                created_record = self.env['sale.order.line.decoration.group.charge.line'].create(
                                                    {'product_id': product_run_charge.id,
                                                     'product_uom_qty': total_qty * (quantity_multiplier if quantity_multiplier > 0 else 1),
                                                     'run_charge_multiplied': quantity_multiplier if quantity_multiplier > 0 else 1,
                                                     'price_unit': 0.0,
                                                     'purchase_price': 0.0,
                                                     'product_uom': product_run_charge.uom_id.id,
                                                     'decoration_group_id': decoration_group_id.id,
                                                     'at_least_one_decoration_recalculate': decoration.id,
                                                     })
                                if self.get_vendor_attribute_code(decoration.purchase_order_line_id, decoration.purchase_order_line_id.product_id.attribute_value_ids.ids):
                                    created_record.name = decoration_group_id.group_sequence  + ' - ' + self.get_vendor_attribute_code(decoration.purchase_order_line_id, decoration.purchase_order_line_id.product_id.attribute_value_ids.ids) + ' - Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - ' + str(decoration.vendor_decoration_location and decoration.vendor_decoration_location.name or False) +  ' - ' + decoration_name
                                    created_record.name_second = self.get_vendor_attribute_code(decoration.purchase_order_line_id, decoration.purchase_order_line_id.product_id.attribute_value_ids.ids) + ' - Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - '+ decoration_name
                                else:
                                    created_record.name = decoration_group_id.group_sequence  + ' - Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - ' + str(decoration.vendor_decoration_location and decoration.vendor_decoration_location.name or False) +  ' - ' + decoration_name
                                    created_record.name_second = ' Run Charges - ' + str(quantity_multiplier if quantity_multiplier > 0 else 0) +  ' Color - '+ decoration_name
                            product_pms_patching.list_price = finisher_finishing_type.pms_matching_sale_price
                            created_record = self.env['sale.order.line.decoration.group.charge.line'].create(
                                        {'product_id': product_pms_patching.id,
                                         'product_uom_qty': len(decoration.pms_code.ids),
                                         'price_unit': finisher_finishing_type.pms_matching_sale_price,
                                         'purchase_price': finisher_finishing_type.pms_matching_fee,
                                         'product_uom': product_pms_patching.uom_id.id,
                                         'decoration_group_id': decoration_group_id.id,
                                         'at_least_one_decoration_recalculate': decoration.id,
                                         })
                            if self.get_vendor_attribute_code(decoration.purchase_order_line_id, decoration.purchase_order_line_id.product_id.attribute_value_ids.ids):
                                created_record.name = decoration_group_id.group_sequence  + ' - ' + self.get_vendor_attribute_code(decoration.purchase_order_line_id, decoration.purchase_order_line_id.product_id.attribute_value_ids.ids) + ' - PMS Matching - ' + str(decoration.vendor_decoration_method and decoration.vendor_decoration_method.name or False) +  ' - ' + str(decoration.vendor_decoration_location and decoration.vendor_decoration_location.name or False) +  ' - ' + decoration_name
                                created_record.name_second = self.get_vendor_attribute_code(decoration.purchase_order_line_id, decoration.purchase_order_line_id.product_id.attribute_value_ids.ids) + ' - PMS Matching - ' + str(decoration.vendor_decoration_location and decoration.vendor_decoration_location.name or False) +  ' - '+ decoration_name
                            else:
                                created_record.name = decoration_group_id.group_sequence  + ' - PMS Matching - ' + str(decoration.vendor_decoration_method and decoration.vendor_decoration_method.name or False) + ' - ' + str(decoration.vendor_decoration_location and decoration.vendor_decoration_location.name or False) +  ' - ' + decoration_name
                                created_record.name_second = ' PMS Matching - ' + str(decoration.vendor_decoration_location and decoration.vendor_decoration_location.name or False) + ' - '+ decoration_name
                        else:
                            if not ltm_policy_applied:
                                self.create_setup_sale_price_and_run_charge_with_zero(decoration, decoration_name, total_qty, len(decoration.stock_color.ids), len(decoration.pms_code.ids), product_setup_sale_price, ltm_policy_applied, 0.00, 0.00, product_run_charge, product_pms_patching, decoration_group_id)
                            else:
                                self.create_setup_sale_price_and_run_charge_with_zero(decoration, decoration_name, total_qty, len(decoration.stock_color.ids), len(decoration.pms_code.ids), product_setup_sale_price, ltm_policy_applied, ltm_policy_price_unit, ltm_policy_purchase_price, product_run_charge, product_pms_patching, decoration_group_id)
                    else:
                        self.create_setup_sale_price_and_run_charge_with_zero(decoration, decoration_name, total_qty, len(decoration.stock_color.ids), len(decoration.pms_code.ids),product_setup_sale_price, ltm_policy_applied, ltm_policy_price_unit, ltm_policy_purchase_price, product_run_charge, product_pms_patching, decoration_group_id)

    @api.onchange('inventory_sent_to_warehouse_flag')
    def _onchange_inventory_sent_to_warehouse_flag(self):
        if self.inventory_sent_to_warehouse_flag:
            self.inventory_sent_to_warehouse_date_stamp = fields.datetime.now()
        else:
            self.inventory_sent_to_warehouse_date_stamp = False

    @api.onchange('inventory_received_to_warehouse_flag')
    def _onchange_inventory_received_to_warehouse_flag(self):
        if self.inventory_received_to_warehouse_flag:
            self.inventory_received_to_warehouse_date_stamp = fields.datetime.now()
        else:
            self.inventory_received_to_warehouse_date_stamp = False

    @api.onchange('inventory_reconciled_to_warehouse_flag')
    def _onchange_inventory_reconciled_to_warehouse_flag(self):
        if self.inventory_reconciled_to_warehouse_flag:
            self.inventory_reconciled_to_warehouse_date_stamp = fields.datetime.now()
        else:
            self.inventory_reconciled_to_warehouse_date_stamp = False

    @api.multi
    def openurl(self):
        ir_config_parameter_obj = self.env['ir.config_parameter']
        base_url = ir_config_parameter_obj.get_param('inventory_url', raise_exception=True)
        vendor = self.partner_id.name
        po_name = self.name
        base_url = base_url.rstrip('/').lstrip(' ')
        url = base_url+'/suppliers_odoo/order_status/'+str(vendor)+'/'+str(po_name)
        return {
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'new', 
        }

    @api.one
    @api.depends('invoice_ids')
    def _get_bill(self):
        """Compute the bill creation date on Purchase order using Vendor bills creation date"""
        if self.invoice_ids:
            balance = 0.0
            date = ''
            for invoice in self.invoice_ids:
                balance += invoice.amount_total
            if self.invoice_count > 0:
                self.invoice_total = balance
                self.difference = self.amount_total - self.invoice_total

    @api.multi
    def sent_to_warehouse(self):
        self.ensure_one()
        ir_model_data = self.env['ir.model.data']
        try:
            template_id = ir_model_data.get_object_reference('pinnacle_purchase', 'email_template_fulfillment_receiving_order')[1]
        except ValueError:
            template_id = False
        try:
            compose_form_id = ir_model_data.get_object_reference('mail', 'email_compose_message_wizard_form')[1]
        except ValueError:
            compose_form_id = False
        ctx = dict()
        ctx.update({
            'default_model': 'purchase.order',
            'default_res_id': self.ids[0],
            'default_use_template': bool(template_id),
            'default_template_id': template_id,
            'default_composition_mode': 'comment',
            'email_template_fulfillment_receiving_order': True,
        })
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }

    @api.one
    def compute_priority_orders(self):
        """Compute Priority Orders and Non Priority order Flag value"""
        status = self.env['purchase.product.status.status'].search([('name', 'in', ['Draft', 'Submitted', 'Processing', 'In Production'])])
        method = self.env['decoration.method'].search([('name', '=', 'Embroidery')], limit=1)
        for po in self:
            priority_order = False
            non_priority_order = False
            priority_trigger_reason = ''
            non_priority_trigger_reason = ''
            if po.sale_order_id:
                po.so_amount_total = po.sale_order_id.amount_total
            # 24 hours after PO and Art Sent and PO not confirmed
            if not po.date_po_confirm:
                if po.state != 'draft':
                    date = str(datetime.date.today() + datetime.timedelta(days=1))
                    if po.po_sent_date or po.art_sent_date:
                        if po.po_sent_date >= date or po.art_sent_date >= date:
                            priority_order = True
                            priority_trigger_reason += '24 hours after PO and Art Sent and PO not confirmed' + ','
                        else:
                            non_priority_order = True
                            non_priority_trigger_reason += 'PO Sent and NOT PO Confirmed (within 24 hours of placement)' + ','
            else:
                non_priority_order = True
                non_priority_trigger_reason += 'PO Sent and PO Confirmed (within 24 hours of placement)' + ','
            
            if po.shipping_lines:
                for line in po.shipping_lines:
                    #Expedited Ship Method
                    if line.service_names and line.service_names not in ['FEDEX_GROUND', 'UPS_Ground']:
                        priority_order = True
                        if 'Expedited Ship Method' not in priority_trigger_reason:
                            priority_trigger_reason += 'Expedited Ship Method' + ','
                    #Firm in hands date marked
                    if line.firm_date:
                        priority_order = True
                        if 'Firm in hands date marked' not in priority_trigger_reason:
                            priority_trigger_reason += 'Firm in hands date marked' + ','
                    #2 days or less prior to shipping/pre-pro shipping
                    if line.ship_date:
                        diff_days = (datetime.datetime.strptime(line.ship_date, '%Y-%m-%d')).date() - datetime.date.today()
                        if diff_days.days <= 2:
                            priority_order = True
                            if '2 days or less prior to shipping/pre-pro shipping' not in priority_trigger_reason:
                                priority_trigger_reason += '2 days or less prior to shipping/pre-pro shipping' + ','
                        else:
                            non_priority_order = True
                            if 'Submission of order date to 2 days prior to shipment' not in priority_trigger_reason:
                                non_priority_trigger_reason += 'Submission of order date to 2 days prior to shipment' + ','
                    if not line.ship_date:
                        priority_order = True
                        if 'Missing Ship Date' not in priority_trigger_reason:
                            priority_trigger_reason += 'Missing Ship Date' + ','
                    if line.ship_date:
                        if line.production_time:
                            diff_days = (datetime.datetime.strptime(line.ship_date, '%Y-%m-%d')).date() - datetime.date.today()
                            if diff_days.days <= 4:
                                priority_order = True
                                if 'Production Time: 24 hours - 3 Days' not in priority_trigger_reason:
                                    priority_trigger_reason += 'Production Time: 24 hours - 3 Days' + ','
                    else:
                        priority_order = True
                        if 'Missing Ship Date' not in priority_trigger_reason:
                            priority_trigger_reason += 'Missing Ship Date' + ','
            #Dollar Amount - $5K
            if po.so_amount_total >= 5000.00:
                priority_order = True
                priority_trigger_reason += 'Dollar Amount - $5K' + ','
            #Certain Vendors - need to be identified
            if po.partner_id.category_id:
                for categ in po.partner_id.category_id:
                    if categ.name == 'Priority':
                        priority_order = True
                        priority_trigger_reason += 'Certain Vendors - need to be identified' + ','
            #Certain Customers - need to be identified
            if po.so_partner_id.category_id:
                for categ in po.partner_id.category_id:
                    if categ.name == 'Priority':
                        priority_order = True
                        priority_trigger_reason += 'Certain Customers - need to be identified' + ','
            #Orders marked with issues
            if po.tag_ids:
                for tag in po.tag_ids:
                    if tag.name == 'Orders with Issues':
                        priority_order = True
                        priority_trigger_reason += 'Orders marked with issues' + ','
            #All pre-pro orders
            if po.proof_required:
                priority_order = True
                priority_trigger_reason += 'All pre-pro orders' + ','
            if priority_order:
                non_priority_order = False
            if po.state in ['done', 'close', 'reopen', 'hold']:
                priority_order = False
                non_priority_order = False
            po.priority_order = priority_order
            po.non_priority_order = priority_order
            po.priority_trigger_reason = priority_trigger_reason
            po.non_priority_trigger_reason = non_priority_trigger_reason
            if po.is_cgp and po.state not in ['draft', 'close', 'done']:
                po.cgp_blank = False
                po.cgp_production = True
            else:
                if po.state not in ['draft', 'close', 'done']:
                    po.cgp_production = False
                    po.cgp_blank = True
                else:
                    po.cgp_production = False
                    po.cgp_blank = False


            if po.product_line_status and po.product_line_status.id in status.ids:
                if method and method.id == po.decoration_method.id and po.tape_complete == False:
                    po.digitization = True
                else:
                    po.digitization =  False    
            else:
                po.digitization =  False
            if po.invoice_total != False and po.invoice_total != po.amount_total:
                po.exception_report = True
            else:
                po.exception_report = False

            po.write({
                'priority_order': priority_order,
                'non_priority_order': non_priority_order,
                'priority_trigger_reason' : priority_trigger_reason,
                'non_priority_trigger_reason' : non_priority_trigger_reason,
                'cgp_blank': po.cgp_blank,
                'cgp_production': po.cgp_production,
                'digitization': po.digitization,
                'exception_report': po.exception_report
            })

    @api.model
    def get_unique_images(self):
        spec_file_exist = []
        spec_file2_exist = []
        unique_file = []
        for lines in self.art_lines:
            group_name =  lines.decoration_group_id
            if lines.spec_file_url and lines.spec_file_name and lines.spec_file_name not in spec_file_exist:
                
                unique_file.append([lines.spec_file_url,('Decoration Group# %s')%(lines.vendor_file_name), False, group_name])
                spec_file_exist.append(lines.spec_file_name)
            if lines.spec_file2_url and lines.spec_file2_name and lines.spec_file2_name not in spec_file2_exist:
                unique_file.append([ lines.spec_file2_url, ('Decoration Group# %s')%(lines.vendor_file_name), True, group_name])
                spec_file2_exist.append(lines.spec_file2_name)
        return unique_file

    @api.model
    def get_image_from_cfs(self, url, image_tag=False):
        cfs_url = str(self.env['ir.config_parameter'].get_param('cfs.web.url'))
        if cfs_url:
            url = cfs_url+url
        def unicodifier(val):
            if val is None or val is False:
                return u''
            if isinstance(val, str):
                return val.decode('utf-8')
            return unicode(val)
        try:
            req = urllib2.urlopen(url, timeout=50)
            image = Image.open(cStringIO.StringIO(req.read()))
            image.load()
        except Exception:
            _logger.exception("Failed to load remote image %r", url)
            return None
        out = cStringIO.StringIO()
        image.save(out, image.format)
        if not image_tag:
            return out.getvalue().encode('base64')
        else:
            return unicodifier('<img style="max-height: 30cm;max-width: 15cm;" src="data:%s;base64,%s">' % (Image.MIME[image.format], out.getvalue().encode('base64')))

    @api.one
    @api.depends('for_decoration', 'for_blank', 'rush_finisher', 'rush_supplier')
    def _compute_art_rfq_button_enable(self):
        self.art_button_enable = False
        if self.partner_id.art_email_enable and ((self.for_decoration and self.for_blank) or (self.for_decoration and not self.for_blank)):
            self.art_button_enable = True

    def get_report_name(self, fulfillment=False):
        self.ensure_one()
        if self.po_type in ['sample', 'non_billable']:
            return 'Sample Order '
        if fulfillment:
            return 'Fulfillment Receiving Order '
        else:
            return 'Purchase Order '

    def get_unique_vendor_url(self):
        vendor_file_exist = []
        result = []
        for lines in self.art_lines:
            if lines.vendor_file_url and lines.vendor_file_name and lines.vendor_file_name not in vendor_file_exist:
                cfs_url = str(self.env['ir.config_parameter'].get_param('cfs.web.url'))
                if cfs_url:
                    url = cfs_url+lines.vendor_file_url+'?dl=1'
                else:
                    url = ''
                vendor_file_exist.append(lines.vendor_file_name)
                result.append(url)
        return result

    def get_template_name(self):
        supplier_email = self.rush_supplier.email or self.partner_id.order_email
        finisher_email = self.rush_finisher.email or self.partner_id.art_email or self.partner_id.order_email
        template_name = 'email_template_edi_purchase'
        module = 'purchase'
        if self.po_type in ['sample', 'non_billable']:
            return 'pinnacle_purchase', 'email_template_for_order_sample_pinnacle'

        if self.env.context.get('order_mail'):
            module = 'pinnacle_purchase'
            if self.for_blank and not self.for_decoration:
                template_name =  'email_template_for_order_blank_pinnacle'
            elif self.env.context.get('art_button_enable'):
                template_name = 'email_template_for_order_pinnacle' 
            else:
                template_name = 'email_template_for_order_art_pinnacle'
        elif self.env.context.get('mail_art'):
            module = 'pinnacle_purchase'
            template_name = 'email_template_for_art_pinnacle_only'
        return module, template_name

    @api.multi
    def rediret_cgp_po(self):
        ir_config_parameter_obj = self.env['ir.config_parameter']
        url = ir_config_parameter_obj.get_param('purchase.cgp.url', raise_exception=True)
        return {
                  'name'     : 'CGP Purchase Order',
                  'res_model': 'ir.actions.act_url',
                 'type'     : 'ir.actions.act_url',
                  'target'   : 'new',
                  'url'      : ("%s%s") % (url, self.id)
              }

    @api.multi
    def action_pinnacle_rfq_send(self): 
        '''
        This function opens a window to compose an email, with the edi purchase template message loaded by default
        '''
        self.ensure_one()

        ir_model_data = self.env['ir.model.data']
        try:
            module, template_name =  self.get_template_name()
            template_id = ir_model_data.get_object_reference(module, template_name)[1]
        except ValueError:
            template_id = False
        try:
            compose_form_id = ir_model_data.get_object_reference('mail', 'email_compose_message_wizard_form')[1]
        except ValueError:
            compose_form_id = False
        ctx = dict(self.env.context or {})
        ctx.update({
            'default_model': 'purchase.order',
            'default_res_id': self.ids[0],
            'default_use_template': bool(template_id),
            'default_template_id': template_id,
            'default_composition_mode': 'comment',
            'order_mail': self.env.context.has_key('order_mail'),
            'mail_art': self.env.context.has_key('mail_art'),
            'art_button_enable': self.art_button_enable,
            'email_address_custom': True,
        })
        return {
            'name': _('Compose Email'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }
    def _get_email_send_to(self, only_email=None):
        if self.po_type in ['non_billable', 'sample']:
            supplier_email = self.rush_supplier.email or self.partner_id.sample_email or self.partner_id.order_email or self.partner_id.email
        else:
            supplier_email = self.rush_supplier.email or self.partner_id.order_email or self.partner_id.email
        finisher_email = self.rush_finisher.email or self.partner_id.art_email or self.partner_id.order_email or self.partner_id.email

        if only_email:
            return [supplier_email, finisher_email]
        rush_supplier_name = self.rush_supplier.name or self.partner_id.name
        rush_finisher_name = self.rush_finisher.name or self.partner_id.name

        if self.env.context.get('order_mail'):
            return ('%s <%s>' % (rush_supplier_name ,supplier_email)), self.partner_id.id
        elif self.env.context.get('mail_art'):
            return ('%s <%s>' % (rush_finisher_name ,finisher_email)), self.partner_id.id
    @api.multi
    def add_purchase_order_line(self):
        self.ensure_one()
        new_context = dict(self.env.context).copy()
        new_context.update({'purchase_order': self.id or False})
        form_view_id = self.env.ref('pinnacle_purchase.purchase_order_wizard_change_form_add')
        return {'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'target': 'new',
                'view_id': form_view_id.id,
                'res_model': 'purchase.order.wizard.change.add',
                'name': 'Add Purchase Order Line',
                'context': new_context,
                }
    @api.multi
    def edit_purchase_order_line(self):
        self.ensure_one()
        active_ids = []
        service = False
        for po in self.order_line:
            if po.select:
                active_ids.append(po.id)
                if not service and po.product_id.type == 'service':
                    service = True
        self.clear_all_selection_for_purchase_order()
        new_context = dict(self.env.context).copy()
        new_context.update({'purchase_order': self.id or False,
                            'active_ids': active_ids})
        form_view_id = self.env.ref('pinnacle_purchase.purchase_order_wizard_change_form')
        if service:
            form_view_id = self.env.ref('pinnacle_purchase.purchase_order_wizard_change_form_service')
        return {'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'target': 'new',
                'view_id': form_view_id.id,
                'res_model': 'purchase.order.wizard.change',
                'name': 'Edit Purchase Order Line',
                'context': new_context,
                }
    @api.multi
    def action_cancel(self):
        for order in self:
            if not (order.state in ['draft', 'ipo_to_approve', 'ipo_approved', 'ipo_rejected', 'to approve', 'sent', 'purchase'] or self.env.context.get('super_admin_delete_sale_order', False)):
                raise UserError(_('A purchase order (#%s) cannot be cancelled once it has been shipped.') % order.name)
        status = self.env.ref('pinnacle_purchase.purchase_product_status_status_cancel')
        for psl in self.product_status_line:
            psl.product_status_status = status.id
        for pdg in self.purchase_decoration_groups:
            pdg.product_status_status = status.id
        self.button_cancel()
        return True

    @api.multi
    def action_hold(self):
        for order in self:
            order.last_state = order.state
            order.state = 'hold'
        return True

    def create_note(self, vals):
        fields = self.fields_get()
        new_note = ""
        for val in vals:
            if not fields.has_key(val):continue
            if fields[val]['type'] not in ['one2many']:
                if fields[val]['type'] in ['many2one']:
                    old_value = False
                    if self.read([val]):
                        old_value = self.read([val])[0][val]
                    new_value = vals[val]
                    if old_value:
                        old_value = old_value[1]
                    model = self.env[fields[val]['relation']].browse(new_value)
                    new_value = model.name_get()
                    if new_value:
                        new_value = new_value[0][1]
                    else:
                        new_value = False
                else:
                    old_value = False
                    if self.read([val]):
                        old_value = self.read([val])[0][val]
                    new_value = vals[val]
                if new_value != old_value:
                    new_note += "In PO, <b>%s: </b>"%(fields[val]['string'])
                    new_note += "\n<span >Change to <b>%s</b>, Previously it was <b>%s</b>.</span> </br>" % (new_value , old_value)
        return new_note


    def generate_seqence_from_so(self, sale_order):
        number = self.search_count([('sale_order_id', '=', sale_order.id)])
        return'%s-%s' % (sale_order.name.replace('SO', 'PO'), list(string.ascii_uppercase)[number])

    @api.model
    def create(self, vals):
        if vals.get('sale_order_id', False):
            sale_order = self.env['sale.order'].browse([vals['sale_order_id']])
            if sale_order:
                vals['so_partner_id'] = sale_order.partner_id.id
                vals['name'] = self.generate_seqence_from_so(sale_order)
        else:
            seq = self.env['ir.sequence'].next_by_code('sale.purchase.order')
            vals['name'] ='%s%s' % ('PO', seq)

        vals['date_order'] = fields.Datetime.now()
        res = super(PurchaseOrder, self).create(vals)
        # Internal Purchase Order
        if res and vals.get('po_type', False) == 'internal_purchase_order':
            res.write({'name': 'I' + res.name, 'purchase_order_name_updated': True})
        followers = []
        if vals.get('ass_account_manager', False):
            for ass_account_manager in res.ass_account_manager:
                followers.append(ass_account_manager.partner_id.id)
        if vals.get('account_manager', False):
            followers.append(res.account_manager.partner_id.id)
        if vals.get('soc_id', False):
            followers.append(res.soc_id.partner_id.id)
        res.message_subscribe(followers)
        return res

    @api.multi
    def write(self, vals):
        if vals.get('sale_order_id', False):
            sale_order = self.env['sale.order'].browse([vals['sale_order_id']])
            if sale_order:
                vals['so_partner_id'] = sale_order.partner_id.id
           
        if vals.has_key('sale_order_id') and not self.env.context.get('no_recur'):
            for this in self:
                if not vals.get('sale_order_id', False):
                    # name = self.env['ir.sequence'].next_by_code('sale.purchase.order').replace('SO-', '')
                    seq = self.env['ir.sequence'].next_by_code('sale.purchase.order')
                    name ='%s%s' % ('PO', seq)
                else:
                    sale_order = self.env['sale.order'].browse([vals['sale_order_id']])
                    name = self.generate_seqence_from_so(sale_order)
                this.with_context(no_recur=True).write({'name': name, 'purchase_order_name_updated': False})

        state_dic = {}
        for rec in self:
            state_dic[rec.id] = rec.state
        # note = self.create_note(vals)
        if vals.get('decoration_line', False) and vals.get('order_line', False):
            del vals['decoration_line']
        if vals.get('decorations', False):
            del vals['decorations']
        result = super(PurchaseOrder, self).write(vals)
        # Internal Purchase Order
        if result and vals.has_key('po_type'):
            for record in self:
                if record.po_type == 'internal_purchase_order':
                    if record.name and not record.purchase_order_name_updated:
                        record.write({'name': 'I' + record.name, 'purchase_order_name_updated': True})
                else:
                    if record.name:
                        record.write({'name': record.name[1:], 'purchase_order_name_updated': False})
                    else:
                        record.write({'purchase_order_name_updated': False})
        if result and vals.get('name', False):
            for record in self:
                if record.po_type == 'internal_purchase_order':
                    if record.name and not record.purchase_order_name_updated:
                        record.write({'name': 'I' + record.name, 'purchase_order_name_updated': True})
                else:
                    record.write({'purchase_order_name_updated': False})
        # if note:
        #     if not self.env.context.get('skip_log'):
        allow_pdf_generate = self.env['ir.values'].get_default('base.config.settings', 'pdf_generate_enable')

        if vals.get('state') in ['sent', 'purchase', 'close']:
            self.move_product_status_line_to_submit(vals.get('state'))

        for this in self:
            if vals.get('state'):
                new_state = this.state
                if allow_pdf_generate:
                    report_name = 'Purchase Order ' + this.name
                    result, format = self.env['report'].sudo().get_pdf(
                        this._ids, 'purchase.report_purchaseorder'), 'pdf'
                    ext = "." + format
                    if not report_name.endswith(ext):
                        report_name += ext
                    attach_ids = [(report_name, result)]
                else:
                    attach_ids = []
                old_state = po_dict.get(state_dic.get(this.id, {}), {})
                new_state = po_dict.get(new_state)
                if old_state == new_state:
                    continue
                body = '<b>Status</b> change from <b>%s</b> to <b>%s</b>. <br/>' % (old_state, new_state)
                this.message_post(body=body, attachments=attach_ids, message_type='comment')
            # Start : Auto shipping & delivering sale order based on purchase order
            if vals.get('state', False): 
                for this in self:
                    if this.sale_order_id.is_shipped_order and this.sale_order_id.state not in ['invoiced', 'delivered', 'close', 'reopen', 'done']:
                        this.sale_order_id.state = 'shipped'
                    if this.sale_order_id.is_delivered_order and this.sale_order_id.state not in ['close', 'reopen', 'done']:
                        this.sale_order_id.state = 'delivered'
                        this.sale_order_id.sale_order_delivery_date = fields.datetime.now()
            # End : Auto shipping & delivering sale order based on purchase order 
        if result and (vals.get('order_line', False) or vals.get('product_receives', False)):
            order_lines = []
            if vals.get('order_line', False):
                order_lines = vals['order_line']
            if vals.get('product_receives', False):
                order_lines = vals['product_receives']
            for order_line in order_lines:
                if order_line[0] == 1:
                    if 'product_qty' in order_line[2]:
                        purchase_order_line_obj = self.env['purchase.order.line']
                        if purchase_order_line_obj.browse(order_line[1]) and purchase_order_line_obj.browse(order_line[1]).exists():
                            for decoration in purchase_order_line_obj.browse(order_line[1]).decorations:
                                if decoration.decoration_group_id:
                                    if decoration.decoration_group_id.charge_lines.exists():
                                        decoration.decoration_group_id.charge_lines.exists().unlink()
        return result


    def move_product_status_line_to_submit(self, state):
        states = {
            'sent': self.env.ref('pinnacle_purchase.purchase_product_status_status_submitted').id or False,
            'purchase': self.env.ref('pinnacle_purchase.purchase_product_status_status_confirm').id or False,
            'close': self.env.ref('pinnacle_purchase.purchase_product_status_status_close').id or False,
        }
        if states.get(state):
            for order in self:
                order.product_status_line.write({'product_status_status': states.get(state)})
                order.purchase_decoration_groups.write({'product_status_status': states.get(state)})

    @api.onchange('partner_id')
    def onchange_partner_id_notes(self):
        if self.partner_id:
            addr = self.partner_id.address_get(['invoice'])
            values = {
                'vendor_note': self.partner_id.po_notes,
                'partner_invoice_id': addr['invoice'],
            }
            self.update(values)
        else:
            self.vendor_note = ''

    def onchange_order_line_notes(self, variant):
        seller = variant.product_tmpl_id._fetch_seller(self.partner_id.id)
        product_sku = variant.product_tmpl_id.fetch_vendor_product_sku(self.partner_id,variant) or ' '
        notes = ''
        if self.product_note:
            notes = self.product_note
        self.product_note = ('%s \n %s %s') % (notes, product_sku, seller.po_notes or ' ')

    @api.multi
    def print_quotation(self):
        self.write({'state': "sent"})
        for order in self:
            for product_status_line in order.product_status_line:
                product_purchase_product_status_status_submitted = self.env.ref('pinnacle_purchase.purchase_product_status_status_submitted')
                if not product_purchase_product_status_status_submitted:
                    raise UserError(_('Purchase Status Status Submitted Product Not Found'))
                else:
                    product_status_line.product_status_status = product_purchase_product_status_status_submitted.id
            for purchase_decoration_group in order.purchase_decoration_groups:
                if purchase_decoration_group.product_status_status.id == product_purchase_product_status_status_draft.id:
                    purchase_decoration_group.product_status_status = product_purchase_product_status_status_submitted.id
        return self.env['report'].get_action(self, 'purchase.report_purchasequotation')

    @api.depends('state', 'order_line.qty_invoiced', 'order_line.product_qty')
    def _get_invoiced(self):
        precision = self.env['decimal.precision'].precision_get('Product Unit of Measure')
        for order in self:
            if order.state not in ('purchase', 'done', 'deliver'):
                order.invoice_status = 'no'
                continue

            if any(float_compare(line.qty_invoiced, line.product_qty, precision_digits=precision) == -1 for line in order.order_line):
                order.invoice_status = 'to invoice'
            elif all(float_compare(line.qty_invoiced, line.product_qty, precision_digits=precision) >= 0 for line in order.order_line):
                order.invoice_status = 'invoiced'
            else:
                order.invoice_status = 'no'

    @api.multi
    def _track_subtype(self, init_values):
        self.ensure_one()
        if 'state' in init_values and self.state == 'purchase':
            return 'purchase.mt_rfq_approved'
        elif 'state' in init_values and self.state == 'to approve':
            return 'purchase.mt_rfq_confirmed'
        elif 'state' in init_values and self.state == 'deliver':
            return 'pinnacle_purchase.mt_rfq_deliver'
        elif 'state' in init_values and self.state == 'done':
            return 'purchase.mt_rfq_done'
        return super(PurchaseOrder, self)._track_subtype(init_values)

    @api.multi
    def button_approve(self, force=False):
        if self.company_id.po_double_validation == 'two_step'\
          and self.amount_total >= self.env.user.company_id.currency_id.compute(self.company_id.po_double_validation_amount, self.currency_id)\
          and not self.user_has_groups('purchase.group_purchase_manager'):
            raise UserError(_('You need purchase manager access rights to validate an order above %.2f %s.') % (self.company_id.po_double_validation_amount, self.company_id.currency_id.name))
        self.write({'state': 'purchase'})
        self._create_picking()
        return {}

    @api.multi
    def move_to_deliver(self):
        product_purchase_product_status_status_delivered = self.env.ref('pinnacle_purchase.purchase_product_status_status_delivered')
        if not product_purchase_product_status_status_delivered:
            raise UserError(_('Purchase Status Status Delivered Product Not Found'))
        for order in self:
            for product_status_line in order.product_status_line:
                product_status_line.product_status_status = product_purchase_product_status_status_delivered.id
            for purchase_decoration_group in order.purchase_decoration_groups:
                purchase_decoration_group.product_status_status = product_purchase_product_status_status_delivered.id
            if not order.shipped_date:
                order.shipped_date = fields.Datetime.now()
            if not order.delivered_date:
                order.delivered_date = fields.Datetime.now()
        self.write({'state': 'deliver'})

    @api.multi
    def move_to_close(self):
        for order in self:
            if order.program_type == 'inventory' and not order.inventory_reconciled_to_warehouse_flag:
                raise UserError(_("This purchase order has Inventory as the Program Type so it must be marked Reconciled at Warehouse before it can be closed."))
        self.write({'state': 'close', 'close_date': fields.Datetime.now()})

    @api.multi
    def move_to_reopen(self):
        for order in self:
            if order.sale_order_id:
                order.reopen_date = fields.Datetime.now()
                order.sale_order_id.set_to_reopen()
                order.sale_order_id.state = 'reopen'
        self.write({'state': 'reopen', 'reopen_date': fields.Datetime.now()})

    @api.multi
    def button_done(self):
        for order in self:
            if order.sale_order_id:
                order.reclose_date = fields.Datetime.now()
                order.sale_order_id.action_done()
                order.sale_order_id.state = 'done'
        self.write({'state': 'done', 'reclose_date': fields.Datetime.now()})

    @api.one
    @api.constrains('po_type')
    def _check_po_type(self):
        if self.po_type == 'sample':
            for this in self.order_line:
                if not this.product_id.product_tmpl_id.sample_ok:
                    raise UserError(_('This item cannot be ordered as a sample.'))
    @api.model
    def default_get(self, fields):
        res = super(PurchaseOrder,self).default_get(fields)
        drop_ship_id = self.env.ref('stock_dropshipping.picking_type_dropship')
        res['picking_type_id'] = drop_ship_id.id
        return res

    @api.multi
    def create_shipping_line(self):
        shipping_line = {}
        # if self.po_type != 'non_billable':
        #     return
        if self.for_decoration and not self.for_blank:
            if self.product_receives:
                for order_line in self.product_receives:
                    supplier_id = self.env['product.supplierinfo'].search([('name', '=', self.partner_id.id), 
                                        ('product_tmpl_id', '=', order_line.product_id.product_tmpl_id.id)], limit=1)
                    if order_line.product_id.product_tmpl_id.id in shipping_line:
                        shipping_line[order_line.product_id.product_tmpl_id.id]['product_id'].append(order_line.product_id.id)
                        shipping_line[order_line.product_id.product_tmpl_id.id]['qty'] += order_line.product_qty                                                            
                        shipping_line[order_line.product_id.product_tmpl_id.id]['supplier'] = supplier_id.id
                        shipping_line[order_line.product_id.product_tmpl_id.id]['partner_supplier'] = supplier_id.name.id
                        #shipping_line[order_line.product_id.product_tmpl_id.id]['order_lines'].append(order_line.id)
                        shipping_line[order_line.product_id.product_tmpl_id.id]['contact'] = self.so_partner_id.id
                    else:
                        shipping_line[order_line.product_id.product_tmpl_id.id] = {'product_id': [order_line.product_id.id],
                                                                                   'qty': order_line.product_qty,
                                                                                   'supplier': supplier_id.id,
                                                                                   'partner_supplier': supplier_id.name.id,
                                                                                   #'order_lines':[order_line.id],
                                                                                   'contact': self.so_partner_id.id
                                                                                  }
        else:
            if self.order_line:
                for order_line in self.order_line:
                    if not order_line.product_id.type == 'service':
                        supplier_id = self.env['product.supplierinfo'].search([('name', '=', self.partner_id.id), 
                                        ('product_tmpl_id', '=', order_line.product_id.product_tmpl_id.id)], limit=1)
                        if order_line.product_id.product_tmpl_id.id in shipping_line:
                            shipping_line[order_line.product_id.product_tmpl_id.id]['product_id'].append(order_line.product_id.id)
                            shipping_line[order_line.product_id.product_tmpl_id.id]['qty'] += order_line.product_qty                                                            
                            shipping_line[order_line.product_id.product_tmpl_id.id]['supplier'] = supplier_id.id
                            shipping_line[order_line.product_id.product_tmpl_id.id]['partner_supplier'] = supplier_id.name.id
                            shipping_line[order_line.product_id.product_tmpl_id.id]['order_lines'].append(order_line.id)
                            shipping_line[order_line.product_id.product_tmpl_id.id]['contact'] = self.so_partner_id.id
                        else:
                            shipping_line[order_line.product_id.product_tmpl_id.id] = {'product_id': [order_line.product_id.id],
                                                                                       'qty': order_line.product_qty,
                                                                                       'supplier': supplier_id.id,
                                                                                       'partner_supplier': supplier_id.name.id,
                                                                                       'order_lines':[order_line.id],
                                                                                       'contact': self.so_partner_id.id
                                                                                      }
        if shipping_line:
            self.generate_shipping_lines(shipping_line)


    @api.multi
    def generate_shipping_lines(self, shipping_line):
        if self.shipping_lines:
            shipping_lines = [line.tmpl_id.id for line in self.env[
            'shipping.line'].search([('purchase_order_id', '=', self.id)])]
            if shipping_lines:
                for k, v in shipping_line.iteritems():
                    if int(k) not in shipping_lines:
                        shipping = self.env['shipping.line'].create({'product_qty': v['qty'], 'supplier': v['supplier'], 'contact': v['contact'],
                                                                     'purchase_order_id': self.id, 'tmpl_id': int(k), 'partner_supplier': v['partner_supplier']})
                        shipping.onchange_contact()
                        shipping.onchange_carton_dimension()
                        for pro in v['product_id']:
                            self.env['product.shipping.line'].create(
                                {'product': pro, 'shipping_id': shipping.id})
                    else:
                        lines = self.env['shipping.line'].search(
                            [('tmpl_id', '=', int(k)), ('purchase_order_id', '=', self.id)])
                        if lines:
                            for line in lines:
                                shipping_lines.remove(int(k))
                                shipping = line.write({'product_qty': v['qty'], 'supplier': v['supplier'], 'contact': v['contact'],
                                                       'purchase_order_id': self.id, 'tmpl_id': int(k), 'partner_supplier': v['partner_supplier']})
                                line.product_line.unlink()
                                for pro in v['product_id']:
                                    line.product_line.create(
                                        {'product': pro, 'shipping_id': line.id})
            else:
                for k, v in shipping_line.iteritems():
                    shipping = self.env['shipping.line'].create({'product_qty': v['qty'], 'supplier': v['supplier'], 'contact': v['contact'],
                                                                'purchase_order_id': self.id, 'tmpl_id': int(k), 'partner_supplier': v['partner_supplier']})
                    shipping.onchange_contact()
                    shipping.onchange_carton_dimension()
                    for pro in v['product_id']:
                        self.env['product.shipping.line'].create(
                            {'product': pro, 'shipping_id': shipping.id})
        else:
            for k, v in shipping_line.iteritems():
                shipping = self.env['shipping.line'].create({'product_qty': v['qty'], 'supplier': v['supplier'], 'contact': v['contact'],
                                                             'purchase_order_id': self.id, 'tmpl_id': int(k), 'partner_supplier': v['partner_supplier']})
                shipping.onchange_contact()
                shipping.onchange_carton_dimension()
                for pro in v['product_id']:
                    self.env['product.shipping.line'].create(
                        {'product': pro, 'shipping_id': shipping.id})

    @api.multi
    def button_confirm(self):
        for order in self:
            if order.state not in ['draft', 'sent']:
                continue
            order.button_approve(force=True)
            order.date_po_confirm = fields.Datetime.now()
        return True

    @api.depends('picking_ids', 'picking_ids.state')
    def _compute_is_shipped(self):
        #Need to confirm all picking or stop creating of picking
        for order in self:
            order.is_shipped = True
    @api.one
    def remove_hold(self):
        self.state = self.last_state

    def create_blank_order_line(self):
        product_id = self.env.ref('pinnacle_sales.product_product_blank_order')
        if product_id:
            self.env['purchase.order.line'].create({
                                            'product_id': product_id.id, 
                                            'name': ' ',
                                            'product_qty': 1,
                                            'product_uom': product_id.uom_id.id,
                                            'blank_line': True,
                                            'order_id': self.id,
                                            'price_unit': 0.0,
                                           })

    @api.multi
    def action_pinnacle_vendor_send(self): 
        '''
        This function opens a window to compose an email, with the edi purchase template message loaded by default
        '''
        self.ensure_one()
        ir_model_data = self.env['ir.model.data']
        try:
            template_id = ir_model_data.get_object_reference('pinnacle_purchase', 'mail_purchase_order_vendor')[1]
        except ValueError:
            template_id = False
        try:
            compose_form_id = ir_model_data.get_object_reference('pinnacle_purchase', 'email_compose_message_wizard_form_ext')[1]
        except ValueError:
            compose_form_id = False
        partner_ids = []
        result = self.env['res.partner'].search([('parent_id', '=', self.partner_id.id)])
        if result:
            partner_ids = [res.id for res in result]
        partner_ids.append(self.partner_id.id)
        ctx = dict(self.env.context or {})
        ctx.update({
            'default_model': 'purchase.order',
            'default_res_id': self.ids[0],
            'default_use_template': bool(template_id),
            'default_template_id': template_id,
            'default_composition_mode': 'comment',
            'show_email': False,
            'pin_partner_ids': partner_ids
        })
        return {
            'name': _('Compose Email'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }

    @api.multi
    def add_decoration(self):
        try:
            self.ensure_one()
            context_active_ids = []
            can_decorate = True
            for purchase_order_line in self.decoration_line:
                if purchase_order_line.select_decoration_line:
                    if not purchase_order_line.added_decoration and not purchase_order_line.is_decoration_disable:
                        context_active_ids.append(purchase_order_line.id)
                        if not purchase_order_line.product_id.can_decorate or purchase_order_line.product_id.type == 'service':
                            can_decorate = False
                    else:
                        raise UserError(_('To add decoration, please select lines with no decoration. To edit or add a decoration to a product, please click Edit Decoration.'))
            self.clear_all_selection_for_purchase_order()
            if not can_decorate:
                raise UserError(_('Please select products that can be' +
                                  ' decorated and that are not type of' +
                                  ' service.'))
            elif len(context_active_ids) >= 1 and self:
                new_context = dict(self.env.context).copy()
                new_context.update({'active_id': context_active_ids[0],
                                    'active_ids': context_active_ids,
                                    'purchase_order_lines': context_active_ids,
                                    'order_id': self.id,
                                    'readonly_by_pass': True})
                form_view_id = self.env.ref(
                    'pinnacle_purchase.purchase_order_add_decoration_form_view')
                return {'type': 'ir.actions.act_window',
                        'view_mode': 'form',
                        'target': 'new',
                        'view_id': form_view_id.id,
                        'res_model': 'purchase.order.add.decoration',
                        'name': 'Add Purchase Order Line Decoration',
                        'context': new_context,
                        }
            else:
                raise UserError(_('Please select a line to add decoration.'))
        finally:
            self.clear_all_selection_for_purchase_order()

    @api.multi
    def edit_decoration(self):
        try:
            self.ensure_one()
            context_active_ids = []
            can_decorate = True
            for purchase_order_line in self.decoration_line:
                if purchase_order_line.select_decoration_line:
                    if purchase_order_line.added_decoration or purchase_order_line.is_decoration_disable:
                        context_active_ids.append(purchase_order_line.id)
                        if not purchase_order_line.product_id.can_decorate or purchase_order_line.product_id.type == 'service':
                            can_decorate = False
                    else:
                        raise UserError(_('To edit decoration, please select lines that have already been decorated.'))
            self.clear_all_selection_for_purchase_order()
            if not can_decorate:
                raise UserError(_('Please select products that can be' +
                                  ' decorated and that are not type of' +
                                  ' service.'))
            elif len(context_active_ids) > 0:
                new_context = dict(self.env.context).copy()
                new_context.update({'active_id': context_active_ids[0],
                                    'active_ids': context_active_ids,
                                    'purchase_order_lines': context_active_ids,
                                    'order_id': self.id,
                                    'readonly_by_pass': True})
                if len(context_active_ids) == 1:
                    self.env['purchase.order.edit.decoration'].search([]).unlink()
                    purchase_order_line = self.env['purchase.order.line']. browse(context_active_ids)
                    form_view_id = self.env.ref(
                        'pinnacle_purchase.purchase_order_edit_decoration_form_view')
                    new_context.update({'is_decoration_disable': purchase_order_line.is_decoration_disable})
                    return {'type': 'ir.actions.act_window',
                            'view_mode': 'form',
                            'target': 'new', 'view_id': form_view_id.id,
                            'res_model': 'purchase.order.edit.decoration',
                            'name': 'Edit Purchase Order Line Decoration',
                            'context': new_context,
                            }
                else:
                    decoration_lines_same = False
                    purchase_order_lines = self.env['purchase.order.line']. browse(context_active_ids)
                    is_decoration_disable_same = True
                    for purchase_order_line in purchase_order_lines:
                        if purchase_order_lines[0].is_decoration_disable != purchase_order_line.is_decoration_disable:
                            is_decoration_disable_same = False
                    for purchase_order_line in purchase_order_lines:
                        decoration_lines_same = self.env['purchase.order.line'].browse(context_active_ids)[0].decorations.decoration_compare(purchase_order_line.decorations)
                    if not decoration_lines_same:
                        raise UserError(_('Please select all editable ' +
                                          'lines with similar ' +
                                          'decorations to edit their\'s' +
                                          ' decorations.'))
                    form_view_id = self.env.ref(
                        'pinnacle_purchase.' +
                        'purchase_order_edit_multi_decoration_form_view')
                    new_context.update({'is_decoration_disable': purchase_order_lines[0].is_decoration_disable})
                    return {'type': 'ir.actions.act_window',
                            'view_mode': 'form',
                            'target': 'new',
                            'view_id': form_view_id.id,
                            'res_model':
                            'purchase.order.edit.multi.decoration',
                            'name':
                            'Edit Multi Purchase Order Lines Decoration',
                            'context': new_context,
                            }
            else:
                raise UserError(_('Please select at least one line with' +
                                  ' any decoration to edit it\'s decoration.'))
        finally:
            self.clear_all_selection_for_purchase_order()

    @api.multi
    def _get_amount(self, field_name):
        blank_product_id = self.env.ref('pinnacle_sales.product_product_blank_shipment_line')
        for order in self:
            amount_untaxed = amount_tax = 0.0
            for line in order.order_line:
                if not (blank_product_id and blank_product_id.id == line.product_id.id) and line.shipping_id and not line.shipping_id.vendor_account:
                    continue
                amount_untaxed += line.price_subtotal
                # FORWARDPORT UP TO 10.0
                if order.company_id.tax_calculation_rounding_method == 'round_globally':
                    taxes = line.taxes_id.compute_all(line.price_unit, line.order_id.currency_id, line.product_qty, product=line.product_id, partner=line.order_id.partner_id)
                    amount_tax += sum(t.get('amount', 0.0) for t in taxes.get('taxes', []))
                else:
                    amount_tax += line.price_tax

            #task 16202
            amount_untaxed = order.currency_id.round(amount_untaxed)
            amount_tax = order.currency_id.round(amount_tax)

            if field_name == 'untaxed':
                return order.currency_id.symbol+' '+ '%.2f' % amount_untaxed
            if field_name == 'tax':
                return order.currency_id.symbol+' '+ '%.2f' % amount_tax
            if field_name == 'total':
                return order.currency_id.symbol+' '+ '%.2f' % (amount_untaxed + amount_tax)

    @api.multi
    def get_untaxed_amount(self):
        untaxed_amount = self._get_amount('untaxed')
        return untaxed_amount

    @api.multi
    def get_tax_amount(self):
        tax_amount = self._get_amount('tax')
        return tax_amount

    @api.multi
    def get_total_amount(self):
        total_amount = self._get_amount('total')
        return total_amount

    @api.multi
    def get_shipping_lines(self):
        shipping_lines = {}
        selected_lines = []
        def check_shipping_lines(ship_line):
            match_lines = []
            for this in self.shipping_lines:
                if this.id == ship_line.id and  this.id not in selected_lines:
                    match_lines.append(this)
                    selected_lines.append(this.id)
                    continue
                if this.firm_date != ship_line.firm_date:
                    continue
                if this.event_date != ship_line.event_date:
                    continue
                if this.state_id.id != ship_line.state_id.id:
                    continue
                if this.contact_country_id.id != ship_line.contact_country_id.id:
                    continue
                if this.ship_date != ship_line.ship_date:
                    continue
                if this.delivery_date != ship_line.delivery_date:
                    continue
                if this.delivery_method.id != ship_line.delivery_method.id:
                    continue

                if isinstance(this.address_name, basestring) and isinstance(ship_line.address_name, basestring) and (this.address_name.strip() != ship_line.address_name.strip()):
                    continue
                elif this.address_name != ship_line.address_name:
                    continue

                if isinstance(this.street1, basestring) and isinstance(ship_line.street1, basestring) and (this.street1.strip() != ship_line.street1.strip()):
                    continue
                elif this.street1 != ship_line.street1:
                    continue

                if isinstance(this.city, basestring) and isinstance(ship_line.city, basestring) and (this.city.strip() != ship_line.city.strip()):
                    continue
                elif this.city != ship_line.city:
                    continue

                if isinstance(this.street2, basestring) and isinstance(ship_line.street2, basestring) and (this.street2.strip() != ship_line.street2.strip()):
                    continue
                elif this.street2 != ship_line.street2:
                    continue

                if isinstance(this.zipcode, basestring) and isinstance(ship_line.zipcode, basestring) and (this.zipcode.strip() != ship_line.zipcode.strip()):
                    continue
                elif this.zipcode != ship_line.zipcode:
                    continue

                if isinstance(this.other_contact_info, basestring) and isinstance(ship_line.other_contact_info, basestring) and (this.other_contact_info.strip() != ship_line.other_contact_info.strip()):
                    continue
                elif this.other_contact_info != ship_line.other_contact_info:
                    continue

                if this.service_names != ship_line.service_names:
                    continue
                if this.id not in selected_lines:
                    match_lines.append(this)
                    selected_lines.append(this.id)
            return match_lines

        for ship_line in self.shipping_lines:
            shipping_lines[ship_line] = check_shipping_lines(ship_line)
        return shipping_lines

    @api.multi
    def set_for_ipo_approval(self):
        self.ensure_one()
        mail_template = self.env.ref('pinnacle_purchase.internal_purchase_order_template_for_approval_notification')
        if self.send_for_ipo_approval and mail_template:
            partner_to = ''
            for user in self.send_for_ipo_approval.users:
                if not partner_to:
                    if user.partner_id.email:
                        partner_to = str(user.partner_id.id)
                        continue
                if partner_to and user.partner_id.email:
                    partner_to = partner_to + ',' + str(user.partner_id.id)
            ipo_approval_url = str(self.env['ir.config_parameter'].search([('key', '=', 'web.base.url')]).value) + '/web#id=' + str(self.id) + '&view_type=form&model=purchase.order&action=' + str(self.env.ref('pinnacle_purchase.purchase_rfq_pinnacle_all').id) + '&menu_id='
            mail_id = mail_template.with_context(ipo_approval_url = ipo_approval_url, partner_to = partner_to).send_mail(self.id)
            body = "<p><b>The User (%s) is Requesting For Internal Purchase Order Approval:- #%s </b></p>" % (str(self.env.user.name), str(self.name))
            self.message_post(body=body, attachments=[], message_type='comment')
            self.env['mail.mail'].browse([mail_id]).send()
            self.state = 'ipo_to_approve'
            self.ipo_approved_or_rejected_by = False
            return True
        return False

    @api.one
    @api.depends('amount_total', 'state', 'po_type')
    def _compute_send_for_ipo_approval(self):
        self.send_for_ipo_approval = False
        if self.state in ['draft', 'ipo_to_approve'] and self.po_type == 'internal_purchase_order':
            slab = self.env['internal.purchase.order.approval.slab'].search(['&', ('lower_limit', '<=', self.amount_total), ('upper_limit', '>=', self.amount_total)], limit=1)            
            if slab:
                self.send_for_ipo_approval = slab.id

    @api.multi
    def approve_ipo(self): 
        self.ensure_one()
        if self.env.user.id in self.send_for_ipo_approval.users.ids:
            self.ipo_approved_or_rejected_by = self.env.user.id
            body = "<p><b>Internal Purchase Order (%s) is approved by (%s) </b></p>" % (self.name, self.env.user.name)
            self.message_post(body=body, attachments=[], message_type='comment')
            self.state = 'ipo_approved'
        else:
            raise UserError(_('You do not have the authority to approve this Internal Purchase Order.'))
        return True

    @api.multi
    def reject_ipo(self):
        self.ensure_one()
        if self.env.user.id in self.send_for_ipo_approval.users.ids:
            self.ipo_approved_or_rejected_by = self.env.user.id
            body = "<p><b>Internal Purchase Order (%s) is rejected by (%s) </b></p>" % (self.name, self.env.user.name)
            self.message_post(body=body, attachments=[], message_type='comment')
            self.state = 'ipo_rejected'
        else:
            raise UserError(_('You do not have the authority to reject this Internal Purchase Order.'))
        return True

    @api.multi
    def button_draft(self):
        self.write({'state': 'draft', 'ipo_approved_or_rejected_by': False})
        return {}

class PurchaseOrderLine(models.Model):
    _inherit = 'purchase.order.line' 
    _order = 'order_id, sequence, id'

    decoration_charge_group_id = fields.Many2one('sale.order.line.decoration.group.charge.line', string="Decoration Group", copy=False)
    sale_order_line_id = fields.Many2one('sale.order.line', readonly=True, string="Sale Order Line")
    price_unit = fields.Float(string='Cost Price', required=True, digits=dp.get_precision('Vendor Level Cost'), default=0)
    product_sku = fields.Char(string="Product ID")
    product_variant_name = fields.Char(string="Variant")
    product_id = fields.Many2one('product.product', string='Product', domain=[('purchase_ok', '=', True)], change_default=True, required=False)
    date_planned = fields.Datetime(string='Scheduled Date', required=False, index=True)
    po_line_type = fields.Selection(compute="_compute_po_line_type", selection=[('regular', 'Regular'), ('sample', 'Sample'), ('non_billable', 'Non Billable Sample'), ('internal_purchase_order','Internal Purchase Order')])
    sale_order_id = fields.Many2one(related='order_id.sale_order_id', relation='sale.order', string='Sale Order')
    state = fields.Selection(related="order_id.state")
    select = fields.Boolean("select", default=False, copy=False)
    select_decoration_line = fields.Boolean(string="Select Decoration Line", default=False, copy=False)
    shipping_id = fields.Many2one('shipping.line')
    blank_line = fields.Boolean()
    product_type =  fields.Selection(related="product_id.type", store=True) 
    is_decoration_disable = fields.Boolean(string="Blank Product")
    decorations = fields.One2many('sale.order.line.decoration', 'purchase_order_line_id',string='Decorations')
    decoration_group_sequences = fields.Many2many('sale.order.line.decoration.group', compute="_compute_decoration_group_sequences",string='Decoration Groups', readonly=True, copy=False)
    added_decoration = fields.Boolean(
        compute="_compute_added_decoration",
        string="Added Decoration ?",
        store=True)
    finisher_id = fields.Many2one('res.partner', string="Finisher") 
    shipping_id = fields.Many2one('shipping.line',  default=False, copy=True)
    art_line_id = fields.Many2one('art.production.line', 'Art Worksheet Line', copy=False)
    change_supplier = fields.Many2one('product.supplierinfo', string="Supplier")
    is_override = fields.Boolean(string="Lock")

    #False: Not to show under decoration tab. True: To show under decoration tab
    can_decorate = fields.Boolean(string="Can Decorated")
    #False: To show under order line tab. True: To show under recieving tab
    incoming = fields.Boolean('Is Imporiting', default=False)

    #Product recieve tab fields
    product_tmpl_id = fields.Many2one('product.template', string="Product Name", domain=[('purchase_ok', '=', True)])
    blank_provider = fields.Many2one("res.partner", string="Blank Provider")
    blank_supplier = fields.Many2one("product.supplierinfo")
    date_expected = fields.Date('Expected Date')
    supplier_required = fields.Boolean(compute="_compute_supplier_required", store=True)
    blank_purchase_orders = fields.Char(compute="_compute_blank_purchase_orders", string="Blank POs", store=True, help="Contains the corresponding Blank POs name")

    @api.one
    @api.depends('sale_order_id', 'sale_order_id.purchase_orders', 'blank_provider')
    def _compute_blank_purchase_orders(self):
        self.blank_purchase_orders = ''
        if self.sale_order_id and self.sale_order_id.purchase_orders:
            self.blank_purchase_orders = ','.join([purchase_order.name for purchase_order in self.sale_order_id.purchase_orders if purchase_order.name != '' and purchase_order.id != self.order_id.id and purchase_order.for_blank == True and purchase_order.partner_id.id == self.blank_provider.id])

    def to_show_line_in_report(self):
        self.ensure_one()
        if self.shipping_id and self.shipping_id.vendor_account:
            return True
        product_id = self.env.ref('pinnacle_sales.product_product_blank_shipment_line')
        if product_id and product_id.id == self.product_id.id:
            return True
        return False

    @api.onchange('blank_provider')
    def _onchange_blank_provider(self):
        self.can_decorate = True
        self.incoming = True
        supplier = self.env['product.supplierinfo']
        new_list = supplier.search([('name', '=', self.blank_provider.id), ('product_tmpl_id', '=', self.product_tmpl_id.id)], limit=1)
        if new_list:
            self.blank_supplier = new_list[0].id

    @api.onchange('product_id', 'product_tmpl_id')
    def _onchange_domain_vendor_available(self):
        supplier = self.env['product.supplierinfo']
        template = False
        ids = []
        preferred = False
        if self.product_tmpl_id:
            template = self.env['product.template'].browse([self.product_tmpl_id.id])
        if template and self.product_id:
            ids, preferred =  template.get_supplier(self.product_id)
        if not self.blank_provider.id:
            if preferred in ids:
                self.blank_provider = supplier.browse(preferred).name.id
                self.blank_supplier = preferred
        elif self.blank_provider.id not in ids:
            self.blank_provider = False

        name_id = []
        for x in supplier.search([('id', 'in', ids)]):
            name_id.append(x.name.id)
        return {'domain': {
            'blank_provider': [('id', 'in', name_id)],
        }}

    @api.depends('product_tmpl_id')
    def _compute_supplier_required(self):
        for this in self:
            if not this.product_tmpl_id.seller_ids:
                this.supplier_required = False
            else:
                this.supplier_required = True

    def get_group_sequence(self, group):
        group_sequence = [grp.group_sequence for grp in group]
        sequence = ", ".join(group_sequence)
        return sequence

    @api.multi
    def view_decoration(self):
        form_view_id = self.env.ref('pinnacle_sales.view_order_line_form_view')
        new_context = self.env.context.copy()
        new_context['invisible_po_line'] = True
        if self:
            return {'type': 'ir.actions.act_window',
                    'view_mode': 'form',
                    'name': "Decoration Lines",
                    'target': 'new',
                    'view_id': form_view_id.id,
                    'res_model': 'purchase.order.line',
                    'res_id': self[0].id,
                    'context': new_context
                    }


    @api.one
    @api.depends('decorations')
    def _compute_added_decoration(self):
        self.added_decoration = False
        if self.decorations:
            self.added_decoration = True

    @api.one
    @api.depends('decorations.decoration_group_id')
    def _compute_decoration_group_sequences(self):
        decoration_ids = []
        for decoration in self.decorations:
            if decoration.decoration_group_id:
                decoration_ids.append(decoration.decoration_group_id.id)
        decoration_ids = list(set(decoration_ids))
        self.decoration_group_sequences = [[6, False, decoration_ids]]
    @api.one
    @api.depends('order_id.po_type')
    def _compute_po_line_type(self):
        self.po_line_type = self.order_id.po_type

    @api.depends('order_id.state', 'move_ids.state')
    def _compute_qty_received(self):
        for line in self:
            if line.order_id.state not in ['purchase', 'deliver', 'done']:
                line.qty_received = 0.0
                continue
            if line.product_id.type not in ['consu', 'product']:
                line.qty_received = line.product_qty
                continue
            total = 0.0
            for move in line.move_ids:
                if move.state == 'done':
                    if move.product_uom != line.product_uom:
                        total += move.product_uom._compute_quantity(move.product_uom_qty, line.product_uom)
                    else:
                        total += move.product_uom_qty
            line.qty_received = total

    @api.multi
    def unlink(self):
        self = self.exists()
        for line in self: 
            if line.order_id.state in ['deliver', 'done']:
                raise UserError(_('Cannot delete a purchase order line which is in state \'%s\'.') %(line.state,))
            for proc in line.procurement_ids:
                #Task 18156
                proc.message_post(body=_('Purchase order line deleted.'), message_type='comment')
            line.procurement_ids.filtered(lambda r: r.state != 'cancel').write({'state': 'exception'})
        decoration_groups = []
        for record in self:
            if record.decoration_charge_group_id:
                record.decoration_charge_group_id.added_to_order_lines = 'NO'
            for decoration in record.decorations:
                if decoration.decoration_group_id:
                    decoration_groups.append(decoration.decoration_group_id.id)
        result = models.Model.unlink(self)
        if not result:
            for record in self:
                if record.decoration_charge_group_id:
                    record.decoration_charge_group_id.added_to_order_lines = 'YES'
        if result:
            decoration_group_deleted = []
            for decoration_group  in self.env['sale.order.line.decoration.group'].browse(decoration_groups):
                if len(decoration_group.decorations) < 1:
                    decoration_group_deleted.append(decoration_group.id)
                else:
                    if decoration_group.charge_lines:
                        decoration_group.charge_lines.unlink()
            if decoration_group_deleted:
                self.env['sale.order.line.decoration.group'].browse(list(set(decoration_group_deleted))).unlink()
        return result

    def create_note(self, vals):
        fields = self.fields_get()
        new_note = ""
        black_field_list = ['select', 'name', 'product_variant_name', 'spec_file', 'spec_file_att', 'vendor_file','vendor_file_att', 'spec_file2', 'spec_file2_att', 'select_art_line']
        for val in vals:
            if not fields.has_key(val) or val in black_field_list:continue
            if fields[val]['type'] not in ['one2many']:
                if fields[val]['type'] in ['many2one']:
                    old_value = self.read([val])[0][val]
                    new_value = vals[val]
                    if old_value:
                        old_value = old_value[1]
                    model = self.env[fields[val]['relation']].browse(new_value)
                    new_value = model.name_get()
                    if new_value:
                        new_value = new_value[0][1]
                    else:
                        new_value = False
                else:
                    old_value = self.read([val])[0][val]
                    new_value = vals[val]
                new_note += "In PO Line <b>%s: </b>"%(fields[val]['string'])
                new_note += "\n<span >Change to <b>%s</b>, Previously it was <b>%s</b>.</span> </br>" % (new_value , old_value)
        return new_note

    @api.multi
    def write(self, vals):
        self = self.exists()
        note = self.create_note(vals)
        result = super(PurchaseOrderLine, self).write(vals)
        if note:
            if not self.env.context.get('skip_log'):
                for this in self:
                    this.order_id.message_post(body=note, attachments=[], message_type='comment')
        if result:
            for record in self:
                if record.decoration_charge_group_id:
                    if 'name' in vals:
                        record.decoration_charge_group_id.name = vals['name']
                    if 'product_qty' in vals:
                        record.decoration_charge_group_id.product_uom_qty = vals['product_qty']
                    if 'price_unit' in vals:
                        record.decoration_charge_group_id.purchase_price = vals['price_unit']
        return result

    @api.model
    def create(self, vals):
        record = super(PurchaseOrderLine, self).create(vals)
        if record and record.order_id:
            if record.order_id.so_partner_id and not record.order_id.sale_order_id:
                analytic_tag_ids = []
                if record.order_id.so_partner_id.parent_id:
                    analytic_tag_ids = record.order_id.so_partner_id.analytic_contact_tag_ids.ids
                else:
                    analytic_tag_ids = record.order_id.so_partner_id.analytic_tag_ids.ids
                record.analytic_tag_ids = [[6, 0, analytic_tag_ids]]
            if record.order_id.sale_order_id and record.order_id.auto_generate == False:
                record.analytic_tag_ids = [[6, 0, record.order_id.sale_order_id.analytic_tag_ids.ids]]
        return record

class PurchaseProductStatusStatus(models.Model):
    _name = 'purchase.product.status.status'

    name = fields.Char('Status Name')

    @api.multi
    def write(self, vals):
        '''
        to aviod inverse functional updating
        '''
        return False

class ProductTrackingNumbers(models.Model):
    _name = 'product.tracking.numbers'

    delivery_carrier = fields.Many2one('delivery.carrier','Delivery Method', required="True")
    delivery_carrier_url = fields.Char(related="delivery_carrier.tracking_url", string="Base URL", readonly=True)
    tracking_number = fields.Char(string="Tracking Number")
    tracking_number_url = fields.Char(string="Tracking URL")
    generated_tracking_number_url = fields.Char(compute="_compute_generated_tracking_number_urll",string="Tracking URL", store=True)
    purchase_order_line_for_tracking_id = fields.Many2one('purchase.order.line.for.tracking', string="Purchase Order Tracking Line")
    purchase_group_id = fields.Many2one('purchase.decoration.group.for.tracking', string='Purchase Order Tracking Decoration Group')

    @api.model
    def default_get(self, fields):
        res = super(ProductTrackingNumbers,self).default_get(fields)
        active_id = self.env.context.get('active_id')
        if not res.get('delivery_carrier') and self.env.context.get('active_id'):
            res['delivery_carrier'] = self.env['purchase.order.line.for.tracking'].browse(active_id).shipping_id.delivery_method.id
        return res

    @api.one
    @api.depends('delivery_carrier_url', 'tracking_number', 'tracking_number_url')
    def _compute_generated_tracking_number_urll(self):
        self.generated_tracking_number_url = ''
        if self.delivery_carrier_url and self.tracking_number:
            self.generated_tracking_number_url = self.delivery_carrier_url + self.tracking_number
        if not self.delivery_carrier_url:
            if self.tracking_number_url:
                self.generated_tracking_number_url = self.tracking_number_url

    def _clean_tracking_number_url(self, tracking_number_url):
        (scheme, netloc, path, params, query, fragment) = urlparse.urlparse(tracking_number_url)
        if not scheme:
            if not netloc:
                netloc, path = path, ''
            tracking_number_url = urlparse.urlunparse(('http', netloc, path, params, query, fragment))
        return tracking_number_url

    @api.multi
    def write(self, vals):
        if vals.get('tracking_number_url'):
            vals['tracking_number_url'] = self._clean_tracking_number_url(vals['tracking_number_url'])
        result = super(ProductTrackingNumbers, self).write(vals)
        return result

    @api.model
    def create(self, vals):
        if vals.get('tracking_number_url'):
            vals['tracking_number_url'] = self._clean_tracking_number_url(vals['tracking_number_url'])
        result = super(ProductTrackingNumbers, self).create(vals)
        return result

class PurchaseOrderLineForTracking(models.Model): 
    _name = 'purchase.order.line.for.tracking'
    _inherit = ['mail.thread', 'ir.needaction_mixin']

    @api.depends('purchase_order_line_id', 'purchase_order_id')
    def _compute_shipping_id(self):
        for this in self:
            shipping_line = self.env['shipping.line'].search([('purchase_order_id', '=', this.purchase_order_id.id),
                                              ('tmpl_id', '=', this.purchase_order_line_id.product_id.product_tmpl_id.id)])
            if shipping_line:
                this.shipping_id = shipping_line[0].id

    select = fields.Boolean('Select', default=False)
    purchase_order_id = fields.Many2one('purchase.order', string='Purchase Order')
    purchase_order_line_id = fields.Many2one('purchase.order.line', ondelete="cascade", string='Purchase Order Line')
    name = fields.Text(string="Description")
    product_sku = fields.Char(string="Product ID")
    product_variant_name = fields.Char(string="Variant")
    product_status_status = fields.Many2one('purchase.product.status.status', string="Status")
    product_status_status_name = fields.Char(related="product_status_status.name", store=True)
    product_tracking_lines = fields.One2many('product.tracking.numbers', 'purchase_order_line_for_tracking_id', string='Tracking Lines')
    added_at_leat_one_tracking_line = fields.Boolean(compute="_compute_added_at_leat_one_tracking_line", string="Is Added At Least One Tracking Line ?", store=True)
    shipping_id = fields.Many2one('shipping.line', compute='_compute_shipping_id')
    auto_generate = fields.Boolean(default=False)
    qty = fields.Integer('Qty')
    for_decoration = fields.Boolean(related='purchase_order_id.for_decoration')

    @api.model
    def create(self, vals):
        
        product_purchase_product_status_status_draft = self.env.ref('pinnacle_purchase.purchase_product_status_status_draft_new')
        if not product_purchase_product_status_status_draft:
            raise UserError(_('Purchase Status Status Draft Product Not Found'))
        else:
            vals['product_status_status'] = product_purchase_product_status_status_draft.id
            #vals['qty'] = self.env['purchase.order.line'].browse(vals['purchase_order_line_id']).product_qty
        result = super(PurchaseOrderLineForTracking, self).create(vals)
        return result

    @api.multi
    def duplicate_line(self):
        res = self.copy()
        res.product_status_status = self.product_status_status
        return {
                    'type': 'ir.actions.client',
                    'tag': 'reload',
                }

    @api.one
    @api.depends('product_tracking_lines')
    def _compute_added_at_leat_one_tracking_line(self):
        self.added_at_leat_one_tracking_line = False
        if self.product_tracking_lines:
            self.added_at_leat_one_tracking_line = True

    @api.multi
    def add_or_edit_tracking_numbers(self):
        form_view_id = self.env.ref('pinnacle_purchase.purchase_order_line_for_tracking_form')
        if self:
            return {'type': 'ir.actions.act_window',
                    'view_mode': 'form',
                    'name': "Product/Service Tracking Lines",
                    'target': 'new',
                    'view_id': form_view_id.id,
                    'res_model': 'purchase.order.line.for.tracking',
                    'res_id': self[0].id,
                     'flags': {'form': {'action_buttons': False}}
                    }

    @api.multi
    def track_all_numbers(self):
        form_view_id = self.env.ref('pinnacle_purchase.purchase_order_line_for_tracking_form_for_view_tracking')
        if self:
            return {'type': 'ir.actions.act_window',
                    'view_mode': 'form',
                    'name': "Product/Service Tracking Lines",
                    'target': 'new',
                    'view_id': form_view_id.id,
                    'res_model': 'purchase.order.line.for.tracking',
                    'res_id': self[0].id,
                    }
    @api.multi
    def write(self, vals):
        if vals.get('product_status_status'):
            status_dict = {}
            for rec in self:
                status_dict[rec.id] = rec.product_status_status_name
        result = super(PurchaseOrderLineForTracking, self).write(vals)
        ship = self.env.ref('pinnacle_purchase.purchase_product_status_status_shipped')
        transit = self.env.ref('pinnacle_purchase.purchase_product_status_status_in_transit')
        deliver = self.env.ref('pinnacle_purchase.purchase_product_status_status_delivered')
        black_list = [ship.id, transit.id, deliver.id]

        for this in self:
            if vals.get('product_status_status'):
                variant = ", ".join([v.attribute_id.name + ': ' +
                                     v.name for v in
                                     this.purchase_order_line_id.product_id.attribute_value_ids.
                                    sorted(key=lambda r: r.
                                           attribute_id)])
                vendor_sku = this.purchase_order_id.get_vendor_attribute_code(this.purchase_order_line_id,
                                                                              this.purchase_order_line_id.product_id.attribute_value_ids.ids)
                body = '<b>Product Status: </b>SKU: %s %s %s Status changed from %s to %s.' % (
                    vendor_sku, variant, this.name, status_dict.get(this.id), this.product_status_status_name)
                this.purchase_order_id.message_post(body=body, message_type='comment')
            check = True
            for line in this.purchase_order_id.product_status_line:
                if line.product_status_status.id not in black_list:
                    check = False
                    break
            if check:
                # Start : Updating PO Shipped Datetimes In SO
                if this.purchase_order_id.sale_order_id:
                    if not this.purchase_order_id.sale_order_id.first_po_shipped:
                        this.purchase_order_id.sale_order_id.first_po_shipped = fields.datetime.now()
                        this.purchase_order_id.sale_order_id.most_recent_po_shipped = fields.datetime.now()
                    else:
                        this.purchase_order_id.sale_order_id.most_recent_po_shipped = fields.datetime.now()
                # End : Updating PO Shipped Datetimes In SO
            if check:
                if not this.purchase_order_id.shipped_date:
                    this.purchase_order_id.shipped_date = fields.Datetime.now()
                this.purchase_order_id.state = 'shipped'
            # Making Automatic Deliver State Of Purchase Order
            delivered = True
            for line in this.purchase_order_id.product_status_line:
                if line.product_status_status.id != deliver.id:
                    delivered = False
                    break
            if delivered:
                if not this.purchase_order_id.shipped_date:
                    this.purchase_order_id.shipped_date = fields.Datetime.now()
                if not this.purchase_order_id.delivered_date:
                    this.purchase_order_id.delivered_date = fields.Datetime.now()
                this.purchase_order_id.state = 'deliver'
        return result


    @api.multi
    def send_tracking_mail(self):
        self.ensure_one()
        ir_model_data = self.env['ir.model.data']
        try:
            template_id = ir_model_data.get_object_reference('pinnacle_purchase', 'mail_customer_tracking_numbers')[1]
        except ValueError:
            template_id = False
        try:
            compose_form_id = ir_model_data.get_object_reference('mail', 'email_compose_message_wizard_form')[1]
        except ValueError:
            compose_form_id = False
        ctx = dict(self.env.context or {})
        
        #id_group = ir_model_data.get_object_reference('account', 'group_account_manager')[1]

        ctx.update({
            'default_model': 'purchase.order.line.for.tracking',
            'default_res_id': self.ids[0],
            'default_use_template': bool(template_id),
            'default_template_id': template_id,
            'default_composition_mode': 'comment',
            'default_partner_id': self.purchase_order_id.partner_id.id
        })
        return {
            'name': _('Compose Email'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }


class PurchaseDecorationGroupForTracking(models.Model): 
    _name = 'purchase.decoration.group.for.tracking'
    _inherit = ['mail.thread', 'ir.needaction_mixin']

    @api.depends('purchase_order_id')
    def _compute_subject_line(self):
        for this in self:
            subject = 'Tracking Information for your'
            templates = [line.product_id.product_tmpl_id for line in this.purchase_order_id.product_receives]
            templates = list(set(templates))
            if templates:
                if len(templates) == 1:
                    this.subject_line = "Tracking Information for Your "+templates[0].name
                if len(templates) > 1:
                    this.subject_line = "We are happy to inform you that your order has shipped"

    @api.depends('group_id')
    def _compute_shipping_ids(self): 
        for this in self:
            data = this.env['sale.order.line.decoration'].search([('purchase_order_id', '=', this.purchase_order_id.id)])
            tmpl_ids = [d.purchase_order_line_id.product_id.product_tmpl_id.id for d in data]
            shipping_lines = this.env['shipping.line'].search([('purchase_order_id', '=', this.purchase_order_id.id), ('tmpl_id', 'in', tmpl_ids)])
            this.shipping_ids = [[6, 0, shipping_lines.ids]]

    select = fields.Boolean('Select', default=False)
    group_sequence = fields.Char(string='Decoration Group', readonly=True)
    group_id = fields.Char(string='Decoration Group ID')
    product_status_status = fields.Many2one('purchase.product.status.status', string="Status")
    product_status_status_name = fields.Char(related="product_status_status.name", store=True)
    product_tracking_lines = fields.One2many('product.tracking.numbers', 'purchase_group_id', string='Tracking Lines')
    added_at_leat_one_tracking_line = fields.Boolean(compute="_compute_added_at_leat_one_tracking_line", string="Is Added At Least One Tracking Line ?", store=True)
    purchase_order_id = fields.Many2one('purchase.order', string='Purchase Order')
    shipping_ids = fields.One2many('shipping.line', 'group_id', compute='_compute_shipping_ids')
    subject_line = fields.Char(compute='_compute_subject_line')
    decoration_group_id = fields.Many2one('sale.order.line.decoration.group', ondelete="cascade")
    auto_generate = fields.Boolean(default=False)
    qty = fields.Integer('Qty')

    @api.multi
    def duplicate_line(self):
        res = self.copy()
        res.product_status_status = self.product_status_status
        return {
                    'type': 'ir.actions.client',
                    'tag': 'reload',
                }
                
    @api.model
    def create(self, vals):
        product_purchase_product_status_status_draft = self.env.ref('pinnacle_purchase.purchase_product_status_status_draft_new')
        if not product_purchase_product_status_status_draft:
            raise UserError(_('Purchase Status Status Draft Product Not Found'))
        else:
            vals['product_status_status'] = product_purchase_product_status_status_draft.id
        result = super(PurchaseDecorationGroupForTracking, self).create(vals)
        return result

    @api.one
    @api.depends('product_tracking_lines')
    def _compute_added_at_leat_one_tracking_line(self):
        self.added_at_leat_one_tracking_line = False
        if self.product_tracking_lines:
            self.added_at_leat_one_tracking_line = True

    @api.multi
    def add_or_edit_tracking_numbers(self):
        form_view_id = self.env.ref('pinnacle_purchase.purchase_decoration_group_for_tracking_form')
        ctx = dict(self.env.context or {})
        ctx.update({'active_id': self[0].id})
        if self:
            return {'type': 'ir.actions.act_window',
                    'view_mode': 'form',
                    'name': "Decoration Group Tracking Lines",
                    'target': 'new',
                    'view_id': form_view_id.id,
                    'res_model': 'purchase.decoration.group.for.tracking',
                    'res_id': self[0].id,
                    'context': ctx
                    }

    @api.multi
    def track_all_numbers(self):
        form_view_id = self.env.ref('pinnacle_purchase.purchase_decoration_group_for_tracking_form_for_view_tracking')
        if self:
            return {'type': 'ir.actions.act_window',
                    'view_mode': 'form',
                    'name': "Decoration Group Tracking Lines",
                    'target': 'new',
                    'view_id': form_view_id.id,
                    'res_model': 'purchase.decoration.group.for.tracking',
                    'res_id': self[0].id,
                    }
    @api.multi
    def write(self, vals):
        result = super(PurchaseDecorationGroupForTracking, self).write(vals)
        ship = self.env.ref('pinnacle_purchase.purchase_product_status_status_shipped')
        transit = self.env.ref('pinnacle_purchase.purchase_product_status_status_in_transit')
        deliver = self.env.ref('pinnacle_purchase.purchase_product_status_status_delivered')
        black_list = [ship.id, transit.id, deliver.id]

        for this in self:
            check = True
            for line in this.purchase_order_id.purchase_decoration_groups:
                if line.product_status_status.id not in black_list:
                    check = False
                    break
            if check:
                # Start : Updating PO Shipped Datetimes In SO
                if this.purchase_order_id.sale_order_id:
                    if not this.purchase_order_id.sale_order_id.first_po_shipped:
                        this.purchase_order_id.sale_order_id.first_po_shipped = fields.datetime.now()
                        this.purchase_order_id.sale_order_id.most_recent_po_shipped = fields.datetime.now()
                    else:
                        this.purchase_order_id.sale_order_id.most_recent_po_shipped = fields.datetime.now()
                # End : Updating PO Shipped Datetimes In SO
            if check:
                if not this.purchase_order_id.shipped_date:
                    this.purchase_order_id.shipped_date = fields.Datetime.now()
                this.purchase_order_id.state = 'shipped'
            # Making Automatic Deliver State Of Purchase Order
            delivered = True
            for line in this.purchase_order_id.purchase_decoration_groups:
                if line.product_status_status.id != deliver.id:
                    delivered = False
                    break
            if delivered:
                if not this.purchase_order_id.shipped_date:
                    this.purchase_order_id.shipped_date = fields.Datetime.now()
                if not this.purchase_order_id.delivered_date:
                    this.purchase_order_id.delivered_date = fields.Datetime.now()
                this.purchase_order_id.state = 'deliver'
        return result

    @api.multi
    def send_tracking_mail_group(self):
        self.ensure_one()
        ir_model_data = self.env['ir.model.data']
        try:
            template_id = ir_model_data.get_object_reference('pinnacle_purchase', 'mail_customer_tracking_numbers_groups')[1]
        except ValueError:
            template_id = False
        try:
            compose_form_id = ir_model_data.get_object_reference('mail', 'email_compose_message_wizard_form')[1]
        except ValueError:
            compose_form_id = False
        ctx = dict(self.env.context or {})
        
        #id_group = ir_model_data.get_object_reference('account', 'group_account_manager')[1]

        ctx.update({
            'default_model': 'purchase.decoration.group.for.tracking',
            'default_res_id': self.ids[0],
            'default_use_template': bool(template_id),
            'default_template_id': template_id,
            'default_composition_mode': 'comment',
            'default_partner_id': self.purchase_order_id.partner_id.id
        })
        return {
            'name': _('Compose Email'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }

class ProductProduct(models.Model):
    _name = 'product.product'
    _inherit = 'product.product'

    @api.multi
    def _purchase_count(self):
        domain = [
            ('state', 'in', ['purchase', 'deliver', 'done']),
            ('product_id', 'in', self.mapped('id')),
        ]
        r = {}
        for group in self.env['purchase.report'].read_group(domain, ['product_id', 'unit_quantity'], ['product_id']):
            r[group['product_id'][0]] = group['unit_quantity']
        for product in self:
            product.purchase_count = r.get(product.id, 0)
        return True

class DeliveryCarrier(models.Model):
    _inherit = 'delivery.carrier'

    tracking_url = fields.Char(string='URL', help='URL With Tracking Parameter')

    def _clean_tracking_url(self, tracking_url):
        (scheme, netloc, path, params, query, fragment) = urlparse.urlparse(tracking_url)
        if not scheme:
            if not netloc:
                netloc, path = path, ''
            tracking_url = urlparse.urlunparse(('http', netloc, path, params, query, fragment))
        return tracking_url

    @api.multi
    def write(self, vals):
        if vals.get('tracking_url'):
            vals['tracking_url'] = self._clean_tracking_url(vals['tracking_url'])
        result = super(DeliveryCarrier, self).write(vals)
        return result

    @api.model
    def create(self, vals):
        if vals.get('tracking_url'):
            vals['tracking_url'] = self._clean_tracking_url(vals['tracking_url'])
        result = super(DeliveryCarrier, self).create(vals)
        return result

class PurchaseOrderPackingSlip(models.Model): 
    _name = 'purchase.order.packing.slip'

    @api.multi
    def read(self, fields=None, load='_classic_read'):
        result = super(PurchaseOrderPackingSlip, self).read(fields=fields, load=load)
        for record in result:
            if record.get('ship_to_location', False) and type(record.get('ship_to_location', False)) is tuple:
                shipping_line = self.env['shipping.line'].browse([record.get('ship_to_location', False)[0]])
                if shipping_line:
                    record['ship_to_location'] = (shipping_line.id, shipping_line.address_name or '')
        return result

    purchase_order_id = fields.Many2one('purchase.order', string="Purchase Order")
    purchase_order_id_state = fields.Selection(related="purchase_order_id.state")
    ship_to_location = fields.Many2one('shipping.line', string="Ship To Location")
    ship_to_address_name = fields.Char(string="Address Name")
    ship_to_address_street = fields.Char(string="Street")
    ship_to_address_street2 = fields.Char(string="Street2")
    ship_to_address_zip = fields.Char(string="Zip")
    ship_to_address_city = fields.Char(string="City")
    ship_to_address_state_id = fields.Many2one("res.country.state", string='State', ondelete='restrict')
    ship_to_address_country_id = fields.Many2one('res.country', string='Country', ondelete='restrict')
    ship_to_address_phone = fields.Char(string="Phone")
    ship_date = fields.Date(string="Ship Date")
    delivery_date = fields.Date(string="Delivery Date")
    delivery_method = fields.Many2one('delivery.carrier', string="Delivery Method")
    order_lines = fields.One2many('purchase.order.packing.slip.order.line', 'purchase_order_packing_slip_id', string="Order Lines")

    @api.onchange('ship_to_location')
    def _onchange_ship_to_location(self):
        self.ship_to_address_street = False
        self.ship_to_address_street2 = False
        self.ship_to_address_zip = False
        self.ship_to_address_city = False
        self.ship_to_address_state_id = False
        self.ship_to_address_country_id = False
        self.ship_to_address_phone = False
        self.ship_date = False
        self.delivery_date = False
        self.delivery_method = False
        records = []
        if self.ship_to_location and self.purchase_order_id:
            for shipping_line in self.purchase_order_id.shipping_lines.sorted(key=lambda r: r.id):
                if self.ship_to_location.id == shipping_line.id:
                    self.ship_to_address_name = shipping_line.address_name
                    self.ship_to_address_street = shipping_line.street1
                    self.ship_to_address_street2 = shipping_line.street2
                    self.ship_to_address_zip = shipping_line.zipcode
                    self.ship_to_address_city = shipping_line.city
                    self.ship_to_address_state_id = shipping_line.state_id.id
                    self.ship_to_address_country_id = shipping_line.contact_country_id.id
                    self.ship_to_address_phone = shipping_line.contact and shipping_line.contact.phone or False
                    self.ship_date = shipping_line.ship_date
                    self.delivery_date = shipping_line.delivery_date
                    self.delivery_method = shipping_line.delivery_method.id
                    product_ids = []
                    if shipping_line.product_line:
                        product_ids = [product_line.product.id for product_line in shipping_line.product_line]
                    for order_line in self.purchase_order_id.order_line:
                        if order_line.product_id.id in product_ids:
                            record = {
                                'quantity': order_line.product_qty,
                                'name': order_line.name,
                                'product_sku': order_line.product_sku,
                                'variant': order_line.product_variant_name,
                            }
                            records.append([0,0,record])
        ship_to_location = []
        if self.purchase_order_id:
            for shipping_line in self.purchase_order_id.shipping_lines:
                if shipping_line.ship_to_location == 'no':
                    ship_to_location.append(shipping_line.id)
        self.update({'order_lines': records})
        return {'domain': {
            'ship_to_location': [('id', 'in', ship_to_location)],
        }}

    @api.multi
    def edit_packing_slips(self):
        form_view_id = self.env.ref(
            'pinnacle_purchase.purchase_order_packing_slip_form_view_form_purchase')
        if self and form_view_id:
            return {'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'name': "Packing Slip",
                    'target': 'new',
                    'view_id': form_view_id.id,
                    'res_model': 'purchase.order.packing.slip',
                    'res_id': self[0].id,
                    }

    @api.multi
    def view_packing_slips(self):
        form_view_id = self.env.ref(
            'pinnacle_purchase.purchase_order_packing_slip_form_view_readonly_view')
        if self and form_view_id:
            return {'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'name': "Packing Slip",
                    'target': 'new',
                    'view_id': form_view_id.id,
                    'res_model': 'purchase.order.packing.slip',
                    'res_id': self[0].id,
                    }
    @api.multi
    def print_packing_slips(self):
        self.ensure_one()
        return self.env['report'].get_action(self, 'pinnacle_purchase.report_purchase_order_packing_slip_template')

class PurchaseOrderPackingSlipOrder(models.Model): 
    _name = 'purchase.order.packing.slip.order.line'

    purchase_order_packing_slip_id = fields.Many2one('purchase.order.packing.slip', string="Packing Slip")
    quantity = fields.Float(string='Quantity', digits=dp.get_precision('Product Unit of Measure'))
    name = fields.Text(string='Description')
    product_sku = fields.Char(string="Product ID") 
    variant = fields.Char(string="Variant") 

class InternalPurchaseOrderApprovalSlab(models.Model):
    _name = 'internal.purchase.order.approval.slab'
    _order = 'lower_limit'

    name = fields.Char('Slab Name', required=True)
    lower_limit = fields.Float('Lower Limit', help="Inclusive Lower Limit", required=True)
    upper_limit = fields.Float('Upper Limit', help="Inclusive Upper Limit", required=True)
    users = fields.Many2many('res.users', string='Slab Approval Authorities/Officiales', required=True)

    _sql_constraints = [('u_must_greater_than_l', 'CHECK(upper_limit > lower_limit)', 'The Upper Limit must be greater than Lower Limit !')]

    @api.onchange('users')
    def _onchange_users(self):
        group_ref = self.env.ref('purchase.group_purchase_manager')
        if group_ref:
            return {'domain': {'users': [('id', 'in', group_ref.users.ids)]}}

    @api.model
    def create(self, vals):
        if 'lower_limit' in vals:
            if len(self.search(['&', ('lower_limit', '<=', vals.get('lower_limit', False)), ('upper_limit', '>=', vals.get('lower_limit', False))])) != 0:
                raise UserError(_('You can not enter a lower limit that will be included in other Slab.'))
        if 'upper_limit' in vals:
            if len(self.search(['&', ('lower_limit', '<=', vals.get('upper_limit', False)), ('upper_limit', '>=', vals.get('upper_limit', False))])) != 0:
                raise UserError(_('You can not enter a upper limit that will be included in other Slab.'))
        if 'lower_limit' in vals and 'upper_limit' in vals:
            if len(self.search(['&', ('lower_limit', '>=', vals.get('lower_limit', False)), ('upper_limit', '<=', vals.get('upper_limit', False))])) != 0:
                raise UserError(_('You can not enter a range that will include the range of other Slab.'))
        result = super(InternalPurchaseOrderApprovalSlab, self).create(vals)
        return result

    @api.multi
    def write(self, vals):
        if 'lower_limit' in vals:
            for record in self:
                if len(self.search(['&', ('lower_limit', '<=', vals.get('lower_limit', False)), '&', ('upper_limit', '>=', vals.get('lower_limit', False)), ('id', '!=', record.id)])) != 0:
                    raise UserError(_('You can not enter a lower limit will be included in other Slab.'))
                if len(self.search(['&', ('lower_limit', '>=', vals.get('lower_limit', False)), ('upper_limit', '<=', record.upper_limit), ('id', '!=', record.id)])) != 0:
                    raise UserError(_('You can not enter a range that will include the range of other Slab.'))
        if 'upper_limit' in vals:
            for record in self:
                if len(self.search(['&', ('lower_limit', '<=', vals.get('upper_limit', False)), '&', ('upper_limit', '>=', vals.get('upper_limit', False)), ('id', '!=', record.id)])) != 0:
                    raise UserError(_('You can not enter a upper limit that will be included in other Slab.'))
                if len(self.search(['&', ('lower_limit', '>=', record.lower_limit), ('upper_limit', '<=', vals.get('upper_limit', False)), ('id', '!=', record.id)])) != 0:
                    raise UserError(_('You can not enter a range that will include the range of other Slab.'))
        if 'lower_limit' in vals and 'upper_limit' in vals:
            for record in self:
                if len(self.search(['&', ('lower_limit', '>=', vals.get('lower_limit', False)), ('upper_limit', '<=', vals.get('upper_limit', False)), ('id', '!=', record.id)])) != 0:
                    raise UserError(_('You can not enter a range that will include the range of other Slab.'))
        result = super(InternalPurchaseOrderApprovalSlab, self).write(vals)
        return result

class PurchaseOrderFFR(models.TransientModel):
    _name = 'purchase.order.frr'

    purchase_order = fields.Many2one('purchase.order', string="Purchase Order", readonly=1)
    sale_order = fields.Many2one('sale.order', string="Sale Order")
    vendor = fields.Many2one("res.partner", string="Vendor", readonly=1)
    status = fields.Char(string="Status")
    schedules_date = fields.Date('Scheduled Date')
    product_sku = fields.Char(string='Product ID')
    variant = fields.Char('Variant')
    est_delivery_date = fields.Date('Est. Delivery Date')
    tracking_number = fields.Char('Tracking')
    tracking_url_ids = fields.One2many('purchase.tracking.wiz.frr', 'wiz_id', 'Tracking URLs')
    line_id = fields.Many2one('purchase.order.line.for.tracking', 'Product Satus Line')
    fulfillment_status = fields.Char('Fulfillment Status')
    product_code = fields.Char('Vendor Product Code')   

    @api.multi
    def view_frr(self):
        """
        This method group by purchase order line and/or decoration groups.

        :returns: None
        """
        form_view_id = self.env.ref('pinnacle_purchase.purchase_order_frr_tree', False)
        new_context = self.env.context.copy()
        po_ids = self.env['purchase.order'].search([('state', 'not in', ['draft', 'close', 'done', 'cancel']), ('program_type', '=', 'inventory'), ('inventory_reconciled_to_warehouse_flag', '=', False)])
        purchase_frr = self.env['purchase.order.frr']
        pur_wiz_ids = purchase_frr.search([])
        pur_wiz_ids.unlink()
        pur_wiz_ids = []
        for po in po_ids:
            fulfillment_status = 'Blank'
            if po.inventory_sent_to_warehouse_flag:
                fulfillment_status = 'Sent'
            if po.inventory_received_to_warehouse_flag:
                fulfillment_status = 'Received'
            if po.inventory_reconciled_to_warehouse_flag:
                fulfillment_status = ''
            for line in po.product_status_line:
                tracking_numbers = []
                tracking_links = []
                for track in line.product_tracking_lines:
                    tracking_numbers.append(track.tracking_number)
                    tracking_links.extend([(0, 0, {'tracking_url': track.generated_tracking_number_url})])
                tracking_num = ', '.join(tracking_numbers)
                shipping_info = po.shipping_lines.filtered(lambda r: r.tmpl_id.product_sku == line.product_sku) 
                product_code = self.env['product.supplierinfo'].search([('name', '=', po.partner_id.id), ('product_tmpl_id', '=', shipping_info.tmpl_id.id)]).product_code
                if not product_code:
                    product_code = self.env['product.supplierinfo'].search([('name', '=', po.partner_id.parent_id and po.partner_id.parent_id.id), ('product_tmpl_id', '=', shipping_info.tmpl_id.id)]).product_code
                rec = purchase_frr.create({'purchase_order': po.id,
                                           'sale_order': po.sale_order_id.id,
                                           'vendor': po.partner_id.id,
                                           'schedules_date': shipping_info.ship_date,
                                           'est_delivery_date': shipping_info.delivery_date,
                                           'product_sku': line.product_sku,
                                           'variant': line.product_variant_name,
                                           'tracking_number': tracking_num and tracking_num or False,
                                           'tracking_url_ids': tracking_links,
                                           'status': line.product_status_status_name,
                                           'fulfillment_status': fulfillment_status,
                                           'product_code': product_code,
                                           'line_id': line.id,})

                pur_wiz_ids.append(rec.id)
        if len(pur_wiz_ids) == 0:
            raise UserError('None of Purchase order comes to the Fulfillment Receiving')
        return pur_wiz_ids

    @api.multi
    def view_purchase_order(self):
        form_view_id = self.env.ref('purchase.purchase_order_form')
        if self.purchase_order:
            return {'type': 'ir.actions.act_window',
                    'view_mode': 'form',
                    'name': "Purchase Order",
                    'view_id': form_view_id.id,
                    'res_model': 'purchase.order',
                    'res_id': self.purchase_order.id,
                    }

    @api.multi
    def track(self):
        form_view_id = self.env.ref('pinnacle_purchase.purchase_order_line_for_tracking_form_for_view_tracking')
        if self:
            return {'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'name': "Product/Service Tracking Lines",
                    'target': 'new',
                    'views': [(form_view_id.id, 'form')],
                    'view_id': form_view_id.id,
                    'res_model': 'purchase.order.line.for.tracking',
                    'res_id': self.line_id.id,
                    }

class PurchaseTrackingWiz(models.TransientModel):
    _name = 'purchase.tracking.wiz.frr'

    wiz_id = fields.Many2one('purchase.order.frr', 'PO line Wizard')
    tracking_url = fields.Char('Tracking Number')
