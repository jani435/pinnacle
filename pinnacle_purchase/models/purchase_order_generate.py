from openerp import models, fields, api, _
from openerp.exceptions import UserError
import openerp.addons.decimal_precision as dp
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from datetime import datetime
import sys
import logging
_logger = logging.getLogger(__name__)

class SaleOrder(models.Model):
    _inherit = 'sale.order'

    @api.multi
    def view_po(self):
        """
        This method group by purchase order line and/or decoration groups.

        :returns: None
        """
        form_view_id = self.env.ref('pinnacle_purchase.purchase_order_wizard_tree', False)
        new_context = self.env.context.copy()
        new_context['search_default_groupby_orderid'] = True
        po_ids = self.env['purchase.order'].search([('sale_order_id', '=', self[0].id)])
        purchase_wiz = self.env['purchase.order.line.wiz']
        pur_wiz_ids = purchase_wiz.search([])
        pur_wiz_ids.unlink()
        pur_wiz_ids = []
        for po in po_ids:
            for line in po.product_status_line:
                tracking_numbers = []
                tracking_links = []
                for track in line.product_tracking_lines:
                    tracking_numbers.append(track.tracking_number)
                    tracking_links.extend([(0, 0, {'tracking_url': track.generated_tracking_number_url})])
                tracking_num = ', '.join(tracking_numbers)
                rec = purchase_wiz.create({'purchase_order': po.id,
                                           'sale_order': po.sale_order_id.id,
                                           'vendor': po.partner_id.id,
                                           'schedules_date': po.sale_order_id.date_order,
                                           'product_sku': line.product_sku,
                                           'variant': line.product_variant_name,
                                           'tracking_number': tracking_num and tracking_num or False,
                                           'tracking_url_ids': tracking_links,
                                           'status': line.product_status_status_name})

                pur_wiz_ids.append(rec.id)

            if not po.for_blank:
                for dec_line in po.purchase_decoration_groups:
                    tracking_numbers = []
                    tracking_links = []
                    for track in dec_line.product_tracking_lines:
                        tracking_numbers.append(track.tracking_number)
                        tracking_links.extend([(0, 0, {'tracking_url': track.generated_tracking_number_url})])
                    tracking_num = ', '.join(tracking_numbers)
                    rec = purchase_wiz.create({
                        'purchase_order': po.id,
                        'sale_order': po.sale_order_id.id,
                        'vendor': po.partner_id.id,
                        'schedules_date': po.sale_order_id.date_order,
                        'tracking_number': tracking_num and tracking_num or False,
                        'tracking_url_ids': tracking_links,
                        'status': dec_line.product_status_status_name})
                    pur_wiz_ids.append(rec.id)

            if not po.product_status_line and not po.purchase_decoration_groups:
                rec = purchase_wiz.create({
                        'purchase_order': po.id,
                        'sale_order': po.sale_order_id.id,
                        'vendor': po.partner_id.id,
                        'schedules_date': po.sale_order_id.date_order,
                        })
                pur_wiz_ids.append(rec.id)
        return {
            'name': _('Purchase Order Line'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'tree,kanban',
            'res_model': 'purchase.order.line.wiz',
            #'views': [(form_view_id.id, 'tree')],
            #'view_id': form_view_id.id,
            'domain': [('id', 'in', pur_wiz_ids)],
            'target': 'current',
            'context': new_context
        }

    @api.multi
    def generate_po(self):
        """
        This method will unlink purchase order if exist for sale order than it will try to create other purchase orders.

        :returns: None
        """
        self.ensure_one()
        if not self.soc_id:
            raise UserError(_("Please select a vendor point of contact in the sales order before generating POs."))
        lines = self.env['purchase.order'].search([('sale_order_id','=', self.id)])
        if len(lines):
            lines.button_cancel()
            lines.unlink()
        self.group_supplier_purchase_order()
        self.generate_purchase_order_new()

        self.skipped_confirmation_step = False
        self.art_processing_needed = False
        self.action_confirm()
        lines = self.env['purchase.order'].search([('sale_order_id','=', self.id)])
        if lines:
            for line in lines:
                line.compute_priority_orders()
        if not len(lines):
            self.state = 'ready_for_fullfillment'

    @staticmethod
    def default_supplier_dict():
        """
        This method will return default supplier dict.

        :returns: dict
        """
        return {
            'so_lines': [],
            'shipping_lines': {},
            'supplier': None,
            'is_finisher': False,
            'is_supplier': True,
            'main_finisher': False,
            'other_line': [],
        }

    @staticmethod
    def default_finisher_dict():
        """
        This method will return default finisher dict.

        :returns: dict
        """
        return {
                'decoration_group_ids': [],
                'decoration_cost_lines':[],
                'decoration_cost_values':{},
                'decoration_charge_values':{},
                'shipping_lines':{},
                'finisher': None,
                'so_lines': [],
                'is_processed': False,
                'is_finisher': True,
                'main_finisher': False,
                'other_line': [],
       }

    def default_vendor_dict(self):
        """
        This method will initialise vendor_group_by object.

        :returns: None
        """
        self.vendor_group_by = {
             'supplier_jobs': {},
             'finisher_jobs': {},
             'other_job': {},
        }

    def group_supplier_purchase_order(self):
        """
        This method will start group by according to supplier's line, than finishers lines.
        at the end it will store lines of ltm and blank order to related supplier and finsihers raw data.

        :returns: None
        """
        self.default_vendor_dict()
        self.fetch_sale_order_shipping_info()
        sale_order_line = self.env['sale.order.line']
        supplier_list = ['changer_supplier_partner' ]
        group_field = [supplier_list]
        for gfields in group_field:
            so_line_exist = []
            for feild in gfields:
                result = sale_order_line.read_group([('order_id', '=', self.id)], fields=[feild], groupby=feild)
                for res in result:
                    if res[feild]:
                        key = res[feild][0]
                        sale_ids = sale_order_line.search([(feild,'=',key),('order_id', '=', self.id)]).ids
                        odd_lines = list(set(sale_ids)^set(list(set(so_line_exist) & set(sale_ids))))
                        if len(odd_lines):
                            so_line_exist += odd_lines
                            if feild in supplier_list:
                                self.process_supplier(odd_lines, key)
                            elif feild in other_lines:
                                self.process_other(odd_lines, key)
        
        #ltm lines
        for ltm_line in sale_order_line.search([('order_id', '=', self.id),('ltm_parent', '!=', False)]):
            supplier_key = ltm_line.ltm_parent.changer_supplier_partner.id
            if self.vendor_group_by['supplier_jobs'].has_key(supplier_key):
                self.vendor_group_by['supplier_jobs'][supplier_key]['other_line'].append(ltm_line.id)

        #blank lines
        for other_line in sale_order_line.search([('order_id', '=', self.id),('other_supplier_id', '!=', False)]):
            supplier_key = other_line.other_supplier_id.id
            if self.vendor_group_by['supplier_jobs'].has_key(supplier_key):
                self.vendor_group_by['supplier_jobs'][supplier_key]['other_line'].append(other_line.id)
            elif self.vendor_group_by['finisher_jobs'].has_key(supplier_key):
                self.vendor_group_by['finisher_jobs'][supplier_key]['other_line'].append(other_line.id)

        _logger.info('Purchase order raw data (%s) for Sale Order Line (%s)', self.vendor_group_by, self.name)

    def process_supplier(self, so_lines, key):
        """
        This method will sale order lines and store in vendor_group_by by 
        suppliers and finishers.
        
        :param so_lines: sale order lines object
        :param key: Integer: res partner ID

        :returns: None
        """
        for so_line in self.env['sale.order.line'].browse(so_lines):
            if self.road_block_for_po(so_line.product_id):
                continue
            supplier_key = self.get_rush_supplier(so_line, key)
            if not self.vendor_group_by['supplier_jobs'].has_key(supplier_key):
                self.vendor_group_by['supplier_jobs'][supplier_key] = self.default_supplier_dict()
                self.vendor_group_by['supplier_jobs'][supplier_key]['main_supplier'] =  so_line.change_supplier.name.id
                self.vendor_group_by['supplier_jobs'][supplier_key]['supplier'] = so_line.change_supplier.id
                
            self.vendor_group_by['supplier_jobs'][supplier_key]['so_lines'].append(so_line.id)

            if not so_line.is_decoration_disable:
                for decoration_line in so_line.decorations:
                    finisher_key = self.get_rush_finisher(so_line, decoration_line.finisher.id)
                    self.process_finisher(decoration_line, finisher_key, so_line, so_line.change_supplier.name.id)
                    self.vendor_group_by['finisher_jobs'][finisher_key]['main_finisher'] = decoration_line.finisher.id
                    if finisher_key == supplier_key:
                        self.vendor_group_by['supplier_jobs'][supplier_key]['is_finisher'] = True

    def process_finisher(self, decoration_line, finisher_key, so_line, main_supplier):
        """
        This method will process decorations and finisher and store
        
        :param decoration_line: sale order decoration line object
        :param finisher_key: Integer: res partner ID
        :param so_line: sale order line object
        :param main_supplier: integer: blank good supplier's res partner ID

        :returns: None
        """
        if not self.vendor_group_by['finisher_jobs'].has_key(finisher_key):
            self.vendor_group_by['finisher_jobs'][finisher_key] = self.default_finisher_dict()

        # Fetched sale order line for decoration
        if decoration_line.sale_order_line_id.id not in self.vendor_group_by['finisher_jobs'][finisher_key]['so_lines']:
            self.vendor_group_by['finisher_jobs'][finisher_key]['so_lines'].append(decoration_line.sale_order_line_id.id)

        if decoration_line.decoration_group_id.id not in self.vendor_group_by['finisher_jobs'][finisher_key]['decoration_group_ids']:
            #Fetched decoration groups
            self.vendor_group_by['finisher_jobs'][finisher_key]['decoration_group_ids'].append(decoration_line.decoration_group_id.id)

            #Fetched decoration cost lines
            for so_lines in decoration_line.decoration_group_id.charge_lines:
                self.vendor_group_by['finisher_jobs'][finisher_key]['decoration_charge_values'][so_lines.id] = [self.set_supplier_value(so_line, so_lines.name, main_supplier)[0], self.set_supplier_value(so_line, so_lines.name_second, main_supplier)[0]]
                if so_lines.added_to_order_lines == 'YES':
                    self.vendor_group_by['finisher_jobs'][finisher_key]['decoration_cost_lines']+= so_lines.sale_order_lines.ids
                    for cost_sale_value in so_lines.sale_order_lines:
                        self.vendor_group_by['finisher_jobs'][finisher_key]['decoration_cost_values'][cost_sale_value.id] = self.set_supplier_value(so_line, cost_sale_value.name, main_supplier)

    def set_supplier_value(self, line, cost_sale_value, main_supplier):
        """
        This method will process on blank product sale order line and sort it to related supplier data.

        :param line: sale order line object
        :param cost_sale_value: string: original decoation name
        :param main_supplier: integer: blank good supplier's res partner ID

        :returns: list: [supplier decoration name, supplier sku name]
        """
        product_sku_dec_names = ''
        final_sku = ''
        finisher = self.env['res.partner'].browse(main_supplier)
        product_supplier_sku = line.product_id.product_tmpl_id.fetch_vendor_product_sku(finisher, line.product_id)
        all_skus = [product_supplier_sku]
        def get_vendor_attribute_code(line, product_attrb_ids):
            product_supplier_sku = line.change_supplier.product_code
            for attrib in line.change_supplier.attribute_vendor_attb_value_ids:
                if set(attrib.product_attribute_values_ids.ids) == set(product_attrb_ids) and attrib.vendor_value_code:
                    product_supplier_sku = attrib.vendor_value_code
                    break
            return product_supplier_sku

        for dec_line in line.decorations:
            name_part = cost_sale_value.split("-")
            if len(name_part[len(name_part)-1].split(",")) > 1:
                final_sku = ' '
                skus = [x.strip() for x in name_part[len(name_part)-1].split(",")]
                product_sku_dec_names = product_supplier_sku
                for xx in self.decorations:
                    if xx.id != dec_line.id and xx.decoration_sequence in skus:
                        code_xx = get_vendor_attribute_code(line, xx.product_id.attribute_value_ids.ids)
                        if code_xx not in all_skus:
                            all_skus.append(code_xx)
                            product_sku_dec_names = product_sku_dec_names +","+code_xx
            else:
                product_sku_dec_names = product_supplier_sku
                final_sku = product_supplier_sku
            if len(name_part) and  name_part[len(name_part)-1].find(dec_line.decoration_sequence) != -1 and dec_line.vendor_decoration_method.id:
                cost_sale_value = cost_sale_value.replace('- ' + str(dec_line.imprint_method.name) + ' -', '- ' + dec_line.vendor_decoration_method.name + ' -')
                cost_sale_value = cost_sale_value.replace('- ' + str(dec_line.imprint_location.name) + ' -', '- ' + dec_line.vendor_decoration_location.name + ' -')


        cost_sale_value = cost_sale_value.replace(line.product_id.product_sku, product_sku_dec_names)
        return [cost_sale_value, final_sku]

    def process_other(self, odd_lines, supplier_key):
        """
        This method will process on blank product sale order line and sort it to related supplier data.

        :returns: None
        """
        if self.vendor_group_by['finisher_jobs'].has_key(supplier_key):
            self.vendor_group_by['finisher_jobs'][supplier_key]['so_lines'] += odd_lines
        elif self.vendor_group_by['supplier_jobs'].has_key(supplier_key):
            self.vendor_group_by['supplier_jobs'][supplier_key]['so_lines'] += odd_lines
        else:
             self.vendor_group_by['supplier_jobs'][supplier_key] = self.default_supplier_dict()
             self.vendor_group_by['supplier_jobs'][supplier_key]['so_lines'] = odd_lines


    def road_block_for_po(self, product_id):
        """
        This method will blocks sale order lines to include in purchase orders.

        :returns: Boolean
        """
        return ((product_id.is_magento_inventory and self.program_type == 'store') or (not product_id.purchase_ok))

    def get_rush_supplier(self, so_line, supplier_key):
        """
        This method process will check rush supplier exists in sale order or not

        :returns: Integer: res partner ID
        """
        rush_supplier = supplier_key
        for line in self.shipping_line:
            if so_line.id in line.shipping_sale_orders.ids and supplier_key == line.partner_supplier.id and line.rush_supplier.id:
                    rush_supplier = line.rush_supplier.id
        return rush_supplier

    def get_rush_finisher(self, so_line, finisher_key):
        """
        This method process will check rush finisher exists in sale order or not

        :returns: Integer: res partner ID
        """
        rush_finisher = finisher_key
        for line in self.shipping_line:
            if so_line.id in line.shipping_sale_orders.ids and finisher_key == line.finisher.id and line.rush_finisher.id:
                rush_finisher = line.rush_finisher.id
        return rush_finisher

    def fetch_sale_order_shipping_info(self):
        """
        This method process shipping data in sale order and sort and store in 'ship_group_by'
        according to supplier to finisher OR finisher-1 to finisher-2, Which is used in 
        shipping line creation in purchase order.

        :returns: None
        """
        sale_obj = self.env['sale.order.line']
        def make_seq(so_line, check_supplier, decoration_list, get_from, sequence=1):
            try:
                if sequence == 1:
                    if check_supplier in decoration_list:
                        for item in ship_group_by[so_line.id]:
                            if item['current_supplier'] == item['sent_to'] == check_supplier:
                                item['sequence'] = 1
                                item['get_from'] = get_from
                                if len(decoration_list) == 1:
                                    decoration_list.remove(check_supplier)
                                    item['sent_to'] = self.partner_id.id
                                    return
                                make_seq(so_line, item['sent_to'], decoration_list, item['current_supplier'], sequence+1)
                                return

                    else:
                        for item in ship_group_by[so_line.id]:
                            if item['current_supplier'] ==  check_supplier:
                                item['sequence'] = 1
                                item['get_from'] = get_from
                                if item['sent_to'] == False:
                                    item['sent_to'] = self.partner_id.id
                                    return
                                make_seq(so_line, item['sent_to'], decoration_list, item['current_supplier'], sequence+1)
                                return

                for item in ship_group_by[so_line.id]:
                    if item['current_supplier'] ==  check_supplier and item['sequence'] == False:
                        item['sequence'] = sequence
                        item['get_from'] = get_from
                        decoration_list.remove(check_supplier)
                        make_seq(so_line, item['sent_to'], decoration_list, item['current_supplier'], sequence+1)
                        return

            except:
                _logger.error('Sale Order Line (%s): Shipping Line Causing Problem (%s).', line_id, sys.exc_info()[0])
                return False

        ship_group_by = {}
        for ship_line in self.shipping_line:
            for so_line in ship_line.shipping_sale_orders:
                if not ship_group_by.has_key(so_line.id):
                    ship_group_by[so_line.id] = []
                ship_group_by[so_line.id].append({
                        'current_supplier': ship_line.partner_supplier.id,
                        'sent_to': ship_line.finisher.id,
                        'sequence': False,
                        'shipping_line': ship_line.id,
                        'get_from': False,
                    })
        for line_id in ship_group_by.keys():
            so_line = sale_obj.browse(line_id)
            decorators = list(set([x.finisher.id for x in so_line.decorations]))
            make_seq(so_line, so_line.changer_supplier_partner.id, decorators, so_line.changer_supplier_partner.id, 1)
            ship_group_by[line_id] = sorted(ship_group_by[line_id], key=lambda k: k['sequence'])

        _logger.info('Shipping lines raw data (%s) for Sale Order (%s)',ship_group_by, self.name)
        self.ship_group_by = ship_group_by


    def generate_shipping_cost(self, purchase_create, finisher_data):
        """
        This method process shipping raw data according to parameters passed and generate shipping cost lines.
        
        :param purchase_create: purchase order object
        :param finisher_data: dict

        :returns: None
        """
        def get_shipping_cost_line(lines):
            all_ship_cost = []
            for this in self.order_line:
                if this.shipping_id and len(list(set(this.shipping_id.shipping_sale_orders.ids) & set(lines))) and this.shipping_id.vendor_account:
                    all_ship_cost.append(this)
            return all_ship_cost
        PurchaseOrderLine = self.env['purchase.order.line']

        for ship_line in get_shipping_cost_line(finisher_data.get('so_lines')):
            exist = PurchaseOrderLine.search([('sale_order_line_id', '=', ship_line.id)])
            if not exist:
                po_line_vals = self._prepare_purchase_order_line_data(ship_line, purchase_create)
                purchase_order_line = PurchaseOrderLine.create(po_line_vals)

    def generate_purchase_order_new(self):
        """
        This method is main method for generting all purchase orders, after collecting all
        raw data this method will start generating purchase orders.

        :returns: None
        """

        ResPartner = self.env['res.partner']
        purchase_order = self.env['purchase.order']
        purchase_order_line = self.env['purchase.order.line']
        PurchaseOrderLineForTracking = self.env['purchase.order.line.for.tracking']
        for supplier_key in self.vendor_group_by['supplier_jobs']:
            supplier_data = self.vendor_group_by['supplier_jobs'][supplier_key]
            to_create = self._prepare_purchase_order_data(ResPartner.browse(supplier_key), supplier_data)[0]
            purchase_create = purchase_order.create(to_create)
            self.generate_supplier_shipping_line(purchase_create, supplier_data)
            
            for sale_line in self.env['sale.order.line'].browse(supplier_data['other_line']):
                to_create_line = self._prepare_purchase_order_line_data(sale_line, purchase_create)
                new_po_line = purchase_order_line.create(to_create_line)

            for sale_line in self.env['sale.order.line'].browse(supplier_data['so_lines']):
                to_create_line = self._prepare_purchase_order_line_data(sale_line, purchase_create)
                new_po_line = purchase_order_line.create(to_create_line)
                PurchaseOrderLineForTracking.create({
                    'purchase_order_line_id': new_po_line.id,
                    'purchase_order_id': purchase_create.id,
                    'name': new_po_line.name,
                    'product_sku': new_po_line.product_sku,
                    'product_variant_name': new_po_line.product_variant_name,
                    'auto_generate': True,
                    'qty': new_po_line.product_qty
                    })
            if supplier_data.get('is_finisher'):
                self.vendor_group_by['finisher_jobs'][supplier_key]['is_processed'] = True
                finisher_data = self.vendor_group_by['finisher_jobs'][supplier_key]
                self.generate_finisher_purchase_order(supplier_key, purchase_create)
            if self.order_type == 'samples':
                self.generate_shipping_cost(purchase_create, supplier_data)

        for finisher_key in self.vendor_group_by['finisher_jobs']:
            if not self.vendor_group_by['finisher_jobs'][finisher_key].get('is_processed'):
                to_create = self._prepare_purchase_order_data(ResPartner.browse(finisher_key),  self.vendor_group_by['finisher_jobs'][finisher_key])[0]
                purchase_create = purchase_order.create(to_create)
                self.generate_finisher_purchase_order(finisher_key, purchase_create)

    def generate_supplier_shipping_line(self, purchase_create, supplier_data):
        """
        This method process shipping raw data according to parameters passed and generate shipping line and shipping cost in purchase order.
        
        :param purchase_create: purchase order object
        :param supplier_data:dict

        :returns: None
        """

        ship_line_exist = []
        for line in self.env['sale.order.line'].browse(supplier_data['so_lines']):
            ship_item = {}
            for ship_line in self.shipping_line:
                if self.ship_group_by.has_key(line.id):
                    for x in self.ship_group_by[line.id]:
                        if x['current_supplier'] == line.changer_supplier_partner.id and x['shipping_line'] == ship_line.id:
                            ship_item = x
                if line.id in ship_line.shipping_sale_orders.ids and ship_line.id not in ship_line_exist:

                    #For Blank Order
                    if line.change_supplier.id == ship_line.supplier.id and line.is_decoration_disable:
                        ship_line_exist.append(ship_line.id)
                        new_shipline = ship_line.copy({'purchase_order_id': purchase_create.id})

                    elif ship_item.get('current_supplier') == ship_line.partner_supplier.id and ship_item.get('sent_to') == ship_line.finisher.id and line.change_supplier.name.id != line.finisher_id.id:
                        ship_line_exist.append(ship_line.id)
                        #The split ship file from an SO should not carry over to the blanks PO when the shipment is to the finisher
                        contact = self.env['res.partner'].browse(ship_item['sent_to'])
                        if contact.country_id:
                            country_id = contact.country_id.id
                        else:
                            country_rec = self.ref('base.us')
                            if country_rec:
                                country_id = country_rec.id
                        new_shipline = ship_line.copy({'delivery_method': ship_line.delivery_method.id, #Task: 16550
                                        'delivery_line':[],
                                        'purchase_order_id': purchase_create.id,
                                        'contact': ship_item['sent_to'] if ship_item else False,
                                        'tracking': ' ',
                                        'ship_to_location': 'no',
                                        'csv_file': None,
                                        'vendor_account': False,
                                        'customer_account': False,
                                        'address_name': contact.name,
                                        'street1': contact.street,
                                        'street2' : contact.street2,
                                        'city' : contact.city,
                                        'state_id' : contact.state_id.id,
                                        'zipcode' : contact.zip,
                                        'contact_country_id': country_id,
                                        'ship_date': False, #Task: 16666
                                        'delivery_date': False, #Task: 16666
                                        })

    def generate_finisher_shipping_line(self, purchase_create, finisher_data):
        """
        This method process shipping raw data according to parameters passed and generate shipping line and shipping cost in purchase order.

        :param purchase_create: purchase order object
        :param finisher_data: dict

        :returns: None
        """

        ship_line_exist = []
        finisher_key = finisher_data['main_finisher']
        for line in self.env['sale.order.line'].browse(finisher_data['so_lines']):
            ship_item = None
            last = None
            if self.ship_group_by.has_key(line.id):
                for x in self.ship_group_by[line.id]:
                    if x['current_supplier'] == finisher_key:
                        ship_item = x
                        break
                if ship_item is None:
                    ship_item = self.ship_group_by[line.id][-1]
                    if ship_item.get('sent_to') != finisher_key:
                        ship_item = None
                last = self.ship_group_by[line.id][-1].get('sequence') == ship_item.get('sequence')
                if ship_item and ship_item['shipping_line'] not in ship_line_exist:
                    ship_line = self.env['shipping.line'].browse(ship_item['shipping_line'])
                    ship_line_exist.append(ship_item['shipping_line'])
                    if ship_line.contact.id == ship_item['sent_to'] or last:
                        contact = ship_item['sent_to']
                        if last:
                            contact = self.partner_id.id
                        new_shipline = ship_line.copy({
                                        'contact':  contact,
                                        'purchase_order_id': purchase_create.id,
                                        'is_decoration_disable': False,
                                        })
                    else:
                        contact = self.env['res.partner'].browse(ship_item['sent_to'])
                        new_shipline = ship_line.copy({'delivery_method': False,
                                        'delivery_line':[],
                                        'purchase_order_id': purchase_create.id,
                                        'contact': ship_item['sent_to'] if ship_item else False,
                                        'tracking': ' ',
                                        'ship_date': False,
                                        'delivery_date': False,
                                        'ship_to_location': 'no',
                                        'csv_file': None,
                                        'vendor_account': False,
                                        'customer_account': False,
                                        'address_name': ship_line.address_name,
                                        'street1': ship_line.street1,
                                        'street2' : ship_line.street2,
                                        'city' : ship_line.city,
                                        'state_id' : ship_line.state_id.id,
                                        'zipcode' : ship_line.zipcode,
                                        'contact_country_id': ship_line.contact_country_id.id
                                        })
                    if ship_line.ship_to_location == 'yes':
                        contact = self.env['ir.model.data'].get_object_reference('pinnacle_vendor', 'for_muptiple_location')[1]
                        new_shipline.write({'contact': contact})

    def fetch_artwork_data(self, dec_line, finisher_key):
        """
        This method process notes on sale order and fetch artwork for PO, according passed paramters.

        :param dec_line: decoration line object
        :param finisher_key: res partner integer ID

        :returns: art work line object
        """
        for line in self.art_line:
            if line.finisher_id.id == finisher_key and dec_line.id in line.decorations.ids:
                return line

    def generate_finisher_purchase_order(self, finisher_key, purchase_create):
        """
        This method process on collected raw data match with passed parameters and create decortion line, decortion groups, decoration cost, art work, shipping charges, shipping lines to finisher purchase orders.

        :param purchase_create: purchae order object
        :param finisher_key: res partner integer ID

        :returns: None
        """
        purchase_order = self.env['purchase.order']
        purchase_order_line = self.env['purchase.order.line']
        finisher_data = self.vendor_group_by['finisher_jobs'][finisher_key]
        sale_order_line = self.env['sale.order.line']
        sale_order_decoration_line = self.env['sale.order.line.decoration']
        decorationgroup = self.env['sale.order.line.decoration.group']
        decoration_group_charge_line = self.env['sale.order.line.decoration.group.charge.line']
        PurchaseDecorationGroupForTracking = self.env['purchase.decoration.group.for.tracking']
        so_to_po_line_dict = {}
        ir_attachment = self.env['ir.attachment']
        
        self.generate_finisher_shipping_line(purchase_create, finisher_data)
        
        for sale_line in self.env['sale.order.line'].browse(finisher_data['other_line']):
            to_create_line = self._prepare_purchase_order_line_data(sale_line, purchase_create)
            new_po_line = purchase_order_line.create(to_create_line)

        for sale_line in sale_order_line.browse(finisher_data['so_lines']):
            if finisher_data.get('is_processed') and sale_line.id in self.vendor_group_by['supplier_jobs'][finisher_key]['so_lines']:
                line_browse = purchase_order_line.search([('order_id', '=', purchase_create.id),('sale_order_line_id', '=', sale_line.id)], limit=1)
                line_browse.write({'can_decorate': True})
                so_to_po_line_dict[sale_line.id] = line_browse.id

            else:
                to_create_line = self._prepare_purchase_order_line_data(sale_line,purchase_create, incoming=True, can_decorate=True)
                try:
                    for item in self.ship_group_by[to_create_line['sale_order_line_id']]:
                        if item['sent_to'] == finisher_data['main_finisher']:
                            to_create_line['blank_provider'] = item['current_supplier']
                            to_create_line['blank_supplier'] = sale_line.change_supplier.id
                except:
                    _logger.error('Sale Order Line (%s): Shipping Line Causing Problem (%s).', sale_line.id, sys.exc_info()[0])
                so_to_po_line_dict[sale_line.id] = purchase_order_line.create(to_create_line).id

        #Adding Decoration Groups to Purchase Order
        for dec_group in decorationgroup.browse(finisher_data['decoration_group_ids']):
            new_dec_group = dec_group.copy({
                'purchase_order_id': purchase_create.id,
                'group_id_sale_ref': dec_group.id,
                'reorder_ref_po': dec_group.reorder_ref_po_id.name if dec_group.reorder_ref_po_id else dec_group.reorder_ref_po_id2.name,
                })
            group_qty = sum(line.sale_order_line_product_uom_qty for line in dec_group.decorations)
            PurchaseDecorationGroupForTracking.create({
                'decoration_group_id': new_dec_group.id,
                'purchase_order_id': purchase_create.id,
                'group_sequence': new_dec_group.group_sequence,
                'group_id': new_dec_group.group_id,
                'auto_generate': True,
                'qty': group_qty
            })

            #Adding Decoration Line to Purchase Order
            res_model = 'sale.order.line.decoration'
            res_id = False

            for dec_line in dec_group.decorations:
                new_line = dec_line.copy({
                         'purchase_order_id': purchase_create.id,
                         'purchase_order_line_id': so_to_po_line_dict.get(dec_line.sale_order_line_id.id),
                         'decoration_id_sale_ref': dec_line.id,
                         'decoration_group_id': new_dec_group.id,
                        })
                dec_spec_1 = dec_spec_2 = dec_vendor_file = False
                dec_spec_name = dec_spec_2_name = dec_vendor_name = ''
                art_line = self.fetch_artwork_data(dec_line, finisher_data.get('main_finisher'))
                if art_line and art_line.spec_file_att:
                    data = art_line.spec_file_att.copy_data()[0]
                    data.update({'res_model': res_model, 'res_id': new_line.id})
                    dec_spec_1 = ir_attachment.create(data).id
                    dec_spec_name = art_line.spec_file_name
                if art_line and art_line.spec_file2_att:
                    data = art_line.spec_file2_att.copy_data()[0]
                    data.update({'res_model': res_model, 'res_id': new_line.id})
                    dec_spec_2 = ir_attachment.create(data).id
                    dec_spec_2_name = art_line.spec_file2_name
                if art_line and art_line.vendor_file_att:
                    data = art_line.vendor_file_att.copy_data()[0]
                    data.update({'res_model': res_model, 'res_id': new_line.id})
                    dec_vendor_file = ir_attachment.create(data).id
                    dec_vendor_name =  art_line.vendor_file_name
                new_line.write({
                        'spec_file_att': dec_spec_1,
                         'vendor_file_att': dec_vendor_file,
                         'spec_file2_att': dec_spec_2,
                         'vendor_file_name': dec_vendor_name,
                         'spec_file2_name': dec_spec_2_name,
                         'spec_file_name': dec_spec_name
                    })
            #Adding Decoration Line to Purchase Order
            for charge_line in dec_group.charge_lines:
                name_des = charge_line.name
                name_second = charge_line.name_second
                if finisher_data.get('decoration_charge_values').has_key(charge_line.id):
                    name_des = finisher_data['decoration_charge_values'].get(charge_line.id)[0]
                    name_second = finisher_data['decoration_charge_values'].get(charge_line.id)[1]
                charge_id = charge_line.copy({
                    'name': name_des,
                    'name_second': name_second,
                    'decoration_group_id': new_dec_group.id,
                    'charge_id_sale_ref': charge_line.id,
                })
        #Adding Decoration Cost to Purchase Order
        for so_line in sale_order_line.browse(finisher_data['decoration_cost_lines']):
            to_create_line = self._prepare_purchase_order_line_data(so_line, purchase_create)

            if finisher_data['decoration_cost_values'].has_key(so_line.id):
                new_value = finisher_data['decoration_cost_values'][so_line.id]
                to_create_line['name'] = new_value[0]
                to_create_line['product_sku'] = new_value[1]
                
            po_line_cost = purchase_order_line.create(to_create_line)
            #Attaching sale order charge line to decoration group charge line
            purchase_order_charge_line = decoration_group_charge_line.search([('charge_id_sale_ref', '=', so_line.decoration_charge_group_id.id)], limit=1)
            po_line_cost.with_context({'skip_log': True}).write({'decoration_charge_group_id': purchase_order_charge_line.id})

        self.generate_shipping_cost(purchase_create, finisher_data)

    def fetch_notes(self, raw_data, key):
        """
        This method process notes on sale order and fetch related notes for PO, according passed paramters.

        :param key: res partner integer ID
        :param raw_data: dict

        :returns: string, string , string
        """

        if raw_data.get('is_finisher'):
            key = raw_data.get('main_finisher')
        if raw_data.get('is_supplier'):
            key = raw_data.get('main_supplier')
        lines_notes = self.env['sale.order.line.notes'].search([('vendor', '=', key),('sale_order_id','=', self.id)])
        vendor_line = []
        product_line = []
        proof_line = []
        for line_note in lines_notes:
            if line_note.proof_notes:
                proof_line.append(line_note.proof_notes)
            if line_note.vendor_notes:
                vendor_line.append(line_note.vendor_notes)
            if line_note.product_notes:
                product_line.append(line_note.product_notes)

        return '\n'.join(elem for elem in list(set(proof_line))), '\n'.join(elem for elem in list(set(vendor_line))), '\n'.join(elem for elem in list(set(product_line))), not(len(proof_line) == 0)
    @api.one
    def _prepare_purchase_order_data(self, company_partner, raw_data):
        """
        This method process raw data and return dictionary to create purchase order.

        :param company_partner: res partner object
        :param raw_data: dict

        :returns: dict:{}
        """

        rush_supplier = None
        rush_finisher = None
        if raw_data.get('main_finisher') and raw_data.get('main_finisher') !=  company_partner.id:
            rush_finisher = company_partner.id
        if raw_data.get('main_supplier') and raw_data.get('main_supplier') !=  company_partner.id:
            rush_supplier = company_partner.id
        addr = company_partner.address_get(['invoice'])
        drop_ship_id = self.env.ref('stock_dropshipping.picking_type_dropship')
        invoice_id = addr.get('invoice', False)
        proof_note, vendor_note, product_note, proof_required =  self.fetch_notes(raw_data, company_partner.id)

        return {
            'origin': self.name,
            'partner_id': company_partner.id,
            'picking_type_id': drop_ship_id.id,
            'company_id': self.company_id.id,
            'fiscal_position_id': company_partner.property_account_position_id.id,
            'payment_term_id': company_partner.property_supplier_payment_term_id.id,
            'sale_order_id': self.id,
            'partner_ref': self.name,
            'dest_address_id': 1,
            'date_planned': self.date_order,
            'po_type': 'regular' if self.is_regular else 'sample',
            'auto_generate': True,
            'internal_so_ref': self.internal_ref,
            'partner_invoice_id': invoice_id,
            'soc_id': self.soc_id.id,
            'ass_account_manager': [(6, 0, self.aam_id.ids)],
            'account_manager': self.user_id.id,
            'state': 'draft',
            'program_id': self.program_id.id,
            'program_type': self.program_type,
            'channel_id': self.channel_id.id,
            'decoration_sequence_id': self.decoration_sequence_id,
            'decoration_group_sequence_id': self.decoration_group_sequence_id,
            'tag_ids': [(6, 0, self.tag_ids.ids)],
            'so_partner_id': self.partner_id.id,
            'for_blank': raw_data.get('is_supplier'),
            'for_decoration': raw_data.get('is_finisher'),
            'proof_required': proof_required,
            'proof_note': proof_note,
            'product_note': product_note, 
            'proof_required': proof_required,
            'vendor_note':  vendor_note,
            'rush_supplier': rush_supplier,
            'rush_finisher': rush_finisher,
        }

    @api.model
    def _prepare_purchase_order_line_data(self, so_line, purchase_id, incoming=False, can_decorate=False):
        """
        This method process sale order other given data and return dictionary to create purchase order line.

        :param so_line: sale order line object.
        :param purchase_id: purchase order object.
        :param incoming: boolean
        :param can_decorate: boolean

        :returns: dict:{}
        """
        taxes = so_line.tax_id
        if so_line.product_id:
            taxes = so_line.product_id.supplier_taxes_id
        if so_line.product_id:taxes = so_line.product_id.supplier_taxes_id

        # fetch taxes by company not by inter-company user
        company_taxes = [tax_rec.id for tax_rec in taxes if tax_rec.company_id.id == self.company_id.id]
        product_sku = so_line.product_id.product_tmpl_id.fetch_vendor_product_sku(so_line.change_supplier.name, so_line.product_id)
        variant = so_line.product_id.product_tmpl_id.fetch_vendor_attribute_value(so_line.change_supplier.name, so_line.product_id)

        return {
            'name': so_line.name if so_line.product_id.type == 'service' else so_line.product_id.product_tmpl_id.fetch_seller_all_info(so_line.change_supplier.name, so_line.product_id, so_line.order_id.name)[2],
            'order_id': purchase_id.id,
            'product_qty': so_line.product_uom_qty,
            'product_id': so_line.product_id and so_line.product_id.id or False,
            'product_sku': product_sku,
            'product_uom': so_line.product_id and so_line.product_id.uom_po_id.id or so_line.product_uom.id,
            'price_unit': 0.0,
            'company_id': so_line.order_id.company_id.id,
            'date_planned': self.date_order,
            'taxes_id': [(6, 0, company_taxes)],
            'sale_order_line_id': so_line.id,
            'price_unit': so_line.purchase_price,
            'product_variant_name': variant,
            'state': 'draft',
            'incoming': incoming,
            'finisher_id': so_line.finisher_id and so_line.finisher_id.id or False,
            'shipping_id': so_line.shipping_id and so_line.shipping_id.id or False,
            'change_supplier': so_line.change_supplier and so_line.change_supplier.id or False,
            'analytic_tag_ids': [(6, 0, so_line.analytic_tag_ids.ids)],
            'is_override': True,
            'can_decorate': can_decorate,
        }
