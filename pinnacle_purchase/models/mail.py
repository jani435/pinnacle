import logging
import threading
from odoo import _, api, fields, models, registry, SUPERUSER_ID, tools
from odoo.osv import expression
from openerp.exceptions import UserError
from email.utils import formataddr

class MailMail(models.Model):
    _inherit = 'mail.mail'

    art_email = fields.Boolean("Art Email", default=False)
    is_custom_email_address = fields.Boolean("Custom Email Address", default=False)

    @api.multi
    def send_get_mail_to_with_combined_email(self, partners=None):
        if self.mail_message_id.model != 'purchase.order' and not (self.is_custom_email_address or self.env.context.get('email_address_custom')):
            return super(MailMail, self).send_get_mail_to_with_combined_email(partners=partners)

        self.write({'is_custom_email_address': True})
        email_list = []
        for partner in partners:
            email = partner.email
            po_browse = self.env['purchase.order'].browse(self.mail_message_id.res_id)
            if po_browse.partner_id.id == partner.id:
                if self.env.context.get('po_email_to'):
                    email = self.env.context.get('po_email_to')
                    if self.env.context.get('art_email'):
                        self.write({'art_email': True})
                else:
                    supp_finisher_emails = po_browse._get_email_send_to(only_email=True)
                    if len(supp_finisher_emails) == 2:
                        if self.art_email:
                            email = supp_finisher_emails[1]
                        else:
                            email = supp_finisher_emails[0]

            email_list.append(formataddr((partner.name, email)))
        return email_list

class MailComposeMessage(models.TransientModel):
    _inherit = 'mail.compose.message'

    @api.multi
    def send_mail_action(self):
        # TDE/ ???
        model = self.env.context.get('default_model')
        res_id = self.env.context.get('default_res_id')
        if model == "purchase.order" and res_id and self.env.context.get('email_address_custom'):
            po_browse = self.env['purchase.order'].browse(res_id)
            #for resend buttton enable
            if self.env.context.get('mail_art') and not po_browse.art_send:
                po_browse.write({'art_send': True, 'art_sent_date': fields.Datetime.now()})
            elif self.env.context.get('order_mail') and not self.env.context.get('mail_art') and not po_browse.po_send:
                po_browse.write({'po_send': True, 'po_sent_date': fields.Datetime.now()})
            
            #for storing art mails history to mail servers
            supp_finisher_emails = po_browse._get_email_send_to(only_email=True)

            if self.env.context.get('mail_art'):
                self.with_context(art_email=True, po_email_to=supp_finisher_emails[1]).send_mail()
            else:

                self.with_context(art_email=False, po_email_to=supp_finisher_emails[0]).send_mail()
        else:
            return super(MailComposeMessage, self).send_mail_action()

    @api.multi
    def send_mail(self, auto_commit=False):
        if self._context.get('default_model') == 'purchase.order' and self._context.get('default_res_id'):
            if not self.filtered('subtype_id.internal'):
                order = self.env['purchase.order'].browse([self._context['default_res_id']])
                ref_id = self.env.ref('pinnacle_purchase.purchase_product_status_status_submitted')
                draft_id = self.env.ref('pinnacle_purchase.purchase_product_status_status_draft_new')
                if order.po_send and self.env.context.get('order_mail'):
                    order.write({
                    'state': 'sent',
                    'inventory_sent_to_warehouse_flag': False,
                    'inventory_received_to_warehouse_flag': False,
                    'inventory_reconciled_to_warehouse_flag': False
                    })
                    for decoration_line in order.purchase_decoration_groups:
                        if decoration_line.product_status_status.id != draft_id.id:
                            decoration_line.write({'product_status_status': ref_id.id})
                    for status_line in order.product_status_line:
                        if status_line.product_status_status.id != draft_id.id:
                            status_line.write({'product_status_status': ref_id.id})
                if order.state == 'draft':
                    order.write({'state': 'sent', 'date_order': fields.Datetime.now()})
                    product_purchase_product_status_status_submitted = self.env.ref('pinnacle_purchase.purchase_product_status_status_submitted')
                    product_purchase_product_status_status_draft = self.env.ref('pinnacle_purchase.purchase_product_status_status_draft_new')
                    if not product_purchase_product_status_status_submitted:
                        raise UserError(_('Purchase Status Status Submitted Product Not Found'))
                    elif not product_purchase_product_status_status_draft:
                        raise UserError(_('Purchase Status Status Draft Product Not Found'))
                    else:
                        for product_status_line in order.product_status_line:
                            if product_status_line.product_status_status.id == product_purchase_product_status_status_draft.id:
                                product_status_line.product_status_status = product_purchase_product_status_status_submitted.id
                        for purchase_decoration_group in order.purchase_decoration_groups:
                            if purchase_decoration_group.product_status_status.id == product_purchase_product_status_status_draft.id:
                                purchase_decoration_group.product_status_status = product_purchase_product_status_status_submitted.id
        if self._context.get('default_model') == 'purchase.decoration.group.for.tracking' or self._context.get('default_model') == 'purchase.order.line.for.tracking':
            if self._context.get('default_res_id'):
                if self.subtype_id:
                    subtype_id = self.subtype_id.id
                else:
                    subtype_id = self.sudo().env.ref('mail.mt_comment', raise_if_not_found=False).id
                res_id = self._context.get('default_res_id')
                po_id = self.env[self._context.get('default_model')].browse(res_id).purchase_order_id.id
                all_mail_values = self.get_mail_values([res_id])
                for res_id, mail_values in all_mail_values.iteritems():
                    self.env['purchase.order'].browse(po_id).message_post(
                                message_type=self.message_type,
                                subtype_id=subtype_id,
                                **mail_values)
        if self._context.get('email_template_fulfillment_receiving_order', False) and self._context.get('default_model') == 'purchase.order' and self._context.get('default_res_id'):
            order = self.env['purchase.order'].browse([self._context['default_res_id']])
            order.sent_to_warehouse_flag = True
        return super(MailComposeMessage, self.with_context(mail_post_autofollow=True)).send_mail(auto_commit=auto_commit)

    @api.onchange('partner_ids')
    def onchange_partner_ids(self):
        if self._context.get('pin_partner_ids', False):
            partner_ids = self._context.get('pin_partner_ids')
            return {'domain': {'partner_ids': [('id', 'in', partner_ids)]}}
