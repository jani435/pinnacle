import logging
import threading

from odoo import _, api, fields, models, registry, SUPERUSER_ID
from odoo.osv import expression

_logger = logging.getLogger(__name__)

class ResPartner(models.Model):
    _inherit = 'res.partner'

    @api.multi
    def render_view_to_po(self):
        self.ensure_one()
        context = dict(self._context or {})
        context['default_account_manager'] = self.user_id.id
        context['default_channel_id'] = self.channel_id.id
        context['default_program_id'] = self.program_id.id
        context['default_so_partner_id'] = self.id

        return {
            'name': _('RFQs and Purchases'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'purchase.order',
            'type': 'ir.actions.act_window',
             'domain': [('so_partner_id', '=', self.id)],
            'context': context,
        }

    @api.multi
    def read(self, fields=None, load='_classic_read'):
        result = super(ResPartner, self).read(fields=fields, load=load)
        if self.env.context.get('email_address_custom'):
            res_id = self.env.context.get('process_res')
            model_name = self.env.context.get('process_model')
            if self.env.context.get('show_email') and res_id and model_name == 'purchase.order':
                display_name, partner_id = self.env['purchase.order'].browse(res_id)._get_email_send_to()
                for res in result:
                    if res.get('display_name'):
                        if res.get('id') == partner_id:
                            res.update({'display_name': display_name})
                        else:
                            partner_get = self.browse(res.get('id'))
                            res.update({'display_name': ('%s <%s>' % (partner_get.name ,partner_get.email))})
        elif self.env.context.get('show_email'):
            for res in result:
                cur_id = self.browse(res.get('id'))
                if res.get('display_name'):
                    res.update({'display_name': '%s <%s>' % ( cur_id.name, cur_id.email)})
        return result

    #inherit to sent context while sending email
    @api.multi
    def _notify_by_email(self, message, force_send=False, send_after_commit=True, user_signature=True):
        """ Method to send email linked to notified messages. The recipients are
        the recordset on which this method is called.

        :param boolean force_send: send notification emails now instead of letting the scheduler handle the email queue
        :param boolean send_after_commit: send notification emails after the transaction end instead of durign the
                                          transaction; this option is used only if force_send is True
        :param user_signature: add current user signature to notification emails """
        if not self.ids:
            return True

        # existing custom notification email
        base_template = None
        if message.model and self._context.get('custom_layout', False):
            base_template = self.env.ref(self._context['custom_layout'], raise_if_not_found=False)
        if not base_template:
            base_template = self.env.ref('pinnacle_sales.mail_template_data_notification_email_default')

        base_template_ctx = self._notify_prepare_template_context(message)
        if not user_signature:
            base_template_ctx['signature'] = False
        base_mail_values = self._notify_prepare_email_values(message)

        # classify recipients: actions / no action
        if message.model and message.res_id and hasattr(self.env[message.model], '_message_notification_recipients'):
            recipients = self.env[message.model].browse(message.res_id)._message_notification_recipients(message, self)
        else:
            recipients = self.env['mail.thread']._message_notification_recipients(message, self)

        emails = self.env['mail.mail']
        recipients_nbr, recipients_max = 0, 50
        for email_type, recipient_template_values in recipients.iteritems():
            if recipient_template_values['followers']:
                # generate notification email content
                template_fol_values = dict(base_template_ctx, **recipient_template_values)  # fixme: set button_unfollow to none
                template_fol_values['has_button_follow'] = False
                template_fol = base_template.with_context(**template_fol_values)
                # generate templates for followers and not followers
                fol_values = template_fol.generate_email(message.id, fields=['body_html', 'subject'])
                # send email
                new_emails, new_recipients_nbr = self._notify_send(fol_values['body'], fol_values['subject'], recipient_template_values['followers'], **base_mail_values)
                # update notifications
                self._notify_udpate_notifications(new_emails)

                emails |= new_emails
                recipients_nbr += new_recipients_nbr
            if recipient_template_values['not_followers']:
                # generate notification email content
                template_not_values = dict(base_template_ctx, **recipient_template_values)  # fixme: set button_follow to none
                template_not_values['has_button_unfollow'] = False
                template_not = base_template.with_context(**template_not_values)
                # generate templates for followers and not followers
                not_values = template_not.generate_email(message.id, fields=['body_html', 'subject'])
                # send email
                new_emails, new_recipients_nbr = self._notify_send(not_values['body'], not_values['subject'], recipient_template_values['not_followers'], **base_mail_values)
                # update notifications
                self._notify_udpate_notifications(new_emails)

                emails |= new_emails
                recipients_nbr += new_recipients_nbr

        # NOTE:
        #   1. for more than 50 followers, use the queue system
        #   2. do not send emails immediately if the registry is not loaded,
        #      to prevent sending email during a simple update of the database
        #      using the command-line.
        test_mode = getattr(threading.currentThread(), 'testing', False)
        if force_send and recipients_nbr < recipients_max and \
                (not self.pool._init or test_mode):
            email_ids = emails.ids
            dbname = self.env.cr.dbname

            def send_notifications():
                db_registry = registry(dbname)
                with db_registry.cursor() as cr:
                    env = api.Environment(cr, SUPERUSER_ID, self.env.context.copy())
                    env['mail.mail'].browse(email_ids).send()

            # unless asked specifically, send emails after the transaction to
            # avoid side effects due to emails being sent while the transaction fails
            if not test_mode and send_after_commit:
                self._cr.after('commit', send_notifications)
            else:
                emails.send()

        return True


    @api.depends('state_id')
    def _compute_name(self):
        for this in self:
            if this.rating_line:
                rating = 0
                count = 0
                for line in this.rating_line:
                    rating = rating+int(line.rating)
                    count += 1
                this.rating_point = int(rating/count)
                this.rating = str(this.rating_point)

    rating = fields.Selection([('0', '0'), ('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5')],compute='_compute_name')
    rating_point = fields.Integer(compute='_compute_name', string='Average', default=0)
    rating_line = fields.One2many('rating.line', 'partner_id')
    order_cutoff_time = fields.Selection([('12AM', '12AM'), ('1AM', '1AM'), ('2AM', '2AM'), ('3AM', '3AM'), ('4AM', '4AM'),
                                            ('5AM', '5AM'), ('6AM', '6AM'), ('7AM', '7AM'), ('8AM', '8AM'), ('9AM', '9AM'),
                                            ('10AM', '10AM'), ('11PM', '11PM'), ('12PM', '12PM'), ('1PM', '1PM'), ('2PM', '2PM'),
                                            ('3PM', '3PM'), ('4PM', '4PM'), ('5PM', '5PM'), ('6PM', '6PM'), ('7PM', '7PM'),
                                            ('8PM', '8PM'), ('9PM', '9PM'), ('10PM', '10PM'), ('11PM', '11PM')], 'Order Cut Off Time')
    purchase_order_count_customer = fields.Integer("Customer's PO", compute="_customer_purchase_order")

    @api.multi
    def _customer_purchase_order(self):
        purchase_order = self.env['purchase.order']
        for this in self:
            this.purchase_order_count_customer = purchase_order.search_count([(
                'so_partner_id', '=', this.id)])
class RatingLine(models.Model):
    _name = 'rating.line'

    name = fields.Many2one('purchase.order', 'Purchase Order')
    user_id = fields.Many2one('res.users', 'Employee')
    rate_date = fields.Date('Date')
    rating = fields.Selection([('0', '0'), ('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5')])
    notes = fields.Char()
    partner_id = fields.Many2one('res.partner')
