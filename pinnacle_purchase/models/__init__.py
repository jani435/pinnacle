# -*- coding: utf-8 -*-

from . import res_partner
from . import purchase
from . import sale
from . import product
from . import mail
from . import purchase_order_generate