odoo.define('odoo.pinnacle_drag_drop_attach', function (require) {

    var core    = require('web.core');
    var form_relational = require('web.form_relational');
    var QWeb = core.qweb;
    var _t = core._t;
    var FormView_extend = require('web.FormView');
    var ajax = require('web.ajax');
    var data = require('web.data');
    var composer = require('mail.composer');
    var field_name = ''
    var data_id
    var fheight  
    var FormView_extend_new = FormView_extend.extend({
        init: function(){
            this._super.apply(this, arguments);
            this.events = _.extend(this.events, {
                "dragover div.o_form_sheet, td[data-field='spec_file_url'], td[data-field='spec_file2_url'], td[data-field='vendor_file_url']": "dragging",
                "drop div.o_form_sheet, td[data-field='spec_file_url'], td[data-field='spec_file2_url'], td[data-field='vendor_file_url']": "dropping",
                "dragleave div.o_form_sheet, td[data-field='spec_file_url'], td[data-field='spec_file2_url'], td[data-field='vendor_file_url']": "dragleaving",
                "dragexit div.o_form_sheet, td[data-field='spec_file_url'], td[data-field='spec_file2_url'], td[data-field='vendor_file_url']": "dragexiting",
                "dragend div.o_form_sheet, td[data-field='spec_file_url'], td[data-field='spec_file2_url'], td[data-field='vendor_file_url']": "dragending",
                "dragstart div.o_notebook, a.o_form_field": "dragprevent",
            });
        },
        dragprevent: function(e){
            e.preventDefault();
        },
        dragging: function(e){  
            var self = this;
                field_name = e.currentTarget.dataset.field
                data_id = $(e.currentTarget).closest("tr").attr("data-id");
                e.preventDefault();e.stopPropagation();
                if (! e.currentTarget.dataset.field && self.get("actual_mode") === "view"){
                    if (!self.$el.find('.o_form_sheet').hasClass('adjust_sheet'))
                        self.$el.find('.o_form_sheet').addClass('adjust_sheet');
                    if (!self.$el.find('div.hidden_formview_drop').hasClass('hidden_drop_formview_highlight'))
                        self.$el.find('div.hidden_formview_drop').addClass('hidden_drop_formview_highlight');
                        self.$el.find('div.hidden_drop_formview_highlight').css('top',-1*self.$el.find('.o_form_sheet').height())
                }else{
                    if (field_name == 'spec_file_url' || field_name == 'spec_file2_url' || field_name == 'vendor_file_url')
                        $(e.currentTarget).closest("tr").height(100);
                        $(e.currentTarget).closest("tr").children(("td[data-field="+field_name+"]")).css("background-color", "#075D86");
                }
        },
        dragleaving: function(e){
            this.toggle_effect(e)
        },
        dragexiting: function(e){
            this.toggle_effect(e)
        },
        dragending: function(e){
            this.toggle_effect(e)
        },
        dropping: function(e){
            var self = this;
            if ((self.get("actual_mode") === "view" && ! e.currentTarget.dataset.field) || (e.currentTarget.dataset.field && self.get("actual_mode") === "edit")){
                    if(e.originalEvent.dataTransfer && e.originalEvent.dataTransfer.files.length){
                        self.toggle_effect(e);
                        self.upload_files(e.originalEvent.dataTransfer.files);
                    }
                }
        },
        toggle_effect: function(e){
            var self = this;
            e.preventDefault();e.stopPropagation();
            if ((self.get("actual_mode") === "view" && ! e.currentTarget.dataset.field) || (e.currentTarget.dataset.field && self.get("actual_mode") === "edit")){
                self.$el.find('div.hidden_formview_drop').removeClass('hidden_drop_formview_highlight');
                self.$el.find('.o_form_sheet').removeClass('adjust_sheet');
            }
            $(e.currentTarget).closest("tr").children(("td[data-field="+field_name+"]")).css("background-color", "#D3D3D3");
            
        },
        upload_files: function(files){
            var self = this;
            var cfs_attachment = false
            var id = self.datarecord.id
            var allwoed_exts = ['image/png', 'image/jpeg', 'image/bmp', 'application/postscript', 'application/pdf', 'application/octet-stream']
            if (field_name == 'spec_file_url' || field_name == 'spec_file2_url' || field_name == 'vendor_file_url'){
                cfs_attachment = true;
                id = data_id;
            }
            _.each(files, function(file){
                if ($.inArray(file.type, allwoed_exts) == -1){
                    return alert("File format not supported");
                }
                var querydata = new FormData();
                    querydata.append('csrf_token',core.csrf_token);
                    querydata.append('callback', 'oe_fileupload_temp86');
                    querydata.append('ufile',file);
                    querydata.append('model', self.model);
                    querydata.append('id', id);
                    querydata.append('cfs_attachment', cfs_attachment);
                    querydata.append('field_name', field_name);
                    $.ajax({
                        url: '/web/binary/upload_attachment',
                        type: 'POST',
                        data: querydata,
                        cache: false,
                        processData: false,  
                        contentType: false,
                        success: function(id){
                            self.load_record(self.datarecord);
                        },
                    });
                
            });
        }
    });
    core.view_registry.add('form', FormView_extend_new);
});

odoo.define('pinnacle_drag_drop_attach.chatter', function (require) {

    var core = require('web.core');
    var data = require('web.data');
    var composer = require('mail.composer');
    var field_name = ''
    var args = {}
    var chatter_height = 0
    var basic_composer = composer.BasicComposer.include({

        init: function (parent, dataset, options) {
            var ctx = dataset['context'];
            ctx = _.extend(ctx, {'url': window.location.href});
            dataset['context'] = ctx
            this._super(parent, dataset, options);
            this.events = _.extend(this.events, {
                "dragover .o_composer": "dragging",
                "drop .o_composer": "dropping",
                "dragleave .o_composer": "dragleaving",
                "dragexit .o_composer": "dragexiting",
                "dragend .o_composer": "dragending",
            });
        },
        toggle_effect: function(e){
            e.preventDefault();e.stopPropagation();
            $('div.o_composer').removeClass('adjust_sheet');
            $('div.o_composer').height(chatter_height).css("background", "#ffffff");
        },
        dragging: function(e){
            e.preventDefault();e.stopPropagation();
            if (!$('div.o_composer').hasClass('adjust_sheet')){
                $('div.o_composer').addClass('adjust_sheet');
                chatter_height = $('div.o_composer').height();
                $('div.o_composer').height(200).css("background", "#8F8F8F");
            }
        },
        dragleaving: function(e){
            this.toggle_effect(e)
        },
        dragexiting: function(e){
            this.toggle_effect(e)
        },
        dragending: function(e){
            this.toggle_effect(e)
        },
        dropping: function(e){
            e.preventDefault();e.stopPropagation();
            if(e.originalEvent.dataTransfer && e.originalEvent.dataTransfer.files.length){
                var self = this
                var event = e
                var attachments = self.get("attachment_ids", [])
                var arglist = []
                self.toggle_effect(e)
                _.each(e.originalEvent.dataTransfer.files, function(file){
                    var querydata = new FormData();
                        querydata.append('csrf_token',core.csrf_token);
                        querydata.append('callback', 'o_chat_fileupload91');
                        querydata.append('ufile',file);
                        querydata.append('model', 'mail.compose.message');
                        querydata.append('id', 0);
                        querydata.append('multi', true);
                        $.ajax({
                            url: '/web/binary/upload_attachment',
                            type: 'POST',
                            data: querydata,
                            cache: false,
                            processData: false,  
                            contentType: false,
                            async: false,
                            success: function(id){
                                args = {
                                            filename: file.name,
                                            id: parseInt(id),
                                            mimetype: file.type
                                        }
                                arglist.push(args)
                                attachments.push({
                                    'id': parseInt(id),
                                    'name': file.name,
                                    'filename': file.name,
                                    'url': '',
                                    'upload': true,
                                    'mimetype': file.type,
                                });
                            },
                            error: function(xhr, textStatus, errorThrown){
                               console.log(errorThrown);
                            },
                        });
                
                });
                self.set('attachment_ids', attachments);
                _.each(arglist, function(args){
                    self.on_attachment_loaded(event, args);
                });

            }

        },

    });

});
